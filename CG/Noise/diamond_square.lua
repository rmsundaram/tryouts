#!/usr/bin/env lua

-- Round to nearest integer
function round(f)
  return math.floor(f + 0.5)
end

-- Count leading zeros in a number and report if it’s power of two (POT)
function clz_pot(i)
  local num_bits = (math.maxinteger > 0X7FFFFFFF) and 64 or 32
  if i == 0 then return num_bits, false end
  local mask = 1 << (num_bits - 1)
  local count = 0
  while ((i & mask) == 0) do
    i = i << 1
    count = count + 1
  end
  return count, ((i << 1) == 0) -- POTs have only one bit set
end

-- Compute log2 of an integer
function log2(i)
  if i == 0 then return 1 end
  local clz, pot = clz_pot(i)
  local num_bits = (math.maxinteger > 0X7FFFFFFF) and 64 or 32
  return (num_bits - 1) - clz + (pot and 0 or 1)
end

-- Round up to nearest power-of-two integer
function round_pow2(i)
  return 1 << log2(i)  -- (2 ^ log2) results in a float
end

-- Alternative float-based round_pow2; unused here
function round_pow2f(f)
  local l = math.log(f, 2)
  if (l % 1.0) ~= 0.0 then
    l = math.floor(l) + 1.0
  end
  return math.tointeger(2^l)
end

-- Average of elements; nils handled gracefully
function avg(...)
  local values = table.pack(...)
  local sum, n = 0, 0
  for i = 1, values.n do
    if values[i] ~= nil then
      sum = sum + values[i]
      n = n + 1
    end
  end
  assert(n ~= 0, "All inputs to avg() can't be nil")
  return sum / n
end

-- Clamp given input to [l, u]; defaults to [0, 1]
function clamp(f, l, u)
  l = l or 0
  u = u or 1
  return (f > 1) and 1 or ((f < 0) and 0 or f)
end

function is_odd(n)
  return (n & 1) == 1  -- only false, nil and nan are falsey, 0 is true
end

-- https://stevelosh.com/blog/2016/06/diamond-square/
function jitter(step)
  local initial_spread = 0.3
  local spread = initial_spread / math.pow(2, step)
  return (2 * spread * math.random()) - spread
end

if (#arg < 2) then
  print("usage: " .. arg[0] .. " SIZE OUTFILE")
  os.exit()
end
local size_str = arg[1]
local file_name = arg[2]

local size_int = math.tointeger(size_str)
assert(size_int and size_int >= 4, "Invalid size; should be ≥ 4")

-- diamond-square works on POT, odd-sized square texture (2ⁿ + 1) only
local grid = { size = round_pow2(size_int) + 1 }  -- size² values ∈ [0, 1]
function grid:at(x, y)
  -- TODO: Set self.size = 2ⁿ and do wrap around look-ups for seamless texture
  -- https://bluh.org/code-the-diamond-square-algorithm/
  if x < 0 or y < 0 or x >= self.size or y >= self.size then return nil end
  local idx = y * self.size + x
  return self[idx]
end
function grid:set(x, y, value)
  assert(tostring(value) ~= tostring(0/0), "Trying to store a nil value")
  local idx = y * self.size + x
  self[idx] = value
end
function grid:sampled_avg(x, y, offset, jitter)
  local value = avg(self:at(x + offset, y),
                    self:at(x, y - offset),
                    self:at(x - offset, y),
                    self:at(x, y + offset)) + jitter
  self:set(x, y, value)
end
-- translate and scale values to be in [0, 1]
function grid:normalize()
  local min, max = math.huge, -math.huge
  local len = self.size * self.size - 1
  for i = 0, len do
    min = self[i] < min and self[i] or min
    max = self[i] > max and self[i] or max
  end
  local band = max - min
  local scale = 1 / band
  for i = 0, len do
    self[i] = (self[i] - min) * scale
  end
end
--[[ Uncomment to return 0, instead of nil, for missing elements
function grid:__index(index)
  return 0
end
setmetatable(grid, grid)
--]]

function grid:square(step)
  local squares_in_1d = 2 ^ (step - 1)
  local offset = (self.size - 1) // squares_in_1d
  for row = 0, (squares_in_1d - 1) do
    for col = 0, (squares_in_1d - 1) do
      local left  = col * offset
      local right = (col + 1) * offset
      local top   = row * offset
      local bot   = (row + 1) * offset
      local value = avg(self:at(left, top),
                        self:at(right, top),
                        self:at(left, bot),
                        self:at(right, bot)) + jitter(step)
      local dst_col = left + ((right - left) // 2)
      local dst_row = top + ((bot - top) // 2)
      self:set(dst_col, dst_row, value)
    end
  end
end

function grid:diamond(step)
  local squares_in_1d = 2 ^ (step - 1)
  local offset = (self.size - 1) // squares_in_1d
  for row = 0, (squares_in_1d - 1) do
    for col = 0, (squares_in_1d - 1) do
      local left  = col * offset
      local right = (col + 1) * offset
      local top   = row * offset
      local bot   = (row + 1) * offset
      local mid_x = left + ((right - left) // 2)
      local mid_y = top + ((bot - top) // 2)
      local offset_half = offset >> 1
      self:sampled_avg(mid_x - offset_half, mid_y, offset_half, jitter(step))
      self:sampled_avg(mid_x, mid_y + offset_half, offset_half, jitter(step))
      self:sampled_avg(mid_x + offset_half, mid_y, offset_half, jitter(step))
      self:sampled_avg(mid_x, mid_y - offset_half, offset_half, jitter(step))
    end
  end
end

-- https://en.wikipedia.org/wiki/Diamond-square_algorithm
math.randomseed(os.time())
-- Initialize Corners
grid:set(0, 0, jitter(0))
grid:set(0, grid.size - 1, jitter(0))
grid:set(grid.size - 1, 0, jitter(0))
grid:set(grid.size - 1, grid.size - 1, jitter(0))

-- Squares and Diamonds
local steps = log2(grid.size - 1)
for i = 1, steps do
  grid:square(i)
  grid:diamond(i)
end

grid:normalize()

-- Write to portable graymap file
local out_file = io.open(file_name .. ".pgm", "w")
local max_colour = 255
out_file:write(
  string.format("P2\n%u %u\n%u\n", grid.size, grid.size, max_colour))
for row = 0, (grid.size-1) do
  local line = {}
  for col = 0, (grid.size-1) do
    local intensity = grid:at(col, row)
    assert((intensity >= 0) and (intensity <= 1),
           string.format("Intensity @ (%u, %u) exceeds limits: %f",
                         col, row, intensity))
    line[#line+1] = round(intensity * max_colour)
  end
  out_file:write(table.concat(line, " "))
  out_file:write("\n")
end
out_file:close()
