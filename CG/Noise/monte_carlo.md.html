<meta charset="utf-8">
    **Monte Carlo Methods**

A broad class of computational algorithms that rely on repeated random sampling to obtain numerical results.  The underlying concept is to use randomness to solve problems that might be deterministic in principle.  The name comes from a casino.

> "MC programs give a statistical estimate of an answer, and this estimate gets more and more accurate the longer you run it.  This basic characteristic of simple programs producing noisy but ever-better answers is what MC is all about."
>    -- Ray Tracing: The Rest of Your Life

Monte Carlo methods are mainly used in three problem classes:

1. Optimization
2. Numerical integration
3. Generating draws from a probability distribution

It’s especially good for applications like graphics where great accuracy isn’t needed.

# Canonical Example - Estimation of π

1. Consider a square and a circle inscribe within it.
2. Pick random points within a square.
3. Observe that the ratio of the points within the unit circle to the unit square will be the same as the ratio between the areas of the circle and the square.
4. Generate random points and arrive at the fraction.
5. Use the fraction to arrive at the value of π.

$$
ratio = \frac{\pi r^2}{(2r)^2} \\
ratio = \frac{\pi}{4}  \\
\boxed{\pi = 4 \times ratio}
$$

!!! Note
    Longer the program runs -- larger the sample set in arriving at ratio -- closer the estimation of π.

## Code

``` lua
-- Monte Carlo method of π estimation

function dist2(centre, point)
  local dx = centre[1] - point[1]
  local dy = centre[2] - point[2]
  return (dx * dx) + (dy * dy)
end

function ratio(count, seed)
  math.randomseed(seed)
  local inside_circle = 0
  -- without args math.random generates values in [0, 1)
  local centre = {0.5, 0.5}
  local r2 = 0.5 * 0.5
  for i = 1, count do
    local rand_point = {math.random(), math.random()}
    if (dist2(centre, rand_point) <= r2) then
      inside_circle = inside_circle + 1
    end
  end
  return inside_circle / count
end

io.write("Estimate of π: ", 4 * ratio(tonumber(arg[1]), tonumber(arg[2])), '\n')
```

# Demerits

- **Computation**: MC methods potentially require massive amount of computation before arriving at meaningful results
- **Law of Diminishing Returns**: each sample helps lesser than the last in getting closer to the actual value

The worst part of Monte Carlo methods are them obeying the _Law of Diminishing Returns_.

## Stratifying

To be (probabilistically) fair, elements making the sample need to be chosen randomly with equal probability.  This is the mitigation for MC: _stratifying_ the samples; often called jittering.  Instead of taking random samples, we divide the space into a grid and take a sample within each cell.

Unfortunately, this mitigation too diminishes with the problem space’s dimension.  So stratifying is useful only for 1 and 2D; its use in higher dimensions will be insignificant e.g. in the sphere-volume case, the advantage would be negligible.

# References

1. [Ray Tracing: The Rest of Your Life][]
2. [Monte Carlo Integration Explanation in 1D][demofox]
3. [Mathematical Foundations of Monte Carlo Methods][ScratchAPixel.com]
4. [Monte Carlo method][] - Wikipedia
5. _Real-World Algorithms_ - Panos Louridas; §16.3 Power Games, §16.4 Searching for Primes
6. [Monte Carlo in BBC Documentary][bbc-monte-carlo]

[Ray Tracing: The Rest of Your Life]: http://www.realtimerendering.com/raytracing/Ray%20Tracing_%20the%20Rest%20of%20Your%20Life.PDF
[Monte Carlo method]: https://en.wikipedia.org/wiki/Monte_Carlo_method
[ScratchAPixel.com]: https://www.scratchapixel.com/lessons/mathematics-physics-for-computer-graphics/monte-carlo-methods-mathematical-foundations
[demofox]: https://blog.demofox.org/2018/06/12/monte-carlo-integration-explanation-in-1d/
[bbc-monte-carlo]: https://www.scratchapixel.com/images/upload/monte-carlo-methods/BBC-code3.mp4

<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none'};</script>
