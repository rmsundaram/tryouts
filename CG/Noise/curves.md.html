<meta charset="utf-8">
    **Curves**

Curves are used to define shapes (static) and paths (dynamic).  When seen dynamically, curves could be describing motion of a particle, gradient of a colour, etc.  Usually we’ve two or more known values/knots/extremes/control points; we use those to formulate an interpolating curve.  Hereon we generate values at missing, intermediate points between control points.  The interpolating parameter `t` usually varies in the domain `[0, 1]`.

When interpolating between known values, we’ve a lot of choices to go with.  To list a few

* Linear (straight line)
* Non-linear (curve)
  - [Polynomial](#polynomialfunctions)
  - [Trigonometric](#trigonometricinterpolation)
  - Spline (curve defined piece-wise by smaller polynomial curves)
    + [Hermite](#hermitecurves)
      - [Catmull-Rom](#catmull-rom)
      - [Cardinal](#cardinal)
      - [Kochanek-Bartels](#kochanek–bartels) (TCB)
      - Natural
    + [Bézier](#béziercurves)
    + B-spline

# Introduction

## Linear vs Non-linear

Linear interpolation, though the most intuitive and straight-forward, has the problem of discontinuity.  The change at every data point is abrupt i.e. the derivative, P', doesn’t exist at those points -- so linear interpolation gives a continuous curve with a discontinuous derivative ($C_0$).  Most natural phenomena are contrary to this: change is gradual (analog) e.g. a stopped car doesn’t linearly interpolate from one point to another; it starts off slowly, travels at a particular speed, and stops gradually -- cross-fades i.e. there’s an ease-in and ease-out, giving [a sigmoid curve][easeInOut] when plotting speed against time.

Likewise, when mixing two quantities, lerping leads to visible discontinuities and obvious patterns ([Mach Bands][]); this is the reason bicubic scaling looks nicer than bilinear scaling in image processing.  Ken Perlin, instead of plain linear interpolation, lerped with the parameter fed through a cubic Hermite basis function to avoid mach bands.

Hence non-linear interpolation methods are valuable tools in computer graphics.

> "A curve defines a path traversed over time."

!!! Note: Shape vs Path
    Curves can be considered both _static_ or _dynamic_.  Static: curves are used to create a shape.  Dynamic: one takes into account the timing -- parameter $t$ is time here -- and change of position, velocity along the curve as an object’s path as $t$ varies [[3](#references)].

## Curve Fitting

Function approximation is a branch of numerical analysis.  There’re two kinds of approximation:

- Interpolation: interpolating curve goes through all knots (known values/control points)
- Least-squares: approximating curve just gets attracted to the knots

What we’re trying to do is a special case of [curve fitting][] - coming up with the equation of an interpolating curve.  Curves are _threaded through_ specified control points; metaphor of a rope with knots is used.  Usually approximating curves are

- Trying to finding the trend in a bunch of random points
- Line fitting (sort of _dimensionality reduction_)
  * We take data points (`nD` e.g. 2D in case of _years_ against _rainfall_) and find the trend line (`1D`) out of them

Other functions like parabola, polynomials, … can be fit too.

## Useful Curve Properties

- Local control (easy to build and modify)
- Stability
- Continuity (smoothness)
- Ease of rendering

[easeInOut]: https://easings.net/en#easeInOutCubic
[Mach Bands]: https://en.wikipedia.org/wiki/Mach_bands
[curve fitting]: https://www.youtube.com/watch?v=tl5QNhSe0Yk

# Continuity

Since we want to arrive at interpolations that’re smooth, we need to talk about continuity.  Also when piecing curves together to form splines, how continuous they’re matters.  After all, splines are curves too and they’ve the same continuity classifications.

> "Informally, we can think of a continuous function as one that we can draw without ever lifting the pen from the page."

Parametric continuity is continuity of basis functions, while geometric continuity is continuity of the curve itself [[13](#references)].

## Parametric Continuity

[Parametric continuity][smoothness] defines the smoothness of curves over some interval $(a, b)$:

1. $C^0$: Curve continuous
2. $C^1$: Curve and first derivative continuous (tangential continuity in interval); shared tangent
3. $C^2$: Curve, first and second derivative continuous (curvature continuity in interval)

## Geometric Continuity

1. $G^0$: same as $C^0$ -- two curves share a common end point
2. $G^1$: tangents share direction but not magnitude

> "In many cases $G^1$ continuity is good enough for our purposes [[1](#references)]."

We can achieve $C^2$ continuity by guaranteeing that the second derivative vectors are equal at the end of one segment and the start of the next segment.  In general, $C^n$ continuity implies $G^n$ continuity, unless the n<sup>th</sup> derivatives are zero [[2](#references)].

[This blog post][interpolation-derivatives] has graphs of derivatives of linear, cosine and cubic Hermite splines.

[smoothness]: https://en.wikipedia.org/wiki/Smoothness#Parametric_continuity
[interpolation-derivatives]: https://codeplea.com/introduction-to-splines

# Polynomial functions

Number of known values (control points) minus one is the polynomial’s degree.  Line equation with 2 points, quadratic curve with 3 points, etc.

* Polynomial fitting of points is called _Lagrange Interpolation_ [[14](#references)]
* Higher the degree, more [turning points][]
  - Changing a control point affect entire curve (global control)
* Linear polynomial
  - Line: constant slope
* Quadratic polynomial
  - Parabola: a line with one turning point
  - One change of direction e.g. negative gradient, zero gradient and positive gradient for U-style parabola
* Cubic polynomial

!!! Tip: Knot Limit
    There are **at most** (n - 1) _changes of direction_ for n-degree polynomial e.g. quadratic usually has 1 maxima/minima (control points/knots); cubic usually has 2 but could be lesser too e.g. $y = x^3$ has only one minima.
    
    Conversely, a curve with $n$ turning points would’ve at least degree $n + 1$.

[How to find turning points of a polynomial?](https://sciencing.com/turning-points-polynomial-8396226.html)
[turning points]: http://www.onemathematicalcat.org/Math/Precalculus_obj/turningPoints.htm

## Derivatives of Polynomial functions

!!! Tip: Position, Velocity and Acceleration
    If a curve defines the path of an object, its first derivative is instantaneous velocity, second is acceleration -- that’s why cubic curves can have inflection points!

- A simpler polynomial -- one degree less -- describing how the original polynomial changes
- Derivative is zero when the original polynomial is at a turning point -- the point at which the graph is neither increasing nor decreasing
- Since derivative has degree one less than the original polynomial, there may be as many as two less turning points than the degree of the original polynomial
- Roots of derivative are places where the original polynomial has turning points

## Cubic Polynomials

!!! Warning: Polynomial Wiggle
    Problem with polynomial interpolation is that more the number of points we add, higher the degree, leading to unstable, wiggly curves between points.  Hence higher degree polynomials doesn’t find widespread use in computer graphics; spline interpolation is preferred [[3](#references)].

Cubic curves are defined by

$$
Q(t) = a + bt + ct^2 + dt^3
$$

Many cubic polynomial curves are joined together in forming a piece-wise [cubic Hermite](#hermitecurves) spline; the most common in computer graphics.

!!! Note: Sweet spot of cubic curves
    Generally cubic curves are preferred in computer graphics as it’s a good balance between flexibility, smoothness and complexity.  Quadratic curves aren’t as smooth as cubic since the latter has continuous second derivative too e.g. $f(x) = x^3 + 2x$ then $f'(x) = 3x^2 + 2$ and $f''(x) = 6x$.  Cubic curves can [change direction][inflection point] too: $f''(x) = 0$.

> "Cubic interpolation is better than linear interpolation in many aspects such as smoothness of the function and higher accuracy in approximating the original function.
>
> Linear interpolation is limited [...] _Mach Bands_ can occur in the resulting image (bands of perceptually exaggerated contrast at discontinuities in color).  To support more colors and eliminate Mach Bands, we can choose a higher-order interpolation function.  A favorite choice in computer graphics is the cubic polynomial.[[6](#references)]"

[inflection point]: https://en.wikipedia.org/wiki/Inflection_point

# Bézier Curves

* Most commonly occurring curve in computer graphics
* Essentially an easy way to build polynomial functions with better (global) control over shape
* Any cubic curve can be represented as a cubic Bézier curve
* Cubic Hermite and Bézier curves are just different representations of the same thing
* Quadratic is used less than cubic due to lesser degree of freedom

!!! Warning: Flipped Tangent
    Assume you draw tangents for Bézier curves from the end to control points.  Generally, tangents to a curve are drawn in the general direction of the curve.  This doesn’t hold for the second tangent of Bézier curves i.e. they’re negated (unlike Hermite curves) [[13](#references)].

Linear

$$
C(t) = (1 - t) P_1 + t P_2
$$

Quadratic

$$
C(t) = (1 - t)^2 P_1 + 2 t(1-t) C_1 + t^2 P_2
$$

Cubic

$$
C(t) = (1 - t)^3 P_1 + 3t(1 - t)^2 C_1 + 3t^2(1 - t) C_2 + t^3 P_2
$$

Notice that the coefficients are actually [Binomial coefficients][] that form the Pascal Triangle; the mathematical basis of Bézier curves are [Bernstein polynomial][].

!!! Tip: Intuition
    Integrating multiple levels of linear interpolation together forms a Bézier curve.  When a curve is differentiated, we get a line; doing the reverse process yields us a curve.

A quadratic Bézier curve is obtained by linearly interpolating along two straight lines ($P_1 \rightarrow C_1$, $C_1 \rightarrow P_2$); again linearly interpolating between resulting points gives us a point on the final curve; doing this for all $t$ gives us the curve.  A cubic Bézier is obtained by lerping between three straight lines, followed by constructing a quadratic Bézier curve with the resulting three points.

Though _forward differencing_ -- uniformly increasing $t$ -- and drawing a Bézier curve is an option, it can be expensive; better control over resolution and speed are given by the _recursive subdivision_ method; subdivide until the size of straight lines to draw the curve is sufficiently small.

## Rational Bézier Curves

By defining a control point in homogeneous coordinates, we can additionally associate a weight to each control point.  Say in $\Bbb{R}^3$, a 4-tuple value will be a rational Bézier curve’s control point.

- A higher weight for a control point means it pulls the curve harder towards it
- Weights enable Bézier curves to define circular arcs, ellipses, hyperbolas and other conic curves
- Rational Bézier curves are perspective transform-invariant, not just affine!
- Control points can be placed at ∞

Refer [[12](#references)] for details.

## Useful Properties

Bézier curves have many desirable properties; these are discussed in detail by [[11](#references)] and [[12](#references)]

- Affine invariance -- basis functions are a partition of unity
  - Transforming a curve is identical to transforming constituent control points (as long as the transformations are affine)
  - Bernstein polynomials are a partition of unity
- Convex hull -- basis functions are all positive and add up to 1 (convex combination)
  - Curve stays within its control points’ convex hull
- Variation Diminishing
  - For any line in $\Bbb{R}^2$ or plane in $\Bbb{R}^3$ ... the number of times it intersecting the curve will be less than or equal number of times it intersecting the curve’s control polygon
- Derivative of a Bézier curve is also a Bézier curve: useful when defining curves that are piece-wise continuous

[Binomial coefficients]: https://en.wikipedia.org/wiki/Binomial_coefficient
[Bernstein polynomial]: https://en.wikipedia.org/wiki/Bernstein_polynomial

# Hermite Curves

> "Sometimes it is more natural to define a curve in terms of its initial and ending positions and velocities than with control points."

Pronounced _er-meet_.

- Need tangents (derivatives) at every point
  - Various methods for automatic tangent calculation exist
- Just an alternative representation -- cubic Hermites are readily convertible to cubic Béziers
  - cubic Bézier curve goes through only two; two are control points
  - cubic Hermite curve goes through only two; two are tangents/velocities at those points

$$
Q(t) = (2t^3 − 3t^2 + 1) P_0 + (−2t^3 + 3t^2) P_1 + (t^3 − 2t^2 + t) P_0' + (t^3 − t^2 ) P_1'
$$

The [best tutorial on Bézier curves and Hermite splines][curve-tutorial] is by Squirrel Eiserloh [in GDC][essential-math-tutorials].  This equation is derived beautifully compactly in matrix form in [[2](#references)].  Relationship and conversion formulae between Hermite and Bézier curves are given in [[3](#references)].

The matrix form of cubic Hermite curves

$$
Q(t) = GMT(t) \\
Q(t) = \left[ \begin{matrix} P_1 & P_2 & T_1 & T_2 \end{matrix} \right]
       \left[ \begin{matrix} 1 & 0 & -3 & 2 \\ 0 & 0 & 3 & -2 \\ 0 & 1 & -2 & 1 \\ 0 & 0 & -1 & 1 \end{matrix} \right]
       \left[ \begin{matrix} 1 \\ t \\ t^2 \\ t^3 \end{matrix} \right] \\
$$

`(point on curve) = (control vector) (basis matrix) (parameter vector)`

## Basis Functions

Notice that each coefficient of $P_i$ is a function of $t$?  These are called basis/blending functions; they give weight of each point[^affine] at some $t$.  These curves/functions are (linearly) blended together with the geometric constraints (points $P_i$) to yield an actual curve[[2](#references)] -- like a reconstruction filter that gives a curve based on the points fed.  Basis functions exist for all type of curves including Bézier, B-Splines, etc.  Refer [[13](#references)] for a visual example of how two basis functions form a curve.

!!! Note: Convex Hull Property
    If all basis functions are positive, all curves of that kind are going to be within the convex hull formed by its constituent points e.g. Bézier curves, unlike Hermite.

!!! Tip: [smoothstep][]
    One of the basis functions of cubic Hermite, $3t^2 - 2t^3$, is popularly known as the `smoothstep` function; available in many graphics libraries including [GLSL][glsl-smoothstep].  It’s the [magic salt][smoothstep-jari] to replace plain $t$!

!!! Info: Pseudo Cubic Interpolation
    [A trick][fake-cubic][^pseudo-cubic] to avoid bicubic interpolation (costly; needing 16 look-ups) is to bilinearly interpolate with $t$ faded by `smoothstep`: because it's smoothly varying, it ameliorates the sharpness of linear interpolation.  Be mindful that [Hermite interpolation][hermiterp] is different from [cubic (Hermite spline) interpolation][cubic-interpolation]; latter is used in (bicubic) image scaling, `smoothstep` does the former.

<figure>
  <a href="https://www.shadertoy.com/embed/wslcR8">
    <img title="Smootherstep interpolation b/w peaks"
         alt="Smootherstep interpolation b/w peaks"
         src="https://www.shadertoy.com/media/shaders/wslcR8.jpg">
  </a>
  <figcaption>A mountain range formed by interpolating between peaks (random values at stops) using `smootherstep`</figcaption>
</figure>

[smoothstep]: https://en.wikipedia.org/wiki/Smoothstep
[glsl-smoothstep]: https://www.khronos.org/registry/OpenGL-Refpages/gl4/html/smoothstep.xhtml
[smoothstep-jari]: http://sol.gfxile.net/interpolation/index.html#c4
[fake-cubic]: https://iquilezles.org/www/articles/texture/texture.htm
[hermiterp]: https://en.wikipedia.org/wiki/Hermite_interpolation

## Hermite Spline

> "Curves are interpolation of points while splines are interpolation of curves. [[9](#references)]"

Multiple Hermite curves are joined together -- each pair sharing a point and a tangent -- forming a Hermite spline -- an interpolating curve going through all its points.  In general, splines have better local control while curves have global control [[3](#references)].

Cubic Hermite splines are the most popular.  Cubic Hermite splines are piecewise cubic i.e. each piece is a third-degree polynomial specified in Hermite form.

Different algorithms for calculating tangents at endpoints exist; these are discussed in [[1](#references)] and [Wikipedia][cubic-interpolation].  The most popular is Catmull-Rom.

### Catmull-Rom

A simple method to calculate tangents for Hermite splines automatically giving $C^1$ continuity.

$$
T_i = \frac12 (P_{i+1} - P_{i-1}) \qquad
T_0 = T_n = \vec 0
$$

[[7](#references)] alternatively suggests

$$
T_i = \frac13 (P_{i+1} - P_{i-1}) \qquad
T_0 = \frac23 (P_1 - P_0)
$$

For end points, instead of choosing a tangent, some choose two "phantom" points -- $P_{-1} = P_0 + t (P_0 - P_1)$ and $P_{n+1} = P_n + t (P_n - P_{n-1})$ -- to avoid special casing in code.

#### Bessel-Overhauser Spline

When control points in a Catmull-Rom spline aren’t uniformly placed, then it’s not always smooth; bad "overshoot" can occur when two close control points are next to widely separated control points.  This is addressed by Bessel-Overhauser’s tangent calculation.  Let $T_{i+\frac12}$ be the average velocity at which the interpolating spline is traversed from $p_i$ to $p_{i+1}$.

$$
T_{i+\frac12} = \frac{p_{i+1} - p_i}{t_{i+1} - t_i} \\
T_i = \frac{(t_{i+1} - t_i) \ T_{i-\frac12} + (t_i - t_{i-1}) \ T_{i+\frac12}}{t_{i+1} - t_{i-1}}
$$

Refer [[11](#references)] and [[12](#references)] for details.

### Cardinal

Generalization of Catmull-Rom; introduces the _tension_ parameter $t$; when $t = \frac12$ it becomes Catmull-Rom.

$$
T_i = t \ (P_{i+1} - P_{i-1})
$$

### Kochanek–Bartels

A generalization of Cardinal splines, additionally adds _continuity_ and _bias_.  Hence often called TCB splines.

$$
T_i = \frac{(1-t)(1+b)(1+c)}{2} \ (P_{i} - P_{i-1}) + \frac{(1-t)(1-b)(1-c)}{2} \ (P_{i+1} - P_i)
$$

[curve-tutorial]: http://www.essentialmath.com/GDC2012/GDC12_Eiserloh_Squirrel_Interpolation-and-Splines.ppt
[essential-math-tutorials]: http://www.essentialmath.com/tutorial.htm
[cubic-interpolation]: https://en.wikipedia.org/wiki/Cubic_Hermite_spline#Interpolating_a_data_set

# Trigonometric Interpolation

$$
C(t) = A \cos^2{t} + B \sin^2{t} \\
C(t) = A \cos^2{t} + B (1 - \cos^2{t})
$$

We see that the coefficients sum up to be 1 making it an affine combination [[10](#references)].

In computer graphics, these costly interpolations are not widely used as there are many effective alternatives.

However, when applied independently to multiple dimensions, both this and cosine interpolation revert to being linear interpolation; refer [[4](#references)] and [this][mathSE-cosine].

[mathSE-cosine]: https://math.stackexchange.com/q/3301219/51968

## Cosine Interpolation

cos θ’s output varies from 1 to -1 to 1 as its input ranges from [0, 2π]. We need something from [0, 1].  Limit the domain to [0, π] to get a codomain of [1, -1].  Subtract from 1 and divide by 2 to get a codomain of [0, 1].

$$
C(t) = P_1 + ((1 - cos(tπ)) * 0.5) (P_2 - P_1)
$$

# Aside: Interpolating what?

There’re two things when interpolating something.  Let’s say we’re interpolating position, as opposed to orientation or velocity, etc.  How the position varies and how the interpolated parameter varies is independent.  There’re two different curves that can be plotted for each.

For linear interpolation

$$
C = t \ A + (1 - t) \ B  \tag{1} \label{1}
$$

the output is a straight line from $A$ to $B$.  When varying $t$ from $[0, 1]$ at regular intervals, we get a straight line between the extremes.  Graphs for both will be straight lines (a) for $t$ it’d be from $[0, 1]$ in both $X$ and $Y$ that’s perfectly straight and has a slope of 1 (b) for position, $C$, it’d be from $A$ to $B$; here $t$ isn’t seen as it’s implicit.

When using easing functions, like ease-in, $t$ is first passed through _another_ function whose output is what is plugged into $\eqref{1}$.  For simple lerp, the easing is just an identity function $y = f(x) = x$.  Otherwise it’s non-trivial e.g. ease-in

$$
t' = t^3
$$

Now the graphs will look different!  For $t$, when varying the input $X$ from $[0, 1]$, the output $Y$ would be a cubic curve[^biasgain] from $[0, 1]$.  However, _position’s graph would still be a straight line_! So what changed?!  How the position changed over $t$’s interval of $[0, 1]$ would be different; the velocity with which the line from $A$ to $B$ was traced will be non-uniform.  When varying $t$ at regular intervals, for lerp, a proportional amount of changed in position ensued.  Now, since we interpolate _non-linearly_ equal amounts of change in $t$ will lead to disproportionate amount of change in position [[3](#references)].  This is because, we don’t feed $t$ directly into $\eqref{1}$ but $t’$, so $\eqref{1}$ should actually be re-written as

$$
C = t^3 \ A + (1 - t^3) \ B
$$

Though $t$ has powers greater than 1 we still get a line and not a cubic curve -- why?  This is because we’ve an affine combination[^convex] -- weighted average where coefficients sum up to 1 -- of two points.  Likewise in trigonometric interpolation

$$
C = A \sin^2{t} + B \cos^2{t}
$$

we’ve an affine combination of two points, and hence we’ve a linear interpolation of position, but a trigonometric interpolation of the parameter $t$.

To have anything other than a straight line, we need an affine combination of more than 2 points e.g. quadratic Bézier curves -- $(1 - t)^2 P_0 + 2t(1-t) P_1 + t^3 P_2$ -- it’s still an affine combination, but of three points.

# References

1. _Essential Mathematics for Games and Interactive Applications_, 2nd Edition; §10.2.1 Interpolation of Position - General Definitions, §10.2.3.3 Hermite Curves - Automatic Generation of Hermite Curves
2. _Mathematics for 3D Game Programming and Computer Graphics_, 3rd Edition; §11.1 Cubic Curves, 
3. _3D Math Primer_, 2nd Edition; §13.1.6 Velocities and Tangents, §13.4.3 Bézier Derivatives and Their Relationship to the Hermite Form, §13.5 Splines
4. Paul Bourke’s article on linear, cosine, cubic and cubic Hermite [Interpolation](http://paulbourke.net/miscellaneous/interpolation/)
5. _Fundamentals of Computer Graphics_, 3rd Edition by Peter Shirley _§11.1.3 Solid Noise_
6. [GPU Programming and Architecture, Homework #2](https://www.seas.upenn.edu/~cis565/homework/Homework2-10.pdf)
7. _Computer Graphics: Principles and Practice_, 3rd Edition; §22.4 Gluing Together Curves and the Catmull-Rom Spline
8. [Hermite Curve Interpolation][hermite], Nils Pipenbrinck
9. [A Primer on Bézier Curves][bezier-primer], Pomax
10. _Mathematics for Computer Graphics_, 4th Edition by John Vince; §8.3 Non-linear Interpolation
11. _Mathematical Tools in Computer Graphics with C# Implementations_; §4.2 Affine Invariance
12. _3-D Computer Graphics: A Mathematical Introduction with OpenGL_; §VII.6 Bézier Curves of General Degree, §VII.12 Rational Bézier Curves
13. [2D Spine Curves][cornell-2dspline]
14. [Curves and Splines][cmu-interpolation]

# See Also

1. [Cubic Hermite Interpolation][bluefox-hermite] with demo
2. [Catmull-Rom Spline][ken-catmull-rom] lecture notes by Ken Joy 
3. [Spline Curves][spline-curves] lecture notes
4. [Reconstructing a Sine curve][giassa] with linear and cubic interpolations
5. [Spline Interpolation][spline-codeproject] - History, Theory and Implementation
6. [Quadratic/Cubic versus Linear interpolations][quadratic-cubic-linear]
7. [Cubic interpolation][paul-interpolation] by Paul
8. _Curves and Surfaces for Computer Graphics_, David Salomon
9. [Bezier Curves lecture][bezier-lecture] by Dr. Jon Shiach
10. [Programmer’s Guide to Polynomials and Splines][polynomials-splines] by Oleksandr Kaleniuk
11. [Lagrange Polynomial as a gateway drug to basis splines][lagrange-polynomial] by Oleksandr Kaleniuk
12. [Curves and Surfaces][ciechanowski_curves_surfaces] by Bartosz Ciechanowski
13. [Drawing Bezier Curves][ciechanowski_bezier] by Bartosz Ciechanowski


[hermite]: https://www.cubic.org/docs/hermite.htm
[bezier-primer]: https://pomax.github.io/bezierinfo/#bsplines
[bluefox-hermite]: https://blog.demofox.org/2015/08/08/cubic-hermite-interpolation/
[spline-curves]: https://people.cs.clemson.edu/~dhouse/courses/405/notes/splines.pdf
[ken-catmull-rom]: http://graphics.cs.ucdavis.edu/~joy/ecs278/notes/Catmull-Rom-Spline.pdf
[giassa]: https://www.giassa.net/?page_id=274
[spline-codeproject]: https://www.codeproject.com/articles/747928/spline-interpolation-history-theory-and-implementa
[quadratic-cubic-linear]: http://abrobecker.free.fr/text/quad.htm
[cornell-2dspline]: https://www.cs.cornell.edu/courses/cs4620/2013fa/lectures/16spline-curves.pdf
[paul-interpolation]: https://www.paulinternet.nl/?page=bicubic
[cmu-interpolation]: http://www.cs.cmu.edu/afs/cs/academic/class/15462-s10/www/lec-slides/lec06.pdf
[bezier-lecture]: https://www.youtube.com/watch?v=2HvH9cmHbG4
[polynomials-splines]: https://wordsandbuttons.online/programmers_guide_to_polynomials_and_splines.html
[lagrange-polynomial]: https://wordsandbuttons.online/lagrange_polynomial_as_a_gateway_drug_to_basis_splines.html
[ciechanowski_curves_surfaces]: https://ciechanow.ski/curves-and-surfaces/
[ciechanowski_bezier]: https://ciechanow.ski/drawing-bezier-curves/

[^affine]: Weights of points should be ≥ 0 and add up to 1; this is a basic rule when adding points in affine space.

[^pseudo-cubic]: Commonly employed by shader authors when texture filtering.

[^biasgain]: Check [Bias and Gain Are Your Friend](https://blog.demofox.org/2012/09/24/bias-and-gain-are-your-friend/) for graph images.

[^convex]: Strictly speaking this is a _convex combination_ as the coefficients are always non-negative besides summing up to 1.  An affine combination of two points is a line, but their convex combination is a line segment.  Affine combination of three non-collinear points is a plane, while their convex combination would be a triangle.


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'medium' };</script>
