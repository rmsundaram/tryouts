#!/usr/bin/env lua

if (#arg < 3) then
  print("usage: white_noise WIDTH HEIGHT OUTFILE")
  os.exit()
end

local width_str = arg[1]
local height_str = arg[2]
local file_name = arg[3]

local width = tonumber(width_str)
local height = tonumber(height_str)
local out_file = io.open(file_name .. ".pgm", "w")
out_file:write("P2\n" .. width_str .. " " .. height_str .. "\n255\n")

for i = 1, height do
  for j = 1, width do
    -- codomain of math.random() is [0, 1); so round() for values to reach 255
    local r = math.random() * 255
    out_file:write(math.floor(r + 0.5), " ")
  end
  out_file:write("\n")
end
out_file:close()
