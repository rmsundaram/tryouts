<meta charset="utf-8">
        **Noise**
    *Smooth Randomness*

# Intuition

> "Noise is the random number generator of Computer Graphics."

Random Number Generators (RNGs) generate values with sharp variation, while in nature, local changes are gradual and global changes are large.  Two points on the surface of a real object usually look almost the same when they are fairly close to each other; but two points that are far apart can look very different.  Just RNGs would be unsuitable for introducing a slight variation to the visual appearance of two points that are spatially close.

Let’s say we want to generate a smoothly varying terrain in 2D; horizontal and vertical axes represent distance and height.  While incrementing X, to calculate Y, if we just do `kHeight * math.random()` -- basically random value in `[0, 1)` scaled by a constant height factor -- we’d simply end up with irregular spikes of ups and downs; very broken, too harsh and unnatural.

However, if the random function (like `drand48`) is evaluated only at some intervals/stops[^fn1] that are well-spaced out, we get some random heights to work on.  Now the gaps[^fn2] between the stops can be filled by interpolation since we know values at its bounds.  Many options exist here:

* Linear (flat line joining the peaks)
* Trigonometric (Cosine, etc.)
* Spline (for an S-shaped/sigmoid curve)

This is the basic idea behind _noise_ in Computer Graphics.  There’re variations built atop these.

# White Noise

RNGs produce _white noise_: an image made of independent random variables with uniform probability distribution over some interval.  White noise texture roughly means that all the pixels are random and unrelated to each other; there’s no correlation between neighbouring values.

See `./white_noise.lua` for a simple program that generates a 2D white noise image.

# Value Noise

ScratchAPixel.com example[[1](#references)]: create a 10 × 10 grid, set random colours at each vertex of the grid and bilinearly interpolate to scale and render a larger image.  This doesn’t look very good but is smooth!  So _applying blur to white noise tends to make it more natural-looking_.

> "**Noise** -- in Computer Graphics -- is a function that blurs random values generated on a lattice."
>    -- ScratchAPixel.com

## Periodicity

To create a smooth random pattern, we assign random values (from a RNG) at fixed positions on a grid -- **lattice** -- and blur these values using some interpolation function.  It’s important that these lattice points/stops are at integer positions.

Noise should be _periodic_ for seamless outputs i.e. tiling the output curve/texture should not lead to a visible seams/artefacts.  Here’s a `noise1` function with periodicity 5 (i.e. 5-stop lattice) generating seamless output even when the input is beyond `)0, 4(`:

``` python
import random

def lerp(x1, x2, t):
  return ((1 - t) * x1) + (t * x2)

class Value:
  def __init__(self, lattice_size):
    self.lattice = []
    for i in range(0, lattice_size):
      random.seed()  # defaults to current system time
      self.lattice.append(random.random())

  def noise1(self, x):
    n = len(self.lattice)
    # bring x within lattice
    #   -6 -5 -4 -3 -2 -1  0  1  2  3  4  5  6  7  8  9  10
    #    4  0  1  2  3  4  0  1  2  3  4  0  1  2  3  4  0
    x = x % n
    left = int(x)
    right = (left + 1) if (left + 1) < n else 0
    t = x - left
    return lerp(self.lattice[left], self.lattice[right], t)

def print_noise(arr):
  for x in arr:
    print("{}\t {}".format(x, v.noise1(x)))
  print()

v = Value(5)
# noise1 for these inputs will be same
print_noise([3.72, 8.72, -1.28])
print_noise([-0.36, 4.64, 9.64])
```

!!! Warning: Modulo Differences
    `-1 % 5 = 4` in Python, Lua, Elisp, etc. while in C it’d be `-1`.  For periodic noise functions we need the former, not the latter.  _ScratchAPixel.com_ uses a much simpler function (below) for C-family which also works with negatives and is faster!

``` c++
// Returns modulo for power of two divisors; see //C++/modulo.cpp for NPOT.
// Named after Donald Knuth who introduced this in TOCP.
uint32_t kmod(int32_t a, uint32_t b) {
  assert(is_power_of_2(b))  //           -10 = 0b1111_1111_1111_0110
  uint32_t mask = b - 1;    // 256 - 1 = 255 = 0b0000_0000_1111_1111
  return a & mask;          //           246 = 0b0000_0000_1111_0110
}
```

Having powers of 2 sized lattices makes the modulo function much simpler and faster.

Using the modulo-technique, we can cycle over the noise function appending copies of the original curve.  In aforementioned example, input [0, 5) produces the original curve, while input [5,10) produces a copy that gets suffixed to the original and [-5, 0) produces a copy that gets prefixed to the original.

> "The trick is to find a size for the period of the noise that is long enough to make it impossible to recognise the pattern as you zoom out but short enough that the random value array stays reasonably small (keeping the memory usage consumption low). And 256 or 512 seems to be good values for achieving that result."
>    -- [Value Noise and Procedural Patterns: Part 1][Value-Noise-SAP], ScratchAPixel.com

See `./value_noise.lua` for a basic 2D value noise generator using bilinear interpolation.

[Value-Noise-SAP]: https://www.scratchapixel.com/lessons/procedural-generation-virtual-worlds/procedural-patterns-noise-part-1/creating-simple-1D-noise

# Noise Manipulation

Two ways to scale the curve generated by `noise`

* Multiplying input `x`
  - Factor > 1 will increase periodicity (frequency), compressing the output curve
  - Factor < 1 will decrease periodicity, stretching the output
  - Used in controlling **frequency**
* Multiplying output `y`
  - Since output is usually [0, 1], one can scale/attenuate it to a required band e.g. mountains with lowest to highest peaks in [200, 500] can be achieved by doing `200 + (500 - 200) * noise1(x)`
  - Used to control **amplitude**
* Adding to output `y`
  - Addition in the above example is _offsetting_ in Y i.e. displacing the output to some location in Y
* Adding to output `x`
  - Adding a positive number to `x` left-shifts the curve
  - Adding a negative number to `x` right-shifts the curve
  - Used to animate output over time by increasing the offset at each frame

# Aside: Signal Processing

White noise is a signal having equal power in any band of a given bandwidth (power spectral density). e.g. with a white noise audio signal, the range of frequencies between 40 Hz and 60 Hz contains the same amount of sound power as the range between 400 Hz and 420 Hz, since both intervals are 20 Hz wide.

_Pink noise_[^fn3], in contrast to white noise, has equal power in the frequency range from 40 to 60 Hz as in the band from 4000 to 6000 Hz.  Since humans hear in such a proportional space, where a doubling of frequency (an octave) is perceived the same regardless of actual frequency (40–60 Hz is heard as the same interval and distance as 4000–6000 Hz), every octave contains the same amount of energy.  Value, Perlin and Simplex noises are forms of pink noise.

# Octaves, fBm and Turbulence

Amplitude
: Height of a wave.  For a noise wave, the difference between the minimum and maximum values a function could produce.

Wavelength
: Distance between two peaks.  For a noise wave, the distance between two points with pre-loaded random value.

Frequency
: 1 / wavelength

Octave
: $\log_2$ unit for ratio between frequencies; one octave corresponds to a doubling of frequency e.g. a noise function’s output with frequency doubled w.r.t previous output is the succeeding octave.

Fractional Brownian Motion (fBm)
: Technique of adding multiple iterations of noise increasing octaves and decreasing amplitude to get finer granularity and more detail.  This is also called _fractal noise_ [[6](#references)].

When successive layers of a fractal noise have an amplitude which is inversely proportional to their frequency, the term used to describe the result is _pink noise_.  The term _octave_ is also sometimes (mis-)used in place of the word _layer_.  layer is more generic than octave (also used in music).  An octave is a doubling or halving of a frequency.  If it is used in a program (or in literature) it should mean that each successive layer in the computation of a fractal sum is twice the frequency of the previous layer; if not using octave is incorrect, layer is appropriate. When frequency is doubled between layers and the amplitude of these layers is inversely proportional to their frequency, a special type of pink noise called _Brownian noise_ is obtained.  Refer [Noise Sample Patterns][sample-patterns].  The word _lacunarity_ is used to control the rate by which the frequency changes from layer to layer.

[sample-patterns]: https://www.scratchapixel.com/lessons/procedural-generation-virtual-worlds/procedural-patterns-noise-part-1/simple-pattern-examples

# To Do

Program to generate images of

1. value noise (possibly with different interpolation functions) - https://pdfs.semanticscholar.org/d7c9/f8969b2c1fef2cafad2f60811ace91c89037.pdf
2. perlin noise

Program to internalize simplex/Perlin noise application

1. Hand-drawn circle (https://lodev.org/cgtutor/randomnoise.html#Turbulence)
2. 2D terrain with spectral synthesis/turbulence
3. Sun’s corona

## Read

### Value

1. https://www.scratchapixel.com/lessons/procedural-generation-virtual-worlds/procedural-patterns-noise-part-1/introduction?url=procedural-generation-virtual-worlds/procedural-patterns-noise-part-1/introduction
2. http://devmag.org.za/2009/04/25/perlin-noise/

### Perlin

1. https://natureofcode.com/book/introduction/ and/or https://www.khanacademy.org/computing/computer-programming/programming-natural-simulations/programming-noise/a/perlin-noise
2. https://eev.ee/blog/2016/05/29/perlin-noise/
3. https://flafla2.github.io/2014/08/09/perlinnoise.html
4. https://thebookofshaders.com/11/
5. https://rtouti.github.io/graphics/perlin-noise-algorithm
6. http://eastfarthing.com/blog/2015-04-21-noise/

Ultimately understand https://www.clicktorelease.com/blog/vertex-displacement-noise-3d-webgl-glsl-three-js/

# Also See

[Diamond Square][] Algorithm
: Another kind of value noise generating algorithm used for procedural terrain generation.

[Diamond Square]: https://en.wikipedia.org/wiki/Diamond-square_algorithm

# References

1. [ScratchAPixel.com](https://www.scratchapixel.com/lessons/procedural-generation-virtual-worlds/procedural-patterns-noise-part-1/)
2. [Perlin Noise](https://eev.ee/blog/2016/05/29/perlin-noise/) by Eevee
3. [Noise in Real-time 3D Graphics](https://pdfs.semanticscholar.org/d7c9/f8969b2c1fef2cafad2f60811ace91c89037.pdf)
4. [A Survey of Procedural Noise Functions](https://hal.inria.fr/hal-00920177/document)
5. [Value Noise](https://web.archive.org/web/20160421115558/freespace.virgin.net/hugo.elias/models/m_perlin.htm) by Hugo Elias
6. [Fractal Brownian Motion](https://thebookofshaders.com/13/)

[^fn1]: Formally called _lattice_ a regular arrangement of points.  Each point is called a lattice point or control point.

[^fn2]: Sometimes called _lacunae_, as in music, where no note is played.

[^fn3]: a.k.a _fractal noise_.

<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none'};</script>
