// g++ -Wall -std=c++17 -pedantic -fsanitize=address -fsanitize=undefined perlin.cpp
// ./a.out 8 512 test

#include <glm/glm.hpp>
#include <glm/ext/scalar_integer.hpp>

#include <iostream>
#include <tuple>
#include <string>
#include <random>
#include <fstream>
#include <array>
#include <vector>
#include <cassert>

float smootherstep(float t) {
  return t * t * t * (t * (t * 6 - 15) + 10);
}

// 2D perlin noise generator
class Perlin {
public:
  Perlin(size_t lattice_size = DEFAULT_GRAD_COUNT,
         bool recompute_gradient = false)
    : lattice_size_1d(lattice_size)
    // TODO: move to 2 * lattice_size hashing solution
    , lattice(lattice_size * lattice_size, 0)
  {
    assert(glm::isPowerOfTwo(lattice_size)); // power of 2

    if (recompute_gradient)
      gradient = gen_random_unit_vec(lattice_size);

    // load lattice with randomly picked gradients
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, gradient.size() - 1);
    for (auto i = 0u; i < lattice.size(); ++i) {
      lattice[i] = dis(gen);
#ifdef TRACE
      std::cout << lattice[i] << '\t';
#endif
    }
  }

  glm::vec2 grad(size_t col, size_t row) const {
    return gradient[lattice[row * lattice_size_1d + col]];
  }

  float noise(glm::vec2 P) const {
    const glm::ivec2 P_int = glm::floor(P);
    const glm::vec2 T = P - glm::vec2(P_int);

    const unsigned mask = lattice_size_1d - 1;
    const auto left = P_int.x & mask;
    const auto right = (left + 1) & mask;

    const auto top = P_int.y & mask;
    const auto bot = (top + 1) & mask;

    auto const grad_lt = grad(left, top);
    auto const grad_rt = grad(right, top);
    auto const grad_lb = grad(left, bot);
    auto const grad_rb = grad(right, bot);

    auto const val_lt = glm::dot(T, grad_lt);
    auto const val_rt = glm::dot(T - glm::vec2(1.0f, 0), grad_rt);
    auto const val_lb = glm::dot(T - glm::vec2(0, 1.0f), grad_lb);
    auto const val_rb = glm::dot(T - glm::vec2(1.0f, 1.0f), grad_rb);

    auto const val_top = glm::mix(val_lt, val_rt, smootherstep(T.x));
    auto const val_bot = glm::mix(val_lb, val_rb, smootherstep(T.x));
    auto val = glm::mix(val_top, val_bot, smootherstep(T.y));
    val = (val * INV_INTERVAL) + 0.5f;
    assert((val >= 0.0) && (val <= 1.0));
    return val;
  }

  size_t lattice_size() const {
    return lattice_size_1d;
  }

private:

  // As per Ken [1], the main axis and long diagonal directions are avoided to
  // preclude the possibility of axis-aligned clumping. He also chooses elements
  // to only have 0s and 1s to improve preformance by avoiding multiplication.
  // This is possible in 3D and above e.g. (1, -1, 0),… but in 2D it’d mean
  // using cardinal axes. So we’ll just use unit vectors in random directions.
  //
  // Using unit vectors, with no dimension zeroed out, limits the output to the
  // interval ±0.5√n for n-D noise; refer comment [2] in [3]. Essentially the
  // maximum can occur when a point is in cell centre (0.5, 0.5), with all four
  // gradient vectors pointing exactly in the same direction as the vector from
  // integer point to this point e.g. In 2D, it’d lead to
  //
  //    Unit vector in 2D with maximum value in both dimensions occurs at 45°
  //        P = (1, 1), unit P = (1/√2, 1/√2) = 0.707106781185
  //
  //    X-top = lerp(((0.5, 0.5) ⋅ (0.707106781185, 0.707106781185)),
  //                 ((-0.5, 0.5) ⋅ (-0.707106781185, 0.707106781185)), 0.5)
  //    X-bot = lerp(((0.5, -0.5) ⋅ (0.707106781185, -0.707106781185)),
  //                 ((-0.5, -0.5) ⋅ (-0.707106781185, -0.707106781185)), 0.5)
  //    X-top = X-bot = 0.707106781185
  //
  //    v = lerp(X-top, X-bot, 0.5) = 0.707106781185
  //
  // Essentially ½(n * max) value of the gradient that occurs in all dimensions
  // collectively would be the limit of v. That’s why Ken’s 3D implementation
  // choice of (1, 1, 0), (1, 0, -1), (0, -1, 1),… leads to a limit of [-1, 1].
  //
  //     limit = 0.5 * 2 * 1 = 1
  //
  // [1]: https://mrl.nyu.edu/~perlin/paper445.pdf
  // [2]: https://disq.us/p/23djvtb
  // [3]: https://eev.ee/blog/2016/05/29/perlin-noise/

  std::vector<glm::vec2> gen_random_unit_vec(size_t count) {
    std::vector<glm::vec2> v;
    v.reserve(count);

    std::random_device rd;
    std::mt19937 gen(rd());
    float constexpr PI = 3.14159265359f;
    // make a random angle, excluding 0° or 360° (X-axis)
    std::uniform_real_distribution<float> dis(0.0f, 2 * PI);

    while (v.size() != count) {
      float const angle = dis(gen);
      // exclude cardinal axes to avoid axial clumping
      if ((angle != 0.0f) && (angle != 2 * PI) && // 0 or 360°
          (angle != PI * 0.5) &&                  // 90°
          (angle != PI) &&                        // 180°
          (angle != (1.5 * PI))) {                // 270°
        v.emplace_back(std::cos(angle), std::sin(angle));

#ifdef TRACE
        auto const deg = angle * (180 / PI);
        auto const quad = std::floor(angle / (PI * 0.5)) + 1;
        std::cout << deg << '\t' << quad << '\t'
                  << v.back().x << ", " << v.back().y << '\n';
#endif
      }
    }
    return v;
  }

  // Generated value will be in the interval ±½√2. To normalize it to [0, 1],
  // divide by the total interval length and offset by ½.
  // [-0.707, 0.707] ÷ (2 * 0.707) -> [-0.5, 0.5] + 0.5 -> [0, 1]
  static auto constexpr INTERVAL = 1.41421356237f;
  static auto constexpr INV_INTERVAL = 1.0f / INTERVAL;

  // Stefan Gustavson recommends 8 or 16 gradient vectors for 2D
  // http://www.itn.liu.se/~stegu/simplexnoise/simplexnoise.pdf
  static constexpr auto DEFAULT_GRAD_COUNT = 8u;

  const size_t lattice_size_1d;
  // pre-computed using gen_random_unit_vec; stored as static data
  std::vector<glm::vec2> gradient = {
    glm::vec2{-0.891282, 0.453449},
    glm::vec2{-0.355151, -0.934809},
    glm::vec2{0.657337, -0.753597},
    glm::vec2{0.582161, 0.813073},
    glm::vec2{-0.985663, -0.168725},
    glm::vec2{-0.594486, 0.804106},
    glm::vec2{0.993346, 0.11517},
    glm::vec2{0.658562, -0.752527}
  };
  std::vector<uint8_t> lattice;
};

auto parse_args(char** params, int /*count*/) {
  int const lattice_size = strtoul(params[0], nullptr, 10);
  int const img_size = strtoul(params[1], nullptr, 10);
  std::string const out_file = std::string(params[2]).append(".pgm");
  return std::make_tuple(lattice_size, img_size, out_file);
}

int main(int argc, char* argv[]) {
  if (argc < 4) {
    std::cout << "Usage: perlin LATTICE_SIZE IMAGE_SIZE OUTFILE\n";
    return 0;
  }
  auto const [lattice_size, img_size, outfile] = parse_args(++argv, argc);

  Perlin p;
  std::vector<float> img(img_size * img_size, 0.0f);
  // Unlike noise()’s usage as solid texture in shaders, when generating a
  // seamless texture image, we need make such lattice is properly “fit” to the
  // image dimensions to obtain a seamless texture. Take the example of a
  // 4 element lattice fit to a 16 element image:
  //
  //       ] |       |       |       |       [|
  //     -1] 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 [6
  //     3¾] 0 ¼ ½ ¾ 1       2       3    3¾ [0
  //
  // Basically we shouldn’t fit lattice’s 4 stops end-to-end to image’s 16 stops
  // i.e. not 16 ÷ (4 - 1) instead we do 16 ÷ 4; So though spans = 3 for 4 stops
  // we divide by stops for a seamless texture.  We need the scaling factor as
  // feeding the integer image coordinates into noise() as-is will only give 0s.
  // Scale = 8 lattice points / 512 stops = 1/64 = 0.015625
  auto const scale = p.lattice_size() / static_cast<float>(img_size);
  for (auto row = 0; row < img_size; ++row) {
    for (auto col = 0; col < img_size; ++col) {
      img[row * img_size + col] = p.noise(glm::vec2(col, row) * scale);
    }
  }

  std::ofstream of(outfile);
  of << "P2\n" << img_size << " " << img_size << "\n255\n";
  // Y-flip to match OpenGL (fragment shader) rendering
  for (auto row = (img_size - 1); row >= 0; --row) {
    for (auto col = 0; col < img_size; ++col) {
      of << std::round(255 * img[row * img_size + col]) << ' ';
    }
    of << '\n';
  }
}
