#!/usr/bin/env lua

-- returns the fractional part of x; 0 if x is an integer
function fract(x)
  return x % 1
end

function round(f)
  return math.floor(f + 0.5)
end

function smootherstep(t)
  return t * t * t * (t * (t * 6 - 15) + 10)
end

function lerp(A, B, t)
  return ((1 - t) * A) + (t * B)
end

function make_lattice(count)
  lattice = {}
  math.randomseed(os.time())
  for i = 1, (count * count) do
    table.insert(lattice, math.random())
  end
  return lattice
end

--[[

  e.g. 4 × 4 lattice (denoted by +); an image of size w × h
  Unknown value at o may be obtained by bilinearly interpolating between
  A(1, 0), B(1, 1), C(2, 1) and D(2, 0)

  Transform to go from image space to lattice space:

     o’ = oM where M = | 1/(w−1) |
                       | 1/(h−1) |

                    1           2           3
        0---->------A-------.---D-----------+
        |           |   α   . β |           |
        v           ........o....           |
        |           |   γ   . δ |           |
      1 +-----------B-------.---C-----------+
        |           |           |           |
        |           |           |           |   AREA
        |           |           |           |     α = x * y
      2 +-----------+-----------+-----------+     β = (1 − x) * y
        |           |           |           |     γ = x * (y − 1)
        |           |           |           |     δ = (1 − x)(1 − y)
        |           |           |           |
      3 +-----------+-----------+-----------+

  fract(o’.x) gives X interpolation factor and fract(o’.y) gives Y factor
  floor(o’.x) and ceil(o’.x) would give the left and right
  floor(o’.y) and ceil(o’.y) would give the top and bottom
  NOTE1: Y axis goes downwards, so meaning of floor and ceil might seem inverted
  NOTE2: area of rectangle _opposite_ to a point is its scaling factor

  V(o) = (1-x)(1-y)A + (1-x)yB + xyC + x(y-1)D

  See [Wikipedia][1] for details
  [1]: https://en.wikipedia.org/wiki/Bilinear_interpolation

]]--


function get_pixel(image, size, new_size, r, c)
  local span = new_size / (size - 1)
  local r_norm = r / span
  local c_norm = c / span

  -- calculate interpolation factor
  local y = fract(r_norm)
  local x = fract(c_norm)

  -- in case (r, c) lies on a stop instead of a span in either dimension or both
  -- ceil and floor should give the same value; above equation will still work
  local left = c_norm - x
  local right = math.ceil(c_norm)
  local top = r_norm - y
  local bottom = math.ceil(r_norm)

  -- fetch bounding lattice values
  local idxA = (top * size + left) + 1      -- Lua indices start with 1
  local idxB = (bottom * size + left) + 1
  local idxC = (bottom * size + right) + 1
  local idxD = (top * size + right) + 1
  local A = image[idxA]
  local B = image[idxB]
  local C = image[idxC]
  local D = image[idxD]

  return lerp(lerp(A, D, smootherstep(x)),
              lerp(B, C, smootherstep(x)), smootherstep(y))
  -- use this to do plain lerp without looping t through smootherstep
  -- return ((1-x) * (1-y) * A) + ((1-x) * y * B) + (x * y * C) + (x * (1-y) * D)
end

function scale(image, size, new_size)
  new_image = {}
  for r = 0, (new_size - 1) do
    for c = 0, (new_size - 1) do
      v = get_pixel(image, size, new_size, r, c)
      table.insert(new_image, v)
    end
  end
  return new_image
end


-- main
if (#arg < 3) then
  print("usage: value_noise LATTICE_SIZE IMAGE_SIZE OUTFILE")
  os.exit()
end

local lattice_size = tonumber(arg[1])
local size_str = arg[2]
local file_name = arg[3]
local img_size = tonumber(size_str)

local lattice = make_lattice(lattice_size)
local img = scale(lattice, lattice_size, img_size)

local out_file = io.open(file_name .. ".pgm", "w")
out_file:write("P2\n" .. size_str .. " " .. size_str .. "\n255\n")
for i = 0, (img_size - 1) do
  for j = 0, (img_size - 1) do
    local idx = (j * img_size + i) + 1     -- Lua indices start with 1
    local norm = img[idx]
    out_file:write(round(255 * norm), " ")
  end
  out_file:write("\n")
end
out_file:close()
