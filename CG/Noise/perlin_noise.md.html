<meta charset="utf-8">
    **(Improved) Perlin Noise**

* [Kenneth H. Perlin][ken] invented this [pseudo-random][PRNG], [gradient noise][][^fn1] function for procedural texture generation
* A texturing primitive used to create "natural" looking textures without memory or bandwidth requirements
* Received an academy award as it found its use in many movies
* [Noise is like salt][noise-machine]/seasoning; boring by itself but food without salt is boring!

[ken]: https://mrl.nyu.edu/~perlin/doc/oscar.html
[gradient noise]: https://en.wikipedia.org/wiki/Gradient_noise
[PRNG]: https://en.wikipedia.org/wiki/Pseudorandom_number_generator
[coherent noise]: http://libnoise.sourceforge.net/coherentnoise/index.html
[noise-machine]: https://web.archive.org/web/20151103105450/http://www.noisemachine.com/talk1/index.html

# Characteristics

* Seemingly random to human perception
* Smooth/gradient transitions; no sharp fall-offs
* It’s a type of [coherent noise][]
  - small change in input produces small change in output
  - large change produces random change
* Dimension-agnostic
  - Fills all of n-D space
* Applicable to a 3D model directly without the “mapping problem” -- like Mercator projection -- 2D textures have
  - Perlin noise is sometimes called _solid noise/texture_ for this reason
  - Essentially, carved out of a virtual solid material defined by the procedural texture
* Essentially a mapping: $ \Bbb{R}_n \mapsto \Bbb{R} $
  - Domain: (±∞, ±∞, …), Codomain: [-1, 1]
  - Codomain can be ±½√n if gradients are unit vectors with no 0 element; see [here][codomain]
* Computational complexity: $O(2^n)$ where $n$ = dimensions
* Deterministic/reproducible; **same input yields same output**
* Interesting effects combining multiple layers (fBm[^fn2]) produced with different scaling factors[^fn3]

[codomain]:https://eev.ee/blog/2016/05/29/perlin-noise/#comment-4557725903

# Improved Noise

## Generation Procedure

Explained in 2D but is the same on other dimensions too.

1. Find bounding/lattice points surrounding the input point ($X$)
  - For n-D, we’d have $2^n$ bounding points
  - Bounding points are generally at integer boundaries
2. From a [LUT][] load _gradient vectors_ ($G_i$) of bounding points ($P_i$)
3. For each bounding point do
  1. Compute vector to input point, $D_i = X - P_i$
  2. Compute noise value, $n_i = D_i \cdot G_i$
4. Compute final output by bilinear interpolation with the parameter fed through [`smootherstep`][smootherstep][^fn4]
  1. Lerp along X-axis with first pair
  2. Lerp along X-axis with second pair
  3. Lerp along Y-axis with output pair
  4. For n-D $2^n-1$ lerps are needed

$$
s = smootherstep(t) = 6t^5 - 15t^4 + 10t^3 \\
N = A + s(B - A) \\
\boxed{ N = s A + (1 - s) B }
$$

!!! Note
    For a given input and bounding point pair, farther along the direction of bounding point’s gradient vector the input is, larger its noise value; larger dot product result.  If input _is_ a bounding point, its noise value will be 0.

!!! Warning
    Though some resources dub this as _cubic interpolation_, it isn’t! [^fn5]  In [Ken’s own words][noise-machine-algo], it’s a “weighted sum using easing curves” -- just lerp “using S-shaped cross-fade curve to weight the interpolant in each dimension”.  [ScratchAPixel.com calls][remap] this _remapping_, since `t` is remapped by some [sigmoid function][] before passing it to `lerp`.

[LUT]: https://en.wikipedia.org/wiki/Lookup_table
[noise-machine-algo]: https://web.archive.org/web/20151103105506/http://www.noisemachine.com/talk1/15.html
[remap]: https://www.scratchapixel.com/lessons/procedural-generation-virtual-worlds/procedural-patterns-noise-part-1/creating-simple-1D-noise
[sigmoid function]: https://en.wikipedia.org/wiki/Sigmoid_function

## Implementation

- Size of a full-sized look-up table will be expensive e.g. a 256 × 256 table, with 2D gradient vectors, is 0.5 MiB, if using [float32][].
- Common to have a 1D gradient vector table and a 2D permutation table.  The second table (2 × 256 elements) indexes into the first (256 elements).

~~~~~ c++ linenumbers
// GENERATION
auto const kTableSize = 256u;
auto const kTableSizeMask = kTableSize - 1u;

float grad[kTableSize] = { 0.0 };
uint8_t permutation[2 * kTableSize] = {};

for (auto i = 0u; i < kTableSize; ++i) {
  grad[i] = randFloat();
  permutation[i] = i;
}

for (auto i = 0u; i < kTableSize; ++i) {
  auto j = randUint() & kTableSizeMask;
  std::swap(permutation[i], permutation[j]);
  permutation[i + kTableSize] = permutation[i];
}

// LOOKUP
float noise(float x, float y) {
  // find int stops

  const float c00 = grad[permutation[permutation[x0] + y0]];
  const float c10 = grad[permutation[permutation[x1] + y0]];
  const float c01 = grad[permutation[permutation[x0] + y1]];
  const float c11 = grad[permutation[permutation[x1] + y1]];
}
~~~~~

!!! Warning
    Though line 16 makes it seem like the first and second half of `permutation` would be identical, they aren’t!  A future iteration might swap `permutation[i]` with some other `permutation[j]`.

We do this to map an n-D value to 1D.  For example, in 2D, `permutation[permutation[x] + y]` first looks up `x` in `permutation`, adds `y` to the result and looks up the result again in `permutation` -- maximum of this expression can be `2 × kTableSize`, hence the double-sized `permutation` -- and its result is the final index in `grad`.  All this since `grad` is a 1D vector and we save memory by not holding 512 × 4-byte floats.

!!! Tip: Hash Map/Function
    This technique maps n-D values (e.g. points) to unique 1D values (e.g. linear memory locations).  [Space-filling curve][]s[^fn6] do the same but with locality i.e. ‘walk’ing linearly along the curve, close coordinates have close outputs.  Z-order/[Morton curve][] and Hilbert curve are [popular in computer graphics][space-curves-CG].

It’s the `permutation` that gets shuffled every time and not the actual gradient array, `grad`.

[float32]: https://en.wikipedia.org/wiki/Single-precision_floating-point_format
[space-filling curve]: https://en.wikipedia.org/wiki/Space-filling_curve
[Morton curve]: https://www.forceflow.be/2013/10/07/morton-encodingdecoding-through-bit-interleaving-implementations/
[libmorton]: https://github.com/Forceflow/libmorton
[Bit Twiddling Hacks]: https://graphics.stanford.edu/~seander/bithacks.html#InterleaveTableObvious
[fxtbook]: https://www.jjj.de/fxt/#fxt
[space-curves-CG]: https://computergraphics.stackexchange.com/q/1479

## Gradient Vector Choices

- Gradient vectors are random, pre-computed unit vectors[^fn7]
- Gradient vector associated to a given bounding point should always be same (reproducibility)
- Beyond 2D, vectors might be generated with a Monte Carlo approach: random Cartesian coordinates are chosen in a unit cube; points falling outside unit sphere are discard while the rest are normalized
- Classic `noise3` had 256 random vectors and a permutation array with 512 entries with scrambled [0, 255]
- Improved `noise3` doesn’t have random vectors!  It only has 12 vectors defined by directions from cube centre to its edges: `{ (1, 1, 0), (-1, 1, 0), (1, -1, 0), (-1, -1, 0), (1, 0, 1), (-1, 0, 1), … }`
  + Idea is to avoid coordinate axes and long diagonals (e.g. `(1, 1, 1)`) to prevent axis-aligned clumping
  + Gradients from this set is chosen by `P modulo 16` (4 duplicates added to that set to avoid the slower `% 12`)

# Usage

`noise` is implemented agnostic of target (image) size i.e. say one needs to generate a 512 × 512 or 8 × 8 image, both can use the same `noise` function.  This is achieved with the _scale_ factor a.k.a _frequency_ parameter; how much you sample from `noise`’s lattice.

``` rust
//
//   +---+---+          +---+---+---+---+
//   |   |   |          |   |   |   |   |
//   +---1---+          +---1---+---2---+
//   |   |   |          |   |   |   |   |
//   +---+---+          +---+---+---+---+
//                      |   |   |   |   |
//                      +---3---+---4---+
//                      |   |   |   |   |
//                      +---+---+---+---+
//
// Same 2 × 2 lattice is used to fill a 2 × 2 and 4 × 4 image

let f1 = 1.0;
let n1 = noise(x1 * f1);

let f2 = 2.0;
let n2 = noise(x2 * f2);
```

# Turbulence

Octaves and spectral synthesis

    noise(p) + ½ noise(2p) + ¼ noise(4p)

- [An Image Synthesizer](https://ohiostate.pressbooks.pub/app/uploads/sites/45/2017/09/perlin-synthesizer.pdf) (on Pixel Stream Editor; _Classic Noise_) - Ken Perlin
- Fundamentals of Computer Graphics, 4th Edition, §11.5.3 - Peter Shirley
- [Texture Generation using Random Noise](https://lodev.org/cgtutor/randomnoise.html#Turbulence)
- [Spectral Synthesis](https://web.cs.wpi.edu/~emmanuel/courses/cs563/S07/talks/dmitriy_janaliyev_noise_wk9_p2.pdf) used with Perlin Noise is fractal Brownian motion(fBm)
- http://web.cse.ohio-state.edu/~wang.3602/courses/cse5542-2013-spring/14-noise.pdf

# References

1. [Generating Perlin Noise][angelcode] by Angel Code
2. [Ken’s GDC Talk][noise-machine] by Ken Perlin
3. [Improved Perlin Noise][simplex-noise] - Reference Implementation by Ken Perlin
4. [Perlin Noise][] at Wikipedia
5. [Perlin noise][perlin-eevee] where Eevee explains Octaves in Perlin noise’s context
6. [Improving Noise][] by Ken Perlin
7. [Classic noise reference implementation][classic-refcode] by Ken Perlin

[angelcode]: http://www.angelcode.com/dev/perlin/
[simplex-noise]: https://mrl.nyu.edu/~perlin/noise/
[Perlin Noise]: https://en.wikipedia.org/wiki/Perlin_noise
[value noise]: https://en.wikipedia.org/wiki/Value_noise
[octaves]: https://en.wikipedia.org/wiki/Octave_(electronics)
[common logarithm]: https://en.wikipedia.org/wiki/Common_logarithm
[binary logarithm]: https://en.wikipedia.org/wiki/Binary_logarithm
[perlin-eevee]: https://eev.ee/blog/2016/05/29/perlin-noise/
[Improving Noise]: https://mrl.nyu.edu/~perlin/paper445.pdf
[classic-refcode]: https://mrl.nyu.edu/~perlin/doc/oscar.html#noise
[fBm]: https://en.wikipedia.org/wiki/Fractional_Brownian_motion
[smoothstep]: https://en.wikipedia.org/wiki/Smoothstep
[smootherstep]: http://sol.gfxile.net/interpolation/index.html#c4

[^fn1]: As opposed to [value noise][]; a commonly confused, different class of noise functions. Value noise has a random value at lattice points, while gradient noise has gradients (vectors).

[^fn2]: [fractional Brownian motion][fBm]

[^fn3]: A.K.A [octaves][]: a unit for measuring frequency ratios on a logarithmic scale, with one octave corresponding to a doubling of frequency e.g. the distance between frequences 20 Hz and 80 Hz is 2 octaves = $\log_2(\frac{80}{20})$. _decade_ is the other common logarithmic unit to measure frequency band but uses [common logarithm][] instead of [binary logarithm][].

[^fn4]: For _Classic Noise_, Ken used [smoothstep][]: $3t^2 - 2 t^3$ (GLSL and HLSL has built-ins).

[^fn5]: cubic (Hermite spline) interpolation is more complex and involves interpolation with 4 values on a cubic curve; one can’t do this with just two values.  [cubic Hermite spline interpolation](https://en.wikipedia.org/wiki/Cubic_Hermite_spline) is different from [Hermite interpolation](https://en.wikipedia.org/wiki/Hermite_interpolation).  Former is done in (bicubic) image scaling, latter is what `smoothstep` does.

[^fn6]: [Matters Computational][fxtbook] has a section on _Space-Filling Curves_.  [libmorton][] seems like a good implementation of Morton codes; [Bit Twiddling Hacks][] also has a section on generating them.

[^fn7]: Exception is 1D -- they’re random scalars in $[-1, 1]$


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'medium'};</script>
