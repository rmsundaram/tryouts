// field of view query; the vision volume here is a viewing cone or a pyramid extending from the AI bot;
// line of sight query is a different problem which involves visibility determination amidst obstacles
// usually former is done to prune (necessary) and the latter is done to confirm (sufficient).
// http://www.gamedev.net/topic/429216-ai-field-of-vision/
// Refer FoV.html workout for a 2D implementation of line of sight and field of view testing.

#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtc/constants.hpp>
#include <glm/gtx/mixed_product.hpp>
#include <glm/gtx/fast_trigonometry.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/epsilon.hpp>
#include <iostream>

// another way is to add all components i.e. l₁ norm = 0
inline bool is_zero(const glm::vec3 &v)
{
    return (glm::dot(v, v) <= glm::epsilon<float>());
}

struct Ray
{
    glm::vec3 org;
    glm::vec3 dir;
};

enum RESULT
{
    POINT,
    SKEW,
    LINEARLY_DEPENDANT,
    MISS
};

struct Plane
{
    // the w coordinate is the NEGATIVE signed distance
    // from the origin along the normal
    glm::vec4 normal_signed_dist;
};

// 5.2.1 Intersection of a line and a plane - from Lengyel's skeleton book
bool ray_plane_intersect(const Ray &ray,
                         const Plane &plane,
                         float *t)
{
    const auto denom = glm::dot(glm::vec4(ray.dir, 0.0f),
                                plane.normal_signed_dist);
    // ray is parallel to plane
    if(glm::epsilonEqual(denom,
                         0.0f,
                         glm::epsilon<float>()))
        return false;
    const auto num = glm::dot(glm::vec4(ray.org, 1.0f),
                              plane.normal_signed_dist);
    const float dist = -num/denom;
    if(dist < 0.0f)    // plane behind ray case
        return false;
    *t = dist;
    return true;
}

constexpr auto vision_radius = 10.0f;
constexpr auto vision_radius_sq = vision_radius * vision_radius;
constexpr auto vision_FoV = 1.57079632679489661923132169163975144f; //glm::half_pi<float>();
constexpr auto vision_FoV_half = vision_FoV / 2.0f;
constexpr auto frustum_size_half = vision_radius * std::tan(vision_FoV_half);
// used for both frustum width and height; should a rectangular frustum be required, then two dimensions are required

// this tests the player's presence within the AI's vision cone sphere; the cone's apex at ai_pos with
// its radius being vision_radius and the vision_FoV giving the sweep angle (steradian);
// the advantage of this method is that it works both in ℝ² and ℝ³
constexpr auto cos_half_FoV = cos(vision_FoV_half);
bool player_visible_cone(const glm::vec3 &player_pos,
                         const glm::vec3 &ai_pos,
                         const glm::mat3 &ai_orientation)
{
    // it is assumed that in a right-handed world, the characters' orientation
    // is right-handed too with the forward being z, up being y and left being x
    const auto ai_dir = glm::row(ai_orientation, 2);
    const auto player_vec = player_pos - ai_pos;
    const auto ai_player_dot = glm::dot(ai_dir, player_vec);
    // trivial rejection when player is behind or at right angle
    if (ai_player_dot > 0.0f)
    {
        const auto player_dist_sq = glm::length2(player_vec);
        if (player_dist_sq <= vision_radius_sq)
        {
            // by here, player is within AI's forward hemisphere defined by radius vision_radius
            const auto cos_angle = ai_player_dot / glm::sqrt(player_dist_sq);
            // Both the methods below are based on the angle between the AI to player vector and the AI's viewing
            // direction. The first one needs sqrt and the second needs both sqrt and acos. However, there's a better
            // method that doesn't need either; it is based on the anti-commutative nature of the cross product; refer
            // FoV.html workout for the method and its implementation. It needs all the three vectors (two bounding
            // vectors and the tested vector) to be one the same plane, so employing this method in ℝ³ would mean
            // projecting the vectors onto a plane before using this method.
#ifndef CHECK_ACTUAL_ANGLE
            // this optimization can be seen in §8.4.3 Spotlights, Essential Math
            return (cos_angle >= cos_half_FoV);
#else
            return (glm::acos(cos_angle) <= vision_FoV_half);
#endif // CHECK_ACTUAL_ANGLE
        }
    }
    return false;
}

#if defined(VIEW_FRUSTUM) && defined(VIEW_CONE)
    #pragma message("Error: both VIEW_FRUSTUM and VIEW_CONE are defined!")
#endif

// player will be deemed visible if position is within the frustum that ends with a spherical rectangle;
// this is for ℝ³ for which we inverse bilerp while for ℝ² inverse lerp needs to be done; in that case the
// interpolant is a line while here it is a plane
bool player_visible_frustum(const glm::vec3 &player_pos,
                            const glm::vec3 &ai_pos,
                            const glm::mat3 &ai_orientation)
{
    const auto ai_dir = glm::row(ai_orientation, 2);
    const auto player_vec = player_pos - ai_pos;
    const auto ai_player_dot = glm::dot(ai_dir, player_vec);
    if (ai_player_dot > 0.0f)
    {
#ifndef VIEW_FRUSTUM
        const auto player_dist_sq = glm::length2(player_vec);
        if (player_dist_sq <= vision_radius_sq)
        {
#endif
            // plane is constructed with the AI's pos. as origin and
            // the normal would always be the basis k w.r.t the AI
            const Plane far_edge{glm::vec4{0.0f, 0.0f, 1.0f, -vision_radius}};
            float t;
            // since the player_vec is in world space, need to orient it w.r.t the AI space
            // ai_orientation would be AI's basis in terms of world i.e. Mai->world hence its
            // inverse would be Mworld->ai
            const auto Mworld2ai = glm::transpose(ai_orientation);
            const auto player_vec_wrt_ai = Mworld2ai * player_vec;
            assert(ray_plane_intersect({{}, player_vec_wrt_ai}, far_edge, &t));
            // since the parallel and behind cases are already rejected
            // by here the ray should always intersect the plane
            const auto point = t * player_vec_wrt_ai;
#ifndef VIEW_CONE
            // this point will be w.r.t the AI's coordinate system
            // hence z will always be vision_radius but x and y would be how far off the point is
            // from the origin (0, 0, 0) i.e. AI space equivalent of ai_pos (which is in world space)
            return ((std::abs(point.x) <= frustum_size_half) && (std::abs(point.y) <= frustum_size_half));
            // this will check if it's within the frustum and within the length limit
#else
            // if the vision volume needs to be a cone sphere like the other method, then do a radius comparison
            // which will change the posterior of the pyramid from a spherical rectangle into a spherical cap
            return (glm::length2(point) <= vision_radius_sq);
#endif
            // same calculation as above but in world space, however this uses sqrt due to calling glm::length
            /*const auto dist = glm::length(ai_pos + (vision_radius * ai_dir));
            const Plane E{glm::vec4{ai_dir, -dist}};
            float s;
            ray_plane_intersect({ai_pos, player_vec}, E, &s);
            const auto pw = ai_pos + (s * player_vec);
            const auto ai_side = glm::row(ai_orientation, 0);
            const auto ai_up = glm::row(ai_orientation, 1);
            return (std::abs(glm::dot(pw, ai_side)) <= frustum_size_half) &&
                   (std::abs(glm::dot(pw, ai_up)) <= frustum_size_half);*/
#ifndef VIEW_FRUSTUM
        }
#endif
    }
    return false;
}

int main()
{
    glm::vec3 ai_pos{0.0f, 0.0f, 0.0f};   // <1, 1, 1>
    glm::mat3 ai_orientation;
    glm::vec3 player_pos{2.5f, 3.4f, 3.6f};
    // see AI_Vision.jpg for the reason on the results differing between the methods
    std::cout << std::boolalpha << player_visible_cone(player_pos, ai_pos, ai_orientation) << std::endl;
    std::cout << std::boolalpha << player_visible_frustum(player_pos, ai_pos, ai_orientation) << std::endl;
}
