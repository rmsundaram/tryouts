#include "static_obj.hpp"

StaticObject StaticObject::make(std::vector<Vector2> points) {
  const Aabb bounds = Aabb::compute(points);
  return StaticObject{std::move(points), bounds};
}
