#include "common.hpp"
#include "dynamic_obj.hpp"

#include "raymath.h"

namespace {

Vector2 random_direction() {
  Prng& rng = Prng::get_instance();
  // 0, 2π works too but binary32 floats have better precision around zero
  const auto angle = rng.rand(-PI, PI);
  const Vector2 forward = {1.0f, 0.0f};
  return Vector2Rotate(forward, angle);
}

Aabb compute_bounds(Vector2 position, float radius) {
  const Vector2 offset = { radius, radius };
  return {
    Vector2Subtract(position, offset),  // min
    Vector2Add(position, offset)        // max
  };
}

}  // namespace

DynamicObject DynamicObject::make(Vector2 position, float radius) {
  Prng& rng = Prng::get_instance();
  return {
    position,
    random_direction(),
    compute_bounds(position, radius),
    radius,
    0.0f, /*timer*/
    rng.rand(5.0f, 10.0f), /*angle_periodicity*/
    random_colour()
  };
}

void DynamicObject::update(float elapsed) {
  timer += elapsed;
  if (timer >= angle_periodicity) {
    timer = 0.0f;
    dir = random_direction();
  }
  constexpr float OFFSET = 0.5f;
  pos = Vector2Add(pos, Vector2Scale(dir, OFFSET));
  // wrap around screen edges
  pos.x = Wrap(pos.x, 0, SCREEN_WIDTH);
  pos.y = Wrap(pos.y, 0, SCREEN_HEIGHT);
  bounds = compute_bounds(pos, radius);
}

void DynamicObject::draw() const {
  constexpr Color AABB_COLOUR = RED;
  constexpr Color HIGHLIGHTED_COLOUR = WHITE;

  const auto paint_colour = (!is_highlighted()) ? colour : HIGHLIGHTED_COLOUR;

  // Object’s radius encompasses circle and arrow; 2:3 ratio between circle
  // radius and actual radius, distance from centre to tip.
  const float circle_radius = radius * 0.6667f;
  DrawCircleV(pos, circle_radius, paint_colour);
  const auto dir_perp = Vector2Rotate(dir, (PI / 4.f));
  const auto v1 = Vector2Add(Vector2Scale(dir_perp, circle_radius), pos);
  const auto v2 = Vector2Add(Vector2Scale(dir, radius), pos);
  const auto dir_perp_opp = Vector2{dir_perp.y, -dir_perp.x};
  const auto v3 = Vector2Add(Vector2Scale(dir_perp_opp, circle_radius), pos);
  DrawTriangle(v1, v2, v3, paint_colour);
  DrawRectangleLinesEx(bounds, 1.0f, AABB_COLOUR);
}
