#pragma once

#include "config.h"

#ifdef USE_XXHASH
#  include "xxhash.h"
#endif  // USE_XXHASH

#include <cstdint>
#include <functional>
#include <utility>

// FNV-1a [1] is renowned for low collision rate with speed.  It only operates
// on octets though unlike modern hashes.  However, it exhibited the least
// collisions in this program compared to xxHash and prospector’s best [2].
// It might be for a reason as called out in SMhasher’s README [3]:
//
//   > When used in a hash table the instruction cache will usually beat the
//   > CPU and throughput measured here. In my tests the smallest FNV1A beats
//   > the fastest crc32_hw1 with Perl 5 hash tables. Even if those worse hash
//   > functions will lead to more collisions, the overall speed advantage and
//   > inline-ability beats the slightly worse quality. See e.g. A
//   > Seven-Dimensional Analysis of Hashing Methods and its Implications on
//   > Query Processing for a concise overview of the best hash table
//   > strategies, confirming that the simplest Mult hashing (bernstein, FNV*,
//   > x17, sdbm) always beat "better" hash functions (Tabulation, Murmur,
//   > Farm, ...) when used in a hash table.
//
// [1]: http://www.isthe.com/chongo/tech/comp/fnv/
// [2]: https://github.com/skeeto/hash-prospector/issues/19
// [3]: https://github.com/rurban/smhasher
// https://gist.github.com/ruby0x1/81308642d0325fd386237cfa3b44785c
template <typename T>
inline
uint64_t hash_fnv1a(const T& data) {
  uint64_t hash = 0xcbf29ce484222325;
  constexpr uint64_t prime = 0x100000001b3;
  const uint8_t* octet = reinterpret_cast<const uint8_t*>(&data);
  for(auto i = 0u; i < sizeof(data); ++i) {
    hash ^= octet[i];
    hash *= prime;
  }

  return hash;
}


#ifdef USE_XXHASH
// Wrapper around original C-style xxHash function
template <typename T>
inline
uint64_t hash_xxh3(const T& data) {
  return XXH3_64bits(reinterpret_cast<const void*>(&data), sizeof(data));
}
#endif  // USE_XXHASH


// Free-standing function to functor type converter; gets inlined even in low
// optimization settings [2].  Instantiate and use e.g. Functor<f>{}();
// [1]: https://stackoverflow.com/a/54121092/183120
// [2]: https://godbolt.org/z/WGh4b1cdT
template <auto T>
struct Functor
{
    template <typename... Args>
    auto operator()(Args&&... args) const {
      return std::invoke(T, std::forward<Args>(args)...);
    }
};
