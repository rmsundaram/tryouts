#pragma once

#include "flat_hash_cell.hpp"
#include "static_obj.hpp"
#include "hashes.hpp"

#include <vector>

#ifndef USE_XXHASH
using CellHasher = Functor<hash_fnv1a<HashGridCellId>>;
#else
using CellHasher = Functor<hash_xxh3<HashGridCellId>>;
#endif  // USE_XXHASH

struct HashGrid {

  HashGrid(float cell_size)
    : m_cell_size{cell_size}
    , m_collisions{0u}
  {
  }

  ~HashGrid();

  // Adds object to grid.  Returns true if addition was successful.
  bool add(StaticObjId id, const StaticObject& obj);

  // Rebuilds grid by clearing all cells and adding |objects| afresh.
  void assign(float cell_size, const std::vector<StaticObject>& objects);

  // Returns static object IDs whose AABB intersects |box|.
  std::vector<StaticObjId> query(Aabb box) const;

  // NOTE: a POT cell size is better as division becomes shift by count of
  // trailing zeros: https://gcc.godbolt.org/z/EnsEvbhWq
  FlatHashCell<64, CellHasher>  m_cells;
  float m_cell_size;
  // Though a FlatHashcell property, it’s actually a meta-property; adding this
  // member disturbs FlatHashcell’s tight object layout; keep at a higher level.
  uint32_t m_collisions;
};
