#pragma once

#include "aabb.hpp"

#include "raylib.h"

#include <vector>

struct StaticObject {
  std::vector<Vector2> points;
  Aabb                 bounds;

  static StaticObject make(std::vector<Vector2> points);
};
