#pragma once

#include "aabb.hpp"

#include "raylib.h"

struct DynamicObject {
  Vector2 pos;
  Vector2 dir;
  Aabb bounds;
  float radius;
  float timer;
  float angle_periodicity;
  Color colour;

  void update(float elapsed);
  void draw() const;
  // HACK: store highlighted state in alpha channel
  void highlight() { colour.a = 0; }
  void unhighlight() { colour.a = 255; }
  bool is_highlighted() const { return colour.a == 0; }

  static DynamicObject make(Vector2 position, float radius);
};
