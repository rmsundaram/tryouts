#pragma once

#include "raylib.h"

#include <cstdint>
#include <utility>
#include <array>
#include <span>
#include <type_traits>
#include <algorithm>
#include <random>
#include <chrono>
using std::chrono::duration;


// Returns ceil(x ÷ y).
// Refer RTCD, §7.1.5 Implicit Grids
inline constexpr
uint32_t div_ceil(uint32_t x, uint32_t y) {
  return (x + (y - 1)) / y;
}


// Returns v rounded to next power of 2.
// https://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
inline constexpr
uint32_t next_pot(uint32_t v) {
  v--;
  v |= v >> 1;
  v |= v >> 2;
  v |= v >> 4;
  v |= v >> 8;
  v |= v >> 16;
  v++;
  return v;
}


template <typename T, size_t N>
inline constexpr
std::array<std::remove_const_t<T>, N>
to_array(const std::span<T, N>& src) {
  std::array<std::remove_const_t<T>, N> dst;
  std::copy(src.begin(), src.end(), dst.begin());
  return dst;
}


constexpr int SCREEN_WIDTH = 1440;
constexpr int SCREEN_HEIGHT = 900;

constexpr float STATIC_CELL_SIZE_INITIAL = 256.f;
constexpr float STATIC_CELL_SIZE_MIN = 64.f;
constexpr float STATIC_CELL_SIZE_MAX = 512.f;

constexpr int DYNAMIC_OBJECT_COUNT = 100;
constexpr int DYNAMIC_OBJECT_MIN_RADIUS = 10;
constexpr int DYNAMIC_OBJECT_MAX_RADIUS = 20;
constexpr int DYNAMIC_GRID_CELL_SIZE =
  next_pot(DYNAMIC_OBJECT_MAX_RADIUS * 2 + 2);
constexpr int DYNAMIC_GRID_COLS = div_ceil(SCREEN_WIDTH,
                                           DYNAMIC_GRID_CELL_SIZE);
constexpr int DYNAMIC_GRID_ROWS = div_ceil(SCREEN_HEIGHT,
                                           DYNAMIC_GRID_CELL_SIZE);


struct Prng {
  static Prng& get_instance() { static Prng rng; return rng; }

  // Returns a random number ∈ [min, max].
  template <typename T>
  T rand(T min, T max) {
    // Multiple distributions with one generator is okay [1] (atleast upto 623
    // distributions [2]) if they needn’t be strictly uncorrelated.  Looks like the
    // distribution object is (logically) stateless; needn’t be a member [3].
    // [1]: https://stackoverflow.com/q/9870541/one-random-engine-multi-distributions
    // [2]: https://stackoverflow.com/q/48093621/generators-with-multiple-uncorrelated-distributions
    // [3]: https://stackoverflow.com/q/29580865/how-to-use-a-random-generator-as-a-class-member
    if constexpr (std::is_integral_v<T>)
      return std::uniform_int_distribution<T>{min, max}(engine);
    else
      return std::uniform_real_distribution<T>{min, max}(engine);
  }

private:
  Prng() : engine{} {
    const auto seed = std::random_device{}();
    engine.seed(seed);
    TraceLog(LOG_WARNING, "Random seed: %u", seed);
  }

  std::mt19937 engine;
};

inline
auto to_dyn_cell(Vector2 coordinate) {
  auto const x = static_cast<uint32_t>(std::max(coordinate.x, 0.f));
  auto const y = static_cast<uint32_t>(std::max(coordinate.y, 0.f));
  // clamp negative coordinates to 0.
  return std::make_pair(x / DYNAMIC_GRID_CELL_SIZE,
                        y / DYNAMIC_GRID_CELL_SIZE);
}


inline
Color random_colour() {
  const Color raylib_preset_colours[] = {
    YELLOW,
    GOLD,
    ORANGE,
    PINK,
    RED,
    MAROON,
    GREEN,
    LIME,
    DARKGREEN,
    SKYBLUE,
    BLUE,
    DARKBLUE,
    PURPLE,
    VIOLET,
    DARKPURPLE,
    BEIGE,
    BROWN,
    DARKBROWN,
    BLACK,
    MAGENTA
  };

  Prng& rng = Prng::get_instance();
  const size_t idx = rng.rand<size_t>(
    0u, std::extent_v<decltype(raylib_preset_colours)> - 1);
  return raylib_preset_colours[idx];
}


// Since HRC is almost always aliased to {steady,system}_clock it’s better to
// use the former directly; avoid latter as it isn’t monotonic i.e. it can go
// back and jump around too!
// https://en.cppreference.com/w/cpp/chrono/high_resolution_clock
// https://stackoverflow.com/q/1487695/183120
// https://stackoverflow.com/q/38252022/183120
// https://gamedev.stackexchange.com/a/131094/14025
typedef std::conditional<
  std::chrono::high_resolution_clock::is_steady,
  std::chrono::high_resolution_clock,
  std::chrono::steady_clock>::type Clock;
static_assert(Clock::is_steady,
               "Clock is not monotonically-increasing (steady).");
