#include "dynamic_scene.hpp"

#include "raylib.h"

#include <algorithm>
#include <cstdio>
#include <type_traits>
#include <cinttypes>

DynamicScene::DynamicScene() {
  Prng& rng = Prng::get_instance();
  // Create objects and register them with implicit bit grid
  for (auto i = 0u; i < DYNAMIC_OBJECT_COUNT; ++i) {
    const Vector2 loc { rng.rand(0.f, static_cast<float>(SCREEN_WIDTH)),
                        rng.rand(0.f, static_cast<float>(SCREEN_HEIGHT)) };
    const float orientation =
      rng.rand(static_cast<float>(DYNAMIC_OBJECT_MIN_RADIUS),
               static_cast<float>(DYNAMIC_OBJECT_MAX_RADIUS));
    objs[i] = DynamicObject::make(loc, orientation);
    mark_in_grid(objs[i].bounds, DynObjId(i));
  }

  TraceLog(LOG_WARNING,
           "Dynamic: sizeof(grid) for %u objects = %u",
           DYNAMIC_OBJECT_COUNT,
           sizeof(grid));
}

DynamicScene::~DynamicScene() {
  TraceLog(LOG_WARNING,
           "Average update duration for %u objects = %f",
           DYNAMIC_OBJECT_COUNT,
           static_cast<double>(avg_update_ms));
}

void DynamicScene::update() {
  if (IsKeyReleased(KEY_P))
    opts = static_cast<Options>(opts ^ Options::PAUSE);
  if (IsKeyReleased(KEY_G))
    opts = static_cast<Options>(opts ^ Options::HIDE_GRID);
  if (IsMouseButtonPressed(MouseButton::MOUSE_BUTTON_LEFT)) {
    auto const cursor = GetMousePosition();
    const auto [cell_x, cell_y] = to_dyn_cell(cursor);
    auto o = grid.objects(cell_x, cell_y);
    TraceLog(LOG_WARNING,
             "Cell (%u, %u) with %u objects",
             cell_x, cell_y,
             o.count());
    for (auto iter = o.cbegin(); iter != o.cend(); ++iter) {
      const auto idx = *iter;
      TraceLog(LOG_WARNING, "  Object: %u", idx);
      TraceLog(LOG_WARNING,
               "    Colour: (%u,%u,%u,%u)",
               objs[idx].colour.r,
               objs[idx].colour.g,
               objs[idx].colour.b,
               objs[idx].colour.a);
      TraceLog(LOG_WARNING,
               "    AABB: (%f, %f), (%f, %f)",
               static_cast<double>(objs[idx].bounds.min.x),
               static_cast<double>(objs[idx].bounds.min.y),
               static_cast<double>(objs[idx].bounds.max.x),
               static_cast<double>(objs[idx].bounds.max.y));
    }
  }

  if (opts & Options::PAUSE)
    return;

  const auto t1 = Clock::now();
  // clear previous update highlights
  for (auto& o : objs)
    o.unhighlight();
  // check for inter-object collisions
  for (auto i = 0u; i < objs.size(); ++i) {
    // skip if already marked as collided
    if (objs[i].is_highlighted())
      continue;

    // get all objects in all straddling cells
    const auto [cell_lt_x, cell_lt_y] = to_dyn_cell(objs[i].bounds.min);
    const auto [cell_rb_x, cell_rb_y] = to_dyn_cell(objs[i].bounds.max);
    auto objects = grid.objects(cell_lt_x,
                                cell_lt_y,
                                cell_rb_x - cell_lt_x + 1,
                                cell_rb_y - cell_lt_y + 1);
    objects.clear(static_cast<DynObjId>(i));
    for (auto iter = objects.cbegin(); iter != objects.cend(); ++iter) {
      const size_t j = *iter;
      if (objs[j].bounds.intersects(objs[i].bounds)) {
        objs[i].highlight();
        objs[j].highlight();
      }
    }
  }
  const auto t2 = Clock::now();
  const float elapsed_ms = duration<float, std::milli>(t2 - t1).count();
  // Moving average
  // https://stackoverflow.com/q/28820904/183120
  // NOTE: update_iter_count will wrap around to 0 after 65535 iterations
  // i.e. ~18 mins and lead to an integer divide by zero exception.
  avg_update_ms += (elapsed_ms - avg_update_ms) / ++update_iter_count;

  // Update objects, deregister from old cell, register at new cell
  const float elapsed = GetFrameTime();
  // TODO: separate into two updates loops; dynamic {objects, grid}
  for (auto i = 0u; i < objs.size(); ++i) {
    const auto old_bounds = objs[i].bounds;
    objs[i].update(elapsed);
    clear_in_grid(old_bounds, DynObjId(i));
    mark_in_grid(objs[i].bounds, DynObjId(i));
  }
}

void DynamicScene::draw() const {
  constexpr Color GRID_COLOUR {102, 191, 255, 64}; // quarter skyblue

  BeginDrawing();
  ClearBackground(DARKGRAY);

  if (!(opts & Options::HIDE_GRID)) {
    for (float i = DYNAMIC_GRID_CELL_SIZE;
         i < SCREEN_WIDTH;
         i += DYNAMIC_GRID_CELL_SIZE) {
      DrawLineV(Vector2{i, 0.f}, Vector2{i, SCREEN_HEIGHT}, GRID_COLOUR);
    }
    for (float i = DYNAMIC_GRID_CELL_SIZE;
         i < SCREEN_HEIGHT;
         i += DYNAMIC_GRID_CELL_SIZE) {
      DrawLineV(Vector2{0.f, i}, Vector2{SCREEN_WIDTH, i}, GRID_COLOUR);
    }
  }

  for (const auto &d : objs)
    d.draw();

  DrawFPS(10, 10);

  if (opts & Options::PAUSE) {
    auto colliding = 0u;
    for (const auto &d : objs)
      if (d.is_highlighted())
        ++colliding;
    char message[512];
    snprintf(message,
             std::extent_v<decltype(message)>,
             "%u / %" PRIu64 " colliding",
             colliding, objs.size());
    DrawText(message, 10, 40, 20, ORANGE);
  }

  EndDrawing();
}

void DynamicScene::mark_in_grid(Aabb bounds, DynObjId idx) {
  const auto [cell_lt_x, cell_lt_y] = to_dyn_cell(bounds.min);
  const auto [cell_rb_x, cell_rb_y] = to_dyn_cell(bounds.max);
  grid.set(cell_lt_x, cell_rb_x, cell_lt_y, cell_rb_y, idx);
}

void DynamicScene::clear_in_grid(Aabb bounds, DynObjId idx) {
  const auto [cell_lt_x, cell_lt_y] = to_dyn_cell(bounds.min);
  const auto [cell_rb_x, cell_rb_y] = to_dyn_cell(bounds.max);
  grid.clear(cell_lt_x, cell_rb_x, cell_lt_y, cell_rb_y, idx);
}
