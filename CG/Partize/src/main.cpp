#include "common.hpp"
#include "static_scene.hpp"
#include "dynamic_scene.hpp"

#include "raylib.h"

int main()
{
  SetConfigFlags(FLAG_MSAA_4X_HINT);
  SetTraceLogLevel(LOG_WARNING);
  InitWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Spatial Partitioning Analyser");
  SetTargetFPS(60);

  StaticScene s;
  DynamicScene d;
  bool dynamic = false;

  while (!WindowShouldClose())
  {
    if (IsKeyReleased(KEY_D))
      dynamic = !dynamic;
    if (dynamic) {
      d.update();
      d.draw();
    } else {
      s.update();
      s.draw();
    }
  }

  CloseWindow();
}
