#pragma once

#include "common.hpp"
#include "implicit_bit_grid.hpp"
#include "dynamic_obj.hpp"

#include <vector>
#include <array>
#include <cstdint>

struct DynamicScene {
  DynamicScene();
  ~DynamicScene();

  void update();
  void draw() const;

private:

  enum Options : uint8_t {
    NONE       = 0,
    PAUSE      = 1 << 0,
    HIDE_GRID  = 1 << 2,
  };

  void mark_in_grid(Aabb bounds, DynObjId idx);
  void clear_in_grid(Aabb bounds, DynObjId idx);

  std::array<DynamicObject, DYNAMIC_OBJECT_COUNT> objs;
  ImplicitBitGrid<DYNAMIC_GRID_COLS,
                  DYNAMIC_GRID_ROWS,
                  DYNAMIC_OBJECT_COUNT> grid = {{}, {}};
  float avg_update_ms = 0.0f;
  uint16_t update_iter_count = 0u;
  Options opts = Options::NONE;
};
