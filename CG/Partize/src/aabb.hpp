#pragma once

#include "raylib.h"

#include <vector>
#include <limits>

struct Aabb {
  Vector2 min;
  Vector2 max;

  float width() const {
    return max.x - min.x;
  }
  float height() const {
    return max.y - min.y;
  }

  // Returns true if AABB is valid; not degenerate.
  bool is_valid() const;

  // Returns true if AABB contains |pt|.
  bool is_containing(Vector2 pt) const;

  // Returns true if this AABB intersects (not touching) |other|.
  bool intersects(Aabb other) const;

  operator Rectangle() const {
    return {
      min.x,
      min.y,
      width(),
      height()
    };
  }

  static Aabb compute(const std::vector<Vector2>& points);
};

// std::is_pod is deprecated in C++20
template <typename T>
inline constexpr bool is_pod_v =
  std::is_standard_layout_v<T> && std::is_trivial_v<T>;

static_assert(is_pod_v<Aabb>);

// Degenerate AABB
inline constexpr Aabb AABB_INVALID = {
  { // min
    std::numeric_limits<float>::infinity(),
    std::numeric_limits<float>::infinity()
  },
  { // max
    -std::numeric_limits<float>::infinity(),
    -std::numeric_limits<float>::infinity()
  }
};
