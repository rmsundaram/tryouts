#pragma once

#include "common.hpp"
#include "static_obj.hpp"
#include "hash_grid.hpp"
#include "aabb.hpp"

#include "raylib.h"

#include <vector>

struct StaticScene {
  enum class State : uint8_t {
    Create,  // scene creation
    Query    // query rectangle
  };

  StaticScene();

  void update();
  void draw() const;

  State state = State::Create;
  std::vector<Vector2> click_pts;
  std::vector<StaticObject> objs;
  HashGrid grid { STATIC_CELL_SIZE_INITIAL };
  float cell_size = STATIC_CELL_SIZE_INITIAL;
  Aabb selection = AABB_INVALID;
  std::vector<StaticObjId> possible_hits;
  std::vector<StaticObjId> hits;
};
