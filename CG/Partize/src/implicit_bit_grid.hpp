#pragma once

#include "common.hpp"

#include <cstddef>
#include <cstdint>
#include <array>
#include <iterator>
#include <span>
#include <cassert>
#include <type_traits>
#include <limits>
#include <bit>

using DynObjId = uint16_t;

// This data structure is explained in RTCD, §7.1.5
// [Record] -> [Line] -> Grid
template <size_t COLS, size_t ROWS, size_t MAX_OBJECTS>
struct ImplicitBitGrid {

  // Bits to represent all objects is one record.  A cell is essentially bitwise
  // AND of a row and column record.
  struct Record {
    static constexpr
    size_t STRIDE = div_ceil(MAX_OBJECTS, 32);

    void clear(DynObjId id) {
      // Object IDs    Element
      //  [ 0, 31]        0
      //  [32, 63]        1
      //  [64, 95]        2
      //
      // Example with unsigned char.
      // 0b00000010'00000000
      // Object 9 = element 1, bit 1
      // Object 8 = element 1, bit 0.
      // Example with uint32_t
      // Object 39 = element 1, bit 7
      const size_t element = id / 32;
      const uint32_t mask = 1 << (id - (32 * element));
      m_data[element] &= ~mask;
    }

    void set(DynObjId id) {
      const size_t element = id / 32;
      const uint32_t mask = 1 << (id - (32 * element));
      m_data[element] |= mask;
    }

    bool is_object_in(DynObjId id) const {
      const size_t element = id / 32;
      const uint32_t mask = 1 << (id - (32 * element));
      return m_data[element] & mask;
    }

    size_t count() const {
      size_t n = 0;
      for (auto subrecord : m_data)
        n += std::popcount(subrecord);
      return n;
    }

    struct Iterator {
      using value_type = DynObjId;
      using difference_type = ptrdiff_t;

      Iterator(const Record* parent)
        : m_parent(parent)
        , m_idx(0u)
        , m_subrecord(parent->m_data[0]) {
        seek_to_next_subrecord();
      }
      Iterator() = default;

      DynObjId operator*() const {
        assert(m_subrecord);
        return static_cast<DynObjId>(
          std::countr_zero(m_subrecord) + (m_idx * 32));
      }
      Iterator& operator++() {
        clear_last_bit();
        seek_to_next_subrecord();
        return *this;
      }
      Iterator operator++(int) { Iterator i = *this; ++(*this); return i; }

      // non-member equality operators
      // https://stackoverflow.com/a/42764771/183120
      friend
      bool operator==(const Iterator& lhs, const Iterator& rhs) {
        return lhs.m_idx == rhs.m_idx;
      }
      friend
      bool operator!=(const Iterator& lhs, const Iterator& rhs) {
        return !(lhs == rhs);
      }

    private:
      void clear_last_bit() {
        const auto mask = 1 << std::countr_zero(m_subrecord);
        m_subrecord &= ~mask;
      }

      void seek_to_next_subrecord() {
        while (!m_subrecord && (++m_idx < STRIDE))
          m_subrecord = m_parent->m_data[m_idx];
      }

      const Record* m_parent = nullptr;
      size_t m_idx = STRIDE;
      uint32_t m_subrecord = 0u;
    };
    static_assert(std::forward_iterator<Iterator>);

    Iterator cbegin() const {
      return Iterator(this);
    }
    Iterator cend() const {
      return Iterator{};
    }

    // non-member (binary) operator methods
    friend
    void operator|=(Record& lhs, const Record& rhs) {
      for (auto i = 0u; i < Record::STRIDE; ++i)
        lhs.m_data[i] |= rhs.m_data[i];
    }

    friend
    Record operator&(const Record& lhs, const Record& rhs) {
      Record r;
      for (auto i = 0u; i < Record::STRIDE; ++i)
        r.m_data[i] = lhs.m_data[i] & rhs.m_data[i];
      return r;
    }

    static_assert(MAX_OBJECTS <= (1 + std::numeric_limits<DynObjId>::max()));

    std::array<uint32_t, STRIDE> m_data;
  };


  template <size_t ELEMENT_COUNT>
  struct Line {
    Record& at(size_t i) {
      assert(i < ELEMENT_COUNT);
      return m_records[i];
    }

    const Record& at(size_t i) const {
      assert(i < ELEMENT_COUNT);
      return m_records[i];
    }

    void clear(size_t i, DynObjId id) {
      at(i).clear(id);
    }

    void set(size_t i, DynObjId id) {
      at(i).set(id);
    }

    bool is_object_in(size_t i, DynObjId id) const {
      return at(i).is_object_in(id);
    }

    std::array<Record, ELEMENT_COUNT> m_records;
  };


  // Registers object |id| at cell (|col|, |row|).
  void set(size_t col, size_t row, DynObjId id) {
    m_cols.set(col, id);
    m_rows.set(row, id);
  }

  // Registers object |id| in cells bounds by a rectangle.
  void set(size_t left, size_t right, size_t top, size_t bottom, DynObjId id) {
    // Though 2D just update 1D as all cells of a single column/row is
    // represented by a single element in m_{cols,rows}.
    for (auto i = left; i <= right; ++i)
      m_cols.set(i, id);
    for (auto i = top; i <= bottom; ++i)
      m_rows.set(i, id);
  }

  // Clears object |id| from cell (|col|, |row|).
  void clear(size_t col, size_t row, DynObjId id) {
    m_cols.clear(col, id);
    m_rows.clear(row, id);
  }

  // Clears object |id| from cells bounds by a rectangle.
  void clear(size_t left,
             size_t right,
             size_t top,
             size_t bottom,
             DynObjId id) {
    // Though 2D just update 1D as all cells of a single column/row is
    // represented by a single element in m_{cols,rows}.
    for (auto i = left; i <= right; ++i)
      m_cols.clear(i, id);
    for (auto i = top; i <= bottom; ++i)
      m_rows.clear(i, id);
  }

  // Returns true if object with |id| ∈ cell (|i|, |j|).
  bool is_object_in(size_t col, size_t row, DynObjId id) const {
    return m_cols.is_object_in(col, id) && m_rows.is_object_in(row, id);
  }

  Record objects(size_t col,
                 size_t row,
                 size_t col_count = 1,
                 size_t row_count = 1) const {
    Record c = m_cols.at(col);
    Record r = m_rows.at(row);
    for (auto i = 1u; i < col_count; ++i)
      c |= m_cols.at(col + i);
    for (auto i = 1u; i < row_count; ++i)
      r |= m_rows.at(row + i);
    return c & r;
  }

  static_assert(std::is_standard_layout_v<Line<COLS>> &&
                std::is_trivial_v<Line<COLS>>);

  Line<COLS> m_cols;
  Line<ROWS> m_rows;
};
