#include "hash_grid.hpp"

#include <cmath>
#include <algorithm>

static
std::vector<HashGridCellId> overlapping_cells(const Aabb& bounds,
                                              float cell_size) {
  using std::floor;
  const auto x1 = static_cast<uint16_t>(floor(bounds.min.x / cell_size));
  const auto x2 = static_cast<uint16_t>(floor(bounds.max.x / cell_size));
  const auto y1 = static_cast<uint16_t>(floor(bounds.min.y / cell_size));
  const auto y2 = static_cast<uint16_t>(floor(bounds.max.y / cell_size));

  std::vector<HashGridCellId> cells;
  for (uint16_t i = x1; i <= x2; ++i)
    for (uint16_t j = y1; j <= y2; ++j)
      cells.push_back(HashGridCellId{i, j});
  return cells;
}

bool HashGrid::add(StaticObjId obj_id, const StaticObject& obj) {
  const std::vector<HashGridCellId> cells = overlapping_cells(obj.bounds,
                                                              m_cell_size);
  for (auto c : cells) {
    const auto [inserted, collided] = m_cells.insert(c, obj_id);
    if (!inserted)
      return false;
    m_collisions += collided;
  }
  return true;
}

static
void log_collisions(uint32_t collisions, float cell_size) {
  if (collisions) {
    TraceLog(LOG_WARNING, "Total collisions: %u @ cell size: %.2f",
             collisions,
             static_cast<double>(cell_size));
  }
}

void HashGrid::assign(float cell_size,
                      const std::vector<StaticObject>& objects) {
  log_collisions(m_collisions, m_cell_size);
  m_collisions = 0;
  m_cells.reset();
  m_cell_size = cell_size;
  for (auto i = 0u; i < objects.size(); ++i)
    add(static_cast<StaticObjId>(i), objects[i]);
}

// Sorts and removes duplicates from |v|.
static
size_t dedup(std::vector<StaticObjId>* v) {
  std::sort(v->begin(), v->end());
  const auto new_end = std::unique(v->begin(), v->end());
  const auto removed = std::distance(new_end, v->end());
  v->erase(new_end, v->end());
  return removed;
}

std::vector<StaticObjId> HashGrid::query(Aabb box) const {
  const std::vector<HashGridCellId> cells = overlapping_cells(box, m_cell_size);
  std::vector<StaticObjId> obj_list;
  obj_list.reserve(16);
  for (auto c : cells) {
    if (const auto opt_list = m_cells.search(c)) {
      const auto cell_obj_list = *opt_list;
      for (auto i = 0u; (cell_obj_list[i] != STATIC_OBJECT_INVALID) &&
             (i < cell_obj_list.size()); ++i)
        obj_list.push_back(cell_obj_list[i]);
    }
  }
  // Remove duplicates due to straddling objects and queried AABB.
  dedup(&obj_list);
  return obj_list;
}

HashGrid::~HashGrid() {
  log_collisions(m_collisions, m_cell_size);
}
