#include "aabb.hpp"

#include <cmath>

Aabb Aabb::compute(const std::vector<Vector2>& points) {
  Aabb bounds = AABB_INVALID;
  for (const auto& p: points) {
    bounds.min.x = std::min(bounds.min.x, p.x);
    bounds.min.y = std::min(bounds.min.y, p.y);
    bounds.max.x = std::max(bounds.max.x, p.x);
    bounds.max.y = std::max(bounds.max.y, p.y);
  }
  return bounds;
}

bool Aabb::is_valid() const {
  return std::isfinite(min.x) && std::isfinite(min.y) &&
         std::isfinite(max.x) && std::isfinite(max.y) &&
         (min.x < max.x) && (min.y < max.y);
}

bool Aabb::is_containing(Vector2 pt) const {
  return ((pt.x >= min.x) && (pt.x < max.x) &&
          (pt.y >= min.y) && (pt.y < max.y));
}

bool Aabb::intersects(Aabb other) const {
  // https://www.gamemath.com/book/geomtests.html#intersection_two_aabbs
  return !((min.x >= other.max.x) || (other.min.x >= max.x) ||
           (min.y >= other.max.y) || (other.min.y >= max.y));
}
