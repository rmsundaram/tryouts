#pragma once

#include "hashes.hpp"

#include "raylib.h"  // Only for TraceLog

#include <cstddef>
#include <cstdint>
#include <cassert>
#include <array>
#include <optional>
#include <limits>
#include <concepts>

// key
using HashGridCellId = std::array<uint16_t, 2>;
constexpr HashGridCellId HASH_GRID_CELL_ID_INVALID = {0xffff, 0xffff};

// value
using StaticObjId = uint8_t;
using StaticObjList = std::array<StaticObjId, 4>;
constexpr StaticObjId STATIC_OBJECT_INVALID = 0xff;
constexpr StaticObjList STATIC_OBJECT_LIST_EMPTY = {
  STATIC_OBJECT_INVALID,
  STATIC_OBJECT_INVALID,
  STATIC_OBJECT_INVALID,
  STATIC_OBJECT_INVALID
};


// Basic open-addressed hash table doing linear probing.  Key and Value types
// aren’t templatized on purpose to keep it simple.  It’s flat as items are not
// on separate lists but inline; bucket size is fixed at compile-time on array
// backing storage.  Not using unordered_map it’s separate chaining [3].
//
//   * Stores key and value separately as recommended by Mike Acton [1]
//   * Uses open addressing with linear probing [2]
//   * Uses % POT as recommended by C++ talk [2]; a prime bucket size is needed
//     for a poor hash function with poor entropy in lower bits [1].
//   * Not using Robin Hood hashing as we expect small bucket sizes where linear
//     search is better.  400000+ items is when Robin’s heroics show up [2].
//
// [1]: https://www.reedbeta.com/blog/data-oriented-hash-table/
// [2]: https://medium.com/applied/gist-better-than-unordered-map-1ad07b0a81b7
//      Original talk: https://youtu.be/M2fKMP47slQ
// [3]: https://stackoverflow.com/q/31112852/183120
template <size_t BUCKETS,
          std::default_initializable Hasher>
struct FlatHashCell {
  FlatHashCell() {
    reset();
  }


  // Clears key-value stores.
  void reset() {
    m_keys.fill(HASH_GRID_CELL_ID_INVALID);
    m_values.fill(STATIC_OBJECT_LIST_EMPTY);
  }


  // Returns true upon successful insertion; second flag denotes collision.
  std::pair<bool, bool>
  insert(HashGridCellId cell_id, StaticObjId obj_id) {
    uint64_t h = hash(cell_id) % BUCKETS;
    const auto final_h = (h != 0) ? (h - 1) : (BUCKETS - 1);
    bool collided = false;
    // Handle collision by linear probing this open addressing hash table.
    while ((m_keys[h] != HASH_GRID_CELL_ID_INVALID) && (m_keys[h] != cell_id)) {
      // only first iteration is collision; rest is just linear probing
      if (!collided) {
        collided = true;
        TraceLog(LOG_WARNING, "Collision: (%u, %u) × (%u, %u)",
                 cell_id[0], cell_id[1],
                 m_keys[h][0], m_keys[h][1]);
      }
      if (h == final_h)       // Searched all items.
        return {false, true}; // No vacancy in backing store.  Bail out!
      h = (h + 1) % BUCKETS;
    }

    // empty
    if (m_keys[h] == HASH_GRID_CELL_ID_INVALID) {
      m_keys[h] = cell_id;
      m_values[h][0] = obj_id;
    } else {  // |cell_id| is already in this slot
      // validate if at least first object slot is filled
      assert(m_values[h][0] != STATIC_OBJECT_INVALID);
      auto i = 1u;
      constexpr auto MAX_OBJECTS_PER_CELL = std::tuple_size<StaticObjList>{};
      while ((i < MAX_OBJECTS_PER_CELL) &&
             (m_values[h][i] != STATIC_OBJECT_INVALID))
        ++i;
      // Can’t insert more than 4 objects/cell by design of this hash table.
      if (i >= MAX_OBJECTS_PER_CELL) {
        // Help debug client code not honouring this limit.
        assert(false);
        return {false, collided};
      }
      m_values[h][i] = obj_id;
    }

    return {true, collided};
  }


  std::optional<StaticObjList> search(HashGridCellId cell_id) const {
    uint64_t h = hash(cell_id) % BUCKETS;
    const auto final_h = (h != 0) ? (h - 1) : (BUCKETS - 1);
    while (m_keys[h] != cell_id) {
      if ((m_keys[h] == HASH_GRID_CELL_ID_INVALID) || (final_h == h))
        return std::nullopt;
      h = (h + 1) % BUCKETS;
    }
    return m_values[h];
  }


  static_assert(BUCKETS && ((BUCKETS & (BUCKETS - 1)) == 0) &&
                (BUCKETS < std::numeric_limits<uint64_t>::max()),
                "Bucket size should be a non-zero power of two value.");


  // Key and value stores kept separate for better cache coherence.
  std::array<HashGridCellId, BUCKETS> m_keys;
  std::array<StaticObjList,  BUCKETS> m_values;

private:

  uint64_t hash(HashGridCellId id) const {
    return Hasher{}(id);
  }

};
