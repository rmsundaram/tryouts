#include "static_scene.hpp"

#include "raymath.h"

#include <algorithm>
#include <iterator>
#include <utility>
#include <cmath>

StaticScene::StaticScene() {
  click_pts.reserve(16);
  objs.reserve(16);
  hits.reserve(8);

  TraceLog(LOG_WARNING, "Static: sizeof(grid) = %u", sizeof(grid));
}

void StaticScene::update() {
  switch (state) {
    case State::Create:
      if (IsMouseButtonPressed(MouseButton::MOUSE_BUTTON_LEFT))
        click_pts.push_back(GetMousePosition());
      // we need a closed figure; atleast a triangle
      else if (IsMouseButtonPressed(MouseButton::MOUSE_BUTTON_RIGHT)) {
        objs.push_back(StaticObject::make(std::move(click_pts)));
        const auto id = static_cast<StaticObjId>(objs.size() - 1);
        // abort if addition fails
        if (!grid.add(id, objs.back()))
          TraceLog(LOG_FATAL, "Addition to static grid failed.");
      }
      // Switch to query state only if we’ve some objects to query and we’re
      // not in the middle of creating an object.
      if (IsKeyReleased(KEY_F) && !objs.empty() && click_pts.empty())
        state = State::Query;
      // Shrink/Grow grid size and reassign objects to grid afresh.
      if (IsKeyDown(KEY_LEFT_BRACKET))
        cell_size = std::max(cell_size - 1, STATIC_CELL_SIZE_MIN);
      else if (IsKeyDown(KEY_RIGHT_BRACKET))
        cell_size = std::min(cell_size + 1, STATIC_CELL_SIZE_MAX);
      if (IsKeyReleased(KEY_LEFT_BRACKET) || IsKeyReleased(KEY_RIGHT_BRACKET))
        grid.assign(cell_size, objs);
      break;
  case State::Query:
    if (IsKeyReleased(KEY_F))
      state = State::Create;
    // create a new query rectangle if we’re not already in one
    if (IsMouseButtonPressed(MouseButton::MOUSE_BUTTON_LEFT) &&
        click_pts.empty()) {
      click_pts.push_back(GetMousePosition());
    }
    else if (IsMouseButtonPressed(MouseButton::MOUSE_BUTTON_RIGHT) &&
             (click_pts.size() == 1)) {
      click_pts.push_back(GetMousePosition());
      selection = Aabb::compute(click_pts);
      click_pts.clear();
      possible_hits = grid.query(selection);
      hits.clear();
      std::copy_if(possible_hits.cbegin(),
                   possible_hits.cend(),
                   std::back_inserter(hits),
                   [&objs = std::as_const(objs),
                    &selection = std::as_const(selection)] (StaticObjId id) {
                     return selection.intersects(objs[id].bounds);
                   });
    }
    break;
  }
}

void StaticScene::draw() const {
  constexpr Color AABB_DEFAULT_COLOUR {230, 41, 55, 128};  // half red
  constexpr Color AABB_HIT_COLOUR = GOLD;
  constexpr Color AABB_MISS_COLOUR = ORANGE;
  constexpr Color GRID_COLOUR {200, 200, 200, 128};  // half lightgrey
  constexpr Color CELL_BORDER_COLOUR {0, 228, 48, 64};  // quarter green

  BeginDrawing();
  ClearBackground(DARKGRAY);

  // Draw grids
  for (auto i = cell_size; i < SCREEN_WIDTH; i += cell_size)
    DrawLineV(Vector2{i, 0.f}, Vector2{i, SCREEN_HEIGHT}, GRID_COLOUR);
  for (auto i = cell_size; i < SCREEN_HEIGHT; i += cell_size)
    DrawLineV(Vector2{0.f, i}, Vector2{SCREEN_WIDTH, i}, GRID_COLOUR);

  // Draw cursor position in cell space and highlight current cell
  Vector2 cursor_cell = Vector2Scale(GetMousePosition(), 1.0f / cell_size);
  cursor_cell.x = std::floor(cursor_cell.x);
  cursor_cell.y = std::floor(cursor_cell.y);
  const Rectangle cell_rect = {
    cursor_cell.x * cell_size + 2,
    cursor_cell.y * cell_size + 2,
    cell_size - 4,
    cell_size - 4
  };
  DrawRectangleLinesEx(cell_rect, 1.f, CELL_BORDER_COLOUR);

  // draw existing figures
  for (auto i = 0u; i < objs.size(); ++i) {
    DrawLineStrip(const_cast<Vector2*>(objs[i].points.data()),
                  static_cast<int>(objs[i].points.size()),
                  RAYWHITE);
    const bool maybe_hit =
      std::ranges::find(possible_hits,
                        StaticObjId(i)) != possible_hits.cend();
    const bool hit = maybe_hit &&
      std::ranges::find(hits, StaticObjId(i)) != hits.cend();
    const Color aabb_colour = hit ? AABB_HIT_COLOUR :
      (maybe_hit ? AABB_MISS_COLOUR : AABB_DEFAULT_COLOUR);
    DrawLineV(objs[i].points.back(), objs[i].points.front(), RAYWHITE);
    DrawRectangleLinesEx(objs[i].bounds, 1.0, aabb_colour);
  }

  // Draw to-be-solidifed figure based on mode.  Draw selection box only in
  // Query mode if finalized.
  if (state == State::Create) {
    if (!click_pts.empty()) {
      DrawLineStrip(const_cast<Vector2*>(click_pts.data()),
                    static_cast<int>(click_pts.size()),
                    GREEN);
      DrawLineV(click_pts.back(), GetMousePosition(), GREEN);
    }
  }
  if (state == State::Query) {
    // if user is drawing a query box ignore selection
    if (!click_pts.empty()) {
      auto temp_click_pts = click_pts;
      temp_click_pts.push_back(GetMousePosition());
      const Aabb cursor_bounds = Aabb::compute(temp_click_pts);
      DrawRectangleLinesEx(cursor_bounds, 1.0, GREEN);
    } else if (selection.is_valid())
      DrawRectangleLinesEx(selection, 1.0, GREEN);
  }

  // Draw help text like current mode, cell ID, etc.
  const char* const mode_name[] = {"Create", "Query"};
  DrawText(mode_name[static_cast<size_t>(state)], 5, 5, 7, RAYWHITE);
  char cell_size_str[20] = "";
  snprintf(cell_size_str,
           std::extent_v<decltype(cell_size_str)>,
           "Cell size: %.0f",
           static_cast<double>(cell_size));
  DrawText(cell_size_str, 5, SCREEN_HEIGHT - 20, 7, RAYWHITE);
  char cursor_cell_str[24] = "";
  snprintf(cursor_cell_str,
           std::extent_v<decltype(cursor_cell_str)>,
           "Cell %.0f, %.0f",
           static_cast<double>(std::floor(cursor_cell.x)),
           static_cast<double>(std::floor(cursor_cell.y)));
  DrawText(cursor_cell_str, 5, SCREEN_HEIGHT - 35, 7, CELL_BORDER_COLOUR);

  EndDrawing();
}
