# Spatial Partitioning Analyser

Prototype to analyse uniform grid spatial partitioning scheme; different grid implementations:

| Scene   | Grid Implementation |
|---------|---------------------|
| Static  | Hash table grid     |
| Dynamic | Implicit bit grid   |

## Static Scene

1. Stage 1: Create objects
  - Use `[` / `]` to shrink/grow grid
  - Left mouse down: start figure
  - Mouse move: draw straight line
  - Right mouse down: complete figure
2. Toggle between create/query modes: press `f` key
3. Stage 2: Query
  - Click, drag, release to draw a query rectangle
  - Writes number of tests performed to `STDOUT`
  - Step 2 loops i.e. another click clears previous query box

## Dynamic Scene

Pause simulation with `p`.  Toggle grid drawing with `g`.

# Uniform Grid

Make cell size slightly larger than the largest dynamic object

> cell size is generally adjusted to be large enough (but not much larger) to fit the largest object at any rotation.
>   — RTCD, § 7.1.1 Cell Size Issues

In both grids objects

- Objects can overlap/_straddle_ cells; not many
  + Multi-cell straddling objects will be part of all cells’ list

## Static Grid

- Tune cell size for optimal results
- Storing a linked list per cell will be costly
  + Using intrusive linked list is an option
    * Per cell overhead is just a `head` pointer to first object
    * Design for max cell overlap limit; we can store N `head`s
- Using static list per cell as
  + We’ve only static objects
  + We know maximum residents per cell

## Dynamic Grid

Measured on an Intel i7-7700HQ (8) @ 3.800GHz machine:

| Objects | Collisions | Release  |
|---------|------------|----------|
| 20      | 1%         | 0.002 ms |
| 100     | 25%        | 0.017 ms |
| 400     | 68%        | 0.138 ms |

Time spent seems proportional to collisions, not objects i.e. when collisions are low, so is time spent.  This implies `Aabb::intersects`, not implicit bit grid queries, are the bottleneck.

# Objects

## Static

Static objects, representing world polygon data, are better-represented by a coarser grid.  Additionally, we can do away with linked-list of items per cell since these are stationary.  An upper limit to the number of such objects within one cell decides the storage set aside per cell to store all object IDs.

Designing with following limits

* Maximum static objects = 255 i.e. IDs = `[0, 254]`; `255` denotes invalid object
* Maximum static objects per cell = 4

We need four `uint8_t` / 4 bytes per cell to denote static objects.

## Dynamic

Dynamic objects, representing characters, are suitable for a finer grid.

Once a LoS system is in place, to check if heroes are spotted by enemies, instead of checking every enemy, checking w.r.t every hero would be better.  For every hero, enumerate all enemies in vicinity and check if they’ve spotted -- this results in lesser LoS queries than the opposite.  It’s because heroes are lesser in number than enemies in general.

# TODO

Implement

- Hierarchical grid
- Bit-encoded Quadtree

# Reference

1. Real-Time Collision Detection, _Christer Ericson_
2. [Game Programming Patterns](https://gameprogrammingpatterns.com/spatial-partition.html), _Robert Nystrom_
3. [Hierarchical Hash Grid for Coarse Collision Detection](https://www10.cs.fau.de/publications/theses/2009/Schornbaum_SA_2009.pdf), _Florian Schornbaum_
4. [Readme Driven Development](https://tom.preston-werner.com/2010/08/23/readme-driven-development.html), _Tom Preston-Werner_
5. [Spatial hashing implementation for fast 2D collisions](https://conkerjo.wordpress.com/2009/06/13/spatial-hashing-implementation-for-fast-2d-collisions/)
6. [How to implement a spatial hash for a 2D game?](https://gamedev.stackexchange.com/a/56592/14025)
7. [Implementing Hash Tables in C](https://www.andreinc.net/2021/10/02/implementing-hash-tables-in-c-part-1#tombstones)
8. [Crafting Interpreters, §20. Hash Tables](https://craftinginterpreters.com/hash-tables.html)
9. [Designing a fast Hash Table](https://www.ilikebigbits.com/2016_08_28_hash_table.html)
10. [Which hashing algorithm is best for uniqueness and speed?](https://softwareengineering.stackexchange.com/q/49550/4154)

# See Also

1. [flatbush][], a JavaScript library, has a [packed Hilbert R-tree][r-tree] implementation to store 2d points, rectangles and query against bounding box for containment and k-nearest neighbours


[flatbush]: https://github.com/mourner/flatbush
[r-tree]: https://en.wikipedia.org/wiki/Hilbert_R-tree#Packed_Hilbert_R-trees
