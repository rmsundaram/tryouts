#include "common.hpp"
#include "main.hpp"

constexpr int window_width  = 800u, window_height = 600u;
constexpr float aspect_ratio =
    static_cast<float>(window_width) /window_height;
constexpr float near_z = 1.0;
constexpr float far_z = 100.0;
constexpr float eye[] = {3.0f,-4.0f, 5.0f};
constexpr float focus[] = {0.0f, 0.5f, 0.0f};
constexpr float up[] = {0.0f, 0.0f, 1.0f};

GLFWwindow* setup_context(int width, int height)
{
    // Without setting the version (1.0) or setting it < 3.2, requesting for core will fail
    // since context profiles only exist for OpenGL 3.2+; likewise forward compatibility only
    // exist from 3.0 onwards. 3.0 marked deprecated, 3.1 removed deprecated (except wide
    // lines), 3.2 reintroduced deprecated under compatibility profile.
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE,
                   GLFW_OPENGL_CORE_PROFILE);
    // Since core only has undeprecated features, making the context forward-compatible is moot.
    // It’s only needed for macOS.
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif  // __APPLE__

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

#ifndef NDEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif  // NDEBUG

    return glfwCreateWindow(width, height, "Hello Triangle", nullptr, nullptr);
}

#ifndef NDEBUG

void gl_debug_logger(GLenum source,
                     GLenum type,
                     GLuint id,
                     GLenum severity,
                     GLsizei /*length*/,
                     const char *msg,
                     const void *user_data)
{
    std::stringstream ss;
    ss << "0x"
       << std::hex << id << std::dec
       << ": "
       << msg <<
       " [source=";

    const char *sources[] = {
                                "API",
                                "WINDOW_SYSTEM",
                                "SHADER_COMPILER",
                                "THIRD_PARTY",
                                "APPLICATION",
                                "OTHER",
                                "UNDEFINED"
                            };
    size_t index = array::item_index(source,
                                     GL_DEBUG_SOURCE_API_ARB,
                                     GL_DEBUG_SOURCE_OTHER_ARB,
                                     array::max_index(sources));
    ss << sources[index];
    if (array::max_index(sources) == index)
    {
        ss << " (" << source << ")";
    }

    const char *types[] = {
                              "ERROR",
                              "DEPRECATED_BEHAVIOR",
                              "UNDEFINED_BEHAVIOR",
                              "PORTABILITY",
                              "PERFORMANCE",
                              "OTHER",
                              "UNDEFINED"
                          };
    index = array::item_index(type,
                              GL_DEBUG_TYPE_ERROR_ARB,
                              GL_DEBUG_TYPE_OTHER_ARB,
                              array::max_index(types));
    ss << " type=" << types[index];
    if (array::max_index(types) == index)
    {
        ss << " (" << type << ")";
    }

    const char *severities[] = {
                                   "HIGH",
                                   "MEDIUM",
                                   "LOW",
                                   "UNKNOWN"
                               };
    // as per the extension, HIGH is the base (smaller) value
    index = array::item_index(severity,
                              GL_DEBUG_SEVERITY_HIGH_ARB,
                              GL_DEBUG_SEVERITY_LOW_ARB,
                              array::max_index(severities));
    ss << " severity=" << severities[index];
    if (array::max_index(severities) == index)
    {
        ss << " (" << severity << ")";
    }
    ss << "]";

    const auto log = ss.str();
    FILE *out_file = reinterpret_cast<FILE*>(const_cast<void*>(user_data));
    fprintf(out_file, "%s\n", log.c_str());

    check(severity != GL_DEBUG_SEVERITY_HIGH_ARB, "High severity GL error logged");
}

void setup_debug(bool enable)
{
    if (!GLAD_GL_ARB_debug_output) return;

    glDebugMessageCallbackARB(enable ? gl_debug_logger : nullptr, stderr);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
    check(GL_NO_ERROR == glGetError(), "Unable to set synchronous debug output");
}

void print_framebuffer_properties() {
  // Skip glBindFrameBuffer; defaults to window-system-provided framebuffer.
  // https://www.khronos.org/opengl/wiki/Default_Framebuffer
  GLint ret;
  glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER,
                                        GL_FRONT_LEFT,
                                        GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE,
                                        &ret);
  assert(ret == GL_FRAMEBUFFER_DEFAULT);
  glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER,
                                        GL_FRONT_LEFT,
                                        GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING,
                                        &ret);
  const char* encoding = (ret == GL_LINEAR) ? "Linear" :
    (ret == GL_SRGB) ? "sRGB" : "Unknown";
  std::cerr << "Framebuffer Colour Encoding: 0x"
            << std::hex << ret << std::dec
            << " (" << encoding << ")\n";
  GLint size_red, size_green, size_blue, size_depth, size_alpha;
  glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER,
                                        GL_FRONT_LEFT,
                                        GL_FRAMEBUFFER_ATTACHMENT_RED_SIZE,
                                        &size_red);
  glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER,
                                        GL_FRONT_LEFT,
                                        GL_FRAMEBUFFER_ATTACHMENT_GREEN_SIZE,
                                        &size_green);
  glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER,
                                        GL_FRONT_LEFT,
                                        GL_FRAMEBUFFER_ATTACHMENT_BLUE_SIZE,
                                        &size_blue);
  glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER,
                                        GL_FRONT_LEFT,
                                        GL_FRAMEBUFFER_ATTACHMENT_ALPHA_SIZE,
                                        &size_alpha);
  glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER,
                                        GL_FRONT_LEFT,
                                        GL_FRAMEBUFFER_ATTACHMENT_DEPTH_SIZE,
                                        &size_depth);
  std::cerr << "Framebuffer Format: RGBA" << size_red
            << size_green
            << size_blue
            << size_alpha
            << ", Depth: " << size_depth << " bits\n";
}

#endif  // NDEBUG

void handle_key(GLFWwindow *window,
                int key,
                int /*scancode*/,
                int action,
                int /*mods*/)
{
    if (GLFW_PRESS == action)
    {
        if (GLFW_KEY_ESCAPE == key)
            glfwSetWindowShouldClose(window, GL_TRUE);
        else if (GLFW_KEY_A == key) {
            // Toggle scissor box by getting current value
            GLint dims[4];
            glGetIntegerv(GL_SCISSOR_BOX, dims);
            GLint width, height, origin_x, origin_y;
            if (dims[0] == 0)
            {
                height = window_height - 100;
                width = static_cast<GLint>(
                  static_cast<float>(height) * aspect_ratio);
                origin_x = (window_width - width) / 2;
                origin_y = (window_height - height) / 2;
            }
            else
            {
                origin_x = 0;
                origin_y = 0;
                width = window_width;
                height = window_height;
            }
            // Backup current clear colour, set it to black to clear all of the
            // surface first.  Disable scissor test as it affects clearing too
            // [1].  Clear, restore old clear colour and re-enable scissor test.
            float clear_colour[4];
            glGetFloatv(GL_COLOR_CLEAR_VALUE, clear_colour);
            glClearColor(0.f, 0.f, 0.f, 1.0f);
            glDisable(GL_SCISSOR_TEST);
            glClear(GL_COLOR_BUFFER_BIT);
            glEnable(GL_SCISSOR_TEST);
            glClearColor(clear_colour[0],
                         clear_colour[1],
                         clear_colour[2],
                         clear_colour[3]);
            // Set new scissor box and viewport; should set to same value [1].
            // [1]: https://gamedev.stackexchange.com/a/40713/14025
            glScissor(origin_x, origin_y, width, height);
            glViewport(origin_x, origin_y, width, height);
        }
    }
}

template <size_t N>
GLuint upload_data(const float (&ptr) [N])
{
    /*
     *  VBOs are “buffers” of video memory – just a bunch of bytes containing any kind of binary data you want.
     *  You can upload 3D points, colors, your music collection, poems to your loved ones – the VBO doesn’t care,
     *  because it just copies a chunk of memory without asking what the memory contains.
     */
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(GL_ARRAY_BUFFER, vbo);
    glBufferData(GL_ARRAY_BUFFER, sizeof(ptr), &ptr, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    return vbo;
}

GLuint setup_attributes(GLuint buffer)
{
    /*
     * The second step to rendering our triangle is to send the points from the VBO into the shaders. Remember
     * how the VBOs are just chunks of data, and have no idea what type of data they contain? Somehow you have to tell
     * OpenGL what type of data is in the buffer, and this is what VAOs are for. VAOs are the link between the VBOs and
     * the shader variables. VAOs describe what type of data is contained within a VBO, and which shader variables the
     * data should be sent to. glGetAttribLocation or layout location is used to find the shader variable location.
     *
     * If you have used VBOs without VAOs in older versions of OpenGL, then you might not agree with this description of
     * VAOs. You could argue that “vertex attributes” set by glVertexAttribPointer are the link between the VBO and that
     * shaders, not VAOs. It depends on whether you consider the vertex attributes to be “inside” the VAO (which I do),
     * or whether they are global state that is external to the VAO. Using the 3.2 core profile and ATI drivers, the VAO
     * is not optional – glEnableVertexAttribArray, glVertexAttribPointer and glDrawArrays all cause a INVALID_OPERATION
     * error if there is no VAO bound. This is what leads me to believe that the vertex attributes are inside the VAO,
     * and not global state. The 3.2 core profile spec says that VAOs are required, but it's told that only ATI drivers
     * throw errors if no VAO is bound. Here are some quotes from the OpenGL 3.2 core profile specification:
     *
     *            All state related to the definition of data used by the vertex processor is encapsulated
     *            in a vertex array object.
     *
     *            The currently bound vertex array object is used for all commands which modify vertex
     *            array state, such as VertexAttribPointer and EnableVertexAttribArray; all commands which
     *            draw from vertex arrays, such as DrawArrays and DrawElements; and all queries of vertex
     *            array state (see chapter 6).
     *
     * glVertexAttribPointer predates VAOs, so there was a time when vertex attributes were just global state. You could
     * see VAOs as just a way to efficiently change that global state. I prefer to think of it like this: if you don’t
     * create a VAO, then OpenGL provides a default global VAO. So when you use glVertexAttribPointer you are still
     * modifying the vertex attributes inside a VAO, it’s just that you’re modifying the default VAO instead of one you
     * created yourself.
     * http://www.tomdalling.com/blog/modern-opengl/01-getting-started-in-xcode-and-visual-cpp/#what_are_vbo_and_vao
     */
    glBindBuffer(GL_ARRAY_BUFFER, buffer);

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return vao;
}

void upload_transforms(GLuint program, float* view_3x4, float* proj_4x4)
{
    const auto V = glGetUniformLocation(program, "V");
    const auto P = glGetUniformLocation(program, "P");
    assert((V != -1) && (P != -1));

    glUniformMatrix3x4fv(V, /*count*/ 1, /*transpose*/ GL_FALSE, view_3x4);
    glUniformMatrix4fv(P, /*count*/ 1, /*transpose*/ GL_FALSE, proj_4x4);
}

void render(GLuint vao, GLuint program, float* view_3x4, float* proj_4x4)
{
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(program);
    glBindVertexArray(vao);

    upload_transforms(program, view_3x4, proj_4x4);

    glDrawArrays(GL_TRIANGLES, 0, 3);

    glBindVertexArray(0);
    glUseProgram(0);
}

void init_rendering()
{
    glEnable(GL_CULL_FACE);
    // GL_MULTISAMPLE is enabled by default as per specification; just setting
    // GLFW_SAMPLES suffices but just in case.
    if (GLAD_GL_ARB_multisample)
      glEnable(GL_MULTISAMPLE_ARB);
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
}

#ifdef USE_DXMATH
void setup_transforms(XMFLOAT3X4A* view, XMFLOAT4X4A* proj) {
    const XMMATRIX view_xform = XMMatrixLookAtRH(
      XMVECTOR{eye[0], eye[1], eye[2]},
      XMVECTOR{focus[0], focus[1], focus[2]},
      XMVECTOR{up[0], up[1], up[2]});
#ifdef PRINT_MATRICES
    XMFLOAT4X4A view_4x4;
    XMStoreFloat4x4A(&view_4x4, view_xform);
    std::cout << view_4x4;
#endif  // PRINT_MATRICES
    XMStoreFloat3x4A(view, view_xform);  // transposed copy

    const XMMATRIX proj_xform =
      XMMatrixPerspectiveFovRH(XM_PIDIV4, aspect_ratio, near_z, far_z);
    XMStoreFloat4x4A(proj, proj_xform);
}
#else
void setup_transforms(glm::mat3x4* view, glm::mat4* proj)
{
    const glm::mat4 view_4x4 = glm::lookAt(
      glm::vec3{eye[0], eye[1], eye[2]},
      glm::vec3{focus[0], focus[1], focus[2]},
      glm::vec3{up[0], up[1], up[2]});
#ifdef PRINT_MATRICES
    std::cout << view_4x4;
#endif  // PRINT_MATRICES
    *view = glm::transpose(view_4x4);
    *proj = glm::perspective(glm::quarter_pi<float>(), aspect_ratio, near_z, far_z);
}
#endif  // USE_DXMATH

int main(int /*argc*/, char ** /*argv*/)
{
#ifdef USE_DXMATH
    XMFLOAT4X4A proj_4x4;
    XMFLOAT3X4A view_3x4;
    setup_transforms(&view_3x4, &proj_4x4);
    const float* view = &view_3x4.m[0][0];
#else
    glm::mat4 proj_4x4;
    glm::mat3x4 view_3x4;
    setup_transforms(&view_3x4, &proj_4x4);
    const float* view = &view_3x4[0][0];
#endif  // USE_DXMATH

#ifdef PRINT_MATRICES
    std::cout << view;
#else
    UNUSED(view);
#endif  // PRINT_MATRICES

    glfwSetErrorCallback(window_error);
    const auto glfw = raii_wrap(check_return(glfwInit,
                                             GL_TRUE,
                                             "Failed to initialize GLFW"),
                                glfwTerminate);

    auto main_window = setup_context(window_width, window_height);
    check(main_window != nullptr, "Failed to create window");
    glfwMakeContextCurrent(main_window);
    // According to C++ FAQ [1], you cannot pass a member function to a C callback; passing a functor, lambda,
    // boost::bind, etc. is for a C++ callback implemented with templates; instead use the void* user_data allowed in
    // the callback; GLFW gives GLFWwindow->SetWindowUserPointer for the same.
    // [1]: http://isocpp.org/wiki/faq/pointers-to-members#memfnptr-vs-fnptr
    glfwSetKeyCallback(main_window, handle_key);

    check((gladLoadGL(glfwGetProcAddress) != 0), "Failed loading OpenGL symbols");

#ifndef NDEBUG
    setup_debug(true);
    print_framebuffer_properties();
#endif  // NDEBUG

    // X →, Y ↑, Z comes out of the screen (chirality: right)
    const float triangle[] = {
                                 0.0f, 1.0f, 0.0f,
                                -1.0f, 0.0f, 0.0f,
                                 1.0f, 0.0f, 0.0f,
                             };
    auto vbo = upload_data(triangle);
    auto vao = setup_attributes(vbo);
    auto program = ShaderUtil::setup_program("shaders/simple.vert",
                                             "shaders/per_obj.frag");

    init_rendering();
    while(!glfwWindowShouldClose(main_window))
    {
#ifdef USE_DXMATH
        render(vao, program, &view_3x4.m[0][0], &proj_4x4.m[0][0]);
#else
        render(vao, program, &view_3x4[0][0], &proj_4x4[0][0]);
#endif  // USE_DXMATH

        glfwSwapBuffers(main_window);
        glfwPollEvents();
    }

    glDeleteProgram(program);
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &vbo);
    // NOTE: Issue with Intel and ATI/AMD drivers.  wglDeleteContext called by glfwDestroyWindow throws a
    // GL_INVALID_VALUE, if this glDeleteProgram was called; commenting it out fixes the issue.
}

GLuint ShaderUtil::compile_shader(GLenum shader_type, const std::string &shader_file_path)
{
    const std::ifstream shader_file{shader_file_path};
    std::ostringstream shader_data;
    shader_data << shader_file.rdbuf();

    const std::string &str_shader_code = shader_data.str();
    const GLchar *shader_code = static_cast<const GLchar*>(str_shader_code.c_str());
    const GLint shader_data_len = static_cast<GLint>(str_shader_code.size());
    if(shader_data_len <= 0)
    {
        throw std::invalid_argument(shader_file_path);
    }

    const GLuint shader_id = glCreateShader(shader_type);
    if(0 == shader_id)
    {
        throw std::runtime_error("Failed to create new shader");
    }
    glShaderSource(shader_id, 1, &shader_code, &shader_data_len);
    glCompileShader(shader_id);

    GLint status;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &status);
    if(GL_FALSE == status)
    {
        GLint log_length;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &log_length);
        const auto err_log =
            std::make_unique<GLchar[]>(static_cast<size_t>(log_length));
        glGetShaderInfoLog(shader_id, log_length, nullptr, err_log.get());
        throw std::runtime_error(err_log.get());
    }

    return shader_id;
}

// although in the interface prog_id isn't const, making it const in the implementation
// prevents accidental overwrites
void ShaderUtil::link_program(const GLuint prog_id, const std::vector<GLuint> &shaders)
{
    for(auto shader_id : shaders)
    {
        glAttachShader(prog_id, shader_id);
    }

    glLinkProgram(prog_id);
    GLint status;
    glGetProgramiv(prog_id, GL_LINK_STATUS, &status);
    if(GL_FALSE == status)
    {
        GLint log_length;
        glGetProgramiv(prog_id, GL_INFO_LOG_LENGTH, &log_length);
        const auto err_log =
            std::make_unique<GLchar[]>(static_cast<size_t>(log_length));
        glGetProgramInfoLog(prog_id, log_length, nullptr, err_log.get());
        throw std::runtime_error(err_log.get());
    }

    for(GLuint shader_id : shaders)
    {
        glDetachShader(prog_id, shader_id);
    }
}

GLuint ShaderUtil::setup_program(const std::string &vert_shader_file, const std::string &frag_shader_file)
{
    const GLuint prog_id = glCreateProgram();
    if(0 == prog_id)
    {
        throw std::runtime_error("Failed to create new program");
    }

    const std::vector<GLuint> shader_ids
    {
        compile_shader(GL_VERTEX_SHADER, vert_shader_file),
        compile_shader(GL_FRAGMENT_SHADER, frag_shader_file)
    };
    link_program(prog_id, shader_ids);
    std::for_each(shader_ids.cbegin(), shader_ids.cend(), glDeleteShader);

    return prog_id;
}
