#ifndef __PRECOMP_H__
#define __PRECOMP_H__

#include "config.h"
#include <glad/gl.h>
#include <GLFW/glfw3.h>

#include <memory>
#include <vector>
#include <algorithm>
#include <string>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <utility>
#include <functional>

#include "raii_wrapper.hpp"

#define UNUSED(_x) (void)(_x)

#ifndef USE_DXMATH
#    include <glm/glm.hpp>
#    include <glm/gtc/matrix_transform.hpp>
#else
#    include <DirectXMath.h>
     using namespace DirectX;
#endif  // USE_DXMATH

#ifndef USE_DXMATH
inline
std::ostream& operator<<(std::ostream& os, const glm::mat4 &m)
{
    const float *e = &m[0][0];
    os << "mat4: [" << *e;
    for (auto i = 1; i < 16; i++)
        os << ", " << e[i];
    os << "]\n";

    return
    os << m[0][0] << '\t' << m[1][0] << '\t' << m[2][0] << '\t' << m[3][0] << '\n'
       << m[0][1] << '\t' << m[1][1] << '\t' << m[2][1] << '\t' << m[3][1] << '\n'
       << m[0][2] << '\t' << m[1][2] << '\t' << m[2][2] << '\t' << m[3][2] << '\n'
       << m[0][3] << '\t' << m[1][3] << '\t' << m[2][3] << '\t' << m[3][3] << '\n';
}
#else
inline
std::ostream& operator<<(std::ostream& os, const XMFLOAT4X4A &m)
{
    const float *e = &m.m[0][0];
    os << "mat4: [" << *e;
    for (auto i = 1; i < 16; i++)
        os << ", " << e[i];
    os << "]\n";

    return
    os << m.m[0][0] << '\t' << m.m[0][1] << '\t' << m.m[0][2] << '\t' << m.m[0][3] << '\n'
       << m.m[1][0] << '\t' << m.m[1][1] << '\t' << m.m[1][2] << '\t' << m.m[1][3] << '\n'
       << m.m[2][0] << '\t' << m.m[2][1] << '\t' << m.m[2][2] << '\t' << m.m[2][3] << '\n'
       << m.m[3][0] << '\t' << m.m[3][1] << '\t' << m.m[3][2] << '\t' << m.m[3][3] << '\n';
}
#endif  // USE_DXMATH

inline
std::ostream& operator<<(std::ostream& os, const float* f) {
#ifdef USE_DXMATH
  const char* const name = "mat3x4";
#else
  const char* const name = "mat4x3";
#endif  // USE_DXMATH
    os << name << ": [" << *f;
    for (auto i = 1; i < 12; i++)
        os << ", " << f[i];
    return os << "]\n";
}

#endif  // __PRECOMP_H__
