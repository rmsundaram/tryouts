#version 330

layout (location = 0) in vec3 position;

uniform mat4 P;
// Model and view transforms are guaranteed to be affine; drop the last row and
// save 25% memory.
// https://fgiesen.wordpress.com/2011/05/04/row-major-vs-column-major-and-gl-es/
uniform mat3x4 V;

void main() {
    // vec4 * mat3x4 => vec3; promote to vec4 before mat4 multiplication
    // https://stackoverflow.com/a/27163095/183120
    // All mat3x4 (affine) xforms are to be post-multiplied
    // All mat4 (projection) xforms are to be pre-multiplied as usual
    gl_Position = P * vec4(vec4(position, 1.0f) * V, 1.0);
}
