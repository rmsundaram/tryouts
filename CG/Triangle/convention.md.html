<meta charset="utf-8">
    **Faster 3D Math with Matrices**
         *Sundaram Ramaswamy*

# Overview

Use DirectXMath in CPU.  GLSL in GPU.  Read their default conventions below.  We’ll break
them intelligently to gain speed.

!!! Note: Notation
    We’ll use row-major notation in this document i.e. _3x4_ means 3 rows, 4 columns.  DirectXMath uses row-major storage and notation (unlike chirality which can be chosen e.g. `XMMatrixPerspective{L,R}H`, this is fixed).  GLM and GLSL uses column-major storage and notation.  [HLSL by default][hlsl-default] expects column-major storage for uniforms, but uses row-major notation.

3D math requires

1. affine (rotate, scale, shear and translate)
2. projection

transforms.  Both are representable as 4x4 matrices.  However, affine transforms always have the last row/column as $\begin{bmatrix}0 & 0 & 0 & 1 \end{bmatrix}$.

We could reap the 25% storage benefit by using a 12-, instead of 16-, element matrix.  This improves memory (storage and GPU upload) and speed.  However, computing this shorter matrix might incur more cycles if not careful.  Consider the following (row-major) matrix

$$
M = \begin{bmatrix}
1 & 0 & 0 & 0 \\
0 & 1 & 0 & 0 \\
0 & 0 & 1 & 0 \\
t_x & t_y & t_z & 1 \\
\end{bmatrix}
$$

A 4x4 matrix is a 4-array of 4-vector.  In memory, it’s goes $[1, 0, 0, \underline{0}, 0, 1, 0, \underline{0}, 0, 0, 1, \underline{0}, t_x, t_y, t_z, \underline{1}]$.  To arrive at a 12-element matrix, we’d have to drop the last element of every 4-vector then pack the whole thing.  This is the case for column-major matrices too.  This is because in row-major matrices we want to drop the last column and in column-major matrices we want to drop the last row :(

We don’t want to do this extra work.  We want to simply drop the last 4-vector (be it row/column-major) and get our 3x4 matrix.  To do this, we need our matrix transposed.  Consider the following (row-major) matrix _transposed_:

$$
M^T = \begin{bmatrix}
1 & 0 & 0 & t_x \\
0 & 1 & 0 & t_y \\
0 & 0 & 1 & t_z \\
0 & 0 & 0 & 1 \\
\end{bmatrix}
$$

After transpose, the storage is $[1, 0, 0, t_x, 0, 1, 0, t_y, 0, 0, 1, t_z, \underline{0, 0, 0, 1}]$.  This would now give us the luxury of simply dropping the last row to yield us a 3x4 matrix.  We also get the storage discount.

$$
M^T_{3 \times 4} = \begin{bmatrix}
1 & 0 & 0 & t_x \\
0 & 1 & 0 & t_y \\
0 & 0 & 1 & t_z \\
\end{bmatrix}
$$

Since the matrix is transposed, we’ve to reverse the normal conventions followed.  In C++ we use DirectXMath; the convention is to right-/post-multiply matrices in transform order.  However, since we use transposed matrices, we’ve to left-/pre-multiply.  In GLSL the convention is to left-/pre-multiply matrices in transform order.  Here too we flip the order to get the desired result.

**Vector-Matrix Product Benefit**: by not following the default convention with both DirectXMath and GLSL, we get an additional benefit.  Usually in row-convention, for $p' = pM$ operation, the matrix is accessed in transposed fashion.  However, if we do $Mp$ the access is just row-wise, no transposed access needed (cache-friendly).

!!! Note: Matrix-Matrix Product Memory Access
    For matrix-matrix product, one of the matrices will be accessed in the transposed fashion.  No tricks to avoid this.

[hlsl-default]: https://learn.microsoft.com/en-us/windows/win32/direct3dhlsl/dx-graphics-hlsl-per-component-math

# Observations

> "So long as you use column-major [^major] with post-multiplication [^order-wrt] and row-major with pre-multiplication, the data is fully compatible."
>     -- [Sean Middleditch][]

If we only use square matrices (`mat4` or `XMMATRIX`) and follow the default convention of the library/language, we’d end up with the same data.  No changes needed; both in C++ (CPU) and GLSL (GPU).

* DirectXMath is row-major both in storage and basis i.e. `ptr[0-3]` -> $\{ \hat{i}_x, \hat{i}_y, \hat{i}_z, 0 \}$
* GLM is column-major both in storage and basis i.e. `ptr[0-3]` -> $\{ \hat{i}_x, \hat{i}_y, \hat{i}_z, 0 \}$
* Results tally in 1D: column-major + pre-multiply[^order-wrt2] (or) row-major + post-multiply

!!! Note: Conventions
    DirectXMath uses row-vector convention: post-/right-multiplication of matrix (OR) pre-/left-multiplication of vector: $p' = pM$.  GLSL uses column-vector convention: pre-/left-multiplication of matrix (OR) post-/right-multiplication of vector: $p' = Mp$.

## Matrix Transposition

$$
\begin{aligned}
R &= ABCD \\
R^T &= (ABCD)^T \\
&= ((AB)(CD))^T \\
&= (CD)^T(AB)^T \\
R^T &= D^T C^T B^T A^T \\
\end{aligned}
$$

Transposing all original matrices and multiplying in reverse order gives transposed result.

## Nature of Matrix Multiplication

In general, `vec4` transformed by a `mat4` means the matrix is transpose-accessed in both row- and column-vector conventions e.g. $\begin{bmatrix}\hat{i}_x & \hat{j}_x & \hat{k}_x & 0\end{bmatrix} \cdot \vec{v}$ gives `result[0]`, $\begin{bmatrix}\hat{i}_y & \hat{j}_y & \hat{k}_y & 0\end{bmatrix} \cdot \vec{v}$ gives `result[1]`, ...

As per GLSL convention of matrix pre-multiplication: $p’ = Mp$; this is implemented in hardware by transpose-access.  However, transposing the equation

$$
p’^T = (Mp)^T \\
p’ = p^T M^T
$$

So if we’ve $M^T$, we can simply post-multiply it to the vector i.e. access every row in $M$ and just dot it with $p$; cache-friendly.  We get the same result but avoiding the transpose-access.

!!! Note: $p’ = p’^T$ in memory
    Though mathematically inequal, a vector and its transpose, as 1-d vector, are equal in memory.

# Convention Details

Use DirectXMath’s `FLOAT3X4` with the linear transform of 3x3 sub-matrix transposed i.e. `ptr[0-3]` -> $\{ \hat{i}_x, \hat{j}_x, \hat{k}_x, t_x \}$ unlike the usual $\{ \hat{i}_x, \hat{i}_y, \hat{i}_z, 0 \}$.

The idea works when the $t_x, t_y, t_z$ components are interleaved with the transposed basis vector components i.e. aren’t the last 4-vector in storage.  This can be achieved using DirectXMath’s `FLOAT3X4` (3 rows, 4 columns) or GLM’s `mat3x4` (3 columns, 4 rows):

$$
M_{DXM} = \begin{bmatrix}
1 & 0 & 0 & t_x \\
0 & 1 & 0 & t_y \\
0 & 0 & 1 & t_z \\
\end{bmatrix}

M_{GLM} = \begin{bmatrix}
1 & 0 & 0 \\
0 & 1 & 0 \\
0 & 0 & 1 \\
t_x & t_y & t_z \\
\end{bmatrix}
$$

## Situations

* Input: Untransposed `FLOAT4X4` (indices 12, 13, 14 has $t_x, t_y, t_z$)
  - Last column is $\begin{bmatrix}0 \\ 0 \\ 0 \\ 1 \end{bmatrix}$ ?
    + Yes (affine): Use transposing `XMStoreFloat3x4A`, store in `FLOAT3X4A`
    + No (projection): Store as-is
  - Examples
    + Received from outside (boundaries) e.g. glTF, `lookAt`, etc.
    + Built-in from functions like `XMMatrixTranslation`
* Input: Transposed `FLOAT4X4`
  - Drop last row + store as `FLOAT3X4A`
  - No built-in DirectXMath function; to be written
  - Example: matrices inside engine
* Vector Transform
  - Dot product every row of `FLOAT3X4A` with vector
    + Works since basis vectors are already transposed
    + This is nothing but _pre-multiplication_; in row-vector convention this means
      non-transpose access i.e. $p' = pM$ transposing $p'^T = M^T p^T$ (we already’ve $M^T$)
  - `XMVECTOR` transformation with `FLOAT3X4A` is faster; skip usual transposed (column)
    access needed in matrix multiplication.  Just .
  - No built-in DirectXMath function; to be written
* Matrix Concatenation
  - Convert `FLOAT3X4A` to `XMMATRIX` by just slapping a 4th row vector $\begin{bmatrix}0 & 0 & 0 & 1 \end{bmatrix}$
  - Concatenate transforms right to left (reversed order) i.e. pre-multiply unlike usual DirectXMath convention
  - Drop last row and store the result
* Shader (GLSL) Usage
  - `FLOAT4X4` (projection): _Pre-multiply_
    + GPU will do the usual transpose access needed for matrix multiplication
  - `FLOAT3X4` (affine): _Post-multiply_
    + Upload to shader as `FLOAT3X4`; receive the transposed matrix as-is
    + We’ve two options to handle this transposed matrix
      1. Transpose and adhere to usual GLSL pre-multiply convention $p' = Mp$
      2. _Post-multiply_ `FLOAT3X4` with `vec4` i.e $p' = pM$
        * Good news: you can’t pre-multiply (compiler errors out; type mismatch)
        * GPU won’t do the usual transpose access needed for matrix multiplication
        * _Post-multiply transposed_ matrices unlike GLSL’s default convention
          - Works since $p' = Mp$ is same as $p'^T = p^T M^T$


[^major]: Notation and storage; every 4-vector -- every column in column-major, row in row-major -- is stored linearly.

[^order-wrt]: Vector is post-/right-multiplied w.r.t to the matrix.

[^order-wrt2]: Successive transforms (matrices) are concatenated to the left or pre-multiplied.

[Sean Middleditch]: https://seanmiddleditch.github.io/matrices-handedness-pre-and-post-multiplication-row-vs-column-major-and-notations/


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'short', inlineCodeLang: 'c++' };</script>
