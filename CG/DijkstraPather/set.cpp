// implementation of §4.2 Dijkstra, Artifical Intelligence for Games, 2nd Ed.
// this does exactly as mandated in the book i.e. NodeRecordList and is costlier
// memory-wise then the min heap implementaion. However, when a node’s cost is
// to be updated, because of (multi)set where removal and reinsertion are both
// O(log n) operations and may be cheaper than reheapifying the heap: O(n)
//
// Uses these data strcutures:
// a. multiset<Edge>; for graph
// b. 2 × map<Node, NodeData> where NodeData = { cost, edge* }
// c. multiset<NodeCost> costs; for cost-prioritized node picking
// d. deque<Edge*>; for path retrival when found

#include <iostream>
#include <limits>
#include <set>
#include <map>
#include <algorithm>
#include <string>
#include <sstream>
#include <deque>

using Node = size_t;

struct Edge {
    Node from = 0;
    Node to = 0;
    float cost = 1.0f;
};

bool operator<(Edge const &a, Edge const &b) {
    return a.from < b.from;
}

bool operator<(Edge const &a, Node b) {
    return a.from < b;
}

bool operator<(Node a, Edge const &b) {
    return a < b.from;
}

struct Graph {
    // use C++14 heterogeneous, transparent comparator
    // http://umich.edu/~eecs381/handouts/HeteroLookup.pdf
    using EdgeSet = std::multiset<Edge, std::less<>>;
    using ConstEdgeIter = EdgeSet::const_iterator;
    using EdgeRange = std::pair<ConstEdgeIter, ConstEdgeIter>;

    EdgeRange getEdgesFrom(Node n) const {
        return edges.equal_range(n);
    }

    EdgeSet edges;
};

struct NodeData {
    Edge const *via;
    float cost;
};

struct NodeCost {
    Node id;
    float cost;
};

bool operator< (NodeCost const &a, NodeCost const &b) {
    return a.cost < b.cost;
}

bool operator< (NodeCost const &n, float c) {
    return n.cost < c;
}

bool operator< (float c, NodeCost const &n) {
    return c < n.cost;
}

struct NodeRecordList {

    size_t size() const { return nodes.size(); }
    bool empty() const { return nodes.empty(); }
    bool contains(Node n) const {
        auto it = nodes.find(n);
        return it != nodes.cend();
    }

    // undefined if empty
    NodeCost cheapest() const {
        return *costs.cbegin();
    }

    float cost(Node n) const {
        auto it = nodes.find(n);
        return (it != nodes.cend()) ?
            it->second.cost :
            std::numeric_limits<float>::infinity();
    }

    // undefined if n is absent
    NodeData data(Node n) const {
        auto it = nodes.find(n);
        return it->second;
    }

    void set(Node n, float cost, Edge const *e) {
        auto it = nodes.find(n);
        // create, if none found
        if (it == nodes.end()) {
            nodes.emplace(n, NodeData{e, cost});
            costs.emplace(NodeCost{n, cost});
        }
        else {
            costs.erase(getCostRecord(it->first, it->second.cost));
            costs.emplace(NodeCost{n, cost});
            it->second.cost = cost;
            it->second.via = e;
        }
    }

    void move(Node n, NodeRecordList &that) {
        auto it = nodes.find(n);
        if (it != nodes.end()) {
            that.set(it->first, it->second.cost, it->second.via);
            erase(it);
        }
    }

private:
    void erase(std::map<Node, NodeData>::iterator it) {
        costs.erase(getCostRecord(it->first, it->second.cost));
        nodes.erase(it);
    }

    std::multiset<NodeCost, std::less<>>::iterator
    getCostRecord(Node n, float cost) const {
        auto range = costs.equal_range(cost);
        return find_if(range.first,
                       range.second,
                       [n](auto const &nodeData) {
                           return nodeData.id == n;
                       });
    }

    std::map<Node, NodeData> nodes;
    std::multiset<NodeCost, std::less<>> costs;
};

void visitNode(Graph const &g,
               NodeCost node,
               NodeRecordList &openNodes,
               NodeRecordList &closedNodes) {
    auto edgeRange = g.getEdgesFrom(node.id);
    for (auto edge = edgeRange.first; edge != edgeRange.second; ++edge) {
        Node const dest = edge->to;
        if (!closedNodes.contains(dest)) {
            float const thisCost = node.cost + edge->cost;
            float const destCost = openNodes.cost(dest);
            if (destCost > thisCost)
                openNodes.set(dest, thisCost, &*edge);
        }
    }
}

std::string retrivePath(NodeRecordList const &closedNodes,
                        NodeRecordList const &openNodes,
                        Node from,
                        Node to) {
    NodeData d = openNodes.data(to);
    float const total_cost = d.cost;
    std::deque<Edge const*> path(1, d.via);
    Node cur = d.via->from;
    while (cur != from) {
        d = closedNodes.data(cur);
        path.push_front(d.via);
        cur = d.via->from;
    }
    std::ostringstream ss;
    ss << path.front()->from;
    for (auto const &stride : path)
        ss << " --" << stride->cost << "--> " << stride->to;
    ss << "; total cost: " << total_cost;
    return ss.str();
}

std::string findPath(Graph const &g, Node from, Node to) {
    // Although the book says both should be of this type, the data structure
    // used for getting the node with least cost isn’t needed for closedNodes.
    NodeRecordList openNodes, closedNodes;
    // set the cost to start node from start node as 0
    // leave the edge via to reach here as nullptr to denote a None (loop)
    openNodes.set(from, 0.0f, nullptr);
    NodeCost node = { };
    while (!openNodes.empty()) {
        node = openNodes.cheapest();
        if (node.id == to)
            break;
        visitNode(g, node, openNodes, closedNodes);
        openNodes.move(node.id, closedNodes);
    }
    return (node.id == to) ? retrivePath(closedNodes,
                                         openNodes,
                                         from,
                                         to) : "no path";
}

std::ostream& operator<<(std::ostream &os, Edge const &e) {
    return os << e.from << " --> " << e.to << ", cost: " << e.cost;
}

std::ostream& operator<<(std::ostream &os, Graph::EdgeRange const &p) {
    for (auto it = p.first; it != p.second; ++it)
        os << *it << '\t';
    return os;
}


void simpleCase() {
    Graph g = {
        {
            {0, 1, 1.0f},
            {1, 0, 1.0f},
            {1, 2, 10.0f},
            {2, 1, 10.0f},
            {2, 3, 5.0f},
            {3, 2, 5.0f},
            {3, 0, 2.0f},
            {0, 3, 2.0f}
        }
    };
    std::cout << "0: " << g.getEdgesFrom(0) << '\n';
    std::cout << "1: " << g.getEdgesFrom(1) << '\n';
    std::cout << "4: " << g.getEdgesFrom(4); // prints nothing
    std::cout << '\n' << "0 to 2: " << findPath(g, 0, 2);
    std::cout << '\n' << "2 to 0: " << findPath(g, 2, 0);
    std::cout << '\n' << "1 to 0: " << findPath(g, 1, 0);
    std::cout << '\n' << "4 to 0: " << findPath(g, 4, 0) << '\n';
}

void bookCase() {
    Graph g = {
        {
            {0, 1, 1.3f},
            {0, 2, 1.6f},
            {0, 3, 3.3f},
            {1, 4, 1.5f},
            {1, 5, 1.9f},
            {2, 3, 1.3f},
            {5, 6, 1.4f}
        }
    };
    std::cout << '\n' << "0 to 6: " << findPath(g, 0, 6);
    std::cout << '\n' << "0 to 3: " << findPath(g, 0, 3) << '\n';
}

void newCase() {
    Graph g = {
        {
            {0, 1, 3.0f},
            {1, 0, 2.0f},
            {1, 2, 7.0f},
            {2, 1, 5.0f},
            {2, 3, 1.0f},
            {3, 2, 4.0f},
            {3, 5, 3.0f},
            {5, 3, 3.0f},
            {3, 8, 2.0f},
            {8, 3, 5.0f},
            {2, 4, 5.0f},
            {4, 2, 2.0f},
            {1, 4, 3.0f},
            {4, 1, 3.0f},
            {0, 4, 7.0f},
            {4, 0, 3.0f},
            {4, 5, 2.0f},
            {5, 4, 1.0f},
            {5, 8, 4.0f},
            {8, 5, 2.0f},
            {4, 6, 1.0f},
            {6, 4, 3.0f},
            {0, 6, 5.0f},
            {6, 0, 8.0f},
            {6, 7, 5.0f},
            {7, 6, 2.0f},
            {7, 8, 3.0f},
            {8, 7, 4.0f}
        }
    };
    std::cout << '\n' << "0 to 8: " << findPath(g, 0, 8);
}

int main() {
    simpleCase();
    bookCase();
    newCase();

    Graph g = {
        {
            { 1, 2, 3.0f },
            { 1, 5, 4.0f },
            { 1, 3, 6.0f },
            { 2, 4, 5.0f },
            { 5, 6, 6.0f },
            { 3, 6, 1.0f },
            { 6, 7, 4.0f },
            { 4, 7, 2.0f }
        }
    };
    std::cout << '\n' << "1 to 7: " << findPath(g, 1, 7);
}
