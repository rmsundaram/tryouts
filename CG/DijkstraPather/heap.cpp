// implementation of §4.2 Dijkstra, Artifical Intelligence for Games, 2nd Ed.
// Memory-wise would be better than the set-based implemenation but may be
// slower than that due to reheapify’s time complexity. Uses the following:
// a. multiset<Edge>; for graph
// b. map<Node, NodeRecord> where NodeRecord = { cost, edge*, state }
// c. vector<Node>; for cost-prioritized node picking
// d. vector<Edge&>; for path retrival when found

// TODO: The greater time complexity is because of using the stock (std) heap
// facilities that has no rebalance(i).  This can be avoided; without
// reheapifying, one can, if the index of the priority-changed element is known,
// adjust just that element by bubbling it up or down restoring the heap
// property of the binary tree.  This is possible with a custom implementation
// like C++/min_heap.cpp. Another example:
// https://github.com/luapower/heap/blob/master/heap.lua

#include <iostream>
#include <set>
#include <map>
#include <limits>
#include <queue>
#include <algorithm>
#include <string>
#include <sstream>
#include <functional>
#include <vector>

using Node = size_t;

struct Edge
{
    Node from = 0;
    Node to = 0;
    float cost = 1.0f;
};

bool operator<(Edge const &a, Edge const &b) {
    return a.from < b.from;
}

bool operator<(Edge const &a, Node b) {
    return a.from < b;
}

bool operator<(Node a, Edge const &b) {
    return a < b.from;
}

struct Graph
{
    // use C++14 heterogeneous, transparent comparator
    // http://umich.edu/~eecs381/handouts/HeteroLookup.pdf
    using EdgeSet = std::multiset<Edge, std::less<>>;
    // multiset since we need the edges to be sorted based on their ‘from’ field
    // and multiple edges will have the same from field
    using ConstEdgeIter = EdgeSet::const_iterator;
    using EdgeRange = std::pair<ConstEdgeIter, ConstEdgeIter>;

    EdgeRange getEdgesFrom(Node n) const {
        return edges.equal_range(n);
    }

    EdgeSet edges;
};

/*
 * ALGORITHM
 *
 *  1. Start from the source node; let its cost-so-far be 0 and reached-via be
 *     nullptr. Make source the current node.
 *  2. For each edge coming out of current node:
 *  3.   If destination is already VISITED, skip it.
 *  4.   Calculate the current cost as current.cost-so-far + edge's cost
 *  5.   If current cost < destination.cost-so-far, update destination's
 *       cost-so-far and reached-via to current cost and current edge. Nodes
 *       unseen thusfar have a cost of ∞.
 *  6.   Mark the destination as SEEN but not VISITED.
 *  7. Mark current node as VISITED.
 *  8. Pick the SEEN node with least cost-so-far
 *  9. If this node is the destination, go to 10, else go to 2
 * 10. If there're no more SEEN nodes, there's no path from src to dst. STOP
 * 11. Calculate path by back tracking from current through its reached-via's
 *     from node.
 *
 * To implement Dijkstra's algorithm, the book talks about two lists with fast
 * search, insertion and deletion: Open and Closed.  The Closed list is looked
 * up by node ID, just to check if a node is already processed.  The Open
 * list too is looked up by node ID to get the node's records like cost-so-far,
 * reached-via, etc. to be used in places like tracing back the path from end
 * to start, checking and updating a node's cost.  However, it is also looked up
 * with cost as a key, to get the cheapest node the list contains.  In practice,
 * the same data structure may be inefficient when searched by different keys,
 * so the idea is to break it into two collections: one sorted by cost and the
 * other by node ID.  A priority queue of (open) nodes sorted by cost and a map
 * of node ID mapping to node's records.  Having a single list for both Open and
 * Closed eliminates the need to move elements from one list to another and also
 * the need to search both the lists when back tracking to find the path.  Hence
 * having the state of a node along with its other records would do in keeping
 * just a single list for both.  It is to be noted that the priority queue
 * should be kept in sync with the list of open nodes i.e. when something is
 * inserted or removed, the respective node's state should be updated
 * accordingly.
 */

enum class NodeState
{
    Unseen = 0,
    Seen,
    Visited
};

struct NodeRecord
{
    // storing the state to avoid creating two maps, SEEN and VISITED
    NodeState state = NodeState::Unseen;
    float cost = std::numeric_limits<decltype(cost)>::infinity();
    Edge const *via = nullptr;
};

// priority_queue with rehapify for allowing updates to element priority
struct NodeHeap
{
    void push(Node n) {
        nodes.push_back(n);
        std::push_heap(std::begin(nodes), std::end(nodes), [&]
                       (Node const &lhs, Node const &rhs) {
                           return nodeData.find(lhs)->second.cost >
                               nodeData.find(rhs)->second.cost;
                       });
    }

    void pop() {
        std::pop_heap(std::begin(nodes), std::end(nodes), [&]
                      (Node const &lhs, Node const &rhs) {
                          return nodeData.find(lhs)->second.cost >
                              nodeData.find(rhs)->second.cost;
                      });
        nodes.pop_back();
    }

    bool empty() const {
        return nodes.empty();
    }

    Node top() const {
        return nodes.front();
    }

    void reheapify() {
        std::make_heap(std::begin(nodes), std::end(nodes), [&]
                       (Node const &lhs, Node const &rhs) {
                           return nodeData.find(lhs)->second.cost >
                               nodeData.find(rhs)->second.cost;
                       });
    }

    NodeHeap(std::map<Node, NodeRecord> const &data) : nodeData(data) {
    }

private:
    // nodeData is needed for getting the node’s priority (cost); another option
    // is to store each node’s cost along with ID. Cost is also duplicated in
    // NodeRecord to print path cost during retrival; this would usually be
    // unneeded and hence in production code, cost can be stored only here.
    std::map<Node, NodeRecord> const &nodeData;
    std::vector<Node> nodes;
};

void visitNode(Node n,
               Graph const &g,
               std::map<Node, NodeRecord> &nodeData,
               NodeHeap &openNodes) {
    NodeRecord &thisNode = nodeData[n];
    auto const thisNodeCost = thisNode.cost;
    auto edgeRange = g.getEdgesFrom(n);
    for (auto edge = edgeRange.first; edge != edgeRange.second; ++edge) {
        auto const newCost = thisNodeCost + edge->cost;
        // get the old record or create a new one
        NodeRecord& dstNode = nodeData[edge->to];
        // if this isn't already visited and we've a better cost, update its
        // records; a newly created node would be Unseen with ∞ cost
        if ((dstNode.state != NodeState::Visited) && (dstNode.cost > newCost)) {
            dstNode.cost = newCost;
            dstNode.via = &*edge;
            // put it in the cost sorted list if Unseen; if it’s a cost update
            // reheapify priority_queue to resort based on updated cost
            if (dstNode.state == NodeState::Unseen)
                openNodes.push(edge->to);
            else
                openNodes.reheapify();
            // TODO: no need to reheapify for every priority change. Do this
            // once just before the next openNodes.pop i.e. just before
            // returning from this function
            dstNode.state = NodeState::Seen;
        }
    }
    thisNode.state = NodeState::Visited;
}

std::string retrivePath(std::map<Node, NodeRecord> const &nodeData,
                        Node from,
                        Node to) {
    std::vector<std::reference_wrapper<const Edge>> edges;
    float total = 0.0f;

    Node cur = to;
    while (cur != from) {
        auto it = nodeData.find(cur);
        Edge const &e = *it->second.via;
        edges.push_back(std::cref(e));
        cur = e.from;
    }
    std::ostringstream ss;
    ss << from;
    for (auto it = edges.crbegin(); it != edges.crend(); ++it) {
        Edge const &e = *it;
        total += e.cost;
        ss << " --" << e.cost << "--> " << e.to;
    }
    ss << "; total cost = " << total;
    std::string path = ss.str();
    return path;
}

std::string findPath(Graph const &g, Node from, Node to)
{
    // set the cost to start node from start node as 0
    // leave the edge via to reach here as nullptr to denote a None (loop)
    std::map<Node, NodeRecord> nodeData {
        { from, { NodeState::Seen, 0.0f, nullptr } }
    };
    NodeHeap openNodes(nodeData);
    openNodes.push(from);
    // stop traversal when there're no more nodes, or if we target node is next
    // in line to be visited
    while (!openNodes.empty() && (openNodes.top() != to)) {
        Node current = openNodes.top();
        openNodes.pop();
        visitNode(current, g, nodeData, openNodes);
    }
    // if we've met the target, then we'd not've popped it, so openNodes
    // wouldn't be empty; otherwise we don't've a valid path between from and to
    return openNodes.empty() ? "no path" : retrivePath(nodeData, from, to);
}

std::ostream& operator<<(std::ostream& os, Edge const &e) {
    return os << e.from << " --> " << e.to << ", cost: " << e.cost;
}

void print_edges(Graph::EdgeRange const &p) {
    for (auto it = p.first; it != p.second; ++it)
        std::cout << *it << '\n';
}


void simpleCase() {
    Graph g = {
        {
            {0, 1, 1.0f},
            {1, 0, 1.0f},
            {1, 2, 10.0f},
            {2, 1, 10.0f},
            {2, 3, 5.0f},
            {3, 2, 5.0f},
            {3, 0, 2.0f},
            {0, 3, 2.0f}
        }
    };
    print_edges(g.getEdgesFrom(0));
    print_edges(g.getEdgesFrom(1));
    print_edges(g.getEdgesFrom(4)); // prints nothing
    std::cout << '\n' << findPath(g, 0, 2);
    std::cout << '\n' << findPath(g, 2, 0);
    std::cout << '\n' << findPath(g, 1, 0);
    std::cout << '\n' << findPath(g, 4, 0);
}

void bookCase() {
    Graph g = {
        {
            {0, 1, 1.3f},
            {0, 2, 1.6f},
            {0, 3, 3.3f},
            {1, 4, 1.5f},
            {1, 5, 1.9f},
            {2, 3, 1.3f},
            {5, 6, 1.4f}
        }
    };
    std::cout << '\n' << findPath(g, 0, 6);
    std::cout << '\n' << findPath(g, 0, 3);
}

void newCase() {
    Graph g = {
        {
            {0, 1, 3.0f},
            {1, 0, 2.0f},
            {1, 2, 7.0f},
            {2, 1, 5.0f},
            {2, 3, 1.0f},
            {3, 2, 4.0f},
            {3, 5, 3.0f},
            {5, 3, 3.0f},
            {3, 8, 2.0f},
            {8, 3, 5.0f},
            {2, 4, 5.0f},
            {4, 2, 2.0f},
            {1, 4, 3.0f},
            {4, 1, 3.0f},
            {0, 4, 7.0f},
            {4, 0, 3.0f},
            {4, 5, 2.0f},
            {5, 4, 1.0f},
            {5, 8, 4.0f},
            {8, 5, 2.0f},
            {4, 6, 1.0f},
            {6, 4, 3.0f},
            {0, 6, 5.0f},
            {6, 0, 8.0f},
            {6, 7, 5.0f},
            {7, 6, 2.0f},
            {7, 8, 3.0f},
            {8, 7, 4.0f}
        }
    };
    std::cout << '\n' << findPath(g, 0, 8);
}

int main() {
    simpleCase();
    bookCase();
    newCase();

    Graph g = {
        {
            { 1, 2, 3.0f },
            { 1, 5, 4.0f },
            { 1, 3, 6.0f },
            { 2, 4, 5.0f },
            { 5, 6, 6.0f },
            { 3, 6, 1.0f },
            { 6, 7, 1.0f },
            { 4, 7, 1.0f }
        }
    };
    std::cout << '\n' << "1 to 7: " << findPath(g, 1, 7);
}
