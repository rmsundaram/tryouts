#include <glm/glm.hpp>
#include <glm/gtx/component_wise.hpp>

#include <cmath>
#include <iostream>

// Given a n-dimensional vector, we want a partition of unity with the elements
// depending on each elements’ magnitude.

// Resulting elements will add up to one but when elements are negative,
// not all will be < 1 (like affine combination).
glm::vec4 weighted_avg(glm::vec4 v) {
    auto const sum = glm::compAdd(v);
    return v / sum;
}

// Result is a proper partition of unity: all elements will be < 1 too, like
// convex combination. softmax gives a discrete probability distribution.
// https://eli.thegreenplace.net/2016/the-softmax-function-and-its-derivative/
glm::vec4 softmax(glm::vec4 v) {
    auto const ev = glm::exp(v);
    auto const esum = glm::compAdd(ev);
    return ev / esum;

    /* Numerically stable softmax by first shifting everything within [-1, 1]
     *
     *   shifted_v = v - glm::max(v)
     *   ev = glm::exp(shifted_v)
     *   return ev / glm::compAdd(ev);
     */
}

std::ostream& operator << (std::ostream &os, const glm::vec4 &v)
{
    return os << '['
              << v[0] << ", " << v[1] << ", " << v[2] << ", " << v[3]
              << ']';
}

int main() {
    glm::vec4 v1 = { 1.4, 2.1, 3.8, 7.05 };
    std::cout << v1 << "\n    "
              << "weighted avg: "
              << weighted_avg(v1)
              << " component-wise sum = "
              << glm::compAdd(weighted_avg(v1)) << "\n    "
              << "softmax: "
              << softmax(v1) << '\n';

    glm::vec4 v2 = { 1.4, 2.1, 3.8, -3.76 };
    std::cout << v2 << "\n    "
              << "weighted avg: "
              << weighted_avg(v2)
              << " component-wise sum = "
              << glm::compAdd(weighted_avg(v2)) << "\n    "
              << "softmax: "
              << softmax(v2) << '\n';
}
