#include <iostream>
#include <iomanip>
#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>

#include <glm/gtx/string_cast.hpp>

/*
   glm/gtx/transform.hpp has transform builders returning a matrix, taking only
   a vector; useful for pre-multiplication.

   glm/ext/matrix_transform.hpp (included by glm/gtc/matrix_transform.hpp) does
   post-mutliplication e.g. glm::translate(M, glm::vec3(1.0f, 0.0f, 0.0f))
   returns a translation matrix post-multiplied to M.
*/

std::ostream& operator<<(std::ostream& os, const glm::mat4 &m)
{
    return
    os << m[0][0] << '\t' << m[1][0] << '\t' << m[2][0] << '\t' << m[3][0] << '\n'
       << m[0][1] << '\t' << m[1][1] << '\t' << m[2][1] << '\t' << m[3][1] << '\n'
       << m[0][2] << '\t' << m[1][2] << '\t' << m[2][2] << '\t' << m[3][2] << '\n'
       << m[0][3] << '\t' << m[1][3] << '\t' << m[2][3] << '\t' << m[3][3] << '\n';
}

std::ostream& operator << (std::ostream &os, const glm::vec4 &v)
{
    return os << v[0] << '\t' << v[1] << '\t' << v[2] << '\t' << v[3] << '\n';
}

glm::mat4 myLookAt(const glm::vec3 &eye, const glm::vec3 &at, const glm::vec3 &up)
{
    const auto Z = glm::normalize(eye - at);
    const auto X = glm::normalize(glm::cross(up, Z));
    const auto Y = glm::cross(Z, X);

    glm::mat3 R{X, Y, Z};
    glm::mat3 R_inv = glm::transpose(R);
    glm::vec3 W = -(R_inv * eye);
    // the items are taken up as filling the columns 0, 1, ...
    return glm::mat4{glm::vec4{R_inv[0], 0.0f},
                     glm::vec4{R_inv[1], 0.0},
                     glm::vec4{R_inv[2], 0.0f},
                     glm::vec4{W, 1.0f}};
}

int main()
{
    {
        auto p = glm::vec4(5.0f, 20.f, -50.0f, 1.0f);
        auto v = glm::vec4(5.0f, 20.f, -50.0f, 0.0f);

        auto R = glm::rotate(glm::radians(90.0f), glm::vec3{0.0f, 0.0f, 1.0f});
        auto T = glm::translate(glm::vec3{1.0f, 2.0f, 12.3f});

        std::cout << R << '\n' << T << std::endl;

        // treats p as a column-vector
        auto pR = R * p;
        std::cout << pR << std::endl;

        // treats p as a row-vector
        auto Rp = p * R;
        std::cout << Rp << std::endl;

        auto pT = T * p;
        std::cout << pT << std::endl;

        auto vT = T * v;
        std::cout << vT << std::endl;

        auto RotTrans = T * R;
        std::cout << RotTrans << std::endl;

        auto TransRot = R * T;
        std::cout << TransRot << std::endl;

        auto RotTrans_p = T * R * p;
        std::cout << RotTrans_p << std::endl;
    }

    glm::vec3 eye{4656.0f, 13.0f, 5.0f},
               at{686.0f, 1021.0f, 9999.0f},
               up{1.0f, 1.0f, -200.0f};
    std::cout << glm::lookAt(eye, at, up) << std::endl;
    std::cout << myLookAt(eye, at, up) << std::endl;

    // transform functions
    const auto R = glm::rotate(glm::radians(45.0f), glm::vec3(1.0f, 0.0f, 0.0f));
    std::cout << "Rotate:\n" << R << '\n';
    const auto T = glm::translate(glm::vec3(10.0f, 20.0f, 30.0f));
    std::cout << "Translate:\n" << T << '\n';
    const auto TandR = R * T;
    // This shows that GLM's scale, rotate and translate functions which takes a
    // matrix along with the transform's params post-multiplies T with M i.e.
    // M' = M * T, where T = one of scale, rotate or translate. Note: changing
    // the above expression to T * R gives an almost identical matrix but that's
    // because we're doing operations that are commutative, but R * T gives an
    // identical matrix to what the below function generates.

    const auto RT = glm::translate(R, glm::vec3(10.0f, 20.0f, 30.0f));

    std::cout << "Custom:\n" << TandR << std::endl;
    std::cout << "Function:\n" << RT << '\n';

    // REMEMBER
    //   1. GLSL matrix type is column-major: matNxM means N cols and M rows
    //   2. GLSL transforms by pre-/left-multiply: p' = V * M * p
    //   3. OpenGL (and D3D) specified matrices have same memory layout: X Y Z T
    //        - Each _column_ concatenated linearly (already transposed)
    //        - Facilitates quick extraction of a column (Cache is King! :)
    //
    // GLM follows GLSL in (1), (2) and does (3).
    //
    // REFERENCES
    //   1. https://stackoverflow.com/q/17717600/183120
    //   2. http://duckmaestro.com/2013/08/17/matrices-are-not-transforms/
    //   3. GLM’s manual

    // pre-multiply (column vector)
    {                               //                 |1 0 Tx|
        auto M = glm::mat3x2(1.0);  // 3 col × 2 row   |0 1 Ty|
        M[2][0] = 5.0;  // Tx   Dimension 1 denotes column, 2 denotes row;
        M[2][1] = 10.0; // Ty   subscripts reversed since transposed in memory.
        glm::vec3 v{2, 3, 1.0};
        auto const mv = M * v;      // pre-/right-multiply
        std::cout << glm::to_string(mv) << '\n';

        // Another way to construct: M
        // Notice translate component cleanly separated.
        // Notice elements transposed in code: each row is a column
        auto const M1 = glm::mat3x2(1.0, 0.0,
                                    0.0, 1.0,
                                    5.0,10.0);
        std::cout << glm::to_string(M1 * v) << '\n';
    }
    // post-multiply (row vector)
    {
        auto M = glm::mat2x3(1.0);  // 2 col × 3 row  | 1  0|
        M[0][2] = 5.0;  // Tx                         | 0  1|
        M[1][2] = 10.0; // Ty                         |Tx Ty|
        glm::vec3 v{2, 3, 1.0};
        auto const mv = v * M;      // post-/left-multiply
        std::cout << glm::to_string(mv) << '\n';

        // Another way to construct: M
        // Notice translate interspersed; no clean separation
        // Notice elements transposed in code: each row is a column
        auto const M1 = glm::mat2x3(1.0, 0.0, 5.0,
                                    0.0, 1.0,10.0);
        std::cout << glm::to_string(v * M1) << '\n';
    }

    // glm/gtc/matrix_access.hpp has convenience functions to get a particular
    // row or column; former is extracted while latter is a simple 1D subscript.
}
