#include <iostream>
#include <iomanip>

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/string_cast.hpp>

// Both methods convert a quaternion into rotation matrix; one outputs matrix
// suited for column-vector convention and the other row-vector convention;
// notice the only difference: inner 3×3 matrix is transposed.  A unit
// quaternion input is expected (scaled output otherwise).  They’re better
// suited with parallel processors sporting SIMD and shuffle/swizzle operations.

// Essential Math 2/e, §5.5.7
glm::mat4 to_mat_col(glm::vec4 q) {
  const glm::mat3 m3(glm::vec3( q.w, q.z,-q.y),
                     glm::vec3(-q.z, q.w, q.x),
                     glm::vec3( q.y,-q.x, q.w));
  glm::mat4 m1(m3), m2(m3);
  m1[3] = q;
  m2[3] = -q;
  m2[3][3] = -m2[3][3];
  m1[0][3] = -q.x;
  m1[1][3] = -q.y;
  m1[2][3] = -q.z;
  m2[0][3] = q.x;
  m2[1][3] = q.y;
  m2[2][3] = q.z;
  return m1 * m2;
}

// http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToMatrix/index.htm
glm::mat4 to_mat_row(glm::vec4 q) {
  const glm::mat3 m3(glm::vec3( q.w,-q.z, q.y),
                     glm::vec3( q.z, q.w,-q.x),
                     glm::vec3(-q.y, q.x, q.w));
  glm::mat4 m1(m3), m2(m3);
  m1[3] = q;
  m2[3] = -q;
  m2[3][3] = -m2[3][3];
  m1[0][3] = -q.x;
  m1[1][3] = -q.y;
  m1[2][3] = -q.z;
  m2[0][3] = q.x;
  m2[1][3] = q.y;
  m2[2][3] = q.z;
  return m1 * m2;
}

int main() {
  // rotate counter-clockwise by 45° along +Z
  constexpr float angle = 23.32f;
  auto rot_col1 = glm::rotate(glm::radians(angle), glm::vec3{0.f, 0.f, 1.0f});
  const glm::vec4 unit_q(0.f,  // <sin ½θ Z, cos ½θ>
                         0.f,
                         sin(glm::radians(angle/2)),
                         cos(glm::radians(angle/2)));

  const auto rot_col2 = to_mat_col(unit_q);
  const auto rot_row = to_mat_row(unit_q);
  std::cout << glm::to_string(rot_col1) << '\n';
  std::cout << glm::to_string(rot_col2) << '\n';
  std::cout << glm::to_string(rot_row) << '\n';
}
