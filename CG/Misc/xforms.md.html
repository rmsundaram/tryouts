<meta charset="utf-8">

    **Concatenation of Transformation Matrices**
                Sundaram Ramaswamy

<!-- Pandoc Markdown with LaTeX math equation; tested on pandoc 2.10.1 -->
<!-- pandoc --mathjax=https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js -f markdown+smart -so xforms.html xforms.md -->
<!--     skip giving --self-contained for HTML output since --mathjax conflicts with it -->
<!-- pandoc -V papersize=letter -V colorlinks --self-contained -f markdown+smart -so xforms.pdf xforms.md -->

# Conventions

* Column-vector
* Column-major matrices
* Right or post-multiplication of vector
* Left or pre-multiplication of successive transforms
* Rotations are in degree; positive values rotate counter-clockwise

Refer [Matrices, Handedness, Pre and Post Multiplication, Row vs Column Major, and Notations](https://seanmiddleditch.github.io/matrices-handedness-pre-and-post-multiplication-row-vs-column-major-and-notations/) for a detailed analysis of other conventions and issues (storage, notation, handedness, …) around them.

!!! Warning: General Convention of Pre- or Post-multiplication
    Literature states D3D traditionally follows pre-multiplication and OpenGL follows post-multiplication; this is based on where the vector is w.r.t to the matrix.  Generally, a system’s convention vests on where the vector goes.  However, in this document we use these terms to specify which side of a matrix is another concatenated/multiplied.

# Summary

Three approaches useful in different situations.  Refer [[6](#references)] for (1.1) and (2) and [[5](#references)] for (2).

1. **Transform points/vectors**: read/apply right to left; transform about fixed frame
    1. Moving/Active transforms e.g. rotate, translate, etc.
    2. Mapping/Passive transforms e.g. map from model to world to view, map local to parent local, etc.
        - Denoted with $M_{a \rightarrow b}$ below
2. **Transform coordinate systems**: read/apply left to right; transform about local frame
    - Denoted with $T_{a \rightarrow b}$ below

\begin{align*}
M_{a \rightarrow c} &= M_{b \rightarrow c} M_{a \rightarrow b}  \\
T_{c \rightarrow a} &= T_{c \rightarrow b} T_{b \rightarrow a}
\end{align*}

With $M$ we transform points in a child system $a$ to its parent system $b$, then from parent to grandparent system $c$.  With $T$ we transform the grandparent system $c$ to parent system $b$ and then to child system.  Both give the same result as $M_{x \rightarrow y}$ is $T_{y \rightarrow x}$ all along.  For mapping reading/applying from right to left (as it’s viewed as transforming points) makes sense, while for coordinate transforms reading/applying from left to right makes sense.  $M_{x \rightarrow y}$ = $T_{y \rightarrow x}$ is nothing but system $x$ specified in terms of $y$ or $y$ basis transformed/merged into $x$ (in $y$ space).

!!! Warning: Don’t mind _alibi_ and _alias_ explanations
    [Many literature][py-xform] explains [active (alibi) and passive (alias) transforms][alibi-alias].  While this may be useful in other domains, not so much in CG.  Passive transform where the coordinate system is applied an _inverse_ transform (as the point in alibi) to get the spatially stationary point’s coordinate in the new space, is confusing and better avoided.  Why?  Points always change in CG.  We ultimately apply all transforms to points/vectors of meshes, lights, etc.  The notion of coordinate system is conceptual.  Using (2) above is more useful in such situations.

[py-xform]: https://dfki-ric.github.io/pytransform3d/user_guide/transformation_ambiguities.html#active-alibi-vs-passive-alias-transformation
[alibi-alias]: https://en.wikipedia.org/wiki/Active_and_passive_transformation

# Transform Points

A matrix $X$ transforms a point $p$ to $p'$ in space $A$.  Every successive transform is pre-multiplied; all transforms happen in a fixed space where the points _live_.  The order of the transform application is right to left.  There’s nothing much here.

# Transform Coordinate System

## Transform

A matrix $T_{a \rightarrow b}$ transforms frame $A$ (its basis and origin) into a new frame $B$ (or merges frame $A$ with frame $B$), such that further transforms are w.r.t frame $B$.  Successive transforms are post-multiplied.  Every successive transform is w.r.t the left-most (descendant) system.

\begin{equation}
  T_{c \rightarrow a} = T_{c \rightarrow b} T_{b \rightarrow a}
  \tag{1}
  \label{1}
\end{equation}

Here, frame $C$ is transformed into frame $B$ and frame $B$ is transformed into frame $A$.

## Mapping

A matrix $M_{a \rightarrow b}$ maps frame $A$ to frame $B$, such that a point $p$ in $A$, when transformed by $M_{a \rightarrow b}$ gives $p'$ in frame $B$ referring to the same spatial point $p$ in $B$'s space.  Since the point stays in the same place but only represented in another system with a different value, it's a mapping.  Successive transforms are pre-multiplied.

\begin{equation}
  M_{a \rightarrow c} = M_{b \rightarrow c} M_{a \rightarrow b}
  \tag{2}
  \label{2}
\end{equation}

Here, the points in frame $A$ are mapped to frame $B$ by $M_{a \rightarrow b}$, now the same points represented in $B$'s frame are mapped to frame $C$ by $M_{b \rightarrow c}$ thereby giving $M_{a \rightarrow c}$.

# Inverse and Reverse operations

Inverse and reverse are two different operators applicable to matrices, the latter is uncommon and introduced here for easy understanding.  An inverse undoes what a transformation did, while reverse simply changes the order of multiplication/application of the transform.

\begin{align*}
Inv(AB) &= B^{-1} A^{-1} \\
Rev(AB) &= BA
\end{align*}

As an example, consider

\begin{align*}
Inv(M_{a \rightarrow c}) &= Inv(M_{b \rightarrow c} M_{a \rightarrow b}) = M_{b \rightarrow a} M_{c \rightarrow b} = M_{c \rightarrow a} \\
Rev(M_{a \rightarrow c}) &= Rev(M_{b \rightarrow c} M_{a \rightarrow b}) = M_{a \rightarrow b} M_{b \rightarrow c}
\end{align*}

$$
\boxed{Inv(M_{a \rightarrow c}) \neq Rev(M_{a \rightarrow c})}
$$

More importantly, for a single transform

\begin{align*}
Inv(M_{a \rightarrow b}) &= M_{b \rightarrow a} \\
Rev(M_{a \rightarrow b}) &= M_{a \rightarrow b}
\end{align*}

$Rev$ is an identity operation since there are no other matrices to change the order of multiplication.

# Transforms and Mappings

\begin{align*}
\text{Mapping} &= Rev(Inv(\text{Transform})) \\
\text{Transform} &= Rev(Inv(\text{Mapping}))
\end{align*}

Applying this for a single transform, we get

\begin{align*}
M_{b \rightarrow a} &= Rev(Inv(T_{b \rightarrow a})) \\
                    &= Rev(T_{a \rightarrow b}) \\
                    &= T_{a \rightarrow b} \\
\end{align*}

\begin{equation}
  \boxed{M_{b \rightarrow a} = T_{a \rightarrow b}}
  \tag{3}
  \label{3}
\end{equation}

This is an important result.

> "The matrix that maps points of frame $B$ to $A$ also transforms frame $A$ into $B$."

See [here](http://www.dgp.toronto.edu/~neff/teaching/418/transformations/transformation.html) for a detailed explanation.  The same matrix can be interpreted as either active or passive.  However, this holds only for a single transform and not for concatenations which require the $Rev$ operator.  For multiple transforms

\begin{align*}
M_{c \rightarrow a} &= Rev(Inv(T_{c \rightarrow a}))
     &= Rev(T_{a \rightarrow c}) \\
     &= Rev(T_{b \rightarrow c} T_{a \rightarrow b}) \\
     &= T_{a \rightarrow b} T_{b \rightarrow c} = T_{a \rightarrow c} \\
     &= M_{b \rightarrow a} M_{c \rightarrow b} & \because T_{a \rightarrow b} = M_{b \rightarrow a} \\
\end{align*}

## Pre and post-multiplication

When having transformations $T_1$, $T_2$ and $T_3$ to be applied in that order, ideally we start from identity to which $T_1$ is pre-multiplied --- this step is usually skipped --- and we've $T_1$ to which $T_2$ is pre-multiplied giving $T_2T_1 = U$ to which the final transform $T_3$ is pre-multiplied $T_3U = T_3(T_2T_1) = W$.  Another way to arrive at $W$, the final transform, is to start from $T_3$ and to post-multiply $T_2$ giving $T_3T_2 = V$ to which $T_1$ is post-multiplied $VT_1 = (T_3T_2)T_1 = W$.  This is allowed as matrix multiplication is associative: $A(BC) = (AB)C$.

Thus, if you look at $T$ as starting from the first frame ($A$) proceeding to the last ($C$) by post-multiplying, instead of pre-multiplying, the reverse operator can be eliminated[^1]:

$$
\begin{align*}
M_{c \rightarrow a} &= Inv(T_{c \rightarrow a}) \\
                    &= T_{a \rightarrow c}                        &\text{T starts from A proceeds to C by post-multiplication} \\
                    &= T_{a \rightarrow b} T_{b \rightarrow c}    &\text {this result is the same as \eqref{1}}^1 \\
                    &= M_{b \rightarrow a} M_{c \rightarrow b}    &\text{M is still in the original pre-multiplying convention} \\
\end{align*}
$$

[^1]: $T_{a \rightarrow c} = T_{a \rightarrow b} T_{b \rightarrow c}$ is written omitting the $Rev$ operator and is done only for deriving $M_{c \rightarrow a}$ and is incorrect in general; the correct equation is $T_{a \rightarrow c} = T_{b \rightarrow c} T_{a \rightarrow b}$.

Ignoring the $Rev$ operator, we can now redefine result $\eqref{3}$ for multiple transforms (concatenations) too, with a modification:

> "A matrix which maps points of frame $C$ to $A$ also transforms frame $A$ into frame $C$ when concatenations are post-multiplied successively and is interpreted from left to right."

Interpreting $T$ as pre-multiplication vs post-multiplication has two deeper meanings, which leads us to the next section.  The terms left and right multiplication is used in place of pre- and post-multiplication sometimes.

## Transforming points vs Coordinate Systems

[[1](#references)], [[2](#references)] and [[5](#references)] (all using column-vector convention) show that when multiple transforms (concatenations) are seen from right to left, then it's transforming points within the same global frame; every further transform is pre-multiplied.  When the same is seen from left to right, then its transforming a frame (not points) into another frame, each transform applied will be relative to the (local) frame currently in and is post-multiplied; finally, the points are interpreted in the last transfomed frame.

### Example

For this example $T$ denotes the translation transform.  The point $(0, 0)$ when transformed by $T_{(2, 2)} \circ R_{90} \circ T_{(1, 1)}$ ends up at $(1, 3)$ --- ordered left to right:  $T_{(1, 1)}$ then $R_{90}$ followed by $T_{2, 2}$; when interpreted right to left, the point first move to $(1, 1)$, then gets rotated to $(-1, 1)$, notice that the rotation was based on the original untransformed coordinate system's origin, with $\sqrt 2$ as the radius since the point was moved to $(1, 1)$ first within the same system.  From $(-1, 1)$ another translation again within the same system moves the point to $(1, 3)$.  Now seeing it from left to right, the original frame ($F_1$)'s origin is translated into a new frame ($F_2$) with origin at $(2, 2)$; the rotation of $90^\circ$ then happens relative to this transformed frame, with its origin as the centre of rotation, thereby leading to a newer frame ($F_3$).  This frame's origin is then translated to $(1, 1)$ with its basis as scale factor (units to move), forming another new frame ($F_4$) where the point $(1, 1)$ is plotted.  Now this point is spatially at the same place as the original frame $F_1$'s $(1, 3)$.

Both gave the same result while the interpretations were different; in the first interpretation there was just one (global) frame throughout the process while in the second there were 4 (local) frames in total.

**Note**: if you are using row vectors instead of column vectors, then transformation matrices are to be transposed, since $(XV)^T = V^T X^T$, hence the meaning of post- and pre-multiplication are reversed.

## Transforming points and coordinate system together

There are times when concatenating both types of transforms into one transform (matrix) is convenient.  To do so, in the column-vector convention, $X = \langle \text{coord} \rangle {} \langle \text{point} \rangle$ would be the correct order.  The point part moves the points within the local coordinate system and the coordinate part maps the final points to the target coordinate system.  Within *point* part matrices are always pre-multiplied but within *coord* part matrices are, post-multiplied if they are coordinate system transforms, or pre-multiplied if they are mapping matrices like in $\eqref{2}$.

When transforming coordinate systems, descendants are closer to the point/vector while ancestors are farther.  Shown by $p’ = M_{b \rightarrow c} M_{a \rightarrow b} p = T_{c \rightarrow b} T_{b \rightarrow a} p$ where $a, b, c$ are child, parent and grandparent.

## Interpretation of Rows and Columns

The columns of the matrix $M_{a \rightarrow b}$ would be $A$'s frame expressed in terms of $B$'s frame.  OTOH the same matrix, whose columns are $A$'s frame expressed using $B$'s frame, is also $T_{b \rightarrow a}$ i.e. $B$'s basis transformed to merge with $A$'s frame.  This is true since $T_{b \rightarrow a} = Rev(M_{a \rightarrow b}) = M_{a \rightarrow b}$.  For transformation we describe the destination in source terms while in mapping we describe the source in destination terms.  Additionally, for orthogonal matrices (like rotation), the rows of $M_{a \rightarrow b}$ would be $B$'s frame expressed using $A$'s frame.  Conversely for an orthogonal matrix $T_{b \rightarrow a}$, the rows are $B$'s frame expressed in $A$'s frame.  The $M_{a \rightarrow b}$ case is explained in [[3](#references)] and the $T_{b \rightarrow a}$ case is in [[4](#references)].

# Case Study: Mapping PDF to D2D

![Mapping points in PDF to D2D](../Calculations/Mapping.jpg "Mapping points in PDF to D2D")

![PDF to D2D with clip](../Calculations/Mapping_with_clip.jpg "Mapping points in PDF to D2D with clip")

# Appendix

Matrices are just a convenient way to do linear combinations (multiplication and addition).  The reason why the number of columns on the matrix should equal number of rows of a vector is because the parameter count should equal the weight count.  For instance, you want to buy a ball; the parameters we are interested in are 3: cost, colour and material.  Irrespective of the number of stores we are considering, for all the stores we need to know these three quantities.  We also need the weightage we give to these parameters.  The product of these two matrices would give the scores of each store.

$$
\begin{pmatrix}
cost_1 & colour_1 & material_1 \\
cost_2 & colour_2 & material_2 \\
cost_3 & colour_3 & material_3 \\
cost_4 & colour_4 & material_4 \\
\vdots & \vdots & \vdots
\end{pmatrix}
\begin{pmatrix}
weight_1 \\
weight_2 \\
weight_3 \\
\vdots
\end{pmatrix} =
\begin{pmatrix}
store_1 \\
store_2 \\
store_3 \\
\vdots
\end{pmatrix}
$$

$$
n \times 3 \times 3 \times 1 = n \times 1
$$

When applied to geometry, it can be seen that a $2 \times 2$ matrix has the basis vectors $i$ and $j$ as its columns:

$$
\begin{pmatrix}
i.x && j.x \\
i.y && j.y
\end{pmatrix}
\begin{pmatrix}
P_x \\
P_y
\end{pmatrix} =
\begin{pmatrix}
P'_x \\
P'_y
\end{pmatrix}
$$

In linear combinations, unlike affine combinations, there's no constraint on the coefficients i.e. there's no requirement that $i.x + j.x = 1$; hence dot product of $P$ with $\langle \text{i.x, j.x} \rangle$ is nothing but calculating how X-ish the point $P$ becomes and dotting $P$ with $\langle \text{i.y, j.y} \rangle$ is calculating how Y-ish the point $P$ is --- this is the nature of dot product; it indicates how much a vector is aligned to another --- based on which $P'$ is obtained as a result.  The origin ($0$ vector) is unaffected by linear combinations since all weights are zeroed; only a translation (affine) has an effect on it [[5](#references)].

# References

1. F. S. Hill, Jr., Stephen M. Kelly, Jr. _Computer Graphics using OpenGL_, Third Edition, §5.4 How to Change Coordinate System, page 227
2. Steven J. Gortler. _Foundations of 3D Computer Graphics_, §2.2 Vectors, Coordinate Vectors, and Bases, §4.2 Multiple Transformations
3. Jason L. McKesson, _[Learning Modern 3D Graphics Programming][gltut]_, §6 Rotation
4. Fletcher Dunn, Ian Parberry. _[3D Math Primer for Graphics and Game Development][primer]_, Second Edition, [§8.2.2 Direction Cosines Matrix][primer-dir-cosine]
5. Samuel R. Buss. *3-D Computer Graphics*, §II.1.1 Basic Definitions, §II.1.7 Another Outlook on Composing Transformations
6. Dave Shreiner. _OpenGL Programming Guide_, Seventh Edition, §3.2 Viewing and Modeling Transformations

[gltut]: https://paroj.github.io/gltut
[primer]: https://www.gamemath.com/
[primer-dir-cosine]: https://www.gamemath.com/book/orient.html#matrix_direction_cosines


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none' };</script>

