// g++ -Wall -std=c++14 -pedantic -DFREEGLUT_STATIC
// -ID:/Libs/glm -ID:/Libs/freeglut/include -LD:/Libs/freeglut/lib
// rgb_hsv_lerp.cpp -lfreeglut_static -lopengl32 -lwinmm -mwindows

// clerp 1 0 0 0 1 1
// clerp 0 1 0 1 0 0
// clerp 0.2 0.8 0.1 0.7 0.1 0.3

#include <GL/freeglut.h>
#include <glm/glm.hpp>
#include <limits>
#include <iostream>

#ifndef NO_SRGB
#  include <glm/gtc/color_space.hpp>
#endif

/*
 * INF3320 - exercise on colour interpolation
 * http://www.uio.no/studier/emner/matnat/ifi/INF3320/h08/
 * undervisningsmateriale/oblig1.pdf
 * http://stackoverflow.com/q/2593832
 *
 * HSV is sometimes referred to as HSB where
 *     H is hue or the actual colour
 *     S is saturation or how washed-out the color is
 *     S → min = {grey, white} depending on {V, L}, max = pure colour
 *     V/B is value/brightness; min = black, max = pure colour
 * HSL is similar; L stands for lightness; min = black, max = white
 * In each cylinder, the angle around the central vertical axis corresponds to
 * "hue", the distance from the axis corresponds to "saturation", and the
 * distance along the axis corresponds to "lightness", "value" or "brightness".
 * The difference is that the brightness of a pure color is equal to the
 * brightness of white, while the lightness of a pure color is equal to the
 * lightness of a medium gray.
 * http://en.wikipedia.org/wiki/HSL_and_HSV
 */

// change represention of a color from RGB space to HSV space
glm::vec3 rgb2hsv(const glm::vec3& rgb)
{
    glm::vec3 hsv;
    int w = 0;
    auto min = rgb[0];
    auto max = rgb[0];
    for (int i = 0; i < 3; i++) {
        if (rgb[i] < min) min = rgb[i];
        if (rgb[i] > max) {
            max = rgb[i];
            w = i;
        }
    }
    hsv[2] = max;
    hsv[1] = (max > std::numeric_limits<float>::epsilon()) ?
             ((max - min) / max) : 0.0f;
    if (hsv[1] > std::numeric_limits<float>::epsilon()) {
        auto const delta = max - min;
        switch(w) {
            case 0: hsv[0] = 60.0f * (0.0f + (rgb[1] - rgb[2]) / delta); break;
            case 1: hsv[0] = 60.0f * (2.0f + (rgb[2] - rgb[0]) / delta); break;
            case 2: hsv[0] = 60.0f * (4.0f + (rgb[0] - rgb[1]) / delta); break;
        }
        while (hsv[0] < 0.0f) hsv[0] += 360.0f;
    }
    return hsv;
}

// change represention of a color from HSV space to RGB space
glm::vec3 hsv2rgb(const glm::vec3 &hsv)
{
    glm::vec3 rgb;
    auto h = hsv[0];
    auto const s = hsv[1];
    auto const v = hsv[2];
    if (s <= std::numeric_limits<float>::epsilon()) {
        for (size_t i = 0; i < 3; i++)
            rgb[i] = v;
    }
    else {
        while(h < 0.0f) h += 360.0f;
        while(h >= 360.0) h -= 360.0f;
        h /= 60.0f;
        int const i = floorf(h);
        float const f = h - i;
        float const p = v * (1.0f - s);
        float const q = v * (1.0f - (s * f));
        float const t = v * (1.0f - (s * (1.0f - f)));
        switch(i) {
        case 0: rgb[0] = v; rgb[1] = t; rgb[2] = p; break;
        case 1: rgb[0] = q; rgb[1] = v; rgb[2] = p; break;
        case 2: rgb[0] = p; rgb[1] = v; rgb[2] = t; break;
        case 3: rgb[0] = p; rgb[1] = q; rgb[2] = v; break;
        case 4: rgb[0] = t; rgb[1] = p; rgb[2] = v; break;
        case 5: rgb[0] = v; rgb[1] = p; rgb[2] = q; break;
        }
    }
    return rgb;
}

constexpr int wd = 512, ht = 512;
constexpr auto ht_2 = ht / 2.0f;
// if width = 10 then unit = 1/5 = 0.2; width * unit should cover 2 units in
// image space i.e. [-1, 1)
constexpr auto unit = 1.0f / ht_2;
glm::vec3 rgb1, rgb2;
glm::vec3 hsv1, hsv2;

void init()
{
    hsv1 = rgb2hsv(rgb1), hsv2 = rgb2hsv(rgb2);
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
}

void setColour(const glm::vec3& rgb)
{
// This article has RGB interpolation outputs done in different spaces.
// https://blog.demofox.org/2018/03/10/dont-convert-srgb-u8-to-linear-u8/
#ifndef NO_SRGB
    const auto colour = glm::convertLinearToSRGB(rgb); // default gamma
#else
    const auto colour = rgb;
#endif
    glColor3f(colour.r, colour.g, colour.b);
}

// Hand-rolled glm::mix.
template <typename T>
T lerp(T &t1, T &t2, float t)
{
    // t1 + (t2 - t1) * t risks floating-point’s catastropic cancellation
    return ((1.0f - t) * t1) + (t * t2);
}

void rgb_lerp()
{
    glBegin(GL_POINTS);

    // Points on window span across [-1, 1) on both X and Y in image space.
    // Debug code to display point at left-top corner i.e. screen space (0, 0).
    //
    // setColour(glm::vec3(1.0f, 1.0f, 1.0f));
    // glVertex2f(-1.0f, 1.0f - 3.90625e-3f);

    // RGB lerp fills the interval [1.0f - unit, 0.0f] in Y;
    // y becomes exactly y1 in the first iteration and exactly y2 in the last
    float const y1 = 1.0f - unit, y2 = 0.0f;
    for (auto j = 0.0f; j < ht_2; ++j) {
        // dec denom by one to avoid off-by-one error
        // 5 stops → 0⁄4, 1⁄4, 2⁄4, 3⁄4, 4⁄4
        float const y = lerp(y1, y2, j / (ht_2 - 1.0f));
        // same logic as Y for X and RGB
        glm::vec2 const p1{-1.0f, y}, p2{1.0f - unit, y};
        for (auto i = 0.0f; i < static_cast<float>(wd); ++i) {
            auto const t = i / (wd - 1.0f);
            auto const c = lerp(rgb1, rgb2, t);
            setColour(c);
            auto const p = lerp(p1, p2, t);
            glVertex2f(p.x, p.y);
        }
    }
    glEnd();
}

void hsv_interpolation()
{
    glBegin(GL_POINTS);

    float const y1 = -unit, y2 = -1.0f;
    // sift out SV for normal lerp
    glm::vec2 const sv1{hsv1.g, hsv1.b}, sv2{hsv2.g, hsv2.b};
    float const h1 = hsv1.r, h2 = hsv2.r;
    /*
     * Angle interpolation isn't straight forward; excerpt from exercise:
     *
     * Interpolating in the HSV space is slightly different. In the S and V
     * components, the linear interpolation formula given above works fine, but
     * the hue is an angle, which is periodic. Interpolation may therefore be
     * done clockwise or counterclockwise. Your application must choose the
     * shortest distance along the circle between the start and end hues. For
     * example, if the starting hue is 5, and ending hue is 340, your
     * interpolation should go something like 5 - 0/360 - 355 - 350 - 345 - 340,
     * instead of increasing from 5 to 340. If the angles are 180 degrees apart,
     * both directions are equally good, and your choice may be arbitrary.
     *
     * See anglerp.cpp workout for details on the interpolation of angle/hue.
     */
    auto const d = h2 - h1;
    auto const delta = (d + (floor(abs(d) / 180.0f) * -copysign(360.0f, d)))
                       / (wd + 1.0f);
    for (auto j = 0.0f; j < ht_2; ++j) {
        float const y = lerp(y1, y2, j / (ht_2 - 1.0f));
        // same logic as Y for X and SV
        glm::vec2 const p1{-1.0f, y}, p2{1.0f - unit, y};
        for (auto i = 0.0f; i < static_cast<float>(wd); ++i) {
            auto const t = i / (wd - 1.0f);
            /*
             * Generally x mod 360 and adding 360 if x's < 0 is the right
             * approach. To avoid the conditional, directly adding 360 followed
             * by a mod 360 won't work in cases where the magnitude of the angle
             * exceeds 360 — e.g. (−700 + 360) % 360 = −340 — since C's mod will
             * always result in a value whose sign will be the same as that of
             * the dividend's; had it been Lua or Python it will be the sign of
             * the divisor (see the Module.jpg calculation). However, for this
             * program, we know that |h| < 360 hence skipping the conditional.
             */
            auto const h = fmod((h1 + ((i + 1.0f) * delta)) + 360.0f, 360.0f);
            glm::vec3 const hsv{h, lerp(sv1, sv2, t)};
            auto const c = hsv2rgb(hsv);
            setColour(c);
            auto const p = lerp(p1, p2, t);
            glVertex2f(p.x, p.y);
        }
    }
    glEnd();
}

void disp()
{
    glClear(GL_COLOR_BUFFER_BIT);
    rgb_lerp();
    hsv_interpolation();
    glutSwapBuffers();
}

void handleKeys(unsigned char key,
                int x,
                int y)
{
    if (key == 27) {             // escape
        glutLeaveMainLoop();
    }
}

int main(int argc, char **argv)
{
    glutInit(&argc, argv);
    // glut expects the unmodified args to be passed to it; client
    // should consume only after glut extracts relevant params
    if (argc >= 7) {
        rgb1 = glm::vec3{std::strtof(argv[1], nullptr),
                         std::strtof(argv[2], nullptr),
                         std::strtof(argv[3], nullptr)};
        rgb2 = glm::vec3{std::strtof(argv[4], nullptr),
                         std::strtof(argv[5], nullptr),
                         std::strtof(argv[6], nullptr)};
        glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
        glutInitWindowSize(wd, ht);
        glutCreateWindow("RGB and HSV Interpolation");
        // instead of abruptly quitting from the main loop, return control
        // back to this function
        glutSetOption(GLUT_ACTION_ON_WINDOW_CLOSE,
                      GLUT_ACTION_GLUTMAINLOOP_RETURNS);
        glutKeyboardFunc(handleKeys);
        glutDisplayFunc(disp);
        init();
        glutMainLoop();
    }
    else
        std::cerr << "Interpolate colour in RGB and HSV colour spaces.\n"
                  << "Input colour expected in RGB space with each channel in"
                     " [0, 1] interval.\nusage: clerp r1 g1 b1 r2 g2 b2\n";
}
