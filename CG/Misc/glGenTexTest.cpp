// g++ -Wall -g -O0 -std=c++11 glGenTexTest.cpp -lglfw -lGL

#include <GLFW/glfw3.h>
#include <iostream>
#include <functional>
#include <stdexcept>
#include <vector>
#include <iterator>

template <typename UninitFuncType,
		  typename SuccessValueType,
		  SuccessValueType successValue>
class RAIIWrapper
{
public:
	template <typename InitFuncType, typename... Args>
	RAIIWrapper(InitFuncType fpInitFunc,
				UninitFuncType fpUninitFunc,
				std::string errorString,
				const Args&... args) :
		fpUninit(fpUninitFunc)
	{
		if (successValue != fpInitFunc(args...))
			throw std::runtime_error(errorString);
	}

	~RAIIWrapper()
	{
        fpUninit();
	}

private:
	std::function<UninitFuncType> fpUninit;
};

using GLFWSystem = RAIIWrapper<decltype(glfwTerminate),
                               decltype(GL_TRUE),
                               GL_TRUE>;

int main(int argc, char **argv)
{
    if (argc < 3) {
        std::cerr << "Insufficient args\n";
        return -1;
    }
	GLFWSystem glfw(glfwInit,
					glfwTerminate,
					"Failed to initialize GLFW");
	glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    auto *win = glfwCreateWindow(500, 500, "OpenGL Textures", NULL, NULL);
    if (!win)
        std::cerr << "Failed to create GLFW window\n";
    glfwMakeContextCurrent(win);

	GLuint texID = atoi(argv[1]);
    glGenTextures(1, &texID);
    std::cout << texID << " "
              << (glIsTexture(texID) ? "is" : "isn't") << " a valid texture\n";
    // glGenTextures isn’t enough, texture bind is when the ID gets associated
	glBindTexture(GL_TEXTURE_2D, texID);
    std::cout << texID << " "
              << (glIsTexture(texID) ? "is" : "isn't") << " a valid texture\n";
    std::vector<GLuint> unusedTexs(atoi(argv[2]));
	glGenTextures(unusedTexs.size(), unusedTexs.data());
    std::ostream_iterator<GLuint> itr(std::cout, ", ");
	copy(unusedTexs.cbegin(), unusedTexs.cend(), itr);
}
