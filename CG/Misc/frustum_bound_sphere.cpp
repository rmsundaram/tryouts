#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>
#include <iostream>
#include <glm/gtc/type_ptr.hpp>

// David H Eberly's code to compute the frustum bounding sphere
// http://www.gamedev.net/topic/604797-minimum-bounding-sphere-of-a-frustum/#entry4827954
void ComputeMinimumVolumeSphereOfFrustum (const float eye[3],
                                          const float direction[3],
                                          const float dmin,
                                          const float dmax,
                                          const float rmax,
                                          const float umax,
                                          float center[3],
                                          float& radius)
{
    float invDMin = 1.0f / dmin;
    float u = umax * invDMin;
    float r = rmax * invDMin;
    float u2pr2 = u * u + r * r;
    // here s isn't the Z coordinate of centre but the scaling factor of dir
    float s = 0.5f * (dmin + dmax) * (1.0f + u2pr2);
    std::cout << "s = " << s << '\n';
    if (s >= dmax)
    {
        s = dmax;
        radius = dmax * sqrt(u2pr2);
    }
    else
    {
        float diff = 1.0f - s / dmax;
        radius = dmax * sqrt(diff * diff + u2pr2);
    }
    for (int i = 0; i < 3; ++i)
    {
        center[i] = eye[i] + s * direction[i];
    }
}

int main()
{
    constexpr auto near = 50.0f;
    constexpr auto far = 5000.0f;

    constexpr auto aspect_ratio = 1.333f, FoV_vert = 0.78539816339f;
    constexpr auto focal_length = 1.0f / tan(FoV_vert);
    constexpr auto inv_focal_length = 1.0f / focal_length;
    constexpr auto FoV_hori = atan2(aspect_ratio, focal_length);
    glm::vec3 const view_window_right_top{aspect_ratio, 1.0f, -focal_length};

    // using similar triangular prism ratios
    constexpr float near_top = near / focal_length;
    constexpr float near_right = aspect_ratio * near_top; // near * aspect_ratio / focal_length
    constexpr float far_top = far / focal_length;
    constexpr float far_right = aspect_ratio * far_top;

    // using angles and trigonometric function
    float const near_top1 = near * tan(FoV_vert);
    float const near_right1 = near * tan(FoV_hori);
    float const far_top1 = far * tan(FoV_vert);
    float const far_right1 = far * tan(FoV_hori);

    std::cout << "PARAMETERS\n";
    std::cout << "Near right top = {" << near_right << ", " << near_top << ", " << -near << "}\n";
    std::cout << "Far right top = {" << far_right << ", " << far_top << ", " << -far << "}\n";
    std::cout << "Near right top = {" << near_right1 << ", " << near_top1 << ", " << -near << "}\n";
    std::cout << "Far right top = {" << far_right1 << ", " << far_top1 << ", " << -far << "}\n";

    // method 1
    glm::vec3 const eye{0.0f, 0.0f, 0.0f};
    float cent[3] = {};
    float rad = 0.0f;
    float dir[3] = {0.0f, 0.0f, -1.0f};
    std::cout << "\nMETHOD 1\n";
    // the reason for sending near and far un-negated is that dir, which is scaled by near and far, is set to −k vector
    ComputeMinimumVolumeSphereOfFrustum(glm::value_ptr(eye), dir, near, far, near_right, near_top, cent, rad);
    std::cout << "Centre = " << cent[0] << ", " << cent[1] << ", " << cent[2] << '\n';
    std::cout << "Radius = " << rad << '\n';

    // method 2 - Dave's method takes only near, far, near_right and near_top and uses them to compute
    // far_right and far_top. Using near_right_top and far_right_top, it then computes the centre.
    // Another way to do it would be to use the third similar triangular prism constituted by the view window.
    // Its right = aspect ratio, height = 1, depth = focal length
    constexpr float fl_sqr = focal_length * focal_length;
    // constexpr float s = ((1.0f + fl_sqr + (aspect_ratio * aspect_ratio)) * (near + far)) / (-2.0f * fl_sqr);
    constexpr float s = -0.5f * (inv_focal_length * inv_focal_length) * ((1.0f + fl_sqr + (aspect_ratio * aspect_ratio)) * (near + far));
    static_assert(s < 0.0f, "Z coordinate of centre non-negative!");
    std::cout << "\nMETHOD 2\n";
    std::cout << "s = " << s << '\n';
    glm::vec3 const far_rt{far_right, far_top, -far};
    // if |s| < |f|, then pick the calculated s, else pick -far
    glm::vec3 const centre{0.0f, 0.0f, (-s <= far) ? s : -far}; // this assumes that s will turn out negative
    std::cout << "Centre = " << centre[0] << ", " << centre[1] << ", " << centre[2] << '\n';
    std::cout << "Radius = " << glm::length(far_rt - centre) << '\n';
    // although we compute the length here, in the actual code length² may be used

    std::cout << "\nMETHOD 3\n";
    // method 3 - RTCD §4.3.2 Computing Bounding Sphere
    // the midpoint of farthest points of the set (frustum in our case) is taken to be the centre
    // since we know that the diagonally separated points would be the farthest, we omit MostSeparatedPointsOnAABB
    // and measure the two possiblilties for their distance of separation
    glm::vec3 const near_lb{-near_right, -near_top, -near};
    auto const len1 = glm::length(near_lb - far_rt);
    glm::vec3 const far_lb{-far_right, -far_top, -far};
    auto const len2 = glm::length(far_lb - far_rt);
    std::cout << "Distance b/w near left bottom & far right top = " << len1 << '\n';
    std::cout << "Distance b/w far left bottom & far right top = " << len2 << '\n';

    auto const radius = 0.5f * ((len1 >= len2) ? len1 : len2);
    auto const centre1 = 0.5f * ((len1 >= len2) ? (near_lb + far_rt) : (far_lb + far_rt));
    std::cout << "Centre = " << centre1[0] << ", " << centre1[1] << ", " << centre1[2] << '\n';
    std::cout << "Radius = " << radius << '\n';
    glm::vec3 const corners[] = {
                                     {-near_right, -near_top, -near}, // near - left bottom
                                     { near_right, -near_top, -near}, // near - right bottom
                                     { near_right,  near_top, -near}, // near - right top
                                     {-near_right,  near_top, -near}, // near - left top
                                     {-far_right, -far_top, -far}, // far - left bottom
                                     { far_right, -far_top, -far}, // far - right bottom
                                     { far_right,  far_top, -far}, // far - right top
                                     {-far_right,  far_top, -far}, // far - left top
                                };
    for (auto const &v : corners)
        std::cout << glm::length(v - centre1) << '\n';
}
