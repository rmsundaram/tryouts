// shortest distance between two skew lines in 3D

#include <glm/glm.hpp>
#include <glm/gtc/epsilon.hpp>
#include <iostream>

struct Line3D {
    glm::vec3 pt;
    glm::vec3 dir;              // not necessarily normalized
};

/*
 * REFERENCES
 * 1. Distances overview, Math 21a (Multivariable Calculus)
 *    http://math.harvard.edu/~ytzeng/worksheet/distance.pdf
 *    Very good summarization of distance calulcation between points, lines,
 *    parallel planes, point-line and point-plane with proof and example.
 * 2. §1.14.8, Geometry for Computer Graphics, John Vince
 * 3. http://members.tripod.com/~Paul_Kirby/vector/Vclose.html
 * 4. http://www.netcomuk.co.uk/~jenolive/skew.html
 *    Explains the same equation from a different viewpoint; the computed normal
 *    is seen as the normal to two parallel planes i.e. same normal but
 *    different points passing though which are the line's points. Now the plane
 *    offset/distance from the origin for P1 and P2 would be along the same
 *    normal; hence their offset difference gives the shortest distance
 *    (a − b) · n = (a · n) − (b · n)
 * 5. §13.5, Equations of Lines and Planes, Math 231
 *    http://www.math.ucsd.edu/~y1zhao/2009Math231/Sec1305.pdf
 *    Same as above with illustrations of the planes and normal.
 */
float dist1(Line3D const &l1, Line3D const &l2) {
#if 0
    /*
     * we know that the shortest distance would be along the perpendicular to
     * both lines. Cross the direction vectors to get the normal i.e. direction
     * of this perpendicular line. Now construct a vector between the points on
     * the lines; its projection on the normal would be the shortest distance.
     */
    auto const normal = glm::normalize(glm::cross(l1.dir, l2.dir));
    return glm::abs(glm::dot(l1.pt - l2.pt, normal));

#else  // handles parallel lines case too

    auto normal = glm::cross(l1.dir, l2.dir);
    auto const n_len = glm::length(normal);
    if (glm::epsilonEqual(n_len, 0.0f, 0.001f)) {
        // lines are parallel, just take a point from one of the lines and do a
        // point-line distance to the other
        auto const v = l2.pt - l1.pt;
        auto const parallel = l1.dir * (glm::dot(v, l1.dir) /
                                        glm::dot(l1.dir, l1.dir));
        auto const perp = v - parallel;
        return glm::length(perp);
        // another alternative way of doing this is given in distance.pdf
        // glm::length(glm::cross(v, l1.dir)) / glm::length(l1.dir);
        // which is nothing but area of parallelogram / base = height =
        // distance between point and line but requires two sqrt calls.
    }
    else {
        return glm::abs(glm::dot(l1.pt - l2.pt, normal) / n_len);
    }
#endif
}

// REFERENCES
// 1. http://geomalgorithms.com/a07-_distance.html
// 2. §2.6.2, 3D Game Engine Design, 1st Edition, David Eberly
// 3. §5.1.2, Mathematics for 3D Game Programming and Computer Graphics,
//    3rd Edition, Eric Lengyel
// This method has the advantage of finding the closest points too; given by sc
// and tc on lines l1 and l2 respectively.
float dist2(Line3D const &l1, Line3D const &l2) {
    auto const w = l1.pt - l2.pt;
    auto const
        a = glm::dot(l1.dir, l1.dir),
        b = glm::dot(l1.dir, l2.dir),
        c = glm::dot(l2.dir, l2.dir),
        d = glm::dot(l1.dir, w),
        e = glm::dot(l2.dir, w);
    float const D = a * c - b * b;
    float sc = (b * e - c * d) / D,
          tc = (a * e - b * d) / D;
    auto const dP = w + (sc * l1.dir) - (tc * l2.dir);
    return glm::length(dP);
}

int main() {
    Line3D const l1{{2, 1, 4}, {-1, 1, 0}}, l2{{-1, 0, 2}, {5, 1, 2}};
    std::cout << dist1(l1, l2) << '\n'
              << dist2(l1, l2) << '\n';

    Line3D const l3{{1, 2, 2}, {4, 3, 2}}, l4{{1, 0, -3}, {4, -6, -1}};
    std::cout << dist1(l3, l4) << '\n'
              << dist2(l3, l4) << '\n';
}
