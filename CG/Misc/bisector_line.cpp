// workout to verify line defined by its ⊥ bisector
// discussed in §9.2.3 of 3D Math Primer, 2/e

#include <iostream>

struct vec2
{
	vec2 operator+(vec2 that)
	{
		that.v[0] += this->v[0];
		that.v[1] += this->v[1];
		return that;
	}

	vec2 operator-(vec2 that)
	{
		that.v[0] = this->v[0] - that.v[0];
		that.v[1] = this->v[1] - that.v[1];
		return that;
	}

	vec2& operator=(vec2 &that)
	{
		if (this != &that)
		{
			v[0] = that.v[0];
			v[1] = that.v[1];
		}
		return *this;
	}

	vec2 operator/(float s)
	{
		return vec2{{v[0] / s, v[1] / s}};
	}

	float v[2];
};

float dot(const vec2 &left, const vec2 &right)
{
	return ((left.v[0] * right.v[0]) + (left.v[1] * right.v[1]));
}

std::ostream& operator<<(std::ostream &os, const vec2 &vec)
{
	return os << vec.v[0] << ", " << vec.v[1];
}

void print_line_eq(const vec2 &normal, float distance)
{
	if (normal.v[0])
		std::cout << normal.v[0] << "x";
	if (normal.v[1])
	{
		if (normal.v[1] > 0)
			std::cout << "+";
		std::cout << normal.v[1] << "y";
	}
	std::cout << " = " << distance;
}

int main()
{
	vec2 q = {2, 2}, r = {6, 6};
	auto normal = r - q;
	auto midpoint = (q + r) / 2;
	auto distance = dot(midpoint, normal);
	std::cout << "Line equation: ";
	print_line_eq(normal, distance);
}
