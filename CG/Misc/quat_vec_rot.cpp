/*
 * Calculating the quaternion that rotates from V₁ to V₂
 *
 * NAIVE METHOD:
 * Consider two unit vectors V₁ and V₂. To determine the axis and angle of rotation from V₁ to V₂,
 * the obvious method is to use the dot and cross products. The cross product gives the axis
 * (non-unit vector) and the dot product would give the cosine of the angle between them; acos would
 * give the angle.
 *
 *     V₁ · V₂ = ‖V₁‖ ‖V₂‖ cos θ; V₁ × V₂ = ‖V₁‖ ‖V₂‖ sin θ A, where A is the unit axis of rotation
 *     V₁ · V₂ = cos θ; V₁ × V₂ = sin θ A                     (∵ ‖V₁‖ ‖V₂‖ = 1)
 *     θ = acos(V₁ · V₂)
 *     quat = [ cos(θ½), sin(θ½) A ]                          (∵ V₁ × V₂ is sin θ times unit vector A)
 *          = [ cos(θ½), (sin(θ½) / sin θ) <V₁ × V₂> ]
 *
 * This involves calling acos, cos once and sin twice.
 *
 * DOUBE QUATERNION:
 * However, if we use the results of those products as-is to construct a quaternion, we'd get a
 * quaternion that would rotate by 2θ.
 *
 *     double_quat = q2 = [V₁ · V₂, V₁ × V₂]
 *
 * HALF-WAY VECTOR METHOD:
 * Adding V₁ and V₂ would give the bisector vector that lies half-way between V₁ and V₂ i.e. angle
 * between V₁ and the bisector would be θ½. Normalizing this vector would give the unit bisector B.
 *
 *     V₁ · B = cos θ½; V₁ × B = sin(θ½) A, where A is the unit axis of rotation
 *     quat = [ cos(θ½), sin(θ½) A ]
 *          = [ V₁ · B, V₁ × B ]
 *
 * This method is definitely better than the naive method since its only costly operation is the
 * normalization of B.
 *
 * HALF-WAY QUATERNION METHOD:
 * Similar to finding the half-way vector, if there's a method that would find a half-way quaternion,
 * that may be employed to deduce the required quaternion. The identity quaternion represents 0 rad
 * rotation:
 *
 *      i = [1, <0, 0, 0>]
 *      cos θ½ = 1 ⇒ θ½ = acos(1) = 0 ⇒ θ = 0
 *      half_quat = q2 + i                              (the quaternion that lies between q2 and i)
 *      quat = normalize(half_quat)
 *
 * Thus adding the identity quaternion to a quaternion q lerps halfway between 0 and θ rad rotation. The
 * resulting half-way quaternion, like the half-way vector, wouldn't be normalized unlike the previous
 * solution; hence normalize it to get the final quaternion. This may be slightly more efficient than
 * the half-way vector solution since it's lacking a few operations (vector addition).
 *
 * As noted by Joseph Thomson in stackoverflow.com, when finding the half-way quaternion, as is also
 * the case with vectors, the quaternions must have the same magnitude, otherwise the result will be
 * skewed towards the quaternion with the larger magnitude. In the above double quaternion workout
 * had V₁ and V₂ been non-unit vectors, it can be seen that double_quat would be scaled by ‖V₁‖ ‖V₂‖
 * since both the dot and cross product would've been scaled by the same factor. Thus to find the
 * half-way qauternion, the identity has to be scaled to the length of double_quat before addition.
 *
 * REFERENCES:
 * §5.5.11 Shortest Path of Rotation, Essential Math
 * http://stackoverflow.com/a/11741520/183120
 * http://www.gamedev.net/topic/429507-finding-the-quaternion-betwee-two-vectors/?p=3856228#entry3856228
 * http://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/transforms/halfAngle.htm
 * http://www.euclideanspace.com/maths/algebra/realNormedAlgebra/quaternions/transforms/minorlogic.htm
 * http://www.euclideanspace.com/maths/algebra/vectors/angleBetween/index.htm
 * https://lolengine.net/blog/2013/09/18/beautiful-maths-quaternion-from-vectors
 * http://codereview.stackexchange.com/a/74997/29287
 */

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtx/quaternion.hpp>
#include <iostream>

// when v1 = v2, both methods would return the identity quaternion

// Half-way Quaternion Method without input vector length assumptions
glm::quat v1_to_v2_rot(glm::vec3 const &v1, glm::vec3 const &v2)
{
    auto const nonunit_axis = glm::cross(v1, v2);
    auto const double_angle = glm::dot(v1, v2);
    if (double_angle < -1.f + 0.0001f)  // vectors are at straight angle (180°)
    {
        const glm::vec3 X{1.0f, 0.0f, 0.0f};
        const glm::vec3 Y{0.0f, 1.0f, 0.0f};
        glm::vec3 const other = (std::abs(glm::dot(v1, X)) < 1.0f) ? X : Y;
        // cos π/2 = 0
        return glm::quat(0.0f, glm::normalize(glm::cross(v1, other)));
    }
    // Slightly slower and unstable method
    // auto const identity_scale = glm::sqrt(glm::length2(v1) * glm::length2(v2));
    // auto q = glm::quat(identity_scale + double_angle, nonunit_axis);
    // glm::length(q) gives identity_scale too but is stabler and cheaper
    auto q = glm::quat(double_angle, nonunit_axis);
    auto const identity_scale = glm::length(q);
    q.w += identity_scale;
    return glm::normalize(q);
}

// Half-way Quaternion Method; requires unit vector inputs
glm::quat v1_to_v2_rotU(glm::vec3 const &v1, glm::vec3 const &v2)
{
    auto const cos_theta = glm::dot(v1, v2);
    // though main branch can handle parallel case branch to avoid normalize
    if (cos_theta >= 1.0 - 0.0001f)  // v1 ∥ v2
      return glm::identity<glm::quat>();
    else if (cos_theta < (-1.f + 0.0001f))  // vectors at straight angle (180°)
    {
        // v1 × v2 would be 0 vector; just pick any vector ⟂ to v1
        const glm::vec3 X{1.0f, 0.0f, 0.0f};
        const glm::vec3 Y{0.0f, 1.0f, 0.0f};
        glm::vec3 const& other = (std::abs(glm::dot(v1, X)) < 1.0f) ? X : Y;
        return glm::quat(/*cos π/2*/ 0.0f,
                         glm::normalize(glm::cross(v1, other)));
    }
    auto constexpr cos_zero = 1.0f;
    auto const cos_half_theta = cos_zero + cos_theta;
    auto const nonunit_axis = glm::cross(v1, v2);
    auto q = glm::quat(cos_half_theta, nonunit_axis);
    return glm::normalize(q);
    // Following may be slightly more optimised; avoids length² calculation but
    // doesn’t handle zero vectors.  Two zero vector checks and vector scale
    // operations may spend the saved cycles; readability isn’t great either.
    //
    // if (is_zero(A) || is_zero(B)) {
    //     glm::identity<glm::quat>();
    // } else {
    //     const float s = std::sqrt(2.0f * cos_half_theta);
    //     const float is = 1.0f / s;
    //     glm::quat(0.5 * s, is * axis);
    // }
}

// Half-way Vector Method; requires unit vector inputs
glm::quat v1_to_v2_rotUHV(glm::vec3 const &v1, glm::vec3 const &v2)
{
    // TODO: floating-point equality comparison should be avoided
    if (v1 == -v2)                // vectors are at straight angle (180°)
    {
        const glm::vec3 X{1.0f, 0.0f, 0.0f};
        const glm::vec3 Y{0.0f, 1.0f, 0.0f};
        glm::vec3 const other = (std::abs(glm::dot(v1, X)) < 1.0f) ? X : Y;
        // cos π/2 = 0
        return glm::quat(0.0f, glm::normalize(glm::cross(v1, other)));
    }
    auto const b = glm::normalize(v1 + v2);
    return glm::quat(glm::dot(v1, b), glm::cross(v1, b));
}

int main()
{
    glm::vec3 const v1(1.0f, 0.0f, 0.0f), v2(0.0f, 1.0f, 0.0f);
    auto q1 = v1_to_v2_rotU(v1, v2);
    auto q2 = v1_to_v2_rotUHV(v1, v2);
    std::cout << "Half-way quaternion method: " << glm::to_string(q1) << '\n';
    std::cout << "Half-way vector method    : " << glm::to_string(q2) << '\n';
    std::cout << "Using glm::rotation()     : "
              << glm::to_string(glm::rotation(v1, v2)) << '\n';

    glm::vec3 const v3(1.0f, 0.0f, 0.0f), v4(-1.0f, 0.0f, 0.0f);
    q1 = v1_to_v2_rotU(v3, v4);
    q2 = v1_to_v2_rotUHV(v3, v4);
    std::cout << "Half-way quaternion method: " << glm::to_string(q1) << '\n';
    std::cout << "Half-way vector method    : " << glm::to_string(q2) << '\n';
    std::cout << "Using glm::rotation()     : "
              << glm::to_string(glm::rotation(v3, v4)) << '\n';
}
