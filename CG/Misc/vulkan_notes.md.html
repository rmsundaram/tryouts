<meta charset="utf-8">

          **Vulkan**
    *Digest of Vulkan Guide*

# Introduction

* Spec implemented by many devices including PC, consoles, hand-helds, etc.
  - Same API irrespective of device type
* Low-level API
* Multi-threading aware
* More direct; less load on driver, more work for the application (programmer)

!!! Tip: When to use
    Use when you want multi-threading (large worlds, many objects, manual control over memory/pipeline, etc.).  Do not use when your bottleneck is GPU-bound.

Validation Layers are useful for catching errors such as using incorrect configurations, using wrong enums, synchronization/object lifetime issues.  Validation Layers work by intercepting your function calls and performs validation on the data. If all data is correctly validated a call to the driver will be performed.  Validation comes with a performance cost.

While OpenGL and D3D 11 created internal objects/state as needed _on-the-fly_, this isn’t done in Vulkan as it’s very explicit.  You create them and decide whether they’re worth caching to avoid recreation.  Most states -- even seemingly internal -- are created and maintained by the application.  Optimal pipeline setup will boost performance as less work is actually done during rendering.  Expensive objects like pipelines are better created in the background threads.

Actual GPU commands are recorded into a command buffer and submitted to a queue.  You can submit to different queues, if available; they may run in parallel.  Their execution can be controlled.

Vulkan has no concept of a frame; how you render is entirely up to you.  Display of frames on screen is done through a swapchain; you do not necessarily have to display anything at all ([headless][]).

!!! Warning: Dynamic rendering makes `VkRenderPass` and `VkFrameBuffer` less relevant
   Vulkan 1.3 (2022 Jan) introduced dynamic rendering and seems to have rendered these verbose APIs less important; [refer][vkrenderpass-deprecated].


[headless]: https://www.saschawillems.de/blog/2017/09/16/headless-vulkan-examples/
[vkrenderpass-deprecated]: https://lesleylai.info/en/vk-khr-dynamic-rendering/


# Notable Vulkan Objects

* `VkInstance`: Vulkan API context
* `VkPhysicalDevice`: interface to an actual GPU
  - Query info like VRAM, queues, etc.
* `VkDevice`: logical GPU where workloads are submitted
  - Created from a `VkPhysicalDevice` or a (single-vendor) _device group_; implemented by driver
  - List of extensions can be loaded during creation
  - Multiple device creation is allowed
* `VkPipeline`: holds GPU states required for a pipeline
* `VkBuffer`: chunk of GPU memory
* `VkImage`: texture for read-write
* `VkCommandBuffer`: encodes GPU commands (everything else is talking to the driver through API calls (CPU))
* `VkQueue`: execution “port” for commands; acceptable commands vary between queues
* `VkDescriptorSet`: binding info of shader inputs ⇿ data
* `VkSwapchainKHR`: extension providing on-screen frames
  - Platform/GPU-dependant
  - Requires re-creation on window resize
  - _Presentation mode_ decides how images in chain are presented
* `VkSemaphore`: Synchronizes between GPU to GPU commands
* `VkFence`: Synchronizes between GPU to CPU commands e.g. command buffer finish on GPU

!!! Note: Direct API
    Since Vulkan is a stateless/direct API, most calls need `VkDevice` passed; some require `VkInstance` for meta stuff like validation layers, instance extensions (like `VK_KHR_surface`), logger, etc.

!!! Warning: Validation layers availability
    Validation layers aren’t built-in.  LunarG Vulkan SDK provides a set of validation layers (open-source) checking for common errors; [refer][vt-validation-layers].  It’s available only if installed (`vulkan-validation-layers` on ArchLinux).

One can directly link to `libvulkan.so.1` or dynamically load it; either ways you’ve to load function pointers.  [volk][] is a handy loader with extensions support.


[volk]: https://github.com/zeux/volk
[vt-validation-layers]: https://vulkan-tutorial.com/Drawing_a_triangle/Setup/Validation_layers

# Command buffer and Queue

* Every device supports a set of queue families
  - Varies drastically b/w devices (e.g. server-class vs mobile GPUs)
* `VkCommandPool`s are associated to queues
* `VkCommandBuffer`s are allocated by a pool
  - Secondary level buffers enable recording commands for a single pass from different threads
* Commands can be recorded once every frame or be cached and submitted
  - Recording commands is relatively cheap; `vkQueueSubmit()` is costlier due to command buffer validation
* Command buffers have states
  - Starts in _Ready_
  - Enters _Recording_ by `vkBeginCommandBuffer`
  - Ends up with _Executable_ state upon `vkEndCommandBuffer`
  - Submitted ones are in _Pending_ state
  - Reset (& reuse) only after done executing
    + Commonly double-buffered hence to continue drawing next frame
* `vkQueueSubmit()` isn’t thread-safe
  - Common to have one (background) thread submit


# Task and Mesh Shaders

When you want to procedurally generate your meshes, cull meshlets before even they hit later pipeline stages, have dynamic LOD, etc. then use these stages which skips input assembly and vertex shader; it directly feeds into the rasterization stage of the pipeline.

Refer the excellent [Mesh Shading Series][mesh-shaders] on this topic.


[mesh-shaders]: https://chaoticbob.github.io/2024/01/24/mesh-shading-part-1.html


# References

1. [Vulkan Guide][vulkan-guide]
2. [Vulkan Tutorial][vulkan-tutorial]


[vulkan-guide]: https://vkguide.dev/docs/new_chapter_1/vulkan_command_flow/
[vulkan-tutorial]: https://vulkan-tutorial.com/


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js" charset="utf-8"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js" charset="utf-8"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none' };</script>
