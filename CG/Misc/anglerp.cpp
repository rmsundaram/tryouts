#include <iostream>
#include <cmath>

// hue/angle interpolation
// http://stackoverflow.com/a/33347522

// http://stackoverflow.com/a/2021986
// Normalize angle, without mod, to arbitrary interval assuming it wraps around
// when going below min or above max. Just removing the floor, will make this
// work for integral types too, since int division is already floored division.
template <typename T>
T normalise_angle(T value, T start, T end)
{
    T const width = end - start;
    // value relative to 0
    value -= start;

    // + start to reset back to start of original range
    return (value - (floor(value / width) * width)) + start;
}

// additional workout for the RGB HSV colour interpolation workout
int main(int argc, char **argv) {
    if (argc > 2) {
        auto const a1 = strtof(argv[1], nullptr);
        auto const a2 = strtof(argv[2], nullptr);
        /*
         *      TOTAL CASES
         *
         *  Δ |  ≤ 180  |  > 180
         * ---|---------|---------
         *  + |  40, 60 | 310, 10
         *  − |  60, 40 | 10, 310
         *
         * if Δ = 180 then both +/− rotation are valid options
         */
        auto constexpr def_steps = 3.0f;
        auto const steps = (argc > 3) ? strtof(argv[3], nullptr) : def_steps;
        auto const d = a2 - a1;
        // rotation from a1 to a2 is counterclockwise if d is positive
        // below line is nothing but d + (|d| > 180) ? ((d > 0) ? −360 : 360) : 0
        float const delta = (d + (floor(abs(d) / 180.0f) * -copysign(360.0f, d)))
                           / (steps + 1.0f);
        // steps is the number of stops between the end points
        std::cout << a1 << '\n';
        for (auto i = 1.0f; i <= steps; ++i) {
            // angle normalization is required as resulting angle may be ≥ 360
            // or < 0 e.g. test cases:
            // anglerp 310 10 10
            // anglerp 10 310 10
            std::cout << normalise_angle(a1 + (delta * i), 0.0f, 360.0f) << '\n';
        }
        std::cout << a2 << '\n';
    }
    else
        std::cerr << "Interpolate angle (in degrees)\n"
                  << "usage: anglerp ANGLE1 ANGLE2 [STEPS]\n";
}
