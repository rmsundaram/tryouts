// generate 2d vertices with a 1d loop
#include <iostream>

int main()
{
    constexpr auto rows = 2, cols = 5;
    constexpr auto vert_v = rows * cols;
    int x = 0, y = -1;
    for (auto i = 0; i < vert_v; ++i)
    {
        y = (y + 1) % cols;
        x = i / cols;
        std::cout << x << ", " << y << '\n';
    }
}
