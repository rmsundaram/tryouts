// g++ -Wall -std=c++11 -pedantic -IC:/Work/libs/glm vec3_map.cpp

#include <glm/glm.hpp>
#include <glm/gtc/epsilon.hpp>
#include <map>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <functional>

/*
lexicographical comparison of vector a and b

if (a[0] < b[0])
    return true;
else if (a[0] == b[0])
{
    if (a[1] < b[1])
        return true;
    else if (a[1] == b[1])
        return a[2] < b[2];
}
return false;

this is however agnostic of floating-point comparison perils;
the below implementation remedies that
*/

template <typename T>
typename std::enable_if<std::is_floating_point<T>::value, bool>::type
float_less(T f1, T f2)
{
    return (f1 < f2) && glm::epsilonNotEqual(f1, f2, glm::epsilon<T>());
}

template <typename T>
struct VecComparer
{
    using FT = typename T::value_type;
    bool operator()(const T &a, const T &b)
    {
        return std::lexicographical_compare(&a[0], &a[0] + a.length(),
                                            &b[0], &b[0] + b.length(),
                                            float_less<FT>);
    }
};

int main()
{
    const auto g = glm::golden_ratio<float>();
    glm::vec3 vertices[] = {
                                 glm::vec3{-1,  g,  0},     // 00
                                 glm::vec3{ 1,  g,  0},     // 01
                                 glm::vec3{-1, -g,  0},     // 02
                                 glm::vec3{ 1, -g,  0},     // 03
                                 glm::vec3{ 0, -1,  g},     // 04
                                 glm::vec3{ 0,  1,  g},     // 05
                                 glm::vec3{ 0, -1, -g},     // 06
                                 glm::vec3{ 0,  1, -g},     // 07
                                 glm::vec3{ g,  0, -1},     // 08
                                 glm::vec3{ g,  0,  1},     // 09
                                 glm::vec3{-g,  0, -1},     // 10
                                 glm::vec3{-g,  0,  1}      // 11
                           };
    std::map<glm::vec3, size_t, VecComparer<glm::vec3>> m;
    std::cout << std::boolalpha;
    std::cout << m.emplace(vertices[0], m.size()).second << '\n';
    std::cout << m.emplace(vertices[11], m.size()).second << '\n';
    std::cout << m.emplace(vertices[5], m.size()).second << '\n';
    std::cout << m.emplace(vertices[0] + vertices[11], m.size()).second << '\n';
    std::cout << m.emplace(vertices[11] + vertices[5], m.size()).second << '\n';
    std::cout << m.emplace(vertices[5] + vertices[0], m.size()).second << '\n';
    std::cout << m.emplace(vertices[0], m.size()).second << '\n';                // should be false
    std::cout << m.emplace(vertices[5], m.size()).second << '\n';                // should be false
    std::cout << m.emplace(vertices[1], m.size()).second << '\n';
    std::cout << m.emplace(vertices[0] + vertices[5], m.size()).second << '\n';  // should be false
    std::cout << m.emplace(vertices[5] + vertices[1], m.size()).second << '\n';
    std::cout << m.emplace(vertices[1] + vertices[0], m.size()).second << '\n';
    std::cout << m.size() << std::endl;
}
