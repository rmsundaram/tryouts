#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>
#include <iostream>

struct Circle
{
    glm::vec2 centre;
    float radius;
};

bool circle_within_circle(const Circle &A, const Circle &B)
{
    if (A.radius > B.radius)
        return false;

    // fully contained if (A.radius + ‖A.centre - B.centre‖) <= B.radius
    // but the √ in glm::length may be avoided thus: a + b ≤ c
    // a ≤ c - b
    // c - a ≥ b    i.e. distance betweem the centres ≤ difference in radius
    // squaring both sides shouldn't affect the result since both are positive numbers
    // we've already verified that c >= a, so c - a should be positive
    // The rules for squaring inequalities are:
    // If a > b and |a| > |b|, then a² > b²
    // If a > b and |a| < |b|, then a² < b²
    const auto r_diff = B.radius - A.radius;
    return (r_diff * r_diff) >= glm::length2(A.centre - B.centre);
}

int main()
{
    Circle A{{0.0f, 0.0f}, 3.0f}, B{{-1.5f, 0.0f}, 1.0f};
    std::cout << std::boolalpha << circle_within_circle(B, A) << '\n';
}
