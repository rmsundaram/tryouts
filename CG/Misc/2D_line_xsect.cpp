#include <glm/glm.hpp>
#include <glm/gtc/epsilon.hpp>
#include <glm/gtx/string_cast.hpp>
#include <cassert>
#include <iostream>

struct Line
{
    glm::vec2 pt;
    glm::vec2 dir;
};

// this method is using the parametric line equation; 3D Math Primer has a method (in §A.7) using the implicit line
// equation Ax + By = C which is solving two simultaneous equations with two unknowns x, y; if this can be solved
// then they intersect. It seems more optimized; however it needs the line to be represented using normal and distance
// from origin as opposed to the one used here
glm::vec2 line_line_xsect(Line const &l1, Line const &l2)
{
    /*
     *  L₁(t) = A + tB
     *  L₂(s) = C + sD
     *  If they intersect, they have atleast a common point, at that point
     *  L₁(t) = L₂(s); equate and isolate s
     *  A + tB = C + sD ⇒ A − C + tB = sD ⇒ (A − C + tB) / D = s
     *  (A.x − C.x + tB.x) / D.x = s
     *  (A.y − C.y + tB.y) / D.y = s
     *  Equating, reducing and rearranging, we get
     *  t = (D.y (A.x − C.x) − D.x(A.y − C.y)) / (D.x B.y − D.y B.x)
     *  likewise, isolate t and get s
     *  s = (B.y (C.x − A.x) − B.x (C.y − A.y)) / (B.x D.y − B.y − D.x)
     *  is they're linearly dependant (parallel) then the denominators would be 0
     */
    glm::vec2 P;
    float const dt = ((l2.dir.x * l1.dir.y) - (l2.dir.y * l1.dir.x));
    if (glm::epsilonNotEqual(dt, 0.0f, 0.0001f))
    {
        float const t = ((l2.dir.y * (l1.pt.x - l2.pt.x)) - (l2.dir.x * (l1.pt.y - l2.pt.y))) / dt;
        float const s = ((l1.dir.y * (l2.pt.x - l1.pt.x)) - (l1.dir.x * (l2.pt.y - l1.pt.y))) /
                        ((l1.dir.x * l2.dir.y) - (l1.dir.y * l2.dir.x));
        P = l1.pt + (t * l1.dir);
        auto const P_dash = l2.pt + (s * l2.dir);
        auto const eq = glm::epsilonEqual(P, P_dash, 0.0001f);
        assert(eq.x && eq.y);
    }
    else
        P.x = P.y = std::numeric_limits<float>::infinity();
    return P;
}

void print_result(glm::vec2 const &p)
{
    if (std::isfinite(p.x))
        std::cout << glm::to_string(p) << '\n';
    else
        std::cout << "The lines are either parallel or coincident";
}

int main()
{
    Line const l1 { {0.292893218812f, 0.292893218812f}, {-1, 1} };
    Line const l2 { {2, 3}, {-4, 0} };
    Line const l3 { {0, 3}, {1, 0} };
    print_result(line_line_xsect(l1, l2));
    print_result(line_line_xsect(l2, l3));
}
