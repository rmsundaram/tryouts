#include <glm/glm.hpp>
#include <iostream>
#include <glm/gtx/string_cast.hpp>

/*
 * Given a vector v, sometimes we need to find a vector w such that they're
 * linearly independent e.g. for one of the ray−ray intersection test. To find a
 * non-zero vector w orthogonal to v in ℝⁿ, 0 all components of v except two,
 * swap them & negate one e.g. in 3D <x, y, z> would be made <−y, x, 0>. However
 * when codifying this, locking onto z as the zeroed component, it'll lead to a
 * zero vector for one particular case: <0, 0, 1>. This can be proved with any
 * component for all dimensions. This is the hairy ball theorem, famously stated
 * as "you can't comb a hairy ball flat without creating a cowlick", "you can't
 * comb the hair on a coconut", or formally as "every smooth vector field on a
 * sphere has a singular point." LoLEngine's blog[1] speaks of a branch-less
 * version[2] using fract to handle both regular and special cases. Another way
 * is to calculate both <−y, x, 0> and <0, −z, y> and choose the right one using
 * the SIMD intrinsic _mm_blend_ps.
 * [1]: http://lolengine.net/blog/2013/09/21/
 *      picking-orthogonal-vector-combing-coconuts
 * [2]: https://github.com/recp/cglm/blob/master/include/cglm/vec3.h
 */

glm::vec3 ortho(glm::vec3 const &v) {
    return (std::abs(v.x) > std::abs(v.z)) ?
                 glm::vec3(v.y,-v.x, 0.f) :
                 glm::vec3(0.f, v.z,-v.y);
}

/*
 * Erin Catto in Box2D's blog[1] provides something similar but it only works
 * for unit vectors and involves a magic number, which can understood better in
 * 2D. Imagine a point moving along the unit circle's circumference from (1, 0)
 * to (0, 1). As it ascends, the value of the abscissa decreases and the
 * ordinate increases; this goes on until it reaches the point where both are
 * equal at 1⁄√2; this is the point where θ = ¼π rad and sin θ = cos θ. Beyond
 * this too the inc/dec continues till the point reaches (0, 1). However, at all
 * times either the abscissa or the ordinate would've been ≥ 1⁄√2. For any unit
 * vector in n-dimension at least one of the vector's element would be ≥ 1⁄√n;
 * if one is equal then all others should also be equal to it for the vector to
 * be of unit length.
 * [1]: https://box2d.org/posts/2014/02/computing-a-basis/
 */
void ComputeBasis(glm::vec3 const &a, glm::vec3 &b, glm::vec3 &c)
{
    // Suppose unit vector A has all equal components (s, s, s) i.e. 3s² = 1;
    // s = √(1/3) = 0.57735. So a unit 3-vector’s largest component ≥ 0.57735.

    b = (std::abs(a.x) >= 0.57735f) ? glm::vec3(a.y, -a.x, 0.0f) :
                                      glm::vec3(0.0f, a.z, -a.y);
    b = glm::normalize(b);
    c = glm::cross(a, b);
}

int main() {
    glm::vec3 a{1.234f, 3.02f, 45.3423f}, b, c;
    auto const a_prime = ortho(a);
    std::cout << "A · B = " << glm::dot(a, a_prime) << '\n';
    a = glm::normalize(a);
    ComputeBasis(a, b, c);
    std::cout << glm::to_string(a) << '\n'
              << glm::to_string(b) << '\n'
              << glm::to_string(c) << '\n';
}
