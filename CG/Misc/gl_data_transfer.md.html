<meta charset="utf-8">

    **OpenGL ⭤ GLSL Data Transfer**
      *Uploading data to the GPU*

When reading OpenGL functions, refer to OpenGL 4 man pages since only those get bug fixes, the rest are terribly outdated! [[1][]]

[1]: https://stackoverflow.com/q/34198702/#comment56153210_34198702

# Weighted Average

A mathematical concept that often comes up in Computer Graphics programming.  Interpolation is a form of _weighted average_.

It’s similar to normal average but with varying weights for each term i.e. not all terms contribute equally towards final average.  It’s a convex combination; weights add up to 1.

## Example

``` text
Marks of students = { 45, 63, 78, 89, 59 }
Average = 334 ÷ 5 = 66.8
We implicitly assume the weight of the terms here as ⅕ = 0.2
Average = (0.2 * 45) + (0.2 * 63) + (0.2 * 78) + (0.2 * 89) + (0.2 * 59) = 66.8

However, let’s say we take different weights for each student: {0.2, 0.1, 0.4, 0.25, 0.05}
Weighted average = (0.2 * 45) + (0.1 * 63) + (0.4 * 78) + (0.25 * 89) + (0.05 * 59) = 71.7
```

# Normalized Integers

When dealing with floats in the range [0, 1] (unsigned) or [-1, 1] (signed), if it’s okay to trade precision for space, they can be compressed to a smaller integer by bucketization i.e. mapping chunks of floats to one corresponding integer[^fn1]; these integers are [normalized][].  E.g. when `f32`’s [0.0, 1.0] is approximated by `u8`, it’s essentially dividing [all states in between][live-eg-1] by 255 = (2⁸ - 1).  By doing this we compress 4 bytes into 1 byte and save 3 bytes.  In textures, oftentimes saving memory improves performance and the precision loss isn’t obvious visually.

``` rust
fn normalize(f: f32) -> u8 {
    assert!((f >= 0.0) && (f <= 1.0));
    (f * 255.0) as u8
}

fn denormalize(u: u8) -> f32 {
    u as f32 / 255.0
}

fn main() {
    assert_eq!(normalize(0.0), 0);
    assert_eq!(normalize(1.0), 255);

    assert_eq!(denormalize(0), 0.0);
    assert_eq!(denormalize(255), 1.0);
}
```

A good example is 32-bpp colour (8-bit/channel) data is typed `R8G8B8A8_UNORM` using `glVertexAttribPointer`; it is used as `vec4 colour` in the shader where the channel values range from `[0.0f, 1.0f]`; likewise normals are commonly passed as signed, normalized integers using `GL_INT_2_10_10_10_REV` format.

## Example

``` text
0.8 → 204
204 → 0.8

0.455 → 116 (≅ 116.025)
116 → 0.454901960784
```

!!! Warning: Signed normalized integers
    Given a B-bit integer, since OpenGL 4.2 conversion is always done with $f = max(\frac{i}{2^{B-1}-1}, -1.0)$ i.e. [-MAX, MAX] $\mapsto$ [-1, 1] with 0 getting an exact normalized integer representation.  Pre-OpenGL 4.2 uses the alternate formula $f = \frac{2i + 1}{2^B - 1}$ at some instances, notably vertex attribute values; no zero representation, [MIN, MAX] $\mapsto$ [-1, 1].

[^fn1]: precision is lost due to the many-to-one mapping

[live-eg-1]: http://coliru.stacked-crooked.com/a/d6f1efcc0eddcf15
[normalized]: https://www.khronos.org/opengl/wiki/Normalized_Integer

# Attribute Data Type

The type of a vertex attribute -- [qualified][type-qualifiers] by `in` or `attribute` in vertex shader -- is determined by the variant of `glVertexAttribPointer` function used to explain the data in the VBO currently bound to `GL_ARRAY_BUFFER`.  The type of the data residing in the VBO doesn’t matter as OpenGL “sees” them as raw bytes of unstructured data.

| Function                  | Type                  |
|:--------------------------|:----------------------|
| `glVertexAttribPointer`   | `vec`_n_              |
| `glVertexAttribIPointer`  | `ivec`_n_, `uvec`_n_  |
| `glVertexAttribLPointer`  | `dvec`_n_             |

For `glVertexAttribPointer`, everything will be converted to `float` by [promoting][free-promo] the data type.  There’s an option to normalize; if `true`, it’ll treat data as integers and map signed type [−MAX, MAX] → [-1.0, 1.0], unsigned type [0, MAX] → [0.0, 1.0]; if `false`, it’ll convert to floats without normalization.  Other two variants of this function doesn’t do normalization.

For `glVertexAttribIPointer` too, everything smaller than either 32-bit signed or unsigned (`uvec` or `ivec`) will be promoted to 32-bits respectively.

In WebGL 2, there’s no `glVertexAttribLPointer`; WebGL 1 doesn’t have `glVertexAttribIPointer` either.

!!! Note: [GLSL has no < 32-bit types][glsl-no-less-32]
    GLSL only has un/signed integers, floats, doubles, booleans and vectors/matrices of them.  Anything lesser passed into the shaders will get promoted.

[type-qualifiers]: https://www.khronos.org/opengl/wiki/Type_Qualifier_(GLSL)#Shader_stage_inputs_and_outputs
[free-promo]: https://stackoverflow.com/a/11679024/183120
[glsl-no-less-32]: https://stackoverflow.com/a/42743158/183120

# Pixel Transfer

Difference between OpenGL buffer and texture objects is that for buffers the user strictly controls the binary format of stored data, while for textures OpenGL controls its format.  The user tells what format to use, but the specific arrangements of bytes is up to OpenGL.  This allows different hardware to store textures in whatever way is most optimal for accessing them [[2][]].

There’s a conversion of user-provided data and what is actually stored in the GPU.  Hence `glTex*` functions specify both what they input to OpenGL and what they expect out of a GLSL shader.

> "It is good practice for the sake of performance to try to match the texture's format with the format of the data that you upload to OpenGL, so as to avoid conversion."

**Input**: The last three parameters of `glTex*` functions explain user-provided data: format, type and address.  Format is about the number, order and data type (integer or float) of components, type is about the bit width.

**Output**: The third parameter is internal format; it defines the properties of the texels stored in GPU, accessible in a shader.  Only when the input in integer and output expected in shader is a float, then it make sense to pass internal format as normalized; OpenGL will read input data as integers and denormalize them to floats.

[2]: https://paroj.github.io/gltut/Texturing/Tutorial%2014.html

## Texel Data Type

When using desktop or WebGL 2 GLSL `texture()` the return type is written `gvec` (generic) i.e. it’ll depend on the sampler being used.  Older versions have dimension in the function name and a concrete return type e.g. `texture2D()` returns a `vec4`.

# See Also

1. Terrain texturing workout which uses [half float](https://en.wikipedia.org/wiki/Half-precision_floating-point_format) (`float16`) texture coordinates.
2. [OpenGL Wiki: Pixel Transfer](https://www.khronos.org/opengl/wiki/Pixel_Transfer)
2. [OpenGL Wiki: Image Format](https://www.khronos.org/opengl/wiki/Image_Format)
3. [Difference between `GL_R16` and `GL_R16UI`?](https://stackoverflow.com/q/23533749)
4. [`glVertexAttribIPointer` documentation](http://docs.gl/gl4/glVertexAttribPointer)


<link rel="stylesheet" href="https://morgan3d.github.io/markdeep/latest/apidoc.css?">

<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none' };</script>
