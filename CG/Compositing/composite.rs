// Refer https://en.wikipedia.org/wiki/Alpha_compositing#Alpha_blending

fn blend_straight(s: &Vec<f32>, d: &Vec<f32>) -> Vec<f32> {
  // Ra = Sa + Da * (1 - Sa); if Ra == 0, Rrgb = 0
  // Rrgb = ((Srgb * Sa) + (D * Da * (1 - Sa))) / Ra
  //
  // If we know Da == 1, then plugging it into above eqns we get
  // Ra = 1
  // Rrgb = (Srgb * Sa) + (Drgb * (1 - Sa))
  assert!(s.len() == 4);
  assert!(s.len() == d.len());
  let r_a = s[3] + d[3] * (1.0 - s[3]);
  if r_a <= 0.0 {
    return vec![0.0, 0.0, 0.0, 0.0];
  }
  let r_s = s[0..3].iter().map(|c| c * s[3]);
  let r_d = d[0..3].iter().map(|c| c * d[3] * (1.0 - s[3]));
  let mut r: Vec<f32> =
    r_d.zip(r_s).map(|(c_s, c_d)| (c_s + c_d) / r_a).collect();
  r.push(r_a);
  r
}

fn blend_premult(s: &Vec<f32>, d: &Vec<f32>) -> Vec<f32> {
  // R = S + D (1 - Sa)
  assert!(s.len() == 4);
  assert!(s.len() == d.len());
  d.iter()
    .map(|c| c * (1.0 - s[3]))
    .zip(s.iter())
    .map(|(c_d, c_s)| c_s + c_d)
    .collect()
}

fn premultiply(colour: &Vec<f32>) -> Vec<f32> {
  let n = colour.len();
  let alpha = colour[n - 1];
  colour[0..n - 1]
    .iter()
    .map(|&c| c * alpha)
    .chain(colour[(n - 1)..].iter().map(|&c| c))
    .collect()
}

fn main() {
  let src: Vec<f32> = vec![0.0, 0.5, 0.0, 0.5];
  let dst: Vec<f32> = vec![1.0, 0.0, 0.0, 1.0];
  println!(
    "blend({:?}, {:?}) = {:?}",
    src,
    dst,
    blend_straight(&src, &dst)
  );

  println!(
    "blend({:?}, {:?}) = {:?}",
    premultiply(&src),
    premultiply(&dst),
    blend_premult(&premultiply(&src), &premultiply(&dst))
  );
}
