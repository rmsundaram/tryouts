<meta charset="utf-8">

    **Alpha Compositing**

[Alpha Compositing][]
: The process of combining two images, often with partial or full transparency.  The most common way is _source-over_ while other ways exist e.g. _destination-over_, _source/destination-in_, _source/destination-out_, _source/destination-atop_, _xor_ etc.

[Alpha Blending][]
: Combining a translucent foreground colour with a background colour, thereby producing a new colour blended between the two.  Once the resulting image’s matte is deduced, [blend modes][] define how the colours are combined e.g. _additive_, _multiply_, _dodge_, _screen_, etc.  _normal_ is the most used.

[Matte][]
: Image’s coverage information, either as a separate layer or a channel, needed to correctly combine images; makes it possible to distinguish between parts where something is drawn or not.

Composite
: Composited result of multiple layers.

!!! Tip: Alpha Compositing vs Alpha Blending
    Though sounding similar, blending only talks about one pixel’s colour, while compositing is about the whole image.  The output of compositing can be one of [Source, Destionation, Both, Neither]; blend mode specifies the output for the _Both_ case.

[Alpha Compositing]: https://en.wikipedia.org/wiki/Alpha_compositing
[Alpha Blending]: https://en.wikipedia.org/wiki/Alpha_compositing#Alpha_blending
[blend modes]: https://en.wikipedia.org/wiki/Blend_modes
[Matte]: https://en.wikipedia.org/wiki/Matte_(filmmaking)

# Opacity

* A translucent rose-tinted glass filters a decent amount of blue and some green light, while letting through all of red light
* Writing down the math, using the convention that R is the result, D is the destination object/image we’re looking at:

$$
R_R = D_R \times 1.0 \\
R_G = D_G \times 0.7 \\
R_B = D_B \times 0.9
$$

* _Transparency_ of this glass depends on the colour of the incident light
* A regular sunglass allowing 30% of incident light will have all three multipliers to be 0.3
* We could say 30% transparent of 70% opaque
* Though opaque objects are 100% opaque in the real-world while in CG we’ve anti-aliasing even for opaque entities (font rendering)

# Coverage

* Some pixels are only covered partially in rendering
* To deduce coverage notice that all line-square intersection can be decomposed into a rectangle and trapezium
* Their areas can be easily computed; sum of their area / square’s area gives percentage of _coverage_ of a pixel
* This coverage is calculated as an exact number at arbitrary precision
* Rendering a smooth looking line avoiding jaggies means using this coverage and partially illuminating the pixel; [sub-pixel rendering][subpixel]

[subpixel]: https://en.wikipedia.org/wiki/Subpixel_rendering

# Alpha

* alpha = opacity × coverage
* A 60% opaque object, on a pixel with 30% coverage, has 18% alpha in that pixel
* An opaque object with no coverage or a fully transparent object with no coverage both have 0 alpha in that pixel
* Once multiplied, the actual values of opacity and coverage are lost; hence “alpha” synonymous to “opacity”
* Higher the bit-depth of alpha better smoothly varying transparency; 8 bits is common RGBA8888
* Unlike colour components that are often encoded using non-linear transforms, alpha is stored linearly i.e. stored alpha of 0.5 corresponds to 0.5 only
* Alpha, other than blocking a background colour a pixel can also add some colour of its own
* Alpha masks/planes are produced with just the alpha channel of all the pixels of a canvas
* 1.0 (white) means fully opaque; 0.0 (black) is fully transparent

!!! Tip: Shape or Matte
    Alpha channel describes the _shape_ of the image.  Images with alpha channel should be thought of as an irregularly shaped cardboard, not as tinted glass.

# Compositing

* Creating a good effect requires multiple operations; composing mutiple layers
* A macOS button has text ⊕ rounded-rect stroke ⊕ gradient background ⊕ transparent shadow
* Compositing is performed in multiple steps: foreground _source_ is composed onto background _destination_
* **Simple compositing**: opaque destination, varying source alpha of
  - 100% covers destination completely
  - 0%, destination is unaffected
  - 25% allows object to emit 25% of its light, lets 75% of light from background through
  - This is just linear interpolation

$$
R_{RGB} = (S_{RGB} \times S_A) + (D_{RGB} \times (1 - S_A)) \\
$$

For simple compositing, since destination is opaque (given), it blocks all of _its_ background light, so we know $R_A = 1$.

# Intermediate Buffers

* Three layers A, B and C can be composited either
  - A ⊕ (B ⊕ C)
  - (A ⊕ B) ⊕ C
* The bracketed expression results in an intermediate buffer generally, holding partial results of compositing; e.g. when masking, blurring, etc.
* Intermediate buffers are sometimes called offscreen passes, transparency layers or side buffers
* Almost _any_ image with transparency can be thought as partial result of some rendering that’ll be, at some point, composed to its final destination

!!! Note: Ultimately no transparency
    Everything you see on screen ultimately ends being composited into a non-transparent destination.

# Combining Alpha

* When mixing two transparent objects, think from transparency aspect instead of alpha
* Light first passing through 80% transparent object will let 80% of incoming light out
* Second object of 60% transparency will let through 60% of incident light
* Total light out = 80% × 60% = 48% of incident light

$$
\begin{align*}
R_T &= S_T \times D_T \\
(1 - R_A) &= (1 - S_T) \times (1 - D_T) \;\;\;\; \because T = 1 - A \\
R_A &= S_A + D_A - S_A \times D_A \\[2ex]
R_A &= S_A + D_A \times (1 - S_A)
\end{align*}
$$

!!! Note: Commutativity
    Resulting alpha doesn’t depend on relative order of objects.  Resulting pixel opacity is the same when source and destination are flipped.  Light shining behind or in front of two objects would come out with same intensity.

# Combining Colours

* A colour $S_{RGB}$ but with opacity $S_A$ causes it to contribute finally only $S_{RGB} \times S_A$
* Destination’s contribution is also limited by its alpha, $D_{RGB} \times D_A$
* However, incident light is also blocked by source, so final contrition of destination $D_{RGB} \times D_A \times (1 - S_A)$
* Likewise (merged) result’s contribution is $R_{RGB} \times R_A$

$$
\begin{align*}
R_{RGB} \times R_A &= (S_{RGB} \times S_A) + (D_{RGB} \times D_A \times (1 - S_A)) \tag{1} \label{1} \\[3ex]

R_A &= S_A + (D_A \times (1 - S_A)) \tag{2} \label{2} \\[1ex]
R_{RGB} &= \frac{(S_{RGB} \times S_A) + (D_{RGB} \times D_A \times (1 - S_A))}{R_A}
\end{align*}
$$

* Last two are the final equations of the resulting colour
* Notice that to get pure colour we’d to divide by result’s alpha
* However, any subsequent compositing operation requires us to multiple colour with alpha
* First equation where all R, S and D have their respective alphas multiplied is perhaps better

# Premultiplied Alpha

* A translucent object’s resulting colour contribution will always be controlled by alpha
* Make this explicit by storing the pure colour values scaled by alpha
* a.k.a. associated alpha in contrast to unassociated/straight alpha
* In straight alpha, a pixel with alpha 0 can have any pure colour value -- there’re many transparent values e.g. (1, 0.3, 0, _0_) = 0, (.7, 1, 0, _0_)
* In premultiplied alpha **there’s only one completely transparent colour**: $(0, 0, 0, 0)$

## Correctness

When alpha blending or filtering -- Gaussian, texture, … -- without premultiplied alpha we may end up with incorrect results.

An image having a red circle (α = 1.0) on a blue background (α = 0.1)[^fn1], when Gaussian blurred leads to a red circle with feathered edges i.e. border of the circle looks soft with transparency.  However, if the image has straight alpha the blue bleeds into the feathered part; the soft edge has a blue fringe.  Similar issues can be seen when bilinear interpolating an image to render it rotated.  Of course, the issue isn’t limited to these operations.

> "Any operation that requires some combination of semi-transparent colors will likely have incorrect results unless the colors are alpha premultiplied."

This happens because filtering works on colour channels (including alpha) independently; there’s no logic to weight pixel’s colour channels by its alpha.  Bordering pixels in the result, where sampling is done from both circle and the background, channels as treated as pure numerical values; colours are filtered oblivious to their alpha/opacity.  Consider just the RGB channels, Gaussian blur mixes the blue background and red circle as pure colours.  With premultiplied alpha, alpha is already factored into the channels so the blue mixed with red is already 90% transparent.  With straight alpha, _after this filtering_ the resulting colour is weighted by alpha when rendering (LHS).

$$
blend(filter(A, B), α) == filter(blend(A, α), blend(B, α))
$$

LHS is what’s done in code but the proper operation is RHS.  This equality only holds for premultiplied alpha since its colours are already weighted.  With straight alpha we end up with ugly fringes since LHS ≠ RHS.  In other words, filtering and _premultiplied_ blending are both linear transforms and hence associative.

[^fn1]: Visualize it as pure red circle centred on a ~~blue~~ checker board, since it’s 90% transparent blue

### Example

Average between red and 1% opaque blue:

``` text
STRAIGHT ALPHA
             circle = (1, 0, 0, 1)
         background = (0, 0, 1, 0.01)
  average at border = (½, 0, ½, 0.505)
       final colour = (0.2525, 0, 0.2525)

PREMULTIPLIED ALPHA
             circle = (1, 0, 0, 1)
         background = (0, 0, 0.01, 0.01)
  average at border = (½, 0, 0.005, 0.505)
       final colour = (0.2525, 0, 0.0025)
```

See the [_Ultimately no transparency_](#intermediatebuffers) note above on why the final colour has no alpha.

## Advantages: Premultiplied Alpha

* Correctness: no fringing/bleeding issues with filtering
  - With straight alpha, mixing colours and alpha values is incorrect since fully transparent pixels can also have an arbitrary colour in them
  - With premultiplied alpha they’re always black, so interpolated values are correct
* Superset of both normal and [additive blending](#additiveblending)
  - Both are doable with one step without breaking batching
  - Setting alpha to zero while RGB is non-zero gets you additive blending
  - Same image can have regions of regular alpha blending (α ≠ 0) and additive alpha blending (α = 0)
* By decreasing only alpha we can transform smoothly from normal to additive blending allowing cool effects such as glowing sparks to dark pieces of soot as particles age, glow effect around a light sabre, etc.
* Plays nice with DXT compression that supports transparent pixels only with RGB (0, 0, 0)
* [Associativity][] allows different groupings and reordering of intermediate renderings without breaking the final composition; all these give the same result
  - (((A ⊕ B) ⊕ C) ⊕ D) ⊕ E
  - (A ⊕ B) ⊕ (C ⊕ D ⊕ E)
  - A ⊕ (B ⊕ (C ⊕ (D ⊕ E)))
  - **Associativity isn’t Commutativity**: (A ⊕ B) ≠ (B ⊕ A) in general
* Blend function has one less multiplication; slightly faster

!!! Warning: PMA is meta
    An image usually doesn’t tell if it’s straight or premultiplied.  The programmer should know this.  Mixing images of straight and premultiplied alpha, or rendering one as another, leads to strange artefacts.

[Associativity]: https://en.wikipedia.org/wiki/Associative_property

# Combining Alpha Premultiplied Colours
With premultiplied alpha, equation $\eqref{1}$ above becomes

$$
R_{RGB} = S_{RGB} + (D_{RGB} \times (1 − S_A))
$$

$\eqref{2}$ stays the same.

$$
R_A = S_A + (D_A \times (1 - S_A))
$$

Since both these equations are the same channel-wise, it can be generalized into

$$
R = \color{gold}{S} \color{red}{+} (\color{green}{D} \color{magenta}{\times} \color{blue}{(1 - S_A)})
$$

Premultiplied alpha made things beautifully simple.  The operation <span style="color:magenta">masks</span> <span style="color:blue">some</span> <span style="color:green">background light</span> and <span style="color:red">adds</span> <span style="color:gold">new light</span>.  This is _source-over_ compositing with _normal_ blend mode -- the most frequently used.

# Porter-Duff Compositing Operations

| Operator | Source                            | Destination                       | Notes                                                              |
|----------|-----------------------------------|-----------------------------------|--------------------------------------------------------------------|
| Over     | $S + D \times(1-S_A)$             | $S \times(1-D_A) + D$             | Src-Over: most-used; Dst-Over: composite behind existing content   |
| Out      | $S \times(1-D_A)$                 | $D \times(1-S_A)$                 | Set [complement][]: Src outside Dst, Dst outside Src: punching out |
| In       | $S \times(D_A)$                   | $D \times(S_A)$                   | Set [intersection][]: masking in; raster geometry intersection     |
| Atop     | $S \times(D_A) + D \times(1-S_A)$ | $S \times(1-D_A) + D \times(S_A)$ | Overlay contents of one keeping other the mask                     |

Operators common to both source and destination:

| Operator    | Formula                             | Notes                               |
|-------------|-------------------------------------|-------------------------------------|
| Xor         | $S \times(1-D_A) + D \times(1-S_A)$ | Everything except overlapping parts |
| Source      | $S$                                 | Source wins unconditionally         |
| Destination | $D$                                 | Destination wins unconditionally    |
| Clear       | $0$                                 | Wipes out everything                |

All these operators have the same form

$$
\boxed{R = S \times{F_S} + D \times{F_D}}
$$

!!! Note: Combinations
    The possible values of $F_S$ are $0$, $1$, $D_A$ or $1-D_A$; similarly $F_D$ can take $0$, $1$, $S_A$ or $1-S_A$.  It doesn’t make sense to multiply (premultiplied) source or destination colour with its own alpha _again_.  Alpha-squaring effect isn’t pleasing aesthetically.

| $F_D$\$F_S$ | 0                | 1           | $D_A$       | $1-D_A$           |
|-------------|------------------|-------------|-------------|-------------------|
| 0           | Clear            | Source      | Source-In   | Source-Out        |
| 1           | Destination      | Lighter     |             | Destionation-Over |
| $S_A$       | Destionation-In  |             |             | Destionation-Atop |
| $1-S_A$     | Destionation-Out | Source-Over | Source-Atop | Xor               |

Note the symmetry along diagonal.

!!! Tip: [OpenGL Blending][]
    OpenGL provides `glBlendFunc(sFactor, dFactor)` to set the respective factors (compositing).  `glBlendEquation(mode)` changes the operator combining the colours from `GL_FUNC_ADD` to say `GL_FUNC_SUBTRACT`, `GL_FUNC_MIN`, etc. (blending; refer [KHR_blend_equation_advanced][] for advanced blend modes).  The functions’ `Separate` variants allow specifying parameters for colours and alpha separately [[OpenGL Wiki](#references)].

[complement]: https://en.wikipedia.org/wiki/Complement_(set_theory)
[intersection]: https://en.wikipedia.org/wiki/Intersect_(set_theory)
[OpenGL Blending]: https://learnopengl.com/Advanced-OpenGL/Blending
[KHR_blend_equation_advanced]: https://www.khronos.org/registry/OpenGL/extensions/KHR/.txt

# Additive Blending

Also go under the names _plus_, _lighter_, _plus-lighter_, _plus_ and _additive lighting_.  In [blend modes][], this is called [addition][bm-add] i.e. _additive blend mode_.

$$
R = S + D
$$

Addition of source light to destination.  This has the effect of lightening (coloured source) or leaving the colour as-is (pure black source: $x + 0 = x$).  When there’re multiple lights in a 3D scene, we add the effect of each light to the out colour; this addition is similar.

With pre-multiplied alpha, _source-over_ with $S_A = 0$ becomes _plus_.

$$
R = S + D \times(1 - S_A)  \mid  S_A = 0 \\[1ex]
R = S + D
$$

If we set source’s alpha plane to 0 but have non-zero RGB channels we achieve additive blending.  The values are no longer correctly pre-multiplied i.e. code having that assumption will work incorrectly when doing this.  Some software might skip processing pixels with α = 0, while other may try to un-premultiply and do some calculations on pure colour which will no longer work as intended.

[bm-add]: https://en.wikipedia.org/wiki/Blend_modes#Addition

# Compositing Caveats

+ **Group Opacity**: For a 50% translucent composite, setting individual components’ opacity to 0.5 won’t work.
  - Render to intermediate buffer, reduce buffer’s opacity and use it for further compositing
+ **Compositing Coverage**: When two perfectly overlapping figures are blended there may be bleeding
  - A black circle on a white background will have its border pixels feathered (gray) by blending background white
  - Superimposing an exact-sized white circle will still show a black border
  - Once coverage is stored in alpha channel, all geometric information is lost

# Alpha, Gamma and Linearity

!!! Warning
    Alpha remains linear, don’t gamma-decode it.

When alpha blending, gamma-decode colour channels, blend in linear space then gamma-encode and store.  **Premultiplied alpha works on linear colour values** i.e. pre-multiplication happens in linear space, followed by gamma-encoding (of colour channels only).

> **Store**: linear data: $(R, G, B, \alpha)$ -> premultiply alpha: $(\alpha R, \alpha G, \alpha B, α)$ -> gamma-encode: $((\alpha R)^\gamma, (\alpha G)^\gamma, (\alpha B)^\gamma, \alpha)$
>
> **Load**: nonlinear data: $((\alpha R)^\gamma, (\alpha G)^\gamma, (\alpha B)^\gamma, \alpha)$ -> gamma-decode: $(\alpha R, \alpha G, \alpha B, α)$ -> divide alpha: $(R, G, B, \alpha)$

!!! Tip
    When compositing, the last step of dividing alpha is unneeded.  It may be needed before some operations like colour space transforms.

Most image processing software by default do compositing with non-linear (gamma-encoded) colour leading to unpleasant artefacts.  Some provide knobs to tweak and gamma-decode before compositing.  Make sure that compositing/blending happens in linear space.  See [Computer Color is Broken](#references) for a neat summary of this issue.

!!! Tip
    $(\alpha, (\alpha R)^\gamma, (\alpha G)^\gamma, (\alpha B)^\gamma)$ seems to be the most preferable pixel format amongst others discussed in [[_Gamma Correction vs. Premultiplied Alpha_](#references)].  OpenGL extensions [EXT_texture_sRGB][] raises R, G, B to 2.2 before texture filtering and [EXT_framebuffer_sRGB][] raises them to (1/2.2) after the final raster operation.

[EXT_texture_sRGB]: http://www.opengl.org/registry/specs/EXT/texture_sRGB.txt
[EXT_framebuffer_sRGB]:http://www.opengl.org/registry/specs/ARB/framebuffer_sRGB.txt

# Premultiplied Alpha and Precision Loss

Perhaps the biggest drawback of using premultiplied alpha is reduction of bit depth of representable colours.  Two values 150, 151 can be represented as-is in 8-bit data type.  With an alpha of `0.2` both map to `30` with `u8`.  In fact `[148, 152]` map to `30`.  Lower the alpha value, more the loss.  Only 25.2% of RGBA8888 end up having unique representations after premultiplication; ~2 bits of `u32` gets wasted.

Transformation of colour channel data to different colour spaces need straight colour values, so alpha division is needed to get pure colour.  Transformations are common; we at least do gamma decoding.  Premultiplication thus reduces accuracy of colour representation and imperfect colour space transformation.

Reduction of this bit depth is still OK for compositing since lower the alpha value the lesser the colour’s impact on the composite/output.

!!! Tip: Avoid `u8` for accuracy
    If accuracy is of essence when processing colours shun 8-bit representations.  Use [float][]s!

[float]: https://en.wikipedia.org/wiki/Floating-point_arithmetic

# References

* [Alpha Compositing](https://ciechanow.ski/alpha-compositing/) by _Bartosz Ciechanowski_
  - Excellent for building mental model
  - Explains how light works with transparent objects
* Ugly fringes when blending -- [1](http://www.shawnhargreaves.com/blog/texture-filtering-alpha-cutouts.html), [2](http://www.shawnhargreaves.com/blog/premultiplied-alpha.html), [3](http://www.shawnhargreaves.com/blog/premultiplied-alpha-and-image-composition.html) -- by _Shawn Hargreaves_
* [Blending](https://cglearn.codelight.eu/pub/computer-graphics/blending#) by _Jaanus Jaggo_
* [Compositing, OpenGL Blending and Premultiplied Alpha](https://apoorvaj.io/alpha-compositing-opengl-blending-and-premultiplied-alpha/) by _Apoorava Joshi_
* [Interpreting Alpha](http://jcgt.org/published/0004/02/03/paper.pdf) by _Andrew Glassner_
  - Shows it’s okay to treat alpha as both opacity and coverage
* [Computer Color is Broken](https://www.youtube.com/watch?v=LKnqECcg6Gw) by _Minute Physics_
* [GPUs Prefer Premultiplication](http://www.realtimerendering.com/blog/gpus-prefer-premultiplication/) by _Eric Haines_
* [Blending - OpenGL Wiki](https://www.khronos.org/opengl/wiki/Blending)
* [Porter/Duff Compositing and Blend Modes](http://ssp.impulsetrain.com/porterduff.html) by _Søren Sandmann Pedersen_
* [Gamma Correction vs. Premultiplied Alpha](http://ssp.impulsetrain.com/gamma-premult.html) by _Søren Sandmann Pedersen_


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'short' };</script>
