// testing if a vector X's direction is within the directions defined by two other vectors in R² by doing the inverse
// lerp i.e. finding parameter t for a point along the interpolant, 2D line; inv_lerp.cpp does the testing right but the
// inverse lerp is incorrect as explained there and in Analysis.svg. This version remedies this by finding the right
// point where X would intersect the interpolant formed by the other two vectors.

#include <glm/glm.hpp>
#include <glm/gtx/mixed_product.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <glm/gtc/epsilon.hpp>
#include <cmath>
#include <iostream>

/*
 * In 3D this can be checked by ‖U × V‖ != 0 but here U and V are 2D vectors so we've to make them 3D (by setting z = 0)
 * and then find their cross product. U = <U.x, U.y, 0>, V = <V.x, V.y, 0>; we've
 *
 *   U × V = <0, 0, (U.x * V.y) - (U.y * V.x)>
 *
 * since U.z = V.z = 0; thus checking ‖U × V‖ = |(U.x * V.y) - (U.y * V.x)| > 0 is enough; another way to find U × V in
 * 2D is to find U-rot <-Uy, Ux> and dot it with V, which leads to the same formula as above.
 *
 * Reference: 3-D Computer Graphics by Samuel R. Buss (Appx. A, cross-prod. of 2D vectors)
 */
bool are_collinear(const glm::vec2 &U, const glm::vec2 &V)
{
    return std::abs((U.x * V.y) - (U.y * V.x)) <= std::numeric_limits<float>::epsilon();
}

// another way is to add all components
template <typename T>
inline bool is_zero(const T &v)
{
    return (glm::dot(v, v) <= std::numeric_limits<float>::epsilon());
}

glm::vec2 perp(const glm::vec2 &V)
{
    return glm::vec2{-V.y, V.x};
}

enum class RESULT : unsigned char
{
    POINT,              // intersection at exactly one point
    SKEW,               // required only for R³ since in R² skew lines always intersect
    LINEARLY_DEPENDANT, // infinite solutions since the lines are collinear
    MISS                // no intersection since it's outside the segment
};

struct Ray2D
{
    glm::vec2 org;
    glm::vec2 dir;
};

// http://geomalgorithms.com/a05-_intersect-1.html
// Based on GeomAlgorithms.com which is also given in Real-time Rendering with an optimisation (by Antonio), which isn't
// employed here. The 2D FoV workout has the implementation with the optimisation.
// Returns t, the scalar of line segment's vector
RESULT line_seg_ray_xsection_2d(const Ray2D &line_seg, const Ray2D &r, float *t)
{
    const auto p = perp(r.dir);
    const auto proj = glm::dot(line_seg.dir, p);
    if (std::abs(proj) <= std::numeric_limits<float>::epsilon())
        return RESULT::LINEARLY_DEPENDANT;
    *t = glm::dot(r.org - line_seg.org, p) / proj;
    if ((*t < 0.0f) || (*t > 1.0f))
        return RESULT::MISS;
    // the ray's parameter s ray should be within [0, ∞) for a valid intersection between a line segment and a ray;
    // however, for this program, we know s will always be in this interval; skip calculating s
    return RESULT::POINT;
}

// the same operation of inverse linear interpolation in R³ can be done with the below ray-ray intersection algorithm
struct Ray3D
{
    glm::vec3 org;
    glm::vec3 dir;
};

// based on Real-time Rendering, 3rd Ed. for 3D lines, rays and segments intersection; the above algorithm of
// 2D Ray-Ray intersection uses only vector operations and hence would be dimension-agnostic; yet Real-Time Rendering
// presents it strictly as a 2D Ray-Ray intersection algorithm, perhaps because it doesn't handle the skew case.
// However the skew case may be omitted when we're sure the line segments intersect; also GeomAlgorithms.com says that
// it can be used for higher dimensions, since if two linear components intersect, then their projection in
// R² (2D plane) will also intersect; hence adapting it for R³ may give a cheaper option than this.
RESULT line_seg_xsection_3d(const Ray3D &r1, const Ray3D &r2, glm::vec3 *point)
{
    const auto normal = glm::cross(r1.dir, r2.dir);
    if (is_zero(normal))
        return RESULT::LINEARLY_DEPENDANT;
    const auto den = glm::dot(normal, normal);
    const auto r1_to_r2 = r2.org - r1.org;
    // scalar triple product is glm::mixedProduct
    const auto s = glm::mixedProduct(r1_to_r2, r2.dir, normal) / den;
    const auto t = glm::mixedProduct(r1_to_r2, r1.dir, normal) / den;
    if ((s < 0.0f) || (s > 1.0f) || (t < 0.0f) || (t > 1.0f))
        return RESULT::MISS;
    const auto i1 = r1.org + (s * r1.dir);
    const auto i2 = r2.org + (t * r2.dir);

    // if the found points aren't the same, then the lines are skew
    // and we've the closest points between the lines
    const auto dist2 = glm::distance2(i1, i2);
    if (dist2 > (std::numeric_limits<float>::epsilon() * std::numeric_limits<float>::epsilon()))
        return RESULT::SKEW;
    // the above method has lesser operations than this one as of GLM 0.9.5.3
    // if (!glm::all(glm::epsilonEqual(i1, i2, std::numeric_limits<float>::epsilon())))
        // return RESULT::SKEW;

    *point = i1;
    return RESULT::POINT;
}

int main(int argc, char **argv)
{
    std::cout << "Enter U:" << std::endl;
    glm::vec2 U;
    std::cin >> U.x >> U.y;
    std::cout << "Enter V:" << std::endl;
    glm::vec2 V;
    std::cin >> V.x >> V.y;

    //make sure the vectors aren't (linearly dependent) parallel or opposite i.e. U != λV, for all λ.
    if (!are_collinear(U, V))
    {
        std::cout << "Enter P:" << std::endl;
        glm::vec2 P;
        std::cin >> P.x >> P.y;

        /*
         * check to make sure that the target vector P is on the same side of the extremes;
         * without this check, alpha values calculated for a P behind will be the same as the
         * ones calculate for a P in front e.g. for U = <1, 1>, V = <1, -1> both P1 = <1, 0.25>
         * and P2 = <-1, 0.25> give α = -0.1859945994 which is misleading; thus calculate α
         * only when P is in side pointed to by N which points to the side pointed by U and V
         */
        // U + V gives the median direction (normal) of U and V
        const auto N = U + V;
        const auto direction = glm::dot(N, P);
        // direction = 0 case i.e. P orthogonal to N is allowed to fall into the else clause
        // since if P is perpendicular to N then it's anyway outside [U, V] range since
        // U and V cannot be parallel (as in straight angle) if the control's reached here
        if (direction > std::numeric_limits<float>::epsilon())
        {
            // interpolant origin and direction
            const Ray2D line = { U, {V - U} };
            float t = std::numeric_limits<float>::max();
            // if the ray from origin passing through P is cutting the interpolant (line segment formed by
            // lerping from U to V) then the direction of P is within the direction defined by U and V
            // doing inverse lerp on that point would give the right interpolation ratio (t)
            if (RESULT::POINT == line_seg_ray_xsection_2d(line, {{}, P}, &t))
            {
                std::cout << "P is inside [U, V]\n";
            }
            else
                std::cout << "P is outside [U, V]\n";
            const auto result = (1.0f - t) * U + (t * V);
            std::cout << 1.0f - t << " U + " << t << " V = <" << result.x << ", " << result.y << ">\n";
        }
        else
            std::cout << "P is behind U and V\n";
    }
    else
        std::cout << "Degenerate case: U and V are linearly dependant i.e. parallel or opposite vectors (U = λV)\n";

    // test driver for line_seg_xsection_3d
    glm::vec3 point;
    auto res = line_seg_xsection_3d({{0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 1.0f}}, {{0.0f, 0.0f, 0.0f}, {0.0f, 0.0f, -1.0f}}, &point);
    std::cout << static_cast<unsigned int>(res) << '\n';
    res = line_seg_xsection_3d({{0.0f, 1.0f, 0.0f}, {0.0f, 0.0f, 1.0f}}, {{0.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 1.0f}}, &point);
    std::cout << static_cast<unsigned int>(res) << '\n';
}
