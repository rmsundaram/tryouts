#include <glm/glm.hpp>
#include <iostream>

int main(int argc, char **argv)
{
    using namespace std;

    cout << "Enter U:" << endl;
    glm::vec2 U;
    cin >> U.x >> U.y;
    cout << "Enter V:" << endl;
    glm::vec2 V;
    cin >> V.x >> V.y;

    /*
     * make sure the vectors aren't (linearly dependent) parallel or opposite i.e.
     * U != λV, for all λ. In 3D this can be checked by ‖U×V‖ != 0 but here U and V are 2D
     * vectors so we've to make them 3D (by setting z = 0) and then find their cross product
     * U = <U.x, U.y, 0>; V = <V.x, V.y, 0>; now U × V = 
     * <(U.y * V.z) - (U.z * V.y), (U.z * V.x) - (U.x * V.z), (U.x * V.y) - (U.y * V.x)>
     * since U.z = V.z = 0, U × V = <0, 0, (U.x * V.y) - (U.y * V.x)>; thus checking
     * ‖U × V‖ = |(U.x * V.y) - (U.y * V.x)| > 0 is enough; another way to find U×V in 2D
     * is to find U-rot <-Uy, Ux> and dot it with V, which leads to the same formula as above
     * Reference: 3-D Computer Graphics by Samuel R. Buss (Appx. A, cross-prod. of 2D vectors)
     */
    if (abs((U.x * V.y) - (U.y * V.x)) > std::numeric_limits<float>::epsilon())
    {
        cout << "Enter P:" << endl;
        glm::vec2 P;
        cin >> P.x >> P.y;

        /*
         * check to make sure that the target vector P is on the same side of the extremes;
         * without this check, alpha values calculated for Ps behind will be the same as the
         * ones calculated for Ps in front e.g. for U = (1, 1); V = (1, -1); P1 = (1, 0.25)
         * => α1 = -0.1859945994 but P2 = (1, -0.25) => α2 = -0.1859945994, which is misleading;
         * thus calculate α only when P is in the direction of the normal N which points in the
         * direction of the side pointed by U and V
         */
        // U + V gives the median direction (normal) of U and V
        const auto N = U + V;
        const auto direction = glm::dot(N, P);
        // direction = 0 case i.e. P orthogonal to N is allowed to fall into the else clause
        // since if P is perpendicular to N then it's anyway outside [U, V] range since
        // U and V cannot be parallel if the control's reached here
        if (direction > 0)
        {
            /*
             * for the process of inverting vector interpolation and finding alpha
             * the vectors involved should be normalized (see Analysis.svg), without which
             * it'd lead to an invalid α value; try U = <1, 1>, V = <1, -1>, P = <1, 0>
             * without normalizing, it'd give α = 1 (meaning P = V), which is invalid
             */
            U = glm::normalize(U);
            V = glm::normalize(V);
            P = glm::normalize(P);
            // formula IV.3 from 3-D Computer Graphics by Samuel R. Buss
            const auto A = V - U;
            const auto alpha = glm::dot(A, P - U) / glm::dot(A, A);
            cout << "normalized U = (" << U.x << ", " << U.y << ")" << endl;
            cout << "normalized V = (" << V.x << ", " << V.y << ")" << endl;
            cout << "normalized P = (" << P.x << ", " << P.y << ")" << endl;
            cout << "α = " << alpha << ", β = " << (1.0f - alpha) << endl;
            if ((alpha >= 0.0f) && (alpha <= 1.0f))
                cout << "P is inside [U, V]" << endl;
            else
                cout << "P is outside [U, V]" << endl;

            /*
             * this verification step shows that the result isn't perfect (see Analysis.svg
             * for the reason); for proper interpolation between vectors spherically, spherical
             * linear interpolation a.k.a. slerp is required; although quaternions are famous
             * for slerp, even vectors can be interpolated spherically. Refer §8.4 Interpolating
             * Vectors in Mathematics for Computer Graphics (2010), 3rd Ed. by John Vince;
             * however this should be inverted to deduce the right α value. See §1.8 Elements of
             * Vector Geometry, figure 1.19 in Mathematics for Computer Graphics Applications by
             * M.E.Mortenson to visualize the linear interpolation done here
             */
            const auto result = glm::normalize(U + (alpha * A));
            cout << "β U + α V = (" << result.x << ", " << result.y << ")" << endl;
        }
        else
            cout << "P is behind U and V" << endl;
    }
    else
        cout << "Degenerate case: U and V are linearly dependant i.e. parallel or opposite vectors (U = λV)" << endl;
}
