/*
 * techniques here achieve camera initialization and manipulation without using glm::lookAt at all
 * the relevant mathematical workout can be seen in Camera_sans_LookAt.jpg; matrix is used for orientation
 * camera orientation representation, refer cam_freelook_mat_quat.cpp for a quaternion based implementation
 */

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/epsilon.hpp>
#include <iostream>

std::ostream& operator << (std::ostream &os, const glm::mat4 &m)
{
    return
    os << m[0][0] << '\t' << m[1][0] << '\t' << m[2][0] << '\t' << m[3][0] << '\n'
       << m[0][1] << '\t' << m[1][1] << '\t' << m[2][1] << '\t' << m[3][1] << '\n'
       << m[0][2] << '\t' << m[1][2] << '\t' << m[2][2] << '\t' << m[3][2] << '\n'
       << m[0][3] << '\t' << m[1][3] << '\t' << m[2][3] << '\t' << m[3][3] << '\n';
}

bool are_equal(const glm::mat4 &m1, const glm::mat4 &m2, float epsilon)
{
    bool are_equal = true;
    for(auto i = 0u; are_equal && (i < 4); ++i)
    {
        auto res = glm::epsilonEqual(m1[i], m2[i], epsilon);
        are_equal &= res[0] & res[1] & res[2] & res[3];
    }
    return are_equal;
}

// this Camera class is copied from src/main.hpp of the workout FreeRoamCam
// that implements a free-roaming camera 
struct Camera
{
    glm::vec3 eye;
    glm::vec3 at;
    glm::vec3 up;
    glm::mat4 view_xform;

    Camera() :
        eye{0.0f, 0.0f, 5.0f},
        at {0.0f, 0.0f, 0.0f},
        up {0.0f, 1.0f, 0.0f},
        view_xform(glm::lookAt(eye, at, up))
    {
    }

    void reset()
    {
        *this = Camera();
    }

/*
 * view_xform maps world to view i.e. its columns are world's basis expressed in
 * view's basis; based on Dr. Ravi's lecture on gluLookAt and 3D Math Primer's
 * §8.2.2 Direction Cosines, the rows of an orthogonal matrix (like rotation)
 * gives the axes of the target coordinate system in base system; in this case
 * view's basis in terms of world's basis
 */
    glm::vec3 X() const
    {
        return glm::vec3(glm::row(view_xform, 0));
    }

    glm::vec3 Y() const
    {
        return glm::vec3(glm::row(view_xform, 1));
    }

    glm::vec3 Z() const
    {
        return glm::vec3(glm::row(view_xform, 2));
    }

    void slide(glm::vec3 mag)
    {
        eye += mag;
        at += mag;
        view_xform = glm::lookAt(eye, at, up);
    }

/*
 * Older (now commented out) method was relying on the eye, at and up for knowing the view axes in
 * terms of world axes which was not optimal as the axes wouldn't be normalized, also the up vector
 * could've changed during orthogonalization in lookAt instead using matrix rows solves these problems
 * elegantly; see comments and implementation of Camera::X() and friends
 */
    void rotate_X(float angle)
    {
//        const auto view_dir = eye - at;
//        const auto right = glm::cross(up, view_dir);
//        const auto rot = glm::rotate(angle, right);
        const auto rot = glm::rotate(angle, X());
        at = eye + glm::vec3(rot * glm::vec4{at - eye, 0.0f});
        up = glm::vec3(rot * glm::vec4(up, 0.0f));
        view_xform = glm::lookAt(eye, at, up);
    }

    void rotate_Y(float angle)
    {
//        const auto rot = glm::rotate(angle, up);
        const auto rot = glm::rotate(angle, Y());
        at = eye + glm::vec3(rot * glm::vec4{at - eye, 0.0f});
        view_xform = glm::lookAt(eye, at, up);
    }

    void rotate_Z(float angle)
    {
//        const auto view_dir = eye - at;
//        const auto rot = glm::rotate(angle, view_dir);
        const auto rot = glm::rotate(angle, Z());
        up = glm::vec3(rot * glm::vec4(up, 0.0f));
        view_xform = glm::lookAt(eye, at, up);
    }
};

/*
 * the idea of local transformation is that instead of having a global system and
 * apply (pre-multiply) all transformations relative to that we start from identity
 * and every transformation applied (post-multiplying) transforms the coordinate system
 * to a new one relative to that system. Say we've a view V of the scene and one of the
 * 6 DoF is changed, we've to form a new V' to map the World to; first V' should be
 * aligned to V and then V to world i.e. V' -> V -> World will give Mw->v'
 */
void move_X(Camera *cam,
            float delta,
            glm::mat4 *view_xform)
{
    cam->slide(cam->X() * delta);

#ifdef NON_OPT
    // since the new view starts from identity, its X-axis would just be basis i<1, 0, 0>
    // i.e. aligning V' to V first with V' (identity) as the basis and then aligning V to World
    const auto inv = glm::translate(glm::vec3{-delta, 0.0f, 0.0f});
    *view_xform = inv * *view_xform;
#else
    // although the above code is correct and also explains what happens conceptually
    // it can further be simplified to this since the translate vector would be <-delta, 0, 0>
    // it only affect the origin.X element of the matrix
    (*view_xform)[3][0] -= delta;
#endif
}

void move_Y(Camera *cam,
            float delta,
            glm::mat4 *view_xform)
{
    cam->slide(cam->Y() * delta);

#ifdef NON_OPT
    // since the new view starts from identity, its Y-axis would just be basis j<0, 1, 0>
    const auto inv = glm::translate(glm::vec3{0.0f, -delta, 0.0f});
    *view_xform = inv * *view_xform;
#else
    (*view_xform)[3][1] -= delta;
#endif
}

void move_Z(Camera *cam,
            float delta,
            glm::mat4 *view_xform)
{
    cam->slide(cam->Z() * delta);

#ifdef NON_OPT
    // since the new view starts from identity, its Z-axis would just be basis k<0, 0, 1>
    const auto inv = glm::translate(glm::vec3{0.0f, 0.0f, -delta});
    *view_xform = inv * *view_xform;
#else
    (*view_xform)[3][2] -= delta;
#endif
}

/*
 * We want to make a camera that can move around, look in different directions, and maybe zoom in and out.
 * However, the clip volume can not be changed. It is always the same size, and in the same position. So,
 * instead of moving the camera, we must move the entire 3D scene so that it fits inside the clip volume
 * cube correctly. For example, if we want to rotate the camera to the right, we actually rotate the whole
 * world to the left. If we want to move the camera closer to the player, we actually move the player
 * closer to the camera. This is how “cameras” work in 3D, they transform the entire world so that it
 * fits into the clip volume and looks correct.
 * ~ http://tomdalling.com/blog/modern-opengl/03-matrices-depth-buffering-animation/
 */
void rotate_X(Camera *cam,
              float angle,
              glm::mat4 *view_xform)
{
    cam->rotate_X(angle);

    /*
     * we've view_xform (Mw->v) already (this is also Tv->w); now the view needs to be rotated, instead of
     * rotating the view's frame (represented in world space) and then forming Tw->v and finding the inverse
     * of it (which is what gluLookAt does), we can imagine that the final system i.e. new rotated view v' is
     * already the space we're; we need Mw->v' or Tv'->w; since we're in v' we now apply the opposite rotation
     * to first merge with the previous frame v i.e. Tv'->v is now available, transforming the previous frame
     * to world's frame is already in view_xform (Tv->w); concatenating Tv'->v Tv->w = Tv'->w = Mw->v'; notice
     * the right (post) multiplication; these are easier to visualize as local transformations.
     */
    // since the new view starts from identity, rotation along X would just be the basis i
    const auto inv = glm::eulerAngleX(-angle);
    *view_xform = inv * *view_xform;
}

void rotate_Y(Camera *cam,
              float angle,
              glm::mat4 *view_xform)
{
    cam->rotate_Y(angle);

    // since the new view starts from identity, rotation along Y would just be the basis j
    const auto inv = glm::eulerAngleY(-angle);
    *view_xform = inv * *view_xform;
}

void rotate_Z(Camera *cam,
              float angle,
              glm::mat4 *view_xform)
{
    cam->rotate_Z(angle);

    // since the new view starts from identity, rotation along Z would just be the basis k
    const auto inv = glm::eulerAngleZ(-angle);
    *view_xform = inv * *view_xform;
}

int main()
{
    // initializes camera with its view xform calculated using glmLookAt
    // eye =  (0, 0, 5), at = (0, 0, 0), up = <0, 1, 0>
    Camera cam;

    // hand-calculated view xform: move the view's origin to the world's origin
    /*
     * for the above configuration, below matrix would be the mapping
     * from world to view; this transform, when viewed as applied to the
     * coordinate system (post-multiplication/local coordinate)
     * moves view's frame to align with world's frame; note that GLM
     * takes these as columns
     */
    auto view_xform = glm::mat4{1.0f, 0.0f, 0.0f, 0.0f,     // col1
                                0.0f, 1.0f, 0.0f, 0.0f,     // col2
                                0.0f, 0.0f, 1.0f, 0.0f,     // col3
                                0.0f, 0.0f,-5.0f, 1.0f};    // col4
    // the Camera object can be replaced by just this one 4x4 matrix
    // it suffices for the camera manipulation on all 6 DoFs
    char ch = '\0';
    bool transformed = true;
    do
    {
        if(transformed)
        {
            std::cout << cam.view_xform << '\n' << view_xform << '\n';
            const auto epsilon = 0.001f;
            const auto equal = are_equal(cam.view_xform, view_xform, epsilon);
            if (!equal)
                break;
        }
        else
            transformed = true;

        const auto angle_delta = 0.2f;
        const auto slide_delta = 0.5f;

        std::cin >> ch;
        switch(ch)
        {
            case 'w':
                move_Y(&cam, -slide_delta, &view_xform);
                std::cout << "Moving up\n";
                break;
            case 'x':
                move_Y(&cam, slide_delta, &view_xform);
                std::cout << "Moving down\n";
                break;
            case 'a':
                move_X(&cam, -slide_delta, &view_xform);
                std::cout << "Moving left\n";
                break;
            case 'd':
                move_X(&cam, slide_delta, &view_xform);
                std::cout << "Moving right\n";
                break;
            case 's':
                move_Z(&cam, -slide_delta, &view_xform);
                std::cout << "Moving forward\n";
                break;
            case 'z':
                move_Z(&cam, slide_delta, &view_xform);
                std::cout << "Moving backward\n";
                break;
            case '4':
                rotate_Y(&cam, angle_delta, &view_xform);
                std::cout << "Yaw left\n";
                break;
            case '6':
                rotate_Y(&cam, -angle_delta, &view_xform);
                std::cout << "Yaw right\n";
                break;
            case '8':
                rotate_X(&cam, angle_delta, &view_xform);
                std::cout << "Pitch up\n";
                break;
            case '2':
                rotate_X(&cam, -angle_delta, &view_xform);
                std::cout << "Pitch down\n";
                break;
            case '1':
                rotate_Z(&cam, angle_delta, &view_xform);
                std::cout << "Roll left\n";
                break;
            case '3':
                rotate_Z(&cam, -angle_delta, &view_xform);
                std::cout << "Roll right\n";
                break;
            default:
                transformed = false;
        }
    } while(ch != 'q');
}
