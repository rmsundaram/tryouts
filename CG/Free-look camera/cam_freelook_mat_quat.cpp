// initialization and manipulation of a free-look camera using matrices and quaternions for orientation representation
// this file is a derivative of cam_manip_sans_lookat.cpp

/*
 * Say the camera UP vector is assumed to be fixed as the world's UP vector; what happens when the player is gradually
 * rotating the camera up?
 *
 * The UP and VIEW_DIR vector eventually aligns due to this action by the player. To form an orthonormal camera frame we
 * need to cross them; but since they're now linearly dependant, we'll end up with a zero vector, there by a rank
 * deficient matrix due to one of its columns being a zero vector. We've a loss of a degree of freedom due to aliasing.
 * If an alternate UP vector is used just as we find out that we've a zero vector, this will lead to an instantaneous
 * 180° rotation around VIEW_DIR⁰. How do we solve this properly?
 *
 * The solution is to rotate the UP vector along with VIEW_DIR vector simultaneously by the same angle along the side
 * axis; this would mean multiplying both the vectors by the same rotation matrix. If the camera orientation is stored
 * as a matrix, instead of multiplying each vector (column) separately and putting them back into the matrix, just
 * pre-multiplying the rotation matrix with the orientation matrix would rotate the both vectors (or the frame) in one
 * go. If gluLookAt is used (less efficient), then both the LOOK_AT point and the UP vector should be rotated by the
 * same rotation matrix before passing them to the function.
 *
 * A cleaner solution that completely avoids this problem would be to use quaternions as explained by Anton Gerdelan¹,
 * since quaternions don't've three axes in the first place to worry about one axis aligning with the other; quaternions
 * store orientation as just a single axis and an angle of rotation associated with it.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * GIMBAL LOCK
 * Although this is a loss of a degree of freedom, this isn't gimbal lock as explained by ² and Anton. When rotations
 * form a hierarchy — second axis altered due to the first rotation and the third axis altered due to the previous two
 * rotations — we've a setup similar to the one in a gyroscope where the physical gimbals form a hierarchy and thus a
 * potential for a gimbal lock in such a system due to aliasing. Say we're storing the orientation of an object as three
 * angles around three axes (i.e. Euler and it's close relative Fixed angles), to convert it into an orientation matrix
 * we form three rotation matrices. A rotation matrix is an orientation transform. Each transform defines a new
 * coordinate system, and the next transform is based on an object in the new space. For example, if we apply the roll
 * first, we have now changed what the axis for the subsequent yaw is³ - thus forming a hierarchy where a rotation's
 * result depends on a previous rotation's result. Here the aliasing happens when rotation along one of the axis leads
 * to the other two axis align i.e. two axes now represent the same DoF and we don't've an axis to represent a DoF; the
 * word "lock" is misleading: no gimbal is restrained. All three gimbals can still rotate freely about their respective
 * axes of suspension. Nevertheless, because of the parallel orientation of two of the gimbals axes there is no gimbal
 * available to accommodate rotation along one axis. Euler angles have the gimbal lock problem due to discontinuity in
 * their representation. These discontinuities are caused by the existence of many-to-one mappings between the Euler
 * angle parametrization of the set of 3D rotations. This allows the data set to flip between different Euler angle
 * combinations which correspond to a single 3D rotation, which, although remaining continuous in the space of rotation,
 * are discontinuous in the Euler angle parameter space. One can use Euler angles to describe every orientation, but you
 * cannot use Euler angles to describe every change in orientation due to aliasing when interpolated.
 *
 * In mathematics there are different types of singularity, in these cases we are talking about the situation where:
 *
 *     • Many points in one representation are mapped onto a single point in another representation
 *     • Infinitesimal changes close to the singularity in one representation may cause large changes in the other
 *
 * The Mercator Projection maps the north and south poles to lines this means that, at these points, the projection is
 * many to one⁴. Coordinate singularity occurs when an apparent singularity or discontinuity occurs in one coordinate
 * frame, which can be removed by choosing a different frame. An object moving due north (for example, along the line 0°
 * longitude) on the surface of a sphere will suddenly experience an instantaneous change in longitude at the pole
 * (i.e., jumping from longitude 0 to longitude 180°). In fact, longitude is not uniquely defined at the poles. This
 * discontinuity, however, is only apparent; it is an artefact of the coordinate system chosen, which is singular at the
 * poles. A different coordinate system would eliminate the apparent discontinuity e.g. by replacing latitude/longitude
 * with n-vectors. Stephen Hawking aptly summed this up with the question “what's north of the north pole?”⁵.
 *
 * In formal language, gimbal lock occurs because the map from Euler angles to rotations (topologically, from the
 * 3-torus T3 to the real projective space RP3) is not a covering map – it is not a local homeomorphism at every point,
 * and thus at some points the rank (degrees of freedom) must drop below 3, at which point gimbal lock occurs. Euler
 * angles provide a means for giving a numerical description of any rotation in three-dimensional space using three
 * numbers, but not only is this description not unique, but there are some points where not every change in the target
 * space (rotations) can be realized by a change in the source space (Euler angles). This is a topological constraint –
 * there is no covering map from the 3-torus to the 3-dimensional real projective space; the only (non-trivial) covering
 * map is from the 3-sphere, as in the use of quaternions⁶. The problem with Euler angles is that they (topologically)
 * make up a 3D solid box, while the rotations are a 3D projective space (which is a 3D sphere with antipodal points
 * glued together). The point is that those two manifolds cannot be mapped onto each other in a way which is “continuous
 * both way” (i.e. there is no homeomorphism [In the mathematical field of topology, a homeomorphism or topological
 * isomorphism or bicontinuous function is a continuous function between topological spaces that has a continuous
 * inverse function.]). A circle and a line are also “organized differently” by “similarity”. Or as a mathematician
 * would say, they have different topology. No matter how you try to match points on the line with points on the circle,
 * somewhere you will find that nearby points on the circle are matched up with points on the line which are not nearby.
 * Put plainly, you must break the circle⁷.
 *
 * From the physical (gyroscope) gimbal analogy, it can be realized that the outermost gimbal rotates both the inner
 * gimbals with it; this corresponds to the first (right-most in column-vector, right-handed convention) rotation matrix
 * which affects the rotation axis of the next two rotations to follow. In X-Y-Z Euler angle configuration, rotation
 * along X affects (rotates) the object's Y and Z axes too. The middle matrix, although rotates both axes of the current
 * system, the axis that really matters is the one along which a rotation is yet to happen. In the above configuration,
 * although rotation along Y would affect the object's X and Z axes, we've already rotated the object along X, and thus
 * will be unaffected w.r.t that axes, the effect Y's rotation has is only on Z. The demo app.³ shows this exactly where
 * three gimbals are mounted forming a hierarchy. The outermost gimbal affects the inner two gimbals and the middle one
 * affects the innermost gimbal alone; rotating the middle gimbal 90° aligns the innermost and outermost gimbals leading
 * to the loss of a DoF.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * AVOIDING GIMBAL LOCK
 * A way of avoiding gimbal lock using Euler or fixed angles is by restricting the values of the angles to avoid
 * aliasing. A common set of restrictions that work for most applications/games:
 * YAW:   any angle
 * PITCH: ±89°
 * ROLL:  ±89°
 *
 * Gimbal lock is not prevented magically by just using quaternions; quaternions are still prone to gimbal lock if you
 * use them incorrectly. It's not what is used to represent rotations that leads to a gimbal lock, it's concatenating
 * multiple rotations that causes it⁸. We can end up with a gimbal lock with quaternions: q(i, 0) * q(j, 90) * q(k, 20);
 * here too the gimbal lock occurred since we concatenated three sequences of rotations. Quaternions can be used to
 * avoid gimbal lock, but so can matrices, axis-angles and other representations except Euler and fixed angles which
 * behave exactly like physical gimbals, where there's a hierarchy of local orientations⁹. Gimbal lock is a property
 * inherent only to Euler angles, independent from the representation of the rotation.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * QUATERNIONS VS OTHERS
 * EULER ANGLES: aliasing, discontinuity and there by gimbal lock
 * MATRICES: take up more space, suffers from matrix creep (numerical stability issues), expensive orthonormalization
             and interpolation isn't easy, as lerping the 3 axes would make the matrix lose orthogonality
 * AXIS-ANGLE: smooth interpolation only along the same axis, concatenation of two axis-angle representations and
 *             rotating vectors are very costly¹⁰. Rotate an object by an angle θ1 about an axis a1, then by θ2 about a
 *             different axis a2. What is the net rotation in axis-angle representation? The net effect of this in terms
 *             of a single rotation, (concatenation) is a lot easier with quaternions
 * UNIT QUATERNIONS: continuous, compact, cheap normalization, easy interpolation, efficient concatenation and
 *                   precomputes transcendental functions for efficient vector rotation¹⁰
 *
 * Code here demonstrates storing camera orientation as a (orientation) matrix and a quaternion. For every turn/move, it
 * changes the orientation by rotating or moving the camera frame into a new one; for the next turn the axis of rotation
 * is w.r.t one of the primary axes of the camera's current reoriented frame, with no concatenation w.r.t the previous
 * step; the previous step's rotation is already part of the system's orientation and wouldn't affect the current one's
 * state, thus no hierarchy is formed. Had it been stored as Euler angles every turn would have been stored as deltas
 * along axes which aren't w.r.t the reoriented state but from the original state (identity frame) of the object instead
 * of a making a new frame out of it; this may eventually lead to gimbal lock.
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 * REFERENCES
 *  ⁰ §6.2.3 Controlling the Camera, Essential Math
 *  ¹ http://antongerdelan.net/opengl/quaternions.html
 *  ² http://gamedev.stackexchange.com/questions/30162/
      calculating-up-vector-to-avoid-gimbal-lock-using-euler-angles#comment50811_30170
 *  ³ §8.1 Gimbal Lock, Learning Modern 3D Graphics Programming by Jason L. McKesson
 *  ⁴ Comparison of Euler Angles and Mercator Projection
 *    http://www.euclideanspace.com/maths/geometry/rotations/euler/singularity/index.htm
 *  ⁵ http://en.wikipedia.org/wiki/Coordinate_singularity
 *  ⁶ http://en.wikipedia.org/wiki/Gimbal_lock
 *  ⁷ http://sundaram.wordpress.com/2013/03/08/mathematical-reason-behind-gimbal-lock-in-euler-angles/
 *  ⁸ http://gamedev.stackexchange.com/questions/51410/how-to-avoid-gimbal-lock#comment87952_51410
 *  ⁹ http://gamedev.stackexchange.com/questions/23540/why-do-people-use-quaternions/23543#comment38126_23543
 * ¹⁰ §5.4.5 Axis-Angle Summary, Essential Math
 *    http://www.gamedev.net/page/resources/_/technical/math-and-physics/a-simple-quaternion-based-camera-r1997
 */

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/epsilon.hpp>
#include <glm/gtx/orthonormalize.hpp>

#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>

#include <iostream>

// this Camera class is copied from src/main.hpp of the workout FreeRoamCam
// that implements a free-roaming camera 
struct Camera
{
    glm::vec3 eye;
    glm::vec3 at;
    glm::vec3 up;
    glm::mat4 view_xform;

    Camera() :
        eye{0.0f, 0.0f, 5.0f},
        at {0.0f, 0.0f, 0.0f},
        up {0.0f, 1.0f, 0.0f},
        view_xform(glm::lookAt(eye, at, up))
    {
    }

    void reset()
    {
        *this = Camera();
    }

/*
 * view_xform maps world to view i.e. its columns are world's basis expressed in
 * view's basis; based on Dr. Ravi's lecture on gluLookAt and 3D Math Primer's
 * §8.2.2 Direction Cosines, the rows of an orthogonal matrix (like rotation)
 * gives the axes of the target coordinate system in base system; in this case
 * view's basis in terms of world's basis
 */
    glm::vec3 X() const
    {
        return glm::vec3(glm::row(view_xform, 0));
    }

    glm::vec3 Y() const
    {
        return glm::vec3(glm::row(view_xform, 1));
    }

    glm::vec3 Z() const
    {
        return glm::vec3(glm::row(view_xform, 2));
    }

    void slide(const glm::vec3 &mag)
    {
        eye += mag;
        at += mag;
        view_xform = glm::lookAt(eye, at, up);
    }

    void rotate_X(float angle)
    {
        const auto rot = glm::rotate(angle, X());
        at = eye + glm::vec3(rot * glm::vec4{at - eye, 0.0f});
        up = glm::vec3(rot * glm::vec4(up, 0.0f));
        view_xform = glm::lookAt(eye, at, up);
    }

    void rotate_Y(float angle)
    {
        const auto rot = glm::rotate(angle, Y());
        at = eye + glm::vec3(rot * glm::vec4{at - eye, 0.0f});
        view_xform = glm::lookAt(eye, at, up);
    }

    void rotate_Z(float angle)
    {
        const auto rot = glm::rotate(angle, Z());
        up = glm::vec3(rot * glm::vec4(up, 0.0f));
        view_xform = glm::lookAt(eye, at, up);
    }
};

// normalize using Padé approximant to avoid sqrt
// http://stackoverflow.com/a/12934750/183120
glm::quat normalize_quat(const glm::quat &q)
{
    const float n_sq = glm::length2(q);
    if (std::abs(1.0f - n_sq) < 2.107342e-8f)
        return (2.0f / (1.0f + n_sq)) * q;
    else
        return (1.0f / std::sqrt(n_sq)) * q;
}

struct Quat_cam
{
    glm::vec3 position;     // camera position in world space
    glm::quat orientation;  // camera orientation in world space initialized with identity quaternion
                            // converting it to a matrix would give camera's basis in terms of world's, Mview->world;
                            // however rotate() does inverse rotation to avoid transposing and get Mworld->view directly
                            // thus this orientation is maintained as Mworld->view at all times

    void rotate(const glm::vec3 &axis, float angle)
    {
        // as we need to construct Mworld->view, we can rotate by angle and then invert (transpose) the
        // resulting matrix Mview->world or avoid the transpose by rotating -angle or along -axis, which
        // would maintain orientation as Mworld->view
        const auto versor = glm::angleAxis(-angle, axis);
        orientation = versor * orientation;
        orientation = normalize_quat(orientation);
    }

    void slide(const glm::vec3 &mag)
    {
        position += mag;
    }

    glm::mat4 view_xform() const
    {
        // we need Mworld->view = Tview->world by post-multiplying
        // rotate view's frame to align with world's frame
        auto R = glm::mat4_cast(orientation);
        // translate view's origin to align with world's origin
        return glm::translate(R, -position);
        // TODO: [optimisation] like orientation, even position's inverse can be maintained by changing slide()
        // into position -= mag which would avoid inverting the position here; in that case Quat_cam::position
        // should also be inverse-initialized with {0.0f, 0.0f, -5.0f} instead of {0.0f, 0.0f, 5.0f}; however
        // this would mean, whenever the camera position is required in world space in the app. code we've to
        // return -position which isn't needed without this optimisation
    }

    // TODO: [optimisation] for the below three functions, instead of constructing the matrix and
    // throwing away 3 rows construct only one (the required axis)
    glm::vec3 X() const
    {
        const auto Mwv = view_xform();
        // this gives Mworld->view (see comment in rotate function) i.e. the upper 3x3 rotation matrix has
        // world's basis in terms of view as columns, hence the rows would be view's basis in terms of world
        return glm::vec3(glm::row(Mwv, 0));
    }

    glm::vec3 Y() const
    {
        const auto Mwv = view_xform();
        return glm::vec3(glm::row(Mwv, 1));
    }

    glm::vec3 Z() const
    {
        const auto Mwv = view_xform();
        return glm::vec3(glm::row(Mwv, 2));
    }
};

std::ostream& operator << (std::ostream &os, const glm::mat4 &m)
{
    return
    os << m[0][0] << '\t' << m[1][0] << '\t' << m[2][0] << '\t' << m[3][0] << '\n'
       << m[0][1] << '\t' << m[1][1] << '\t' << m[2][1] << '\t' << m[3][1] << '\n'
       << m[0][2] << '\t' << m[1][2] << '\t' << m[2][2] << '\t' << m[3][2] << '\n'
       << m[0][3] << '\t' << m[1][3] << '\t' << m[2][3] << '\t' << m[3][3] << '\n';
}

std::ostream& operator << (std::ostream &os, const Quat_cam &q)
{
    const auto mat = q.view_xform();
    return os << mat;
}

bool are_equal(const glm::mat4 &m1, const glm::mat4 &m2, float epsilon)
{
    bool are_equal = true;
    for(auto i = 0u; are_equal && (i < 4); ++i)
    {
        are_equal = glm::all(glm::epsilonEqual(m1[i], m2[i], epsilon));
    }
    return are_equal;
}

/*
 * the idea of local transformation is that instead of having a global system and
 * apply (pre-multiply) all transformations relative to that we start from identity
 * and every transformation applied (post-multiplying) transforms the coordinate system
 * to a new one relative to that system. Say we've a view V of the scene and one of the
 * 6 DoF is changed, we've to form a new V' to map the World to; first V' should be
 * aligned to V and then V to world i.e. V' -> V -> World will give Mw->v'
 */
void move_X(float delta,
            Camera *cam,
            glm::mat4 *view_xform,
            Quat_cam *quat_cam)
{
    cam->slide(delta * cam->X());

#ifdef NON_OPT
    // since the new view starts from identity, its X-axis would just be basis i<1, 0, 0>
    // i.e. aligning V' to V first with V' (identity) as the basis and then aligning V to World
    const auto inv = glm::translate(glm::vec3{-delta, 0.0f, 0.0f});
    *view_xform = inv * *view_xform;
#else
    // although the above code is correct and also explains what happens conceptually
    // it can further be simplified to this since the translate vector would be <-delta, 0, 0>
    // it only affect the origin.X element of the matrix
    (*view_xform)[3][0] -= delta;
#endif

    quat_cam->slide(delta * quat_cam->X());
}

void move_Y(float delta,
            Camera *cam,
            glm::mat4 *view_xform,
            Quat_cam *quat_cam)
{
    cam->slide(delta * cam->Y());

#ifdef NON_OPT
    // since the new view starts from identity, its Y-axis would just be basis j<0, 1, 0>
    const auto inv = glm::translate(glm::vec3{0.0f, -delta, 0.0f});
    *view_xform = inv * *view_xform;
#else
    (*view_xform)[3][1] -= delta;
#endif

    quat_cam->slide(delta * quat_cam->Y());
}

void move_Z(float delta,
            Camera *cam,
            glm::mat4 *view_xform,
            Quat_cam *quat_cam)
{
    cam->slide(delta * cam->Z());

#ifdef NON_OPT
    // since the new view starts from identity, its Z-axis would just be basis k<0, 0, 1>
    const auto inv = glm::translate(glm::vec3{0.0f, 0.0f, -delta});
    *view_xform = inv * *view_xform;
#else
    (*view_xform)[3][2] -= delta;
#endif

    quat_cam->slide(delta * quat_cam->Z());
}

void renormalize(glm::mat4 *mat)
{
    glm::mat3 basis = glm::mat3{*mat};
    basis = glm::orthonormalize(basis);
    const auto T = (*mat)[3];
    *mat = glm::mat4{basis};
    (*mat)[3] = T;
}

/*
 * We want to make a camera that can move around, look in different directions, and maybe zoom in and out.
 * However, the clip volume can not be changed. It is always the same size, and in the same position. So,
 * instead of moving the camera, we must move the entire 3D scene so that it fits inside the clip volume
 * cube correctly. For example, if we want to rotate the camera to the right, we actually rotate the whole
 * world to the left. If we want to move the camera closer to the player, we actually move the player
 * closer to the camera. This is how “cameras” work in 3D, they transform the entire world so that it
 * fits into the clip volume and looks correct.
 * http://www.tomdalling.com/blog/modern-opengl/03-matrices-depth-buffering-animation/
 */
void rotate_X(float angle,
              Camera *cam,
              glm::mat4 *view_xform,
              Quat_cam *quat_cam)
{
    cam->rotate_X(angle);

    /*
       we've view_xform (Mw->v) already (this is also Tv->w); now the view needs to be rotated, instead of
       rotating the view's frame (represented in world space) and then forming Tw->v and finding the inverse
       of it (which is what gluLookAt does), we can imagine that the final system i.e. new rotated view v' is
       already the space we're; we need Mw->v' or Tv'->w; since we're in v' we now apply the opposite rotation
       to first merge with the previous frame v i.e. Tv'->v is now available, transforming the previous frame
       to world's frame is already in view_xform (Tv->w); concatenating Tv'->v Tv->w = Tv'->w = Mw->v'; notice
       the right (post) multiplication; these are easier to visualize as local transformations.
    */
    // since the new view starts from identity, rotation along X would just be the basis i
    const auto inv = glm::eulerAngleX(-angle);
    *view_xform = inv * *view_xform;

    // TODO: orthonormalization of the camera frame matrix to undo the effects of matrix creep isn't needed
    // after every change but only when enough error has accumulated
    renormalize(view_xform);

    quat_cam->rotate({1.0f, 0.0f, 0.0f}, angle);
}

void rotate_Y(float angle,
              Camera *cam,
              glm::mat4 *view_xform,
              Quat_cam *quat_cam)
{
    cam->rotate_Y(angle);

    // since the new view starts from identity, rotation along Y would just be the basis j
    const auto inv = glm::eulerAngleY(-angle);
    *view_xform = inv * *view_xform;

    quat_cam->rotate({0.0f, 1.0f, 0.0f}, angle);
}

void rotate_Z(float angle,
              Camera *cam,
              glm::mat4 *view_xform,
              Quat_cam *quat_cam)
{
    cam->rotate_Z(angle);

    // since the new view starts from identity, rotation along Z would just be the basis k
    const auto inv = glm::eulerAngleZ(-angle);
    *view_xform = inv * *view_xform;
    
    quat_cam->rotate({0.0f, 0.0f, 1.0f}, angle);
}

int main()
{
    // initializes camera with its view xform calculated using glmLookAt
    // eye =  (0, 0, 5), at = (0, 0, 0), up = <0, 1, 0>
    Camera cam;

    /*
     * hand-calculated view xform: just moves the view's origin to the world's origin. For the above configuration,
     * below matrix would be Mworld->view; this transform, when viewed as applied to the coordinate system
     * (post-multiplication/local coordinate) moves view's frame to align with world's frame.
     * Note that GLM takes these as columns
     */
    auto view_xform = glm::mat4{1.0f, 0.0f, 0.0f, 0.0f,     // col1
                                0.0f, 1.0f, 0.0f, 0.0f,     // col2
                                0.0f, 0.0f, 1.0f, 0.0f,     // col3
                                0.0f, 0.0f,-5.0f, 1.0f};    // col4
    /*
     * the Camera object can be replaced by just this one 4x4 matrix
     * it suffices for the camera manipulation on all 6 DoFs
     * this shows that matrices are worthy candidates for orientation representation
     */

    // initializes with camera origin at (0, 0, 5) and identity quaternion <1, (0, 0, 0)>
    Quat_cam quat_cam{{0.0f, 0.0f, 5.0f}};

    char ch = '\0';
    bool transformed = true;
    do
    {
        // compare the three orientation representations and continue if they're near equal
        if(transformed)
        {
            std::cout << cam.view_xform << '\n' << view_xform << '\n';
            const auto epsilon = 0.00001f;
            auto equal = are_equal(cam.view_xform, view_xform, epsilon);
            if (!equal)
                break;
            const auto quat_transform = quat_cam.view_xform();
            std::cout << quat_transform << '\n';
            equal = are_equal(view_xform, quat_transform, epsilon);
            if (!equal)
                break;
        }
        else
            transformed = true;

        const auto angle_delta = 0.2f;
        const auto slide_delta = 0.5f;

        std::cin >> ch;
        switch(ch)
        {
            case 'w':
                move_Y(-slide_delta, &cam, &view_xform, &quat_cam);
                std::cout << "Moving up\n";
                break;
            case 'x':
                move_Y(slide_delta, &cam, &view_xform, &quat_cam);
                std::cout << "Moving down\n";
                break;
            case 'a':
                move_X(-slide_delta, &cam, &view_xform, &quat_cam);
                std::cout << "Moving left\n";
                break;
            case 'd':
                move_X(slide_delta, &cam, &view_xform, &quat_cam);
                std::cout << "Moving right\n";
                break;
            case 's':
                move_Z(-slide_delta, &cam, &view_xform, &quat_cam);
                std::cout << "Moving forward\n";
                break;
            case 'z':
                move_Z(slide_delta, &cam, &view_xform, &quat_cam);
                std::cout << "Moving backward\n";
                break;
            case '4':
                rotate_Y(angle_delta, &cam, &view_xform, &quat_cam);
                std::cout << "Yaw left\n";
                break;
            case '6':
                rotate_Y(-angle_delta, &cam, &view_xform, &quat_cam);
                std::cout << "Yaw right\n";
                break;
            case '8':
                rotate_X(angle_delta, &cam, &view_xform, &quat_cam);
                std::cout << "Pitch up\n";
                break;
            case '2':
                rotate_X(-angle_delta, &cam, &view_xform, &quat_cam);
                std::cout << "Pitch down\n";
                break;
            case '1':
                rotate_Z(angle_delta, &cam, &view_xform, &quat_cam);
                std::cout << "Roll left\n";
                break;
            case '3':
                rotate_Z(-angle_delta, &cam, &view_xform, &quat_cam);
                std::cout << "Roll right\n";
                break;
            default:
                transformed = false;
        }
    } while(ch != 'q');
}
