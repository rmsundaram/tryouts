// https://www.glfw.org/faq.html#215---can-i-use-extension-loaders-with-glfw

#define GLFW_INCLUDE_NONE
#include <GLFW/glfw3.h>

#ifdef _WIN32
     // gl.h in Windows kit expects WINGDIAPI definition.
#    if !defined(WINGDIAPI) && defined(_WIN32)
#        define WINGDIAPI __declspec(dllimport)
#    endif
#    include <gl/gl.h>
     // Windows kit doesn't ship glext.h; manually define needed macros.
#    define GL_NUM_COMPRESSED_TEXTURE_FORMATS 0x86A2
#    define GL_COMPRESSED_TEXTURE_FORMATS 0x86A3
#elif defined(__APPLE__)
#    define GL_SILENCE_DEPRECATION 1
#    include <OpenGL/gl.h>
#    include <OpenGL/glext.h>
#else
#    include <GL/gl.h>
#    include <GL/glext.h>
#endif

#include <cassert>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <iostream>
#include <memory>

#define GL_NUM_EXTENSIONS 0x821D
#define GL_SHADING_LANGUAGE_VERSION 0x8B8C
typedef const GLubyte* (APIENTRY *PFNGLSTRINGI)(GLenum name, GLuint index);

int main(int argc, char** /*argv*/)
{
    glfwInit();

#ifdef __APPLE__
    // https://stackoverflow.com/q/19658745/183120
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    const auto window = glfwCreateWindow(100, 100, "GLversion", NULL, NULL);
    if (!window)
        std::cerr << "Failed to open GLFW window\n";
    glfwMakeContextCurrent(window);

    std::cout << "OpenGL Information\n==================\n";
    std::cout << "Vendor:       " << glGetString(GL_VENDOR)   << '\n';
    std::cout << "Renderer:     " << glGetString(GL_RENDERER) << '\n';
    std::cout << "Version:      " << glGetString(GL_VERSION)  << '\n';
    std::cout << "GLSL Version: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << "\n\n";
    std::cout << "Extensions\n~~~~~~~~~~\n";

    auto glGetStringi = (PFNGLSTRINGI) glfwGetProcAddress("glGetStringi");
    if (glGetStringi)
    {
        GLint n;
        glGetIntegerv(GL_NUM_EXTENSIONS, &n);
        for(auto i = 0; i < n; ++i)
        {
            const auto ext = glGetStringi(GL_EXTENSIONS, static_cast<unsigned>(i));
            std::cout << ext << '\n';
        }
    }
    else      // use the older deprecated way of getting extension strings
    {
        const char *cstrExts = reinterpret_cast<const char *>(glGetString(GL_EXTENSIONS));
        std::istringstream istrStream(cstrExts);
        std::istream_iterator<std::string> front(istrStream);
        std::istream_iterator<std::string> back;
        std::ostream_iterator<std::string> display(std::cout, "\n");
        std::copy(front, back, display);
    }

    // list supported compressed texture formats, if requested
    if (argc > 1)
    {
        GLint numCompressedFormats = 0;
        glGetIntegerv(GL_NUM_COMPRESSED_TEXTURE_FORMATS, &numCompressedFormats);
        std::unique_ptr<GLint[]> formats(new GLint[numCompressedFormats]);
        glGetIntegerv(GL_COMPRESSED_TEXTURE_FORMATS, reinterpret_cast<GLint*>(formats.get()));
        std::ostream_iterator<GLint> displayInts(std::cout, "\n");
        std::cout << "\nCompressed Texture Formats Supported\n"
                  <<   "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
        std::cout << std::hex;
        std::copy(formats.get(), formats.get() + numCompressedFormats, displayInts);
        std::cout << std::dec;
    }

    glfwDestroyWindow(window);
    glfwTerminate();
}
