cmake_minimum_required(VERSION 3.24)
project(GLversion)

include(FetchContent)

# Find or download GLFW3
set(GLFW_BUILD_DOCS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_TESTS OFF CACHE BOOL "" FORCE)
set(GLFW_BUILD_EXAMPLES OFF CACHE BOOL "" FORCE)
set(GLFW_INSTALL OFF CACHE BOOL "" FORCE)
FetchContent_Declare(
  glfw
  URL      https://github.com/glfw/glfw/archive/refs/tags/3.3.8.tar.gz
  URL_HASH MD5=55d99dc968f4cec01a412562a7cf851c
  FIND_PACKAGE_ARGS 3.0.1 NAMES glfw3
)
FetchContent_MakeAvailable(glfw)

find_package(OpenGL REQUIRED)

add_executable(${PROJECT_NAME}
  main.cpp
)
target_link_libraries(${PROJECT_NAME} PRIVATE
  glfw
  OpenGL::GL
)
target_compile_options(${PROJECT_NAME} PRIVATE
  $<$<CXX_COMPILER_ID:GNU,Clang,AppleClang>:-fdiagnostics-color -Wall -pedantic
  -Wno-missing-field-initializers -Wextra -Wcast-align -Wconversion -Wcast-qual
  -Wdouble-promotion -Wvector-operation-performance -Wno-div-by-zero -Wlogical-op>
)
target_compile_features(${PROJECT_NAME} PUBLIC cxx_std_17)
set_target_properties(${PROJECT_NAME} PROPERTIES
  CXX_STANDARD_REQUIRED ON
  CXX_EXTENSIONS OFF
)

target_link_options(${PROJECT_NAME} PRIVATE
  $<$<AND:$<CONFIG:Release>,$<CXX_COMPILER_ID:GNU,Clang,AppleClang>>:
  -static-libgcc -static-libstdc++>
)
