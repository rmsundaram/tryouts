This tool prints the OpenGL specs like maker, model, OpenGL and GLSL version, … along with the extensions supported by the video driver installed; make sure you've the latest drivers installed.  Also do not run this utility remotely as the driver will not exhibit the true capabilities of the card.

Pre-built Win32 binary is available in the [Downloads section](https://bitbucket.org/rmsundaram/tryouts/downloads).
