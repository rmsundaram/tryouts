#include "mainwindow.h"

#include <QApplication>
#include <QSurfaceFormat>

int main(int argc, char *argv[])
{
    // This needs to be set before QApplication is created.
    // https://doc.qt.io/qt-6/qopenglwidget.html#details
    QSurfaceFormat format;
    format.setRenderableType(QSurfaceFormat::OpenGL);
    format.setSamples(4);
    format.setDepthBufferSize(24);
    // Setting to OpenGL 3.3 leads to blank screen since we don’t use VAOs.
    //format.setVersion(3, 3);
    format.setProfile(QSurfaceFormat::CoreProfile);
    QSurfaceFormat::setDefaultFormat(format);
    // Setup sharing between QOpenGLWidget's OpenGL contexts.
    QCoreApplication::setAttribute(Qt::AA_ShareOpenGLContexts, true);

    QApplication a(argc, argv);
    MainWindow w;
    w.show();
    return a.exec();
}
