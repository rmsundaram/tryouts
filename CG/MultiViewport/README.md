`QOpenGLContext` [2][] states `setShareContext` should be called _before_ `create`d.  Hence it’s only applicable secondary contexts created outside, not for `QOpenGLWidget` which has its own `QOpenGLContext` created internally by Qt.  Hence for sharing the right thing to do is set the `QCoreApplication` attribute `AA_ShareOpenGLContexts` as mentioned in [3][].

> To set up sharing between `QOpenGLWidget` instances belonging to different windows, set the `Qt::AA_ShareOpenGLContexts` application attribute before instantiating `QApplication`. This will trigger sharing between all `QOpenGLWidget` instances without any further steps.

# References

1. Setting vertical stack layout on main window without any addition widgets [1][]

[1]: https://stackoverflow.com/questions/1508939/qt-layout-on-qmainwindow
[2]: https://doc.qt.io/qt-6/qopenglcontext.html#context-resource-sharing
[3]: https://doc.qt.io/qt-6/qopenglwidget.html#details
