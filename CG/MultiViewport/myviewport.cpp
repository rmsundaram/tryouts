#include "myviewport.h"

#include <iostream>
#include <type_traits>
#include <QtMath>

QOpenGLShaderProgram MyViewport::program;

MyViewport::MyViewport(QWidget* parent) : QOpenGLWidget(parent) {
    // Default surface format already set globally; refer main().
}

MyViewport::~MyViewport() {
    // https://code.qt.io/cgit/qt/qtbase.git/tree/examples/opengl/cube/mainwidget.cpp?h=6.4
    makeCurrent();
    // delete owned resources like geometries, textures, etc.
    doneCurrent();
}

void MyViewport::setTransform(QMatrix4x4 view) {
    m_V = view;
    setPV(QVector2D(width(), height()));
}

void MyViewport::setClearColour(uint32_t colour) {
    m_clearColour = colour;
}

void MyViewport::initializeGL() {
    this->initializeOpenGLFunctions();
    float r = ((m_clearColour & 0xff'00'00'00) >> 24) / 256.0f;
    float g = ((m_clearColour & 0x00'ff'00'00) >> 16) / 256.0f;
    float b = ((m_clearColour & 0x00'00'ff'00) >> 8) / 256.0f;
    float a = (m_clearColour & 0x00'00'00'ff) / 256.0f;
    this->glClearColor(r, g, b, a);
    this->glEnable(GL_MULTISAMPLE);

    // Not checking this->context()->currentContext() since it's guaranteed to
    // be this widget's context within initializeGL, resizeGL and paintGL.
    // https://doc.qt.io/qt-6/qopenglwidget.html#details
    std::cout << "OpenGL version: "
              << this->context()->format().version().first
              << '.'
              << this->context()->format().version().second
              << std::endl;
    std::cout << "This context:          "
              << this->context() << std::endl
              << "Global shared context: "
              << QOpenGLContext::globalShareContext() << std::endl;

    // NOTE: There isn't a possibility for race since both QOpenGLWidgets are
    // initialized by the same UI thread (in function Ui_MainWindow::setupUi).
    if (!program.isLinked()) {
        const char* vs = R"raw(
            #version 330

            layout (location = 0) in vec3 position;

            uniform mat4 PV;

            void main() {
                gl_Position = PV * vec4(position, 1.0);
            }
        )raw";
        const char* fs = R"raw(
            #version 330

            out vec4 colour;

            void main() {
                colour = vec4(0.0, 0.0, 0.0, 1.0);
            }
        )raw";
        program.addShaderFromSourceCode(QOpenGLShader::Vertex, vs);
        program.addShaderFromSourceCode(QOpenGLShader::Fragment, fs);
        assert(program.link());
    }
}

static
QVector2D adjustAspectRatio(QVector2D dimensions,
                            float expectedAspectRatio) {
    const float aspectRatio = dimensions.x() / dimensions.y();
    auto adjustedDims = dimensions;
    if (aspectRatio > expectedAspectRatio)
      adjustedDims.setX(adjustedDims.y() * expectedAspectRatio);
    else
      adjustedDims.setY(adjustedDims.x() / expectedAspectRatio);
    return adjustedDims;
}

void MyViewport::resizeGL(int w, int h) {
    const QVector2D unscaledDim(w, h);
    setPV(unscaledDim);
    constexpr float TARGET_ASPECT_RATIO = 1.3333333334f;
    const float aspectRatio = m_adjustAspect ? TARGET_ASPECT_RATIO
                                             : ((float)w/h);
    const auto retinaScale = devicePixelRatio();
    const QVector2D adjustedDim = retinaScale *
      adjustAspectRatio(unscaledDim, aspectRatio);

    // Use scissor test to crop viewport to desired aspect ratio.
    // Set viewport and scissor box to same values; refer
    // https://gamedev.stackexchange.com/q/40704
    const auto orgDim = retinaScale * unscaledDim;
    const auto adjustedOrigin = 0.5 * (orgDim - adjustedDim);

    GLfloat oldClear[4];
    this->glGetFloatv(GL_COLOR_CLEAR_VALUE, oldClear);
    this->glClearColor(0.f, 0.f, 0.f, 1.0f);
    this->glDisable(GL_SCISSOR_TEST);
    this->glClear(GL_COLOR_BUFFER_BIT);
    // use glEnablei to enable it specifically for one viewport
    // https://www.khronos.org/opengl/wiki/Scissor_Test
    this->glEnable(GL_SCISSOR_TEST);
    this->glClearColor(oldClear[0], oldClear[1], oldClear[2], oldClear[3]);
    this->glViewport(adjustedOrigin.x(),
                     adjustedOrigin.y(),
                     adjustedDim.x(),
                     adjustedDim.y());
    this->glScissor(adjustedOrigin.x(),
                    adjustedOrigin.y(),
                    adjustedDim.x(),
                    adjustedDim.y());
}

void MyViewport::setPV(QVector2D dimension) {
    const float aspectRatio = dimension.x() / dimension.y();
    QMatrix4x4 proj;
    // NOTE: use aspect_ratio as we want the TARGET_ASPECT_RATIO only to crop.
    // Using TARGET_ASPECT_RATIO for projection leads to stretched renders.
    proj.perspective(qRadiansToDegrees(M_PI_4), aspectRatio, 1.0f, 100.0f);
    m_PV = proj * m_V;
}

void MyViewport::paintGL() {
    this->glClear(GL_COLOR_BUFFER_BIT);
    program.bind();
    // NOTE: Not using VAO as QOpenGLFunctions provides cross-platform
    // access to OpenGL ES 2.0 API which lacks glGenVertexArrays.
    // X →, Y ↑, Z comes out of the screen (chirality: right)
    const float vertices[] = {
        0.0f, 0.8f, 0.0f,
       -0.8f,-0.8f, 0.0f,
        0.8f,-0.8f, 0.0f,
    };

    const auto attribPos = program.attributeLocation("position");
    const auto uniPV = program.uniformLocation("PV");
    program.setUniformValue(uniPV, m_PV);

    this->glVertexAttribPointer(attribPos, 3, GL_FLOAT, GL_FALSE, 0, vertices);
    this->glEnableVertexAttribArray(attribPos);
    this->glDrawArrays(GL_TRIANGLES, 0, std::extent_v<decltype(vertices)>/3);
    this->glDisableVertexAttribArray(attribPos);
    program.release();
    this->update();
}

void MyViewport::HandleLocChanged(Qt::DockWidgetArea area) {
    // Update to fix black screen renders in OpenGL surface upon re-docking.
    update();
}

void MyViewport::ToggleFixAspectRatio(int value) {
    m_adjustAspect = !m_adjustAspect;
    resizeGL(width(), height());
    // BUG: Crop shows up only after resizing the window containing QSurface.
    // Calling `update()` or `repaint()` doesn't help.
    update();
}
