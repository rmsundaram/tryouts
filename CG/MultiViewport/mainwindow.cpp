#include "mainwindow.h"
#include "./ui_mainwindow.h"

#include <QMatrix4x4>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow) {
    ui->setupUi(this);

    // set states for different viewports; these are called before initalizeGL.
    ui->viewportMain->setClearColour(0xffffffff);
    ui->viewportDocked->setClearColour(0x8c8c8cff);

    QMatrix4x4 mainCam;
    mainCam.lookAt(QVector3D(3.0f,-4.0f, 5.0f) /*eye*/,
                   QVector3D(0.f, 0.5f, 0.f) /*at*/,
                   QVector3D(0.0f, 0.0f, 1.0f) /*up*/);
    ui->viewportMain->setTransform(mainCam);

    QMatrix4x4 dockCam;
    dockCam.lookAt(QVector3D(0.0f, 0.0f, 5.0f) /*eye*/,
                   QVector3D(0.f, 0.0f, 0.f) /*at*/,
                   QVector3D(0.0f, 1.0f, 0.0f) /*up*/);
    ui->viewportDocked->setTransform(dockCam);
}

MainWindow::~MainWindow() {
    delete ui;
}
