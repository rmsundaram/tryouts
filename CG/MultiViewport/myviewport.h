#ifndef MYVIEWPORT_H
#define MYVIEWPORT_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>
#include <QMatrix4x4>
#include <cstdint>

#include <QDockWidget>
#include <iostream>

class MyViewport : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT
public:
    MyViewport(QWidget* parent);
    ~MyViewport();

    void setClearColour(uint32_t colour);
    void setTransform(QMatrix4x4 view);

public slots:
    void HandleLocChanged(Qt::DockWidgetArea area);
    void ToggleFixAspectRatio(int value);

protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override;

private:
    void setPV(QVector2D dimension);

    static QOpenGLShaderProgram program;

    QMatrix4x4 m_V;
    QMatrix4x4 m_PV;
    uint32_t m_clearColour = 0u;
    bool m_adjustAspect = false;
};

#endif // MYVIEWPORT_H
