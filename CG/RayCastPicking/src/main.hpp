#ifndef __MAIN_HPP__
#define __MAIN_HPP__

inline void window_error(int, const char *error_msg)
{
    throw std::runtime_error(error_msg);
}

// Camera with 6 DoF (free-roaming robot); slide and rotate on all 3 axes
// the implmentation here is optimized lookup cam_manip_sans_lookat.cpp
// for a straight forward implmentation to understand what's going on
struct Camera
{
    // world to view transform
    glm::mat4 view_xform;

    // projection parameters
    float FoV, aspect_ratio, near, far;
    glm::mat4 projection;

    // manipulation constants in per millisecond units
    static constexpr float slide_offset = 0.007f;
    static constexpr float rot_angle = 0.0015f;

    Camera(float FoV,
           float aspect_ratio,
           float near,
           float far) :
        view_xform{1.0f, 0.0f,  0.0f, 0.0f,   // col 1
                   0.0f, 1.0f,  0.0f, 0.0f,   // col 2
                   0.0f, 0.0f,  1.0f, 0.0f,   // col 3
                   0.0f, 0.0f, -5.0f, 1.0f},  // col 4
        FoV{FoV},
        aspect_ratio{aspect_ratio},
        near{near},
        far{far},
        projection(glm::perspective(FoV, aspect_ratio, near, far))
    {
    }

    void reset()
    {
        view_xform = glm::mat4{1.0f, 0.0f,  0.0f, 0.0f,   // col 1
                               0.0f, 1.0f,  0.0f, 0.0f,   // col 2
                               0.0f, 0.0f,  1.0f, 0.0f,   // col 3
                               0.0f, 0.0f, -5.0f, 1.0f};
    }

    void increase_FoV()
    {
        FoV += 0.002f;
        projection = glm::perspective(FoV, aspect_ratio, near, far);
    }

    void decrease_FoV()
    {
        FoV -= 0.002f;
        projection = glm::perspective(FoV, aspect_ratio, near, far);
    }

    void slide_X(float delta)
    {
        view_xform[3][0] -= delta;
    }

    void slide_Y(float delta)
    {
        view_xform[3][1] -= delta;
    }

    void slide_Z(float delta)
    {
        view_xform[3][2] -= delta;
    }

    void rotate_X(float angle)
    {
        const auto inv = glm::eulerAngleX(-angle);
        view_xform = inv * view_xform;
    }

    void rotate_Y(float angle)
    {
        const auto inv = glm::eulerAngleY(-angle);
        view_xform = inv *view_xform;
    }

    void rotate_Z(float angle)
    {
        const auto inv = glm::eulerAngleZ(-angle);
        view_xform = inv * view_xform;
    }
};

struct Selection
{
    const glm::vec4 sel_colour = glm::vec4{1.0f, 1.0f, 1.0f, 1.0f};
    size_t index = 0u;
    float hit_dist = std::numeric_limits<float>::max();
    glm::vec4 prev_colour;
};

struct Scene
{
    GLenum rendering_mode = GL_FILL;

    Camera cam;
    Selection sel;

    // actors
    std::vector<Primitives::Sphere> spheres;
    std::vector<Primitives::Box> boxes;
    std::vector<Primitives::PlaneSegment> plane_segs;

    // mesh data
    std::vector<Primitives::Mesh> meshes;

    // constructor
    Scene(float FoV,
          float aspect_ratio,
          float near,
          float far) : cam{FoV, aspect_ratio, near, far}
    {
    }
};

#endif  // __MAIN_HPP__
