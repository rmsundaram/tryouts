/*
 * Techniques learnt:
 *  1.    Constructing an icosphere mesh
 *  2.    Strict-weak ordering of nD vectors lexicographically
 *  3.    Ray/AABB intersection testing (Kay-Kajiya's slabs method)
 *  4.    Ray/Sphere intersection testing
 *  5.    Plane and plane segment/Ray intersection testing
 *  6.    Using ms/f instead of fps; the latter is non-linear and not intuitive
 *  7.    Picking by Ray Casting (Essential Math, §6.6)
 *  8.    Wrapped buffers, VAOs, etc. so that glDelete* needn't be called
 *  9.    Using VAOs for each model which has the VBO and IBO data coupled
 * 10.    Representing primitives and their meshes independently
 * 11.    Custom SDL format instead of hard coding actors of the scene
 * 12.    Common mesh structure for rendering objects of different types
 *
 * ToDo:
 *      Have transform and colour seprately instead of having them in the mesh structure true to DoD)
 *      Perhaps release all the mesh data once uploaded to the GPU; just have data required for rendering
 *      Since a sphere, when formed by tessellating an icosahedron, will go beyond 255 vertices and thus
 *         the data type used for each index element was bumped from byte to short in Primitives::Mesh.
 *         Try to pass the right GL_type based on the count of vertices for the indices in IBO (may be
 *         using Boost.Variant) as passing a smaller type is always better to conserve memory bandwidth
 *         The total count of indices may be arbitarily large but still the VBO element data type can be
 *         maintained as short using the glDrawElementsBaseVertex and friends
 *           http://www.opengl.org/wiki/GLAPI/glDrawElementsBaseVertex
 *           http://stackoverflow.com/a/26755538/183120
 *           http://arcsynthesis.org/gltut/Positioning/Tut05 Optimization Base Vertex.html
 *           http://stackoverflow.com/questions/696399/how-to-put-different-template-types-into-one-vector
 *      Remove all usages of vec3 and make them vec4 (may be favourable for auto vectorization)
 *      Change all float relational operator comaprison to helper functions which check for equality too
 */

#include "precomp.hpp"
#include "Util.hpp"
#include "Dataless_wrapper.hpp"
#include "GL_res_wrap.hpp"
#include "GL_util.hpp"
#include "Shader_util.hpp"
#include "Primitives.hpp"
#include "main.hpp"
#include <chrono>

using GLFW_wrapper = Dataless_wrap<decltype(&glfwTerminate), glfwTerminate>;

constexpr unsigned screen_width  = 1024u, screen_height = 768u;
constexpr float aspect_ratio = static_cast<float>(screen_width) / screen_height;
const float FoV = glm::quarter_pi<float>();
const char* const app_title = "Ray-Casting Picker";

GLFWwindow* setup_context(uint32_t width, uint32_t height)
{
    /*
     * without setting the version (1.0) or setting it < 3.2, requesting for core will fail
     * since context profiles only exist for OpenGL 3.2+; likewise forward compatibility
     * only exist from 3.0 onwards. 3.0 marked deprecated, 3.1 removed deprecated (except
     * wide lines), 3.2 reintroduced deprecated under compatibility profile
     */
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE,
                   GLFW_OPENGL_CORE_PROFILE);
    // since core only has undeprecated features (except wide lines), making the context
    // forward-compatible is moot; only wide lines (the only deprecated feature) will be removed
    // http://www.opengl.org/wiki/Core_And_Compatibility_in_Contexts#Forward_compatibility
    // glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

#ifndef NDEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

    auto window = glfwCreateWindow(width,
                                   height,
                                   app_title,
                                   nullptr,
                                   nullptr);
    check(window != nullptr, "Failed to create window");
#ifndef NDEBUG
    int w, h;
    glfwGetWindowSize(window, &w, &h);
    assert((w == screen_width) && (h == screen_height));
#endif
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
#ifdef _WIN32
    // http://stackoverflow.com/q/16285546
    // GLFW doesn't honour call to VSYNC when DWM compositing is enabled, which is practically all machines today;
    // turn it on using the using WGL_EXT_swap_control extension on Win32
    // TODO: do this for other platforms too
    typedef int (APIENTRY *PFNWGLSWAPINTERVALEXTPROC)(int interval);
    auto wglSwapInterval = reinterpret_cast<PFNWGLSWAPINTERVALEXTPROC>(glfwGetProcAddress("wglSwapIntervalEXT"));
    if (wglSwapInterval)
    {
        typedef int (APIENTRY *PFNWGLGETSWAPINTERVALEXTPROC)(void);
        auto wglGetSwapInterval =
            reinterpret_cast<PFNWGLGETSWAPINTERVALEXTPROC>(glfwGetProcAddress("wglGetSwapIntervalEXT"));
        // if the get function isn't there or is returning 0
        if (!wglGetSwapInterval || !wglGetSwapInterval())
            wglSwapInterval(1);
    }
#endif
    return window;
}

#ifndef NDEBUG

APIENTRY
void gl_debug_logger(GLenum source,
                     GLenum type,
                     GLuint id,
                     GLenum severity,
                     GLsizei /*length*/,
                     const char *msg,
                     const void *file_ptr)
{
    std::stringstream ss;
    ss << "GLError 0x"
       << std::hex << id << std::dec
       << ": "
       << msg <<
       " [source=";

    const char *sources[] = {
                                "API",
                                "WINDOW_SYSTEM",
                                "SHADER_COMPILER",
                                "THIRD_PARTY",
                                "APPLICATION",
                                "OTHER",
                                "UNDEFINED"
                            };
    size_t index = Array::item_index(source,
                                     GL_DEBUG_SOURCE_API_ARB,
                                     GL_DEBUG_SOURCE_OTHER_ARB,
                                     Array::max_index(sources));
    ss << sources[index];
    if (Array::max_index(sources) == index)
    {
        ss << " (" << source << ")";
    }

    const char *types[] = {
                              "ERROR",
                              "DEPRECATED_BEHAVIOR",
                              "UNDEFINED_BEHAVIOR",
                              "PORTABILITY",
                              "PERFORMANCE",
                              "OTHER",
                              "UNDEFINED"
                          };
    index = Array::item_index(type,
                              GL_DEBUG_TYPE_ERROR_ARB,
                              GL_DEBUG_TYPE_OTHER_ARB,
                              Array::max_index(types));
    ss << " type=" << types[index];
    if (Array::max_index(types) == index)
    {
        ss << " (" << type << ")";
    }

    const char *severities[] = {
                                   "HIGH",
                                   "MEDIUM",
                                   "LOW",
                                   "UNKNOWN"
                               };
    // as per the extension, HIGH is the base (smaller) value
    index = Array::item_index(severity,
                              GL_DEBUG_SEVERITY_HIGH_ARB,
                              GL_DEBUG_SEVERITY_LOW_ARB,
                              Array::max_index(severities));
    ss << " severity=" << severities[index];
    if (Array::max_index(severities) == index)
    {
        ss << " (" << severity << ")";
    }
    ss << "]";

    const auto log = ss.str();
    FILE *out_file = reinterpret_cast<FILE*>(const_cast<void*>(file_ptr));
    fprintf(out_file, "%s\n", log.c_str());

    check(severity != GL_DEBUG_SEVERITY_HIGH_ARB, "High severity GL error logged");
}

void setup_debug(bool enable)
{
    if(glewIsSupported("GL_ARB_debug_output"))
    {
        glDebugMessageCallbackARB(enable ? gl_debug_logger : nullptr, stderr);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
        check(GL_NO_ERROR == glGetError(), "Unable to set synchronous debug output");
    }
}
#endif  // NDEBUG

void handle_key(GLFWwindow *window,
                int key,
                int /*scancode*/,
                int action,
                int /*mods*/)
{
    auto scene = reinterpret_cast<Scene*>(glfwGetWindowUserPointer(window));
    assert(scene);

    if(GLFW_PRESS == action)
    {
        switch(key)
        {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, GL_TRUE);
            break;
        case GLFW_KEY_R:
            scene->cam.reset();
            break;
        case GLFW_KEY_0:
            scene->rendering_mode = (scene->rendering_mode == GL_LINE) ? GL_FILL : GL_LINE;
            break;
        }
    }
}

template <typename Container, typename TesterFunc>
void hit_test(const Primitives::Ray &ray,
              const Container &v,
              TesterFunc test,
              float min_dist,
              float max_dist,
              float *dist,
              size_t base_index,
              size_t *index)
{
    for(auto i = 0u; i < v.size(); ++i)
    {
        float t;
        if(test(ray, v[i], &t) &&
/*
 * when objects are too close to the camera it gets clipped by the near plane; however, the clipped
 * part of the object would be between the ray origin and the near plane; thus the ray first hits the
 * invisible side; there by the hit value would be less than near plane hit value and thus rejected
 */
               (t >= min_dist) &&
               (t <= max_dist) &&
               (t < *dist))
        {
            *dist = t;
            *index = base_index + i;
        }
    }
}

void hit_test(Scene *scene,
              const Primitives::Ray &ray,
              float min_dist,
              float max_dist,
              float *hit_dist,
              size_t *hit_index)
{
    size_t current_base = 0u;
    *hit_dist = std::numeric_limits<float>::max();
    hit_test(ray,
             scene->spheres,
             Primitives::ray_sphere_intersect,
             min_dist,
             max_dist,
             hit_dist,
             current_base,
             hit_index);
    current_base += scene->spheres.size();
    hit_test(ray,
             scene->boxes,
             Primitives::ray_box_intersect,
             min_dist,
             max_dist,
             hit_dist,
             current_base,
             hit_index);
    current_base += scene->boxes.size();
    hit_test(ray,
             scene->plane_segs,
             Primitives::ray_plane_segment_intersect,
             min_dist,
             max_dist,
             hit_dist,
             current_base,
             hit_index);
}

void select(Scene *scene,
            float hit_dist,
            size_t hit_index)
{
    // deselect if something's already selected
    if(scene->sel.hit_dist != std::numeric_limits<float>::max())
    {
        scene->meshes[scene->sel.index].colour = scene->sel.prev_colour;
        scene->sel.hit_dist = std::numeric_limits<float>::max();        // reset distance
    }

    // select if something was hit
    if(hit_dist != std::numeric_limits<float>::max())
    {
        scene->sel.prev_colour = scene->meshes[hit_index].colour;
        scene->meshes[hit_index].colour = scene->sel.sel_colour;
        scene->sel.index = hit_index;
        scene->sel.hit_dist = hit_dist;
    }
}

void pick(float x,
          float y,
          Scene *scene)
{
    /*
     * for the z (world) coordinate of the pick point we need the focal length
     * which is cot (FoV/2) but since the perspective projection matrix already has it
     * we can reuse the value instead of computing it; this optimisation is discussed in
     * Frank D. Luna's excellent book; it explains picking quite comprehensively
     */
    const auto focal_length = scene->cam.projection[1][1];
    // const auto focal_length = glm::cot(scene->FoV / 2.0f);

    /*
     * in view space this point generated would be (x, y, z, 1); in that space
     * subtracting origin (0, 0, 0, 1) from that point would give the same point
     * with the w coordinate becoming 0; thus this is the ray in view space
     */
    const glm::vec4 view_ray(((2.0f * x / screen_width) - 1.0f) * scene->cam.aspect_ratio,
                             1.0f - (2.0f * y / screen_height),
                             -focal_length,
                             0.0f);
    /*
     * we've Mw->v, we need its inverse Mv->w; since Mw->v = TR
     * Mv->w = R^-1 T^-1; this is done by GLM's affineInverse; although just transposing the upper
     * 3x3 matrix in Mw->v would suffice for transforming the ray, we need the affine inverse for
     * transforming the ray origin i.e. camera location in world space
     */
    const auto inv_view = glm::affineInverse(scene->cam.view_xform);
    // since we're transforming a ray, the last column doesn't matter, but we use it for ray origin
    const auto ray_dir = glm::normalize(glm::vec3(inv_view * view_ray));
    const auto ray_org = glm::vec3(inv_view[3]);    // camera locaton in world space
    const Primitives::Ray pick_ray{ray_org, ray_dir};
    /*
     * ths ray originates from camera, goes throw the click point on the view plane and extends to infinity;
     * however, bounding it within near and far is optimal since objects only within these are visible
     *
     * normal is the same for both planes; the NEGATIVE distance from the origin along normal differs;
     * constructing the planes in view space is easier, we'll then map them to world space
     * 5.2.3 Transforming Planes from Lengyel's Skeleton book gives (F^-1)^T; however, we
     * needn't find the inverse of Mview->world since we already have it Mworld->view = view_xform
     */
    const auto f_inv_trans = glm::transpose(scene->cam.view_xform);
    const Primitives::Plane near_plane{f_inv_trans * glm::vec4(0.0f, 0.0f, -1.0f, -scene->cam.near)};
    const Primitives::Plane far_plane{f_inv_trans * glm::vec4(0.0f, 0.0f, -1.0f, -scene->cam.far)};
    float t_near, t_far;
#ifdef NDEBUG
    Primitives::ray_plane_intersect(pick_ray, near_plane, &t_near);
    Primitives::ray_plane_intersect(pick_ray, far_plane, &t_far);
#else
    const auto near_hit = Primitives::ray_plane_intersect(pick_ray, near_plane, &t_near);
    const auto far_hit = Primitives::ray_plane_intersect(pick_ray, far_plane, &t_far);
    assert(near_hit && far_hit);    // ray should always hit these 2 planes
#endif

    float hit_dist;
    size_t hit_index;
    hit_test(scene, pick_ray, t_near, t_far, &hit_dist, &hit_index);
    select(scene, hit_dist, hit_index);
}

void handle_mouse(GLFWwindow *window,
                  int button,
                  int action,
                  int /*mods*/)
{
    if ((GLFW_MOUSE_BUTTON_LEFT == button) && (GLFW_RELEASE == action))
    {
        double xpos, ypos;
        glfwGetCursorPos(window, &xpos, &ypos);
        auto scene = reinterpret_cast<Scene*>(glfwGetWindowUserPointer(window));
        assert(scene);
        pick(static_cast<float>(xpos),
             static_cast<float>(ypos),
             scene);
    }
}

void handle_input(GLFWwindow *window,
                  Camera *cam,
                  float ticks)
{
    const auto slide_offset = ticks * Camera::slide_offset;
    const auto rot_angle = ticks * Camera::rot_angle;

    // rotation
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_UP))
        cam->rotate_X(rot_angle);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_DOWN))
        cam->rotate_X(-rot_angle);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_LEFT))
        cam->rotate_Y(rot_angle);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_RIGHT))
        cam->rotate_Y(-rot_angle);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_PERIOD))
        cam->rotate_Z(rot_angle);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_COMMA))
        cam->rotate_Z(-rot_angle);

    // translation
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_D))
        cam->slide_X(slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_A))
        cam->slide_X(-slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_W))
        cam->slide_Y(slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_X))
        cam->slide_Y(-slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_Z))
        cam->slide_Z(slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_S))
        cam->slide_Z(-slide_offset);

    // FoV
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_KP_ADD))
        cam->increase_FoV();
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_KP_SUBTRACT))
        cam->decrease_FoV();

    glfwPollEvents();
}

void init_rendering()
{
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
// enabling culling culls out the plane when trying to view it from underneath hence disabled
//  glEnable(GL_CULL_FACE);
// the reason is that a plane, unlike a model, has no inside and hence culling doesn't make sense here
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
}

void upload_meshes(const std::vector<Primitives::Mesh> &meshes,
                   std::vector<GL::VAO> *VAOs)
{
    const size_t attrib_lengths[] = { 3u };
    VAOs->reserve(meshes.size());
    for(size_t i = 0u; i < meshes.size(); ++i)
    {
        GL::Buffer vbo = GL::upload_data(meshes[i].vertices, GL_ARRAY_BUFFER);
        GL::Buffer ibo = GL::upload_data(meshes[i].indices, GL_ELEMENT_ARRAY_BUFFER);
        VAOs->emplace_back(GL::setup_attributes<GLfloat>(vbo, attrib_lengths, ibo));
    }
}

void render(GLuint program,
            const Scene &scene,
            const std::vector<GL::VAO> &VAOs,
            GLuint xform_id,
            GLuint colour_id)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPolygonMode(GL_FRONT_AND_BACK, scene.rendering_mode);
    glUseProgram(program);
    const auto PV = scene.cam.projection * scene.cam.view_xform;
    for(auto i = 0u; i < scene.meshes.size(); ++i)
    {
        glBindVertexArray(VAOs[i]);
        const auto PVM = PV * scene.meshes[i].model_xform;
        glUniformMatrix4fv(xform_id, 1, GL_FALSE, &PVM[0][0]);
        glUniform4fv(colour_id, 1, &scene.meshes[i].colour[0]);
        glDrawElements(GL_TRIANGLES,
                       static_cast<GLint>(scene.meshes[i].indices.size()),
                       GL::type_id(decltype(scene.meshes[i].indices)::value_type{}),
                       0u);
        glBindVertexArray(0u);
    }
    glUseProgram(0);
}

void update_frame_time(std::chrono::milliseconds *ticks,
                       unsigned *frames_rendered,
                       GLFWwindow *window)
{
    ++*frames_rendered;
    if(ticks->count() >= 1000)
    {
        std::ostringstream ss;
        ss << app_title << " ~ " << (1000.0 / *frames_rendered) << "ms/frame";
        const auto str_mpf = ss.str();
        glfwSetWindowTitle(window, str_mpf.c_str());
        *frames_rendered = 0;
        *ticks = std::chrono::milliseconds{0};
    }
}

int main(int /*argc*/, char** /*argv*/)
{
    glfwSetErrorCallback(window_error);
    const auto glfw = std::move(GLFW_wrapper{Check_return<decltype(&glfwInit),
                                                          glfwInit,
                                                          decltype(GL_TRUE),
                                                          GL_TRUE>{}});
    // RAII warpping isn't mandatory here, since glfwTerminate destroys open windows, if any remain
    std::unique_ptr<GLFWwindow,
                    decltype(&glfwDestroyWindow)> main_wnd_ptr{setup_context(screen_width, screen_height),
                                                               glfwDestroyWindow};

    glewExperimental = GL_TRUE;
    auto err = glewInit();
    check(GLEW_OK == err, reinterpret_cast<const char*>(glewGetErrorString(err)));
    glGetError();       // to clear error due to GLEW bug #120

#ifndef NDEBUG
    setup_debug(true);
#endif

    Scene scene{FoV,
                aspect_ratio,
                1.0f,
                100.f};
    Primitives::parse_SDL("data/scene.sdl",
                          &scene.spheres,
                          &scene.boxes,
                          &scene.plane_segs,
                          &scene.meshes);

    std::vector<GL::VAO> VAOs;
    upload_meshes(scene.meshes, &VAOs);

    // http://www.parashift.com/c++-faq-lite/memfnptr-vs-fnptr.html
    // according to C++ FAQ, you cannot pass a member function to a C callback; passing a functor, lambda, boost::bind,
    // etc. is for a C++ callback implemented with templates; instead use the void* user_data allowed in the callback;
    // GLFW gives GLFWwindow->SetWindowUserPointer for the same
    glfwSetWindowUserPointer(main_wnd_ptr.get(), reinterpret_cast<void*>(&scene));
    glfwSetKeyCallback(main_wnd_ptr.get(), handle_key);
    glfwSetMouseButtonCallback(main_wnd_ptr.get(), handle_mouse);

    GL::Program program{GL::setup_program("shaders/p_vs.glsl",
                                          "shaders/col_fs.glsl")};
    const auto colour_id = glGetUniformLocation(program, "solid_col");
    assert(colour_id != -1);
    const auto xform_id = glGetUniformLocation(program, "PVM");
    assert(xform_id != -1);

    init_rendering();
    unsigned frames = 0u;
    std::chrono::milliseconds since_last_sec;
    std::chrono::steady_clock::time_point now, earlier;
    while(!glfwWindowShouldClose(main_wnd_ptr.get()))
    {
        now = std::chrono::steady_clock::now();
        auto const elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(now - earlier);
        auto const ticks = elapsed.count();
        render(program,
               scene,
               VAOs,
               xform_id,
               colour_id);
        glfwSwapBuffers(main_wnd_ptr.get());
        handle_input(main_wnd_ptr.get(), &scene.cam, static_cast<float>(ticks));
        since_last_sec += elapsed;
        update_frame_time(&since_last_sec, &frames, main_wnd_ptr.get());
        earlier = now;
    }
}
