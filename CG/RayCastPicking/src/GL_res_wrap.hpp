#ifndef GL_RES_WRAP_HPP
#define GL_RES_WRAP_HPP

namespace GL
{

// incomplete struct for non-integral types
template <typename T_res,
          typename T_release_func,
          T_release_func func,
          typename Enable = void>
struct Res_code;

// actual implementation
template <typename T_res,
          typename T_release_func,
          T_release_func func>
struct Res_code <T_res,
                 T_release_func,
                 func,
                 typename std::enable_if<std::is_integral<T_res>::value>::type>
{
    Res_code(T_res name_ = 0) : name{name_}
    {
    }

    Res_code(Res_code &&that) noexcept : name{that.name}
    {
        that.name = 0;
    }

    Res_code& operator=(Res_code &&that)
    {
        std::swap(name, that.name);
        return *this;
    }

    // by delaring the move contructor this is already non-copyable
    // no need to explicitly mention it

    operator T_res()  const
    {
        assert(name);
        return name;
    }

    void release()
    {
        *this = std::move(typename std::remove_reference<decltype(*this)>::type{});
    }

    ~Res_code()
    {
        if(name)
            (*func)(name);
    }

private:
    T_res name;
};


// although this can be used for glDeleteShader and glDeleteProgram
// it cannot be used for glDeleteBuffers, glDeleteVertexArrays or glDeleteTextures
// since they expect array of resources; for these below wrapper works

// incomplete struct for non-integral types
template <typename T_res,
          typename T_release_func,
          T_release_func func,
          typename Enable = void>
struct Res_data_N;

// actual implementation
template <typename T_res,
          typename T_release_func,
          T_release_func func>
struct Res_data_N <T_res,
                   T_release_func,
                   func,
                   typename std::enable_if<std::is_integral<T_res>::value>::type>
{
    Res_data_N(size_t count_ = 0,
               T_res *names_ = nullptr) : count{count_},
                                          names{names_}
    {
        assert(count > 0);
    }

    T_res operator[](size_t i)  const
    {
        // count should already be 0 for such cases, but just to be sure null check is made
        // such an access would anyways fail since a null pointer is dereferenced
        assert(names && (i < count));
        return names[i];
    }

    Res_data_N(Res_data_N &&that) noexcept : count{that.count},
                                             names{that.names}
    {
        that.count = 0;
        that.names = nullptr;
    }

    Res_data_N& operator=(Res_data_N &&that)
    {
        std::swap(count, that.count);
        std::swap(names, that.names);
        return *this;
    }

    void release()
    {
        *this = std::move(typename std::remove_reference<decltype(*this)>::type{});
    }

    ~Res_data_N()
    {
        if(count)
            (*func)(count, names);
    }

private:
    size_t count;
    T_res *names;
};


// although Res_data_1 was intended to be a partial specialization of Res_code for the special case of
// one resource, for the deleter taking an array type; this is to avoid the expense of one size_t when using
// Res_data_N; but since GL functions are accessed via GLEW the partial specilization lookup doesn't match
// this; hence making it a separate class

// incomplete struct for non-integral types
template <typename T_res,
          typename T_release_func,
          T_release_func func,
          typename Enable = void>
struct Res_data_1;

// actual implementation
template <typename T_res,
          typename T_release_func,
          T_release_func func>
struct Res_data_1 <T_res,
                   T_release_func,
                   func,
                   typename std::enable_if<std::is_integral<T_res>::value>::type>

{
    Res_data_1(T_res name_ = 0) : name{name_}
    {
    }

    operator T_res() const
    {
        assert(name);
        return name;
    }

    Res_data_1(Res_data_1 &&that) noexcept : name{that.name}
    {
        that.name = 0;
    }

    Res_data_1& operator=(Res_data_1 &&that)
    {
        std::swap(name, that.name);
        return *this;
    }

    void release()
    {
        *this = std::move(typename std::remove_reference<decltype(*this)>::type{});
    }

    ~Res_data_1()
    {
        if(name)
            (*func)(1, &name);
    }
private:
    T_res name;
};

using Program = Res_code  <GLuint, decltype(&glDeleteProgram), &glDeleteProgram>;
using Shader  = Res_code  <GLuint, decltype(&glDeleteShader), &glDeleteShader>;
using Buffer  = Res_data_1<GLuint, decltype(&glDeleteBuffers), &glDeleteBuffers>;
using Texture = Res_data_1<GLuint, decltype(&glDeleteTextures), &glDeleteTextures>;
using VAO     = Res_data_1<GLuint, decltype(&glDeleteVertexArrays), &glDeleteVertexArrays>;
}   // namespace GL

#endif // GL_RES_WRAP_HPP
