#ifndef PRIMITIVES_HPP
#define PRIMITIVES_HPP

namespace Primitives
{

struct Mesh // indexed triangles list
{
    glm::vec4 colour;
    // although short is used for indices, the type should be based
    // on vertices.size() since having a lesser sized datatype is better
    // Boost.Variant can help here
    std::vector<GLushort> indices;
    std::vector<glm::vec3> vertices;
    glm::mat4 model_xform;
};

struct Plane
{
    // the w coordinate is the NEGATIVE signed distance
    // from the origin along the normal
    glm::vec4 normal_signed_dist;
};

struct PlaneSegment
{
    glm::vec3 centre;
    glm::mat3 axes;
    glm::vec2 half_extent;

    Plane get_plane() const
    {
        // point on plane DOT normal
        const auto signed_distance = glm::dot(centre, axes[2]);
        return Plane{glm::vec4(axes[2], -signed_distance)};
    }
};

struct Box  // OBB
{
    glm::vec3 centre;
    glm::vec3 half_extent;
    glm::mat3 axes;
};

struct Sphere
{
    glm::vec3 centre;
    float radius;
};

struct Ray
{
    glm::vec3 origin;
    glm::vec3 direction;
};

bool ray_plane_intersect(const Ray &ray,
                         const Plane &plane,
                         float *t);
bool ray_plane_segment_intersect(const Ray &ray,
                                 const PlaneSegment &plane_segment,
                                 float *t);
bool ray_sphere_intersect(const Ray &ray,
                          const Sphere &sphere,
                          float *t);
bool ray_box_intersect(const Ray &ray,
                       const Box &box,
                       float *t);

void parse_SDL(const char *file_path,
               std::vector<Sphere> *spheres,
               std::vector<Box> *boxes,
               std::vector<PlaneSegment> *plane_segs,
               std::vector<Mesh> *meshes);
} // namespace Primitives

#endif // PRIMITIVES_HPP
