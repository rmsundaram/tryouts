#ifndef __PRECOMP_H__
#define __PRECOMP_H__

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/epsilon.hpp>
#include <glm/gtc/reciprocal.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include <memory>
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <utility>
#include <functional>
#include <cassert>
#include <iterator>
#include <numeric>

#endif  // __PRECOMP_H__
