#include "precomp.hpp"
#include "Util.hpp"
#include "Primitives.hpp"

void make_box_mesh(Primitives::Box &b,
                   const glm::vec4 &colour,
                   std::vector<Primitives::Mesh> *meshes)
{
    static constexpr GLubyte indices[] = {1, 0, 3, // floor
                                          3, 2, 1,
                                          5, 1, 2, // front
                                          6, 5, 2,
                                          0, 1, 5, // right
                                          0, 5, 4,
                                          2, 3, 6, // left
                                          7, 6, 3,
                                          0, 4, 3, // back
                                          3, 4, 7,
                                          4, 5, 7, // ceil
                                          7, 5, 6};

    const std::vector<glm::vec3> vertices{glm::vec3{ b.half_extent.x,
                                                    -b.half_extent.y,
                                                     b.half_extent.z},
                                          glm::vec3{ b.half_extent.x,
                                                    -b.half_extent.y,
                                                    -b.half_extent.z},
                                          glm::vec3{-b.half_extent.x,
                                                    -b.half_extent.y,
                                                    -b.half_extent.z},
                                          glm::vec3{-b.half_extent.x,
                                                    -b.half_extent.y,
                                                     b.half_extent.z},
                                          glm::vec3{ b.half_extent.x,
                                                     b.half_extent.y,
                                                     b.half_extent.z},
                                          glm::vec3{ b.half_extent.x,
                                                     b.half_extent.y,
                                                    -b.half_extent.z},
                                          glm::vec3{-b.half_extent.x,
                                                     b.half_extent.y,
                                                    -b.half_extent.z},
                                          glm::vec3{-b.half_extent.x,
                                                     b.half_extent.y,
                                                     b.half_extent.z}};
    // rotate and translate
    const glm::mat4 M{glm::vec4{b.axes[0], 0.0f},
                      glm::vec4{b.axes[1], 0.0f},
                      glm::vec4{b.axes[2], 0.0f},
                      glm::vec4{b.centre, 1.0f}};
    meshes->push_back({colour,
                       {indices, indices + std::extent<decltype(indices)>::value},
                       std::move(vertices),
                       M});
}

void parse_box(std::istringstream &istr,
               std::vector<Primitives::Box> *boxes,
               std::vector<Primitives::Mesh> *meshes)
{
    float x, y, z, w;
    istr >> x >> y >> z >> w;
    const glm::vec4 colour{x, y, z, w};

    istr >> x >> y >> z;
    const glm::vec3 centre{x, y, z};

    istr >> x >> y >> z;
    const glm::vec3 half_extents{x, y, z};   // in box's space

    x = 0.0f, y = 0.0f, z = 0.0f;
    istr >> x >> y >> z;
    // orient box's frame
    const auto axes = glm::mat3(glm::eulerAngleYXZ(y, x, z));

    boxes->push_back({centre, half_extents, axes});
    make_box_mesh(boxes->back(), colour, meshes);
}

void make_plane_mesh(Primitives::PlaneSegment &p,
                     const glm::vec4 &colour,
                     std::vector<Primitives::Mesh> *meshes)
{
    static constexpr GLubyte indices[] = {0, 1, 3,
                                          3, 1, 2};
    const glm::vec3 half_disp_X{p.half_extent.x, 0.0f, 0.0f};
    const glm::vec3 half_disp_Y{0.0f, p.half_extent.y, 0.0f};
    const std::vector<glm::vec3> vertices{half_disp_X - half_disp_Y,
                                          half_disp_X + half_disp_Y,
                                         -half_disp_X + half_disp_Y,
                                         -half_disp_X - half_disp_Y};
    // rotate and translate
    const glm::mat4 M{glm::vec4(p.axes[0], 0.0f),
                      glm::vec4(p.axes[1], 0.0f),
                      glm::vec4(p.axes[2], 0.0f),
                      glm::vec4(p.centre, 1.0f)};
    meshes->push_back({colour,
                       {indices, indices + std::extent<decltype(indices)>::value},
                       std::move(vertices),
                       M});
}

void parse_plane_segment(std::istringstream &istr,
                         std::vector<Primitives::PlaneSegment> *plane_segs,
                         std::vector<Primitives::Mesh> *meshes)
{
    float x, y, z, w;
    istr >> x >> y >> z >> w;
    const glm::vec4 colour{x, y, z, w};

    istr >> x >> y >> z;
    const glm::vec3 centre{x, y, z};

    istr >> x >> y >> z;
    const auto normal = glm::normalize(glm::vec3{x, y, z});
    istr >> x >> y >> z;
    const auto right = glm::normalize(glm::vec3{x, y, z});
    const auto up = glm::cross(normal, right);
    glm::mat3 axes{right, up, normal};
    // plane's coordinate system formed

    istr >> x >> y;
    const glm::vec2 half_extent{x, y};      // in plane's space

    x = 0.0f, y = 0.0f, z = 0.0f;
    istr >> x >> y >> z;
    const auto R = glm::mat3(glm::eulerAngleYXZ(y, x, z));
    axes *= R;          // plane oriented by rotation

    plane_segs->push_back({centre, axes, half_extent});
    make_plane_mesh(plane_segs->back(), colour, meshes);
}

void tessellate_normalize(std::vector<glm::vec3> *vertices,
                          std::vector<GLushort> *indices)
{
    assert(indices->size() % 3 == 0);
    std::map<glm::vec3, size_t, Vec_compare<glm::vec3>> new_verts;
    std::remove_reference<decltype(*indices)>::type new_indices;
    new_indices.reserve(4 * indices->size());
    for(auto i = 0u; i < indices->size(); i += 3)
    {
        constexpr auto split_vert_count = 6u;   // 3 vertices + 3 midpoint vertices
        std::array<GLushort, split_vert_count> split_indices;
        // get the new index of each vertex, including the midpoint vertices
        auto res = new_verts.emplace((*vertices)[(*indices)[i]], new_verts.size());
        // had this vertex been already present res.second would be false
        split_indices[0] = static_cast<GLushort>((res.first)->second);
        res = new_verts.emplace((*vertices)[(*indices)[i + 1]], new_verts.size());
        split_indices[1] = static_cast<GLushort>((res.first)->second);
        res = new_verts.emplace((*vertices)[(*indices)[i + 2]], new_verts.size());
        split_indices[2] = static_cast<GLushort>((res.first)->second);
        res = new_verts.emplace((*vertices)[(*indices)[i]] + (*vertices)[(*indices)[i + 1]], new_verts.size());
        split_indices[3] = static_cast<GLushort>((res.first)->second);
        res = new_verts.emplace((*vertices)[(*indices)[i + 1]] + (*vertices)[(*indices)[i + 2]], new_verts.size());
        split_indices[4] = static_cast<GLushort>((res.first)->second);
        res = new_verts.emplace((*vertices)[(*indices)[i + 2]] + (*vertices)[(*indices)[i]], new_verts.size());
        split_indices[5] = static_cast<GLushort>((res.first)->second);

        new_indices.insert(std::end(new_indices), {    // four faces formed by splitting one face
                                                       split_indices[0], split_indices[3], split_indices[5],
                                                       split_indices[3], split_indices[1], split_indices[4],
                                                       split_indices[4], split_indices[2], split_indices[5],
                                                       split_indices[3], split_indices[4], split_indices[5],
                                                  });
    }
    // instead of map a boost::bimap would be more performant since it allows reverse lookups;
    // this vector resize could be avoided
    vertices->resize(new_verts.size());
    for(const auto &v : new_verts)
    {
        (*vertices)[v.second] = glm::normalize(v.first);
    }
    *indices = std::move(new_indices);
}

void make_sphere_mesh(const glm::vec3 &centre,
                      float radius,
                      size_t tessellation_count,
                      const glm::vec4 &colour,
                      std::vector<Primitives::Mesh> *meshes)
{
    /*
     * Icosphere (as Blender calls it) is a sphere construction out of an icosahedron; the same
     * can be done from any closed convex solid; where tessellation is done to achieve the required
     * smoothness and all vertices are normalized from the centre. References:
     *
     * http://stackoverflow.com/questions/7687148/drawing-sphere-in-opengl-without-using-glusphere
     * http://blog.andreaskahler.com/2009/06/creating-icosphere-mesh-in-code.html
     * http://sol.gfxile.net/sphere
     * OpenGL Red Book (v1.1, Chapter 2 ~ http://www.glprogramming.com/red/chapter02.html)
     * Introduction to 3D Game Programming with DirectX 11
     * https://medium.com/game-dev-daily/four-ways-to-create-a-mesh-for-a-sphere-d7956b825db4
     */

    // define an icosahedron
    const auto g = glm::golden_ratio<float>();
    std::vector<glm::vec3> vertices{
                                       glm::vec3{-1,  g,  0},
                                       glm::vec3{ 1,  g,  0},
                                       glm::vec3{-1, -g,  0},
                                       glm::vec3{ 1, -g,  0},
                                       glm::vec3{ 0, -1,  g},
                                       glm::vec3{ 0,  1,  g},
                                       glm::vec3{ 0, -1, -g},
                                       glm::vec3{ 0,  1, -g},
                                       glm::vec3{ g,  0, -1},
                                       glm::vec3{ g,  0,  1},
                                       glm::vec3{-g,  0, -1},
                                       glm::vec3{-g,  0,  1}
                                   };
    std::vector<GLushort> indices{
                                      0, 11,  5,    0,  5,  1,    0, 1,  7,
                                      0,  7, 10,    0, 10, 11,    1, 5,  9,
                                      5, 11,  4,   11, 10,  2,   10, 7,  6,
                                      7,  1,  8,    3,  9,  4,    3, 4,  2,
                                      3,  2,  6,    3,  6,  8,    3, 8,  9,
                                      4,  9,  5,    2,  4, 11,    6, 2, 10,
                                      8,  6,  7,    9,  8,  1
                                 };

    //tessellate it according to the tessellation_count
    for(auto i = 0u; i < tessellation_count; ++i)
    {
        tessellate_normalize(&vertices, &indices);
    }

    // scale and translate
    auto M = glm::scale(glm::vec3{radius, radius, radius});
    M[3] = glm::vec4(centre, 1.0f);
    meshes->push_back({colour,
                       std::move(indices),
                       std::move(vertices),
                       M});
}

void parse_sphere(std::istringstream &istr,
                  std::vector<Primitives::Sphere> *spheres,
                  std::vector<Primitives::Mesh> *meshes)
{
    float x, y, z, w;
    istr >> x >> y >> z >> w;
    const glm::vec4 colour{x, y, z, w};

    istr >> x >> y >> z;
    const glm::vec3 centre{x, y, z};

    float radius;
    istr >> radius;
    spheres->push_back({centre, radius});

    // an optimal tessellation count should be chosen for the sphere to not have rough edges;
    // this would vary based on the viewing scale factor and the radius of the sphere; for a smaller
    //factor, a lower count would suffice, while for a larger one, a higher value may be required
    constexpr auto tessellation_count = 2u;
    make_sphere_mesh(centre, radius, tessellation_count, colour, meshes);
}

void Primitives::parse_SDL(const char *file_path,
                           std::vector<Primitives::Sphere> *spheres,
                           std::vector<Primitives::Box> *boxes,
                           std::vector<Primitives::PlaneSegment> *plane_segs,
                           std::vector<Primitives::Mesh> *meshes)
{
    assert(spheres && boxes && plane_segs && meshes);

    std::vector<Primitives::Mesh> box_meshes, plane_meshes;
    std::ifstream sdl_file{file_path};
    for(std::string line; std::getline(sdl_file, line); )
    {
        std::istringstream istr{line};
        char code;
        if (istr >> code)
        {
            switch(code)
            {
                case 'b':
                    parse_box(istr, boxes, &box_meshes);
                    break;
                case 'p':
                    parse_plane_segment(istr, plane_segs, &plane_meshes);
                    break;
                case 's':
                    parse_sphere(istr, spheres, meshes);
            }
        }
    }
    meshes->insert(meshes->end(),
                   std::make_move_iterator(box_meshes.begin()),
                   std::make_move_iterator(box_meshes.end()));
    meshes->insert(meshes->end(),
                   std::make_move_iterator(plane_meshes.begin()),
                   std::make_move_iterator(plane_meshes.end()));
}

// 5.2.1 Intersection of a line and a plane - from Lengyel's skeleton book
bool Primitives::ray_plane_intersect(const Ray &ray,
                                     const Plane &plane,
                                     float *t)
{
    const auto denom = glm::dot(glm::vec4(ray.direction, 0.0f),
                                plane.normal_signed_dist);
    // ray is parallel to plane
    if(glm::epsilonEqual(denom,
                         0.0f,
                         glm::epsilon<float>()))
        return false;
    const auto num = glm::dot(glm::vec4(ray.origin, 1.0f),
                              plane.normal_signed_dist);
    const float dist = -num/denom;
    if(dist < 0.0f)    // plane behind ray case
        return false;
    *t = dist;
    return true;
}

bool Primitives::ray_plane_segment_intersect(const Primitives::Ray &ray,
                                             const Primitives::PlaneSegment &plane_segment,
                                             float *t)
{
    // first check if the infinite plane, of which this is a segment, is intersected by the ray
    const auto plane = plane_segment.get_plane();
    float dist;
    if(!ray_plane_intersect(ray, plane, &dist))
        return false;

    // point of intersection on the infinite plane
    const glm::vec3 point = ray.origin + (dist * ray.direction);

    // 2D solution: check if the point is within the plane segment;
    // calculate the X, Y coordinates of the point w.r.t. the plane segment's frame
    // should their values exceed the half extent on a dimension then the point is OUT
    const auto position = point - plane_segment.centre;
    const auto X = glm::dot(position, plane_segment.axes[0]);
    if(glm::abs(X) > plane_segment.half_extent[0])
        return false;
    const auto Y = glm::dot(position, plane_segment.axes[1]);
    if(glm::abs(Y) > plane_segment.half_extent[1])
        return false;
    *t = dist;
    return true;
}

// Real-Time Rendering, 3rd edition; 16.6.2 Optimized (Ray/Sphere) Solution
bool Primitives::ray_sphere_intersect(const Primitives::Ray &ray,
                                      const Primitives::Sphere &sphere,
                                      float *t)
{
    const auto l = sphere.centre - ray.origin;
    const auto l_sqr = glm::dot(l, l);
    const auto r_sqr = sphere.radius * sphere.radius;
    const auto s = glm::dot(ray.direction, l);
    // ray starts outside and the sphere is behind
    if((l_sqr > r_sqr) && (s < 0.0f))
        return false;
    const auto s_sqr = s * s;
    const auto m_sqr = l_sqr - s_sqr;
    // distance from centre to perpendicular projection
    // of ray direction on origin to centre is greater than radius
    // i.e. no hit
    if(m_sqr > r_sqr)
        return false;
    const auto q = glm::sqrt(r_sqr - m_sqr);
    // subtract if ray origin is outside otherwise add
    *t = (l_sqr > r_sqr) ? (s - q) : (s + q);
    return true;
}

// Real-Time Rendering, 3rd Edition ~ Kay-Kajiya's slabs method
bool Primitives::ray_box_intersect(const Primitives::Ray &ray,
                                   const Primitives::Box &box,
                                   float *t)
{
    constexpr float epsilon = 10.0E-20f;
    constexpr auto n_dimensions = 3u;

    float farthest_entry = -std::numeric_limits<float>::max();
    float nearest_exit   =  std::numeric_limits<float>::max();
    const auto p = box.centre - ray.origin;
    for (auto i = 0u; i < n_dimensions; ++i)
    {
        const auto e = glm::dot(box.axes[i], p);
        const auto f = glm::dot(box.axes[i], ray.direction);

        // ray not parallel to slab planes
        if (std::abs(f) > epsilon)
        {
            const auto reciprocal_f = 1.0f / f;
            // calculate entry and exit
            const auto entry = (e + box.half_extent[i]) * reciprocal_f;
            const auto exit = (e - box.half_extent[i]) * reciprocal_f;
            if (exit > entry)
            {
                if (exit < nearest_exit) nearest_exit = exit;
                if (entry > farthest_entry) farthest_entry = entry;
            }
            else
            {
                if (entry < nearest_exit) nearest_exit = entry;
                if (exit > farthest_entry) farthest_entry = exit;
            }
            if ((farthest_entry > nearest_exit) || (nearest_exit < 0.0f))
                return false;
        }
        else if (((-box.half_extent[i] - e) > 0.0f) || ((box.half_extent[i] - e) < 0.0f))
            return false;
    }
    *t = (farthest_entry > 0.0f) ? farthest_entry : nearest_exit;
    return true;
}
