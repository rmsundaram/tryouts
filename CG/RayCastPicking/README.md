Pre-built Win32 binary available in the [Downloads section](https://bitbucket.org/rmsundaram/tryouts/downloads).

SCREENSHOT
==========
![RayCastPick.png](https://bitbucket.org/repo/no48Gg/images/3498768015-RayCastPick.png "Primitive picker using ray casting")

CONTROLS
========

Action                    | Key
--------------------------|------------
Pick primitive            | Mouse click
Slide camera ±X           | `D`/`A`
Slide camera ±Y           | `W`/`X`
Slide camera ±Z           | `S`/`Z`
Rotate camera X           | `↑`/`↓`
Rotate camera Y           | `←`/`→`
Rotate camera Z           | `,`/`.`
Toggle wireframe render   | `0`
Reset camera              | `R`
Decrease FoV (Telephoto)  | `Numpad −`
Increse FoV (Wideangle)   | `Numpad +`
Exit                      | `Esc`

SCENE
=====
The scene rendered is described as a simple SDL (scene description language) file at `./data/map.sdl`. This can be edited to alter the scene without recompiling the project.

LEARNINGS
=========
 1. Constructing an icosphere mesh
 2. Strict-weak ordering of vectors lexicographically
 3. Ray/AABB intersection testing (Kay-Kajiya's slabs method)
 4. Ray/Sphere intersection testing
 5. Plane and plane segment/Ray intersection testing
 6. Using ms/f instead of fps; the latter is non-linear and not intuitive
 7. Picking by Ray Casting (Essential Math, §6.6)
 8. Wrapped buffers, VAOs, etc. so that glDelete* needn't be called
 9. Using VAOs for each model which has the VBO and IBO data coupled
10. Representing primitives and their meshes independently
11. Custom SDL format instead of hard coding actors of the scene
12. Common mesh structure for rendering objects of different types
