#version 330

uniform vec4 solid_col;

out vec4 colour;

void main()
{
    colour = solid_col;
}
