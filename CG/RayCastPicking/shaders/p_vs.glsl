#version 330

uniform mat4 PVM;

layout (location = 0) in vec3 position;

void main()
{
    gl_Position = PVM * vec4(position, 1.0f);
}
