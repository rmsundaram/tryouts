#include <random>
#include <chrono>
#include <memory>
#include <algorithm>
#include <fstream>
#include <iostream>

void visit(int x, int y,
           float hit_x, float hit_y,
           float t,
           int* &res_n,
           float* &res_f)
{
    *res_n++ = x;
    *res_n++ = y;
    *res_f++ = hit_x;
    *res_f++ = hit_y;
    *res_f++ = t;
}

void trace_cells_1(const int x0, const int y0,
                         int x1,       int y1,
                    int* &res_n, float* &res_f)
{
    const auto dx = std::abs(x1 - x0);
    const auto dy = std::abs(y1 - y0);

    const auto step_x = x1 > x0 ? 1 : -1;
    const auto step_y = y1 > y0 ? 1 : -1;

    // error is a fraction m = dy/dx, which is scaled by dx to avoid floating-point
    // calculations; when dy > dx, err = 1/m = dx/dy scaled by dy
    const bool horizontal = dx >= dy;
    int err = 0;

    auto x = x0, y = y0;
    // compute the ray's direction, (x0, y0) is the origin
    const float vx = x1 - x0, vy = y1 - y0;
    while ((x != x1) || (y != y1))
    {
        const auto cell_x = x, cell_y = y;
        float hit_x, hit_y, t;
        // each cell is denoted by its lower left corner point on the grid from the
        // ray's viewpoint (this matters when x1 < x0 or y1 < y0)
        if (horizontal)
        {
            // the actual y value in real space equals y0 (integer) + err (fraction)
            err += dy;
            if (err > dx)
            {
                y += step_y;
                /*
                 * once enough error has gotten accumulated (err > 1) remove one from
                 * it and continue to keep it as a fraction e.g. for dy/dx = 3/5 for 
                 * iteration 2 we've err = 2 * 3/5 = 6/5, we know we've to remove 5/5
                 * from it to make it 1/5, but 1/5 is the error in the next stepping
                 * of x0 and not this one, hence also subtract dy/dx from it apart from
                 * dx/dx 1/5 - 3/5 = -2/5 which is the right error for this x value as
                 * the real y should be lesser than (below) y0; also with the next err
                 * increment of 3/5, the error would rightly become 1/5 for that x0
                 */
                err -= (dx + dy);

                // p = o + tv; finding t with hit_x and finding hit_x thereby
                hit_y = y;
                t = (hit_y - y0) / vy;
                hit_x = x0 + t * vx;
            }
            else if (err < dx)
            {
                x += step_x;

                hit_x = x;
                t = (hit_x - x0) / vx;
                hit_y = y0 + t * vy;
            }
            else
            {
                x += step_x;
                y += step_y;
                err = 0u;

                hit_x = x;
                hit_y = y;
                t = (hit_x - x0) / vx;
            }
        }
        else
        {
            err += dx;
            if (err > dy)
            {
                x += step_x;
                err -= (dy + dx);

                hit_x = x;
                t = (hit_x - x0) / vx;
                hit_y = y0 + t * vy;
            }
            else if (err < dy)
            {
                y += step_y;

                hit_y = y;
                t = (hit_y - y0) / vy;
                hit_x = x0 + t * vx;
            }
            else
            {
                y += step_y;
                x += step_x;
                err = 0u;

                hit_y = y;
                hit_x = x;
                t = (hit_y - y0) / vy;
            }
        }
        visit(cell_x, cell_y, hit_x, hit_y, t, res_n, res_f);
    }
}

// void trace_cells_2(const int x0, const int y0,    // implementation const to preserve origin
                         // int x1,       int y1,
                     // int *res_n, float *res_f)
// {
    // const auto vx = x1 - x0, vy = y1 - y0;
    // const auto dx = std::abs(vx), dy = std::abs(vy);

    // const auto step_x = (x1 > x0) ? 1 : -1;
    // const auto step_y = (y1 > y0) ? 1 : -1;

    // // error is a fraction m = dy/dx, which is scaled by dx to avoid floating-point
    // // calculations; when dy > dx, err = 1/m = dx/dy scaled by dy
    // const bool horizontal = dx >= dy;
    // int err = 0;
    // auto x = x0, y = y0;
    // while ((x != x1) || (y != y1))
    // {
        // // each cell is denoted by its lower left corner point on the grid from the
        // // point of view of the ray (this matters when x1 < x0 or y1 < y0)
        // const auto cell_x = x, cell_y = y;
        // if (horizontal)
        // {
            // err += dy;
            // if (err <= dx) x += step_x;
            // if (err >= dx) y += step_y;
            // const auto t = std::div(x - x0, vx);
            // // err is nothing but y's fraction part as an integer; based on p = o + tv
            // //err = (vx * y0) + (vy * (t.quot * vx + t.rem)) - (vx * y); below is the simplified form
            // err = vx * (y0 + (vy * t.quot) - y) + (t.rem * vy);
        // }
        // else
        // {
            // err += dx;
            // if (err <= dy) y += step_y;
            // if (err >= dy) x += step_x;
            // const auto t = std::div(y - y0, vy);
            // err = vy * (x0 + (vx * t.quot) - x) + (t.rem * vx);
        // }
        // float t_flt;
        // if (horizontal)
            // t_flt = (err < 0) ? (y - y0) / static_cast<float>(vy) : (x - x0) / static_cast<float>(vx);
        // else
            // t_flt = (err < 0) ? (x - x0) / static_cast<float>(vx) : (y - y0) / static_cast<float>(vy);
        // const auto hit_x = x0 + t_flt * vx, hit_y = y0 + t_flt * vy;
        // visit(cell_x, cell_y, hit_x, hit_y, t_flt, res_n, res_f);
    // }
// }

void trace_cells_3(const int x0, const int y0,
                         int x1,       int y1,
                   int* &res_n, float* &res_f)
{
    // this shouldn't be unsigned due to its usage in std::max(dx - 1, 0) below
    const auto dx = std::abs(x1 - x0);
    const auto dy = std::abs(y1 - y0);

    const auto step_x = x1 > x0 ? 1 : -1;
    const auto step_y = y1 > y0 ? 1 : -1;

    // compute the ray's direction, (x0, y0) is the origin
    const float vx = x1 - x0, vy = y1 - y0;
    // when error is positive, then ray is intersecting a line x = k, so increment x and y is rational
    // when it's positive, the ray is intersecting a line y = k, so increment y and x is in rational
    int err = dx - dy;
    auto x = x0, y = y0;
    while ((x != x1) || (y != y1))
    {
        const auto cell_x = x, cell_y = y;
        float hit_x, hit_y, t;
        if (err > 0)
        {
            x += step_x;
            err -= dy;

            // p = o + tv; finding t with hit_x and finding hit_y thereby
            hit_x = x;
            t = (hit_x - x0) / vx;
            hit_y = y0 + t * vy;
        }
        else if (err < 0)
        {
            y += step_y;
            err += dx;

            hit_y = y;
            t = (hit_y - y0) / vy;
            hit_x = x0 + t * vx;
        }
        else
        {
            x += step_x;
            y += step_y;
            err += dx - dy;

            hit_x = x;
            hit_y = y;
            t = (hit_y - y0) / vy;
        }
        visit(cell_x, cell_y, hit_x, hit_y, t, res_n, res_f);
    }
}

std::mt19937 eng;
std::uniform_int_distribution<int> size(-100, 100);

struct vec4
{
    int x1, y1;
    int x2, y2;
};

// Since HRC is almost always aliased to {steady,system}_clock it’s better to
// use the former directly; avoid latter as it isn’t monotonic i.e. it can go
// back and jump around too!
// https://en.cppreference.com/w/cpp/chrono/high_resolution_clock
// https://stackoverflow.com/q/1487695/183120
// https://stackoverflow.com/q/38252022/183120
// https://gamedev.stackexchange.com/a/131094/14025
typedef std::conditional<
  std::chrono::high_resolution_clock::is_steady,
  std::chrono::high_resolution_clock,
  std::chrono::steady_clock>::type Clock;
static_assert(Clock::is_steady,
               "Clock is not monotonically-increasing (steady).");
typedef std::chrono::duration<double, std::nano> nano;

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        std::cout << "No seed value passed\n";
        return 0;
    }
    const auto s = std::atoi(argv[1]);
    eng.seed(s);
    constexpr int N = 20000;
    std::vector<vec4> v;
    v.reserve(N);
    for (int i = 0; i < N; ++i)
    {
        v.push_back({size(eng), size(eng), size(eng), size(eng)});
    }
    std::cout << "Test data generated.\n";

    // result storage
    constexpr auto n_vert_res = 2000u, n_t_res = 3000u;
    std::unique_ptr<int[]> spVertResult1{new int[N * n_vert_res]};
    std::unique_ptr<float[]> spTResult1{new float[N * n_t_res]};
    // std::unique_ptr<int[]> spVertResult2{new int[N * n_vert_res]};
    // std::unique_ptr<float[]> spTResult2{new float[N * n_t_res]};
    std::unique_ptr<int[]> spVertResult3{new int[N * n_vert_res]};
    std::unique_ptr<float[]> spTResult3{new float[N * n_t_res]};

    int *ptr_n = spVertResult1.get();
    float *ptr_f = spTResult1.get();
    const auto t1 = Clock::now();
    for(const auto &i : v)
    {
        trace_cells_1(i.x1, i.y1, i.x2, i.y2, ptr_n, ptr_f);
    }
    const nano duration1 = Clock::now() - t1;
    std::cout << "Duration of trace_cells_1: " << duration1.count() << '\n';
    const auto n_items1 = ptr_n - spVertResult1.get();
    const auto f_items1 = ptr_f - spTResult1.get();

    // ptr_n = spVertResult2.get();
    // ptr_f = spTResult2.get();
    // const auto t2 = Clock::now();
    // for(const auto &i : v)
    // {
        // trace_cells_2(i.x1, i.y1, i.x2, i.y2, ptr_n, ptr_f);
    // }
    // const nano duration2 = Clock::now() - t2;
    // std::cout << "Duration of trace_cells_2: " << duration2.count() << '\n';

    ptr_n = spVertResult3.get();
    ptr_f = spTResult3.get();
    const auto t3 = Clock::now();
    for(const auto &i : v)
    {
        trace_cells_3(i.x1, i.y1, i.x2, i.y2, ptr_n, ptr_f);
    }
    const nano duration3 = Clock::now() - t3;
    std::cout << "Duration of trace_cells_3: " << duration3.count() << '\n';
    const auto n_items3 = ptr_n - spVertResult3.get();
    const auto f_items3 = ptr_f - spTResult3.get();

    if (n_items1 == n_items3) std::cout << "Matches!\n";
    else std::cout << n_items1 << '\t' << n_items3 << '\n';

    // std::ofstream output("res.log");
    // for (const auto &i : v)
    // {
        // output << i.x1 << ", " << i.y1 << ", " << i.x2 << ", " << i.y2 << '\n';
    // }
    // output << "\n\n";
    // for(auto i = 0; i < n_items1; ++i)
    // {
        // output << spVertResult1[i] << '\t' << spVertResult3[i] << '\n';
    // }
}
