// based on http://playtechs.blogspot.in/2007/03/raytracing-on-grid.html
// Graphics Gems 4: Voxel Traversal along a 3D Line is the site author's reference
// another similar paper is voxel ray traversal in 3D which does it with floats
// A Fast Voxel Traversal Algorithm for Ray Tracing by John Amanatides and Andrew Woo

#include <cstdlib>
#include <cassert>
#include <iostream>

void visit(int x, int y,
           float vx, float vy,
           float hit_x, float hit_y,
           float t)
{
    const auto next_x = x + ((vx < 0) ? -1 : 1);
    const auto next_y = y + ((vy < 0) ? -1 : 1);
    std::cout << "Vertices: (" << x << ", " << y << "), ("
              << next_x << ", " << y << "), ("
              << x << ", " << next_y << "), ("
              << next_x << ", " << next_y << ")"
              << " Exit: (" << hit_x << ", " << hit_y << ")\tt: " << t << "\n";
}

// this isn't DDA since DDA plots exactly max(dx, dy) dots while this does more
void trace_cells(const int x0, const int y0,    // implementation const to preserve origin
                       int x1,       int y1)
{
    const auto vx = x1 - x0, vy = y1 - y0;
    const auto dx = std::abs(vx), dy = std::abs(vy);
    assert(!(dx == 0 && dy == 0));

    const auto step_x = (x1 > x0) ? 1 : -1;
    const auto step_y = (y1 > y0) ? 1 : -1;

    // error is a fraction m = dy/dx, which is scaled by dx to avoid floating-point
    // calculations; when dy > dx, err = 1/m = dx/dy scaled by dy
    const bool horizontal = dx >= dy;
    int err = 0;
    auto x = x0, y = y0;
    while ((x != x1) || (y != y1))
    {
        // each cell is denoted by its lower left corner point on the grid from the
        // point of view of the ray (this matters when x1 < x0 or y1 < y0)
        const auto cell_x = x, cell_y = y;
        if (horizontal)
        {
            err += dy;
            if (err <= dx) x += step_x;
            if (err >= dx) y += step_y;
            const auto t = std::div(x - x0, vx);
            // err is nothing but y's fraction part as an integer; based on p = o + tv
            //err = (vx * y0) + (vy * (t.quot * vx + t.rem)) - (vx * y); below is the simplified form
            err = vx * (y0 + (vy * t.quot) - y) + (t.rem * vy);
        }
        else
        {
            err += dx;
            if (err <= dy) y += step_y;
            if (err >= dy) x += step_x;
            const auto t = std::div(y - y0, vy);
            err = vy * (x0 + (vx * t.quot) - x) + (t.rem * vx);
        }
        float t_flt = (cell_x != x) ? ((x - x0) / static_cast<float>(vx)) : ((y - y0) / static_cast<float>(vy));
        const auto px = x0 + t_flt * vx, py = y0 + t_flt * vy;
        visit(cell_x, cell_y, vx, vy, px, py, t_flt);
    }
}

int main()
{
    // trace_cells(11, 22, 16, 25);    // horizontal left to right, bottom to top
    // std::cout << '\n';
    // trace_cells(16, 24, 11, 22);    // horizontal right to left, top to bottom
    // std::cout << '\n';
    // trace_cells(11, 22, 15, 27);    // vertical bottom to top, left to right
    // std::cout << '\n';
    // trace_cells(14, 27, 11, 22);    // vertical top to bottom, right to left
    // std::cout << '\n';
    // trace_cells(14, 27, 13, 22);    // (almost) pure vertical top to bottom, right to left
    // std::cout << '\n';
    // trace_cells(12, 24, 15, 24);    // pure horizontal left to right, equal
    // std::cout << '\n';
    // trace_cells(16, 22, 11, 27);    // diagonal line
    // std::cout << '\n';
    // trace_cells(-78, 5, -82, -15);  // case where the line passes through a vertex but not a diagonal line
    trace_cells(-58, 17, 77, -77);
}
