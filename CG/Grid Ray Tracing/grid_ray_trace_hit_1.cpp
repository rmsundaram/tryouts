// based on http://playtechs.blogspot.in/2007/03/raytracing-on-grid.html
// Graphics Gems 4: Voxel Traversal along a 3D Line is the site author's reference
// another similar paper is voxel ray traversal in 3D which does it with floats
// A Fast Voxel Traversal Algorithm for Ray Tracing by John Amanatides and Andrew Woo

#include <cstdlib>
#include <cassert>
#include <cmath>
#include <iostream>

void visit(int x, int y,
           float vx, float vy,
           float hit_x, float hit_y,
           float t)
{
    const auto next_x = x + ((vx < 0) ? -1 : 1);
    const auto next_y = y + ((vy < 0) ? -1 : 1);
    std::cout << "Vertices: (" << x << ", " << y << "), ("
              << next_x << ", " << y << "), ("
              << x << ", " << next_y << "), ("
              << next_x << ", " << next_y << ")"
              << " Exit: (" << hit_x << ", " << hit_y << ")\tt: " << t << "\n";
}

/*
 * another way to do this would be to start from the ray origin, find tx for x + step_x
 * and ty for y + step_y and see the lesser of the two to find the exit point and use
 * that for the next iteration as the starting point; however that implementation would
 * have had more float to int conversions than this and we need ints for array indices
 */
// this isn't DDA since DDA plots exactly max(dx, dy) dots while this does more
void trace_cells(const int x0, const int y0,    // implementation const to preserve origin
                       int x1,       int y1)
{
    const auto dx = std::abs(x1 - x0);
    const auto dy = std::abs(y1 - y0);
    assert(!(dx == 0 && dy == 0));

    const auto step_x = x1 > x0 ? 1 : -1;
    const auto step_y = y1 > y0 ? 1 : -1;

    // error is a fraction m = dy/dx, which is scaled by dx to avoid floating-point
    // calculations; when dy > dx, err = 1/m = dx/dy scaled by dy
    const bool horizontal = dx >= dy;
    int err = 0;

    auto x = x0, y = y0;
    // compute the ray's direction, (x0, y0) is the origin
    const float vx = x1 - x0, vy = y1 - y0;
    while ((x != x1) || (y != y1))
    {
        const auto cell_x = x, cell_y = y;
        float hit_x, hit_y, t;
        // each cell is denoted by its lower left corner point on the grid from the
        // ray's viewpoint (this matters when x1 < x0 or y1 < y0)
        if (horizontal)
        {
            // the actual y value in real space equals y0 (integer) + err (fraction)
            err += dy;
            if (err > dx)
            {
                y += step_y;
                /*
                 * once enough error has gotten accumulated (err > 1) remove one from
                 * it and continue to keep it as a fraction e.g. for dy/dx = 3/5 for 
                 * iteration 2 we've err = 2 * 3/5 = 6/5, we know we've to remove 5/5
                 * from it to make it 1/5, but 1/5 is the error in the next stepping
                 * of x0 and not this one, hence also subtract dy/dx from it apart from
                 * dx/dx 1/5 - 3/5 = -2/5 which is the right error for this x value as
                 * the real y should be lesser than (below) y0; also with the next err
                 * increment of 3/5, the error would rightly become 1/5 for that x0
                 */
                err -= (dx + dy);

                // p = o + tv; finding t with hit_x and finding hit_x thereby
                hit_y = y;
                t = (hit_y - y0) / vy;
                hit_x = x0 + t * vx;
            }
            else if (err < dx)
            {
                x += step_x;

                hit_x = x;
                t = (hit_x - x0) / vx;
                hit_y = y0 + t * vy;
            }
            else
            {
                x += step_x;
                y += step_y;
                err = 0u;

                hit_x = x;
                hit_y = y;
                t = (hit_x - x0) / vx;
            }
        }
        else
        {
            err += dx;
            if (err > dy)
            {
                x += step_x;
                err -= (dy + dx);

                hit_x = x;
                t = (hit_x - x0) / vx;
                hit_y = y0 + t * vy;
            }
            else if (err < dy)
            {
                y += step_y;

                hit_y = y;
                t = (hit_y - y0) / vy;
                hit_x = x0 + t * vx;
            }
            else
            {
                y += step_y;
                x += step_x;
                err = 0u;

                hit_y = y;
                hit_x = x;
                t = (hit_y - y0) / vy;
            }
        }
        visit(cell_x, cell_y, vx, vy, hit_x, hit_y, t);
    }
}

int main()
{
    trace_cells(11, 22, 16, 25);    // horizontal left to right, bottom to top
    std::cout << '\n';
    trace_cells(16, 24, 11, 22);    // horizontal right to left, top to bottom
    std::cout << '\n';
    trace_cells(11, 22, 15, 27);    // vertical bottom to top, left to right
    std::cout << '\n';
    trace_cells(14, 27, 11, 22);    // vertical top to bottom, right to left
    std::cout << '\n';
    trace_cells(14, 27, 13, 22);    // (almost) pure vertical top to bottom, right to left
    std::cout << '\n';
    trace_cells(12, 24, 15, 24);    // pure horizontal left to right, equal
    std::cout << '\n';
    trace_cells(16, 22, 11, 27);    // diagonal line
    std::cout << '\n';
    trace_cells(-78, 5, -82, -15);  // case where the line passes through a vertex but not a diagonal line
}
