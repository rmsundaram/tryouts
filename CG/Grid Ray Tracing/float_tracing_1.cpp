#include <cmath>
#include <cassert>
#include <limits>
#include <iostream>

struct Ray
{
    float ox, oy;
    float dx, dy;
};

float length(const Ray &r)
{
    return std::sqrt(r.dx * r.dx + r.dy * r.dy);
}

constexpr auto epsilon = 0.000001f;

bool equal(float lhs, float rhs)
{
    return std::abs(lhs - rhs) < epsilon;
}

void visit(float x, float y,
           float out_x, float out_y)
{
    std::cout << '(' << x << ", " << y << ") to (" << out_x << ", " << out_y << ")\n";
}

float distance(float x1, float x2)
{
    return std::abs(x2 - x1);
}

float closer(float x, float x1, float x2)
{
    // finding the lesser of the two after taking abs on both was considered but it
    // will not work when one is positive and the other is not
    return (distance(x, x1) <= distance(x, x2)) ? x1 : x2;
}

float next_int(float x, float dx)
{
    return ((dx >= 0.0f) ? std::floor(x) : std::ceil(x)) + dx;
}

void cell_traverse(const Ray &r)
{
    assert((r.dx != 0) || (r.dy != 0));

    const auto l = length(r);
    const auto dx = r.dx / l;
    const auto dy = r.dy / l;
    const auto dest_x = r.ox + r.dx;
    const auto dest_y = r.oy + r.dy;
    const auto step_x = (dx != 0.0f) ? (dx > 0.0f ? 1.0f : -1.0f) : 0.0f;
    const auto step_y = (dy != 0.0f) ? (dy > 0.0f ? 1.0f : -1.0f) : 0.0f;
    auto x = r.ox, y = r.oy;
    while (!equal(x, dest_x) || !equal(y, dest_y))
    {
        const auto cur_x = x, cur_y = y;
        // next value would be a line x = k or the destination x
        const auto next_x = closer(x, next_int(x, step_x), dest_x);
        const auto next_y = closer(y, next_int(y, step_y), dest_y);
        const auto tx = dx ? ((next_x - x) / dx) : 0.0f;
        const auto ty = dy ? ((next_y - y) / dy) : 0.0f;
        if ((ty == 0.0f) || ((tx != 0 ) && (tx <= ty)))
        {
            y += tx * dy;
            x = next_x;
        }
        else
        {
            x += ty * dx;
            y = next_y;
        }
        visit(cur_x, cur_y, x, y);
    }
    std::cout << std::endl;
}

/*
++++

+---
-+--
--+-
---+

++--
-++-
--++

+--+
+-+-
-+-+

+++-
-+++
+-++
++-+

----
*/

int main()
{
    // std::cout << std::floor(-3.1) << '\t' << std::floor(-3.8) << '\t' << std::floor(-3.5) << '\n';
    // std::cout << std::floor(3.1) << '\t' << std::floor(3.8) << '\t' << std::floor(3.5) << '\n';
    // std::cout << std::ceil(-3.1) << '\t' << std::ceil(-3.8) << '\t' << std::ceil(-3.5) << '\n';
    // std::cout << std::ceil(3.1) << '\t' << std::ceil(3.8) << '\t' << std::ceil(3.5) << '\n';

    cell_traverse({-8.8f, 9.7f, -4.0f, 0.0f});
    cell_traverse({-8.8f, 9.7f, 0.0f, 3.0f});

    cell_traverse({7.2f, 9.7f, 4.0f, 3.0f});

    cell_traverse({7.2f, -9.7f, -4.0f, -3.0f});
    cell_traverse({-7.2f, 9.7f, -4.0f, -3.0f});
    cell_traverse({-7.2f, -9.7f, 4.0f, -3.0f});
    cell_traverse({-7.2f, -9.7f, -4.0f, 3.0f});

    cell_traverse({7.2f, 9.7f, -4.0f, -3.0f});
    cell_traverse({-7.2f, 9.7f, 4.0f, -3.0f});
    cell_traverse({-7.2f, -9.7f, 4.0f, 3.0f});

    cell_traverse({7.2f, -9.7f, -4.0f, 3.0f});
    cell_traverse({7.2f, -9.7f, 4.0f, -3.0f});
    cell_traverse({-7.2f, 9.7f, -4.0f, 3.0f});

    cell_traverse({7.2f, 9.7f, 4.0f, -3.0f});
    cell_traverse({-7.2f, 9.7f, 4.0f, 3.0f});
    cell_traverse({7.2f, -9.7f, 4.0f, 3.0f});
    cell_traverse({7.2f, 9.7f, -4.0f, 3.0f});

    cell_traverse({-7.2f, -9.7f, -4.0f, -3.0f});
}
