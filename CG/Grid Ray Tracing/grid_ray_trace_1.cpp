// based on http://playtechs.blogspot.in/2007/03/raytracing-on-grid.html
// Graphics Gems 4: Voxel Traversal along a 3D Line is the site author's reference
// another similar paper is voxel ray traversal in 3D which does it with floats
// A Fast Voxel Traversal Algorithm for Ray Tracing by John Amanatides and Andrew Woo

#include <cstdlib>
#include <cassert>
#include <iostream>

void visit(int x, int y)
{
    std::cout << "(" << x << ", " << y << ")\n";
}

// this isn't DDA since DDA plots exactly max(dx, dy) dots while this does more
void trace_cells(int x0, int y0,
                 int x1, int y1)
{
    // this shouldn't be unsigned due to its usage in std::max(dx - 1, 0) below
    const auto dx = std::abs(x1 - x0);
    const auto dy = std::abs(y1 - y0);
    assert(!(dx == 0 && dy == 0));

    const auto step_x = x1 > x0 ? 1 : -1;
    const auto step_y = y1 > y0 ? 1 : -1;

    // error is a fraction m = dy/dx, which is scaled by dx to avoid floating-point
    // calculations; when dy > dx, err = 1/m = dx/dy scaled by dy
    const bool horizontal = dx >= dy;
    int err = 0;

    // number of cells in between - 1 (leaving the first cell) in both directions
    // and adding one commonly for the first; another way to look at it is number
    // of horizontal and vertical grid line intersections + 1
    int cells = std::max(dx - 1, 0) + std::max(dy - 1, 0) + 1u;
    // greater than 0 to make sure that the additional dec. isn't leading to a non-zero negative value
    while (0 < cells--)
    {
        // each cell is denoted by its lower left corner point on the grid from the
        // point of view of the ray (this matters when x1 < x0 or y1 < y0)
        visit(x0, y0);
        if (horizontal)
        {
            // the actual y value in real space equals y0 (integer) + err (fraction)
            err += dy;
            if (err > dx)
            {
                y0 += step_y;
                /*
                 * once enough error has gotten accumulated (err > 1) remove one from
                 * it and continue to keep it as a fraction e.g. for dy/dx = 3/5 for 
                 * iteration 2 we've err = 2 * 3/5 = 6/5, we know we've to remove 5/5
                 * from it to make it 1/5, but 1/5 is the error in the next stepping
                 * of x0 and not this one, hence also subtract dy/dx from it apart from
                 * dx/dx 1/5 - 3/5 = -2/5 which is the right error for this x value as
                 * the real y should be lesser than (below) y0; also with the next err
                 * increment of 3/5, the error would rightly become 1/5 for that x0
                 */
                err -= (dx + dy);
            }
            else if (err < dx)
            {
                x0 += step_x;
            }
            else //if (err == dx)
            {
                x0 += step_x;
                y0 += step_y;
                err = 0u;
                // when the line passes through a vertex, the new point found is exactly on a vertex then the
                // line intersection count should be decremented once more since there're two intersections
                --cells;
            }

        }
        else
        {
            err += dx;
            if (err > dy)
            {
                x0 += step_x;
                err -= (dy + dx);
            }
            else if (err < dy)
            {
                y0 += step_y;
            }
            else //if (err == dx)
            {
                x0 += step_x;
                y0 += step_y;
                err = 0u;
                --cells;
            }
        }
    }
}

int main()
{
    trace_cells(11, 22, 16, 25);    // horizontal left to right, bottom to top
    std::cout << '\n';
    trace_cells(16, 24, 11, 22);    // horizontal right to left, top to bottom
    std::cout << '\n';
    trace_cells(11, 22, 15, 27);    // vertical bottom to top, left to right
    std::cout << '\n';
    trace_cells(14, 27, 11, 22);    // vertical top to bottom, right to left
    std::cout << '\n';
    trace_cells(14, 27, 13, 22);    // (almost) pure vertical top to bottom, right to left
    std::cout << '\n';
    trace_cells(12, 24, 15, 24);    // pure horizontal left to right, equal
    std::cout << '\n';
    trace_cells(16, 22, 11, 27);    // diagonal line
    std::cout << '\n';
    trace_cells(-78, 5, -82, -15);  // case where the line passes through a vertex but not a diagonal line
}
