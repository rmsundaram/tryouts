// based on http://playtechs.blogspot.in/2007/03/raytracing-on-grid.html
// Graphics Gems 4: Voxel Traversal along a 3D Line is the site author's reference
// another similar paper is voxel ray traversal in 3D which does it with floats
// A Fast Voxel Traversal Algorithm for Ray Tracing by John Amanatides and Andrew Woo

#include <cstdlib>
#include <cassert>
#include <cmath>
#include <iostream>

void visit(int x, int y)
{
    std::cout << "(" << x << ", " << y << ")\n";
}

/*
 * another way to do this would be to start from the ray origin, find tx for x + step_x
 * and ty for y + step_y and see the lesser of the two to find the exit point and use
 * that for the next iteration as the starting point; however that implementation would
 * have had more float to int conversions than this
 */
// this isn't DDA since DDA plots exactly max(dx, dy) dots while this does more
void trace_cells(int x0, int y0,
                 int x1, int y1)
{
    // this shouldn't be unsigned due to its usage in std::max(dx - 1, 0) below
    const auto dx = std::abs(x1 - x0);
    const auto dy = std::abs(y1 - y0);
    assert(!(dx == 0 && dy == 0));

    const auto step_x = x1 > x0 ? 1 : -1;
    const auto step_y = y1 > y0 ? 1 : -1;

    // number of cells in between - 1 (leaving the first cell) in both directions
    // and adding one commonly for the first; another way to look at it is number
    // of horizontal and vertical grid line intersections + 1
    int cells = std::max(dx - 1, 0) + std::max(dy - 1, 0) + 1u;
    // when error is positive, then ray is intersecting a line x = k, so increment x and y is rational
    // when it's positive, the ray is intersecting a line y = k, so increment y and x is in rational
    int err = dx - dy;
    // greater than 0 to make sure that the additional dec. isn't leading to a non-zero negative value
    while (0 < cells--)
    {
        const auto cell_x = x0, cell_y = y0;
        if (err > 0)
        {
            x0 += step_x;
            err -= dy;
        }
        else if (err < 0)
        {
            y0 += step_y;
            err += dx;
        }
        else
        {
            x0 += step_x;
            y0 += step_y;
            err += dx - dy;
            // when the line passes through a vertex, the new point found is exactly on a vertex then the
            // line intersection count should be decremented once more since there're two intersections
            --cells;
        }
        visit(cell_x, cell_y);
    }
}

int main()
{
    trace_cells(11, 22, 16, 25);    // horizontal left to right, bottom to top
    std::cout << '\n';
    trace_cells(16, 24, 11, 22);    // horizontal right to left, top to bottom
    std::cout << '\n';
    trace_cells(11, 22, 15, 27);    // vertical bottom to top, left to right
    std::cout << '\n';
    trace_cells(14, 27, 11, 22);    // vertical top to bottom, right to left
    std::cout << '\n';
    trace_cells(14, 27, 13, 22);    // (almost) pure vertical top to bottom, right to left
    std::cout << '\n';
    trace_cells(12, 24, 15, 24);    // pure horizontal left to right, equal
    std::cout << '\n';
    trace_cells(16, 22, 11, 27);
    std::cout << '\n';
    trace_cells(-78, 5, -82, -15);
}
