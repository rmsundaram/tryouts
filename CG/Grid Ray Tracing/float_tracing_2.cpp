// command to get all the GCC macros defined
// gcc -dM -E -
// pass required flags along to see macros defined for them too like
// echo .|gcc -ffast-math -dM -E -|grep "FAST_MATH"

#include <cmath>
#include <cassert>
#include <limits>
#include <iostream>

void visit(float x, float y,
           float out_x, float out_y,
           float t)
{
    std::cout << '(' << x << ", " << y << ") to ("
              << out_x << ", " << out_y << ")\t" << t << '\n';
}

struct Ray
{
    float ox, oy;
    float dx, dy;
};

// using std::numeric_limits<float>::epsilon() leads to an infinite loop
// since the condition in while fails
constexpr auto epsilon = 0.000001f;

bool equal(float lhs, float rhs)
{
    return std::abs(lhs - rhs) < epsilon;
}

float distance(float x1, float x2)
{
    return std::abs(x2 - x1);
}

float closer(float x, float x1, float x2)
{
    // finding the lesser of the two after taking abs on both was considered
    // but it will not work when one is positive and the other is not
    return (distance(x, x1) <= distance(x, x2)) ? x1 : x2;
}

// dx is really vector, it has the direction and the magnitude of shift;
// if positive round towards -∞ (floor) else towards +∞ (ceil)
// and add the quantity; dx = 0 is actually a degenerate case
float next_int(float x, float dx)
{
    return ((dx >= 0.0f) ? std::floor(x) : std::ceil(x)) + dx;
}

void cell_traverse(const Ray &r)
{
    assert((r.dx != 0) || (r.dy != 0));

    const auto dest_x = r.ox + r.dx;
    const auto dest_y = r.oy + r.dy;
    const auto step_x = (r.dx != 0.0f) ? (r.dx > 0.0f ? 1.0f : -1.0f) : 0.0f;
    const auto step_y = (r.dy != 0.0f) ? (r.dy > 0.0f ? 1.0f : -1.0f) : 0.0f;
    auto x = r.ox, y = r.oy;
    while (!equal(x, dest_x) || !equal(y, dest_y))
    {
        const auto cur_x = x, cur_y = y;
        // next value would be a line x = k or the destination x
        const auto next_x = closer(x, next_int(x, step_x), dest_x);
        const auto next_y = closer(y, next_int(y, step_y), dest_y);
        auto t = 0.0f;
#ifndef __FAST_MATH__
        const auto tx = (next_x - r.ox) / r.dx;
        const auto ty = (next_y - r.oy) / r.dy;
        // instead of checking for infinity (isinf) checking for
        // both INF and NAN (isfinite) since dx = 0 → tx = 0/0 = NAN
        if ((!std::isfinite(ty)) || (std::isfinite(tx) && (tx <= ty)))
        // even when ty = NAN, std::isfinite returns false when -ffast-math is used
        // verified on GCC (MinGW) 4.8.1
#else
        const auto tx = r.dx ? ((next_x - r.ox) / r.dx) : 0.0f;
        const auto ty = r.dy ? ((next_y - r.oy) / r.dy) : 0.0f;
        if ((ty == 0.0f) || ((tx != 0.0f) && (tx <= ty)))
#endif
        {
            // y = r.oy + tx * r.dy;
            y = std::fma(tx, r.dy, r.oy);
            x = next_x;
            t = tx;
        }
        else
        {
            // x = r.ox + ty * r.dx;
            x = std::fma(ty, r.dx, r.ox);
            y = next_y;
            t = ty;
        }
        visit(cur_x, cur_y, x, y, t);
    }
    std::cout << std::endl;
}

/*
++++

+---
-+--
--+-
---+

++--
-++-
--++

+--+
+-+-
-+-+

+++-
-+++
+-++
++-+

----
*/

int main()
{
    // cell_traverse({7.2f, 9.7f, 4.0f, 3.0f});

    // cell_traverse({7.2f, -9.7f, -4.0f, -3.0f});
    // cell_traverse({-7.2f, 9.7f, -4.0f, -3.0f});
    // cell_traverse({-7.2f, -9.7f, 4.0f, -3.0f});
    // cell_traverse({-7.2f, -9.7f, -4.0f, 3.0f});

    // cell_traverse({7.2f, 9.7f, -4.0f, -3.0f});
    // cell_traverse({-7.2f, 9.7f, 4.0f, -3.0f});
    // cell_traverse({-7.2f, -9.7f, 4.0f, 3.0f});

    // cell_traverse({7.2f, -9.7f, -4.0f, 3.0f});
    // cell_traverse({7.2f, -9.7f, 4.0f, -3.0f});
    // cell_traverse({-7.2f, 9.7f, -4.0f, 3.0f});

    // cell_traverse({7.2f, 9.7f, 4.0f, -3.0f});
    // cell_traverse({-7.2f, 9.7f, 4.0f, 3.0f});
    // cell_traverse({7.2f, -9.7f, 4.0f, 3.0f});
    // cell_traverse({7.2f, 9.7f, -4.0f, 3.0f});

    // cell_traverse({-7.2f, -9.7f, -4.0f, -3.0f});

    cell_traverse({0.0f, 0.0f, 5.0f, 5.0f});
    cell_traverse({-8.8f, 9.7f, -4.0f, 0.0f});
    cell_traverse({-8.8f, 9.7f, 0.0f, 3.0f});

}
