Initially just finding the cells was written (`grid_ray_trace_1.cpp`); the need for each cell's exit point was later recognized leading to the _hit_ variant (`grid_ray_trace_hit_1.cpp`).

Version 1 was hand written with my own logic and the image shows the workout, while version 3 is from a website which takes it from _Game Programming Gems 4_. Version 2 is buggy due the attempt to generalize everything :)

When benchmarked (`benchmark.cpp`), surprisingly both 1 and 3 give very similar results right from `-O1` onwards, only when `-O0` is given do I see that 3 is very slightly faster.  This shows that not everything that looks fast isn't so; with optimization enabled, even non-cryptic code seems to run fast too.  _Want speed? Measure_.

# References

* [A Fast Voxel Traversal Algorithm for Ray Tracing](http://www.cse.yorku.ca/~amana/research/grid.pdf) by John Amanatides, Andrew Woo
