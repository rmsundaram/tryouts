// g++ -Wall -std=c++11 First.cpp -lglfw -lGLU -lGL

#include "GLFW/glfw3.h"
#ifdef _WIN32
#  include <minwindef.h>
#endif
#include "GL/glext.h"
#include "GL/glu.h"
#include <stdexcept>
#include <string>
#include <functional>
#include <vector>
#include <iostream>

#include "First.h"

using std::vector;
using std::cout;
using std::endl;

void Square::putVertices() const
{
    const unsigned uHalfSize = uSize / 2;
    glColor3ub(0xFF, 0, 0);
    glVertex2i(ptCentre.iX - uHalfSize, ptCentre.iY - uHalfSize);
    glColor3ub(0, 0xFF, 0);
    glVertex2i(ptCentre.iX + uHalfSize, ptCentre.iY - uHalfSize);
    glColor3ub(0, 0, 0xFF);
    glVertex2i(ptCentre.iX + uHalfSize, ptCentre.iY + uHalfSize);
    glColor3ub(0xFF, 0xFF, 0xFF);
    glVertex2i(ptCentre.iX - uHalfSize, ptCentre.iY + uHalfSize);
}

GLenum Square::getPrimitiveType() const
{
    return GL_QUADS;
}

void paint(const vector<Square> &rgModels)
{
    glClear(GL_COLOR_BUFFER_BIT);

    vector<Square>::const_iterator it = rgModels.cbegin();
    for(; it != rgModels.cend(); ++it)
    {
        glBegin(it->getPrimitiveType());
        it->putVertices();
        glEnd();
    }
}

inline int flipYAxis(int y)
{
    return SCREEN_HEIGHT - y;
}

bool cPressed = false;

void updateStates(vector<Square> &rgModels, GLFWwindow* win)
{
    if (cPressed)
    {
        const size_t numSquares = rgModels.size();
        cout << numSquares << endl;
        if (numSquares < 10)
        {
            double pointerPosX, pointerPosY;
            glfwGetCursorPos(win, &pointerPosX, &pointerPosY);
            rgModels.push_back(Square(Point(pointerPosX,
                                            flipYAxis(pointerPosY)),
                                      30));
        }
        cPressed = false;
    }
}

void processInput(GLFWwindow* /*win*/,
                  int key,
                  int /*scancode*/,
                  int action,
                  int /*mods*/)
{
    if (('C' == key) && (GLFW_RELEASE == action))
    {
        cPressed = true;
    }
}

void setup2D()
{
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_DITHER);
    glDisable(GL_MULTISAMPLE);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    gluOrtho2D(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT);
}

int main()
{
    GLFWSystem glfw(glfwInit,
                    glfwTerminate,
                    "Failed to initialize GLFW");
       auto* win = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT,
                                    "First OpenGL", NULL, NULL);
        if (win)
        {
            glfwSetKeyCallback(win, processInput);
            glfwMakeContextCurrent(win);

            vector<Square> rgModels;
            rgModels.reserve(5);

            setup2D();

            while (!glfwWindowShouldClose(win) &&
                   (glfwGetKey(win, GLFW_KEY_ESCAPE) != GLFW_PRESS))
            {
                updateStates(rgModels, win);     // query the input here for now
                paint(rgModels);
                glfwSwapBuffers(win);
                glfwPollEvents();
            }
        }
}
