#ifndef __FIRST_H__
#define __FIRST_H__

const unsigned SCREEN_WIDTH = 800;
const unsigned SCREEN_HEIGHT = 600;

struct Point
{
	Point(int X = 0, int Y = 0) : iX(X), iY(Y)
	{
	}

	int iX, iY;
};

struct Square
{
	Square(const Point &ptCentre_ = Point(), unsigned uSize_ = 0) : ptCentre(ptCentre_), uSize(uSize_)
	{
	}

	Point& GetCentre()
	{
		return ptCentre;
	}

	const Point& GetCentre() const
	{
		return ptCentre;
	}

	unsigned& GetSize()
	{
		return uSize;
	}

	unsigned GetSize() const
	{
		return uSize;
	}

	GLenum getPrimitiveType() const;
	void putVertices() const;

private:
	Point ptCentre;
	unsigned uSize;
};

template <typename UninitFuncType,
		  typename SuccessValueType,
		  SuccessValueType successValue>
class RAIIWrapper
{
public:
	template <typename InitFuncType, typename... Args>
	RAIIWrapper(InitFuncType fpInitFunc,
				UninitFuncType fpUninitFunc,
				std::string errorString,
				const Args&... args) :
		fpUninit(fpUninitFunc)
	{
		if (successValue != fpInitFunc(args...))
			throw std::runtime_error(errorString);
	}

	~RAIIWrapper()
	{
        fpUninit();
	}

private:
	std::function<UninitFuncType> fpUninit;
};

using GLFWSystem = RAIIWrapper<decltype(glfwTerminate),
                               decltype(GL_TRUE),
                               GL_TRUE>;

#endif
