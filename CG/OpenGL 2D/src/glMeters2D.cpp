// g++ -Wall -std=c++11 First.cpp -lglfw3 -lopengl32 -lglu32

#include <GLFW/glfw3.h>
#ifdef _WIN32
#  include <minwindef.h>
#endif
#include <GL/glu.h>
#include <stdexcept>

struct glfwRaii
{
    glfwRaii()
    {
        if (GL_FALSE == glfwInit())
        {
            throw std::runtime_error("Error initializing GLFW!");
        }
    }
    ~glfwRaii()
    {
        glfwTerminate();
    }
};

auto screenWidth = 800;
auto screenHeight = 600;

auto screenWidthMeters = screenWidth * 0.02f;
auto screenHeightMeters = screenHeight * 0.02f;

void setup2D()
{
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_DITHER);

#ifdef GL_MULTISAMPLE
    glDisable(GL_MULTISAMPLE);
#endif
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    gluOrtho2D(0, screenWidthMeters, 0, screenHeightMeters);

    glMatrixMode(GL_MODELVIEW);

    // for points, lines and polygons to be smooth
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_FASTEST);
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_FASTEST);
    glEnable(GL_POINT_SMOOTH);
    glHint(GL_POINT_SMOOTH_HINT, GL_FASTEST);
}

void paint()
{
    glBegin(GL_LINE_LOOP);
    glVertex2f(1.f, 11.9f);
    glVertex2f(2.f, 11.9f);
    glVertex2f(1.5f, 10.f);
    glEnd();

    glBegin(GL_POLYGON);
    glVertex2f(0.f, 0.f);
    glVertex2f(15.9f, 0.f);
    glVertex2f(15.9f, 1.f);
    glVertex2f(0.f, 1.f);
    glEnd();
}

int main()
{
    glfwRaii glfwInstance;
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
    auto* win = glfwCreateWindow(screenWidth, screenHeight, "glMeters2D", NULL, NULL);
    if (win)
    {
        glfwMakeContextCurrent(win);
        setup2D();
        while (!glfwWindowShouldClose(win) && (glfwGetKey(win, GLFW_KEY_ESCAPE) != GLFW_PRESS))
        {
            paint();
            glfwSwapBuffers(win);
            glfwPollEvents();
        }
    }
}
