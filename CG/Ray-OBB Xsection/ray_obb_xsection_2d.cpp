//g++ -Wall -std=c++11 -pedantic -IC:/Work/Libs/glm ray_obb_xsection_2d.cpp

/*
 * Dr. Ravi explains that ray-ellipsoid intersection can be done by transforming the ray into a space where the
 * ellipsoid (world) is a sphere (model) i.e. the ray and ellipsoid are transformed by the matrix Minv, the inverse
 * of M which transforms (shear/scale) the unit sphere into an ellipsoid. Now a simple ray-sphere intersection
 * performed in the model space would give the resulting point of intersection, if any; this is then transformed
 * back to the world space by transforming it using the model to world matrix M. This technique is verified here with
 * a simpler example. We've a AABB in model space which after undergoing rotation and translation (Mmodel->world)
 * becomes an OBB in world space. Given a ray in world space, instead of performing Ray-OBB intersection, the ray
 * is transformed into the model space using Mworld->model. Now a simple ray-AABB intersection testing is done in the
 * model space, if it intersects, the obtained intersection point is transformed back to the world space.
 */

#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <iostream>
#include <limits>

struct AABB
{
    glm::vec3 min;  // left_bottom
    glm::vec3 max;  // right_top
};

struct Ray
{
    glm::vec3 org;
    glm::vec3 dir;
};

// TODO: see if the right-to-left case may be folded into left-to-right by swapping min and max initially
// assumes a system with Y axis going from bottom to top (bottom < top)
bool ray_aabb_intersect(const Ray &ray,
                        const AABB &box,
                        float *dist,
                        glm::vec3 *point = nullptr)
{
    // if the box is behind or below the ray return false early
    if (((ray.dir.x > 0.0f) && (ray.org.x >= box.max.x)) ||
        ((ray.dir.x < 0.0f) && (ray.org.x <= box.min.x)) ||
        ((ray.dir.y > 0.0f) && (ray.org.y >= box.max.y)) ||
        ((ray.dir.y < 0.0f) && (ray.org.y <= box.min.y)))
        return false;

    // the parameter t is stored in the z coordinate of intersection vec3
    glm::vec3 X(std::numeric_limits<float>::max());
    /* 
     * if box in front of ray then first intersect with lines of equation x = k (X intersection, X.x) with
     * k = min else with k = max; this is when ray is from left to right, handle for opposite case too.
     * X.x and t are unaltered if ray is parallel to the lines x = k i.e. ray.dir.x = 0
     */
    // here (ray.org.x <= box.max.x) should've been accompanied by a second clause (ray.org.x > box.min.x)
    // which makes sure the box isn't behind the ray but that's already handled above hence omitting that
    X.x = (((ray.dir.x > 0.0f) && (ray.org.x < box.min.x)) ||                         // --> |   |
          ((ray.dir.x < 0.0f) && (ray.org.x <= box.max.x))) ? box.min.x :             // | <--|
          (((ray.dir.x < 0.0f) && (ray.org.x > box.max.x)) ||                         // |   | <--
          ((ray.dir.x > 0.0f) && (ray.org.x >= box.min.x))) ? box.max.x : X.x;        // |--> |
    if (X.x != std::numeric_limits<float>::max())     // test xsection (true unless ray is purely vertical, dir.x = 0)
    {
        // calculate the parameter t
        X.z = (X.x - ray.org.x) / ray.dir.x;
        X.y = ray.org.y + X.z * ray.dir.y;
        // if calculated Y component of intersection is not within bounds then reset parameter
        if ((X.y < box.min.y) || (X.y > box.max.y))
            X.z = std::numeric_limits<float>::max();
    }
    // same as above for Y intersection
    glm::vec3 Y(std::numeric_limits<float>::max());
    Y.y = (((ray.dir.y > 0.0f) && (ray.org.y < box.min.y)) ||
          ((ray.dir.y < 0.0f) && (ray.org.y <= box.max.y))) ? box.min.y :
          (((ray.dir.y < 0.0f) && (ray.org.y > box.max.y)) ||
          ((ray.dir.y > 0.0f) && (ray.org.y >= box.min.y))) ? box.max.y : Y.y;
    if (Y.y != std::numeric_limits<float>::max())
    {
        Y.z = (Y.y - ray.org.y) / ray.dir.y;
        Y.x = ray.org.x + Y.z * ray.dir.x;
        if ((Y.x < box.min.x) || (Y.x > box.max.x))
            Y.z = std::numeric_limits<float>::max();
    }

    // no intersection
    if ((std::numeric_limits<float>::max() == X.z) && (std::numeric_limits<float>::max() == Y.z))
        return false;

    *dist = (X.z < Y.z) ? X.z : Y.z;
    if (point)
    {
        *point = (X.z < Y.z) ? X : Y;
        point->z = 1.0f;                   // homogenous coordinate for point
    }
    return true;
}

int main()
{
    glm::vec3 modelPt[4] = {
                            {-0.5f, -0.5f, 1.0f},
                            { 0.5f, -0.5f, 1.0f},
                            { 0.5f,  0.5f, 1.0f},
                            {-0.5f,  0.5f, 1.0f}
                           };
    glm::mat3 T{
                 { 1.0f, 0.0f, 0.0f },
                 { 0.0f, 1.0f, 0.0f },
                 { 2.0f, 2.0f, 1.0f }
               };
    // since sin 45 = cos 45, just calculate it once
    const auto a = glm::cos(glm::quarter_pi<float>());
    glm::mat3 R{
                 {    a,    a, 0.0f},
                 {   -a,    a, 0.0f},
                 { 0.0f, 0.0f, 1.0f}
               };
    const auto MmTow = T * R;
    std::cout << "Model To World:\n" << glm::to_string(MmTow) << "\n\n";
    glm::vec3 worldPt[4] = {
                             MmTow * modelPt[0],
                             MmTow * modelPt[1],
                             MmTow * modelPt[2],
                             MmTow * modelPt[3]
                           };
    std::cout << "Box in Model Space:\n"
              << glm::to_string(modelPt[0]) << '\n'
              << glm::to_string(modelPt[1]) << '\n'
              << glm::to_string(modelPt[2]) << '\n'
              << glm::to_string(modelPt[3]) << "\n\n";

    std::cout << "Box in World Space:\n"
              << glm::to_string(worldPt[0]) << '\n'
              << glm::to_string(worldPt[1]) << '\n'
              << glm::to_string(worldPt[2]) << '\n'
              << glm::to_string(worldPt[3]) << "\n\n";

    glm::mat3 Rinv{
                    {   a,   -a, 0.0f},
                    {   a,    a, 0.0f},
                    {0.0f, 0.0f, 1.0f}
                  };
    glm::mat3 Tinv{
                    {  1.0f,  0.0f, 0.0f },
                    {  0.0f,  1.0f, 0.0f },
                    { -2.0f, -2.0f, 1.0f }
                  };
    glm::mat3 MwTom = Rinv * Tinv;
    std::cout << "World To Model:\n" << glm::to_string(MwTom) << "\n\n";
    glm::vec3 modelPtVerify[4] = {
                                   MwTom * worldPt[0],
                                   MwTom * worldPt[1],
                                   MwTom * worldPt[2],
                                   MwTom * worldPt[3]
                                 };
    std::cout << "Box in Model Space: (verification)\n"
              << glm::to_string(modelPtVerify[0]) << '\n'
              << glm::to_string(modelPtVerify[1]) << '\n'
              << glm::to_string(modelPtVerify[2]) << '\n'
              << glm::to_string(modelPtVerify[3]) << "\n\n";

    glm::vec3 Ray_world_org{ 0.0f, 0.0f, 1.0f };
    glm::vec3 Ray_world_dir{ 1.0f, 1.0f, 0.0f };
    glm::vec3 Ray_world_end{ 1.0f, 1.0f, 1.0f }; // org + dir

    std::cout << "Ray in World Space:\n"
              << "Origin:    " << glm::to_string(Ray_world_org) << '\n'
              << "Direction: " << glm::to_string(Ray_world_dir) << '\n'
              << "End:       " << glm::to_string(Ray_world_end) << "\n\n";

    const auto Ray_model_org = MwTom * Ray_world_org;
    // ray.dir is a contravariant (parallel) vetor, hence no need to transform by inverse transpose
    // unlike covariant (perpendicular) (co)vectors which have to remain perpendicular
    // see note in page 96 of OpenGL 4.0 Shading Language Cookbook, 1/e
    // http://en.wikipedia.org/wiki/Covariance_and_contravariance_of_vectors
    // http://www.idius.net/tutorials/tensor-calculus/contra-vs-covariant/
    const auto Ray_model_dir = MwTom * Ray_world_dir;
    const auto Ray_model_end = MwTom * Ray_world_end;

    std::cout << "Ray in Model Space:\n"
              << "Origin:    " << glm::to_string(Ray_model_org) << '\n'
              << "Direction: " << glm::to_string(Ray_model_dir) << '\n'
              << "End:       " << glm::to_string(Ray_model_end) << "\n\n";

    AABB box = {{-0.5f,-0.5f, 1.0f},
                { 0.5f, 0.5f, 1.0f}};
    Ray ray = { Ray_model_org, Ray_model_dir };
    float dist;
    glm::vec3 xPtModel;
    if (ray_aabb_intersect(ray, box, &dist, &xPtModel))
    {
        const auto xPtWorld = MmTow * xPtModel;
        // just for verification
        const auto dist_world = glm::dot((xPtWorld - Ray_world_org), Ray_world_dir) /
                                glm::dot(Ray_world_dir, Ray_world_dir);
        // since there's no scale difference between world and model space, this value
        // should be the same as dist in model space

        std::cout << "Distance along ray in model space = "
                  << dist << '\n'
                  << "Intersection in model space = "
                  << glm::to_string(xPtModel) << '\n'
                  << "Intersection in world space = "
                  << glm::to_string(xPtWorld) << '\n'
                  << "Distance along ray in world space = "
                  << dist_world << std::endl;
                  // distance is same in both spaces since there's no scale or shear
                  // in Mmodel->world transform; however affine transforms in general don't
                  // preserve angles between lines or distance between points; they only
                  // preserve ratios of lengths, collinearity and parallelism
    }
    else
        std::cout << "No intersection" << std::endl;
}
