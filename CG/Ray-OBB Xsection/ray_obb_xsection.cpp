#include <glm/glm.hpp>
#include <cmath>
#include <iostream>

struct Ray
{
    glm::vec3 origin;
    glm::vec3 direction;
};

struct OBB
{
    glm::vec3 centre;
    glm::vec3 half_extent;
    glm::mat3 xform;
};

bool intersectRayOBB(const Ray &r,
                     const OBB &B,
                     float *t)
{
    const float epsilon = 10.0E-20f;

    float tFarthestEntry = -std::numeric_limits<float>::max();
    float tNearestExit   =  std::numeric_limits<float>::max();
    const auto p = B.centre - r.origin;
    for (auto i = 0u; i < 3; ++i)
    {
        const auto e = glm::dot(B.xform[i], p);
        const auto f = glm::dot(B.xform[i], r.direction);

        // ray not parallel to slab planes
        if (std::abs(f) > epsilon)
        {
            const auto reciprocalF = 1.0f / f;
            // calculate entry and exit
            const auto tEntry = (e + B.half_extent[i]) * reciprocalF;
            const auto tExit = (e - B.half_extent[i]) * reciprocalF;
            if (tExit > tEntry)
            {
                if (tExit < tNearestExit) tNearestExit = tExit;
                if (tEntry > tFarthestEntry) tFarthestEntry = tEntry;
            }
            else
            {
                if (tEntry < tNearestExit) tNearestExit = tEntry;
                if (tExit > tFarthestEntry) tFarthestEntry = tExit;
            }
            if ((tFarthestEntry > tNearestExit) || (tNearestExit < 0.0f))
                return false;
        }
        else if (((-B.half_extent[i] - e) > 0.0f) || ((B.half_extent[i] - e) < 0.0f))
            return false;
    }
    *t = (tFarthestEntry > 0.0f) ? tFarthestEntry : tNearestExit;
    return true;
}

int main()
{
    Ray r = { {0.0f, 0.0f, 0.0f}, {1.0f, 1.0f, 0.0f} };
    OBB B = { {5.0f, 5.0f, 1.0f}, {1.0f, 1.0f, 1.0f} };

    float t{};
    std::cout << std::boolalpha << intersectRayOBB(r, B, &t);
    std::cout << '\t' << t << std::endl;
}
