// http://www.gamedev.net/topic/609818-quadtree-terrain-with-odd-of-vertices/#entry4857063

#include <glm/glm.hpp>
#include <glm/gtx/string_cast.hpp>
#include <vector>
#include <iostream>
#include <limits>
#include <cstdlib>
#include <cassert>

#ifdef _MSC_VER
	#include <intrin.h>
#endif

inline unsigned long ulog2(unsigned long x)
{
	// log 0 is undefined for all bases, since there's no number the base can be raised-to to get 0
	if (0 == x) return 0;

#ifdef __GNUC__
	// passing signed T to numeric_limits<T>::digits will give total bits - 1 and shouldn't be used in this case
	// since it hampers readability as an unsigned int is later passed as input to clz
	constexpr auto max_i_index = std::numeric_limits<unsigned int>::digits - 1; // = 31
	return max_i_index - __builtin_clzl(x);
#else
	unsigned long value;
	_BitScanReverse(&value, x);
	return value;
#endif
}

// Orson Peters (nightcracker)
// http://gist.github.com/orlp/3551590
int64_t ipow(int32_t base, uint8_t exp)
{
    static const uint8_t highest_bit_set[] =
    {
        0, 1, 2, 2, 3, 3, 3, 3,
        4, 4, 4, 4, 4, 4, 4, 4,
        5, 5, 5, 5, 5, 5, 5, 5,
        5, 5, 5, 5, 5, 5, 5, 5,
        6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 6, 6, 6, 255, // anything past 63 is a guaranteed overflow with base > 1
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
    };

    uint64_t result = 1;

    switch (highest_bit_set[exp])
    {
    case 255: // we use 255 as an overflow marker and return 0 on overflow/underflow
        if (base == 1)
            return 1;

        if (base == -1)
            return 1 - 2 * (exp & 1);

        return 0;
    case 6:
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    case 5:
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    case 4:
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    case 3:
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    case 2:
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    case 1:
        if (exp & 1) result *= base;
    default:
        return result;
    }
}

struct AABB_2D
{
    glm::vec2 centre;
    float hextent;
};

struct QuadTree
{
    QuadTree(unsigned total_cells, unsigned leaf_cells, float cell_size);
    std::vector<AABB_2D> nodes;

    uint16_t total_nodes(uint8_t depth) const;
    uint8_t max_depth() const;
    uint16_t offset(uint8_t depth) const;
    uint16_t child_offset(uint16_t node) const;
    uint16_t descendant_leaf_offset(uint16_t node, uint16_t *offset) const;
    // returns true if node is below a given depth, root is at depth 1
    // e.g. is_descendant(5, 3) = false, is_descendant(19, 3) = false, is_descendant(21, 3) = true
    bool is_descendant(uint16_t node, uint8_t depth) const;
};

enum class DIRECTION : uint8_t
{
    NORTH_WEST = 0,
    NORTH_EAST,
    SOUTH_WEST,
    SOUTH_EAST
};

void split_space(std::vector<AABB_2D> &bounds, uint16_t nodes)
{
    for (std::vector<AABB_2D>::size_type i = 1u; i < nodes; ++i)
    {
        auto const parent = (i - 1u) / 4u;
        auto const first_child = (parent * 4u) + 1u;
        auto const child_quad = static_cast<DIRECTION>(i - first_child);
        auto const hextent = 0.5f * bounds[parent].hextent;
        switch (child_quad)
        {
            // although vec2 calls it as (x, y) in 3D it's (x, z) since
            // x goes right, y goes up and z goes out
            // hence north would mean -y and west would mean -x
        case DIRECTION::NORTH_WEST:
            bounds.push_back({{bounds[parent].centre.x - hextent,
                               bounds[parent].centre.y - hextent}, hextent});
            break;
        case DIRECTION::NORTH_EAST:
            bounds.push_back({{bounds[parent].centre.x + hextent,
                               bounds[parent].centre.y - hextent}, hextent});
            break;
        case DIRECTION::SOUTH_WEST:
            bounds.push_back({{bounds[parent].centre.x - hextent,
                               bounds[parent].centre.y + hextent}, hextent});
            break;
        case DIRECTION::SOUTH_EAST:
            bounds.push_back({{bounds[parent].centre.x + hextent,
                               bounds[parent].centre.y + hextent}, hextent});
            break;
        }
    }
}

QuadTree::QuadTree(unsigned total_cells, unsigned leaf_cells, float cell_size)
{
    bool const dummy = (total_cells % 2u == 0);
    if (dummy)
    {
        std::cout << "Dummy row and column added\n";
        total_cells += 1;
    }
    unsigned const cells = total_cells - 1;
    assert(cells % 4 == 0);
    unsigned const leaves = total_cells / leaf_cells;     // compute L, leaf count in 1D
    // 1 for the root, then log₄L², the squaring is to make the count in 2D
    unsigned const depth = 1u + ulog2(leaves);
    unsigned const n_nodes = total_nodes(depth);
    unsigned const leaf_offset = total_nodes(depth - 1);
    std::cout << "Tree depth = " << depth << '\n';
    std::cout << "Nodes = " << n_nodes << '\n';
    std::cout << "Leaves offset = " << leaf_offset << '\n';
    std::cout << "Leaf count = " << n_nodes - leaf_offset << '\n';
    // (leaves * leaves) or ipow(4, depth - 1) would also work
    // the latter formula may be obtained by simplifying
    // n_nodes - leaf_offset i.e. total_nodes(depth) - total_nodes(depth - 1)

    float const half_extent = (cells / 2) * cell_size;
    nodes.reserve(n_nodes);
    // the world always starts at (0, 0, 0)
    nodes.push_back({ { 0.0f, 0.0f }, half_extent });
    split_space(nodes, n_nodes);
}

uint16_t QuadTree::total_nodes(uint8_t depth) const
{
    // sum of geometric series, a = 1, r = 4
    // a (rⁿ− 1) / (r − 1)
    return (ipow(4, depth) - 1) / 3;
}

uint8_t QuadTree::max_depth() const
{
    // from the same formula used in total_nodes
    const auto depth = ulog2(3u * nodes.size() + 1u) / 2u;
    assert(depth >= 1u);
    return depth;
}

uint16_t QuadTree::child_offset(uint16_t node) const
{
    // leaves don't have children, make sure this isn't a leaf
    assert(is_descendant(node, max_depth() - 1) == false);
    return 4u * node + 1;
    // each node before this would've 4 children + 1 to go the actual child
    // e.g. node = 3, nodes before 3 = {0, 1, 2}, total 3 nodes * 4 = 12 children
    // starting at 1 ending at 12; adding 1 gives the start offset of 3's children
}

uint16_t QuadTree::offset(uint8_t depth) const
{
    assert(depth > 0u && depth <= max_depth());
    // total nodes in a tree with one depth lesser would be the beginning offset of given depth
    const auto offset = total_nodes(depth - 1u);
    assert(offset < nodes.size());
    return offset;
}

uint16_t QuadTree::descendant_leaf_offset(uint16_t node, uint16_t *offset) const
{
    // 4x + 1 gives the child offset of a node (see child_offset function)
    // but this child may have a child in turn, thus we need the final child offset
    // in this chain. f¹(x) = 4x + 1, f²(x) = 4(4x + 1) + 1, f³(x) = 4(4(4x + 1) + 1) + 1
    // fⁿ(x) = 4ⁿx + ((4ⁿ− 1) / 3), where n is max_depth - node depth = subtree depth - 1
    const auto node_depth = (ulog2(3u * node + 1u) / 2u) + 1u; // same formula as max_depth with 1 added to the result
    const unsigned pow = ipow(4, max_depth() - node_depth); // 4ⁿ
    *offset = (pow * node) + ((pow - 1) / 3);
    return pow; // is also the leaf count of the subtree (see constructor for explanation)
}

bool QuadTree::is_descendant(uint16_t node, uint8_t depth) const
{
    assert(depth > 0 && depth <= max_depth() && node < nodes.size());
    return node >= offset(depth + 1);
}

int main(int argc, char **argv)
{
    if (argc < 3)
    {
        std::cerr << "Usage: quad <root size> <leaf size> [cell size = 10.0f]\n"
                  << "E.g.: quad 256 32 5.5";
        return -1;
    }
    const long root_size = strtol(argv[1], nullptr, 0);
    const long leaf_size = strtol(argv[2], nullptr, 0);
    constexpr auto default_cell_size = 10.0f;
    float const cell_size = (argc > 3) ? strtof(argv[3], nullptr) : default_cell_size;
    const QuadTree q{static_cast<unsigned>(root_size), static_cast<unsigned>(leaf_size), cell_size};
    std::cout << "Max depth = " << +q.max_depth() << '\n'
              << "Depth Offset(3) = " << q.offset(3) << '\n'
              << "Child offset(3) = " << q.child_offset(3) << '\n'
              << "Is descendant(19, 3) = " << q.is_descendant(19, 3) << '\n';
    uint16_t offset = 0u;
    const auto count = q.descendant_leaf_offset(2u, &offset);
    std::cout << count << " descendants of 2 start at " << offset << '\n';
    for (auto i = 0u; i < q.nodes.size(); ++i)
    {
        std::cout << i << ": centre: " << glm::to_string(q.nodes[i].centre)
                  << "\t half extent: " << q.nodes[i].hextent << '\n';
    }
}
