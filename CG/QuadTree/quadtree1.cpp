// naive recursive implementation with incompatible structure
// different non-leaf and leaf nodes are good for space saving but
// one can't store the leaf in a non-leaf node; also having a common
// interface will be an overkill with the option of arrays getting
// ruled out since arrays and runtime polymorphism don't play well
#include <array>
#include <memory>
#include <glm/glm.hpp>
#include <iostream>

struct AABB
{
    std::array<glm::vec2, 2u> pt;
};

struct QuadTree
{
    AABB bounds;
    std::unique_ptr<QuadTree[]> quads;
};

struct QuadLeaf
{
    AABB bounds;
    short offset;  // used by ARB_draw_elements_base_vertex
};

template <typename FuncT, FuncT end_cond>
void subdivide(QuadTree *node, unsigned size, unsigned cut = 0)
{
    auto cells_1d = size - 1u;
    auto cells = cells_1d * cells_1d;
    auto tris = cells * 2;
    auto &bounds = node->bounds;
    if (end_cond(cut, size, tris, bounds))
    {
        std::cout << "Make quad leaves here\n";
        return;
    }
    node->quads = std::make_unique<QuadTree[]>(4u);
    auto const centre = 0.5f * (bounds.pt[1] - bounds.pt[0]);
    node->quads[0] = QuadTree{{centre, bounds.pt[1]}};
    node->quads[1] = QuadTree{{glm::vec2{bounds.pt[0].x, centre.y}, glm::vec2{centre.x, bounds.pt[1].y}}};
    node->quads[2] = QuadTree{{bounds.pt[0], centre}};
    node->quads[3] = QuadTree{{glm::vec2{centre.x, bounds.pt[0].y}, glm::vec2{bounds.pt[1].x, centre.y}}};

    ++cut;
    size /= 2u;
    for (auto i = 0u; i < 4u; ++i)
        subdivide<FuncT, end_cond>(&node->quads[i], size, cut);
}

bool max_cut(unsigned cuts, unsigned size, unsigned tris, AABB bounds)
{
    std::cout << "Cuts: " << cuts << " Size: " << size << " Triangles: " << tris << '\n';
    return cuts == 2u;
    //return tris < 10000;
}

int main()
{
    QuadTree root = {{{glm::vec2{-10.0f, -10.0f}, glm::vec2{10.0f, 10.0f}}}};
    subdivide<decltype(&max_cut), max_cut>(&root, 256u);
}
