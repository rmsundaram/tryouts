#include <iostream>
#include <bitset>
#include <cstdint>

/*
 * Maps a quad tree node index to the equivalent 2D array index pair
 *
 *    +-----+-----+-----+-----+       +------+------+------+------+
 *    |  1  |  2  |  5  |  6  |       | 0, 0 | 1, 0 | 2, 0 | 3, 0 |
 *    +-----+-----+-----+-----+       +------+------+------+------+
 *    |  3  |  4  |  7  |  8  |       | 0, 1 | 1, 1 | 2, 1 | 3, 1 |
 *    +-----+-----+-----+-----+       +------+------+------+------+
 *    |  9  | 10  | 13  | 14  |       | 0, 2 | 1, 2 | 2, 2 | 3, 2 |
 *    +-----+-----+-----+-----+       +------+------+------+------+
 *    | 11  | 12  | 15  | 16  |       | 0, 3 | 1, 3 | 2, 3 | 3, 3 |
 *    +-----+-----+-----+-----+       +------+------+------+------+
 *            depth = 3
 */

std::pair<uint16_t, uint16_t>
remap(uint16_t node, uint8_t depth)
{
    std::bitset<4> const off_x{10};  // { 0, 1, 0, 1 }
    std::bitset<4> const off_y{12};  // { 0, 0, 1, 1 }
    uint16_t x = 0u, y = 0u;
    uint16_t c = 1u;         // number of items at depth in 1D, increasing powers of 2
    while (depth-- > 1)      // depth = 1 would just be root
    {
        // rem = index within quad at this depth
        // quot = parent's index at depth - 1
        auto const d = std::div(node - 1, 4);
        x += off_x[d.rem] * c;
        y += off_y[d.rem] * c;
        // update for next iteration
        c *= 2u;
        node = d.quot + 1u;
    }
    return {x, y};
}

int main(int argc,char **argv)
{
    if (argc < 3)
    {
        std::cerr << "Input a quad index and depth";
        return -1;
    }
    uint16_t const node = strtoul(argv[1], nullptr, 0);
    uint8_t const depth = strtoul(argv[2], nullptr, 0);
    auto const p = remap(node, depth);
    std::cout << node << " at depth " << +depth << " maps to (" << p.first << ", " << p.second << ")\n";
}
