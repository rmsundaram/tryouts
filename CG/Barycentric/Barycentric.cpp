#include <iostream>
#include <type_traits>
#include <cmath>
#include <chrono>
#include <limits>
#include <random>
#include <vector>

using std::cout;
using std::endl;
using std::ostream;

// Since HRC is almost always aliased to {steady,system}_clock it’s better to
// use the former directly; avoid latter as it isn’t monotonic i.e. it can go
// back and jump around too!
// https://en.cppreference.com/w/cpp/chrono/high_resolution_clock
// https://stackoverflow.com/q/1487695/183120
// https://stackoverflow.com/q/38252022/183120
// https://gamedev.stackexchange.com/a/131094/14025
typedef std::conditional<
  std::chrono::high_resolution_clock::is_steady,
  std::chrono::high_resolution_clock,
  std::chrono::steady_clock>::type Clock;
static_assert(Clock::is_steady,
               "Clock is not monotonically-increasing (steady).");

struct Vector3f
{
    // Vector3f() = default;
    // Vector3f(float x, float y, float z) : components{x, y, z}  { }

    float components[3];
};

float length(const Vector3f &v)
{
    return sqrt(v.components[0] * v.components[0] +
                v.components[1] * v.components[1] +
                v.components[2] * v.components[2]);
}

Vector3f normalize(const Vector3f &v)
{
    const auto len = length(v);
    return Vector3f{v.components[0] / len,
                    v.components[1] / len,
                    v.components[2] / len};
}

// negation
Vector3f operator-(const Vector3f &v)
{
    return Vector3f{-v.components[0], -v.components[1], -v.components[2]};
}

// addition
Vector3f operator+(const Vector3f &v1, const Vector3f &v2)
{
    return Vector3f{v1.components[0] + v2.components[0],
                    v1.components[1] + v2.components[1],
                    v1.components[2] + v2.components[2]};
}

// subtraction
Vector3f operator-(const Vector3f &v1, const Vector3f &v2)
{
    return Vector3f{v1.components[0] - v2.components[0],
                    v1.components[1] - v2.components[1],
                    v1.components[2] - v2.components[2]};
}

// cross product
Vector3f operator*(const Vector3f &v1, const Vector3f &v2)
{
    return Vector3f{(v1.components[1] * v2.components[2]) - (v1.components[2] * v2.components[1]),
                    (v1.components[2] * v2.components[0]) - (v1.components[0] * v2.components[2]),
                    (v1.components[0] * v2.components[1]) - (v1.components[1] * v2.components[0])};
}

// scale
Vector3f operator*(float scalar, const Vector3f &vec)
{
    return Vector3f{vec.components[0] * scalar, vec.components[1] * scalar, vec.components[2] * scalar};
}

// scale (operands swapped)
Vector3f operator*(const Vector3f &vec, float scalar)
{
    return (scalar * vec);
}

float dot(const Vector3f &v1, const Vector3f &v2)
{
    return ((v1.components[0] * v2.components[0]) +
            (v1.components[1] * v2.components[1]) +
            (v1.components[2] * v2.components[2]));
}

Vector3f projection(const Vector3f &vec, const Vector3f &on)
{
    return (dot(vec, on) / dot(on, on)) * on;
}

Vector3f gram_schmidt_ortho(const Vector3f &base, const Vector3f &slanting)
{
    return slanting - projection(slanting, base);
}

struct Triangle
{
    void compute_edges_normal()
    {
        edges[0] = vertices[1] - vertices[0];
        edges[1] = vertices[2] - vertices[1];
        edges[2] = vertices[0] - vertices[2];

        // both works
        //normal = edges[0] * edges[1];
        normal = edges[1] * edges[2];
        // normalisation of the normal isn't needed since the coordinates are ratios
        //normal.normalize();
    }

    // preprocessing for 3D Math Primer method
    void compute_area()
    {
        area_squared = dot(normal, normal);
    }

    void compute_height_vectors()
    {
        /* Buss' method is basically computing height vector, m and then scaling it;
         * the result is just dotted for each test point vector, u
         *     m·u     1
         * γ = ---- = ---- m·u  (this is allowed since (a·b) / k = (a/k)·b, where a, b are vectors & k is a scalar)
         *     m·e1   m·e1
         * height_vec[0] = m(1/m·e1) is precomputed; likewise for the other side too
         */

        /* 3 ways of calculating height_vec - all are equivalent; choosing the last
         * since it has the least number of operations

            GSO = 2 dot, 1 div, 1 mul, 3 sub
            hv_0 = 1 dot, 1 div, 3 mul
            ________________________________
            (3 dot, 2 div, 4 mul, 3 sub) * 2 for two hv's
            --------------------------------
            6 dot, 4 div, 8 mul, 6 sub = 18 mul, 12 add, 4 div, 8 mul, 6 sub =
            26 mul, 4 div, 12 add, 6 sub


            3 dot = 3 * (3 mul, 2 add)
                  = 9 mul, 6 add
            denom = 2 mul, 1 sub, 1 div
            hv_0 = 3 mul, 1 sub
            hv_1 = 3 mul, 1 sub
            ___________________________
            17 mul, 1 div, 6 add, 3 sub
            ---------------------------

        // const auto &vertical_vec = gram_schmidt_ortho(edges[0], edges[1]);
        // height_vec[0] = vertical_vec * (1.0f / dot(edges[1], vertical_vec));

        // height_vec[0] = vertical_vec * (1.0f / dot(vertical_vec, vertical_vec));

        // const auto &horiz_vec = gram_schmidt_ortho(edges[2], edges[0]);
        // height_vec[1] = horiz_vec * (1.0f / dot(edges[0], horiz_vec));

        // height_vec[1] = horiz_vec * (1.0f / dot(horiz_vec, horiz_vec));*/

        const auto &e2 = edges[0];
        const auto &e1 = -edges[2];

        const auto e1_sq = dot(e1, e1);
        const auto e2_sq = dot(e2, e2);
        const auto e1_e2 = dot(e1, e2);

        const auto denom = 1.0f / ((e1_sq * e2_sq) - (e1_e2 * e1_e2));
        height_vec[0] = ((e2_sq * e1) - (e1_e2 * e2)) * denom;
        height_vec[1] = ((e1_sq * e2) - (e1_e2 * e1)) * denom;
    }

    Vector3f vertices[3];
    Vector3f edges[3];

    // required by both methods to test if the point is within the triangle
    // see is_point_on_plane
    Vector3f normal;

    // additional data for 3D Primer's Method
    float area_squared;       // area of the parallelogram (NOT the triangle) squared

    // additional data for Buss' method; space complexity's much higher than Primer's
    // 6 vs 1 float
    Vector3f height_vec[2];
};

// Barycentric coordinates are meaningful only when the point tested for lies on the plane formed
// by the triangle; scalar triple product can be used to test their coplanarity
bool is_point_on_plane(const Triangle &t, const Vector3f &p)
{
    const auto position = p - t.edges[0];
    return (std::abs(dot(t.normal, position)) <= std::numeric_limits<float>::epsilon());
}

// 3D Math Primer's method has more operations (2 vector sub, 2 dots, 2 divides and 2 subs)
// compared to Buss' method
bool Primer_Barycentric(const Triangle &t, const Vector3f &point, Vector3f &bary)
{
    if (is_point_on_plane(t, point))
    {
        auto const a = point - t.vertices[0];
        // auto const b = point - triangle.vertices[1];
        auto const c = point - t.vertices[2];
        auto const gamma = dot(t.edges[0] * a, t.normal) / t.area_squared;
        auto const beta = dot(t.edges[2] * c, t.normal) / t.area_squared;
        bary = {1.0f - beta - gamma, beta, gamma};
        return true;
    }
    else
        return false;
}

// Buss' method involves very little work (1 vector sub, 2 dots and 2 subtractions)
bool Buss_Barycentric(const Triangle &t, const Vector3f &point, Vector3f &bary)
{
    if (is_point_on_plane(t, point))
    {
        const auto point_vec = point - t.vertices[0];
        const auto gamma = dot(point_vec, t.height_vec[0]);
        const auto beta = dot(point_vec, t.height_vec[1]);
        bary = {1.0f - beta - gamma, beta, gamma};
        return true;
    }
    else
        return false;
}

ostream& operator<<(ostream &os, const Vector3f &v)
{
    return os << v.components[0] << ", " << v.components[1] << ", " << v.components[2];
}

int main(int argc, char **argv)
{
    Triangle t ={   // vertices in counter-clockwise direction
                    {
                        { 0.0f, 0.0f, 0.0f},
                        { 100.0f, 0.0f, 0.0f},
                        { 0.0f, 100.0f, 0.0f}
                    }
                };
    t.compute_edges_normal();              // common & required for both methods

    const auto t_start_primer_preprocess = Clock::now();
    t.compute_area();        // required for 3D Primer method
    const auto t_end_primer_preprocess = Clock::now();
    t.compute_height_vectors();     // required for Buss' method
    const auto t_end_buss_preprocess = Clock::now();

    cout << "Time preprocessing data - 3D Primer method = "
         << (t_end_primer_preprocess - t_start_primer_preprocess).count() << endl;
    cout << "Time preprocessing data - Buss' method = "
         << (t_end_buss_preprocess - t_end_primer_preprocess).count() << "\n\n";

    // sanity test
    const Vector3f point{50.0f, 20.0f, 0.0f};
    Vector3f bary1, bary2;
    Primer_Barycentric(t, point, bary1);
    Buss_Barycentric(t, point, bary2);
    cout << bary1 << '\n';
    cout << bary2 << "\n\n";

    std::random_device rd;
    std::mt19937 gen{rd()};
    std::uniform_real_distribution<float> dis(0, 100);
    constexpr size_t trials = 100000;
    std::vector<Vector3f> pts;
    pts.reserve(trials);
    for (auto i = 0u; i < trials; ++i)
    {
        pts.push_back({dis(gen), dis(gen), 0.0f});
    }

    std::vector<Vector3f> barys1(pts.size()), barys2(pts.size());
    const auto t_start_primer = Clock::now();
    for (auto i = 0u; i < pts.size(); ++i)
        Primer_Barycentric(t, pts[i], barys1[i]);
    const auto t_end_primer = Clock::now();
    for (auto i = 0u; i < pts.size(); ++i)
        Buss_Barycentric(t, pts[i], barys2[i]);
    const auto t_end_buss = Clock::now();

    cout << "Time diff for 3D Primer method = " << (t_end_primer - t_start_primer).count() << endl;
    cout << "Time diff for Buss' method = " << (t_end_buss - t_end_primer).count() << endl;
}
