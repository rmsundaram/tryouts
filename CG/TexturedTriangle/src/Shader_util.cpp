#include "common.hpp"
#include "Shader_util.hpp"

namespace
{

template <typename Query_func, typename Info_func>
void check(GLuint obj_id,
           GLenum param,
           Query_func query,
           Info_func info)
{
    GLint value;
    query(obj_id, param, &value);
    if(GL_FALSE == value)
    {
        GLint log_length;
        query(obj_id, GL_INFO_LOG_LENGTH, &log_length);
        const std::unique_ptr<GLchar[]> err_log(new GLchar[log_length]);
        info(obj_id, log_length, nullptr, err_log.get());
        throw std::runtime_error(err_log.get());
    }
}

}

GLuint ShaderUtil::compile_shader(GLenum shader_type, const std::string &shader_file_path)
{
    const std::ifstream shader_file{shader_file_path};
    std::ostringstream shader_data;
    shader_data << shader_file.rdbuf();

    const std::string &str_shader_code = shader_data.str();
    const GLchar *shader_code = static_cast<const GLchar*>(str_shader_code.c_str());
    const GLint shader_data_len = str_shader_code.size();
    if(shader_data_len <= 0)
    {
        throw std::invalid_argument(shader_file_path);
    }

    const GLuint shader_id = glCreateShader(shader_type);
    if(0 == shader_id)
    {
        throw std::runtime_error("Failed to create new shader");
    }
    glShaderSource(shader_id, 1, &shader_code, &shader_data_len);
    glCompileShader(shader_id);

    check(shader_id, GL_COMPILE_STATUS, glGetShaderiv, glGetShaderInfoLog);

    return shader_id;
}

// although in the interface prog_id isn't const, making it const in the implementation
// prevents accidental overwrites
void ShaderUtil::link_program(const GLuint prog_id, const std::vector<GLuint> &shaders)
{
    for(auto shader_id : shaders)
    {
        glAttachShader(prog_id, shader_id);
    }

    /*
     * Linking the shaders give the driver the opportunity to trim down the shaders and optimize them according to
     * their relationships. For example, you may pair a vertex shader that emits a normal with a fragment shader that
     * ignores it. In that case the GLSL compiler in the driver can remove the normal related functionality of the
     * shader and enable faster execution of the vertex shader. If that shader is later paired with a fragment shader
     * that use the normal then linking the other program will generate a different vertex shader.
     * ~ ogldev.atspace.co.uk/www/tutorial04/tutorial04.html
     */
    glLinkProgram(prog_id);
    check(prog_id, GL_LINK_STATUS, glGetProgramiv, glGetProgramInfoLog);

    for(GLuint shader_id : shaders)
    {
        glDetachShader(prog_id, shader_id);
    }
}

void ShaderUtil::validate_program(const GLuint prog_id)
{
#ifndef NDEBUG
    /*
     * Why do we need to validate a program after it has been successfully linked? Linking checks for errors based on
     * the combination of shaders while validation checks whether the program can be executed given the current pipeline
     * state. In a complex application with multiple shaders and lots of state changes it is better to validate
     * before every draw call. Also, you may want to do this check only during development and avoid this overhead in
     * the final product.
     * ~ ogldev.atspace.co.uk/www/tutorial04/tutorial04.html
     *
     * A valid VAO needs to be bound as it validates if this program is valid in the current OpenGL state.
     */
    glValidateProgram(prog_id);
    check(prog_id, GL_VALIDATE_STATUS, glGetProgramiv, glGetProgramInfoLog);
#endif
}

GLuint ShaderUtil::setup_program(const std::string &vert_shader_file, const std::string &frag_shader_file)
{
    const GLuint prog_id = glCreateProgram();
    if(0 == prog_id)
    {
        throw std::runtime_error("Failed to create new program");
    }

    const std::vector<GLuint> shader_ids
    {
        compile_shader(GL_VERTEX_SHADER, vert_shader_file),
        compile_shader(GL_FRAGMENT_SHADER, frag_shader_file)
    };
    link_program(prog_id, shader_ids);
    std::for_each(shader_ids.cbegin(), shader_ids.cend(), glDeleteShader);

    return prog_id;
}
