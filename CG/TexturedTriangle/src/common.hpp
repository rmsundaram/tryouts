#ifndef __PRECOMP_H__
#define __PRECOMP_H__

#include "gl_3_3.h"
#include <GLFW/glfw3.h>
#include <gli/gli.hpp>

#include <memory>
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <utility>
#include <functional>
#include <cassert>
#include <numeric>

#endif  // __PRECOMP_H__
