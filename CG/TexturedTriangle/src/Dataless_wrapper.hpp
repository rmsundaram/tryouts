#ifndef __DATALESS_WRAPPER_HPP__
#define __DATALESS_WRAPPER_HPP__

template <typename T>
struct Return_type;

template <typename T_Return, typename... Args>
struct Return_type<T_Return(*) (Args...)>
{
    typedef T_Return type;
};

// this is for function pairs like glfwInit & glfwTerminate where the
// constructor takes arguments but the destructor takes no arguments; if the
// uninit function expects arguments i.e. has a resource to be released, then
// Dataless_wrap isn't the right wrapper; should use std::unique_ptr with custom
// deleter or a home-made RAII_wrapper

template <typename T_uninit_func,
          T_uninit_func uninit_func>
struct Dataless_wrap
{
    Dataless_wrap() : initialized{false} { }

    template <typename T_init_func, typename... Args>
    Dataless_wrap(T_init_func init_func, Args&&... args)
    {
        init_func(std::forward<Args>(args)...);
        initialized = true;
    }

    Dataless_wrap(Dataless_wrap&& that) : initialized{that.initialized}
    {
        that.initialized = false;
    }

    Dataless_wrap& operator=(Dataless_wrap&& that)
    {
        initialized = that.initialized;
        that.initialized = false;
        return *this;
    }

    ~Dataless_wrap()
    {
        if(initialized)
        {
            uninit_func();
        }
    }

    // non-copyable
    Dataless_wrap& operator=(const Dataless_wrap& that) = delete;
    Dataless_wrap(const Dataless_wrap&) = delete;

private:
    bool initialized;
};

template <typename T_init_func,
          T_init_func init_func,
          typename T_success,
          T_success success_val>
struct Check_return
{
    template <typename... Args>
    void operator()(Args&&... args)
    {
        if (success_val != init_func(std::forward<Args>(args)...))
        {
            throw std::runtime_error("Initialization failed");
        }
    }
};

template <typename T_init_func,
          T_init_func init_func>
struct Ignore_return
{
    Ignore_return()
    {
        static_assert(std::is_void<typename Return_type<T_init_func>::type>::value,
                      "Return value of a non-void function ignored");
    }
    template <typename... Args>
    void operator()(Args&&... args)
    {
        init_func(std::forward<Args>(args)...);
    }
};

#endif // __DATALESS_WRAPPER_HPP__
