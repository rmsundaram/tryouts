#ifndef __MAIN_HPP__
#define __MAIN_HPP__

inline void window_error(int, const char *error_msg)
{
    throw std::runtime_error(error_msg);
}

inline void check(bool condition, const char *error_msg)
{
    if(condition != true)
    {
        throw std::runtime_error(error_msg);
    }
}

namespace array
{

template <typename T>
inline constexpr typename std::enable_if<std::is_array<T>::value, size_t>::type
max_index(const T& /*arr*/)
{
    return std::extent<T>::value - 1;
}

inline
size_t item_index(size_t item_id,
                  size_t base_id,
                  size_t max_id,
                  size_t err_index)
{
    const auto max_index = max_id - base_id;
    int index = item_id - base_id;
    return ((index < 0) || (static_cast<unsigned>(index) > max_index)) ?
            err_index :
            static_cast<size_t>(index);
}

}

inline std::ostream& tab(std::ostream &os)
{
    return os << '\t';
}

template <typename T>
typename std::enable_if<std::is_integral<T>::value, bool>::type
is_power_of_2(T v)
{
    return (v && !(v & (v - 1)));
}

namespace GL
{

template <typename T>
constexpr GLenum type_id(T);

template <>
constexpr GLenum type_id(GLfloat)
{
    return GL_FLOAT;
}

template <>
constexpr GLenum type_id(GLubyte)
{
    return GL_UNSIGNED_BYTE;
}

}

#endif  // __MAIN_HPP__
