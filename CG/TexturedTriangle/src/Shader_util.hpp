#ifndef __SHADER_UTIL_HPP__
#define __SHADER_UTIL_HPP__

namespace ShaderUtil
{
    /**
     * @brief Takes shader code file and type and returns a compiled shader handle.
     */
    GLuint compile_shader(GLenum shader_type, const std::string &shader_file_path);

    /**
     * @brief Takes shader and links them into the given program handle
     */
    void link_program(GLuint prog_id, const std::vector<GLuint> &shaders);

    /**
     * @brief Validates a program in the current OpenGL state; expects a VAO to be bound.
     */
    void validate_program(const GLuint prog_id);

    /**
     * @brief Takes shader file paths, compiles and links them in to a program; returns program handle.
     */
    GLuint setup_program(const std::string &vert_shader_file, const std::string &frag_shader_file);
}

#endif  //  __SHADER_UTIL_HPP__
