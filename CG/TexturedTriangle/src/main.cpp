/*
 * Techniques learnt:
 *      Texturing (and Mipmaps)
 *      VBO Indexing
 *      Interleaving Attributes
 *      Gamma-correct, linear rendering pipeline
 */

#include "common.hpp"
#include "Dataless_wrapper.hpp"
#include "Shader_util.hpp"
#include "main.hpp"

using GLFW_wrapper = Dataless_wrap<decltype(&glfwTerminate), glfwTerminate>;

const unsigned window_width = 800u, window_height = 600u;

GLFWwindow* setup_context(uint32_t width, uint32_t height)
{
    /*
     * without setting the version (1.0) or setting it < 3.2, requesting for core will fail
     * since context profiles only exist for OpenGL 3.2+; likewise forward compatibility only
     * exist from 3.0 onwards. 3.0 marked deprecated, 3.1 removed deprecated (except wide
     * lines), 3.2 reintroduced deprecated under compatibility profile
     */
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE,
                   GLFW_OPENGL_CORE_PROFILE);
    // since core only has undeprecated features, making the context forward-compatible is moot
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
    glfwWindowHint(GLFW_SRGB_CAPABLE, GL_TRUE);

#ifndef NDEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

    auto window = glfwCreateWindow(width, height, "Textured Triangle", nullptr, nullptr);
    check(window != nullptr, "Failed to create window");
    glfwMakeContextCurrent(window);
    return window;
}

#ifndef NDEBUG

APIENTRY
void gl_debug_logger(GLenum source,
                     GLenum type,
                     GLuint id,
                     GLenum severity,
                     GLsizei /*length*/,
                     const GLchar *msg,
                     const void *file_ptr)
{
    std::stringstream ss;
    ss << "GLError 0x"
       << std::hex << id << std::dec
       << ": "
       << msg <<
       " [source=";

    const char *sources[] = {
                                "API",
                                "WINDOW_SYSTEM",
                                "SHADER_COMPILER",
                                "THIRD_PARTY",
                                "APPLICATION",
                                "OTHER",
                                "UNDEFINED"
                            };
    size_t index = array::item_index(source,
                                     GL_DEBUG_SOURCE_API_ARB,
                                     GL_DEBUG_SOURCE_OTHER_ARB,
                                     array::max_index(sources));
    ss << sources[index];
    if (array::max_index(sources) == index)
    {
        ss << " (" << source << ")";
    }

    const char *types[] = {
                              "ERROR",
                              "DEPRECATED_BEHAVIOR",
                              "UNDEFINED_BEHAVIOR",
                              "PORTABILITY",
                              "PERFORMANCE",
                              "OTHER",
                              "UNDEFINED"
                          };
    index = array::item_index(type,
                              GL_DEBUG_TYPE_ERROR_ARB,
                              GL_DEBUG_TYPE_OTHER_ARB,
                              array::max_index(types));
    ss << " type=" << types[index];
    if (array::max_index(types) == index)
    {
        ss << " (" << type << ")";
    }

    const char *severities[] = {
                                   "HIGH",
                                   "MEDIUM",
                                   "LOW",
                                   "UNKNOWN"
                               };
    // as per the extension, HIGH is the base (smaller) value
    index = array::item_index(severity,
                              GL_DEBUG_SEVERITY_HIGH_ARB,
                              GL_DEBUG_SEVERITY_LOW_ARB,
                              array::max_index(severities));
    ss << " severity=" << severities[index];
    if (array::max_index(severities) == index)
    {
        ss << " (" << severity << ")";
    }
    ss << "]";

    const auto log = ss.str();
    FILE *out_file = reinterpret_cast<FILE*>(const_cast<void*>(file_ptr));
    fprintf(out_file, "%s\n", log.c_str());

    check(severity != GL_DEBUG_SEVERITY_HIGH_ARB, "High severity GL error logged");
}

void setup_debug(bool enable)
{
    if (ogl_ext_ARB_debug_output == ogl_LOAD_FAILED) return;

    glDebugMessageCallbackARB(enable ? gl_debug_logger : nullptr, stderr);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
    check(GL_NO_ERROR == glGetError(), "Unable to set synchronous debug output");
}

void print_framebuffer_properties() {
  // Skip glBindFrameBuffer; defaults to window-system-provided framebuffer.
  // https://www.khronos.org/opengl/wiki/Default_Framebuffer
  GLint ret;
  glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER,
                                        GL_FRONT_LEFT,
                                        GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE,
                                        &ret);
  assert(ret == GL_FRAMEBUFFER_DEFAULT);
  glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER,
                                        GL_FRONT_LEFT,
                                        GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING,
                                        &ret);
  const char* encoding = (ret == GL_LINEAR) ? "Linear" :
    (ret == GL_SRGB) ? "sRGB" : "Unknown";
  std::cerr << "Framebuffer Colour Encoding: 0x"
            << std::hex << ret << std::dec
            << " (" << encoding << ")\n";
  GLint size_red, size_green, size_blue, size_depth, size_alpha;
  glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER,
                                        GL_FRONT_LEFT,
                                        GL_FRAMEBUFFER_ATTACHMENT_RED_SIZE,
                                        &size_red);
  glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER,
                                        GL_FRONT_LEFT,
                                        GL_FRAMEBUFFER_ATTACHMENT_GREEN_SIZE,
                                        &size_green);
  glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER,
                                        GL_FRONT_LEFT,
                                        GL_FRAMEBUFFER_ATTACHMENT_BLUE_SIZE,
                                        &size_blue);
  glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER,
                                        GL_FRONT_LEFT,
                                        GL_FRAMEBUFFER_ATTACHMENT_ALPHA_SIZE,
                                        &size_alpha);
  glGetFramebufferAttachmentParameteriv(GL_DRAW_FRAMEBUFFER,
                                        GL_FRONT_LEFT,
                                        GL_FRAMEBUFFER_ATTACHMENT_DEPTH_SIZE,
                                        &size_depth);
  std::cerr << "Framebuffer Format: RGBA" << size_red
            << size_green
            << size_blue
            << size_alpha
            << ", Depth: " << size_depth << " bits\n";
}
#endif

void handle_key(GLFWwindow *window,
                int key,
                int /*scancode*/,
                int action,
                int /*mods*/)
{
    if((GLFW_KEY_ESCAPE == key) && (GLFW_PRESS == action))
    {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
}

template <typename T, size_t N>
GLuint upload_data(const T (&data) [N], GLenum target)
{
    /*
     *  VBOs are “buffers” of video memory – just a bunch of bytes containing any kind of binary data you want.
     *  You can upload 3D points, colors, your music collection, poems to your loved ones – the VBO doesn’t care,
     *  because it just copies a chunk of memory without asking what the memory contains.
     */
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(target, vbo);
    glBufferData(target, sizeof(data), &data, GL_STATIC_DRAW);
    glBindBuffer(target, 0);

    return vbo;
}

// Simple pixel transform function that ‘unpacks’ data i.e. moves data from main memory to video memory as pixel data in
// user memory is said to be packed. Therefore, transfers to OpenGL memory are called unpack operations, and transfers
// from OpenGL memory are called pack operations.  See [1] for different OpenGL functions of this kind.
// [1]: https://www.khronos.org/opengl/wiki/Pixel_Transfer
GLuint upload_texture(const char *file_name, GLenum texture_unit)
{
    gli::texture tex = gli::load(file_name);
    assert(!tex.empty());

    gli::gl gl_profile{gli::gl::PROFILE_GL33};
    gli::gl::format const format = gl_profile.translate(tex.format(), tex.swizzles());

    GLuint texture;
    glGenTextures(1, &texture);
    /*
     * texture objects are similar to buffer objects; we generate them, bind them to some target and whatever
     * operation is done on the target affects the bound object; however to support muti-texturing there's an
     * array of targets that can be used E.g. more than one GL_TEXTURE_2D targets are required to support
     * texture splatting (with an alpha map) since we need to operate with two 2D texture objects at the same
     * time, so as to support this, one more level of indirection was introduced, the active texture unit.
     * http://stackoverflow.com/a/5191575/183120
     * http://stackoverflow.com/q/8866904/183120
     */
    glActiveTexture(texture_unit);
    glBindTexture(GL_TEXTURE_2D, texture);

    /*
     * Refer CG/Terrain/Texturing/src/main.cpp for details on filtering and addressing modes.
     * Refer CG/Noise/white_noise.html for details on pixel and texel centre which is at (0.5, 0.5) since OpenGL uses
     * area-oriented, instead of sample-oriented raster data addressing model.
     * http://hacksoflife.blogspot.com/2009/12/texture-coordinate-system-for-opengl.html
     *
     * http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=9 says that the below method of
     * associating sampling parameters with the texture using glTexParameter is just a vestige from OpenGL 2.1.
     * If we wanted to use the same texture with different filtering modes, we need to constantly be changing
     * its parameters. OpenGL 3.3 introduces sampler objects to decouple the sampling data from texture objects.
     * These sampler objects are not to be confused with GLSL's uniform sampler variables used for getting the
     * texel value at a particular texture coordinate in a texture. Sampling is the process of fetching a value
     * from a texture at a given position, so sampler is an object where we store info of how to do it. If we
     * want to change filtering, we just bind different samplers with different propertiees, and we're done.
     * From the spec: "If a sampler object is bound to a texture unit and that unit is used to sample from a
     * texture, the parameters in the sampler are used to sample from the texture, rather than the equivalent
     * parameters in the texture object bound to that unit". If a sampler is bound to the texture, its
     * parameters supersedes texture parameters. The image in OGLDev's tutorial pages illustrates this too.
     * http://ogldev.atspace.co.uk/www/tutorial16/tutorial16.html
     *
     * Without setting GL_TEXTURE_MIN_FILTER to GL_LINEAR the plane was rendered black; the default
     * GL_TEXTURE_MIN_FILTER​ state is GL_NEAREST_MIPMAP_LINEAR​. And because OpenGL defines the default
     * GL_TEXTURE_MAX_LEVEL​ to be 1000, OpenGL will expect there to be mipmap levels defined. Since only a few
     * are defined, OpenGL will consider the texture incomplete until the GL_TEXTURE_MAX_LEVEL​ is properly set,
     * or the GL_TEXTURE_MIN_FILTER​ parameter is set to not use mipmaps e.g. GL_LINEAR. Better code would be to
     * use texture storage functions (on OpenGL 4.2+ or ARB_texture_storage) to allocate the texture's storage,
     * then upload with glTexSubImage2D​. This issue applies only to the minification filter, the magnification
     * filter don't have mipmaps.
     * http://opengl.org/wiki/Common_Mistakes
     */
    const auto max_levels = tex.levels() - 1;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);       // initial value is 0, hence redundant
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, max_levels);

    // since multiple (mipmap) levels are loaded for the texture, setting it to GL_LINEAR isn't correct as
    // mipmaps will not be used
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    // DDS files under ../data were converted using Compressonator from images (PNG/JPEG/BMPs) which had sRGB compressed
    // data though not marked thus (e.g. PNG not having the sRGB chunk) so gli::is_srgb(tex.format()) returns false and
    // format.Internal is the non-sRGB equivalent. Force to sRGB equivalents as we know residing data is sRGB-encoded.
    // Prefer ASTC-encoded KTX (contained) textures instead of DXT, BCn, etc.
    //   https://developer.nvidia.com/astc-texture-compression-for-game-assets
    //   https://doc.babylonjs.com/resources/multi-platform_compressed_textures
    const std::map<int, int> srgb_equivalent =
    {
        {GL_COMPRESSED_RGB_S3TC_DXT1_EXT, GL_COMPRESSED_SRGB_S3TC_DXT1_EXT},
        {GL_COMPRESSED_RGBA_S3TC_DXT1_EXT, GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT1_EXT},
        {GL_COMPRESSED_RGBA_S3TC_DXT3_EXT, GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT3_EXT},
        {GL_COMPRESSED_RGBA_S3TC_DXT5_EXT, GL_COMPRESSED_SRGB_ALPHA_S3TC_DXT5_EXT}
    };
    int internal_format = format.Internal;
    auto const it = srgb_equivalent.find(internal_format);
    if (it != srgb_equivalent.cend()) {
      internal_format = it->second;
    }

    for(auto i = 0u; i <= max_levels; ++i)
    {
        auto dim = tex.extent(i);
        // the pixel width and height of a texture should be a power of 2
        assert(is_power_of_2(dim.x) && is_power_of_2(dim.y));
        // https://www.khronos.org/opengl/wiki/S3_Texture_Compression
        // https://www.khronos.org/registry/OpenGL/extensions/EXT/EXT_texture_sRGB.txt
        // https://www.g-truc.net/post-0263.html
        glCompressedTexImage2D(GL_TEXTURE_2D,
                               i,
                               internal_format,
                               dim.x,
                               dim.y,
                               0,
                               static_cast<GLsizei>(tex.size(i)),
                               tex.data(0 /*layer*/, 0 /*face*/, i));
    }
    // Existing libraries generate mip-maps on the CPU, which is an horrendously slow process. In the past, to avoid
    // this slow process, games used DDS (Microsoft Direct Draw Surface) images, which had mip-maps pre-built in them.
    // In modern graphics we can use the GPU (glGenerateMipmap) to do this - which is extremely efficient.
    // http://antongerdelan.net/opengl/texturemaps.html

    glBindTexture(GL_TEXTURE_2D, 0);
    return texture;
}

template <typename T, size_t attribs>
GLuint setup_attributes(GLuint vertex_buffer,
                        const size_t (&attrib_length) [attribs],
                        GLuint index_buffer)
{
    /*
     * The second step to rendering our triangle is to send the points from the VBO into the shaders. Remember
     * how the VBOs are just chunks of data, and have no idea what type of data they contain? Somehow you have to tell
     * OpenGL what type of data is in the buffer, and this is what VAOs are for. VAOs are the link between the VBOs and
     * the shader variables. VAOs describe what type of data is contained within a VBO, and which shader variables the
     * data should be sent to. glGetAttribLocation or glBindAttribLocation is used to get/set the attribute (shader)
     * variable index. Alternatively, layout location can be used to set the index of a named attribute variable.
     *
     * If you have used VBOs without VAOs in older versions of OpenGL, then you might not agree with this description of
     * VAOs. You could argue that “vertex attributes” set by glVertexAttribPointer are the link between the VBO and that
     * shaders, not VAOs. It depends on whether you consider the vertex attributes to be “inside” the VAO (which I do),
     * or whether they are global state that is external to the VAO. Using the 3.2 core profile and ATI drivers, the VAO
     * is not optional – glEnableVertexAttribArray, glVertexAttribPointer and glDrawArrays all cause INVALID_OPERATION
     * error, if there is no VAO bound. This is what leads me to believe that the vertex attributes are inside the VAO,
     * and not global state. The 3.2 core profile spec says that VAOs are required, but it's told that only ATI drivers
     * throw errors if no VAO is bound. Here are some quotes from the OpenGL 3.2 core profile specification:
     *
     *            All state related to the definition of data used by the vertex processor is encapsulated
     *            in a vertex array object.
     *
     *            The currently bound vertex array object is used for all commands which modify vertex
     *            array state, such as VertexAttribPointer and EnableVertexAttribArray; all commands which
     *            draw from vertex arrays, such as DrawArrays and DrawElements; and all queries of vertex
     *            array state (see chapter 6).
     *
     * glVertexAttribPointer predates VAOs, so there was a time when vertex attributes were just global state. You could
     * see VAOs as just a way to efficiently change that global state. I prefer to think of it like this: if you don’t
     * create a VAO, then OpenGL provides a default global VAO. So when you use glVertexAttribPointer you are still
     * modifying the vertex attributes inside a VAO, it’s just that you’re modifying the default VAO instead of one you
     * created yourself.
     */

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    /*
     * if a valid buffer is bound to GL_ARRAY_BUFFER, when a VAO is binded this gets saved in it, while the
     * same isn't true for GL_ELEMENT_ARRAY_BUFFER (although the spec says it should); hence bind any buffer
     * only after binding a VAO for them to be captured in it
     */

    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    const auto vertex_size = std::accumulate(attrib_length, attrib_length + attribs, 0u) * sizeof(T);
    size_t offset = 0u;
    for(auto i = 0u; i < attribs; ++i)
    {
        glEnableVertexAttribArray(i);
        glVertexAttribPointer(i,
                              attrib_length[i],
                              GL::type_id(T{}),
                              GL_FALSE,
                              vertex_size,
                              reinterpret_cast<GLvoid*>(offset * sizeof(T)));
        offset += attrib_length[i];
    }
    /*glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), reinterpret_cast<GLvoid*>(3 * sizeof(float)));*/

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);

    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return vao;
}

void render(GLuint vao,
            GLuint texture,
            GLint  tex_unit,
            GLenum indices_type,
            GLuint program)
{
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(program);
    glBindVertexArray(vao);
    ShaderUtil::validate_program(program);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(tex_unit, 0);
    glEnable(GL_FRAMEBUFFER_SRGB);

    glDrawElements(GL_TRIANGLES, 6, indices_type, 0);

    glDisable(GL_FRAMEBUFFER_SRGB);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(0);
    glUseProgram(0);
}

void init_rendering()
{
    glEnable(GL_CULL_FACE);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
}

int main(int /*argc*/, char** /*argv*/)
{
    glfwSetErrorCallback(window_error);
    const auto glfw = std::move(GLFW_wrapper{Check_return<decltype(&glfwInit),
                                                          glfwInit,
                                                          decltype(GL_TRUE),
                                                          GL_TRUE>{}}
                               );
    // RAII warpping isn't mandatory here, since glfwTerminate destroys open windows, if any remain
    std::unique_ptr<GLFWwindow,
                    decltype(&glfwDestroyWindow)> main_wnd_ptr{setup_context(window_width, window_height),
                                                               glfwDestroyWindow};

    // http://www.parashift.com/c++-faq-lite/memfnptr-vs-fnptr.html
    // according to C++ FAQ, you cannot pass a member function to a C callback; passing a functor, lambda, boost::bind,
    // etc. is for a C++ callback implemented with templates; instead use the void* user_data allowed in the callback;
    // GLFW gives GLFWwindow->SetWindowUserPointer for the same
    glfwSetKeyCallback(main_wnd_ptr.get(), handle_key);

    check((ogl_LoadFunctions() == ogl_LOAD_SUCCEEDED), "Failed loading OpenGL symbols");

#ifndef NDEBUG
    setup_debug(true);
    print_framebuffer_properties();
#endif

    // interleave vertex attributes - perhaps more performant
    // http://stackoverflow.com/questions/14874914/performance-gain-using-interleaved-attribute-arrays-in-opengl4-0
    const GLfloat vertices[] = {
    /*
     * OpenGL spec calls the texture coordinates as s, t, r, q but in GLSL since r is already taken by red
     * s, t, p, q are the members of vec4 type; the common u, v, w is avoided since it conflicts with the
     * homogeneous w in x, y, z, w. Texture coordinates are normalized [0, 1]; effectively it really is a
     * ratio or a weighted average for linear, bilinear or trilinear interpolation; it decouples the actual
     * image's dimensions from affecting the texture coordinates every time; say (1, 0.5) in a 128 x 128
     * texture would be the texel (128, 64) while in a 2 x 2 image would be (2, 1)
     */
                                     // x,    y,    z,      s,    t
                                     0.0f, 0.8f, 0.0f,   0.5f, 1.0f,
                                    -0.8f,-0.8f, 0.0f,   0.0f, 0.0f,
                                     0.8f,-0.8f, 0.0f,   1.0f, 0.0f,
                               };
    const size_t attrib_lengths[] = { 3u, 2u };
    const GLubyte indices[] = {
                                   0, 1, 2,
                                   2, 3, 0
                              };

    auto vbo = upload_data(vertices, GL_ARRAY_BUFFER);
    auto ibo = upload_data(indices, GL_ELEMENT_ARRAY_BUFFER);
    auto texture = upload_texture("data/RGB.dds", GL_TEXTURE0);
    auto vao = setup_attributes<GLfloat>(vbo, attrib_lengths, ibo);
    auto program = ShaderUtil::setup_program("shaders/pt_vs.glsl",
                                             "shaders/tex_fs.glsl");
    auto tex_unit = glGetUniformLocation(program, "tex_unit");
    assert(tex_unit != -1);

    init_rendering();
    while(!glfwWindowShouldClose(main_wnd_ptr.get()))
    {
        render(vao,
               texture,
               tex_unit,
               GL::type_id(std::remove_extent<decltype(indices)>::type{}),
               program);

        glfwSwapBuffers(main_wnd_ptr.get());
        glfwPollEvents();
    }

    glDeleteProgram(program);
    glDeleteVertexArrays(1, &vao);
    glDeleteTextures(1, &texture);
    glDeleteBuffers(1, &ibo);
    glDeleteBuffers(1, &vbo);
}
