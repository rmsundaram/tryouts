set_languages("c++14")
add_cxflags("-Wall",
            "-pedantic",
            "-Wextra",
            "-Wno-missing-field-initializers",
            "-Wcast-align",
            "-Wconversion",
            "-Wcast-qual",
            "-Wdouble-promotion",
            "-Wvector-operation-performance",
            "-Wno-div-by-zero",
            "-Wlogical-op")

-- add modes: debug and release
add_rules("mode.debug", "mode.release")
add_requires("glm >=0.5.0", "glfw3 >=3.0", "gli >=0.8.2")

-- Used glLoadGen for Core 3.3 with some extensions
-- glLoadGen 2.0.5 used to generate src/gl_3_3.{h,c}
-- lua5.1 LoadGen.lua 3_3 -style=pointer_c -spec=gl -version=3.3 -profile=core -indent=space -lineends=unix -geninfo=cmdline -exts ARB_debug_output EXT_texture_compression_s3tc EXT_texture_sRGB

-- add target
target("TexTriangle")
    -- set kind
    set_kind("binary")
    set_targetdir("$(projectdir)")

    -- add files
    add_files("src/*.cpp", "src/gl_3_3.c")

    -- add libraries
    add_packages("glm", {links={}},
                 "gli", {links={}},
                 "glfw3")

    if is_plat("macosx") then
      add_frameworks("OpenGL")
    elseif is_plat("linux") then
      add_links("GL")
    end

    -- https://xmake.io/#/manual/custom_rule
    add_rules("mode.debug", "mode.release")
    if is_mode("release") then
      add_defines("NDEBUG")
    elseif is_mode("debug") then
      add_defines("DEBUG", "_DEBUG")
      -- on Windows, QtCreator + GDB works better with Dwarf 3 debug data
      if is_plat("windows") and compiler.has_features("gdwarf-3") then
        add_cxflags("-gdwarf-3")
      end
    end
