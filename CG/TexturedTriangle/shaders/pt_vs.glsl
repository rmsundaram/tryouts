#version 330

layout (location = 0) in vec3 position;
// Interpolation qualifiers like flat, smooth and noperspective can also be
// specified; see link for difference in outputs.  Default: smooth (perspective
// correct).  noperspective is unsupported by OpenGL ES and thereby WebGL.
// http://www.geeks3d.com/20130514/opengl-interpolation-qualifiers-glsl-tutorial
layout (location = 1) in vec2 vert_tex_coord;

out vec2 frag_tex_coord;

void main()
{
    gl_Position = vec4(position, 1.0f);
    frag_tex_coord = vert_tex_coord;
}
