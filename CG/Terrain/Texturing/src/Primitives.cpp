#include "common.hpp"
#include "Util.hpp"
#include "GL_res_wrap.hpp"
#include "GL_util.hpp"
#include "Primitives.hpp"
#include "Light.hpp"
#include "Camera.hpp"
#include "main.hpp"

constexpr int Primitives::Mesh::type_id[4];
constexpr unsigned Primitives::Mesh::elem_lens[4];

namespace
{

std::vector<GLushort> generate_indices(size_t cols)
{
    assert(cols > 1u);

    std::vector<GLushort> indices;
    indices.reserve(cols * 2);
    for (auto c = 0u; c < cols; ++c)
    {
        indices.push_back(static_cast<GLshort>(c));
        indices.push_back(static_cast<GLshort>(c + cols));
    }
    return indices;
}

// custom implementation of the vertex normal averaging technique
// explained in §6.2.1 Introduction to 3D Game Programming with DirectX 10
void calculate_normals(Primitives::HeightMap *height_map,
                       std::vector<Primitives::PNT> &vertices)
{
    const size_t vrtx_cnt = height_map->rows * height_map->cols;

    int r = 0, c = -1;
    /*
     * Compute normal of triangle(s) formed by a given vertex
     * and add it to the normal of the constituent vertices.
     *         0  1  2  3
     *         +--+--+--+
     *         | /| /| /|
     *         +--+--+--+
     *         4  5  6  7
     * Triangles are formed from 4 onwards. Vertices 5 and 6 would
     * form 2 triangles while 4 and 7 complete only one.
     */
    // skip the first row since those points don't form any triangles
    for (auto t = height_map->cols; t < vrtx_cnt; ++t)
    {
        c = (c + 1) % height_map->cols;     // horizontal x-axis
        r = t / height_map->cols;           // vertical y-axis
        auto &me = vertices[t];
        const auto north_idx = height_map->cols * (r - 1) + c;
        auto &north = vertices[north_idx];

        // points 4 to 6 form such triangles
        if (c != static_cast<signed>(height_map->cols) - 1)
        {
            auto &north_east = vertices[north_idx + 1];
            const auto n1 = glm::triangleNormal(me.pos, north_east.pos, north.pos);
            me.normal += n1;
            north_east.normal += n1;
            north.normal += n1;
        }

        // points 5 to 7 form such triangles; note this shouldn't be an else if
        if (c != 0)
        {
            auto &west = vertices[t - 1];
            const auto n2 = glm::triangleNormal(me.pos, north.pos, west.pos);
            me.normal += n2;
            north.normal += n2;
            west.normal += n2;
        }
    }
}

void make_terrain_mesh(Primitives::HeightMap *height_map,
                       std::vector<Primitives::Mesh> *meshes,
                       std::vector<int32_t> &&textures,
                       glm::vec2 tex_tile)
{
    const size_t vrtx_count = height_map->rows * height_map->cols;
    std::vector<Primitives::PNT> vertices;
    vertices.reserve(vrtx_count);

    const auto offset_x = 0.5f * ((static_cast<float>(height_map->cols) - 1.0f) * height_map->cell_size.x);
    const auto offset_z = 0.5f * ((static_cast<float>(height_map->rows) - 1.0f) * height_map->cell_size.z);

    height_map->min.z = -offset_z;
    height_map->min.x = -offset_x;
    height_map->max.z = offset_z;
    height_map->max.x = offset_x;

    // generate vertices
    int i = 0, j = -1;
    for (auto t = 0u; t < vrtx_count; ++t)
    {
        j = (j + 1) % height_map->cols;
        i = t / height_map->cols;
        const auto x = (static_cast<float>(j) * height_map->cell_size.x) - offset_x;
        const auto z = (static_cast<float>(i) * height_map->cell_size.z) - offset_z;
        const auto y = height_map->heights[t];
        // mapping a 5 unit texture to 4 vertices
        //        |   |   |   |   |
        //        0  1/3 2/3  1  4/3
        const glm::vec2 tex_coords{static_cast<float>(j) / (tex_tile.x - 1.0f),
                                   static_cast<float>(i) / (tex_tile.y - 1.0f)};
#ifdef HALF_TEX_COORD
        vertices.push_back({glm::vec3{x, y, z}, glm::packHalf2x16(tex_coords)});
#else
        vertices.push_back({glm::vec3{x, y, z}, tex_coords.x, tex_coords.y});
#endif
    }

    calculate_normals(height_map, vertices);
    // generate indices for one row
    std::vector<GLushort> indices = generate_indices(height_map->cols);

    meshes->push_back({GL_TRIANGLE_STRIP,
                       std::move(indices),
                       std::move(vertices),
                       std::move(textures)});
}

void parse_height_map(std::istringstream &istr,
                      std::ifstream &ifile,
                      Primitives::HeightMap *height_map,
                      std::vector<Primitives::Mesh> *meshes)
{
    istr >> height_map->rows >> height_map->cols;
    const auto &rows = height_map->rows, &cols = height_map->cols;

    glm::vec2 tex_tile;
    istr >> tex_tile.y >> tex_tile.x;     // rows and then cols i.e. V and then U
    std::vector<int32_t> textures;
    uint16_t tex_id;
    while (istr >> tex_id)
        textures.push_back(static_cast<uint8_t>(tex_id));

    // a texture, in both dimensions, should be mapped to some sizable area and thus cannot be ≤ 0
    // mathematically this avoids potential divide by zero at make_terrain_mesh site
    tex_tile.x = std::max(0.1f, tex_tile.x);
    tex_tile.y = std::max(0.1f, tex_tile.y);

    height_map->heights.reserve(rows * cols);
    float max_height = -std::numeric_limits<float>::min();
    for(auto i = 0u; i < rows; ++i)
    {
        next_line(ifile, istr);
        float height;
        while(istr >> height)
        {
            assert(height >= 0.0f);
            if(max_height < height)
                max_height = height;
            height_map->heights.push_back(height);
        }
        assert((height_map->heights.size() % cols) == 0);
    }
    // minimum height = 0 which is set by vec3::vec3()
    height_map->max.y = max_height;

    make_terrain_mesh(height_map,
                      meshes,
                      std::move(textures),
                      tex_tile);
}

Bitmap read_bitmap(const std::string &file_name)
{
    std::ifstream bmp_file{file_name, std::ios_base::in | std::ios_base::binary};

    BMP_header header;
    // reader header
    if(! (bmp_file.read(reinterpret_cast<char*>(&header), sizeof(BMP_header)) && (BM_ASCII == header.magic)))
    {
        throw std::invalid_argument{file_name + " isn't a valid BMP file"};
    }

    // check if the size is right
    const uint32_t data_size = header.width * header.height *
                               (header.bits_per_pixels / std::numeric_limits<unsigned char>::digits);
    auto file_size = bmp_file.tellg();
    bmp_file.seekg(0, std::ios_base::end);
    file_size = bmp_file.tellg() - file_size;
    if(file_size != data_size)
    {
        throw std::invalid_argument{file_name + " is a malformed BMP file"};
    }

    // read data
    bmp_file.seekg(header.data_offset, std::ios_base::beg);
    std::vector<uint8_t> bitmap(data_size);
    bmp_file.read(reinterpret_cast<char*>(bitmap.data()), data_size);
    return {header.width, header.height, bitmap};
}

void load_terrain_file(std::istringstream &istr,
                       Primitives::HeightMap *height_map,
                       std::vector<Primitives::Mesh> *meshes)
{
    std::string file_name;
    istr >> file_name;
    const Bitmap bmp = read_bitmap(file_name);
    assert((bmp.data.size() % num_bmp_channels) == 0);
    const auto data_length = bmp.data.size() / num_bmp_channels;
    height_map->heights.reserve(data_length);
    for(size_t i = 0u; i < bmp.data.size(); i += num_bmp_channels)
    {
        const float ht = bmp.data[i];
        height_map->heights.push_back(ht);
        height_map->max.y = std::max(height_map->max.y, ht);
    }

    height_map->rows = bmp.width;
    height_map->cols = bmp.height;

    glm::vec2 tex_tile;
    istr >> tex_tile.y >> tex_tile.x;     // rows and then cols i.e. V and then U
    std::vector<int32_t> textures;
    uint16_t tex_id;
    while (istr >> tex_id)
        textures.push_back(static_cast<uint8_t>(tex_id));

    // a texture, in both dimensions, should be mapped to some sizable area and thus cannot be ≤ 0
    // mathematically this avoids potential divide by zero at make_terrain_mesh site
    tex_tile.x = std::max(0.1f, tex_tile.x);
    tex_tile.y = std::max(0.1f, tex_tile.y);

    make_terrain_mesh(height_map, meshes, std::move(textures), tex_tile);
}

void load_texture(std::string file_name, std::vector<GL::Texture> *textures)
{
    // actual value should be quieried from GL_MAX_TEXTURE_IMAGE_UNITS
    // but all GL 3.3 cards should support atleast 16
    constexpr auto max_textures = 16u;
    assert(textures->size() <= max_textures);
    // the texture unit is irrelevant to texture creation, only while using do we have to make
    // sure different textures are put in different units; hence using GL_TEXTURE0 always for creation
    // http://3dgep.com/multi-textured-terrain-in-opengl/
    textures->push_back(GL::upload_texture(file_name.data(), GL_TEXTURE0));
}

void setup_light(std::istringstream &istr,
                 std::vector<Light> *lights)
{
    glm::vec3 dir;
    glm::vec4 ambient, diffuse;

    istr >> dir.x >> dir.y >> dir.z;
    istr >> ambient.r >> ambient.g >> ambient.b >> ambient.a;
    istr >> diffuse.r >> diffuse.g >> diffuse.b >> diffuse.a;

    // light direction in SDL is defined as flowing from light to object surface
    // invert it to make the vector go from surface to light for GLSL (see §5.2, RTR 3/e)
    lights->push_back({glm::normalize(-dir),
                       ambient,
                       diffuse});
}

}   // unnamed namespace

namespace Primitives
{

// this variant doesn't work in terrain space but the underlying heightmap (grid) space
float HeightMap::height_at(size_t i, size_t j)
{
    // 3 x 3 cells implies i, j can vary between [0, 3]
    assert((i <= rows) && (j <= cols));
    return heights[i * cols + j];
}

// this variant takes in x, z locations
float HeightMap::height_at(float x, float z)
{
    constexpr float eps = 10.0E-6f;
    // the calculation here depends on how the cells were tesselated when generating the terrain mesh;
    // the origin of the cell differs when it's |/| as opposed to |\|; currently it is the former
    const auto idx_x = x_to_index(x);
    const auto idx_z = z_to_index(z);

    // on a height map of size 4 x 5, when (idx_x, idx_z) = (4, 0.885162175) i.e. the height at right-most or
    // bottom-most is requested this would lead to a height lookup at (0, 4), (0, 5), (1, 4), (1, 5);
    // the 2nd and 3rd are out of bounds request, need to cap it for such cases
    const auto left_real = std::fmin(floor_if_not_int(idx_x, eps), static_cast<float>(cols - 2));
    const auto top_real = std::fmin(floor_if_not_int(idx_z, eps), static_cast<float>(rows - 2));
    const auto left = static_cast<size_t>(left_real);
    const auto top = static_cast<size_t>(top_real);
    const auto right = left + 1u;
    const auto bottom = top + 1u;
    const auto alpha = idx_x - left_real;
    const auto beta = idx_z - top_real;
    // using barycentric coordinates (convex combination) to find if the point is in the upper or lower triangle
    // a point is on a triangle if p = o + αS + βT such that α + β ≤ 1 and both α, β ∈ [0, 1]
    // taking the upper left point as the origin, if α + β ≤ 1 implies upper triangle
    // Ref: §19.5 in Introduction to 3D Game Programming with DirectX 11 by Frank D. Luna
    if ((alpha + beta) <= 1.0f)
    {
        return (1.0f - alpha - beta) * height_at(top, left) +
                               alpha * height_at(top, right) +
                                beta * height_at(bottom, left);
    }
    else    // invert alpha, beta and gamma for the lower triangle's barycentric coordinates
    {
        return (alpha + beta - 1.0f) * height_at(bottom, right) +  // 1 - (1 - α) - (1 - β)
                      (1.0f - alpha) * height_at(bottom, left) +
                       (1.0f - beta) * height_at(top, right);
    }
}

void HeightMap::get_triangles(const Cell &cell,
                              Triangle *t1,
                              Triangle *t2)
{
    const auto l_x = index_to_x(static_cast<float>(cell.x));
    const auto b_z = index_to_z(static_cast<float>(cell.y + 1u));
    const auto lb_y = height_at(static_cast<size_t>(cell.y + 1u),
                                static_cast<size_t>(cell.x));
    const auto r_x = index_to_x(static_cast<float>(cell.x + 1u));
    const auto t_z = index_to_z(static_cast<float>(cell.y));
    const auto rt_y = height_at(static_cast<size_t>(cell.y),
                                static_cast<size_t>(cell.x +1u));

    t1->verts[0] = t2->verts[0] = {l_x, lb_y, b_z};
    t1->verts[1] = t2->verts[2] = {r_x, rt_y, t_z};
    t1->verts[2] = {l_x, height_at(static_cast<size_t>(cell.y), static_cast<size_t>(cell.x)), t_z};
    t2->verts[1] = {r_x, height_at(static_cast<size_t>(cell.y + 1u), static_cast<size_t>(cell.x + 1u)), b_z};
}

/*
 * these functions mapping between position and index transform position values into
 * indices of a unit grid that has origin at left-top-down, x goes to right, y goes up
 * and z goes down; when viewed top down along y axis, x and z form a 2D system useful
 * for grid ray tracing which is similar to the screen coordinate system used in most
 * operating systems, where x goes right, and y (earlier z, now y) goes down.
 */
float HeightMap::x_to_index(float pos)
{
    const auto ret = (pos / cell_size.x) + (0.5f * static_cast<float>(cols - 1));
    return glm::clamp(ret, 0.0f, static_cast<float>(cols));
}

float HeightMap::y_to_index(float pos)
{
    // y starts from 0 and goes upwards, no negative values to offset the origin
    return pos / cell_size.y;
}

float HeightMap::z_to_index(float pos)
{
    const auto ret = (pos / cell_size.z) + (0.5f * static_cast<float>(rows - 1));
    return glm::clamp(ret, 0.0f, static_cast<float>(rows));
}

float HeightMap::index_to_x(float index)
{
    return cell_size.x * (index - (0.5f * static_cast<float>(cols - 1)));
}

float HeightMap::index_to_y(float index)
{
    return cell_size.y * index;
}

float HeightMap::index_to_z(float index)
{
    return cell_size.z * (index - (0.5f * static_cast<float>(rows - 1)));
}

void parse_SDL(const char *file_path,
               Scene *scene)
{
    assert(file_path && scene);

    std::ifstream sdl_file{file_path};
    for(std::string line; std::getline(sdl_file, line); )
    {
        std::istringstream istr{line};
        char code;
        if (istr >> code)
        {
            switch(code)
            {
                case 'i':
                {
                    std::string file_name;
                    istr >> file_name;
                    load_texture(file_name, &scene->textures);
                    break;
                }
                case 'd':
                {
                    setup_light(istr, &scene->lights);
                    break;
                }
                case 'h':
                {
                    // having more than one terrain (both 't and 'h') in an SDL file is invalid
                    // first one takes precedence, rest are ignored
                    if (scene->height_map.heights.empty())
                        parse_height_map(istr, sdl_file, &scene->height_map, &scene->meshes);
                    break;
                }
                case 't':
                {
                    // having more than one terrain (both 't and 'h') in an SDL file is invalid
                    // first one takes precedence, rest are ignored
                    if (scene->height_map.heights.empty())
                        load_terrain_file(istr, &scene->height_map, &scene->meshes);
                    break;
                }
            }
        }
    }
}

// 5.2.1 Intersection of a line and a plane - from Lengyel's skeleton book
bool ray_plane_intersect(const Ray &ray,
                         const Plane &plane,
                         float *t)
{
    const auto denom = glm::dot(glm::vec4(ray.direction, 0.0f),
                                plane.normal_signed_dist);
    // ray is parallel to plane
    if(glm::epsilonEqual(denom,
                         0.0f,
                         glm::epsilon<float>()))
        return false;
    const auto num = glm::dot(glm::vec4(ray.origin, 1.0f),
                              plane.normal_signed_dist);
    const float dist = -num/denom;
    if(dist < 0.0f)    // plane behind ray case
        return false;
    *t = dist;
    return true;
}

// Real-Time Rendering, 3rd edition with errata taken into account
bool ray_triangle_intersect(const Ray &ray,
                            const Triangle &tri,
                            float *t)
{
    const auto e0 = tri.verts[1] - tri.verts[0];
    const auto e1 = tri.verts[2] - tri.verts[0];

    const auto q = glm::cross(ray.direction, e1);
    const auto a = glm::dot(e0, q);
    if (glm::epsilonEqual(a, 0.0f, glm::epsilon<float>()))
    {
        return false;
    }
    const auto f = 1.0f / a;
    const auto s = ray.origin - tri.verts[0];
    const auto u = f * glm::dot(s, q);
    if (u < 0.0f)
        return false;
    const auto r = glm::cross(s, e0);
    const auto v = f * glm::dot(ray.direction, r);
    if ((v < 0.0f) || ((u + v) > 1.0f))
        return false;
    *t = f * glm::dot(e1, r);
    return true;
}

// Real-Time Rendering, 3rd Edition ~ Kay-Kajiya's slabs method
bool ray_obb_intersect(const Ray &ray,
                       const OBB &box,
                       float *t1,
                       float *t2)
{
    constexpr float eps_parallel = 10.0E-20f;
    constexpr float eps = 10.0E-6f;
    constexpr auto n_dimensions = 3u;

    float farthest_entry = -std::numeric_limits<float>::max();
    float nearest_exit   =  std::numeric_limits<float>::max();
    const auto p = box.centre - ray.origin;
    for (auto i = 0u; i < n_dimensions; ++i)
    {
        const auto e = glm::dot(box.axes[i], p);
        const auto f = glm::dot(box.axes[i], ray.direction);

        // ray not parallel to slab planes
        if (std::abs(f) > eps_parallel)
        {
            const auto reciprocal_f = 1.0f / f;
            // calculate entry and exit
            const auto entry = (e + box.half_extent[i]) * reciprocal_f;
            const auto exit = (e - box.half_extent[i]) * reciprocal_f;
            if (exit > entry)
            {
                if (exit < nearest_exit) nearest_exit = exit;
                if (entry > farthest_entry) farthest_entry = entry;
            }
            else
            {
                if (entry < nearest_exit) nearest_exit = entry;
                if (exit > farthest_entry) farthest_entry = exit;
            }
            if ((farthest_entry > nearest_exit) || (nearest_exit < 0.0f))
                return false;
        }
        else if (((-box.half_extent[i] - e) > 0.0f) || ((box.half_extent[i] - e) < 0.0f))
            return false;
    }
    // if farthest entry > 0 then ray originates outside the box
    // if farthest entry = 0 then ray originates on the box's surface
    // else originates inside the box
    if (less(farthest_entry, 0.0f, eps))
        *t1 =  nearest_exit;
    else
    {
        *t1 = farthest_entry;
        if (t2)
            *t2 = nearest_exit;
    }
    return true;
}

glm::vec3 get_point(const Ray &ray, float t)
{
    return {std::fma(t, ray.direction.x, ray.origin.x),
            std::fma(t, ray.direction.y, ray.origin.y),
            std::fma(t, ray.direction.z, ray.origin.z)};
}

bool is_degenerate(const Ray &ray, float epsilon)
{
    return glm::epsilonEqual(ray.direction.x, 0.0f, epsilon) &&
           glm::epsilonEqual(ray.direction.y, 0.0f, epsilon) &&
           glm::epsilonEqual(ray.direction.z, 0.0f, epsilon);
}

OBB aabb_to_obb(const glm::vec3 &min, const glm::vec3 &max)
{
    const auto box_extent = max - min;
    const auto half_extent = 0.5f * box_extent;
    return {min + half_extent, half_extent, glm::mat3{}};
}

Ray clip_ray(const Ray &ray,
             const glm::vec3 &min,
             const glm::vec3 &max)
{
    const auto clip_box = aabb_to_obb(min, max);
    float t1 = std::numeric_limits<float>::infinity(), t2 = std::numeric_limits<float>::infinity();
    if (ray_obb_intersect(ray, clip_box, &t1, &t2))
    {
        const auto p1 = get_point(ray, t1);
        // ray originates outside the box
        if (t2 != std::numeric_limits<float>::infinity())
        {
            const auto p2 = get_point(ray, t2);
            return {p1, p2 - p1};
        }
        else
        {
            return {ray.origin, p1 - ray.origin};
        }
    }
    // no intersection, return a degenerate ray
    return {};
}

// here left and top are the lesser values, similar to most screen coordinate systems
glm::vec4 left_top_right_bottom(glm::vec2 min, glm::vec2 max)
{
    if (min.x > max.x)
        std::swap(min.x, max.x);
    if (min.y > max.y)
        std::swap(min.y, max.y);
    return {min.x, min.y, max.x, max.y};
}

}  // Primitives
