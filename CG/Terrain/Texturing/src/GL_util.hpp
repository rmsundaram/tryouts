#ifndef GL_UTIL_HPP
#define GL_UTIL_HPP

namespace GL
{

template <typename T>
constexpr GLenum type_id(T);

template <>
constexpr GLenum type_id(GLfloat)
{
    return GL_FLOAT;
}

template <>
constexpr GLenum type_id(GLubyte)
{
    return GL_UNSIGNED_BYTE;
}

template <>
constexpr GLenum type_id(GLushort)
{
    return GL_UNSIGNED_SHORT;
}

template <>
constexpr GLenum type_id(GLuint)
{
    return GL_UNSIGNED_INT;
}

// returns a VBO
template <typename T>
Buffer upload_data(const std::vector<T> &data, GLenum target);

// returns a VAO
template <size_t atrib_cnt>
VAO setup_attributes(GLuint vertex_buffer,
                     const int (&attrib_type) [atrib_cnt],
                     const unsigned (&elem_cnt) [atrib_cnt],
                     GLuint index_buffer);

// returns a texture
inline
Texture upload_texture(const char *file_name, GLenum texture_unit);

inline
GLint uniform_loc(GLint program, const char *name)
{
    const auto loc = glGetUniformLocation(program, name);
    assert(loc != -1);
    return loc;
}

}   // namespace GL

#include "GL_util.inl"

#endif // GL_UTIL_HPP
