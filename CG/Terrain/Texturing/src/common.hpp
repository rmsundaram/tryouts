#ifndef __COMMON_HPP__
#define __COMMON_HPP__

#include "gl_3_3.h"

// http://stackoverflow.com/q/118774/183120
#undef far
#undef near

#undef __STRICT_ANSI__
#include <cmath>
#define __STRICT_ANSI__

#include <gli/gli.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/rotate_normalized_axis.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/epsilon.hpp>
#include <glm/gtc/reciprocal.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtx/normal.hpp>
#include <glm/gtc/packing.hpp>

#ifndef M_PI_4
#include <glm/ext/scalar_constants.hpp>

constexpr auto M_PI_4 = glm::quarter_pi<double>();
#endif // !M_PI_4

#include <memory>
#include <vector>
#include <map>
#include <algorithm>
#include <string>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <utility>
#include <functional>
#include <cassert>
#include <iterator>

#endif  // __COMMON_HPP__
