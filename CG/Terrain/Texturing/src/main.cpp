/*
 * Learnings:
 *  1. Wrapping a texture on more than one triangle; also generating texture coordinates on the fly.
 *  2. Loading multiple textures via the SDL file and without rebuilding the program, the shader can use upto 10
 *      textures without hard-coding the current texture count
 *  3. Pixel transfer² and its quirks³: how pixel data is uploaded on to the GPU memory (unpacking): glTexImage2D
 *      Internal formats is how the texture will be laid out in GL's memory, (external) format and type describe
 *      the data in the client (RAM) memory; if the format has stride/pitch unpack alignment is to be set too.
 *  4. Used glm::packHalf2x16 to convert two floats (texture coordinates) into halfs and pack them into a uint32_t.
 *  5. Using half floats instead of floats to repesent texture coordinates and there by having aligned per-vertex
 *      data: 3 floats for position and one for texture coordintes. Sending halfs, shorts or anything via
 *      glVertexAttribPointer would mean they get promoted to floats⁴. For integral data the normalized parameter
 *      decides it they are convereted to [0, 1] or just their values are casted into float as-is. If one wishes
 *      to retain them as integrals then glVertexAttribIPointer is to be used. The size of the vertex data is
 *      defined by glVertexAttribPointer, not the GLSL attribute variable. One needn't represent the smaller
 *      components in the shader; the hardware will convert from smaller components to larger ones.
 *  6. Uploading to an array of uniforms in one shot.
 *  7. Calcuating normals for vertices⁵ by vertex normal averaging⁶.
 *  8. Multi-sampling based anti-aliasing (MSAA) through ARB_multisample. Yet to explore FXAA and MLAA.
 * ========== (just by reading)
 *  8. Texture filtering: magnification and minification; they don't change the points where the texture is pinned on to
 *      the mesh but are merely an zoom in/out of the texture image. Frank D. Luna's book⁷ gives a clear illustration
 *      comparing nearest-neighbour (point) and linear filtering; Shawn’s articles explain it too¹⁰.
 *  9. Texture coordinates are simply normalized coordinates [0, 1] to avoid referring to different dimensions of
 *      various textures.
 * 10. Addressing mode (clamp, border, wrap, mirror) is the mechanism to handle texture coordinates beyond [0, 1].
 * 11. When multi-texturing, the blend factor may be the height of the terrain at that fragment (like here) or an
 *      alpha value encoded in one of the textures (like recipe 4.2⁸) or a separate alpha map as shown in Wikipedia's
 *      article⁹ on texture splatting, a method for combining different textures. It works by applying an alphamap
 *      (also called a "weightmap" or "splat map") to the higher levels, revealing the layers underneath where the
 *      alphamap is partially or completely transparent. Alpha maps are just like layer masks in GIMP.
 * 12. Anisotrophic filtering is to fix the distortion that occurs when the view vector and the normal of a surface
 *      (say ground) is obtuse/oblique so that it appears non-orthogonal; hence the word anistrophic. "an" for not,
 *      "iso" for same, and "tropic" from tropism, relating to direction; anisotropic filtering does not filter the
 *      same in every direction. This is the most expensive filter but may be worth the cost to correct such
 *      distortions as moire.
 * 13. For the vertex attributes, instead of having a struct in primitives to denote position, normal, texture
 *      coordinates, etc. one can also pack them inside a byte vector to upload to a buffer and then define the
 *      layout through glVertexAttribPointer like the examples in mbsoftworks.sk.
 * 14. Triplanar texture mapping is a technique used to address the texture stretching artefact occuring when there's
 *      a steep triangle in the mesh where the texture is applied on a single plane (say XZ) and the variation in
 *      those directions are less, while variation in the other direction (say Y) is high.
 *
 * REFERENCES:
 *     MB Softworks tutorial 11 and 21 (multi-texturing and multi-layered terrain)
 *  1. http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=14
 *  2. https://www.opengl.org/wiki/Pixel_Transfer#Pixel_type
 *  3. http://stackoverflow.com/q/27200528/183120
 *  4. http://stackoverflow.com/a/11679024/183120
 *  5. http://stackoverflow.com/q/6656358/183120
 *  6. §6.2.1, Introduction to 3D Game Programming with DirectX 10
 *  7. Introduction to 3D Game Programming with DirectX 10
 *  8. OpenGL 4.0 Shading Language Cookbook by David Wolff
 *  9. http://en.wikipedia.org/wiki/Texture_splatting
 * 10. http://www.shawnhargreaves.com/blog/texture-filtering.html
 */

#include "common.hpp"
#include "Util.hpp"
#include "Dataless_wrapper.hpp"
#include "GL_res_wrap.hpp"
#include "GL_util.hpp"
#include "Shader_util.hpp"
#include "Primitives.hpp"
#include "Light.hpp"
#include "Camera.hpp"
#include "main.hpp"

#include "Window.inl"

constexpr auto *scene_file = "data/map.sdl";

namespace
{

float get_hit_tri(Scene *scene,
                  const Primitives::Ray &ray,
                  const Primitives::Cell &hit_cell,
                  uint8_t *triangle_id)
{
    // perform ray-triangle intersection
    Primitives::Triangle tri[2];
    scene->height_map.get_triangles(hit_cell, &tri[0], &tri[1]);
    uint8_t tri_id = 2u;
    float t = std::numeric_limits<float>::infinity();
    if (Primitives::ray_triangle_intersect(ray, tri[0], &t))
    {
        tri_id = 0;
    }
    else if (Primitives::ray_triangle_intersect(ray, tri[1], &t))
    {
        tri_id = 1;
    }
    assert(tri_id < 2);
    *triangle_id = tri_id;
    return t;
}

bool hit_test_cell(Scene *scene,
                   const Primitives::Ray &ray,
                   const glm::vec2 &entry,
                   const glm::vec2 &exit,
                   float t_exit,
                   Primitives::Cell *hit_cell)
{
    // here entry.y and exit.y are really z values since height is denoted by y
    // z denotes the top and bottom in the grid when viewed along Y axis from top
    // since these are going to be array indices they cannot be < 0
    assert(entry.x >= 0.0f && entry.y >= 0.0f);
    assert(exit.x >= 0.0f && exit.y >= 0.0f);

    const auto exit_x = scene->height_map.index_to_x(exit.x);
    const auto exit_z = scene->height_map.index_to_z(exit.y);
    const auto ray_exit_ht = std::fma(t_exit, ray.direction.y, ray.origin.y);
    const auto map_exit_ht = scene->height_map.height_at(exit_x, exit_z);

    // TODO: As of 0.9.5.3 GLM has float x, y, z, w; for vec4 and not float[4], thus such access is not strictly
    // portable due to struct padding
    const glm::vec4 ltrb = Primitives::left_top_right_bottom(entry, exit);
    const auto left = static_cast<size_t>(floor_if_not_int(ltrb[0]));
    const auto top = static_cast<size_t>(floor_if_not_int(ltrb[1]));
    *hit_cell = {left, top};

    // TODO: when camera is underneath the terrain surface, inverting this condition (>) is required
    // as the ray would originate below the surface and it should be tested for when it'll go above it
    const bool hit = ray_exit_ht <= map_exit_ht;
#ifdef DEBUG_SELECTION
    // if debugging selection, add triangles of every cell visited
    if (!hit)
    {
        scene->sel_cells_dbg.insert(scene->sel_cells_dbg.end(), {*hit_cell, *hit_cell});
        scene->sel_tris_dbg.insert(scene->sel_tris_dbg.end(), {0, 1});
    }
#endif  // DEBUG_SELECTION

    return hit;
}

void reset_selection(Scene *scene)
{
    scene->sel_tri = 2u;
#ifdef DEBUG_SELECTION
    scene->sel_cells_dbg.clear();
    scene->sel_tris_dbg.clear();
#endif  // DEBUG_SELECTION
}

float hit_test_height_map(Scene *scene, const Primitives::Ray &ray)
{
    // TODO: it would be simpler and more performant if the traversal and hit testing happen in the same space.

    // cell_traverse happens in a space where the rows and columns are in integer points
    // however the height map might have scaling, hence transforming ray to the unit grid space
    const glm::vec3 grid_ray_origin = {
                                        scene->height_map.x_to_index(ray.origin.x),
                                        scene->height_map.y_to_index(ray.origin.y),
                                        scene->height_map.z_to_index(ray.origin.z)
                                      };
    const auto end_point = ray.origin + ray.direction;
    const glm::vec3 grid_ray_end = {
                                     scene->height_map.x_to_index(end_point.x),
                                     scene->height_map.y_to_index(end_point.y),
                                     scene->height_map.z_to_index(end_point.z)
                                   };
    Primitives::Ray grid_ray = { grid_ray_origin, grid_ray_end - grid_ray_origin };

    float t = std::numeric_limits<float>::infinity();

    Primitives::Cell hit_cell;
    if (Primitives::grid_traverse(grid_ray,
                                  [&](const glm::vec2 &entry, const glm::vec2 exit, float t)
                                  {
                                      return hit_test_cell(scene, ray, entry, exit, t, &hit_cell);
                                  }))
    {
        // entering here confirms that a hit was made by the ray on hit_cell
        uint8_t tri_id;
        t = get_hit_tri(scene, ray, hit_cell, &tri_id);
        scene->sel_cell = hit_cell;
        scene->sel_tri = tri_id;
    }
    return t;
}

void init_rendering()
{
    // this is set by default
    //glViewport(0, 0, screen_width, screen_height);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // enabling culling culls out the plane when trying to view it from underneath; the reason is that a plane,
    // unlike a model, has no inside and hence culling it doesn't make sense. If you want planes to be visible
    // in all directions then disable this
    glEnable(GL_CULL_FACE);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    // enabling this avoids clipping by near and far planes but any fragments that would have
    // normalized z coordinates outside of the [-1, 1] range will be clamped to that range.
    // It screws up depth tests between fragments that are being clamped, but usually those
    // objects are far away.
    glEnable(GL_DEPTH_CLAMP);
    glEnable(GL_MULTISAMPLE);
}

void upload_meshes(const std::vector<Primitives::Mesh> &meshes,
                   std::vector<GL::VAO> *VAOs)
{
    VAOs->reserve(meshes.size());
    for(size_t i = 0u; i < meshes.size(); ++i)
    {
        GL::Buffer vbo = GL::upload_data(meshes[i].vertices, GL_ARRAY_BUFFER);
        GL::Buffer ibo = GL::upload_data(meshes[i].indices, GL_ELEMENT_ARRAY_BUFFER);
        VAOs->emplace_back(GL::setup_attributes(vbo,
                                                Primitives::Mesh::type_id,
                                                Primitives::Mesh::elem_lens,
                                                ibo));
    }
}

void set_uniforms(GLuint program, const Scene &scene)
{
    constexpr auto MAX_UNIFORM_NAME = 200;

    const auto PV_xform_id = GL::uniform_loc(program, "PV");
    auto PV = scene.current_cam()->projection * scene.current_cam()->view_xform;
    glUniformMatrix4fv(PV_xform_id, 1, GL_FALSE, &PV[0][0]);

    for (auto i = 0u; i < scene.lights.size(); ++i)
    {
        const char *light_dir = "lights[%u].direction";
        const char *light_amb = "lights[%u].ambient";
        const char *light_dif = "lights[%u].diffuse";
        char uniform_name[MAX_UNIFORM_NAME] = {};
        snprintf(uniform_name, MAX_UNIFORM_NAME, light_dir, i);
        const auto light_dir_id = GL::uniform_loc(program, uniform_name);
        snprintf(uniform_name, MAX_UNIFORM_NAME, light_amb, i);
        const auto light_amb_id = GL::uniform_loc(program, uniform_name);
        snprintf(uniform_name, MAX_UNIFORM_NAME, light_dif, i);
        const auto light_diffuse_id = GL::uniform_loc(program, uniform_name);

        glUniform3fv(light_dir_id, 1, &scene.lights[i].direction[0]);
        glUniform4fv(light_amb_id, 1, &scene.lights[i].ambient[0]);
        glUniform4fv(light_diffuse_id, 1, &scene.lights[i].diffuse[0]);
    }

    const auto lights_used_id = GL::uniform_loc(program, "lights_used");
    glUniform1ui(lights_used_id, scene.lights.size());
}

void render(GLuint program,
            const Scene &scene,
            const std::vector<GL::VAO> &VAOs)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
#ifndef NDEBUG
    glPolygonMode(GL_FRONT_AND_BACK, scene.rendering_mode);
#endif
    glUseProgram(program);
    set_uniforms(program, scene);
    for(auto i = 0u; i < scene.meshes.size(); ++i)
    {
        glBindVertexArray(VAOs[i]);

        const auto M_xform_id = GL::uniform_loc(program, "M");
        glUniformMatrix4fv(M_xform_id, 1, GL_FALSE, &scene.meshes[i].model_xform[0][0]);

        const auto txr_cnt = scene.meshes[i].textures.size();
        std::vector<GLint> tex_units;
        for (auto j = 0u; j < txr_cnt; ++j)
        {
            glActiveTexture(GL_TEXTURE0 + j);
            glBindTexture(GL_TEXTURE_2D, scene.textures[scene.meshes[i].textures[j]]);
            tex_units.push_back(tex_units.size());
        }
        if (txr_cnt)
        {
            const auto tex_unit = GL::uniform_loc(program, "tex_unit");
            glUniform1iv(tex_unit, txr_cnt, tex_units.data());
            const auto tex_used = GL::uniform_loc(program, "tex_used");
            glUniform1ui(tex_used, txr_cnt);
        }

#ifndef MULTI_DRAW
        for (auto j = 0u; j < scene.height_map.rows - 1; ++j)
            glDrawElementsBaseVertex(scene.meshes[i].primitive_type,
                                     scene.meshes[i].indices.size(),
                                     GL::type_id(decltype(scene.meshes[i].indices)::value_type{}),
                                     reinterpret_cast<GLvoid*>(0u),
                                     j * scene.height_map.cols);
#else
        const auto vertical_cells = scene.height_map.rows - 1;
        // TODO: generate only once (when terrain data is generated) and reuse
        std::vector<GLsizei> counts(vertical_cells, scene.meshes[i].indices.size());
        std::vector<GLsizei> indices(vertical_cells, 0);
        std::vector<GLint> base_verts(vertical_cells);
        auto j = 0u;
        std::generate(base_verts.begin(), base_verts.end(), [&] { return scene.height_map.cols * j++; });
        glMultiDrawElementsBaseVertex(scene.meshes[i].primitive_type,
                                      counts.data(),
                                      GL::type_id(decltype(scene.meshes[i].indices)::value_type{}),
                                      reinterpret_cast<GLvoid const* const*>(indices.data()),
                                      counts.size(),
                                      base_verts.data());
#endif  // MULTI_DRAW

        // no need to activte each texture unit and unbind the texture from the target
        // this should automatically happen when a new texture is bound
        //glBindTexture(GL_TEXTURE_2D, 0);
        glBindVertexArray(0);
    }
    glUseProgram(0);
}

void render_sel(GLuint program,
                const Scene &scene,
                const std::vector<GL::VAO> &VAOs)
{
    // validate selection
    if (scene.sel_tri < 2u)
    {
#ifndef NDEBUG
        glPolygonMode(GL_FRONT_AND_BACK, scene.rendering_mode);
#endif
        glUseProgram(program);
        auto PV = scene.current_cam()->projection * scene.current_cam()->view_xform;
        // FIXME: hack in directly indexing height map's mesh
        // should actually be queried from the scene.height_map
        glBindVertexArray(VAOs[0]);
        const auto PV_xform_id = GL::uniform_loc(program, "PV");
        glUniformMatrix4fv(PV_xform_id, 1, GL_FALSE, &PV[0][0]);
        const auto M_xform_id = GL::uniform_loc(program, "M");
        glUniformMatrix4fv(M_xform_id, 1, GL_FALSE, &scene.meshes[0].model_xform[0][0]);
        const auto colour_id = GL::uniform_loc(program, "solidColour");
        const glm::vec4 sel_colour{1.0f, 0.0f, 0.0f, 1.0f};
        glUniform4fv(colour_id, 1, &sel_colour[0]);

        using Index_type = decltype(scene.meshes[0].indices)::value_type;
#ifdef DEBUG_SELECTION
        for (auto i = 0u; i < scene.sel_tris_dbg.size(); ++i)
        {
            const auto base = (scene.sel_cells_dbg[i].y * scene.height_map.cols) + scene.sel_cells_dbg[i].x;
            const auto offset = scene.sel_tris_dbg[i] * sizeof(Index_type); // offset inside the index buffer, in bytes
            glFrontFace(GL_CCW - scene.sel_tris_dbg[i]);    // if sel_tri == 0, then CCW, else CCW - 1 = CW
            glDrawElementsBaseVertex(scene.meshes[0].primitive_type,
                                     3,
                                     GL::type_id(Index_type{}),
                                     reinterpret_cast<GLvoid*>(offset),
                                     base);
            glFrontFace(GL_CCW);
        }
#endif  // DEBUG_SELECTION

        // render selection
        const auto base = (scene.sel_cell.y * scene.height_map.cols) + scene.sel_cell.x;
        const auto offset = scene.sel_tri * sizeof(Index_type); // offset inside the index buffer, in bytes
        /*
         * The index data is such
         *     0 1 2 3 4
         *     5 6 7 8 9
         * When triangle strip is used, the resulting triangles' winding order is determined by the first triangle's
         * winding order. Also GL autmatically "fixes" every second triangle formed to confirm with the winding formed
         * by the first. So in this case, if 0 5 1 was the first triangle then the second, although would be 5 1 6, GL
         * would swap the first two indices resulting in 1 5 6, but for the next triangle, it'll be 1 6 2. Had the
         * first triangle been 5 0 6 (CW) then GL would make sure that all resulting triangles turn out with CW winding:
         * 6 0 1, 6 1 2, etc.
         * Since we're re-using the index data that was generated for rendering a triangle strip to render a single
         * triangle in-between instead of starting from the first and giving one additional index per new triangle,
         * we've to take care of the winding somehow since OpenGL can't fix it for us, since from GL's viewpoint this
         * is the first triangle starting the strip (the same even if GL_TRIANGLES was use). If we're enabling facet
         * [glEnable(GL_FACE_CULLING)] culling with the back face to be the culled one [glCullFace(GL_BACK)] and the
         * front face being set as the triangles with CCW winding [glFrontFace(GL_CCW)], then this poses no problem
         * for even tringles, since they too are CCW winding in our case. However, for the lower triangle in a cell,
         * say cell 0, we've to offset the index buffer at 1 and count = 3 i.e. starting at 5 leading to 5 1 6 which
         * results in a CW wound triangle.
         *
         * We've 3 options to remedy this: (i) turn off culling just for this draw call and turn it back on
         * (ii) tell GL to cull the front and not the back face, again just for this draw call (iii) set the
         * winding order that determines which is the front face from CCW to CW, only for this draw call and
         * revert it post call; choosing (iii) since it is the most local fix
         */
        glFrontFace(GL_CCW - scene.sel_tri);    // if sel_tri == 0, then CCW, else CCW - 1 = CW
        glDrawElementsBaseVertex(scene.meshes[0].primitive_type,
                                 3,
                                 GL::type_id(Index_type{}),
                                 reinterpret_cast<GLvoid*>(offset),
                                 base);
        glFrontFace(GL_CCW);
        glBindVertexArray(0);
        glUseProgram(0);
    }
}

void update_frame_time(double *last_time,
                       unsigned *frames_rendered,
                       GLFWwindow *window)
{
    ++*frames_rendered;
    const auto now = glfwGetTime();
    if((now - (*last_time)) >= 1.0)
    {
        std::ostringstream ss;
        ss << app_title << " ~ " << (1000.0 / *frames_rendered) << "ms/frame";
        const auto str_mpf = ss.str();
        glfwSetWindowTitle(window, str_mpf.c_str());
        *frames_rendered = 0;
        *last_time = now;
    }
}

}   // unnamed namespace

void pick(float x, float y, Scene *scene)
{
    reset_selection(scene);
    /*
     * for the z (world) coordinate of the pick point we need the focal length
     * which is cot (FoV/2) but since the perspective projection matrix already has it
     * we can reuse the value instead of computing it; this optimisation is discussed in
     * Frank D. Luna's excellent book; it explains picking quite comprehensively
     */
    const auto focal_length = scene->current_cam()->projection[1][1];
    // const auto focal_length = glm::cot(scene->FoV / 2.0f);

    /*
     * in view space this point generated would be (x, y, z, 1); in that space
     * subtracting origin (0, 0, 0, 1) from that point would give the same point
     * with the w coordinate becoming 0; thus this is the ray in view space
     */
    const glm::vec4 view_ray(((2.0f * x / scene->current_cam()->width) - 1.0f) * scene->current_cam()->aspect_ratio,
                             1.0f - (2.0f * y / scene->current_cam()->height),
                             -focal_length,
                             0.0f);
    /*
     * we've Mw->v, we need its inverse Mv->w; since Mw->v = TR
     * Mv->w = R^-1 T^-1; this is done by GLM's affineInverse; although just transposing the upper
     * 3x3 matrix in Mw->v would suffice for transforming the ray, we need the affine inverse for
     * transforming the ray origin i.e. camera location in world space
     */
    const auto inv_view = glm::affineInverse(scene->current_cam()->view_xform);
    // since we're transforming a ray, the last column doesn't matter, but we use it for ray origin
    const auto ray_dir = glm::normalize(glm::vec3(inv_view * view_ray));
    const auto ray_org = glm::vec3(inv_view[3]);    // camera locaton in world space
    const Primitives::Ray pick_ray = Primitives::clip_ray({ray_org, ray_dir},
                                                           scene->height_map.min,   // AABB with min and max
                                                           scene->height_map.max);
    /*
     * this ray originates from camera, goes throw the click point on the view plane and extends to infinity;
     * however, bounding it within min and max terrain height is optimal since there can't be vertices above
     * or below those limits.
     */
    // TODO: if the ray only touches an edge / corner of the AABB (formed by height_map.min and max), then
    // too we'd have a degenerate ray, however grid ray tracing isn't required in that case and just a hit
    // test of a triangle would do

    if (!Primitives::is_degenerate(pick_ray))
    {
        hit_test_height_map(scene, pick_ray);
    }
}

void Scene::setup_camera(float width, float height)
{
    assert(!height_map.heights.empty());
    // slightly higher than the possibly tallest peak for an 8-bit grayscale height map
    const auto min_height_to_terrain = height_map.max.y + 50.0f;
    cam[0].reset(new RTSCamera{FoV, width, height, near, far, min_height_to_terrain});
    cam[1].reset(new FreeCamera{FoV, width, height, near, far});
}

int main(int /*argc*/, char** /*argv*/)
{
    GLFW_wrapper glfw;
    auto glfw_wnd = create_window(glfw);
    Scene scene;
    Primitives::parse_SDL(scene_file, &scene);
    // TODO: instead of hard-coding this, see if this can be queried
    // from the program object
    ::check(scene.lights.size() < 5, "Too many lights to handle for the fragment shader!");
    ::check(scene.textures.size() < 5, "Too many textures to handle for the fragment shader!");

    int w, h;
    glfwGetWindowSize(glfw_wnd.get(), &w, &h);
    float width = static_cast<float>(w), height = static_cast<float>(h);
    scene.setup_camera(width, height);

    std::vector<GL::VAO> VAOs;
    upload_meshes(scene.meshes, &VAOs);

    // http://www.parashift.com/c++-faq-lite/memfnptr-vs-fnptr.html
    // according to C++ FAQ, you cannot pass a member function to a C callback; passing a functor, lambda, boost::bind,
    // etc. is for a C++ callback implemented with templates; instead use the void* user_data allowed in the callback;
    // GLFW gives GLFWwindow->SetWindowUserPointer for the same
    glfwSetWindowUserPointer(glfw_wnd.get(), reinterpret_cast<void*>(&scene));
    glfwSetKeyCallback(glfw_wnd.get(), handle_key);
    glfwSetMouseButtonCallback(glfw_wnd.get(), handle_mouse);
    glfwSetCursorPosCallback(glfw_wnd.get(), handle_cursor);
    glfwSetScrollCallback(glfw_wnd.get(), handle_scroll);

    GL::Program program{GL::setup_program("shaders/pos.vert",
                                          "shaders/tex.frag")};
    GL::Program program_sel{GL::setup_program("shaders/pos.vert",
                                              "shaders/solid.frag")};
    init_rendering();
    auto last_time = glfwGetTime();
    unsigned frames = 0u;
    while(!glfwWindowShouldClose(glfw_wnd.get()))
    {
        render(program, scene, VAOs);
        render_sel(program_sel, scene, VAOs);
        glfwSwapBuffers(glfw_wnd.get());
        handle_input(glfw_wnd.get(), &scene);
        update_frame_time(&last_time, &frames, glfw_wnd.get());
    }
}
