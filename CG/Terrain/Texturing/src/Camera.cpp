#include "common.hpp"
#include "Util.hpp"
#include "Primitives.hpp"
#include "Camera.hpp"

void RTSCamera::reset()
{
    // instead of directly using eye_x, eye_y, eye_z these're constexprs are put in
    // an array since directly passing them to glm::vec3::vec3, which takes them by reference,
    // leads to undefined reference error to define these static variables in a cpp
    constexpr float eye[3] = { eye_x, eye_y, eye_z };
    constexpr float look[3] = { look_x, look_y, look_z };
    view_xform = glm::lookAt(glm::vec3{eye[0], eye[1], eye[2]},
                             glm::vec3{look[0], look[1], look[2]},
                             glm::vec3{0.0f, 1.0f, 0.0f});
    ground_dist = std::sqrt((eye_x * eye_x) + (eye_y * eye_y) + (eye_z * eye_z));
}

void RTSCamera::slide_Y(float delta)
{
    // get view's Y-axis in terms of world space
    const auto &along = glm::row(view_xform, 1);
    // project it on XZ plane, normalize and scale it accordingly
    const auto pan_vert = -delta * glm::normalize(glm::vec3{along[0], 0.0f, along[2]});
    /*
     * We need Mw->v' = Rev(Tv'->w) = Mv->v' Mw->v. So the missing piece is Mv->v' or
     * Tv'->v. Since we know the required transformation in world units and finding it in
     * view units is more work, instead of pre-multiplying multiplying Mv->v',  we keep
     * v the same but change w to w' instead i.e. move the world. This works since moving
     * the camera north works out the same as moving the world south. Now we need
     * Mw'->v = Mw->v Mw'->w i.e. post-multiply Tw->w'. In the free-look camera workout,
     * we took the other route of pre-multiplying Mv->v' since it was convenient to
     * measure the units of transformation in terms of the view units than in world units.
     */
    view_xform = glm::translate(view_xform, pan_vert);
}

void RTSCamera::slide_Z(float delta)
{
    const float angle = std::atan2(eye_z - look_z, eye_y - look_y);
    const float cos = std::cos(angle);
    assert(cos > 0.0f); // Invalid angle between view dir and ground!
    const float min_dist = min_height / cos;

    delta = ((ground_dist + delta) < min_dist) ? (min_dist - ground_dist) : delta;
    ground_dist += delta;

    view_xform[3][2] -= delta;
}

void RTSCamera::rotate_Y(float angle)
{
    auto Mv2w = glm::affineInverse(view_xform);
    // eye in view space is the origin, transforming that with Mv2w would mean
    // the linear (upper 3x3) part would become zero, leaving only the affine (translation) part
    const glm::vec3 eye = glm::vec3(Mv2w[3]);
#ifdef NDEBUG
    // lookAt point w.r.t view space is trivial, transforming
    // that to world space gives lookAt point in world space
    const glm::vec4 lookat_view{0.0f, 0.0f, -ground_dist, 1.0f};
    const auto lookat = glm::vec3(Mv2w * lookat_view);
#else
    // viewdir is basis k in view space i.e. row 2 in view_xform, but col 2 of Mv2w would have the
    // same row transposed (xyz will be the same, w will be different - affineInverse); using it
    // avoids another transpose by glm::row since matrices are stored as columns by GLM
    const glm::vec3 viewdir = -glm::vec3(glm::column(Mv2w, 2));
    const Primitives::Ray cam = {eye, viewdir};
    const Primitives::Plane terrain = { {0.0f, 1.0f, 0.0f, 0.0f} };
    float t = -std::numeric_limits<float>::infinity();
    assert(Primitives::ray_plane_intersect(cam, terrain, &t));
    const auto lookat = Primitives::get_point(cam, t);
#endif  // NDEBUG
    const auto toOrg = glm::translate(-lookat);
    const auto rotor = glm::rotateNormalizedAxis(glm::mat4{}, angle, glm::vec3{0.0f, 1.0f, 0.0f});
    const auto froOrg = glm::translate(lookat);
    const auto new_eye = froOrg * rotor * toOrg * glm::vec4(eye, 1.0f);
    //view_xform = glm::lookAt(glm::vec3(new_eye), lookat, glm::vec3{0.0f, 1.0f, 0.0f});
    // this achieves the same as gluLookAt without using normalize and cross
    Mv2w[3] = {0.0f, 0.0f, 0.0f, 1.0f};
    Mv2w = rotor * Mv2w;
    Mv2w[3] = new_eye;
    view_xform = glm::affineInverse(Mv2w);
}
