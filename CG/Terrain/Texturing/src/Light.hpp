#ifndef LIGHT_HPP
#define LIGHT_HPP

struct Light
{
   static constexpr float rot_delta = 0.009f;
   glm::vec3 direction;
   glm::vec4 ambient;
   glm::vec4 diffuse;

   void rotate(float angle, glm::vec3 axis)
   {
       const auto versor = glm::angleAxis(angle, axis);
       direction = glm::normalize(versor * direction);
   }
};

#endif // LIGHT_HPP
