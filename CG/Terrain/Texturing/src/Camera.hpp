#ifndef CAMERA_HPP
#define CAMERA_HPP

struct Camera
{
    // manipulation constants
    static constexpr float slide_offset = 2.0f;
    static constexpr float rot_delta = 0.015f;
    static constexpr float FoV_delta = 0.002f;

    // world to view transform
    glm::mat4 view_xform;

    // projection parameters
    float FoV, width, height, aspect_ratio, near, far;
    glm::mat4 projection;

    Camera(float FoV,
           float width,
           float height,
           float near,
           float far) :
        FoV{FoV},
        width{width},
        height{height},
        aspect_ratio{width / height},
        near{near},
        far{far},
        projection(glm::perspective(FoV, aspect_ratio, near, far))
    {
    }

    virtual ~Camera() { }
    virtual void reset() = 0;
    virtual void slide_X(float delta) = 0;
    virtual void slide_Y(float delta) = 0;
    virtual void slide_Z(float delta) = 0;
    virtual void rotate_X(float angle) = 0;
    virtual void rotate_Y(float angle) = 0;
    virtual void rotate_Z(float angle) = 0;
    virtual void change_FoV(float angle)
    {
        FoV += angle;
        projection = glm::perspective(FoV, aspect_ratio, near, far);
    }
};

// Camera with 6 DoF (free-roaming robot); slide and rotate on all 3 axes
// the implmentation here is optimized lookup Free-look Camera workout
// for a straight forward implmentation to understand what's going on
struct FreeCamera : Camera
{

    FreeCamera(float FoV,
               float width,
               float height,
               float near,
               float far) :
        Camera(FoV, width, height, near, far)
    {
        // position camera to initial view
        reset();
    }

    void reset()
    {
        view_xform = glm::mat4{      1.0f,          0.0f,         0.0f, 0.0f,   // col 1
                                     0.0f,  0.619263887f, 0.785196841f, 0.0f,   // col 2
                                     0.0f, -0.785196841f, 0.619263887f, 0.0f,   // col 3
                               18.962925f,   1.83618283f,  -51.548912f, 1.0f};  // col 4
        //view_xform = glm::lookAt(glm::vec3(0, 60, 30), glm::vec3(0, 0, 0), glm::vec3(0.0f, 1.0f, 0.0f));
    }

    void slide_X(float delta)
    {
        view_xform[3][0] -= delta;
    }

    void slide_Y(float delta)
    {
        view_xform[3][1] -= delta;
    }

    void slide_Z(float delta)
    {
        view_xform[3][2] -= delta;
    }

    void rotate_X(float angle)
    {
        const auto inv = glm::eulerAngleX(-angle);
        view_xform = inv * view_xform;
    }

    void rotate_Y(float angle)
    {
        const auto inv = glm::eulerAngleY(-angle);
        view_xform = inv *view_xform;
    }

    void rotate_Z(float angle)
    {
        const auto inv = glm::eulerAngleZ(-angle);
        view_xform = inv * view_xform;
    }
};

struct RTSCamera : Camera
{
    RTSCamera(float FoV,
              float width,
              float height,
              float near,
              float far,
              float min_ht)
        : Camera{FoV, width, height, near, far}
        , min_height{min_ht}
    {
        // position camera to initial view
        reset();
    }

    void reset();
    void slide_X(float delta) { view_xform[3][0] -= delta; }
    void slide_Y(float delta);
    void slide_Z(float delta);
    void rotate_X(float /*angle*/) { }
    void rotate_Y(float angle);
    void rotate_Z(float /*angle*/) { }

private:
    static constexpr float eye_x = 0.0f;
    static constexpr float eye_y = 500.0f;
    static constexpr float eye_z = 400.0f;
    static constexpr float look_x = 0.0f;
    static constexpr float look_y = 0.0f;
    static constexpr float look_z = 0.0f;

    // minimum height to maintain between camera and ground
    float min_height;
    // the distance b/w view origin and ground, in view's Z scale;
    float ground_dist;
};

#endif // CAMERA_HPP
