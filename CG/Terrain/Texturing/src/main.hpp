#ifndef __MAIN_HPP__
#define __MAIN_HPP__

#ifndef NDEBUG
#define DEBUG_SELECTION 1
#endif  // NDEBUG

struct Scene
{
    static constexpr float FoV = static_cast<float>(M_PI_4);
    static constexpr float near = 50.0f;
    static constexpr float far = 5000.0f;

    // debug data
#ifndef NDEBUG
    GLenum                                  rendering_mode = GL_FILL;
#endif  // NDEBUG

    std::unique_ptr<Camera>                 cam[2];
    uint8_t                                 cur_cam = 0u;
    std::vector<Light>                      lights;

    // actors
    Primitives::HeightMap                   height_map;

#ifdef DEBUG_SELECTION
    std::vector<Primitives::Cell>           sel_cells_dbg;
    std::vector<uint8_t>                    sel_tris_dbg;
#endif  // DEBUG_SELECTION
    Primitives::Cell                        sel_cell;
    uint8_t                                 sel_tri = 2u;

    // mesh data
    std::vector<Primitives::Mesh>           meshes;
    std::vector<GL::Texture>                textures;

    // constructor
    Scene() : height_map{}
    {
    }

    void setup_camera(float width, float height);
    Camera* current_cam() { return cam[cur_cam].get(); }
    const Camera* current_cam() const { return cam[cur_cam].get(); }
};

void pick(float x, float y, Scene *scene);

#endif  // __MAIN_HPP__
