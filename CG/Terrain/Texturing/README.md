CONTROLS
========

Action                    | Key
--------------------------|------------------------------------
Pan camera                | Cursor keys (or `ALT` + Mouse move)
Circle camera             | `A`/`D` (or `CTRL` + Scroll)
Zoom In/Out               | `S`/`Z` (or Scroll)
Rotate light source       | `[`/`]`
Reset camera              | `R`
Decrease FoV (Telephoto)  | `T`
Increse FoV (Wideangle)   | `Shift` + `T`
Pick triangle on terrain  | Mouse click
Exit                      | `Esc`

DEBUG
=====

Action                    | Key
--------------------------|--------
Show wireframe            | `0`
Switch to default cam     | `F1`
Switch to Free-look cam   | `F2`
Free-look cam Rotate X    | `W`/`X`
Free-look cam Rotate Z    | `,`/`.`

SCENE
=====
The scene rendered is described as a simple SDL (scene description language) file at `./data/map.sdl`. This can be edited to alter the scene without recompiling the project.

LEARNINGS
=========

1. Wrapping a texture on more than one triangle; also generating texture coodinates on the fly.
2. Loading multiple textures via the SDL file and without rebuilding the program, the shader can use upto 10 textures without hard-coding the current texture count.
3. Pixel transfer[2] and its quirks[3]: how pixel data is uploaded on to the GPU memory (unpacking): `glTexImage2D` Internal formats is how the texture will be laid out in GL's memory, (external) format and type describe the data in the client (RAM) memory; if the format has stride/pitch unpack alignment is to be set too.
4. Used `glm::packHalf2x16` to convert two floats (texture coordinates) into halfs and pack them into a `uint32_t`.
5. Using half floats instead of floats to repesent texture coordinates and there by having aligned per-vertex data: 3 floats for position and one for texture coordintes. Sending halfs, shorts or anything via `glVertexAttribPointer` would mean they get promoted to floats[4]. For integral data the normalized parameter decides it they are convereted to [0, 1] or just their values are casted into float as-is. If one wishes to retain them as integrals then `glVertexAttribIPointer` is to be used. The size of the vertex data is defined by `glVertexAttribPointer`, not the GLSL attribute variable. One needn't represent the smaller components in the shader; the hardware will convert from smaller components to larger ones.
6. Uploading to an array of uniforms in one shot.
7. Calcuating normals for vertices[5] by vertex normal averaging[6].
8. Multi-sampling based anti-aliasing (MSAA) through `ARB_multisample`.
9. Understood texture filtering: magnification and minification; they don't change the points where the texture is pinned on to the mesh but are merely an zoom in/out of the texture image. Frank D. Luna's book gives a clear illustration comparing nearest-neighbour (point) and linear filtering.
10. Texture coordinates are simply normalized coordinates [0, 1] to avoid referring to different dimensions of various textures.
11. Addressing mode (clamp, border, wrap, mirror) is the mechanism to handle texture coordinates beyond [0, 1].
12. When multi-texturing, the blend factor may be the height of the terrain at that fragment (like here) or an alpha value encoded in one of the textures (like recipe 4.2[7]) or a separate alpha map as shown in Wikipedia's article[8] on texture splatting, a method for combining different textures. It works by applying an alphamap (also called a "weightmap" or "splat map") to the higher levels, revealing the layers underneath where the alphamap is partially or completely transparent. Alpha maps are just like layer masks in GIMP.
12. Anisotrophic filtering is to fix the distortion that occurs when the view vector and the normal of a surface (say ground) is obtuse/oblique so that it appears non-orthogonal; hence the word anistrophic. "an" for not, "iso" for same, and "tropic" from tropism, relating to direction; anisotropic filtering does not filter the same in every direction. This is the most expensive filter but may be worth the cost to cost to correct such distortions as moire.
13. For the vertex attributes, instead of having a struct in primitives to denote position, normal, texture coordinates, etc. one can also pack them inside a byte vector to upload to a buffer and then define the layout through glVertexAttribPointer like the examples in mbsoftworks.sk.
14. Triplanar texture mapping is a technique used to address the texture stretching artefact occuring when there's a steep triangle in the mesh where the texture is applied on a single plane (say XZ) and the variation in those directions are less, while variation in the other direction (say Y) is high.

### REFERENCES
0. MB Softworks tutorial 11 and 21 (multi-texturing and multi-layered terrain)
1. http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=14
2. https://www.opengl.org/wiki/Pixel_Transfer#Pixel_type
3. http://stackoverflow.com/q/27200528/183120
4. http://stackoverflow.com/a/11679024/183120
5. http://stackoverflow.com/q/6656358/183120
6. §6.2.1, Introduction to 3D Game Programming with DirectX 10, Frank D. Luna
7. OpenGL 4.0 Shading Language Cookbook, David Wolff
8. http://en.wikipedia.org/wiki/Texture_splatting
