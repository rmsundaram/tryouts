Pre-built Win32 binary available in the [Downloads section](https://bitbucket.org/rmsundaram/tryouts/downloads).

SCREENSHOT
==========
![Screen.png](https://bitbucket.org/repo/no48Gg/images/360783447-Screen.png)

CONTROLS
========

Action                    | Key
--------------------------|------------------------------------
Pick triangle on terrain  | Mouse click
Pan camera                | Cursor keys (or `ALT` + Mouse move)
Circle camera             | `A`/`D` (or `CTRL` + Scroll)
Zoom In/Out               | `S`/`Z` (or Scroll)
Rotate light source       | `[`/`]`
Reset camera              | `R`
Decrease FoV (Telephoto)  | `T`
Increase FoV (Wideangle)  | `Shift` + `T`
Exit                      | `Esc`

DEBUG
=====

Action                    | Key
--------------------------|--------
Show wireframe            | `0`
Switch to default cam     | `F1`
Switch to Free-look cam   | `F2`
Free-look cam Rotate X    | `W`/`X`
Free-look cam Rotate Z    | `,`/`.`

SCENE
=====
The scene rendered is described as a simple SDL (scene description language) file at `./data/map.sdl`. This can be edited to alter the scene without recompiling the project.

TO DO
=====

* [BUG] Avoid pruning quads intersecting with camera when `FRUSTUM_CULL` is defined
  - Zoom/Pan free-look camera to reproduce this issue
  - Test by drawing debug shapes for quads and frustum

LEARNINGS
=========

1. QuadTree construction with no pointer chasing only using simple math
2. Calculation of frustum bounding sphere and cone
3. Cone-sphere, frustum-sphere, frustum-OBB, point-OBB intersection tests
4. Frustum culling and optimizations surrounding it from FlipCode
5. Parsing QuadTree and rendering only completely or partially visible nodes
6. Octant test for intersection tests involving polytopes like OBB or frustum

### REFERENCES
1. QuadTree workout 2
2. Frustum sphere workout and flip code article cited below
3. Geometric Tools article and workouts: Circle-Trapezium Intersection, OBB-Point Test
4. http://www.flipcode.com/archives/Frustum_Culling.shtml
5. http://www.gamedev.net/page/resources/_/technical/graphics-programming-and-theory/quadtrees-r1303
6. §16.14.2, RTR, 3/e
