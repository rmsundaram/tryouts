#version 330

uniform vec4 solidColour;

// this is a solid colour shader, hence discard actual colour
in vec4 colour;

out vec4 finalColour;

void main()
{
    finalColour = solidColour;
}
