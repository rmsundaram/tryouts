#version 330

uniform mat4 PV;
uniform mat4 M;

layout (location = 0) in vec3 pos;
layout (location = 1) in float tex_X;
layout (location = 2) in float tex_Y;
layout (location = 3) in vec3 iNormal;

out vec2 tex_coords;
out float height;
out vec3 normal;

void main()
{
    gl_Position = PV * M * vec4(pos, 1.0);
    tex_coords = vec2(tex_X, tex_Y);
    height = pos.y / 255.0;
    // output normal in world space, if M had non-uniform scaling, we'd need transpose(inv(mat3(M)))
    normal = normalize(mat3(M) * iNormal);
}
