#version 330

// NOTE: since this shader only does ambient and diffuse lighting
// this could be done in the vertex shader as an optimization
struct Light
{
    vec3 direction;
    vec4 diffuse;
    vec4 ambient;
};
uniform Light lights[5];
uniform uint lights_used;

uniform sampler2D tex_unit[5];
uniform uint tex_used;

in vec2 tex_coords;
in float height;    // normalized height of the terrain at this vertex
in vec3 normal;
out vec4 colour;

// this is necessary since the sampler array index
// must be an integral constant expression till GLSL 3.30
// http://stackoverflow.com/a/12031821/183120
vec4 tex_lookup(in uint idx)
{
    vec4 colour;
    switch (idx)
    {
    case 0u:
        colour = texture(tex_unit[0], tex_coords); break;
    case 1u:
        colour = texture(tex_unit[1], tex_coords); break;
    case 2u:
        colour = texture(tex_unit[2], tex_coords); break;
    case 3u:
        colour = texture(tex_unit[3], tex_coords); break;
    case 4u:
        colour = texture(tex_unit[3], tex_coords); break;
    default:
        colour = texture(tex_unit[4], tex_coords); break;
    }
    return colour;
}

void main()
{
    // obtain texture colour
    // except the last/top-most, every texture will've one additional blended band
    uint max_bands = (2u * tex_used) - 1u;
    // choose the band this vertex falls under
    // we need n spans not n stops, hence height * (max_bands - 1) wouldn't help
    // however, when height = 1.0, idx = max_bands (while addressable would be one less)
    uint idx = uint(min(height * max_bands, max_bands - 1u));
    uint idx_half = idx / 2u;
    vec4 band_colour = tex_lookup(idx_half);
    // an odd index mean the fragment lies in a blended band
    if ((idx % 2u) != 0u)
    {
        float band_size = 1.0 / float(max_bands);
        // band_size * idx would give the height upto this band
        float height_in_band = height - (band_size * idx);
        float t = height_in_band / band_size;
        band_colour = mix(band_colour, tex_lookup(idx_half + 1u), t);
    }

    // calculate light colour - based on 3-D Computer Graphics by Samuel R. Buss
    colour = vec4(0.0f);
    for (uint i = 0u; i < lights_used; ++i)
    {
        colour += lights[i].ambient;
        // clamp NdotL to [0, 1], cos(x)'s codomain is [-1, 1], hence max would do
        float intensity = max(dot(normal, lights[i].direction), 0.0);
        colour += intensity * lights[i].diffuse;
    }
    // colour shouldn't be negative here since all we do is addition (with positive values)
    colour = min(colour, 1.0);

    // return product of texture and light colours
    colour *= band_colour;
}
