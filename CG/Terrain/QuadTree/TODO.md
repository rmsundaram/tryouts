# Overview

1. Fundamental Method 1
    * Frustum planes extraction
    * Frustum-Sphere Xsection
    * Frustum-AABB Xsection
2. Optimizations 1
    * Node fully inside - no need to test children - opposite of fully outside
    * Frustum apex inside node - consider as intersecting and test children
    * Test frustum-sphere before proceeding to frustum-AABB
3. Fundamental Method 2
    * Sphere-sphere Xsection
    * Sphere-cone Xsection
4. Optimizations 2
    * Frustum apex in AABB, if not
    * Sphere-sphere test, if yes
    * Cone-sphere test, if yes
    * Frustum-sphere test, if yes
    * Frustum-AABB test, yes if, render

# Explore

1. Grids
2. H-Grids

# To Do

1. Perform terrain picking using the quadtree instead of grid traversal.
2. Move to quaternion-based camera
3. Why did _Mathematics and Physics for Programmers, 2/e_  write-off quadtree thus?

> it is not significantly quicker than just partitioning the space into a flat grid

More reason to learn H-Grids.

4. Consider performing frustum tests in homogeneous space (post the perspective divide) so that the test are extremely simple; check for additional optimisations recommended by _Game Engine Architecture, 2/e_
5. Consider 3x4 instead of 4x4 matrices as explained in _Game Engine Architecture_.
6. Textures needn't be linearly mapped by height; they can also be mapped by other parameters like slope.  Check if this can be used here.
