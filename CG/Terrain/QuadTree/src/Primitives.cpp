#include "common.hpp"
#include "Util.hpp"
#include "GL_res_wrap.hpp"
#include "GL_util.hpp"
#include "Quadtree.hpp"
#include "Primitives.hpp"

constexpr int Primitives::Mesh::type_id[4];
constexpr unsigned Primitives::Mesh::elem_lens[4];

namespace Primitives
{

// custom implementation of the vertex normal averaging technique
// explained in §6.2.1 Introduction to 3D Game Programming with DirectX 10
void calculate_normals(Primitives::HeightMap *height_map,
                       std::vector<Primitives::PNT> &vertices)
{
    const size_t vrtx_cnt = height_map->rows * height_map->cols;

    int r = 0, c = -1;
    /*
     * Compute normal of triangle(s) formed by a given vertex
     * and add it to the normal of the constituent vertices.
     *         0  1  2  3
     *         +--+--+--+
     *         | /| /| /|
     *         +--+--+--+
     *         4  5  6  7
     * Triangles are formed from 4 onwards. Vertices 5 and 6 would
     * form 2 triangles while 4 and 7 complete only one.
     */
    // skip the first row since those points don't form any triangles
    for (auto t = height_map->cols; t < vrtx_cnt; ++t)
    {
        c = (c + 1) % height_map->cols;     // horizontal x-axis
        r = t / height_map->cols;           // vertical y-axis
        auto &me = vertices[t];
        const auto north_idx = height_map->cols * (r - 1) + c;
        auto &north = vertices[north_idx];

        // points 4 to 6 form such triangles
        if (c != static_cast<signed>(height_map->cols) - 1)
        {
            auto &north_east = vertices[north_idx + 1];
            const auto n1 = glm::triangleNormal(me.pos, north_east.pos, north.pos);
            me.normal += n1;
            north_east.normal += n1;
            north.normal += n1;
        }

        // points 5 to 7 form such triangles; note this shouldn't be an else if
        if (c != 0)
        {
            auto &west = vertices[t - 1];
            const auto n2 = glm::triangleNormal(me.pos, north.pos, west.pos);
            me.normal += n2;
            north.normal += n2;
            west.normal += n2;
        }
    }
}

// this variant doesn't work in terrain space but the underlying heightmap (grid) space
float HeightMap::height_at(size_t i, size_t j) const
{
    // 3 x 3 cells implies i, j can vary between [0, 3]
    assert((i <= rows) && (j <= cols));
    return heights[i * cols + j];
}

// this variant takes in x, z locations
float HeightMap::height_at(float x, float z) const
{
    constexpr float eps = 10.0E-6f;
    // the calculation here depends on how the cells were tesselated when generating the terrain mesh;
    // the origin of the cell differs when it's |/| as opposed to |\|; currently it is the former
    const auto idx_x = x_to_index(x);
    const auto idx_z = z_to_index(z);

    // on a height map of size 4 x 5, when (idx_x, idx_z) = (4, 0.885162175) i.e. the height at right-most or
    // bottom-most is requested this would lead to a height lookup at (0, 4), (0, 5), (1, 4), (1, 5);
    // the 2nd and 3rd are out of bounds request, need to cap it for such cases
    const auto left_real = std::fmin(floor_if_not_int(idx_x, eps), static_cast<float>(cols - 2));
    const auto top_real = std::fmin(floor_if_not_int(idx_z, eps), static_cast<float>(rows - 2));
    const auto left = static_cast<size_t>(left_real);
    const auto top = static_cast<size_t>(top_real);
    const auto right = left + 1u;
    const auto bottom = top + 1u;
    const auto alpha = idx_x - left_real;
    const auto beta = idx_z - top_real;
    // using barycentric coordinates (convex combination) to find if the point is in the upper or lower triangle
    // a point is on a triangle if p = o + αS + βT such that α + β ≤ 1 and both α, β ∈ [0, 1]
    // taking the upper left point as the origin, if α + β ≤ 1 implies upper triangle
    // Ref: §19.5 in Introduction to 3D Game Programming with DirectX 11 by Frank D. Luna
    if ((alpha + beta) <= 1.0f)
    {
        return (1.0f - alpha - beta) * height_at(top, left) +
                               alpha * height_at(top, right) +
                                beta * height_at(bottom, left);
    }
    else    // invert alpha, beta and gamma for the lower triangle's barycentric coordinates
    {
        return (alpha + beta - 1.0f) * height_at(bottom, right) +  // 1 - (1 - α) - (1 - β)
                      (1.0f - alpha) * height_at(bottom, left) +
                       (1.0f - beta) * height_at(top, right);
    }
}

void HeightMap::get_triangles(const Cell &cell,
                              Triangle *t1,
                              Triangle *t2) const
{
    const auto l_x = index_to_x(static_cast<float>(cell.x));
    const auto b_z = index_to_z(static_cast<float>(cell.y + 1u));
    const auto lb_y = height_at(static_cast<size_t>(cell.y + 1u),
                                static_cast<size_t>(cell.x));
    const auto r_x = index_to_x(static_cast<float>(cell.x + 1u));
    const auto t_z = index_to_z(static_cast<float>(cell.y));
    const auto rt_y = height_at(static_cast<size_t>(cell.y),
                                static_cast<size_t>(cell.x +1u));

    t1->verts[0] = t2->verts[0] = {l_x, lb_y, b_z};
    t1->verts[1] = t2->verts[2] = {r_x, rt_y, t_z};
    t1->verts[2] = {l_x, height_at(static_cast<size_t>(cell.y), static_cast<size_t>(cell.x)), t_z};
    t2->verts[1] = {r_x, height_at(static_cast<size_t>(cell.y + 1u), static_cast<size_t>(cell.x + 1u)), b_z};
}

/*
 * these functions mapping between position and index transform position values into
 * indices of a unit grid that has origin at left-top-down, x goes to right, y goes up
 * and z goes down; when viewed top down along y axis, x and z form a 2D system useful
 * for grid ray tracing which is similar to the screen coordinate system used in most
 * operating systems, where x goes right, and y (earlier z, now y) goes down.
 */
float HeightMap::x_to_index(float pos) const
{
    const auto ret = (pos / cell_size.x) + (0.5f * static_cast<float>(cols - 1));
    return glm::clamp(ret, 0.0f, static_cast<float>(cols));
}

float HeightMap::y_to_index(float pos) const
{
    // y starts from 0 and goes upwards, no negative values to offset the origin
    return pos / cell_size.y;
}

float HeightMap::z_to_index(float pos) const
{
    const auto ret = (pos / cell_size.z) + (0.5f * static_cast<float>(rows - 1));
    return glm::clamp(ret, 0.0f, static_cast<float>(rows));
}

float HeightMap::index_to_x(float index) const
{
    return cell_size.x * (index - (0.5f * static_cast<float>(cols - 1)));
}

float HeightMap::index_to_y(float index) const
{
    return cell_size.y * index;
}

float HeightMap::index_to_z(float index) const
{
    return cell_size.z * (index - (0.5f * static_cast<float>(rows - 1)));
}

size_t HeightMap::chunk_count() const
{
    return cols / chunk_cells;
}

Primitives::HeightMap::ChunkType HeightMap::chunk_type(size_t col, size_t row) const
{
    const auto chunks = chunk_count();
    assert(col < chunks && row < chunks);

    if (partitions.dummy_row_col)
    {
        if ((col == (chunks - 1)) && (row == (chunks - 1)))
            return ChunkType::Dummy_Corner;
        if (col == (chunks - 1))
            return ChunkType::Dummy_Col;
        if (row == (chunks - 1))
            return ChunkType::Dummy_Row;
    }

    return ChunkType::Normal;
}

X_state sphere_sphere_intersect(const Sphere &A, const Sphere &B)
{
    // §16.13.1, RTR 3/e
    const auto c_diff2 = glm::length2(A.centre - B.centre);
    const auto r_sum = A.radius + B.radius;
    const auto r_sum2 = r_sum * r_sum;
    if (c_diff2 > r_sum2)
        return X_state::Disjoint;

    // intersecting vs within part is not from any literature; deduced by self
    // refer the circle containment workout
    if (A.radius > B.radius)
        return X_state::Intersecting;
    // fully contained if (A.radius + ‖A.centre - B.centre‖) <= B.radius
    // but the √ in glm::length may be avoided thus: a + b ≤ c
    // a ≤ c - b
    // c - a ≥ b    i.e. distance between the centres ≤ difference in radius
    // squaring both sides shouldn't affect the result since both are positive numbers
    // we've already verified that c >= a, so c - a should be positive
    const auto r_diff = B.radius - A.radius;
    const auto r_diff2 = r_diff * r_diff;
    return (r_diff2 >= c_diff2) ? X_state::Within : X_state::Intersecting;
}

// §5.2.1 Intersection of a line and a plane - from Lengyel's skeleton book
bool ray_plane_intersect(const Ray &ray,
                         const Plane &plane,
                         float *t)
{
    const auto denom = glm::dot(glm::vec4(ray.direction, 0.0f),
                                plane.ABCD);
    // ray is parallel to plane
    if(glm::epsilonEqual(denom,
                         0.0f,
                         glm::epsilon<float>()))
        return false;
    const auto num = glm::dot(glm::vec4(ray.origin, 1.0f),
                              plane.ABCD);
    const float dist = -num/denom;
    if(dist < 0.0f)    // plane behind ray case
        return false;
    *t = dist;
    return true;
}

// Real-Time Rendering, 3rd edition with errata taken into account
bool ray_triangle_intersect(const Ray &ray,
                            const Triangle &tri,
                            float *t)
{
    const auto e0 = tri.verts[1] - tri.verts[0];
    const auto e1 = tri.verts[2] - tri.verts[0];

    const auto q = glm::cross(ray.direction, e1);
    const auto a = glm::dot(e0, q);
    if (glm::epsilonEqual(a, 0.0f, glm::epsilon<float>()))
    {
        return false;
    }
    const auto f = 1.0f / a;
    const auto s = ray.origin - tri.verts[0];
    const auto u = f * glm::dot(s, q);
    if (u < 0.0f)
        return false;
    const auto r = glm::cross(s, e0);
    const auto v = f * glm::dot(ray.direction, r);
    if ((v < 0.0f) || ((u + v) > 1.0f))
        return false;
    *t = f * glm::dot(e1, r);
    return true;
}

// Real-Time Rendering, 3rd Edition ~ Kay-Kajiya's slabs method
bool ray_obb_intersect(const Ray &ray,
                       const OBB &box,
                       float *t1,
                       float *t2)
{
    constexpr float eps_parallel = 10.0E-20f;
    constexpr float eps = 10.0E-6f;
    constexpr auto n_dimensions = 3u;

    float farthest_entry = -std::numeric_limits<float>::max();
    float nearest_exit   =  std::numeric_limits<float>::max();
    const auto p = box.centre - ray.origin;
    for (auto i = 0u; i < n_dimensions; ++i)
    {
        const auto e = glm::dot(box.axes[i], p);
        const auto f = glm::dot(box.axes[i], ray.direction);

        // ray not parallel to slab planes
        if (std::abs(f) > eps_parallel)
        {
            const auto reciprocal_f = 1.0f / f;
            // calculate entry and exit
            const auto entry = (e + box.half_extent[i]) * reciprocal_f;
            const auto exit = (e - box.half_extent[i]) * reciprocal_f;
            if (exit > entry)
            {
                if (exit < nearest_exit) nearest_exit = exit;
                if (entry > farthest_entry) farthest_entry = entry;
            }
            else
            {
                if (entry < nearest_exit) nearest_exit = entry;
                if (exit > farthest_entry) farthest_entry = exit;
            }
            if ((farthest_entry > nearest_exit) || (nearest_exit < 0.0f))
                return false;
        }
        else if (((-box.half_extent[i] - e) > 0.0f) || ((box.half_extent[i] - e) < 0.0f))
            return false;
    }
    // if farthest entry > 0 then ray originates outside the box
    // if farthest entry = 0 then ray originates on the box's surface
    // else originates inside the box
    if (less(farthest_entry, 0.0f, eps))
        *t1 =  nearest_exit;
    else
    {
        *t1 = farthest_entry;
        if (t2)
            *t2 = nearest_exit;
    }
    return true;
}

glm::vec3 get_point(const Ray &ray, float t)
{
    return {std::fma(t, ray.direction.x, ray.origin.x),
            std::fma(t, ray.direction.y, ray.origin.y),
            std::fma(t, ray.direction.z, ray.origin.z)};
}

bool is_degenerate(const Ray &ray, float epsilon)
{
    return glm::epsilonEqual(ray.direction.x, 0.0f, epsilon) &&
           glm::epsilonEqual(ray.direction.y, 0.0f, epsilon) &&
           glm::epsilonEqual(ray.direction.z, 0.0f, epsilon);
}

// Based on Skeleton book but effective radius calculation from RTR 3/e
X_state OBB_plane_intersect(const OBB &b, const Plane &p)
{
    const glm::vec3 n{p.ABCD};
    const float r_eff = b.half_extent.x * std::abs(glm::dot(b.axes[0], n)) +
                        b.half_extent.y * std::abs(glm::dot(b.axes[1], n)) +
                        b.half_extent.z * std::abs(glm::dot(b.axes[2], n));
    const float dist = glm::dot(glm::vec4(b.centre, 1.0f), p.ABCD);
    if (dist <= -r_eff)
        return X_state::Disjoint;
    else if (dist >= r_eff)
        return X_state::Within;
    return X_state::Intersecting;
}

X_state frustum_OBB_intersect(const Frustum &f, const OBB &b, glm::uint32 planes)
{
    bool intersecting = false;
    glm::ivec4 p = glm::unpackI3x10_1x2(planes);
    for (auto i = 0u; i < 3; ++i)
    {
        const auto res = OBB_plane_intersect(b, f.p[p[i]]);
        if (res == X_state::Disjoint)
            return res;
        else if (res == X_state::Intersecting)
            intersecting = true;
    }
    return (intersecting ? X_state::Intersecting : X_state::Within);
}

X_state frustum_sphere_intersect(const Frustum &f, const Sphere &s, glm::uint32 planes)
{
    bool intersecting = false;
    glm::ivec4 p = glm::unpackI3x10_1x2(planes);
    for (auto i = 0u; i < 3u; ++i)
    {
        auto const res = sphere_plane_intersect(s, f.p[p[i]]);
        // early return if out of any line
        if (res == X_state::Disjoint)
            return res;
        else if (res == X_state::Intersecting)
            intersecting = true;
    }
    return (intersecting ? X_state::Intersecting : X_state::Within);
}

X_state sphere_plane_intersect(const Sphere &s, const Plane &p)
{
    float dist = point_plane_dist(glm::vec4(s.centre, 1.0f), p);
    if (dist <= -s.radius)
        return X_state::Disjoint;
    else if (dist >= s.radius)
        return X_state::Within;
    return X_state::Intersecting;
}

float point_plane_dist(const glm::vec4 &pt, const Plane &p)
{
    return glm::dot(pt, p.ABCD);
}

bool point_OBB_test(const glm::vec3 &pt, const OBB &b)
{
    const auto p = pt - b.centre;
    const glm::vec3 d {
                        glm::dot(p, glm::column(b.axes, 0)),
                        glm::dot(p, glm::column(b.axes, 1)),
                        glm::dot(p, glm::column(b.axes, 2))
                      };
    return glm::all(glm::lessThanEqual(glm::abs(d), b.half_extent));
}

void view_to_frustum_space(glm::vec3 &pt, const Frustum &f)
{
    pt.z += f.neg_centre_z;
}

glm::uint32 point_octant_planes(const glm::vec3 &pt)
{
    glm::ivec4 planes;
    planes.x = (pt.x < 0.0f) ? 0u : 1u;
    planes.y = (pt.y < 0.0f) ? 2u : 3u;
    planes.z = (pt.z < 0.0f) ? 5u : 4u;
    return glm::packI3x10_1x2(planes);
}

OBB aabb_to_obb(const glm::vec3 &min, const glm::vec3 &max)
{
    const auto box_extent = max - min;
    const auto half_extent = 0.5f * box_extent;
    return {min + half_extent, half_extent, glm::mat3(1.0)};
}

Ray clip_ray(const Ray &ray,
             const glm::vec3 &min,
             const glm::vec3 &max)
{
    const auto clip_box = aabb_to_obb(min, max);
    float t1 = std::numeric_limits<float>::infinity(), t2 = std::numeric_limits<float>::infinity();
    if (ray_obb_intersect(ray, clip_box, &t1, &t2))
    {
        const auto p1 = get_point(ray, t1);
        // ray originates outside the box
        if (t2 != std::numeric_limits<float>::infinity())
        {
            const auto p2 = get_point(ray, t2);
            return {p1, p2 - p1};
        }
        else
        {
            return {ray.origin, p1 - ray.origin};
        }
    }
    // no intersection, return a degenerate ray
    return {};
}

// here left and top are the lesser values, similar to most screen coordinate systems
glm::vec4 left_top_right_bottom(glm::vec2 min, glm::vec2 max)
{
    if (min.x > max.x)
        std::swap(min.x, max.x);
    if (min.y > max.y)
        std::swap(min.y, max.y);
    return {min.x, min.y, max.x, max.y};
}

void transform_frustum(const Frustum &f,
                       const glm::mat4 &xform,
                       const glm::mat4 *normal_xform,
                       Frustum &xform_f)
{
    // use normal transform if already passed in else calculate from point transform
    // to transform a plane by M we need (M⁻¹)ᵀ, see §5.2.3 in Skeleton book
    const glm::mat4 &n_xform = normal_xform ? *normal_xform : glm::transpose(glm::affineInverse(xform));
    for (auto i = 0u; i < f.p.size(); ++i)
    {
        xform_f.p[i].ABCD = n_xform * f.p[i].ABCD;
    }
    xform_f.s.centre = glm::vec3(xform * glm::vec4(f.s.centre, 1.0f));
    // NOTE: radius not transformed since the (view) xform is assumed to do a uniform scaling of 1 unit
    // so is the case with c.dir, where renormalization is ideally required.
    // Cone::angle and friends are untransformed too
    xform_f.c.dir = glm::vec3(xform * glm::vec4(f.c.dir, 0.0f));
    xform_f.c.apex = glm::vec3(xform * glm::vec4(f.c.apex, 1.0f));

    // copy remaining params as-is
    xform_f.s.radius = f.s.radius;
    xform_f.c.reciSin = f.c.reciSin;
    xform_f.c.sin2 = f.c.sin2;
    xform_f.neg_centre_z = f.neg_centre_z;
}

// sphere cone intersection from Geometric Tools by Dave Eberly
bool cone_sphere_intersect(const Cone &c, const Sphere &s)
{
    // compute the apex of the extended cone C'' which encompasses the spherical
    // sector formed by offseting the original cone C with sphere's radius
    const auto U = c.apex - ((s.radius * c.reciSin) * c.dir);
    auto D = s.centre - U;
    auto D2 = glm::dot(D, D);
    auto e = glm::dot(D, c.dir);
    const auto cos2 = 1.0f - c.sin2;
    if ((e > 0) && ((e * e) >= (D2 * cos2)))
    {
        // centre is inside C''
        // do the same check as above for C now i.e. use apex instead of U
        D = s.centre - c.apex;
        D2 = glm::dot(D, D);
        e = -glm::dot(D, c.dir);
        // centre inside C'' and C' (the complementary cone)
        if ((e > 0) && ((e * e) >= (D2 * c.sin2)))
            return D2 <= (s.radius * s.radius);
        // centre inside C'' but outside C'
        else
            return true;
    }
    return false;
}

}  // Primitives
