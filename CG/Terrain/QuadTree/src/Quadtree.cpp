#include "common.hpp"
#include "Util.hpp"
#include "Quadtree.hpp"
#include "Primitives.hpp"

namespace
{

void split_space(std::vector<Primitives::AABB_2D> &bounds, uint16_t nodes)
{
    enum class DIRECTION : uint8_t
    {
        NORTH_WEST = 0,
        NORTH_EAST,
        SOUTH_WEST,
        SOUTH_EAST
    };

    for (std::vector<Primitives::AABB_2D>::size_type i = 1u; i < nodes; ++i)
    {
        auto const parent = (i - 1u) / 4u;
        auto const first_child = (parent * 4u) + 1u;
        auto const child_quad = static_cast<DIRECTION>(i - first_child);
        auto const half_extent = 0.5f * bounds[parent].half_extent;
        switch (child_quad)
        {
            // although vec2 calls it as (x, y) in 3D it's (x, z) since
            // x goes right, y goes up and z goes out
            // hence north would mean -y and west would mean -x
        case DIRECTION::NORTH_WEST:
            bounds.push_back({{bounds[parent].centre.x - half_extent,
                               bounds[parent].centre.y - half_extent}, half_extent});
            break;
        case DIRECTION::NORTH_EAST:
            bounds.push_back({{bounds[parent].centre.x + half_extent,
                               bounds[parent].centre.y - half_extent}, half_extent});
            break;
        case DIRECTION::SOUTH_WEST:
            bounds.push_back({{bounds[parent].centre.x - half_extent,
                               bounds[parent].centre.y + half_extent}, half_extent});
            break;
        case DIRECTION::SOUTH_EAST:
            bounds.push_back({{bounds[parent].centre.x + half_extent,
                               bounds[parent].centre.y + half_extent}, half_extent});
            break;
        }
    }
}

uint16_t total_nodes(uint8_t depth)
{
    // sum of geometric series, a = 1, r = 4
    // a (rⁿ− 1) / (r − 1)
    return static_cast<uint16_t>((ipow(4, depth) - 1) / 3);
}

float get_radius(const std::vector<float> &radii, uint16_t node)
{
    const auto node_depth = (ulog2(3u * node + 1u) / 2u); // same formula as max_depth
    assert(node_depth < radii.size());
    return radii[node_depth];
}

Primitives::Sphere aabb2d_to_sphere(const Primitives::AABB_2D &b, float half_height, float radius)
{
    return {{b.centre.x, half_height, b.centre.y}, std::max(radius, half_height)};
}

Primitives::OBB aabb2d_to_OBB(const Primitives::AABB_2D &aabb, float half_height)
{
    return { {aabb.centre.x, half_height, aabb.centre.y},
             { aabb.half_extent, half_height, aabb.half_extent },
             glm::mat3(1.0f) };
}

std::vector<Primitives::Sphere> compute_bounding_spheres(const std::vector<Primitives::AABB_2D> &nodes,
                                                         const std::vector<float> &radii,
                                                         float half_height)
{
    std::vector<Primitives::Sphere> s;
    s.reserve(nodes.size());
    for (uint8_t i = 0u; i < nodes.size(); ++i)
    {
        s.push_back(aabb2d_to_sphere(nodes[i], half_height, get_radius(radii, i)));
    }
    return s;
}

std::vector<Primitives::OBB> compute_OBBs(const std::vector<Primitives::AABB_2D> &nodes, float half_height)
{
    std::vector<Primitives::OBB> b;
    b.reserve(nodes.size());
    for (auto i = 0u; i < nodes.size(); ++i)
    {
        b.push_back(aabb2d_to_OBB(nodes[i], half_height));
    }
    return b;
}

std::vector<float> calculate_radii(uint8_t count,
                                      float half_extent,
                                      float half_height)
{
    std::vector<float> radii;
    radii.reserve(count);
    half_extent *= 2.0f;
    for (uint8_t i = 0u; i < count; ++i)
    {
        half_extent /= 2.0f;
        const float he = std::max(half_extent, half_height);
        // length = √(x² + y² + z²) = √(3x²) = x√3  (∵ x = y = z)
        radii.push_back(he * glm::root_three<float>());
    }
    return radii;
}

}  // unnamed namespace

QuadTree make_quadtree(uint16_t total_cells, uint16_t leaf_cells, float cell_size, float height)
{
    assert((height > 0.0f) && (cell_size > 0.0f) && (total_cells > leaf_cells));
    const bool dummy = (total_cells % 2u == 0);
    if (dummy)
        total_cells = static_cast<uint16_t>(total_cells + 1u);
    const unsigned cells = total_cells - 1;
    assert(cells % 4 == 0);
    const unsigned leaves = total_cells / leaf_cells;     // compute L, leaf count in 1D
    // 1 for the root, then log₄L², the squaring is to make the count in 2D
    const uint8_t depth = static_cast<uint8_t>(1u + ulog2(leaves));
    const uint16_t n_nodes = total_nodes(depth);

    const float half_extent = static_cast<float>(cells / 2u) * cell_size;
    std::vector<Primitives::AABB_2D> nodes;
    nodes.reserve(n_nodes);
    // world is centred at (0, 0, 0)
    nodes.push_back({ { 0.0f, 0.0f }, half_extent });
    split_space(nodes, n_nodes);

    std::vector<float> radii;
    const auto half_height = height * 0.5f;
    radii = calculate_radii(depth, half_extent, half_height);
    std::vector<Primitives::Sphere> spheres = compute_bounding_spheres(nodes, radii, half_height);
    std::vector<Primitives::OBB> OBBs = compute_OBBs(nodes, half_height);
    return {dummy, std::move(nodes), std::move(spheres), std::move(OBBs)};
}

uint8_t QuadTree::max_depth() const
{
    // from the same formula used in total_nodes
    const auto depth = static_cast<uint8_t>(ulog2(3u * nodes.size() + 1u) / 2u);
    assert(depth >= 1u);
    return depth;
}

uint16_t QuadTree::child_offset(uint16_t node) const
{
    // leaves don't have children, make sure this isn't a leaf
    assert(is_descendant(node, static_cast<uint8_t>(max_depth() - 1u)) == false);
    return static_cast<uint16_t>(4u * node + 1u);
    // each node before this would've 4 children + 1 to go the actual child
    // e.g. node = 3, nodes before 3 = {0, 1, 2}, total 3 nodes * 4 = 12 children
    // starting at 1 ending at 12; adding 1 gives the start offset of 3's children
}

uint16_t QuadTree::offset(uint8_t depth) const
{
    assert(depth > 0u && depth <= max_depth());
    // total nodes in a tree with one depth lesser would be the beginning offset of given depth
    const auto offset = total_nodes(static_cast<uint8_t>(depth - 1u));
    assert(offset < nodes.size());
    return offset;
}

uint16_t QuadTree::descendant_leaf_offset(uint16_t node, uint16_t *offset) const
{
    // 4x + 1 gives the child offset of a node (see child_offset function)
    // but this child may have a child in turn, thus we need the final child offset
    // in this chain. f¹(x) = 4x + 1, f²(x) = 4(4x + 1) + 1, f³(x) = 4(4(4x + 1) + 1) + 1
    // fⁿ(x) = 4ⁿx + ((4ⁿ− 1) / 3), where n is max_depth - node depth = subtree depth - 1
    const auto node_depth = (ulog2(3u * node + 1u) / 2u) + 1u; // same formula as max_depth with 1 added to the result
    const uint16_t pow = static_cast<uint16_t>(ipow(4, static_cast<uint8_t>(max_depth() - node_depth))); // 4ⁿ
    *offset = static_cast<uint16_t>((pow * node) + ((pow - 1u) / 3u));
    return pow; // is also the leaf count of the subtree (see constructor for explanation)
}

bool QuadTree::is_descendant(uint16_t node, uint8_t depth) const
{
    assert(depth > 0 && depth <= max_depth() && node < nodes.size());
    return node >= offset(static_cast<uint8_t>(depth + 1u));
}

glm::uvec2 QuadTree::index_2D(uint16_t node, uint8_t depth) const
{
    // node 1 at depth 3 would be node 5 at the tree level, map 5 to 1
    node = static_cast<uint16_t>(node - offset(depth) + 1u);      // translate it to an index starting from 1 for this depth
    std::bitset<4> const off_x{10};  // { 0, 1, 0, 1 }
    std::bitset<4> const off_y{12};  // { 0, 0, 1, 1 }
    uint16_t x = 0u, y = 0u;
    uint16_t c = 1u;          // number of items at depth in 1D, increasing powers of 2
    while (depth-- > 1u)      // depth = 1 would just be root
    {
        // rem = index within quad at this depth
        // quot = parent's index at depth - 1
        auto const d = std::div(node - 1, 4);
        x = static_cast<uint16_t>(x + off_x[d.rem] * c);
        y = static_cast<uint16_t>(y + off_y[d.rem] * c);
        // update for next iteration
        c = static_cast<uint16_t>(c * 2u);
        node = static_cast<uint16_t>(d.quot + 1u);
    }
    return {x, y};
}
