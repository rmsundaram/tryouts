#include "common.hpp"
#include "Util.hpp"
#include "Quadtree.hpp"
#include "Primitives.hpp"
#include "Camera.hpp"

Camera::Camera(float FoV,
               float width,
               float height,
               float near,
               float far)
    : view_xform{glm::mat4(1.0)}
    , view_world_xform{glm::mat4(1.0)}
    , FoV{FoV}
    , width{width}
    , height{height}
    , aspect_ratio{width / height}
    , near{near}
    , far{far}
    , projection(glm::perspective(FoV, aspect_ratio, near, far))
    , frustum_changed{true}
{
    calc_frustum_params();
}

void Camera::change_FoV(float angle)
{
    FoV += angle;
    projection = glm::perspective(FoV, aspect_ratio, near, far);

    // recompute frustum
    calc_frustum_params();
    frustum_changed = true;
}

void Camera::update_dependants()
{
    /*
     * we've Mw->v, we need its inverse Mv->w; since Mw->v = TR ⇒ Mv->w = R⁻¹ T⁻¹; glm::affineInverse does exactly
     * this. Transposing the upper 3x3 matrix in Mw->v would give view frame's orientation in world space; we need the
     * last column (the inverted rotated translation; see §4.4.1 of skeleton book) for view frame's origin in world
     * space i.e. camera location in world. Although named affineInverse, it doesn't truly compute the inverse of the
     * upper 3×3 matrix which may be any linear transform, it assumes that this part is a rotation and simply transposes
     * it; perhaps rigidTransformInverse is a better name.
     */
    view_world_xform = glm::affineInverse(view_xform);
    frustum_changed = true;
}

/*
 * This function shouldn't be called multiple times (only during initialization and
 * FoV change - which is pretty rare). Otherwise the move/rotate functions are called
 * more often but they don't affect the frustum's shape and in view space it continues
 * to be at the origin with the same orientation, but in world space will it be
 * positioned and oriented differently, this is tackled using the view matrix
 */
void Camera::calc_frustum_params()
{
    const auto FoV_2 = 0.5f * FoV;            // ½FoVy
    d = static_cast<float(*)(float)>(glm::cot)(FoV_2);
    sqrt_d2_a2 = std::sqrt((d * d) + (aspect_ratio * aspect_ratio));
    sqrt_d2_1 = std::sqrt((d * d) + 1.0f);

    // based on Frustum Planes workout (originally based on Skeleton book)
    f.p[0].ABCD = glm::vec4{ d / sqrt_d2_a2,          0.0f,-aspect_ratio / sqrt_d2_a2, 0.0f};
    f.p[1].ABCD = glm::vec4{-d / sqrt_d2_a2,          0.0f,-aspect_ratio / sqrt_d2_a2, 0.0f};
    f.p[2].ABCD = glm::vec4{           0.0f, d / sqrt_d2_1,         -1.0f / sqrt_d2_1, 0.0f};
    f.p[3].ABCD = glm::vec4{           0.0f,-d / sqrt_d2_1,         -1.0f / sqrt_d2_1, 0.0f};
    f.p[4].ABCD = glm::vec4{           0.0f,          0.0f,                     -1.0f,-near};
    f.p[5].ABCD = glm::vec4{           0.0f,          0.0f,                      1.0f,  far};

    f.neg_centre_z = (near + far) * 0.5f;

    // compute the frustum bounding cone - www.flipcode.com/archives/Frustum_Culling.shtml
    // “The only consideration with the cone is the calculation of the cone angle. If we choose the cone
    // angle to be the same as the field of view of the frustum, then we create a cone that cuts off the
    // corners of the frustum. Picture the biggest circle that fits inside a box. So we need to create the
    // cone angle such that it encompasses the corners of the frustum instead of just the sides.”
    // Imagine the triangle formed by eye, view plane centre and view window right top corner; then angle
    // between the view direction and the vector from eye to the corner would give the cone angle encompassing
    // the corner. The corner is obtained by Pythagoras; the offset to top and right on the view plane from its
    // origin are 1.0 and aspect ratio respectively (see §6.5.3 Perspective Projection in Essential Math)
    const float distCentre2RTCorner = glm::sqrt(1.0f + (aspect_ratio * aspect_ratio));
    // using atan instead of atan2 since we're sure that angle will be within (-90, 90)
    // and the angle should be positive since both aspect_ratio and d are positive
    const auto angle = std::atan(distCentre2RTCorner / d);   // compute the diagonal FoV
    f.c.sin2 = sinf(angle);
    f.c.reciSin = 1.0f / f.c.sin2;
    f.c.sin2 *= f.c.sin2;

    calc_bounding_sphere(d);
}

void Camera::calc_bounding_sphere(float d)
{
    // from the frustum sphere construction workout
    const auto d2 = d * d;
    const auto inv_d2 = 1.0f / d2;
    const auto t = -0.5f * inv_d2 * ((1.0f + d2 + (aspect_ratio * aspect_ratio)) * (near + far));
    assert(t < 0.0f);
    // based on similar triangular prism ratios
    // the three vertices of prism 1: view origin/eye (0, 0, 0), (0, 1, -d), (ar, 0, -d)
    // the three vertices of prism 2: eye (0, 0, 0), (0, far_top, -far), (far_right, 0, -far)
    const auto far_top = far / d;
    const glm::vec3 far_right_top{far_top * aspect_ratio, far_top, -far};
    // if |t| < |f|, then pick the calculated t, else pick -far
    f.s.centre.z = (-t <= far) ? t : -far; // this assumes that t will turn out negative
    f.s.radius = glm::length(far_right_top - f.s.centre);
}

void Camera::world_frustum(Primitives::Frustum *fstum) const
{
    // we need planes in camera space to be mapped to world space; we've Mworld→view and need
    // Mview→world = (Mworld→view)⁻¹; however to transform a plane by M we need (M⁻¹)ᵀ, see §5.2.3
    // in Skeleton book. Thus we need ((view_xform⁻¹)⁻¹)ᵀ
    const auto n_xform = glm::transpose(view_xform);
    Primitives::transform_frustum(f, view_world_xform, &n_xform, *fstum);
}

bool Camera::frustum(Primitives::Frustum *fstum, bool skip_if_same) const
{
    assert(fstum);
    if (!skip_if_same || frustum_changed)
    {
        world_frustum(fstum);
        frustum_changed = false;
        return true;
    }
    return false;
}

FreeCamera::FreeCamera(float FoV,
                       float width,
                       float height,
                       float near,
                       float far) :
    Camera(FoV, width, height, near, far)
{
    // position camera to initial view
    reset();
}

void FreeCamera::reset()
{
    view_xform = glm::mat4{      1.0f,          0.0f,         0.0f, 0.0f,   // col 1
                                 0.0f,  0.619263887f, 0.785196841f, 0.0f,   // col 2
                                 0.0f, -0.785196841f, 0.619263887f, 0.0f,   // col 3
                           18.962925f,   1.83618283f,  -51.548912f, 1.0f};  // col 4
    //view_xform = glm::lookAt(glm::vec3(0, 60, 30), glm::vec3(0, 0, 0), glm::vec3(0.0f, 1.0f, 0.0f));
    update_dependants();
}

void FreeCamera::slide_X(float delta)
{
    view_xform[3][0] -= delta;
    update_dependants();
}

void FreeCamera::slide_Y(float delta)
{
    view_xform[3][1] -= delta;
    update_dependants();
}

void FreeCamera::slide_Z(float delta)
{
    view_xform[3][2] -= delta;
    update_dependants();
}

void FreeCamera::rotate_X(float angle)
{
    const auto inv = glm::eulerAngleX(-angle);
    view_xform = inv * view_xform;
    update_dependants();
}

void FreeCamera::rotate_Y(float angle)
{
    const auto inv = glm::eulerAngleY(-angle);
    view_xform = inv *view_xform;
    update_dependants();
}

void FreeCamera::rotate_Z(float angle)
{
    const auto inv = glm::eulerAngleZ(-angle);
    view_xform = inv * view_xform;
    update_dependants();
}

RTSCamera::RTSCamera(float FoV,
                     float width,
                     float height,
                     float near,
                     float far,
                     float min_ht)
    : Camera{FoV, width, height, near, far}
    , min_height{min_ht}
{
    // position camera to initial view
    reset();
}

void RTSCamera::reset()
{
    // instead of directly using eye_x, eye_y, eye_z these're constexprs are put in
    // an array since directly passing them to glm::vec3::vec3, which takes them by reference,
    // leads to undefined reference error to define these static variables in a cpp
    constexpr float eye[3] = { eye_x, eye_y, eye_z };
    constexpr float look[3] = { look_x, look_y, look_z };
    view_xform = glm::lookAt(glm::vec3{eye[0], eye[1], eye[2]},
                             glm::vec3{look[0], look[1], look[2]},
                             glm::vec3{0.0f, 1.0f, 0.0f});
    ground_dist = std::sqrt((eye_x * eye_x) + (eye_y * eye_y) + (eye_z * eye_z));
    update_dependants();
}

void RTSCamera::slide_X(float delta)
{
    view_xform[3][0] -= delta;
    update_dependants();
}

void RTSCamera::slide_Y(float delta)
{
    // get view's Y-axis in terms of world space
    const auto &along = glm::row(view_xform, 1);
    // project it on XZ plane, normalize and scale it accordingly
    const auto pan_vert = -delta * glm::normalize(glm::vec3{along[0], 0.0f, along[2]});
    /*
     * We need Mw->v' = Rev(Tv'->w) = Mv->v' Mw->v. So the missing piece is Mv->v' or
     * Tv'->v. Since we know the required transformation in world units and finding it in
     * view units is more work, instead of pre-multiplying multiplying Mv->v',  we keep
     * v the same but change w to w' instead i.e. move the world. This works since moving
     * the camera north works out the same as moving the world south. Now we need
     * Mw'->v = Mw->v Mw'->w i.e. post-multiply Tw->w'. In the free-look camera workout,
     * we took the other route of pre-multiplying Mv->v' since it was convenient to
     * measure the units of transformation in terms of the view units than in world units.
     */
    view_xform = glm::translate(view_xform, pan_vert);
    update_dependants();
}

void RTSCamera::slide_Z(float delta)
{
    // libc++ doesn’t have math functions constexpr unlike libstdc++; not use about MSVC
    // standard doesn’t mandate them to be constexprs though
    // https://stackoverflow.com/a/25892335
#if defined(__GLIBCXX__) || defined(__GLIBCPP__)
    constexpr float angle = std::atan2(eye_z - look_z, eye_y - look_y);
    constexpr float cosine = std::cos(angle);
    static_assert(cosine > 0.0f, "Invalid angle between view dir and ground!");
#else
    const float angle = std::atan2(eye_z - look_z, eye_y - look_y);
    const float cosine = std::cos(angle);
    assert(cosine > 0.0f);
#endif  // defined(__GLIBCXX__) || defined(__GLIBCPP__)
    const float min_dist = min_height / cosine;

    delta = ((ground_dist + delta) < min_dist) ? (min_dist - ground_dist) : delta;
    ground_dist += delta;

    view_xform[3][2] -= delta;
    update_dependants();
}

void RTSCamera::rotate_Y(float angle)
{
    auto& Mv2w = view_world_xform;
    // eye in view space is the origin, transforming that with Mv2w would mean
    // the linear (upper 3x3) part would become zero, leaving only the affine (translation) part
    const glm::vec3 eye = glm::vec3(Mv2w[3]);
#ifdef NDEBUG
    // lookAt point w.r.t view space is trivial, transforming
    // that to world space gives lookAt point in world space
    const glm::vec4 lookat_view{0.0f, 0.0f, -ground_dist, 1.0f};
    const auto lookat = glm::vec3(Mv2w * lookat_view);
#else
    // viewdir is basis k in view space i.e. row 2 in view_xform, but col 2 of Mv2w would have the
    // same row transposed (xyz will be the same, w will be different - affineInverse); using it
    // avoids another transpose by glm::row since matrices are stored as columns by GLM
    const glm::vec3 viewdir = -glm::vec3(glm::column(Mv2w, 2));
    const Primitives::Ray cam = {eye, viewdir};
    const Primitives::Plane terrain = { {0.0f, 1.0f, 0.0f, 0.0f} };
    float t = -std::numeric_limits<float>::infinity();
    VERIFY(Primitives::ray_plane_intersect(cam, terrain, &t));
    const auto lookat = Primitives::get_point(cam, t);
#endif  // NDEBUG
    const auto toOrg = glm::translate(-lookat);
    const auto rotor = glm::rotateNormalizedAxis(glm::mat4(1.0), angle, glm::vec3{0.0f, 1.0f, 0.0f});
    const auto froOrg = glm::translate(lookat);
    const auto new_eye = froOrg * rotor * toOrg * glm::vec4(eye, 1.0f);
    //view_xform = glm::lookAt(glm::vec3(new_eye), lookat, glm::vec3{0.0f, 1.0f, 0.0f});
    // this achieves the same as gluLookAt without using normalize and cross
    Mv2w[3] = {0.0f, 0.0f, 0.0f, 1.0f};
    Mv2w = rotor * Mv2w;
    Mv2w[3] = new_eye;
    view_xform = glm::affineInverse(Mv2w);
    // Mv2w i.e world_view_xform is already updated, hence no need to call update_dependants
    // just set the flag
    frustum_changed = true;
}
