#ifndef PRIMITIVES_HPP
#define PRIMITIVES_HPP

namespace Primitives
{

#pragma pack(push, 1)
struct PNT
{
    glm::vec3 pos;
#ifdef HALF_TEX_COORD
    glm::uint tex;
#else
    float tex_X, tex_Y;
#endif
    glm::vec3 normal;
};
#pragma pack(pop)

#ifdef HALF_TEX_COORD
static_assert(sizeof(Primitives::PNT) == 28u, "Error: Vertex attrib struct packing failed!");
#else
static_assert(sizeof(Primitives::PNT) == 32u, "Error: Vertex attrib struct packing failed!");
#endif

// http://stackoverflow.com/q/26755248/183120
struct DrawData
{
    std::vector<GLsizei> counts;
    std::vector<GLvoid*> offsets;
    std::vector<GLint> base_verts;
};

struct Mesh // indexed triangles list
{
    // an element per vertex attribute
#ifdef HALF_TEX_COORD
    static constexpr int type_id[4] = { GL_FLOAT, GL_HALF_FLOAT, GL_HALF_FLOAT, GL_FLOAT };
#else
    static constexpr int type_id[4] = { GL_FLOAT, GL_FLOAT, GL_FLOAT, GL_FLOAT };
#endif
    // for each element, the count of (type_id) items
    static constexpr unsigned elem_lens[4] = { 3u, 1u, 1u, 3u };

    GLenum primitive_type;
    // although int is used for indices, the type should be based on vertices.size()
    // since having a lesser sized datatype is better when uploading to GPU
    // Boost.Variant can help here
    std::vector<GLushort> indices;
    // position and colour
    std::vector<PNT> vertices;
    std::vector<int32_t> textures;
    glm::mat4 model_xform;
    std::unique_ptr<DrawData> multi_draw;
};

struct OBB
{
    glm::vec3 centre;
    glm::vec3 half_extent;
    glm::mat3 axes;
};

struct Plane
{
    // the w coordinate is the NEGATIVE signed distance
    // from the origin along the normal (based on Skeleton book)
    glm::vec4 ABCD;
};

struct Ray
{
    glm::vec3 origin;
    glm::vec3 direction;
};

struct Triangle
{
    glm::vec3 verts[3];
};

// cells, identified by their left top indices, are elements of the 2d projection of
// the height map - a grid, with positional x acting as grid x and positional z acting
// as grid y, advancing from top to bottom like most screen coordinate systems
typedef glm::uvec2 Cell;

struct HeightMap
{
    enum class ChunkType : uint8_t
    {
        Normal,
        Dummy_Col,
        Dummy_Row,
        Dummy_Corner
    };

    size_t rows, cols;
    std::vector<float> heights;
    glm::vec3 min, max;
    const glm::vec3 cell_size{13.3333f, 1.0f, 13.3333f};
    static constexpr uint16_t chunk_cells = 32u;
    QuadTree partitions;

    float height_at(size_t row, size_t col) const;
    float height_at(float x, float z) const;
    void get_triangles(const Cell &cell, Triangle *t1, Triangle *t2) const;
    float x_to_index(float pos) const;
    float y_to_index(float pos) const;
    float z_to_index(float pos) const;
    float index_to_x(float index) const;
    float index_to_y(float index) const;
    float index_to_z(float index) const;
    size_t chunk_count() const;
    ChunkType chunk_type(size_t col, size_t row) const;
};

struct AABB_2D
{
    glm::vec2 centre;
    float half_extent;
};

struct Sphere
{
    glm::vec3 centre;
    float radius;
};

struct Cone
{
    glm::vec3 dir;
    float reciSin;
    float sin2;
    glm::vec3 apex;
};

struct Frustum
{
    // left, right, bottom, top, near, far
    std::array<Plane, 6> p;
    Sphere s;
    Cone c;
    // frustum centre in view space (0, 0, -neg_centre_z)
    float neg_centre_z;
};

enum class X_state : uint8_t
{
    Disjoint,
    Intersecting,
    Within
};

// http://stackoverflow.com/a/9877317/183120
inline
bool operator!(X_state s)
{
  return s == X_state::Disjoint;
}

// returns X_state::Within if A is completely contained by B
X_state sphere_sphere_intersect(const Sphere &A, const Sphere &B);
bool cone_sphere_intersect(const Cone &c, const Sphere &s);
bool ray_plane_intersect(const Ray &ray,
                         const Plane &plane,
                         float *t);
bool ray_triangle_intersect(const Ray &ray,
                            const Triangle &tri,
                            float *t);
bool ray_obb_intersect(const Ray &ray,
                       const OBB &box,
                       float *t1,
                       float *t2 = nullptr);
X_state OBB_plane_intersect(const OBB &b, const Plane &p);
X_state frustum_OBB_intersect(const Frustum &f, const OBB &b, glm::uint32 planes);
X_state frustum_sphere_intersect(const Frustum &f, const Sphere &s, glm::uint32 planes);
X_state sphere_plane_intersect(const Sphere &s, const Plane &p);
float point_plane_dist(const glm::vec4 &pt, const Plane &p);
bool point_OBB_test(const glm::vec3 &pt, const OBB &b);
// frustum space is just the view space but the origin shifted to frustum centre
void view_to_frustum_space(glm::vec3 &pt, const Frustum &f);
// depending on the signs of pt's coordinates, this returns the planes to be tested
// the returning values first 10 bits give left or right, next 10 gives top or bottom, etc.
glm::uint32 point_octant_planes(const glm::vec3 &pt);
// xform should be a rigid-body transform
void transform_frustum(const Frustum &f,
                       const glm::mat4 &xform,
                       const glm::mat4 *normal_xform,
                       Frustum &xform_f);

OBB aabb_to_obb(const glm::vec3 &min, const glm::vec3 &max);
Ray clip_ray(const Ray &r,
             const glm::vec3 &min,  // if no clipping is required in a dimension
             const glm::vec3 &max); // pass the same value for both min and max
glm::vec3 get_point(const Primitives::Ray &ray, float t);
bool is_degenerate(const Ray &ray, float epsilon = 10.0E-6f);

glm::vec4 left_top_right_bottom(glm::vec2 min, glm::vec2 max);

template <typename T>
bool grid_traverse(const Primitives::Ray &r, T visit);

void calculate_normals(Primitives::HeightMap *height_map,
                       std::vector<Primitives::PNT> &vertices);

} // namespace Primitives

constexpr unsigned verts_per_triangle = 3u;

template <typename T>
bool Primitives::grid_traverse(const Primitives::Ray &r,
                               T visit)
{
    assert(!is_degenerate(r));

    // this method and the epsilon is taken from the (float) Grid Ray Tracing workout
    constexpr auto eps = 10.0E-6f;
    const auto dest_x = r.origin.x + r.direction.x;
    const auto dest_z = r.origin.z + r.direction.z;
    const auto step_x = (r.direction.x != 0.0f) ? (r.direction.x > 0.0f ? 1.0f : -1.0f) : 0.0f;
    const auto step_z = (r.direction.z != 0.0f) ? (r.direction.z > 0.0f ? 1.0f : -1.0f) : 0.0f;
    auto x = r.origin.x, z = r.origin.z;
    auto reached = false;
    while (!reached && (glm::epsilonNotEqual(x, dest_x, eps) || glm::epsilonNotEqual(z, dest_z, eps)))
    {
        auto cur_x = x, cur_z = z;
        // next value would be a line x = k or the destination x
        const auto next_x = closer(x, prev_int(x, step_x) + step_x, dest_x);
        const auto next_z = closer(z, prev_int(z, step_z) + step_z, dest_z);
        auto t_exit = 0.0f;
#ifndef __FAST_MATH__
        const auto tx = (next_x - r.origin.x) / r.direction.x;
        const auto tz = (next_z - r.origin.z) / r.direction.z;
        // instead of checking for infinity (isinf) checking for
        // both INF and NAN (isfinite) since dx = 0 → tx = 0/0 = NAN
        if ((!std::isfinite(tz)) || (std::isfinite(tx) && (tx <= tz)))
        // even when ty = NAN, std::isfinite returns false when -ffast-math is used
#else
        const auto tx = r.direction.x ? ((next_x - r.origin.x) / r.direction.x) : 0.0f;
        const auto tz = r.direction.z ? ((next_z - r.origin.z) / r.direction.z) : 0.0f;
        if ((tz == 0.0f) || ((tx != 0.0f) && (tx <= tz)))
#endif
        {
            // z = r.org.z + tx * r.dir.z;
            z = std::fma(tx, r.direction.z, r.origin.z);
            x = next_x;
            t_exit = tx;
        }
        else
        {
            // x = r.org.x + tz * r.dir.x;
            x = std::fma(tz, r.direction.x, r.origin.x);
            z = next_z;
            t_exit = tz;
        }
        reached = visit({cur_x, cur_z}, {x, z}, t_exit);
    }
    return reached;
}

#endif // PRIMITIVES_HPP
