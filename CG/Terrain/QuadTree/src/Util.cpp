#include "common.hpp"
#include "Util.hpp"

unsigned long ulog2(unsigned long x)
{
    // log 0 is undefined for all bases
    if (0u == x) return 0u;

#ifdef __GNUC__
    auto constexpr max_index = std::numeric_limits<decltype(x)>::digits - 1;
    return max_index - __builtin_clzl(x);
#else
    unsigned long value;
    _BitScanReverse(&value, x);
    return value;
#endif
}

// Orson Peters (nightcracker)
// http://gist.github.com/orlp/3551590
int64_t ipow(int64_t base, uint8_t exp)
{
    static const uint8_t highest_bit_set[] =
    {
        0, 1, 2, 2, 3, 3, 3, 3,
        4, 4, 4, 4, 4, 4, 4, 4,
        5, 5, 5, 5, 5, 5, 5, 5,
        5, 5, 5, 5, 5, 5, 5, 5,
        6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 6, 6, 6, 255, // anything past 63 is a guaranteed overflow with base > 1
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
    };

    uint64_t result = 1;
    int iexp = exp;
    switch (highest_bit_set[exp])
    {
    case 255: // we use 255 as an overflow marker and return 0 on overflow/underflow
        if (base == 1)
            return 1;

        if (base == -1)
            return 1 - 2 * (iexp & 1);

        return 0;
    case 6:
        if (iexp & 1) result *= base;
        iexp >>= 1;
        base *= base;
    case 5:
        if (iexp & 1) result *= base;
        iexp >>= 1;
        base *= base;
    case 4:
        if (iexp & 1) result *= base;
        iexp >>= 1;
        base *= base;
    case 3:
        if (iexp & 1) result *= base;
        iexp >>= 1;
        base *= base;
    case 2:
        if (iexp & 1) result *= base;
        iexp >>= 1;
        base *= base;
    case 1:
        if (iexp & 1) result *= base;
    default:
        return result;
    }
}

Bitmap read_bitmap(const std::string &file_name)
{
    std::ifstream bmp_file{file_name, std::ios_base::in | std::ios_base::binary};

    BMP_header header;
    // reader header
    if(! (bmp_file.read(reinterpret_cast<char*>(&header), sizeof(BMP_header)) && (BM_ASCII == header.magic)))
    {
        throw std::invalid_argument{file_name + " isn't a valid BMP file"};
    }

    // check if the size is right
    const uint32_t data_size = header.width * header.height *
                               (header.bits_per_pixels / std::numeric_limits<unsigned char>::digits);
    auto file_size = bmp_file.tellg();
    bmp_file.seekg(0, std::ios_base::end);
    file_size = bmp_file.tellg() - file_size;
    if(file_size != data_size)
    {
        throw std::invalid_argument{file_name + " is a malformed BMP file"};
    }

    // read data
    bmp_file.seekg(header.data_offset, std::ios_base::beg);
    std::vector<uint8_t> bitmap(data_size);
    bmp_file.read(reinterpret_cast<char*>(bitmap.data()), data_size);
    return {header.width, header.height, bitmap};
}

size_t Array::item_index(size_t item_id,
                         size_t base_id,
                         size_t max_id,
                         size_t err_index)
{
    const auto max_index = max_id - base_id;
    int index = static_cast<int>(item_id - base_id);
    return ((index < 0) || (static_cast<unsigned>(index) > max_index)) ?
            err_index :
            static_cast<size_t>(index);
}
