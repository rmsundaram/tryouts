#ifndef UTIL_HPP
#define UTIL_HPP

#ifdef NDEBUG
#define VERIFY(X) (X)
#else
#define VERIFY(X) assert(X)
#endif

namespace Array
{

template <typename T>
inline constexpr typename std::enable_if<std::is_array<T>::value, size_t>::type
max_index(const T& /*arr*/)
{
    return std::extent<T>::value - 1;
}

size_t item_index(size_t item_id,
                  size_t base_id,
                  size_t max_id,
                  size_t err_index);
}   // namespace Array

inline void check(bool condition, const char *error_msg)
{
    if (condition != true)
    {
        std::cerr << error_msg << '\n';
        throw std::runtime_error(error_msg);
    }
}

inline std::ostream& tab(std::ostream &os)
{
    return os << '\t';
}

// returns true when x is within [min, max]
template <typename T>
typename std::enable_if<std::is_arithmetic<T>::value, bool>::type
is_within(T x, T a, T b)
{
    return (a < b) ? ((x >= a) && (x <= b)) : ((x >= b) && (x <= a));
}

template <typename T>
typename std::enable_if<std::is_arithmetic<T>::value, T>::type
distance(T x1, T x2)
{
    return std::abs(x2 - x1);
}

template <typename T>
typename std::enable_if<std::is_arithmetic<T>::value, T>::type
closer(T x, T x1, T x2)
{
    // finding the lesser of the two after taking abs on both was considered
    // but it will not work when one is positive and the other is not
    return (distance(x, x1) <= distance(x, x2)) ? x1 : x2;
}

template <typename T, typename U>
typename std::enable_if<std::is_floating_point<T>::value, T>::type
prev_int(T x, U dx)
{
    // dx is really vector, it has the direction and the magnitude of shift;
    // the word prev is from the PoV of the ray dx from the origin x
    // if positive round towards -∞ (floor) else towards +∞ (ceil)
    // passing dx = 0 is undefiend since it's actually a degenerate case
    return (dx < U{}) ? std::ceil(x) : std::floor(x);
}

template <typename T>
typename std::enable_if<std::is_integral<T>::value, bool>::type
is_power_of_2(T v)
{
    return (v && !(v & (v - 1)));
}

template <typename T>
typename std::enable_if<std::is_floating_point<T>::value, bool>::type
less(T f1, T f2, T e = glm::epsilon<T>())
{
    return (f1 < f2) && glm::epsilonNotEqual(f1, f2, e);
}

template <typename T>
typename std::enable_if<std::is_floating_point<T>::value, bool>::type
less_equal(T f1, T f2, T e = glm::epsilon<T>())
{
    return (f1 < f2) || glm::epsilonEqual(f1, f2, e);
}

template <typename T>
typename std::enable_if<std::is_floating_point<T>::value, T>::type
floor_if_not_int(T f, T epsilon = glm::epsilon<T>())
{
    T integral;
    if (glm::epsilonEqual(std::modf(f, &integral), T(), epsilon))
    {
        return integral;
    }
    return std::floor(f);
}

template <typename T>
struct Vec_compare
{
    using FT = typename T::value_type;
    bool operator()(const T &a, const T &b)
    {
        // TODO: accessing GLM's vectors this way isn't strictly correct since
        // GLM declares this as a struct and not an array, hence padding can mar the data
        return std::lexicographical_compare(&a[0], &a[0] + a.length(),
                                            &b[0], &b[0] + b.length(),
                                            [](FT f1, FT f2)
                                            {
                                                return less<FT>(f1, f2, glm::epsilon<FT>());
                                            });
    }
};

// FIXME: reusing an istringstream may not be a good thing
// http://stackoverflow.com/questions/17743903/how-to-initialize-stringstream-object#comment25871163_17744203
inline
void next_line(std::ifstream &ifs, std::istringstream &iss)
{
    std::string line;
    std::getline(ifs, line);
    iss.str(line);
    iss.clear();
}

unsigned long ulog2(unsigned long x);
int64_t ipow(int64_t base, uint8_t exp);

#pragma pack(push, 2)
struct BMP_header
{
    // BMP header
    uint16_t magic = 0;
    uint32_t file_size = 0;
    uint32_t reserved = 0;
    uint32_t data_offset = 0;

    // DIB header
    uint32_t dib_header_length = 0;
    uint32_t width = 0;
    uint32_t height = 0;
    uint16_t num_colour_planes = 0;
    uint16_t bits_per_pixels = 0;
    uint32_t bi_bitfields = 0;
    uint32_t data_size = 0;
    uint32_t physical_width = 0;
    uint32_t physical_height = 0;
    uint32_t num_palette_colours = 0;
    uint32_t num_important_colours = 0;
};
#pragma pack(pop)
static_assert(sizeof(BMP_header) == 54u, "BMPHeader structure packing failed");

constexpr unsigned BM_ASCII = 19778u;    // "BM" magic
constexpr unsigned num_bmp_channels = 3u;

struct Bitmap
{
    size_t width, height;
    std::vector<uint8_t> data;
};

Bitmap read_bitmap(const std::string &file_name);

#endif // UTIL_HPP
