#ifndef SDL_HPP
#define SDL_HPP

namespace Util
{

void parse_SDL(const char *file_path,
               Scene *scene);

}

#endif // SDL_HPP
