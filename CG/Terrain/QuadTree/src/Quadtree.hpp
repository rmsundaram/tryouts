#ifndef QUADTREE_HPP
#define QUADTREE_HPP

namespace Primitives
{
    struct AABB_2D;
    struct Sphere;
    struct OBB;
}

// based on the second QuadTree workout
struct QuadTree
{
    bool dummy_row_col;
    std::vector<Primitives::AABB_2D> nodes;
    std::vector<Primitives::Sphere> node_spheres;
    std::vector<Primitives::OBB> node_OBB;

    uint8_t max_depth() const;
    uint16_t offset(uint8_t depth) const;
    uint16_t child_offset(uint16_t node) const;
    uint16_t descendant_leaf_offset(uint16_t node, uint16_t *offset) const;
    // returns true if node is below a given depth, root is at depth 1
    // e.g. is_descendant(5, 3) = false, is_descendant(19, 3) = false, is_descendant(21, 3) = true
    bool is_descendant(uint16_t node, uint8_t depth) const;
    glm::uvec2 index_2D(uint16_t node, uint8_t depth) const;
};

// for a depth of 2, total_cells would be 16 and leaf_cells would be 4
// height parameter is to make this quadtree usable in 3D too
QuadTree make_quadtree(uint16_t total_cells, uint16_t leaf_cells, float cell_size, float height);

#endif // QUADTREE_HPP
