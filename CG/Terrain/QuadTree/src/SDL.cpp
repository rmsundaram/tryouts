#include "common.hpp"
#include "Util.hpp"
#include "GL_res_wrap.hpp"
#include "GL_util.hpp"
#include "Quadtree.hpp"
#include "Primitives.hpp"
#include "Light.hpp"
#include "Camera.hpp"
#include "main.hpp"
#include "SDL.hpp"

namespace
{

std::vector<GLushort> generate_indices(size_t cols, size_t total_cols)
{
    assert(cols > 1u && total_cols >= cols);

    std::vector<GLushort> indices;
    indices.reserve(cols * 2);
    for (auto c = 0u; c < cols; ++c)
    {
        indices.push_back(static_cast<GLshort>(c));
        indices.push_back(static_cast<GLshort>(c + total_cols));
    }
    return indices;
}

void make_terrain_mesh(Primitives::HeightMap *height_map,
                       std::vector<Primitives::Mesh> *meshes,
                       std::vector<int32_t> &&textures,
                       glm::vec2 tex_tile)
{
    const size_t vrtx_count = height_map->rows * height_map->cols;
    std::vector<Primitives::PNT> vertices;
    vertices.reserve(vrtx_count);

    const auto offset_x = 0.5f * ((static_cast<float>(height_map->cols) - 1.0f) * height_map->cell_size.x);
    const auto offset_z = 0.5f * ((static_cast<float>(height_map->rows) - 1.0f) * height_map->cell_size.z);

    height_map->min.z = -offset_z;
    height_map->min.x = -offset_x;
    height_map->max.z = offset_z;
    height_map->max.x = offset_x;

    // generate vertices
    int i = 0, j = -1;
    for (auto t = 0u; t < vrtx_count; ++t)
    {
        j = (j + 1) % height_map->cols;
        i = t / height_map->cols;
        const auto x = (static_cast<float>(j) * height_map->cell_size.x) - offset_x;
        const auto z = (static_cast<float>(i) * height_map->cell_size.z) - offset_z;
        const auto y = height_map->heights[t];
        // mapping a 5 unit texture to 4 vertices
        //        |   |   |   |   |
        //        0  1/3 2/3  1  4/3
        const glm::vec2 tex_coords{static_cast<float>(j) / (tex_tile.x - 1.0f),
                                   static_cast<float>(i) / (tex_tile.y - 1.0f)};
#ifdef HALF_TEX_COORD
        vertices.push_back({glm::vec3{x, y, z}, glm::packHalf2x16(tex_coords), glm::vec3()});
#else
        vertices.push_back({glm::vec3{x, y, z}, tex_coords.x, tex_coords.y, glm::vec3()});
#endif
    }

    Primitives::calculate_normals(height_map, vertices);
    // generate indices for one chunk
    const auto chunk_cols = Primitives::HeightMap::chunk_cells + 1;
    // an std::move here is an incorrect, pessimizing move; warned by Clang++
    // https://stackoverflow.com/a/28605003
    // https://developers.redhat.com/blog/2019/04/12/understanding-when-not-to-stdmove-in-c/
    std::vector<GLushort> indices = generate_indices(chunk_cols, height_map->cols);

    meshes->push_back({GL_TRIANGLE_STRIP,
                       std::move(indices),
                       std::move(vertices),
                       std::move(textures),
                       glm::mat4(1.0),
                       nullptr /*multi_draw*/});

    // generate multidraw data
    auto &terrain = meshes->back();
    terrain.multi_draw.reset(new Primitives::DrawData);
    terrain.multi_draw->counts.assign(Primitives::HeightMap::chunk_cells, terrain.indices.size());
    terrain.multi_draw->offsets.assign(Primitives::HeightMap::chunk_cells, 0u);
    terrain.multi_draw->base_verts.assign(Primitives::HeightMap::chunk_cells, 0u);  // to be overwritten by every draw call
}

void parse_height_map(std::istringstream &istr,
                      std::ifstream &ifile,
                      Primitives::HeightMap *height_map,
                      std::vector<Primitives::Mesh> *meshes)
{
    istr >> height_map->rows >> height_map->cols;
    const auto &rows = height_map->rows, &cols = height_map->cols;
    assert(rows == cols);

    glm::vec2 tex_tile;
    istr >> tex_tile.y >> tex_tile.x;     // rows and then cols i.e. V and then U
    std::vector<int32_t> textures;
    uint16_t tex_id;
    while (istr >> tex_id)
        textures.push_back(static_cast<uint8_t>(tex_id));

    // a texture, in both dimensions, should be mapped to some sizable area and thus cannot be ≤ 0
    // mathematically this avoids potential divide by zero at make_terrain_mesh site
    tex_tile.x = std::max(0.1f, tex_tile.x);
    tex_tile.y = std::max(0.1f, tex_tile.y);

    height_map->heights.reserve(rows * cols);
    float max_height = -std::numeric_limits<float>::min();
    for(auto i = 0u; i < rows; ++i)
    {
        next_line(ifile, istr);
        float height;
        while(istr >> height)
        {
            assert(height >= 0.0f);
            max_height = std::max(max_height, height);
            height_map->heights.push_back(height);
        }
        assert((height_map->heights.size() % cols) == 0);
    }
    // minimum height = 0 which is set by vec3::vec3()
    height_map->max.y = max_height;

    height_map->partitions = make_quadtree(static_cast<uint16_t>(height_map->cols),
                                           Primitives::HeightMap::chunk_cells,
                                           height_map->cell_size.x,
                                           height_map->max.y);
    make_terrain_mesh(height_map,
                      meshes,
                      std::move(textures),
                      tex_tile);
}

void load_terrain_file(std::istringstream &istr,
                       Primitives::HeightMap *height_map,
                       std::vector<Primitives::Mesh> *meshes)
{
    std::string file_name;
    istr >> file_name;
    const Bitmap bmp = read_bitmap(file_name);
    assert(bmp.width == bmp.height);
    assert((bmp.data.size() % num_bmp_channels) == 0);
    const auto data_length = bmp.data.size() / num_bmp_channels;
    height_map->heights.reserve(data_length);
    for(size_t i = 0u; i < bmp.data.size(); i += num_bmp_channels)
    {
        const float ht = bmp.data[i];
        height_map->heights.push_back(ht);
        height_map->max.y = std::max(height_map->max.y, ht);
    }

    height_map->rows = bmp.width;
    height_map->cols = bmp.height;

    glm::vec2 tex_tile;
    istr >> tex_tile.y >> tex_tile.x;     // rows and then cols i.e. V and then U
    std::vector<int32_t> textures;
    uint16_t tex_id;
    while (istr >> tex_id)
        textures.push_back(static_cast<uint8_t>(tex_id));

    // a texture, in both dimensions, should be mapped to some sizable area and thus cannot be ≤ 0
    // mathematically this avoids potential divide by zero at make_terrain_mesh site
    tex_tile.x = std::max(0.1f, tex_tile.x);
    tex_tile.y = std::max(0.1f, tex_tile.y);

    height_map->partitions = make_quadtree(static_cast<uint16_t>(height_map->cols),
                                           Primitives::HeightMap::chunk_cells,
                                           height_map->cell_size.x,
                                           height_map->max.y);
    make_terrain_mesh(height_map, meshes, std::move(textures), tex_tile);
}

void load_texture(std::string file_name, std::vector<GL::Texture> *textures)
{
#ifndef NDEBUG
    // actual value should be quieried from GL_MAX_TEXTURE_IMAGE_UNITS
    // but all GL 3.3 cards should support atleast 16
    constexpr auto max_textures = 16u;
    assert(textures->size() <= max_textures);
#endif
    // the texture unit is irrelevant to texture creation, only while using do we have to make
    // sure different textures are put in different units; hence using GL_TEXTURE0 always for creation
    // http://3dgep.com/multi-textured-terrain-in-opengl/
    textures->push_back(GL::upload_texture(file_name.data(), GL_TEXTURE0));
}

void setup_light(std::istringstream &istr,
                 std::vector<Light> *lights)
{
    glm::vec3 dir;
    glm::vec4 ambient, diffuse;

    istr >> dir.x >> dir.y >> dir.z;
    istr >> ambient.r >> ambient.g >> ambient.b >> ambient.a;
    istr >> diffuse.r >> diffuse.g >> diffuse.b >> diffuse.a;

    // light direction in SDL is defined as flowing from light to object surface
    // invert it to make the vector go from surface to light for GLSL (see §5.2, RTR 3/e)
    lights->push_back({glm::normalize(-dir),
                       ambient,
                       diffuse});
}

}  // unnamed namespace

void Util::parse_SDL(const char *file_path,
                     Scene *scene)
{
    assert(file_path && scene);

    std::ifstream sdl_file{file_path};
    for(std::string line; std::getline(sdl_file, line); )
    {
        std::istringstream istr{line};
        char code;
        if (istr >> code)
        {
            switch(code)
            {
                case 'i':
                {
                    std::string file_name;
                    istr >> file_name;
                    load_texture(file_name, &scene->textures);
                    break;
                }
                case 'd':
                {
                    setup_light(istr, &scene->lights);
                    break;
                }
                case 'h':
                {
                    // having more than one terrain (both 't and 'h') in an SDL file is invalid
                    // first one takes precedence, rest are ignored
                    if (scene->height_map.heights.empty())
                        parse_height_map(istr, sdl_file, &scene->height_map, &scene->meshes);
                    break;
                }
                case 't':
                {
                    // having more than one terrain (both 't and 'h') in an SDL file is invalid
                    // first one takes precedence, rest are ignored
                    if (scene->height_map.heights.empty())
                        load_terrain_file(istr, &scene->height_map, &scene->meshes);
                    break;
                }
            }
        }
    }
}
