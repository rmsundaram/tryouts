#ifndef CAMERA_HPP
#define CAMERA_HPP

struct Camera
{
    // manipulation constants in per millisecond units
    static constexpr float slide_offset = 0.7f;
    static constexpr float rot_angle = 0.0015f;
    static constexpr float FoV_delta = 0.002f;

    // world to view transform
    glm::mat4 view_xform;
    glm::mat4 view_world_xform;

    // projection parameters
    float FoV, width, height, aspect_ratio, near, far;
    glm::mat4 projection;

    // frustum in view space
    Primitives::Frustum f = {{} /*plane*/,
                             {} /*sphere*/,
                             {{0.0f, 0.0f, -1.0f}, 0.0f, 0.0f, {}}, /*cone*/
                             0.0f /* negative centre.z */};
    // Cone::apex is zero-initialized and Cone::dir is initialized to <0, 0, -1>
    // even when it's called on an FoV change the apex and the dir should remain the same

    Camera(float FoV,
           float width,
           float height,
           float near,
           float far);
    virtual ~Camera() { }
    virtual void reset() = 0;
    virtual void slide_X(float delta) = 0;
    virtual void slide_Y(float delta) = 0;
    virtual void slide_Z(float delta) = 0;
    virtual void rotate_X(float angle) = 0;
    virtual void rotate_Y(float angle) = 0;
    virtual void rotate_Z(float angle) = 0;
    virtual void change_FoV(float angle);

    // frustum in world space
    bool frustum(Primitives::Frustum *fstum, bool skip_if_same = true) const;

protected:
    void update_dependants();
    // frustum parameters
    float d = 0.0f;     // focal length
    float sqrt_d2_a2;
    float sqrt_d2_1;

    mutable bool frustum_changed;

private:
    void calc_frustum_params();
    void world_frustum(Primitives::Frustum *fstum) const;
    void calc_bounding_sphere(float focal_length);
};

// Camera with 6 DoF (free-roaming robot); slide and rotate on all 3 axes
// the implmentation here is optimized lookup Free-look Camera workout
// for a straight forward implmentation to understand what's going on
struct FreeCamera : Camera
{
    FreeCamera(float FoV,
               float width,
               float height,
               float near,
               float far);

    void reset() override;
    void slide_X(float delta) override;
    void slide_Y(float delta) override;
    void slide_Z(float delta) override;
    void rotate_X(float angle) override;
    void rotate_Y(float angle) override;
    void rotate_Z(float angle) override;
};

struct RTSCamera : Camera
{
    RTSCamera(float FoV,
              float width,
              float height,
              float near,
              float far,
              float min_ht);

    void reset() override;
    void slide_X(float delta) override;
    void slide_Y(float delta) override;
    void slide_Z(float delta) override;
    void rotate_X(float /*angle*/) override { }
    void rotate_Y(float angle) override;
    void rotate_Z(float /*angle*/) override { }

private:

    static constexpr float eye_x = 0.0f;
    static constexpr float eye_y = 500.0f;
    static constexpr float eye_z = 400.0f;
    static constexpr float look_x = 0.0f;
    static constexpr float look_y = 0.0f;
    static constexpr float look_z = 0.0f;

    // minimum height to maintain between camera and ground
    float min_height;
    // the distance b/w view origin and ground, in view's Z scale;
    float ground_dist;
};

#endif // CAMERA_HPP
