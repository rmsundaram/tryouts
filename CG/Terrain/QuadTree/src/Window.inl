#ifndef WINDOW_HPP
#define WINDOW_HPP

#include "Dataless_wrapper.hpp"
#include <GLFW/glfw3.h>

namespace
{

using GLFW_wrapper = Dataless_wrap<decltype(&glfwTerminate), glfwTerminate>;
using GLFW_window = std::unique_ptr<GLFWwindow, decltype(&glfwDestroyWindow)>;

const char* const app_title = "Quadtree Frustum Culling";

GLFWwindow* setup_context()
{
    /*
     * without setting the version (defaulting to 1.0) or setting it < 3.2, requesting for core
     * will fail since context profiles only exist for OpenGL 3.2+; likewise forward compatibility
     * only exist from 3.0 onwards. 3.0 marked deprecated, 3.1 removed deprecated (except
     * wide lines), 3.2 reintroduced deprecated under compatibility profile
     */
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    // enable multisampling
    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

#ifdef __APPLE__
    // https://stackoverflow.com/q/19658745/183120
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

#ifndef NDEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

#ifdef FULL_SCREEN
    int disp_modes;
    const auto monitor = glfwGetPrimaryMonitor();
    const auto modes = glfwGetVideoModes(monitor, &disp_modes);
    ::check(modes, "Failed enumerating video modes");
    const auto m = modes[disp_modes - 1];
    auto window = glfwCreateWindow(m.width,
                                   m.height,
                                   app_title,
                                   monitor,
                                   nullptr);
#else
    auto window = glfwCreateWindow(1024,
                                   768,
                                   app_title,
                                   nullptr /*monitor*/,
                                   nullptr);

#endif  // WINDOWED

    ::check(window != nullptr, "Failed to create window");

    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
#ifdef _WIN32
    // http://stackoverflow.com/q/16285546
    // GLFW doesn't honour call to VSYNC when DWM compositing is enabled, which is practically all machines today;
    // turn it on using the using WGL_EXT_swap_control extension on Win32
    // TODO: do this for other platforms too
    typedef int (APIENTRY *PFNWGLSWAPINTERVALEXTPROC)(int interval);
    auto wglSwapInterval = reinterpret_cast<PFNWGLSWAPINTERVALEXTPROC>(glfwGetProcAddress("wglSwapIntervalEXT"));
    if (wglSwapInterval)
    {
        typedef int (APIENTRY *PFNWGLGETSWAPINTERVALEXTPROC)(void);
        auto wglGetSwapInterval =
            reinterpret_cast<PFNWGLGETSWAPINTERVALEXTPROC>(glfwGetProcAddress("wglGetSwapIntervalEXT"));
        // if the get function isn't there or is returning 0
        if (!wglGetSwapInterval || !wglGetSwapInterval())
            wglSwapInterval(1);
    }
#endif
    return window;
}

#ifndef NDEBUG

APIENTRY
void gl_debug_logger(GLenum source,
                     GLenum type,
                     GLuint id,
                     GLenum severity,
                     GLsizei /*length*/,
                     const GLchar *msg,
                     const void *file_ptr)
{
    std::ostringstream ss;
    ss << "GLError 0x"
       << std::hex << id << std::dec
       << ": "
       << msg <<
       " [source=";

    const char *sources[] = {
                                "API",
                                "WINDOW_SYSTEM",
                                "SHADER_COMPILER",
                                "THIRD_PARTY",
                                "APPLICATION",
                                "OTHER",
                                "UNDEFINED"
                            };
    size_t index = Array::item_index(source,
                                     GL_DEBUG_SOURCE_API_ARB,
                                     GL_DEBUG_SOURCE_OTHER_ARB,
                                     Array::max_index(sources));
    ss << sources[index];
    if (Array::max_index(sources) == index)
    {
        ss << " (" << source << ")";
    }

    const char *types[] = {
                              "ERROR",
                              "DEPRECATED_BEHAVIOR",
                              "UNDEFINED_BEHAVIOR",
                              "PORTABILITY",
                              "PERFORMANCE",
                              "OTHER",
                              "UNDEFINED"
                          };
    index = Array::item_index(type,
                              GL_DEBUG_TYPE_ERROR_ARB,
                              GL_DEBUG_TYPE_OTHER_ARB,
                              Array::max_index(types));
    ss << " type=" << types[index];
    if (Array::max_index(types) == index)
    {
        ss << " (" << type << ")";
    }

    const char *severities[] = {
                                   "HIGH",
                                   "MEDIUM",
                                   "LOW",
                                   "UNKNOWN"
                               };
    // as per the extension, HIGH is the base (smaller) value
    index = Array::item_index(severity,
                              GL_DEBUG_SEVERITY_HIGH_ARB,
                              GL_DEBUG_SEVERITY_LOW_ARB,
                              Array::max_index(severities));
    ss << " severity=" << severities[index];
    if (Array::max_index(severities) == index)
    {
        ss << " (" << severity << ")";
    }
    ss << "]";

    const auto log = ss.str();
    FILE *out_file = reinterpret_cast<FILE*>(const_cast<void*>(file_ptr));
    fprintf(out_file, "%s\n", log.c_str());

    // log everything but throw only on high severity errors
    ::check(severity != GL_DEBUG_SEVERITY_HIGH_ARB, "High severity GL error logged");
}

void setup_debug(bool enable)
{
    if (ogl_ext_ARB_debug_output == ogl_LOAD_FAILED) return;

    glDebugMessageCallbackARB(enable ? gl_debug_logger : nullptr, stderr);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
    ::check(GL_NO_ERROR == glGetError(), "Unable to set synchronous debug output");
}
#endif  // NDEBUG

void handle_key(GLFWwindow *window,
                int key,
                int /*scancode*/,
                int action,
                int /*mods*/)
{
    auto scene = reinterpret_cast<Scene*>(glfwGetWindowUserPointer(window));
    assert(scene);

    if(GLFW_PRESS == action)
    {
        switch(key)
        {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, GL_TRUE);
            break;
        case GLFW_KEY_R:
            scene->current_cam()->reset();
            break;
        case GLFW_KEY_F1:
            scene->cur_cam = 0u;
            break;
        case GLFW_KEY_F2:
            scene->cur_cam = 1u;
            break;
        case GLFW_KEY_0:
            scene->rendering_mode = (scene->rendering_mode == GL_LINE) ? GL_FILL : GL_LINE;
            break;
        }
    }
}

void handle_mouse(GLFWwindow *window,
                  int button,
                  int action,
                  int /*mods*/)
{
    if ((GLFW_MOUSE_BUTTON_LEFT == button) && (GLFW_RELEASE == action))
    {
        double xpos, ypos;
        glfwGetCursorPos(window, &xpos, &ypos);
        auto scene = reinterpret_cast<Scene*>(glfwGetWindowUserPointer(window));
        assert(scene);
        pick(static_cast<float>(xpos),
             static_cast<float>(ypos),
             scene);
    }
}

void handle_cursor(GLFWwindow *window,
                   double xoffset,
                   double yoffset)
{
    const float offset_x = static_cast<float>(xoffset), offset_y = static_cast<float>(yoffset);
    static float prev_x = 0.0f, prev_y = 0.0f;
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_LEFT_ALT) ||
        GLFW_PRESS == glfwGetKey(window, GLFW_KEY_RIGHT_ALT))
    {
        const auto delta_x = offset_x - prev_x, delta_y = offset_y - prev_y;
        auto *scene = reinterpret_cast<Scene*>(glfwGetWindowUserPointer(window));
        Camera *cam = scene->current_cam();
        cam->slide_X(-static_cast<float>(delta_x));
        cam->slide_Y(static_cast<float>(delta_y));
    }
    prev_x = offset_x, prev_y = offset_y;
}

void handle_scroll(GLFWwindow *window,
                   double /*xoffset*/,
                   double yoffset)
{
    auto *scene = reinterpret_cast<Scene*>(glfwGetWindowUserPointer(window));
    Camera *cam = scene->current_cam();
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) ||
        GLFW_PRESS == glfwGetKey(window, GLFW_KEY_RIGHT_CONTROL))
        cam->rotate_Y(static_cast<float>(yoffset) / 15.0f);
    else
        cam->slide_Z(static_cast<float>(-yoffset) * 20.0f);
}

void handle_input(GLFWwindow *window,
                  Scene *scene,
                  int64_t ticks)
{
    const auto slide_offset = static_cast<float>(ticks) * Camera::slide_offset;
    const auto rot_delta = static_cast<float>(ticks) * Camera::rot_angle;

    Camera* cam = scene->current_cam();
    // translation
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_LEFT))
        cam->slide_X(-slide_offset);
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_RIGHT))
        cam->slide_X(slide_offset);
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_DOWN))
        cam->slide_Y(-slide_offset);
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_UP))
        cam->slide_Y(slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_Z))
        cam->slide_Z(slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_S))
        cam->slide_Z(-slide_offset);

    // rotation
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_W))
        cam->rotate_X(rot_delta);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_X))
        cam->rotate_X(-rot_delta);
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_A))
        cam->rotate_Y(rot_delta);
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_D))
        cam->rotate_Y(-rot_delta);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_PERIOD))
        cam->rotate_Z(rot_delta);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_COMMA))
        cam->rotate_Z(-rot_delta);

    // FoV
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_T))
    {
        if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) ||
            GLFW_PRESS == glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT))
            cam->change_FoV(Camera::FoV_delta);
        else
            cam->change_FoV(-Camera::FoV_delta);
    }

    // rotate primary light
    if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_LEFT_BRACKET))
    {
        scene->lights[0].rotate(-Light::rot_delta, {1.0f, 0.0f, 0.0f});
    }
    else if (GLFW_PRESS == glfwGetKey(window, GLFW_KEY_RIGHT_BRACKET))
    {
        scene->lights[0].rotate(Light::rot_delta, {1.0f, 0.0f, 0.0f});
    }

    glfwPollEvents();
}

void window_error(int, const char *error_msg)
{
    std::cerr << error_msg << '\n';
    throw std::runtime_error(error_msg);
}

GLFW_window create_window(GLFW_wrapper &glfw)
{
    glfwSetErrorCallback(window_error);
    glfw = GLFW_wrapper(Check_return<decltype(&glfwInit),
                        glfwInit,
                        decltype(GL_TRUE),
                        GL_TRUE>{});
    // RAII warpping isn't mandatory here, since glfwTerminate destroys open windows, if any remain
    GLFW_window wnd_ptr{setup_context(), glfwDestroyWindow};

    ::check((ogl_LoadFunctions() == ogl_LOAD_SUCCEEDED), "Failed loading OpenGL symbols");

#ifndef NDEBUG
    setup_debug(true);
#endif
    return wnd_ptr;
}

} // unnamed namespace

#endif // WINDOW_HPP
