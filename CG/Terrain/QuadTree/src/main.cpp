/*
 * LEARNINGS:
 *  1. QuadTree construction with no pointer chasing only using simple math
 *  2. Calculation of frustum bounding sphere and cone
 *  3. Cone-sphere, frustum-sphere, frustum-OBB, point-OBB intersection tests
 *  4. Frustum culling and optimizations surrounding it from FlipCode
 *  5. Parsing QuadTree and rendering only competely or partially visible nodes
 *  6. Octant test for intersection tests involving polytopes like OBB or frustum
 *  7. TODO: Instead of taking the max_height/2 of the terrain as the height of all
 *     quadtree nodes, fitting it more tightly would give greater gains; this can be
 *     precomputed and loaded to make the quadtree
 *
 * REFERENCES:
 *  ₁ QuadTree workout 2
 *  ² Frustum sphere workout and flip code article cited below
 *  ³ Geometric Tools article and workouts: Circle-Trapezium Intesection, OBB_Point_Test
 *  ⁴ http://www.flipcode.com/archives/Frustum_Culling.shtml
 *  ⁵ http://www.gamedev.net/page/resources/_/technical/
 *         graphics-programming-and-theory/quadtrees-r1303
 *  ⁶ §16.14.2, RTR, 3/e
 */

#include "common.hpp"
#include "Util.hpp"
#include "GL_res_wrap.hpp"
#include "GL_util.hpp"
#include "Shader_util.hpp"
#include "Quadtree.hpp"
#include "Primitives.hpp"
#include "Light.hpp"
#include "Camera.hpp"
#include "main.hpp"
#include "SDL.hpp"
#include "Window.inl"
#include <chrono>

constexpr auto *scene_file = "data/map.sdl";

namespace
{

float get_hit_tri(Scene *scene,
                  const Primitives::Ray &ray,
                  const Primitives::Cell &hit_cell,
                  uint8_t *triangle_id)
{
    // perform ray-triangle intersection
    Primitives::Triangle tri[2];
    scene->height_map.get_triangles(hit_cell, &tri[0], &tri[1]);
    uint8_t tri_id = 2u;
    float t = std::numeric_limits<float>::infinity();
    if (Primitives::ray_triangle_intersect(ray, tri[0], &t))
    {
        tri_id = 0;
    }
    else if (Primitives::ray_triangle_intersect(ray, tri[1], &t))
    {
        tri_id = 1;
    }
    assert(tri_id < 2);
    *triangle_id = tri_id;
    return t;
}

bool hit_test_cell(Scene *scene,
                   const Primitives::Ray &ray,
                   const glm::vec2 &entry,
                   const glm::vec2 &exit,
                   float t_exit,
                   Primitives::Cell *hit_cell)
{
    // here entry.y and exit.y are really z values since height is denoted by y
    // z denotes the top and bottom in the grid when viewed along Y axis from top
    // since these are going to be array indices they cannot be < 0
    assert(entry.x >= 0.0f && entry.y >= 0.0f);
    assert(exit.x >= 0.0f && exit.y >= 0.0f);

    const auto exit_x = scene->height_map.index_to_x(exit.x);
    const auto exit_z = scene->height_map.index_to_z(exit.y);
    const auto ray_exit_ht = std::fma(t_exit, ray.direction.y, ray.origin.y);
    const auto map_exit_ht = scene->height_map.height_at(exit_x, exit_z);

    // TODO: when camera is underneath the terrain surface, inverting this condition (>) is required
    // as the ray would originate below the surface and it should be tested for when it'll go above it
    const bool hit = ray_exit_ht <= map_exit_ht;
#ifndef DEBUG_SELECTION
    if (hit)
#endif
    {
        // TODO: As of 0.9.5.3 GLM has float x, y, z, w; for vec4 and not float[4], thus such access is not strictly
        // portable due to struct padding
        const glm::vec4 ltrb = Primitives::left_top_right_bottom(entry, exit);
        const auto left = static_cast<size_t>(floor_if_not_int(ltrb[0]));
        const auto top = static_cast<size_t>(floor_if_not_int(ltrb[1]));
        *hit_cell = {left, top};
    }
#ifdef DEBUG_SELECTION
    // if debugging selection, add triangles of every cell visited
    if (!hit)
    {
        scene->sel_cells_dbg.insert(scene->sel_cells_dbg.end(), {*hit_cell, *hit_cell});
        scene->sel_tris_dbg.insert(scene->sel_tris_dbg.end(), {0, 1});
    }
#endif  // DEBUG_SELECTION

    return hit;
}

void reset_selection(Scene *scene)
{
    scene->sel_tri = 2u;
#ifdef DEBUG_SELECTION
    scene->sel_cells_dbg.clear();
    scene->sel_tris_dbg.clear();
#endif  // DEBUG_SELECTION
}

float hit_test_height_map(Scene *scene, const Primitives::Ray &ray)
{
    // TODO: it would be simpler and more performant if the traversal and hit testing happen in the same space.

    // cell_traverse happens in a space where the rows and columns are in integer points
    // however the height map might have scaling, hence transforming ray to the unit grid space
    const glm::vec3 grid_ray_origin = {
                                        scene->height_map.x_to_index(ray.origin.x),
                                        scene->height_map.y_to_index(ray.origin.y),
                                        scene->height_map.z_to_index(ray.origin.z)
                                      };
    const auto end_point = ray.origin + ray.direction;
    const glm::vec3 grid_ray_end = {
                                     scene->height_map.x_to_index(end_point.x),
                                     scene->height_map.y_to_index(end_point.y),
                                     scene->height_map.z_to_index(end_point.z)
                                   };
    Primitives::Ray grid_ray = { grid_ray_origin, grid_ray_end - grid_ray_origin };

    float t = std::numeric_limits<float>::infinity();

    Primitives::Cell hit_cell;
    if (Primitives::grid_traverse(grid_ray,
                                  [&](const glm::vec2 &entry, const glm::vec2 exit, float t)
                                  {
                                      return hit_test_cell(scene, ray, entry, exit, t, &hit_cell);
                                  }))
    {
        // entering here confirms that a hit was made by the ray on hit_cell
        uint8_t tri_id;
        t = get_hit_tri(scene, ray, hit_cell, &tri_id);
        scene->sel_cell = hit_cell;
        scene->sel_tri = tri_id;
    }
    return t;
}

void init_rendering()
{
    // this is set by default
    //glViewport(0, 0, screen_width, screen_height);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // enabling culling culls out the plane when trying to view it from underneath; the reason is that a plane,
    // unlike a model, has no inside and hence culling it doesn't make sense. If you want planes to be visible
    // in all directions then disable this
    glEnable(GL_CULL_FACE);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    // enabling this avoids clipping by near and far planes but any fragments that would have
    // normalized z coordinates outside of the [-1, 1] range will be clamped to that range.
    // It screws up depth tests between fragments that are being clamped, but usually those
    // objects are far away.
    glEnable(GL_DEPTH_CLAMP);
    // https://www.khronos.org/opengl/wiki/Multisampling
    glEnable(GL_MULTISAMPLE);
}

void upload_meshes(const std::vector<Primitives::Mesh> &meshes,
                   std::vector<GL::VAO> *VAOs)
{
    VAOs->reserve(meshes.size());
    for(size_t i = 0u; i < meshes.size(); ++i)
    {
        GL::Buffer vbo = GL::upload_data(meshes[i].vertices, GL_ARRAY_BUFFER);
        GL::Buffer ibo = GL::upload_data(meshes[i].indices, GL_ELEMENT_ARRAY_BUFFER);
        VAOs->emplace_back(GL::setup_attributes(vbo,
                                                Primitives::Mesh::type_id,
                                                Primitives::Mesh::elem_lens,
                                                ibo));
    }
}

void set_uniforms(GLuint program, const Scene &scene)
{
    constexpr auto MAX_UNIFORM_NAME = 200;

    const auto PV_xform_id = GL::uniform_loc(program, "PV");
    auto PV = scene.current_cam()->projection * scene.current_cam()->view_xform;
    glUniformMatrix4fv(PV_xform_id, 1, GL_FALSE, &PV[0][0]);

    for (auto i = 0u; i < scene.lights.size(); ++i)
    {
        const char *light_dir = "lights[%u].direction";
        const char *light_amb = "lights[%u].ambient";
        const char *light_dif = "lights[%u].diffuse";
        char uniform_name[MAX_UNIFORM_NAME] = {};
        snprintf(uniform_name, MAX_UNIFORM_NAME, light_dir, i);
        const auto light_dir_id = GL::uniform_loc(program, uniform_name);
        snprintf(uniform_name, MAX_UNIFORM_NAME, light_amb, i);
        const auto light_amb_id = GL::uniform_loc(program, uniform_name);
        snprintf(uniform_name, MAX_UNIFORM_NAME, light_dif, i);
        const auto light_diffuse_id = GL::uniform_loc(program, uniform_name);

        glUniform3fv(light_dir_id, 1, &scene.lights[i].direction[0]);
        glUniform4fv(light_amb_id, 1, &scene.lights[i].ambient[0]);
        glUniform4fv(light_diffuse_id, 1, &scene.lights[i].diffuse[0]);
    }

    const auto lights_used_id = GL::uniform_loc(program, "lights_used");
    glUniform1ui(lights_used_id, scene.lights.size());
}

void set_mesh_uniforms(GLuint program,
                       const Primitives::Mesh &mesh,
                       const std::vector<GL::Texture> &textures)
{
    const auto M_xform_id = GL::uniform_loc(program, "M");
    glUniformMatrix4fv(M_xform_id, 1, GL_FALSE, &mesh.model_xform[0][0]);

    const auto txr_cnt = mesh.textures.size();
    std::vector<GLint> tex_units;
    for (auto j = 0u; j < txr_cnt; ++j)
    {
        glActiveTexture(GL_TEXTURE0 + j);
        glBindTexture(GL_TEXTURE_2D, textures[mesh.textures[j]]);
        tex_units.push_back(tex_units.size());
    }
    if (txr_cnt)
    {
        const auto tex_unit = GL::uniform_loc(program, "tex_unit");
        glUniform1iv(tex_unit, txr_cnt, tex_units.data());
        const auto tex_used = GL::uniform_loc(program, "tex_used");
        glUniform1ui(tex_used, txr_cnt);
    }
}

#ifdef FRUSTUM_CULLING

void test_chunks(const Primitives::HeightMap &ht_map,
                 const Camera &cam,
                 std::vector<glm::uvec2> &visible)
{
    Primitives::Frustum f;
    if (!cam.frustum(&f))
        return;

    visible.clear();
    // use a queue to do a breadth-first traversal of the quadtree
    std::queue<uint16_t> to_visit;  // nodes to be visited, assume root passes hit test
    to_visit.push(0u);
    auto &quad = ht_map.partitions;
    const auto tree_depth = quad.max_depth();
    while (!to_visit.empty())
    {
        const uint16_t node = to_visit.front();
        to_visit.pop();
        const bool is_leaf = quad.is_descendant(node, static_cast<uint8_t>(tree_depth - 1u));
        const auto &bs = quad.node_spheres[node];
        Primitives::X_state state = Primitives::sphere_sphere_intersect(bs, f.s);
        state = ((!!state) && Primitives::cone_sphere_intersect(f.c, bs)) ?
                    Primitives::X_state::Intersecting : Primitives::X_state::Disjoint;
        if (!!state)
        {
            glm::vec3 bv_centre = glm::vec3(cam.view_xform * glm::vec4(bs.centre, 1.0f));
            Primitives::view_to_frustum_space(bv_centre, f);
            const auto test_planes = Primitives::point_octant_planes(bv_centre);
            // test bounding sphere and OBB against frustum's possible plans from octant test
            state = Primitives::frustum_sphere_intersect(f, bs, test_planes);
            state = (!!state) ? Primitives::frustum_OBB_intersect(f, quad.node_OBB[node], test_planes) : state;
        }
        // intersecting or fully-in
        if (!!state)
        {
            if (is_leaf)
                visible.push_back(quad.index_2D(node, tree_depth));
            else
            {
                if (state == Primitives::X_state::Within)
                {
                    uint16_t leaves;
                    size_t count = quad.descendant_leaf_offset(node, &leaves);
                    for (uint16_t i = 0u; i < count; ++i)
                        visible.push_back(quad.index_2D(static_cast<uint16_t>(leaves + i), tree_depth));
                }
                else
                {
                    const auto offset = quad.child_offset(node);
                    to_visit.push(offset);
                    to_visit.push(static_cast<uint16_t>(offset + 1u));
                    to_visit.push(static_cast<uint16_t>(offset + 2u));
                    to_visit.push(static_cast<uint16_t>(offset + 3u));
                }
            }
        }
    }
}

#endif  // FRUSTUM_CULLING

void render_chunk(size_t col,
                  size_t row,
                  const Primitives::HeightMap &height_map,
                  const Primitives::Mesh &mesh)
{
    auto grid_row_id = Primitives::HeightMap::chunk_cells * row;
    const auto grid_col_id = Primitives::HeightMap::chunk_cells * col;
    auto &draw_data = *mesh.multi_draw;
    std::generate(draw_data.base_verts.begin(), draw_data.base_verts.end(), [&] {
        return (height_map.cols * grid_row_id++) + grid_col_id; });
    const auto chunk_type = height_map.chunk_type(col, row);
    const bool dummy_col = (chunk_type == Primitives::HeightMap::ChunkType::Dummy_Col) ||
                           (chunk_type == Primitives::HeightMap::ChunkType::Dummy_Corner);
    const bool dummy_row = (chunk_type == Primitives::HeightMap::ChunkType::Dummy_Row) ||
                           (chunk_type == Primitives::HeightMap::ChunkType::Dummy_Corner);
    // avoid drawing the last column cell since it isn't there (dummy)
    if (dummy_col)
    {
        std::for_each(draw_data.counts.begin(), draw_data.counts.end(), [&](GLsizei &count)
        {
            count -= 2;
        });
    }
    const GLsizei row_cell_count = draw_data.counts.size() - (dummy_row ? 1u : 0u);
    // refer Terrain/HeightMap workout for comments on glMultiDrawElementsBaseVertex
    glMultiDrawElementsBaseVertex(mesh.primitive_type,
                                  draw_data.counts.data(),
                                  GL::type_id(decltype(mesh.indices)::value_type{}),
                                  draw_data.offsets.data(),
                                  row_cell_count,
                                  draw_data.base_verts.data());

    // The above call translate to this internally; useful for debugging
    // for (auto i = 0; i < row_cell_count; ++i)
    //     glDrawElementsBaseVertex(mesh.primitive_type,
    //                              draw_data.counts[i],
    //                              GL_UNSIGNED_INT,
    //                              reinterpret_cast<GLvoid const* const*>(draw_data.offsets[i]),
    //                              draw_data.base_verts[i]);

    // revert changes made
    if (dummy_col)
    {
        std::for_each(draw_data.counts.begin(), draw_data.counts.end(), [&](GLsizei &count) {
            count += 2; });
    }
}

void render(GLuint program,
            Scene &scene,
            const std::vector<GL::VAO> &VAOs)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPolygonMode(GL_FRONT_AND_BACK, scene.rendering_mode);
    const auto mesh_count = scene.meshes.size();
    for(auto i = 0u; i < mesh_count; ++i)
    {
        glBindVertexArray(VAOs[i]);
        GL::validate_program(program);
        glUseProgram(program);
        set_uniforms(program, scene);

        set_mesh_uniforms(program, scene.meshes[i], scene.textures);
        if (scene.meshes[i].multi_draw)
        {
#if FRUSTUM_CULL
            test_chunks(scene.height_map, *scene.current_cam(), scene.visible_chunks);
            for (const auto chunk : scene.visible_chunks)
                render_chunk(chunk.x, chunk.y, scene.height_map, scene.meshes[i]);
#else
            const auto chunks = scene.height_map.chunk_count();
            for (auto r = 0u; r < chunks; ++r)
                for (auto c = 0u; c < chunks; ++c)
                    render_chunk(c, r, scene.height_map, scene.meshes[i]);
#endif
        }

        // no need to activate each texture unit and unbind the texture from the target
        // this should automatically happen when a new texture is bound
        //glBindTexture(GL_TEXTURE_2D, 0);
        glBindVertexArray(0);
    }
    glUseProgram(0);
}

void render_sel(GLuint program,
                const Scene &scene,
                const std::vector<GL::VAO> &VAOs)
{
    // validate selection
    if (scene.sel_tri < 2u)
    {
        glPolygonMode(GL_FRONT_AND_BACK, scene.rendering_mode);
        auto PV = scene.current_cam()->projection * scene.current_cam()->view_xform;
        // FIXME: hack in directly indexing height map's mesh
        // should actually be queried from the scene.height_map
        glBindVertexArray(VAOs[0]);

        GL::validate_program(program);
        glUseProgram(program);

        const auto PV_xform_id = GL::uniform_loc(program, "PV");
        glUniformMatrix4fv(PV_xform_id, 1, GL_FALSE, &PV[0][0]);
        const auto M_xform_id = GL::uniform_loc(program, "M");
        glUniformMatrix4fv(M_xform_id, 1, GL_FALSE, &scene.meshes[0].model_xform[0][0]);
        const auto colour_id = GL::uniform_loc(program, "solidColour");
        const glm::vec4 sel_colour{1.0f, 0.0f, 0.0f, 1.0f};
        glUniform4fv(colour_id, 1, &sel_colour[0]);

        using Index_type = decltype(scene.meshes[0].indices)::value_type;
#ifdef DEBUG_SELECTION
        for (auto i = 0u; i < scene.sel_tris_dbg.size(); ++i)
        {
            const auto base = (scene.sel_cells_dbg[i].y * scene.height_map.cols) + scene.sel_cells_dbg[i].x;
            const auto offset = scene.sel_tris_dbg[i] * sizeof(Index_type); // offset inside the index buffer, in bytes
            glFrontFace(GL_CCW - scene.sel_tris_dbg[i]);    // if sel_tri == 0, then CCW, else CCW - 1 = CW
            glDrawElementsBaseVertex(scene.meshes[0].primitive_type,
                                     3,
                                     GL::type_id(Index_type{}),
                                     reinterpret_cast<GLvoid*>(offset),
                                     base);
            glFrontFace(GL_CCW);
        }
#endif  // DEBUG_SELECTION

        // render selection
        const auto base = (scene.sel_cell.y * scene.height_map.cols) + scene.sel_cell.x;
        const auto offset = scene.sel_tri * sizeof(Index_type); // offset inside the index buffer, in bytes
        /*
         * The index data is such
         *     0 1 2 3 4
         *     5 6 7 8 9
         * When triangle strip is used, the resulting triangles' winding order is determined by the first triangle's
         * winding order. Also GL autmatically "fixes" every second triangle formed to confirm with the winding formed
         * by the first. So in this case, if 0 5 1 was the first triangle then the second, although would be 5 1 6, GL
         * would swap the first two indices resulting in 1 5 6, but for the next triangle, it'll be 1 6 2. Had the
         * first triangle been 5 0 6 (CW) then GL would make sure that all resulting triangles turn out with CW winding:
         * 6 0 1, 6 1 2, etc.
         * Since we're re-using the index data that was generated for rendering a triangle strip to render a single
         * triangle in-between instead of starting from the first and giving one additional index per new triangle,
         * we've to take care of the winding somehow since OpenGL can't fix it for us, since from GL's viewpoint this
         * is the first triangle starting the strip (the same even if GL_TRIANGLES was use). If we're enabling facet
         * [glEnable(GL_FACE_CULLING)] culling with the back face to be the culled one [glCullFace(GL_BACK)] and the
         * front face being set as the triangles with CCW winding [glFrontFace(GL_CCW)], then this poses no problem
         * for even tringles, since they too are CCW winding in our case. However, for the lower triangle in a cell,
         * say cell 0, we've to offset the index buffer at 1 and count = 3 i.e. starting at 5 leading to 5 1 6 which
         * results in a CW wound triangle.
         *
         * We've 3 options to remedy this: (i) turn off culling just for this draw call and turn it back on
         * (ii) tell GL to cull the front and not the back face, again just for this draw call (iii) set the
         * winding order that determines which is the front face from CCW to CW, only for this draw call and
         * revert it post call; choosing (iii) since it is the most local fix
         */
        glFrontFace(GL_CCW - scene.sel_tri);    // if sel_tri == 0, then CCW, else CCW - 1 = CW
        glDrawElementsBaseVertex(scene.meshes[0].primitive_type,
                                 3,
                                 GL::type_id(Index_type{}),
                                 reinterpret_cast<GLvoid*>(offset),
                                 base);
        glFrontFace(GL_CCW);
        glBindVertexArray(0);
        glUseProgram(0);
    }
}

void update_frame_time(std::chrono::milliseconds *ticks,
                       unsigned *frames_rendered,
                       GLFWwindow *window)
{
    ++*frames_rendered;
    if(ticks->count() >= 1000)
    {
        std::ostringstream ss;
        ss << app_title << " ~ " << (1000.0 / *frames_rendered) << "ms/frame";
        const auto str_mpf = ss.str();
        glfwSetWindowTitle(window, str_mpf.c_str());
        *frames_rendered = 0;
        *ticks = std::chrono::milliseconds{0};
    }
}

}   // unnamed namespace

void pick(float x, float y, Scene *scene)
{
    reset_selection(scene);
    /*
     * for the z (world) coordinate of the pick point we need the focal length
     * which is cot (FoV/2) but since the perspective projection matrix already has it
     * we can reuse the value instead of computing it; this optimisation is discussed in
     * Frank D. Luna's excellent book; it explains picking quite comprehensively
     */
    const auto focal_length = scene->current_cam()->projection[1][1];
    // const auto focal_length = glm::cot(scene->FoV / 2.0f);

    /*
     * in view space this point generated would be (x, y, z, 1); in that space
     * subtracting origin (0, 0, 0, 1) from that point would give the same point
     * with the w coordinate becoming 0; thus this is the ray in view space
     */
    const glm::vec4 view_ray(((2.0f * x / scene->current_cam()->width) - 1.0f) * scene->current_cam()->aspect_ratio,
                             1.0f - (2.0f * y / scene->current_cam()->height),
                             -focal_length,
                             0.0f);
    const auto &inv_view = scene->current_cam()->view_world_xform;
    // since we're transforming a ray, the last column doesn't matter, but we use it for ray origin
    const auto ray_dir = glm::normalize(glm::vec3(inv_view * view_ray));
    const auto ray_org = glm::vec3(inv_view[3]);    // camera locaton in world space
    const Primitives::Ray pick_ray = Primitives::clip_ray({ray_org, ray_dir},
                                                           scene->height_map.min,   // AABB with min and max
                                                           scene->height_map.max);
    /*
     * this ray originates from camera, goes throw the click point on the view plane and extends to infinity;
     * however, bounding it within min and max terrain height is optimal since there can't be vertices above
     * or below those limits.
     */
    // TODO: if the ray only touches an edge / corner of the AABB (formed by height_map.min and max), then
    // too we'd have a degenerate ray, however grid ray tracing isn't required in that case and just a hit
    // test of a triangle would do

    if (!Primitives::is_degenerate(pick_ray))
    {
        hit_test_height_map(scene, pick_ray);
    }
}

void Scene::setup_camera(float width, float height)
{
    assert(!height_map.heights.empty());
    // slightly higher than the possibly tallest peak for an 8-bit grayscale height map
    const auto min_height_to_terrain = height_map.max.y + 50.0f;
    cam[0].reset(new RTSCamera{FoV, width, height, near, far, min_height_to_terrain});
    cam[1].reset(new FreeCamera{FoV, width, height, near, far});

    // reserve space for visible chunks post frustum culling
    visible_chunks.reserve(height_map.partitions.nodes.size() / 4u);
}

int main(int /*argc*/, char** /*argv*/)
{
    GLFW_wrapper glfw;
    auto glfw_wnd = create_window(glfw);
    Scene scene;
    Util::parse_SDL(scene_file,
                    &scene);
    // TODO: instead of hard-coding this, see if this can be queried from the program object
    ::check(scene.lights.size() < 5, "Too many lights to handle for the fragment shader!");
    ::check(scene.textures.size() < 5, "Too many textures to handle for the fragment shader!");

    int w, h;
    glfwGetWindowSize(glfw_wnd.get(), &w, &h);
    float width = static_cast<float>(w), height = static_cast<float>(h);
    scene.setup_camera(width, height);

    std::vector<GL::VAO> VAOs;
    upload_meshes(scene.meshes, &VAOs);

    // http://www.parashift.com/c++-faq-lite/memfnptr-vs-fnptr.html
    // according to C++ FAQ, you cannot pass a member function to a C callback; passing a functor, lambda, boost::bind,
    // etc. is for a C++ callback implemented with templates; instead use the void* user_data allowed in the callback;
    // GLFW gives GLFWwindow->SetWindowUserPointer for the same
    glfwSetWindowUserPointer(glfw_wnd.get(), reinterpret_cast<void*>(&scene));
    glfwSetKeyCallback(glfw_wnd.get(), handle_key);
    glfwSetMouseButtonCallback(glfw_wnd.get(), handle_mouse);
    glfwSetCursorPosCallback(glfw_wnd.get(), handle_cursor);
    glfwSetScrollCallback(glfw_wnd.get(), handle_scroll);

    GL::Program program{GL::setup_program("shaders/pos.vert",
                                          "shaders/tex.frag")};
    GL::Program program_sel{GL::setup_program("shaders/pos.vert",
                                              "shaders/solid.frag")};
    init_rendering();
    unsigned frames = 0u;
    std::chrono::milliseconds since_last_sec;
    std::chrono::steady_clock::time_point now, earlier;
    while(!glfwWindowShouldClose(glfw_wnd.get()))
    {
        now = std::chrono::steady_clock::now();
        auto const elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(now - earlier);
        auto const ticks = elapsed.count();
        render(program, scene, VAOs);
        render_sel(program_sel, scene, VAOs);
        glfwSwapBuffers(glfw_wnd.get());
        handle_input(glfw_wnd.get(), &scene, ticks);
        since_last_sec += elapsed;
        update_frame_time(&since_last_sec, &frames, glfw_wnd.get());
        earlier = now;
    }
}
