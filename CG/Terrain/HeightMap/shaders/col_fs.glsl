#version 330

uniform float max_height;

in vec3 pos;
out vec4 colour;

void main()
{
    colour = vec4(0.0, pos.y / max_height, 0.0, 1.0);
}
