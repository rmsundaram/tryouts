#version 330

uniform mat4 PVM;

layout (location = 0) in vec3 position;
out vec3 pos;

void main()
{
    gl_Position = PVM * vec4(position, 1.0f);
    pos = position;
}
