#ifndef UTIL_HPP
#define UTIL_HPP

namespace Array
{

template <typename T>
inline constexpr typename std::enable_if<std::is_array<T>::value, size_t>::type
max_index(const T& /*arr*/)
{
    return std::extent<T>::value - 1;
}

inline
size_t item_index(size_t item_id,
                  size_t base_id,
                  size_t max_id,
                  size_t err_index)
{
    const auto max_index = max_id - base_id;
    int index = item_id - base_id;
    return ((index < 0) || (static_cast<unsigned>(index) > max_index)) ?
            err_index :
            static_cast<size_t>(index);
}

}   // namespace Array

inline void check(bool condition, const char *error_msg)
{
    if(condition != true)
    {
        throw std::runtime_error(error_msg);
    }
}

inline std::ostream& tab(std::ostream &os)
{
    return os << '\t';
}

template <typename T>
typename std::enable_if<std::is_integral<T>::value, bool>::type
is_power_of_2(T v)
{
    return (v && !(v & (v - 1)));
}

template <typename T>
typename std::enable_if<std::is_floating_point<T>::value, bool>::type
float_less(T f1, T f2)
{
    return (f1 < f2) && glm::epsilonNotEqual(f1, f2, glm::epsilon<T>());
}

template <typename T>
struct Vec_compare
{
    using FT = typename T::value_type;
    bool operator()(const T &a, const T &b) const
    {
        return std::lexicographical_compare(&a[0], &a[0] + a.length(),
                                            &b[0], &b[0] + b.length(),
                                            float_less<FT>);
    }
};

#endif // UTIL_HPP
