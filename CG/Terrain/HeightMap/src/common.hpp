#ifndef __COMMON_HPP__
#define __COMMON_HPP__

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/euler_angles.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/epsilon.hpp>
#include <glm/gtc/reciprocal.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include <memory>
#include <vector>
#include <map>
#include <numeric>
#include <algorithm>
#include <string>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <utility>
#include <functional>
#include <cassert>
#include <iterator>

#endif  // __COMMON_HPP__
