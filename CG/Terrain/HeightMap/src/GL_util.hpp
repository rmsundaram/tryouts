#ifndef GL_UTIL_HPP
#define GL_UTIL_HPP

namespace GL
{

template <typename T>
constexpr GLenum type_id(T);

template <>
constexpr GLenum type_id(GLfloat)
{
    return GL_FLOAT;
}

template <>
constexpr GLenum type_id(GLubyte)
{
    return GL_UNSIGNED_BYTE;
}

template <>
constexpr GLenum type_id(GLushort)
{
    return GL_UNSIGNED_SHORT;
}

template <>
constexpr GLenum type_id(GLuint)
{
    return GL_UNSIGNED_INT;
}

//GLenum indices_type(size_t vertex_count)
//{
//    assert((vertex_count - 1) <= std::numeric_limits<uint32_t>::max());

//    if((vertex_count - 1) <= std::numeric_limits<uint8_t>::max())
//        return GL_UNSIGNED_BYTE;
//    else if((vertex_count - 1) <= std::numeric_limits<uint16_t>::max())
//        return GL_UNSIGNED_SHORT;
//    else// if((vertex_count - 1) <= std::numeric_limits<uint32_t>::max())
//        return GL_UNSIGNED_INT;
//}

// returns a VBO
template <typename T>
Buffer upload_data(const std::vector<T> &data, GLenum target);

// returns a VAO
template <typename T, size_t attribs>
VAO setup_attributes(GLuint vertex_buffer,
                     const size_t (&attrib_length) [attribs],
                     GLuint index_buffer);

// returns a texture
inline
Texture upload_texture(const char *file_name, GLenum texture_unit);

}   // namespace GL

#include "GL_util.inl"

#endif // GL_UTIL_HPP
