#include "common.hpp"
#include "Util.hpp"
#include "Primitives.hpp"

namespace
{

GLushort add_terrain_vertex(size_t row, size_t col,
                            float start_x, float start_z,
                            float step_x, float step_z,
                            Primitives::HeightMap *height_map,
                            std::vector<glm::vec3> *vertices,
                            std::map<glm::vec3, size_t, Vec_compare<glm::vec3>> *verts)
{
    auto x = start_x + (step_x * static_cast<float>(col));
    auto y = height_map->height_at(row, col);
    auto z = start_z + (step_z * static_cast<float>(row));
    const glm::vec3 V{x, y, z};
    auto res = verts->emplace(V, vertices->size());
    if(res.second)
    {
        vertices->push_back(V);
    }
    assert(vertices->size() < std::numeric_limits<GLushort>::max());
    return static_cast<GLushort>((res.first)->second);
}

void make_terrain_mesh(Primitives::HeightMap *height_map,
                       std::vector<Primitives::Mesh> *meshes)
{
    constexpr auto indices_per_cell = 6u;   // 3 per triangle
    constexpr auto step_x = 13.3333f;
    constexpr auto step_z = step_x;

    const auto cells_z = height_map->rows - 1;
    const auto cells_x = height_map->cols - 1;
    const auto start_z = (static_cast<float>(cells_z) * step_z) / -2.0f;
    const auto start_x = (static_cast<float>(cells_x) * step_x) / -2.0f;

    meshes->push_back(Primitives::Mesh{{}, {}, {}, glm::mat4(1.0f)});
    auto &m = meshes->back();

    m.vertices.reserve(height_map->heights.size());
    m.colour = glm::vec4(0.0f, 0.8f, 0.0f, 1.0f);

    // How can a terrain be rendered with glMultiDrawElementsBaseVertex?
    // https://stackoverflow.com/q/26755248/183120
#if defined(BASE_VERTEX) || defined(MULTI_BASE_VERTEX)
    /*         0          1          2           3           4
     *         +----------+----------+-----------+-----------+
     *         |          |          |           |           |
     *       5 |----------+----------+-----------+-----------| 9
     *         |          |          |           |           |
     *      10 |----------+----------+-----------+-----------|
     *         |          |          |           |           |
     *      15 +----------+----------+-----------+-----------+
     *
     *  Vertices are given in order: 0, 1, 2, … while indices are given in alternating order like {0, 5, 1, 6, 2, 7, …}
     *  for triangle strip to work. Indices are given only for one horizontal strip, the rest are drawn using the same
     *  with a base vertex offset e.g. for the second draw call, a base offset of 5 would be {5, 10, 6, 11, 7, 12, …}
     */
    m.indices.reserve((cells_x + 1) * 2);
    for (auto row = 0u; row < height_map->rows; ++row)
    {
        for (auto col = 0u; col < height_map->cols; ++col)
        {
            auto x = start_x + (step_x * static_cast<float>(col));
            auto y = height_map->height_at(row, col);
            auto z = start_z + (step_z * static_cast<float>(row));
            m.vertices.emplace_back(x, y, z);
        }
    }
    for (auto i = 0u; i < height_map->cols; ++i)
    {
        m.indices.push_back(i);
        m.indices.push_back(i + height_map->cols);
    }
#else
    m.indices.reserve(cells_x * cells_z * indices_per_cell);
    std::map<glm::vec3, size_t, Vec_compare<glm::vec3>> vert_idx_map;
    for(auto row = 0u; row < cells_z; ++row)
    {
        for(auto col = 0u; col < cells_x; ++col)
        {
            const auto idx0 = add_terrain_vertex(row, col,
                                                 start_x, start_z,
                                                 step_x, step_z,
                                                 height_map,
                                                 &m.vertices,
                                                 &vert_idx_map);
            const auto idx1 = add_terrain_vertex(row + 1, col,
                                                 start_x, start_z,
                                                 step_x, step_z,
                                                 height_map,
                                                 &m.vertices,
                                                 &vert_idx_map);
            const auto idx2 = add_terrain_vertex(row, col + 1,
                                                 start_x, start_z,
                                                 step_x, step_z,
                                                 height_map,
                                                 &m.vertices,
                                                 &vert_idx_map);
            const auto idx3 = add_terrain_vertex(row + 1, col + 1,
                                                 start_x, start_z,
                                                 step_x, step_z,
                                                 height_map,
                                                 &m.vertices,
                                                 &vert_idx_map);
            m.indices.insert(m.indices.end(), {idx0, idx1, idx2, idx2, idx1, idx3});
        }
    }
#endif  // defined(BASE_VERTEX) || defined(MULTI_BASE_VERTEX)
}

void parse_height_map(std::ifstream &ifile,
                      Primitives::HeightMap *height_map,
                      std::vector<Primitives::Mesh> *meshes)
{
    const auto &rows = height_map->rows, &cols = height_map->cols;
    height_map->heights.reserve(rows * cols);
    float max_height = -std::numeric_limits<float>::min();
    for(auto i = 0u; i < rows; ++i)
    {
        std::string line;
        std::getline(ifile, line);
        std::istringstream istr{line};
        float height;
        while(istr >> height)
        {
            assert(height >= 0.0f);
            if(max_height < height)
                max_height = height;
            height_map->heights.push_back(height);
        }
        assert((height_map->heights.size() % cols) == 0);
    }
    height_map->max_height = max_height;
    make_terrain_mesh(height_map, meshes);
}

}   // unnamed namespace

float Primitives::HeightMap::height_at(size_t row, size_t col)
{
    assert((row < rows) && (col < cols));
    return heights[row * cols + col];
}

void Primitives::parse_SDL(const char *file_path,
                           Primitives::HeightMap *height_map,
                           std::vector<Primitives::Mesh> *meshes)
{
    assert(file_path && height_map && meshes);

    std::ifstream sdl_file{file_path};
    for(std::string line; std::getline(sdl_file, line); )
    {
        std::istringstream istr{line};
        char code;
        if (istr >> code)
        {
            switch(code)
            {
                case 'h':
                {
                    istr >> height_map->rows >> height_map->cols;
                    parse_height_map(sdl_file, height_map, meshes);
                    break;
                }
            }
        }
    }
}
