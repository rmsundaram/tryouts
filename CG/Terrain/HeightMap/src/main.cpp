/*
 * Techniques learnt:
 *  1. First height map rendering from hand-coded data
 *     http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=8
 */

#include "common.hpp"
#include "Util.hpp"
#include "Dataless_wrapper.hpp"
#include "GL_res_wrap.hpp"
#include "GL_util.hpp"
#include "Shader_util.hpp"
#include "Primitives.hpp"
#include "main.hpp"

namespace
{

using GLFW_wrapper = Dataless_wrap<decltype(&glfwTerminate), glfwTerminate>;

constexpr unsigned screen_width  = 1024u, screen_height = 600u;
constexpr float aspect_ratio = static_cast<float>(screen_width) / screen_height;
const float FoV = glm::quarter_pi<float>();
constexpr float near = 0.001f;
constexpr float far = 1000.0f;
const char* const app_title = "Height Map";

GLFWwindow* setup_context(uint32_t width, uint32_t height)
{
    /*
     * without setting the version (defaulting to 1.0) or setting it < 3.2, requesting for core
     * will fail since context profiles only exist for OpenGL 3.2+; likewise forward compatibility
     * only exist from 3.0 onwards. 3.0 marked deprecated, 3.1 removed deprecated (except
     * wide lines), 3.2 reintroduced deprecated under compatibility profile
     */
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE,
                   GLFW_OPENGL_CORE_PROFILE);
    // this also needs enabling of multisampling: glEnable(GL_MULTISAMPLE)
    glfwWindowHint(GLFW_SAMPLES, 4);

   /*
    * since core only has undeprecated features (except wide lines), making the context
    * forward-compatible is moot; only wide lines, the lone deprecated feature in core profile,
    * will be removed
    * http://www.opengl.org/wiki/Core_And_Compatibility_in_Contexts#Forward_compatibility
    */
#ifdef __APPLE__
    // on macOS this bit needs to be set to get a modern OpenGL context
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
#endif

    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

#ifndef NDEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

    auto window = glfwCreateWindow(width,
                                   height,
                                   app_title,
                                   nullptr,
                                   nullptr);
    ::check(window != nullptr, "Failed to create window");
#ifndef NDEBUG
    int w, h;
    glfwGetWindowSize(window, &w, &h);
    assert((w == static_cast<signed>(width)) && (h == static_cast<signed>(height)));
#endif
    glfwMakeContextCurrent(window);
    return window;
}

#ifndef NDEBUG

APIENTRY
void gl_debug_logger(GLenum source,
                     GLenum type,
                     GLuint id,
                     GLenum severity,
                     GLsizei /*length*/,
                     const GLchar *msg,
                     const void *file_ptr)
{
    std::ostringstream ss;
    ss << "GLError 0x"
       << std::hex << id << std::dec
       << ": "
       << msg <<
       " [source=";

    const char *sources[] = {
                                "API",
                                "WINDOW_SYSTEM",
                                "SHADER_COMPILER",
                                "THIRD_PARTY",
                                "APPLICATION",
                                "OTHER",
                                "UNDEFINED"
                            };
    size_t index = Array::item_index(source,
                                     GL_DEBUG_SOURCE_API_ARB,
                                     GL_DEBUG_SOURCE_OTHER_ARB,
                                     Array::max_index(sources));
    ss << sources[index];
    if (Array::max_index(sources) == index)
    {
        ss << " (" << source << ")";
    }

    const char *types[] = {
                              "ERROR",
                              "DEPRECATED_BEHAVIOR",
                              "UNDEFINED_BEHAVIOR",
                              "PORTABILITY",
                              "PERFORMANCE",
                              "OTHER",
                              "UNDEFINED"
                          };
    index = Array::item_index(type,
                              GL_DEBUG_TYPE_ERROR_ARB,
                              GL_DEBUG_TYPE_OTHER_ARB,
                              Array::max_index(types));
    ss << " type=" << types[index];
    if (Array::max_index(types) == index)
    {
        ss << " (" << type << ")";
    }

    const char *severities[] = {
                                   "HIGH",
                                   "MEDIUM",
                                   "LOW",
                                   "UNKNOWN"
                               };
    // as per the extension, HIGH is the base (smaller) value
    index = Array::item_index(severity,
                              GL_DEBUG_SEVERITY_HIGH_ARB,
                              GL_DEBUG_SEVERITY_LOW_ARB,
                              Array::max_index(severities));
    ss << " severity=" << severities[index];
    if (Array::max_index(severities) == index)
    {
        ss << " (" << severity << ")";
    }
    ss << "]";

    const auto log = ss.str();
    FILE *out_file = reinterpret_cast<FILE*>(const_cast<void*>(file_ptr));
    fprintf(out_file, "%s\n", log.c_str());

    ::check(severity != GL_DEBUG_SEVERITY_HIGH_ARB, "High severity GL error logged");
}

void setup_debug(bool enable)
{
    if(glewIsSupported("GL_ARB_debug_output"))
    {
        glDebugMessageCallbackARB(enable ? gl_debug_logger : nullptr, stderr);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
        ::check(GL_NO_ERROR == glGetError(), "Unable to set synchronous debug output");
    }
}
#endif  // NDEBUG

void window_error(int, const char *error_msg)
{
    throw std::runtime_error(error_msg);
}

void handle_key(GLFWwindow *window,
                int key,
                int /*scancode*/,
                int action,
                int /*mods*/)
{
    auto scene = reinterpret_cast<Scene*>(glfwGetWindowUserPointer(window));
    assert(scene);

    if(GLFW_PRESS == action)
    {
        switch(key)
        {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, GL_TRUE);
            break;
        case GLFW_KEY_R:
            scene->cam.reset();
            break;
#ifndef NDEBUG
        case GLFW_KEY_0:
            scene->rendering_mode = (scene->rendering_mode == GL_LINE) ? GL_FILL : GL_LINE;
            break;
#endif
        }
    }
}

void handle_input(GLFWwindow *window,
                  Camera *cam)
{
    // rotation
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_UP))
        cam->rotate_X(Camera::rot_delta);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_DOWN))
        cam->rotate_X(-Camera::rot_delta);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_LEFT))
        cam->rotate_Y(Camera::rot_delta);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_RIGHT))
        cam->rotate_Y(-Camera::rot_delta);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_PERIOD))
        cam->rotate_Z(Camera::rot_delta);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_COMMA))
        cam->rotate_Z(-Camera::rot_delta);

    // translation
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_D))
        cam->slide_X(Camera::slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_A))
        cam->slide_X(-Camera::slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_W))
        cam->slide_Y(Camera::slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_X))
        cam->slide_Y(-Camera::slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_Z))
        cam->slide_Z(Camera::slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_S))
        cam->slide_Z(-Camera::slide_offset);

    // FoV
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_KP_ADD))
        cam->increase_FoV();
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_KP_SUBTRACT))
        cam->decrease_FoV();

    glfwPollEvents();
}

void init_rendering()
{
    // this is set by default
    //glViewport(0, 0, screen_width, screen_height);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    // enabling culling culls out a plane when trying to view it from underneath hence disabled
    //glEnable(GL_CULL_FACE);
    // the reason is that a plane, unlike a model, has no inside and hence culling doesn't make sense here
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    // this also needs multisample context (render target)
    glEnable(GL_MULTISAMPLE);
}

void upload_meshes(const std::vector<Primitives::Mesh> &meshes,
                   std::vector<GL::VAO> *VAOs)
{
    const size_t attrib_lengths[] = { 3u };
    VAOs->reserve(meshes.size());
    for(size_t i = 0u; i < meshes.size(); ++i)
    {
        GL::Buffer vbo = GL::upload_data(meshes[i].vertices, GL_ARRAY_BUFFER);
        GL::Buffer ibo = GL::upload_data(meshes[i].indices, GL_ELEMENT_ARRAY_BUFFER);
        VAOs->emplace_back(GL::setup_attributes<GLfloat>(vbo, attrib_lengths, ibo));
    }
}

void render(GLuint program,
            const Scene &scene,
            const std::vector<GL::VAO> &VAOs,
            GLuint xform_id,
            GLuint max_height_id)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
#ifndef NDEBUG
    glPolygonMode(GL_FRONT_AND_BACK, scene.rendering_mode);
#endif
    glUseProgram(program);
    auto PV = scene.cam.projection * scene.cam.view_xform;
    for(auto i = 0u; i < scene.meshes.size(); ++i)
    {
        glBindVertexArray(VAOs[i]);
        const auto PVM = PV * scene.meshes[i].model_xform;
        glUniformMatrix4fv(xform_id, 1, GL_FALSE, &PVM[0][0]);
        glUniform1f(max_height_id, scene.height_map.max_height);
#if defined(BASE_VERTEX)
        // NOTE: Lots of confusing documentation about glDrawElementsBaseVertex and glMultiDrawElementsBaseVertex:
        // parameter 3 (type) isn’t about parameter 4 (indices) at all -- unlike what the documentation says -- it’s the
        // data type of indices in the ELEMENT_ARRAY_BUFFER; try differing type of meshes[i].indices from what’s passed
        // for parameter 3, the rendering will go hay wire. Parameter 4 (indices) is a byte-offset, sent via a pointer
        // type, into the now bound ELEMENT_ARRAY_BUFFER, to start reading indices from, for this draw call. See
        // glDrawElements’s doc in OpenGL Programming Guide, 8th edition.
        //
        // Conceptually eff_index = *reinterpret_cast<type>(ELEMENT_ARRAY_BUFFER + indices) + basevertex
        for (auto row = 0u; row < (scene.height_map.rows - 1); ++row)
            glDrawElementsBaseVertex(GL_TRIANGLE_STRIP,
                                     scene.meshes[i].indices.size(),
                                     GL::type_id(decltype(scene.meshes[i].indices)::value_type{}) /*type of indices*/,
                                     0 /*indices*/,
                                     (row * scene.height_map.cols) /*basevertex*/);
#elif defined(MULTI_BASE_VERTEX)
        std::vector<GLsizei> counts(scene.height_map.rows - 1, scene.meshes[i].indices.size());
        // NOTE: it’s extremely important that the value_type of offset be void*; giving anything else leads to offset issues
        std::vector<void*> offset(scene.height_map.rows - 1, 0u);
        std::vector<GLint> baseverts(scene.height_map.rows - 1, 0u);
        auto row = 0u;
        std::generate(std::begin(baseverts), std::end(baseverts), [&row, &scene](){ return (row++ * scene.height_map.cols); });
        glMultiDrawElementsBaseVertex(GL_TRIANGLE_STRIP,
                                      counts.data(),
                                      GL::type_id(decltype(scene.meshes[i].indices)::value_type{}) /*type of indices*/,
                                      offset.data(),
                                      counts.size(),
                                      baseverts.data());
#else
        glDrawElements(GL_TRIANGLES,
                       scene.meshes[i].indices.size(),
                       GL::type_id(decltype(scene.meshes[i].indices)::value_type{}),
                       0u);
#endif  // defined(BASE_VERTEX) || defined(MULTI_BASE_VERTEX)
        glBindVertexArray(0u);
    }
    glUseProgram(0);
}

void update_frame_time(double *last_time,
                       unsigned *frames_rendered,
                       GLFWwindow *window)
{
    ++*frames_rendered;
    const auto now = glfwGetTime();
    if((now - (*last_time)) >= 1.0)
    {
        std::ostringstream ss;
        ss << app_title << " ~ " << (1000.0 / *frames_rendered) << "ms/frame";
        const auto str_mpf = ss.str();
        glfwSetWindowTitle(window, str_mpf.c_str());
        *frames_rendered = 0;
        *last_time = now;
    }
}

}   // unnamed namespace

int main(int /*argc*/, char** /*argv*/)
{
    glfwSetErrorCallback(window_error);
    const auto glfw = std::move(GLFW_wrapper{Check_return<decltype(&glfwInit),
                                                          glfwInit,
                                                          decltype(GL_TRUE),
                                                          GL_TRUE>{}});
    // RAII warpping isn't mandatory here, since glfwTerminate destroys open windows, if any remain
    std::unique_ptr<GLFWwindow,
                    decltype(&glfwDestroyWindow)> main_wnd_ptr{setup_context(screen_width, screen_height),
                                                               glfwDestroyWindow};

    glewExperimental = GL_TRUE;
    auto err = glewInit();
    check(GLEW_OK == err, reinterpret_cast<const char*>(glewGetErrorString(err)));
    glGetError();       // to clear error due to GLEW bug #120

#ifndef NDEBUG
    setup_debug(true);
#endif

    Scene scene{FoV,
                aspect_ratio,
                near,
                far};
    Primitives::parse_SDL("data/map.sdl",
                          &scene.height_map,
                          &scene.meshes);

    std::vector<GL::VAO> VAOs;
    upload_meshes(scene.meshes, &VAOs);

    // http://www.parashift.com/c++-faq-lite/memfnptr-vs-fnptr.html
    // according to C++ FAQ, you cannot pass a member function to a C callback; passing a functor, lambda, boost::bind,
    // etc. is for a C++ callback implemented with templates; instead use the void* user_data allowed in the callback;
    // GLFW gives GLFWwindow->SetWindowUserPointer for the same
    glfwSetWindowUserPointer(main_wnd_ptr.get(), reinterpret_cast<void*>(&scene));
    glfwSetKeyCallback(main_wnd_ptr.get(), handle_key);

    GL::Program program{GL::setup_program("shaders/p_vs.glsl",
                                          "shaders/col_fs.glsl")};
    const auto max_height_id = glGetUniformLocation(program, "max_height");
    assert(max_height_id != -1);
    const auto xform_id = glGetUniformLocation(program, "PVM");
    assert(xform_id != -1);

    init_rendering();
    auto last_time = glfwGetTime();
    unsigned frames = 0u;
    while(!glfwWindowShouldClose(main_wnd_ptr.get()))
    {
        render(program,
               scene,
               VAOs,
               xform_id,
               max_height_id);
        glfwSwapBuffers(main_wnd_ptr.get());
        handle_input(main_wnd_ptr.get(), &scene.cam);
        update_frame_time(&last_time, &frames, main_wnd_ptr.get());
    }
}
