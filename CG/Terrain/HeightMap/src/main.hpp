#ifndef __MAIN_HPP__
#define __MAIN_HPP__

// Camera with 6 DoF (free-roaming robot); slide and rotate on all 3 axes
// the implmentation here is optimized lookup cam_manip_sans_lookat.cpp
// for a straight forward implmentation to understand what's going on
struct Camera
{
    // world to view transform
    glm::mat4 view_xform;

    // projection parameters
    float FoV, aspect_ratio, near, far;
    glm::mat4 projection;

    // manipulation constants
    static constexpr float slide_offset = 0.007f;
    static constexpr float rot_delta = 0.003f;
    static constexpr float FoV_delta = 0.002f;

    Camera(float FoV,
           float aspect_ratio,
           float near,
           float far) :
//        view_xform{1.0f, 0.0f,  0.0f, 0.0f,   // col 1
//                   0.0f, 1.0f,  0.0f, 0.0f,   // col 2
//                   0.0f, 0.0f,  1.0f, 0.0f,   // col 3
//                   0.0f,-60.0f, -30.0f, 1.0f},  // col 4
        FoV{FoV},
        aspect_ratio{aspect_ratio},
        near{near},
        far{far},
        projection(glm::perspective(FoV, aspect_ratio, near, far))
    {
        view_xform = glm::lookAt(glm::vec3(0, 60, 30), glm::vec3(0, 0, 0), glm::vec3(0.0f, 1.0f, 0.0f));
    }

    void reset()
    {
        view_xform = glm::mat4{1.0f, 0.0f,  0.0f, 0.0f,   // col 1
                               0.0f, 1.0f,  0.0f, 0.0f,   // col 2
                               0.0f, 0.0f,  1.0f, 0.0f,   // col 3
                               0.0f, 0.0f, -5.0f, 1.0f};  // col 4
    }

    void increase_FoV()
    {
        FoV += FoV_delta;
        projection = glm::perspective(FoV, aspect_ratio, near, far);
    }

    void decrease_FoV()
    {
        FoV -= FoV_delta;
        projection = glm::perspective(FoV, aspect_ratio, near, far);
    }

    void slide_X(float delta)
    {
        view_xform[3][0] -= delta;
    }

    void slide_Y(float delta)
    {
        view_xform[3][1] -= delta;
    }

    void slide_Z(float delta)
    {
        view_xform[3][2] -= delta;
    }

    void rotate_X(float angle)
    {
        const auto inv = glm::eulerAngleX(-angle);
        view_xform = inv * view_xform;
    }

    void rotate_Y(float angle)
    {
        const auto inv = glm::eulerAngleY(-angle);
        view_xform = inv *view_xform;
    }

    void rotate_Z(float angle)
    {
        const auto inv = glm::eulerAngleZ(-angle);
        view_xform = inv * view_xform;
    }
};

struct Scene
{
    // debug data
#ifndef NDEBUG
    GLenum rendering_mode = GL_FILL;
#endif

    Camera cam;

    // actors
    Primitives::HeightMap height_map;

    // mesh data
    std::vector<Primitives::Mesh> meshes;

    // constructor
    Scene(float FoV,
          float aspect_ratio,
          float near,
          float far) : cam{FoV, aspect_ratio, near, far}
    {
    }
};

#endif  // __MAIN_HPP__
