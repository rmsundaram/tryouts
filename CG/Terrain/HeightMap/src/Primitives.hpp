#ifndef PRIMITIVES_HPP
#define PRIMITIVES_HPP

namespace Primitives
{

struct Mesh // indexed triangles list
{
    glm::vec4 colour;
    // although short is used for indices, the type should be based
    // on vertices.size() since having a lesser sized datatype is better
    // Boost.Variant can help here
    std::vector<GLushort> indices;
    std::vector<glm::vec3> vertices;
    glm::mat4 model_xform;
};

struct HeightMap
{
    size_t rows, cols;
    std::vector<float> heights;
    float max_height;

    float height_at(size_t row, size_t col);
};

void parse_SDL(const char *file_path,
               Primitives::HeightMap *height_map,
               std::vector<Primitives::Mesh> *meshes);
} // namespace Primitives

#endif // PRIMITIVES_HPP
