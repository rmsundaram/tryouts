#ifndef PRIMITIVES_HPP
#define PRIMITIVES_HPP

namespace Primitives
{

typedef std::array<glm::vec4, 2> Position_colour;

struct Mesh // indexed triangles list
{
    GLenum primitive_type;
    // although int is used for indices, the type should be based on vertices.size()
    // since having a lesser sized datatype is better when uploading to GPU
    // Boost.Variant can help here
    std::vector<GLushort> indices;
    // position and colour
    std::vector<Position_colour> vertices;
    glm::mat4 model_xform;
};

struct OBB
{
    glm::vec3 centre;
    glm::vec3 half_extent;
    glm::mat3 axes;
};

struct Ray
{
    glm::vec3 origin;
    glm::vec3 direction;
};

struct Triangle
{
    glm::vec3 verts[3];
};

// cells, identified by their left top indices, are elements of the 2d projection of
// the height map - a grid, with positional x acting as grid x and positional z acting
// as grid y, advancing from top to bottom like most screen coordinate systems
typedef glm::uvec2 Cell;

struct HeightMap
{
    size_t rows, cols;
    std::vector<float> heights;
    glm::vec3 min, max;
    const glm::vec3 cell_size{13.3333f, 1.0f, 13.3333f};

    float height_at(size_t row, size_t col);
    float height_at(float x, float z);
    void get_triangles(const Cell &cell, Triangle *t1, Triangle *t2);
    float x_to_index(float pos);
    float y_to_index(float pos);
    float z_to_index(float pos);
    float index_to_x(float index);
    float index_to_y(float index);
    float index_to_z(float index);
};

bool ray_triangle_intersect(const Ray &ray,
                            const Triangle &tri,
                            float *t);
bool ray_obb_intersect(const Ray &ray,
                       const OBB &box,
                       float *t1,
                       float *t2 = nullptr);
OBB aabb_to_obb(const glm::vec3 &min, const glm::vec3 &max);

Ray clip_ray(const Ray &r,
             const glm::vec3 &min,  // if no clipping is required in a dimension
             const glm::vec3 &max); // pass the same value for both min and max
glm::vec3 get_point(const Primitives::Ray &ray, float t);
bool is_degenerate(const Ray &ray, float epsilon = 10.0E-6f);

glm::vec4 left_top_right_bottom(glm::vec2 min, glm::vec2 max);

template <typename T>
bool grid_traverse(const Primitives::Ray &r, T visit);

void parse_SDL(const char *file_path,
               Primitives::HeightMap *height_map,
               std::vector<Primitives::Mesh> *meshes);
} // namespace Primitives

constexpr unsigned verts_per_triangle = 3u;

#pragma pack(push, 2)
struct BMP_header
{
    // BMP header
    uint16_t magic = 0;
    uint32_t file_size = 0;
    uint32_t reserved = 0;
    uint32_t data_offset = 0;

    // DIB header
    uint32_t dib_header_length = 0;
    uint32_t width = 0;
    uint32_t height = 0;
    uint16_t num_colour_planes = 0;
    uint16_t bits_per_pixels = 0;
    uint32_t bi_bitfields = 0;
    uint32_t data_size = 0;
    uint32_t physical_width = 0;
    uint32_t physical_height = 0;
    uint32_t num_palette_colours = 0;
    uint32_t num_important_colours = 0;
};
#pragma pack(pop)
// when the compiler supports, switch to
// platform independent struct alignas(2) BMPHeader
static_assert(sizeof(BMP_header) == 54u, "BMPHeader structure mismatch");
constexpr unsigned BM_ASCII = 19778u;    // "BM" magic
constexpr unsigned num_bmp_channels = 3u;

struct Bitmap
{
    size_t width, height;
    std::vector<uint8_t> data;
};

template <typename T>
bool Primitives::grid_traverse(const Primitives::Ray &r,
                               T visit)
{
    assert(!is_degenerate(r));

    // this method and the epsilon is taken from the (float) Grid Ray Tracing workout
    constexpr auto eps = 10.0E-6f;
    const auto dest_x = r.origin.x + r.direction.x;
    const auto dest_z = r.origin.z + r.direction.z;
    const auto step_x = (r.direction.x != 0.0f) ? (r.direction.x > 0.0f ? 1.0f : -1.0f) : 0.0f;
    const auto step_z = (r.direction.z != 0.0f) ? (r.direction.z > 0.0f ? 1.0f : -1.0f) : 0.0f;
    auto x = r.origin.x, z = r.origin.z;
    auto reached = false;
    while (!reached && (glm::epsilonNotEqual(x, dest_x, eps) || glm::epsilonNotEqual(z, dest_z, eps)))
    {
        auto cur_x = x, cur_z = z;
        // next value would be a line x = k or the destination x
        const auto next_x = closer(x, prev_int(x, step_x) + step_x, dest_x);
        const auto next_z = closer(z, prev_int(z, step_z) + step_z, dest_z);
        auto t_exit = 0.0f;
#ifndef __FAST_MATH__
        const auto tx = (next_x - r.origin.x) / r.direction.x;
        const auto tz = (next_z - r.origin.z) / r.direction.z;
        // instead of checking for infinity (isinf) checking for
        // both INF and NAN (isfinite) since dx = 0 → tx = 0/0 = NAN
        if ((!std::isfinite(tz)) || (std::isfinite(tx) && (tx <= tz)))
        // even when ty = NAN, std::isfinite returns false when -ffast-math is used
#else
        const auto tx = r.direction.x ? ((next_x - r.origin.x) / r.direction.x) : 0.0f;
        const auto tz = r.direction.z ? ((next_z - r.origin.z) / r.direction.z) : 0.0f;
        if ((tz == 0.0f) || ((tx != 0.0f) && (tx <= tz)))
#endif
        {
            // z = r.org.z + tx * r.dir.z;
            z = std::fma(tx, r.direction.z, r.origin.z);
            x = next_x;
            t_exit = tx;
        }
        else
        {
            // x = r.org.x + tz * r.dir.x;
            x = std::fma(tz, r.direction.x, r.origin.x);
            z = next_z;
            t_exit = tz;
        }
        reached = visit({cur_x, cur_z}, {x, z}, t_exit);
    }
    return reached;
}

#endif // PRIMITIVES_HPP
