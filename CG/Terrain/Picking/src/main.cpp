/*
 * Learnings:
 *  1. Terrain rendering from bitmap data (RasterTek.com)
 *  2. Voxel traversal both in int and float spaces (see the Grid Ray Tracing workout)
 *  3. Terrain picking using 2D grid traversal (as implemented by the above workout). When exiting each
 *      cell, the ray's height at exit point is compared against the cell's height at the same exit point.
 *      If it's lesser or equal then the ray has made a crossover by penetrating the terrain. Now a couple
 *      of ray-triangle intersection tests are made to determine the triangle that was hit and the point
 *      of intersection too.
 *  4. BaseVertex calls
 *           https://www.opengl.org/registry/specs/ARB/draw_elements_base_vertex.txt
 *           http://arcsynthesis.org/gltut/Positioning/Tut05 Optimization Base Vertex.html
 *  5. MultiDraw calls
 *           http://stackoverflow.com/a/26755538/183120
 *           https://www.opengl.org/wiki/Vertex_Rendering
 *           https://www.opengl.org/wiki/Vertex_Specification_Best_Practices
 *  6. Z buffer resolution¹ and z-fighting
 *      The greater the ratio of far/near, the less effective the depth buffer will be at distinguishing
 *      surfaces that are near each other. Having a large depth range i.e. huge difference between near
 *      and far planes would lead to artifacts such as missing triangles when multiple fragments are
 *      fighting to win when their depth values match. There isn't a permanent "fix" for this, it can be
 *      ameliorated by reducing zFar or increasing zNear. Another option is to increase the depth buffer
 *      resolution. This issue² was seen with the initial value of zNear = 0.001 and zFar = 5000. However,
 *      Humus, in his phonewire demo³, sets zNear = 0.1 and zFar = 5000, the only differnce being the
 *      zNear is closer in his, which makes the issue vanish. This is explained very well in OpenGL Wiki⁴:
 *            Depth buffering seems to work, but polygons seem to bleed through polygons
 *            that are in front of them. What's going on?
 *            You may have configured your zNear and zFar clipping planes in a way that
 *            severely limits your depth buffer precision. Generally, this is caused by a
 *            zNear clipping plane value that's too close to 0.0. As the zNear clipping
 *            plane is set increasingly closer to 0.0, the effective precision of the
 *            depth buffer decreases dramatically. Moving the zFar clipping plane further
 *            away from the eye always has a negative impact on depth buffer precision,
 *            but it's not one as dramatic as moving the zNear clipping plane.
 *    ¹ Figure II.21, 3-D Computer Graphics by Samuel R. Buss.
 *    ² http://gamedev.stackexchange.com/q/87329/14025
 *    ³ http://humus.name/index.php?page=3D&ID=89
 *    ⁴ https://www.opengl.org/wiki/Depth_Buffer_Precision
 *      http://www.sjbaker.org/steve/omniv/love_your_z_buffer.html
 *  7. The reason a range of 0.001 to 5000 was set is that the terrain should be visible even when the
 *      camera is far off. However, this compromises depth buffer resolution. A solution, other than
 *      the ones stated above, would be to maintain the difference between near and far values at a
 *      value appropriate for a good z-buffer resolution but vary the actual values of zNear and zFar
 *      such that the terrain is between them even when the camera moves away. To visualise this, say
 *      when the camera, at y = 300, is shooting a top view of the terrain whose height ranges between
 *      0 and 255, then the zNear and zFar can be 45 and 300. As we move farther, say at y = 400, we
 *      change them to 145 and 400. Even when the camera is at 1000, zNear = 745, zFar = 1000 should
 *      work, since in all these cases the difference zFar-zNear was maintained as 255. Here zNear and
 *      and zFar is in camera space and the rest are in world space.
 *  8. Humus' terrain in the phonewire demo never gets clipped out of view, irrespective of how afar
 *      the camera is. How he achieves this in D3D10 is unknown, but this is achieved, in OpenGL, here
 *      by enabling depth clamping as suggested by Jason: http://stackoverflow.com/a/11949687/183120.
 *      Enabling depth clamping, essentially diables clipping by the near and far planes, and any frag
 *      whose depth goes beyond [-1, 1] will be clamped to this interval. The downside to this is that
 *      it screws up depth tests between fragments that are being clamped, but usually those objects
 *      are far away.
 *  9. Vertex texture fetch is a feature where vertex shader can do a texture lookup; the number of such
 *      lookups can be known by querying GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS; this is particularly useful
 *      when rendering terrains if the texture levelling (peak heights) can be done at render time using
 *      vertex shaders - this approach, called displacement mapping, isn't implemented here.
 * 10. Facet culling has 3 knobs and they can be changed. Their state is in effect until changed again
 *      and the current state affects all draw calls until next change. The knobs (i) the winding order
 *      for front facing triangles, (ii) which face should be culled (iii) facet culling on/off.
 * 11. Primitive restart index could be used in rendering a height map as mbsoftworks.sk points out:
 *      http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=8. When we've vertices like
 *                 0  1  2  3  4
 *                 5  6  7  8  9
 *                10 11 12 13 14
 *      The indices for GL_TRIANGLE_STRIP would be 0 5 1 6 2 7 3 8 4 9, however for the next row, we
 *      can't put a 10 here since that would mean a continuation of the previous series. However we can
 *      have a new series as 5 10 6 11 7 12 8 13 9 14 but only in a separate buffer, since in the previous
 *      buffer having anything after 9 would be continuing the previous strip primitive. To avoid having
 *      to allocate a new index buffer and to continue using the same buffer, GL gives the primitive
 *      restart index, which we insert between the two series (between 9 and 5) some arbitraryly chosen
 *      index, say 200 to notify that restart of the primitive i.e. to begin a new triangle strip from
 *      the next index onwards. The chosen index is informed to GL through glPrimitiveRestartIndex.
 *      However, this requires more indices in memory that the BaseVertex techinique which has them only
 *      for one row. This matters when the number of rows are high.
 *
 * References for grid traversal based picking:
 *  1. §5.10, XNA 3.0 Game Programming Recipes, Riemer Grootjans
 *  2. http://www.gamedev.net/topic/529824-terrain-picking/
 *  3. http://www.gamedev.net/topic/528369-terrain-collision/
 *  4. http://www.gamedev.net/topic/617341-xna-picking
 *  5. http://vterrain.org/Implementation/Libs/ray-heighhtfield.html
 *  6. http://www.gamedev.net/topic/618968-picking-terrain-with-mouse-for-editing/#entry4906242
 *
 * TODO: Perform smoothing filter i.e. to avoid rougher terrain, every vertex position is substituted by
 *        averaging its surrounding vertex positions as described in Chapter 19 of Introduction to 3D Game
 *        Programming with DirectX 11 by Frank D. Luna.
 */

#include "common.hpp"
#include "Util.hpp"
#include "Dataless_wrapper.hpp"
#include "GL_res_wrap.hpp"
#include "GL_util.hpp"
#include "Shader_util.hpp"
#include "Primitives.hpp"
#include "main.hpp"
#include <chrono>

#include "Window.inl"

constexpr auto *scene_file = "data/map.sdl";

namespace
{

float get_hit_tri(Scene *scene,
                  const Primitives::Ray &ray,
                  const Primitives::Cell &hit_cell,
                  uint8_t *triangle_id)
{
    // perform ray-triangle intersection
    Primitives::Triangle tri[2];
    scene->height_map.get_triangles(hit_cell, &tri[0], &tri[1]);
    uint8_t tri_id = 2u;
    float t = std::numeric_limits<float>::infinity();
    if (Primitives::ray_triangle_intersect(ray, tri[0], &t))
    {
        tri_id = 0;
    }
    else if (Primitives::ray_triangle_intersect(ray, tri[1], &t))
    {
        tri_id = 1;
    }
    assert(tri_id < 2);
    *triangle_id = tri_id;
    return t;
}

bool hit_test_cell(Scene *scene,
                   const Primitives::Ray &ray,
                   const glm::vec2 &entry,
                   const glm::vec2 &exit,
                   float t_exit,
                   Primitives::Cell *hit_cell)
{
    // here entry.y and exit.y are really z values since height is denoted by y
    // z denotes the top and bottom in the grid when viewed along Y axis from top
    // since these are going to be array indices they cannot be < 0
    assert(entry.x >= 0.0f && entry.y >= 0.0f);
    assert(exit.x >= 0.0f && exit.y >= 0.0f);

    const auto exit_x = scene->height_map.index_to_x(exit.x);
    const auto exit_z = scene->height_map.index_to_z(exit.y);
    const auto ray_exit_ht = std::fma(t_exit, ray.direction.y, ray.origin.y);
    const auto map_exit_ht = scene->height_map.height_at(exit_x, exit_z);

    // TODO: As of 0.9.5.3 GLM has float x, y, z, w; for vec4 and not float[4], thus such access is not strictly
    // portable due to struct padding
    const glm::vec4 ltrb = Primitives::left_top_right_bottom(entry, exit);
    const auto left = static_cast<size_t>(floor_if_not_int(ltrb[0]));
    const auto top = static_cast<size_t>(floor_if_not_int(ltrb[1]));
    *hit_cell = {left, top};

    // TODO: when camera is underneath the terrain surface, inverting this condition (>) is required
    // as the ray would originate below the surface and it should be tested for when it'll go above it
    const bool hit = ray_exit_ht <= map_exit_ht;
#ifdef DEBUG_SELECTION
    // if debugging selection, add triangles of every cell visited
    if (!hit)
    {
        scene->sel_cells_dbg.insert(scene->sel_cells_dbg.end(), {*hit_cell, *hit_cell});
        scene->sel_tris_dbg.insert(scene->sel_tris_dbg.end(), {0, 1});
    }
#endif  // DEBUG_SELECTION

    return hit;
}

void reset_selection(Scene *scene)
{
    scene->sel_tri = 2u;
#ifdef DEBUG_SELECTION
    scene->sel_cells_dbg.clear();
    scene->sel_tris_dbg.clear();
#endif  // DEBUG_SELECTION
}

float hit_test_height_map(Scene *scene, const Primitives::Ray &ray)
{
    // TODO: it would be simpler and more performant if the traversal and hit testing happen in the same space.

    // cell_traverse happens in a space where the rows and columns are in integer points
    // however the height map might have scaling, hence transforming ray to the unit grid space
    const glm::vec3 grid_ray_origin = {
                                        scene->height_map.x_to_index(ray.origin.x),
                                        scene->height_map.y_to_index(ray.origin.y),
                                        scene->height_map.z_to_index(ray.origin.z)
                                      };
    const auto end_point = ray.origin + ray.direction;
    const glm::vec3 grid_ray_end = {
                                     scene->height_map.x_to_index(end_point.x),
                                     scene->height_map.y_to_index(end_point.y),
                                     scene->height_map.z_to_index(end_point.z)
                                   };
    Primitives::Ray grid_ray = { grid_ray_origin, grid_ray_end - grid_ray_origin };

    float t = std::numeric_limits<float>::infinity();

    Primitives::Cell hit_cell;
    if (Primitives::grid_traverse(grid_ray,
                                  [&](const glm::vec2 &entry, const glm::vec2 exit, float t)
                                  {
                                      return hit_test_cell(scene, ray, entry, exit, t, &hit_cell);
                                  }))
    {
        // entering here confirms that a hit was made by the ray on hit_cell
        uint8_t tri_id;
        t = get_hit_tri(scene, ray, hit_cell, &tri_id);
        scene->sel_cell = hit_cell;
        scene->sel_tri = tri_id;
    }
    return t;
}

void init_rendering()
{
    // this is set by default
    //glViewport(0, 0, screen_width, screen_height);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

    // enabling culling culls out the plane when trying to view it from underneath; the reason is that a plane,
    // unlike a model, has no inside and hence culling it doesn't make sense. If you want planes to be visible
    // in all directions then disable this
    glEnable(GL_CULL_FACE);

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    // enabling this avoids clipping by near and far planes but any fragments that would have
    // normalized z coordinates outside of the [-1, 1] range will be clamped to that range.
    // It screws up depth tests between fragments that are being clamped, but usually those
    // objects are far away. See Learning #8 at the beginning of this file.
    glEnable(GL_DEPTH_CLAMP);
}

void upload_meshes(const std::vector<Primitives::Mesh> &meshes,
                   std::vector<GL::VAO> *VAOs)
{
    // an element per attribute with number of floats as value
    const size_t attrib_lengths[] = { 4u, 4u };
    VAOs->reserve(meshes.size());
    for(size_t i = 0u; i < meshes.size(); ++i)
    {
        GL::Buffer vbo = GL::upload_data(meshes[i].vertices, GL_ARRAY_BUFFER);
        GL::Buffer ibo = GL::upload_data(meshes[i].indices, GL_ELEMENT_ARRAY_BUFFER);
        VAOs->emplace_back(GL::setup_attributes<GLfloat>(vbo, attrib_lengths, ibo));
    }
}

void render(GLuint program,
            const Scene &scene,
            const std::vector<GL::VAO> &VAOs,
            GLuint xform_id)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPolygonMode(GL_FRONT_AND_BACK, scene.rendering_mode);
    glUseProgram(program);
    auto PV = scene.cam.projection * scene.cam.view_xform;
    for(auto i = 0u; i < scene.meshes.size(); ++i)
    {
        glBindVertexArray(VAOs[i]);
        const auto PVM = PV * scene.meshes[i].model_xform;
        glUniformMatrix4fv(xform_id, 1, GL_FALSE, &PVM[0][0]);

#ifdef TERRAIN_TRIANGLE_STRIP

#ifndef MULTI_DRAW
        for (auto j = 0u; j < scene.height_map.rows - 1; ++j)
            glDrawElementsBaseVertex(scene.meshes[i].primitive_type,
                                     scene.meshes[i].indices.size(),
                                     GL::type_id(decltype(scene.meshes[i].indices)::value_type{}),
                                     reinterpret_cast<GLvoid*>(0u),
                                     j * scene.height_map.cols);
#else
        const auto vertical_cells = scene.height_map.rows - 1;
        // TODO: generate only once (when terrain data is generated) and reuse
        std::vector<GLsizei> counts(vertical_cells, scene.meshes[i].indices.size());
        std::vector<GLsizei> indices(vertical_cells, 0);
        std::vector<GLint> base_verts(vertical_cells);
        auto j = 0u;
        std::generate(base_verts.begin(), base_verts.end(), [&] { return scene.height_map.cols * j++; });
        glMultiDrawElementsBaseVertex(scene.meshes[i].primitive_type,
                                      counts.data(),
                                      GL::type_id(decltype(scene.meshes[i].indices)::value_type{}),
                                      reinterpret_cast<GLvoid const* const*>(indices.data()),
                                      counts.size(),
                                      base_verts.data());
#endif  // MULTI_DRAW

#else   // draw with GL_TRIANGLES
        glDrawElements(scene.meshes[i].primitive_type,
                       scene.meshes[i].indices.size(),
                       GL::type_id(decltype(scene.meshes[i].indices)::value_type{}),
                       0u);
#endif  // TERRAIN_TRIANGLE_STRIP
        glBindVertexArray(0);
    }
    glUseProgram(0);
}

void render_sel(GLuint program,
                const Scene &scene,
                const std::vector<GL::VAO> &VAOs,
                GLuint xform_id,
                GLuint colour_id)
{
    // validate selection
    if (scene.sel_tri < 2u)
    {
        glPolygonMode(GL_FRONT_AND_BACK, scene.rendering_mode);
        glUseProgram(program);
        auto PV = scene.cam.projection * scene.cam.view_xform;
        // FIXME: hack in directly indexing height map's mesh
        // should actually be queried from the scene.height_map
        glBindVertexArray(VAOs[0]);
        const auto PVM = PV * scene.meshes[0].model_xform;
        glUniformMatrix4fv(xform_id, 1, GL_FALSE, &PVM[0][0]);
        const glm::vec4 sel_colour{1.0f, 1.0f, 1.0f, 1.0f};
        glUniform4fv(colour_id, 1, &sel_colour[0]);

        using Index_type = decltype(scene.meshes[0].indices)::value_type;
#ifdef DEBUG_SELECTION
        for (auto i = 0u; i < scene.sel_tris_dbg.size(); ++i)
        {
#ifdef TERRAIN_TRIANGLE_STRIP
            const auto base = (scene.sel_cells_dbg[i].y * scene.height_map.cols) + scene.sel_cells_dbg[i].x;
            const auto offset = scene.sel_tris_dbg[i] * sizeof(Index_type); // offset inside the index buffer, in bytes
            glFrontFace(GL_CCW - scene.sel_tris_dbg[i]);    // if sel_tri == 0, then CCW, else CCW - 1 = CW
            glDrawElementsBaseVertex(scene.meshes[0].primitive_type,
                                     3,
                                     GL::type_id(Index_type{}),
                                     reinterpret_cast<GLvoid*>(offset),
                                     base);
            glFrontFace(GL_CCW);
#else
            constexpr auto verts_per_triangle = 3u;
            constexpr auto tris_per_cell = 2u;
            const auto cells = scene.sel_cells_dbg[i].y * (scene.height_map.cols - 1) + scene.sel_cells_dbg[i].x;
            const auto offset = (tris_per_cell * cells + scene.sel_tris_dbg[i]) * verts_per_triangle * sizeof(Index_type);
            glDrawElements(scene.meshes[0].primitive_type,
                           verts_per_triangle,
                           GL::type_id(Index_type{}),
                           reinterpret_cast<GLvoid*>(offset));
#endif  // TERRAIN_TRIANGLE_STRIP
        }
#endif  // DEBUG_SELECTION

        // render selection
#ifdef TERRAIN_TRIANGLE_STRIP
        const auto base = (scene.sel_cell.y * scene.height_map.cols) + scene.sel_cell.x;
        const auto offset = scene.sel_tri * sizeof(Index_type); // offset inside the index buffer, in bytes
        /*
         * The index data is such
         *     0 1 2 3 4
         *     5 6 7 8 9
         * When triangle strip is used, the resulting triangles' winding order is determined by the first triangle's
         * winding order. Also GL autmatically "fixes" every second triangle formed to confirm with the winding formed
         * by the first. So in this case, if 0 5 1 was the first triangle then the second, although would be 5 1 6, GL
         * would swap the first two indices resulting in 1 5 6, but for the next triangle, it'll be 1 6 2. Had the
         * first triangle been 5 0 6 (CW) then GL would make sure that all resulting triangles turn out with CW winding:
         * 6 0 1, 6 1 2, etc.
         * Since we're re-using the index data that was generated for rendering a triangle strip to render a single
         * triangle in-between instead of starting from the first and giving one additional index per new triangle,
         * we've to take care of the winding somehow since OpenGL can't fix it for us, since from GL's viewpoint this
         * is the first triangle starting the strip (the same even if GL_TRIANGLES was use). If we're enabling facet
         * [glEnable(GL_FACE_CULLING)] culling with the back face to be the culled one [glCullFace(GL_BACK)] and the
         * front face being set as the triangles with CCW winding [glFrontFace(GL_CCW)], then this poses no problem
         * for even tringles, since they too are CCW winding in our case. However, for the lower triangle in a cell,
         * say cell 0, we've to offset the index buffer at 1 and count = 3 i.e. starting at 5 leading to 5 1 6 which
         * results in a CW wound triangle.
         *
         * We've 3 options to remedy this: (i) turn off culling just for this draw call and turn it back on
         * (ii) tell GL to cull the front and not the back face, again just for this draw call (iii) set the
         * winding order that determines which is the front face from CCW to CW, only for this draw call and
         * revert it post call; choosing (iii) since it is the most local fix
         */
        glFrontFace(GL_CCW - scene.sel_tri);    // if sel_tri == 0, then CCW, else CCW - 1 = CW
        glDrawElementsBaseVertex(scene.meshes[0].primitive_type,
                                 3,
                                 GL::type_id(Index_type{}),
                                 reinterpret_cast<GLvoid*>(offset),
                                 base);
        glFrontFace(GL_CCW);
#else   // render with GL_TRIANGLES
        constexpr auto verts_per_triangle = 3u;
        constexpr auto tris_per_cell = 2u;
        const auto cells = scene.sel_cell.y * (scene.height_map.cols - 1) + scene.sel_cell.x;
        const auto offset = (tris_per_cell * cells + scene.sel_tri) * verts_per_triangle * sizeof(Index_type);
        glDrawElements(scene.meshes[0].primitive_type,
                       verts_per_triangle,
                       GL::type_id(Index_type{}),
                       reinterpret_cast<GLvoid*>(offset));
#endif  // TERRAIN_TRIANGLE_STRIP

        glBindVertexArray(0);
        glUseProgram(0);
    }
}

void update_frame_time(std::chrono::milliseconds *ticks,
                       unsigned *frames_rendered,
                       GLFWwindow *window)
{
    ++*frames_rendered;
    if(ticks->count() >= 1000)
    {
        std::ostringstream ss;
        ss << app_title << " ~ " << (1000.0 / *frames_rendered) << "ms/frame";
        const auto str_mpf = ss.str();
        glfwSetWindowTitle(window, str_mpf.c_str());
        *frames_rendered = 0;
        *ticks = std::chrono::milliseconds{0};
    }
}

}   // unnamed namespace

void pick(float x, float y, Scene *scene)
{
    reset_selection(scene);
    /*
     * for the z (world) coordinate of the pick point we need the focal length
     * which is cot (FoV/2) but since the perspective projection matrix already has it
     * we can reuse the value instead of computing it; this optimisation is discussed in
     * Frank D. Luna's excellent book; it explains picking quite comprehensively
     */
    const auto focal_length = scene->cam.projection[1][1];
    // const auto focal_length = glm::cot(scene->FoV / 2.0f);

    /*
     * in view space this point generated would be (x, y, z, 1); in that space
     * subtracting origin (0, 0, 0, 1) from that point would give the same point
     * with the w coordinate becoming 0; thus this is the ray in view space
     */
    const glm::vec4 view_ray(((2.0f * x / screen_width) - 1.0f) * scene->cam.aspect_ratio,
                             1.0f - (2.0f * y / screen_height),
                             -focal_length,
                             0.0f);
    /*
     * we've Mw->v, we need its inverse Mv->w; since Mw->v = TR
     * Mv->w = R^-1 T^-1; this is done by GLM's affineInverse; although just transposing the upper
     * 3x3 matrix in Mw->v would suffice for transforming the ray, we need the affine inverse for
     * transforming the ray origin i.e. camera location in world space
     */
    const auto inv_view = glm::affineInverse(scene->cam.view_xform);
    // since we're transforming a ray, the last column doesn't matter, but we use it for ray origin
    const auto ray_dir = glm::normalize(glm::vec3(inv_view * view_ray));
    const auto ray_org = glm::vec3(inv_view[3]);    // camera locaton in world space
    const Primitives::Ray pick_ray = Primitives::clip_ray({ray_org, ray_dir},
                                                           scene->height_map.min,   // AABB with min and max
                                                           scene->height_map.max);
    /*
     * this ray originates from camera, goes throw the click point on the view plane and extends to infinity;
     * however, bounding it within min and max terrain height is optimal since there can't be vertices above
     * or below those limits.
     */
    // TODO: if the ray only touches an edge / corner of the AABB (formed by height_map.min and max), then
    // too we'd have a degenerate ray, however grid ray tracing isn't required in that case and just a hit
    // test of a triangle would do

    if (!Primitives::is_degenerate(pick_ray))
    {
        hit_test_height_map(scene, pick_ray);
    }
}

int main(int /*argc*/, char** /*argv*/)
{
    GLFW_wrapper glfw;
    auto glfw_wnd = std::move(create_window(glfw));
    Scene scene{FoV,
                aspect_ratio,
                near,
                far};
    Primitives::parse_SDL(scene_file,
                          &scene.height_map,
                          &scene.meshes);

    std::vector<GL::VAO> VAOs;
    upload_meshes(scene.meshes, &VAOs);

    // http://www.parashift.com/c++-faq-lite/memfnptr-vs-fnptr.html
    // according to C++ FAQ, you cannot pass a member function to a C callback; passing a functor, lambda, boost::bind,
    // etc. is for a C++ callback implemented with templates; instead use the void* user_data allowed in the callback;
    // GLFW gives GLFWwindow->SetWindowUserPointer for the same
    glfwSetWindowUserPointer(glfw_wnd.get(), reinterpret_cast<void*>(&scene));
    glfwSetKeyCallback(glfw_wnd.get(), handle_key);
    glfwSetMouseButtonCallback(glfw_wnd.get(), handle_mouse);

    GL::Program program{GL::setup_program("shaders/pos.vert",
                                          "shaders/col.frag")};
    const auto xform_id = glGetUniformLocation(program, "PVM");
    assert(xform_id != -1);

    GL::Program program_sel{GL::setup_program("shaders/pos.vert",
                                              "shaders/solid.frag")};
    const auto colour_id = glGetUniformLocation(program_sel, "solidColour");
    assert(colour_id != -1);

    init_rendering();
    unsigned frames = 0u;
    std::chrono::milliseconds since_last_sec;
    std::chrono::steady_clock::time_point now, earlier;
    while(!glfwWindowShouldClose(glfw_wnd.get()))
    {
        now = std::chrono::steady_clock::now();
        auto const elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(now - earlier);
        auto const ticks = elapsed.count();
        render(program, scene, VAOs, xform_id);
        render_sel(program_sel, scene, VAOs, xform_id, colour_id);
        glfwSwapBuffers(glfw_wnd.get());
        handle_input(glfw_wnd.get(), &scene.cam, ticks);
        since_last_sec += elapsed;
        update_frame_time(&since_last_sec, &frames, glfw_wnd.get());
        earlier = now;
    }
}
