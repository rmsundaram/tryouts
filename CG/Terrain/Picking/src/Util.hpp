#ifndef UTIL_HPP
#define UTIL_HPP

namespace Array
{

template <typename T>
inline constexpr typename std::enable_if<std::is_array<T>::value, size_t>::type
max_index(const T& /*arr*/)
{
    return std::extent<T>::value - 1;
}

inline
size_t item_index(size_t item_id,
                  size_t base_id,
                  size_t max_id,
                  size_t err_index)
{
    const auto max_index = max_id - base_id;
    int index = item_id - base_id;
    return ((index < 0) || (static_cast<unsigned>(index) > max_index)) ?
            err_index :
            static_cast<size_t>(index);
}

}   // namespace Array

inline void check(bool condition, const char *error_msg)
{
    if(condition != true)
    {
        throw std::runtime_error(error_msg);
    }
}

inline std::ostream& tab(std::ostream &os)
{
    return os << '\t';
}

// returns true when x is within [min, max]
template <typename T>
typename std::enable_if<std::is_arithmetic<T>::value, bool>::type
is_within(T x, T a, T b)
{
    return (a < b) ? ((x >= a) && (x <= b)) : ((x >= b) && (x <= a));
}

template <typename T>
typename std::enable_if<std::is_arithmetic<T>::value, T>::type
distance(T x1, T x2)
{
    return std::abs(x2 - x1);
}

template <typename T>
typename std::enable_if<std::is_arithmetic<T>::value, T>::type
closer(T x, T x1, T x2)
{
    // finding the lesser of the two after taking abs on both was considered
    // but it will not work when one is positive and the other is not
    return (distance(x, x1) <= distance(x, x2)) ? x1 : x2;
}

template <typename T, typename U>
typename std::enable_if<std::is_floating_point<T>::value, T>::type
prev_int(T x, U dx)
{
    // dx is really vector, it has the direction and the magnitude of shift;
    // the word prev is from the PoV of the ray dx from the origin x
    // if positive round towards -∞ (floor) else towards +∞ (ceil)
    // passing dx = 0 is undefiend since it's actually a degenerate case
    return (dx < U{}) ? std::ceil(x) : std::floor(x);
}

template <typename T>
typename std::enable_if<std::is_integral<T>::value, bool>::type
is_power_of_2(T v)
{
    return (v && !(v & (v - 1)));
}

template <typename T>
typename std::enable_if<std::is_floating_point<T>::value, bool>::type
less(T f1, T f2, T e = glm::epsilon<T>())
{
    return (f1 < f2) && glm::epsilonNotEqual(f1, f2, e);
}

template <typename T>
typename std::enable_if<std::is_floating_point<T>::value, bool>::type
less_equal(T f1, T f2, T e = glm::epsilon<T>())
{
    return (f1 < f2) || glm::epsilonEqual(f1, f2, e);
}

template <typename T>
typename std::enable_if<std::is_floating_point<T>::value, T>::type
floor_if_not_int(T f, T epsilon = glm::epsilon<T>())
{
    T integral;
    if (glm::epsilonEqual(std::modf(f, &integral), T(), epsilon))
    {
        return integral;
    }
    return std::floor(f);
}

template <typename T>
struct Vec_compare
{
    using FT = typename T::value_type;
    bool operator()(const T &a, const T &b)
    {
        // TODO: accessing GLM's vectors this way isn't strictly correct since
        // GLM declares this as a struct and not an array, hence padding can mar the data
        return std::lexicographical_compare(&a[0], &a[0] + a.length(),
                                            &b[0], &b[0] + b.length(),
                                            [](FT f1, FT f2)
                                            {
                                                return less<FT>(f1, f2, glm::epsilon<FT>());
                                            });
    }
};

#endif // UTIL_HPP
