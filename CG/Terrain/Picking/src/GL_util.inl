template <typename T>
GL::Buffer GL::upload_data(const std::vector<T> &data, GLenum target)
{
    /*
     *  VBOs are “buffers” of video memory – just a bunch of bytes containing any kind of binary data you want.
     *  You can upload 3D points, colors, your music collection, poems to your loved ones – the VBO doesn’t care,
     *  because it just copies a chunk of memory without asking what the memory contains.
     */
    GLuint vbo_id;
    glGenBuffers(1, &vbo_id);
    glBindBuffer(target, vbo_id);
    GL::Buffer buffer = vbo_id;
    glBufferData(target, data.size() * sizeof(T), data.data(), GL_STATIC_DRAW);
    glBindBuffer(target, 0);

    return buffer;
}

template <typename T, size_t attribs>
GL::VAO GL::setup_attributes(GLuint vertex_buffer,
                             const size_t (&attrib_length) [attribs],
                             GLuint index_buffer)
{
    /*
     * The second step to rendering our triangle is to send the points from the VBO into the shaders. Remember
     * how the VBOs are just chunks of data, and have no idea what type of data they contain? Somehow you have to tell
     * OpenGL what type of data is in the buffer, and this is what VAOs are for. VAOs are the link between the VBOs and
     * the shader variables. VAOs describe what type of data is contained within a VBO, and which shader variables the
     * data should be sent to. glGetAttribLocation or glBindAttribLocation is used to get/set the attribute (shader)
     * variable index. Alternatively, layout location can be used to set the index of a named attribute variable.
     *
     * If you have used VBOs without VAOs in older versions of OpenGL, then you might not agree with this description of
     * VAOs. You could argue that “vertex attributes” set by glVertexAttribPointer are the link between the VBO and that
     * shaders, not VAOs. It depends on whether you consider the vertex attributes to be “inside” the VAO (which I do),
     * or whether they are global state that is external to the VAO. Using the 3.2 core profile and ATI drivers, the VAO
     * is not optional – glEnableVertexAttribArray, glVertexAttribPointer and glDrawArrays all cause INVALID_OPERATION
     * error, if there is no VAO bound. This is what leads me to believe that the vertex attributes are inside the VAO,
     * and not global state. The 3.2 core profile spec says that VAOs are required, but it's told that only ATI drivers
     * throw errors if no VAO is bound. Here are some quotes from the OpenGL 3.2 core profile specification:
     *
     *            All state related to the definition of data used by the vertex processor is encapsulated
     *            in a vertex array object.
     *
     *            The currently bound vertex array object is used for all commands which modify vertex
     *            array state, such as VertexAttribPointer and EnableVertexAttribArray; all commands which
     *            draw from vertex arrays, such as DrawArrays and DrawElements; and all queries of vertex
     *            array state (see chapter 6).
     *
     * glVertexAttribPointer predates VAOs, so there was a time when vertex attributes were just global state. You could
     * see VAOs as just a way to efficiently change that global state. I prefer to think of it like this: if you don’t
     * create a VAO, then OpenGL provides a default global VAO. So when you use glVertexAttribPointer you are still
     * modifying the vertex attributes inside a VAO, it’s just that you’re modifying the default VAO instead of one you
     * created yourself.
     */

    GLuint vao_id;
    glGenVertexArrays(1, &vao_id);
    glBindVertexArray(vao_id);
    // if a valid buffer is bound to GL_ARRAY_BUFFER, when a VAO is binded this gets saved in it, while the
    // same isn't true for GL_ELEMENT_ARRAY_BUFFER (although the spec says it should); hence bind any buffer
    // only after binding a VAO for them to be captured in it
    GL::VAO vao = vao_id;

    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    const auto vertex_size = std::accumulate(attrib_length, attrib_length + attribs, 0u) * sizeof(T);
    size_t offset = 0u;
    for(auto i = 0u; i < attribs; ++i)
    {
        glEnableVertexAttribArray(i);
        glVertexAttribPointer(i,
                              attrib_length[i],
                              GL::type_id(T{}),
                              GL_FALSE,
                              vertex_size,
                              reinterpret_cast<GLvoid*>(offset * sizeof(T)));
        offset += attrib_length[i];
    }
    /*glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), reinterpret_cast<GLvoid*>(3 * sizeof(float)));*/

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);

    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return vao;
}

GL::Texture GL::upload_texture(const char *file_name, GLenum texture_unit)
{
    auto texture_storage = gli::load_dds(file_name);
    assert(!texture_storage.empty());
    gli::texture2D tex{texture_storage};

    GLuint texture_id;
    glGenTextures(1, &texture_id);
    glActiveTexture(texture_unit);
    glBindTexture(GL_TEXTURE_2D, texture_id);
    GL::Texture texture = texture_id;

    // without setting GL_TEXTURE_MIN_FILTER to GL_LINEAR the plane was rendered black; the default
    // GL_TEXTURE_MIN_FILTER​ state is GL_NEAREST_MIPMAP_LINEAR​. And because OpenGL defines the default
    // GL_TEXTURE_MAX_LEVEL​ to be 1000, OpenGL will expect there to be mipmap levels defined. Since only a few
    // are defined, OpenGL will consider the texture incomplete until the GL_TEXTURE_MAX_LEVEL​ is properly set,
    // or the GL_TEXTURE_MIN_FILTER​ parameter is set to not use mipmaps e.g. GL_LINEAR. Better code would be to
    // use texture storage functions (on OpenGL 4.2+ or ARB_texture_storage) to allocate the texture's storage,
    // then upload with glTexSubImage2D​. This issue applies only to the minification filter, the magnification
    // filter don't have mipmaps ~ opengl.org/wiki/Common_Mistakes
    const auto max_levels = tex.levels() - 1;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);       // initial value is 0, hence redundant
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, max_levels);

    // since multiple (mipmap) levels are loaded for the texture, setting it to just
    // GL_LINEAR isn't correct as mipmaps will not be used
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

    for(auto i = 0u; i <= max_levels; ++i)
    {
        const auto width = tex[i].dimensions().x, height = tex[i].dimensions().y;
        // the pixel width and height of a texture should be a power of 2
        assert(is_power_of_2(width) && is_power_of_2(height));
        glCompressedTexImage2D(GL_TEXTURE_2D,
                               i,
                               gli::internal_format(tex.format()),
                               width,
                               height,
                               0,
                               tex[i].size(),
                               tex[i].data());
    }
    // Existing libraries generate mip-maps on the CPU, which is an horrendously slow process. In the past, to avoid
    // this slow process, games used DDS (Microsoft Direct Draw Surface) images, which had mip-maps pre-built in them.
    // In modern graphics we can use the GPU (glGenerateMipmap) to do this - which is extremely efficient.
    // ~ antongerdelan.net/opengl/texturemaps.html

    glBindTexture(GL_TEXTURE_2D, 0);
    return texture;
}
