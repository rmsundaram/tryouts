#ifndef CONTEXT_HPP
#define CONTEXT_HPP

namespace
{

using GLFW_wrapper = Dataless_wrap<decltype(&glfwTerminate), glfwTerminate>;
using GLFW_window = std::unique_ptr<GLFWwindow, decltype(&glfwDestroyWindow)>;

constexpr unsigned screen_width  = 1024u, screen_height = 600u;
constexpr float aspect_ratio = static_cast<float>(screen_width) / screen_height;
const float FoV = glm::quarter_pi<float>();
constexpr float near = 50.0f;
constexpr float far = 5000.0f;
const char* const app_title = "Grid Traversal Picking";

GLFWwindow* setup_context(uint32_t width, uint32_t height)
{
    /*
     * without setting the version (defaulting to 1.0) or setting it < 3.2, requesting for core
     * will fail since context profiles only exist for OpenGL 3.2+; likewise forward compatibility
     * only exist from 3.0 onwards. 3.0 marked deprecated, 3.1 removed deprecated (except
     * wide lines), 3.2 reintroduced deprecated under compatibility profile
     */
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE,
                   GLFW_OPENGL_CORE_PROFILE);

   /*
    * since core only has undeprecated features (except wide lines), making the context
    * forward-compatible is moot; only wide lines, the lone deprecated feature in core profile,
    * will be removed
    * http://www.opengl.org/wiki/Core_And_Compatibility_in_Contexts#Forward_compatibility
    * glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
    */
    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

#ifndef NDEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

    auto window = glfwCreateWindow(width,
                                   height,
                                   app_title,
                                   nullptr,
                                   nullptr);
    ::check(window != nullptr, "Failed to create window");
#ifndef NDEBUG
    int w, h;
    glfwGetWindowSize(window, &w, &h);
    assert((w == static_cast<signed>(width)) && (h == static_cast<signed>(height)));
#endif
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
#ifdef _WIN32
    // http://stackoverflow.com/q/16285546
    // GLFW doesn't honour call to VSYNC when DWM compositing is enabled, which is practically all machines today;
    // turn it on using the using WGL_EXT_swap_control extension on Win32
    // TODO: do this for other platforms too
    typedef int (APIENTRY *PFNWGLSWAPINTERVALEXTPROC)(int interval);
    auto wglSwapInterval = reinterpret_cast<PFNWGLSWAPINTERVALEXTPROC>(glfwGetProcAddress("wglSwapIntervalEXT"));
    if (wglSwapInterval)
    {
        typedef int (APIENTRY *PFNWGLGETSWAPINTERVALEXTPROC)(void);
        auto wglGetSwapInterval =
            reinterpret_cast<PFNWGLGETSWAPINTERVALEXTPROC>(glfwGetProcAddress("wglGetSwapIntervalEXT"));
        // if the get function isn't there or is returning 0
        if (!wglGetSwapInterval || !wglGetSwapInterval())
            wglSwapInterval(1);
    }
#endif
    return window;
}

#ifndef NDEBUG

APIENTRY
void gl_debug_logger(GLenum source,
                     GLenum type,
                     GLuint id,
                     GLenum severity,
                     GLsizei /*length*/,
                     const GLchar *msg,
                     const void *file_ptr)
{
    std::ostringstream ss;
    ss << "GLError 0x"
       << std::hex << id << std::dec
       << ": "
       << msg <<
       " [source=";

    const char *sources[] = {
                                "API",
                                "WINDOW_SYSTEM",
                                "SHADER_COMPILER",
                                "THIRD_PARTY",
                                "APPLICATION",
                                "OTHER",
                                "UNDEFINED"
                            };
    size_t index = Array::item_index(source,
                                     GL_DEBUG_SOURCE_API_ARB,
                                     GL_DEBUG_SOURCE_OTHER_ARB,
                                     Array::max_index(sources));
    ss << sources[index];
    if (Array::max_index(sources) == index)
    {
        ss << " (" << source << ")";
    }

    const char *types[] = {
                              "ERROR",
                              "DEPRECATED_BEHAVIOR",
                              "UNDEFINED_BEHAVIOR",
                              "PORTABILITY",
                              "PERFORMANCE",
                              "OTHER",
                              "UNDEFINED"
                          };
    index = Array::item_index(type,
                              GL_DEBUG_TYPE_ERROR_ARB,
                              GL_DEBUG_TYPE_OTHER_ARB,
                              Array::max_index(types));
    ss << " type=" << types[index];
    if (Array::max_index(types) == index)
    {
        ss << " (" << type << ")";
    }

    const char *severities[] = {
                                   "HIGH",
                                   "MEDIUM",
                                   "LOW",
                                   "UNKNOWN"
                               };
    // as per the extension, HIGH is the base (smaller) value
    index = Array::item_index(severity,
                              GL_DEBUG_SEVERITY_HIGH_ARB,
                              GL_DEBUG_SEVERITY_LOW_ARB,
                              Array::max_index(severities));
    ss << " severity=" << severities[index];
    if (Array::max_index(severities) == index)
    {
        ss << " (" << severity << ")";
    }
    ss << "]";

    const auto log = ss.str();
    FILE *out_file = reinterpret_cast<FILE*>(const_cast<void*>(file_ptr));
    fprintf(out_file, "%s\n", log.c_str());

    ::check(severity != GL_DEBUG_SEVERITY_HIGH_ARB, "High severity GL error logged");
}

void setup_debug(bool enable)
{
    if(glewIsSupported("GL_ARB_debug_output"))
    {
        glDebugMessageCallbackARB(enable ? gl_debug_logger : nullptr, stderr);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
        ::check(GL_NO_ERROR == glGetError(), "Unable to set synchronous debug output");
    }
}
#endif  // NDEBUG

void handle_key(GLFWwindow *window,
                int key,
                int /*scancode*/,
                int action,
                int /*mods*/)
{
    auto scene = reinterpret_cast<Scene*>(glfwGetWindowUserPointer(window));
    assert(scene);

    if(GLFW_PRESS == action)
    {
        switch(key)
        {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, GL_TRUE);
            break;
        case GLFW_KEY_R:
            scene->cam.reset();
            break;
        case GLFW_KEY_0:
            scene->rendering_mode = (scene->rendering_mode == GL_LINE) ? GL_FILL : GL_LINE;
            break;
        }
    }
}

void handle_mouse(GLFWwindow *window,
                  int button,
                  int action,
                  int /*mods*/)
{
    if ((GLFW_MOUSE_BUTTON_LEFT == button) && (GLFW_RELEASE == action))
    {
        double xpos, ypos;
        glfwGetCursorPos(window, &xpos, &ypos);
        auto scene = reinterpret_cast<Scene*>(glfwGetWindowUserPointer(window));
        assert(scene);
        pick(static_cast<float>(xpos),
             static_cast<float>(ypos),
             scene);
    }
}

void handle_input(GLFWwindow *window,
                  Camera *cam,
                  int64_t ticks)
{
    const auto slide_offset = static_cast<float>(ticks) * Camera::slide_offset;
    const auto rot_delta = static_cast<float>(ticks) * Camera::rot_angle;

    // rotation
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_UP))
        cam->rotate_X(rot_delta);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_DOWN))
        cam->rotate_X(-rot_delta);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_LEFT))
        cam->rotate_Y(rot_delta);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_RIGHT))
        cam->rotate_Y(-rot_delta);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_PERIOD))
        cam->rotate_Z(rot_delta);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_COMMA))
        cam->rotate_Z(-rot_delta);

    // translation
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_D))
        cam->slide_X(slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_A))
        cam->slide_X(-slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_W))
        cam->slide_Y(slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_X))
        cam->slide_Y(-slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_Z))
        cam->slide_Z(slide_offset);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_S))
        cam->slide_Z(-slide_offset);

    // FoV
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_KP_ADD))
        cam->increase_FoV();
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_KP_SUBTRACT))
        cam->decrease_FoV();

    glfwPollEvents();
}

void window_error(int, const char *error_msg)
{
    throw std::runtime_error(error_msg);
}

GLFW_window create_window(GLFW_wrapper &glfw)
{
    glfwSetErrorCallback(window_error);
    glfw = std::move(GLFW_wrapper(Check_return<decltype(&glfwInit),
                                               glfwInit,
                                               decltype(GL_TRUE),
                                               GL_TRUE>{}
                                 ));
    // RAII warpping isn't mandatory here, since glfwTerminate destroys open windows, if any remain
    GLFW_window wnd_ptr{setup_context(screen_width, screen_height),
                        glfwDestroyWindow};

    glewExperimental = GL_TRUE;
    auto err = glewInit();
    ::check(GLEW_OK == err, reinterpret_cast<const char*>(glewGetErrorString(err)));
    glGetError();       // to clear error due to GLEW bug #120

#ifndef NDEBUG
    setup_debug(true);
#endif
    return wnd_ptr;
}

} // unnamed namespace

#endif // CONTEXT_HPP
