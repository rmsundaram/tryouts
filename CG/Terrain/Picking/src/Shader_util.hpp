#ifndef __SHADER_UTIL_HPP__
#define __SHADER_UTIL_HPP__

namespace GL
{
    /**
     * @brief Takes shader code file and type and returns a compiled shader handle.
     */
    Shader compile_shader(GLenum shader_type, const std::string &shader_file_path);

    /**
     * @brief Takes shader and links them into the given program handle
     */
    template <typename T>
    void link_program(GLuint prog_id, const T &shaders);

    /**
     * @brief Takes shader file paths, compiles and links them in to a program; returns program handle.
     */
    Program setup_program(const std::string &vert_shader_file, const std::string &frag_shader_file);
}

#include "Shader_util.inl"

#endif  //  __SHADER_UTIL_HPP__
