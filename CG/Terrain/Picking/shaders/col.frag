#version 330

in vec4 colour;

out vec4 finalColour;

void main()
{
    finalColour = colour;
}
