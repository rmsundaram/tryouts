#version 330

uniform mat4 PVM;

layout (location = 0) in vec4 pos;
layout (location = 1) in vec4 col;

out vec4 colour;

void main()
{
    gl_Position = PVM * pos;
    colour = col;
}
