Pre-built Win32 binary available in the [Downloads section](https://bitbucket.org/rmsundaram/tryouts/downloads).

SCREENSHOT
==========
![TerrainPick.png](https://bitbucket.org/repo/no48Gg/images/2591742115-TerrainPick.png "Grid traversal based terrain picking")

CONTROLS
========

Action                    | Key
--------------------------|------------
Pick triangle on terrain  | Mouse click
Slide camera ±X           | `D`/`A`
Slide camera ±Y           | `W`/`X`
Slide camera ±Z           | `S`/`Z`
Rotate camera X           | `↑`/`↓`
Rotate camera Y           | `←`/`→`
Rotate camera Z           | `,`/`.`
Toggle wireframe render   | `0`
Reset camera              | `R`
Decrease FoV (Telephoto)  | `Numpad −`
Increse FoV (Wideangle)   | `Numpad +`
Exit                      | `Esc`

SCENE
=====
The scene rendered is described as a simple SDL (scene description language) file at `./data/map.sdl`. This can be edited to alter the scene without recompiling the project.

LEARNINGS
=========

1. Terrain rendering from bitmap data (RasterTek.com)
2. Voxel traversal both in int and float spaces (see the Grid Ray Tracing workout)
3. Terrain picking using 2D grid traversal (as implemented by the above workout). When exiting each cell, the ray's height at exit point is compared against the cell's height at the same exit point. If it's lesser or equal then the ray has made a crossover by penetrating the terrain. Now a couple of ray-triangle intersection tests are made to determine the triangle that was hit and the point of intersection too.
4. OpenGL BaseVertex and MultiDraw calls[1], [2], [3], [4], [5]
5. Z buffer resolution[6] and z-fighting — The greater the ratio of far/near, the less effective the depth buffer will be at distinguishing surfaces that are near each other. Having a large depth range i.e. huge difference between near and far planes would lead to artifacts such as missing triangles when multiple fragments are fighting to win when their depth values match. There isn't a permanent "fix" for this, it can be ameliorated by reducing zFar or increasing zNear. Another option is to increase the depth buffer resolution. This issue[7] was seen with the initial value of zNear = 0.001 and zFar = 5000. However, Humus, in his phonewire demo[8], sets zNear = 0.1 and zFar = 5000, the only differnce being the zNear is closer in his, which makes the issue vanish. This is explained very well in OpenGL Wiki[9], [10]:
> Depth buffering seems to work, but polygons seem to bleed through polygons that are in front of them. What's going on? You may have configured your zNear and zFar clipping planes in a way that severely limits your depth buffer precision. Generally, this is caused by a zNear clipping plane value that's too close to 0.0. As the zNear clipping plane is set increasingly closer to 0.0, the effective precision of the depth buffer decreases dramatically. Moving the zFar clipping plane further away from the eye always has a negative impact on depth buffer precision, but it's not one as dramatic as moving the zNear clipping plane.
6. The reason a range of 0.001 to 5000 was set is that the terrain should be visible even when the camera is far off. However, this compromises depth buffer resolution. A solution, other than the ones stated above, would be to maintain the difference between near and far values at a value appropriate for a good z-buffer resolution but vary the actual values of zNear and zFar such that the terrain is between them even when the camera moves away. To visualise this, say when the camera, at y = 300, is shooting a top view of the terrain whose height ranges between 0 and 255, then the zNear and zFar can be 45 and 300. As we move farther, say at y = 400, we change them to 145 and 400. Even when the camera is at 1000, zNear = 745, zFar = 1000 should work, since in all these cases the difference zFar-zNear was maintained as 255. Here zNear and and zFar is in camera space and the rest are in world space.
7. Humus' terrain in the phonewire demo never gets clipped out of view, irrespective of how afar the camera is. How he achieves this in D3D10 is unknown, but this is achieved, in OpenGL, here by enabling depth clamping as suggested by Jason[11]:
> Enabling depth clamping, essentially diables clipping by the near and far planes, and any frag whose depth goes beyond [-1, 1] will be clamped to this interval. The downside to this is that it screws up depth tests between fragments that are being clamped, but usually those objects are far away.
8. Vertex texture fetch is a feature where vertex shader can do a texture lookup; the number of such lookups can be known by querying `GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS`; this is particularly useful when rendering terrains if the texture levelling (peak heights) can be done at render time using vertex shaders - this approach, called displacement mapping, isn't implemented here.
9. Facet culling has 3 knobs and they can be changed. Their state is in effect until changed again and the current state affects all draw calls until next change. The knobs (i) the winding order for front facing triangles, (ii) which face should be culled (iii) facet culling on/off.
10. Primitive restart index could be used in rendering a height map as mbsoftworks.sk points out[12]:
> When we've vertices like
```
                0  1  2  3  4
                5  6  7  8  9
               10 11 12 13 14
```
> The indices for GL_TRIANGLE_STRIP would be 0 5 1 6 2 7 3 8 4 9, however for the next row, we can't put a 10 here since that would mean a continuation of the previous series. However we can have a new series as 5 10 6 11 7 12 8 13 9 14 but only in a separate buffer, since in the previous buffer having anything after 9 would be continuing the previous strip primitive. To avoid having to allocate a new index buffer and to continue using the same buffer, GL gives the primitive restart index, which we insert between the two series (between 9 and 5) some arbitraryly chosen index, say 200 to notify that restart of the primitive i.e. to begin a new triangle strip from the next index onwards. The chosen index is informed to GL through glPrimitiveRestartIndex. However, this requires more indices in memory that the BaseVertex techinique which has them only for one row. This matters when the number of rows are high.

### REFERENCES

1. https://www.opengl.org/registry/specs/ARB/draw_elements_base_vertex.txt
2. http://arcsynthesis.org/gltut/Positioning/Tut05 Optimization Base Vertex.html
3. http://stackoverflow.com/a/26755538/183120
4. https://www.opengl.org/wiki/Vertex_Rendering
5. https://www.opengl.org/wiki/Vertex_Specification_Best_Practices
6. Figure II.21, 3-D Computer Graphics by Samuel R. Buss.
7. http://gamedev.stackexchange.com/q/87329/14025
8. http://humus.name/index.php?page=3D&ID=89
9. https://www.opengl.org/wiki/Depth_Buffer_Precision
10. http://www.sjbaker.org/steve/omniv/love_your_z_buffer.html
11. http://stackoverflow.com/a/11949687/183120
12. http://www.mbsoftworks.sk/index.php?page=tutorials&series=1&tutorial=8
13. §5.10, XNA 3.0 Game Programming Recipes, Riemer Grootjans
14. http://www.gamedev.net/topic/529824-terrain-picking/
15. http://www.gamedev.net/topic/528369-terrain-collision/
16. http://www.gamedev.net/topic/617341-xna-picking
17. http://vterrain.org/Implementation/Libs/ray-heighhtfield.html
18. http://www.gamedev.net/topic/618968-picking-terrain-with-mouse-for-editing/#entry4906242
