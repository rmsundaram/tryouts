#include "D3D11Graphics.h"

#include <D3Dcompiler.h>
#include <stdexcept>
#include <fstream>
#include <memory>

struct PerspectiveTransforms
{
    DirectX::XMMATRIX world;
    DirectX::XMMATRIX view;
    DirectX::XMMATRIX projection;
};

struct VertexData
{
    DirectX::XMFLOAT3 position;
    DirectX::XMFLOAT4 colour;
};

VertexData sun[] = {
                    { DirectX::XMFLOAT3(-1.0f, -1.0f, -1.0f), DirectX::XMFLOAT4( 0.0f, 0.0f, 1.0f, 1.0f ) }, // llb 0
                    { DirectX::XMFLOAT3(-1.0f,  1.0f, -1.0f), DirectX::XMFLOAT4( 0.0f, 1.0f, 0.0f, 1.0f ) }, // ulb 1
                    { DirectX::XMFLOAT3( 1.0f,  1.0f, -1.0f), DirectX::XMFLOAT4( 1.0f, 0.0f, 0.0f, 1.0f ) }, // urb 2
                    { DirectX::XMFLOAT3( 1.0f, -1.0f, -1.0f), DirectX::XMFLOAT4( 1.0f, 1.0f, 0.0f, 1.0f ) }, // lrb 3
                    { DirectX::XMFLOAT3( 1.0f, -1.0f,  1.0f), DirectX::XMFLOAT4( 1.0f, 0.0f, 1.0f, 1.0f ) }, // lrf 4
                    { DirectX::XMFLOAT3(-1.0f, -1.0f,  1.0f), DirectX::XMFLOAT4( 0.0f, 0.0f, 0.0f, 1.0f ) }, // llf 5
                    { DirectX::XMFLOAT3(-1.0f,  1.0f,  1.0f), DirectX::XMFLOAT4( 1.0f, 1.0f, 1.0f, 1.0f ) }, // ulf 6
                    { DirectX::XMFLOAT3( 1.0f,  1.0f,  1.0f), DirectX::XMFLOAT4( 0.0f, 1.0f, 1.0f, 1.0f ) }  // urf 7
                   };

// counterclock wound faces are culled; hence the order is clockwise
unsigned short sunIndices[] = { // front
                                0, 1, 2,
                                0, 2, 3,

                                // right
                                4, 2, 7,
                                4, 3, 2,

                                // back
                                5, 7, 6,
                                5, 4, 7,

                                // left
                                0, 6, 1,
                                0, 5, 6,

                                // floor
                                5, 0, 3,
                                5, 3, 4,

                                // ceil
                                6, 2, 1,
                                6, 7, 2
                              };


D3D11Graphics::D3D11Graphics(HWND hwindow,
                             unsigned int width,
                             unsigned int height) :
            m_paused(false),
            m_angleY(0.0f),
            m_width(static_cast<float>(width)),
            m_height(static_cast<float>(height)),
            m_pPShader(nullptr),
            m_pVShader(nullptr),
            m_pTXMatrix(nullptr),
            m_pSunIndicesData(nullptr),
            m_pSunData(nullptr),
            m_pDepthStencilView(nullptr),
            m_pRenderTargetView(nullptr),
            m_pd3d11Device (nullptr),
            m_pd3d11DeviceContext (nullptr),
            m_pdxgiSwapChain (nullptr)
{
    UINT flags = 0;
#if defined(_DEBUG) || defined(DEBUG)
    //flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
    D3D_FEATURE_LEVEL featureLevels[] = { D3D_FEATURE_LEVEL_11_0,
                                          D3D_FEATURE_LEVEL_10_1,
                                          D3D_FEATURE_LEVEL_10_0,
                                          D3D_FEATURE_LEVEL_9_3 };
    DXGI_SWAP_CHAIN_DESC swapChainDesc = { };
    swapChainDesc.BufferCount = 1;
    swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    swapChainDesc.BufferDesc.Width = width;
    swapChainDesc.BufferDesc.Height = height;
    swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
    swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
    swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    swapChainDesc.OutputWindow = hwindow;
    swapChainDesc.SampleDesc.Count = 2; // MSAA
    swapChainDesc.SampleDesc.Quality = 0;
    swapChainDesc.Windowed = TRUE;

    if (FAILED(D3D11CreateDeviceAndSwapChain(nullptr,
                                             D3D_DRIVER_TYPE_HARDWARE,
                                             NULL,
                                             flags,
                                             featureLevels,
                                             ARRAYSIZE(featureLevels),
                                             D3D11_SDK_VERSION,
                                             &swapChainDesc,
                                             &m_pdxgiSwapChain,
                                             &m_pd3d11Device,
                                             nullptr,
                                             &m_pd3d11DeviceContext)))
        throw std::runtime_error("Error initializing D3D11 graphics instance");

    ID3D11Texture2D *pBackBuffer = nullptr;
    if (FAILED(m_pdxgiSwapChain->GetBuffer(0, __uuidof(pBackBuffer), reinterpret_cast<void**>(&pBackBuffer))))
        throw std::runtime_error("Error getting back buffer");

    if (FAILED(m_pd3d11Device->CreateRenderTargetView(pBackBuffer, NULL, &m_pRenderTargetView)))
        throw std::runtime_error("Error creating render target view");
    releaseNclearPtr(&pBackBuffer);

    D3D11_TEXTURE2D_DESC dsd = { };
    dsd.ArraySize = 1;
    dsd.MipLevels = 1;
    dsd.Width = static_cast<UINT>(m_width);
    dsd.Height = static_cast<UINT>(m_height);
    dsd.Usage = D3D11_USAGE_DEFAULT;
    dsd.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    dsd.Format = DXGI_FORMAT_D16_UNORM;
    dsd.SampleDesc.Count = 1;
    ID3D11Texture2D *pDepthStencilBuffer;
    if (FAILED(m_pd3d11Device->CreateTexture2D(&dsd, nullptr, &pDepthStencilBuffer)))
        throw std::runtime_error("Error creating depth stencil buffer");
    D3D11_DEPTH_STENCIL_VIEW_DESC dsvd = { };
    dsvd.Format = dsd.Format;
    dsvd.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    if (FAILED(m_pd3d11Device->CreateDepthStencilView(pDepthStencilBuffer, &dsvd, &m_pDepthStencilView)))
        throw std::runtime_error("Error creating depth stencil view");
    releaseNclearPtr(&pDepthStencilBuffer);

    m_pd3d11DeviceContext->OMSetRenderTargets(1, &m_pRenderTargetView, m_pDepthStencilView);

    D3D11_VIEWPORT viewport = { 0 };
    viewport.Width = static_cast<FLOAT>(width);
    viewport.Height = static_cast<FLOAT>(height);
    viewport.MaxDepth = 1.0f;
    m_pd3d11DeviceContext->RSSetViewports(1, &viewport);
}

void D3D11Graphics::SetupData()
{
    // sun vextes data
    D3D11_BUFFER_DESC buffDesc = { 0 };
    D3D11_SUBRESOURCE_DATA srd = { 0 };
    buffDesc.Usage = D3D11_USAGE_DEFAULT;
    buffDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    buffDesc.ByteWidth = sizeof(sun);
    srd.pSysMem = sun;
    if (FAILED(m_pd3d11Device->CreateBuffer(&buffDesc, &srd, &m_pSunData)))
        throw std::runtime_error("Error creating GPU buffer for object: sun");
    const UINT strides = sizeof(VertexData);
    const UINT offsets = 0;
    m_pd3d11DeviceContext->IASetVertexBuffers(0, 1, &m_pSunData, &strides, &offsets);

    // sun index data
    buffDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
    buffDesc.ByteWidth = sizeof(sunIndices);
    srd.pSysMem = sunIndices;
    if (FAILED(m_pd3d11Device->CreateBuffer(&buffDesc, &srd, &m_pSunIndicesData)))
        throw std::runtime_error("Error creating GPU buffer for object: sun indices");
    m_pd3d11DeviceContext->IASetIndexBuffer(m_pSunIndicesData, DXGI_FORMAT_R16_UINT, 0);

    // const buffer for passing transformation matrices
    buffDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    buffDesc.ByteWidth = sizeof(PerspectiveTransforms);
    if (FAILED(m_pd3d11Device->CreateBuffer(&buffDesc, nullptr, &m_pTXMatrix)))
        throw std::runtime_error("Error creating constant buffer for Tx matrix");
    m_pd3d11DeviceContext->VSSetConstantBuffers(0, 1, &m_pTXMatrix);

    // compile shaders
    ID3DBlob *vsBlob = compileShader(L"shader.fx", "vs_4_0", "vsMain");
    if (FAILED(m_pd3d11Device->CreateVertexShader(vsBlob->GetBufferPointer(),
                                                  vsBlob->GetBufferSize(),
                                                  nullptr,
                                                  &m_pVShader)))
        throw std::runtime_error("Failed creating vertex shader from shader.fx");
    ID3DBlob *psBlob = compileShader(L"shader.fx", "ps_4_0", "psMain");
    if (FAILED(m_pd3d11Device->CreatePixelShader(psBlob->GetBufferPointer(),
                                                 psBlob->GetBufferSize(),
                                                 nullptr,
                                                 &m_pPShader)))
        throw std::runtime_error("Failed creating pixel shader from shader.fx");
    releaseNclearPtr(&psBlob);

    // input layout setup
    D3D11_INPUT_ELEMENT_DESC inputLayout[] = {
                                                {
                                                    "POSITION",
                                                    0,
                                                    DXGI_FORMAT_R32G32B32_FLOAT,
                                                    0,
                                                    0,
                                                    D3D11_INPUT_PER_VERTEX_DATA,
                                                    0
                                                },
                                                {
                                                    "COLOUR",
                                                    0,
                                                    DXGI_FORMAT_R32G32B32A32_FLOAT,
                                                    0,
                                                    sizeof(DirectX::XMFLOAT3),
                                                    D3D11_INPUT_PER_VERTEX_DATA,
                                                    0
                                                }
                                             };
    ID3D11InputLayout *pInputLayout;
    if (FAILED(m_pd3d11Device->CreateInputLayout(inputLayout,
                                                 ARRAYSIZE(inputLayout),
                                                 vsBlob->GetBufferPointer(),
                                                 vsBlob->GetBufferSize(),
                                                 &pInputLayout)))
        throw std::runtime_error("Failed creating an input layout");
    m_pd3d11DeviceContext->IASetInputLayout(pInputLayout);

    releaseNclearPtr(&pInputLayout);
    releaseNclearPtr(&vsBlob);

    // setup camera and projection matrices
    auto camera = DirectX::XMVectorSet(3.0f, 4.0f, -5.0f, 0.0f);
    auto focus = DirectX::XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
    auto up = DirectX::XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
    m_viewMatrix = DirectX::XMMatrixLookAtLH(camera, focus, up);
    m_projectionMatrix = DirectX::XMMatrixPerspectiveFovLH(DirectX::XM_PIDIV2, m_width / m_height, 0.01f, 100.0f);

    // setup primitive topology for rendering
    m_pd3d11DeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

void D3D11Graphics::Render()
{
    const FLOAT clearColour[] = { 0.0f, 0.0f, 0.0f, 1.0f };
    m_pd3d11DeviceContext->ClearRenderTargetView(m_pRenderTargetView, clearColour);
    m_pd3d11DeviceContext->ClearDepthStencilView(m_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);

    // spin angle and set Sun's transformations

    if (!m_paused)
        m_angleY += 0.0025f;
    DirectX::XMMATRIX worldMatrix = DirectX::XMMatrixRotationY(m_angleY);
    PerspectiveTransforms Txs = { worldMatrix, m_viewMatrix, m_projectionMatrix };
    m_pd3d11DeviceContext->UpdateSubresource(m_pTXMatrix, 0, nullptr, &Txs, 0, 0);

    // render Sun
    m_pd3d11DeviceContext->VSSetShader(m_pVShader, nullptr, 0);
    m_pd3d11DeviceContext->PSSetShader(m_pPShader, nullptr, 0);
    m_pd3d11DeviceContext->DrawIndexed(ARRAYSIZE(sunIndices), 0, 0);

    // render Earth
    worldMatrix = DirectX::XMMatrixScaling(0.5f, 0.5f, 0.5f)
                * DirectX::XMMatrixRotationZ(-m_angleY * 2.0f)
                * DirectX::XMMatrixTranslation(5.0f, 0.0f, 0.0f)
                * DirectX::XMMatrixRotationY(-m_angleY / 2.0f);
                  
    Txs.world = worldMatrix;
    m_pd3d11DeviceContext->UpdateSubresource(m_pTXMatrix, 0, nullptr, &Txs, 0, 0);
    m_pd3d11DeviceContext->DrawIndexed(ARRAYSIZE(sunIndices), 0, 0);   

    m_pdxgiSwapChain->Present(0, 0);
}

void D3D11Graphics::Pause()
{
    m_paused = true;
}

void D3D11Graphics::Resume()
{
    m_paused = false;
}

bool D3D11Graphics::IsPaused() const
{
    return m_paused;
}

D3D11Graphics::~D3D11Graphics(void)
{
    releaseNclearPtr(&m_pPShader);
    releaseNclearPtr(&m_pVShader);
    releaseNclearPtr(&m_pTXMatrix);
    releaseNclearPtr(&m_pSunIndicesData);
    releaseNclearPtr(&m_pSunData);
    releaseNclearPtr(&m_pDepthStencilView);
    releaseNclearPtr(&m_pRenderTargetView);
    releaseNclearPtr(&m_pdxgiSwapChain);
    releaseNclearPtr(&m_pd3d11DeviceContext);
    releaseNclearPtr(&m_pd3d11Device);
}

ID3DBlob* D3D11Graphics::compileShader(const wchar_t *shaderFile,
                                       const char    *shaderTarget,
                                       const char    *entryPoint)
{
    std::ifstream strm(shaderFile, std::ios_base::binary|std::ios_base::ate);
    size_t fileLen = static_cast<size_t>(strm.tellg());
    std::unique_ptr<char[]> spFileData(new char [fileLen]);
    strm.seekg(0, std::ios_base::beg);
    strm.read(spFileData.get(), fileLen);

    UINT compileFlag = D3DCOMPILE_ENABLE_STRICTNESS;
#if defined(_DEBUG) || defined(DEBUG)
    compileFlag |= D3DCOMPILE_DEBUG;
#endif
    ID3DBlob *pBlob = nullptr, *pErr = nullptr;
    if (FAILED(D3DCompile(spFileData.get(),
                          fileLen,
                          nullptr,
                          nullptr,
                          nullptr,
                          entryPoint,
                          shaderTarget,
                          compileFlag,
                          0,
                          &pBlob,
                          &pErr)))
    {
        const char* errBegin = reinterpret_cast<const char*>(pErr->GetBufferPointer());
        std::string errStr(errBegin, errBegin + pErr->GetBufferSize());
        throw std::logic_error(errStr);
    }
    return pBlob;
}
