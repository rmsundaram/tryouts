#include "D3D11Graphics.h"

#include <stdexcept>
#include <cstdio>

HWND InitializeWindow(HINSTANCE hInst, unsigned int nWidth, unsigned int nHeight);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Render();

int CALLBACK WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpCmdLine,
                     int nCmdShow)
{
    const auto width = 800, height = 600;
    HWND hWnd = InitializeWindow(hInstance, width, height);
    D3D11Graphics graphSystem(hWnd, width, height);
    SetWindowLongPtr(hWnd, 0, reinterpret_cast<LONG_PTR>(&graphSystem));
    graphSystem.SetupData();

    DWORD lastTime = GetTickCount();
    bool playing = true;
    while (playing)
    {
        MSG msg;
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message != WM_QUIT)
                DispatchMessage(&msg);
            else
                playing = false;
        }
        else
        {
            graphSystem.Render();
        }

        // put FPS on title bar
        DWORD currentTime = GetTickCount();
        char fps[35] = "The Sun and The Earth - ";
        snprintf(&fps[24], ARRAYSIZE(fps) - 24, "%5lu fps", (1000 / (1 + (currentTime - lastTime))));
        SetWindowText(hWnd, fps);
        lastTime = currentTime;
    }
    return 0;
}

HWND InitializeWindow(HINSTANCE hInst, unsigned int nWidth, unsigned int nHeight)
{
    WNDCLASSEX wc = { sizeof(wc) };
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.hbrBackground = static_cast<HBRUSH>(GetStockObject(WHITE_BRUSH));
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
    wc.hInstance = hInst;
    wc.lpfnWndProc = WndProc;
    wc.lpszClassName = "SunEarth";
    wc.lpszMenuName = NULL;
    // http://stackoverflow.com/a/7386287/183120
    wc.cbWndExtra = sizeof(LONG_PTR);
    if (!RegisterClassEx(&wc))
        throw std::runtime_error("Failed to register class: SunEarth");

    RECT windowRect = {0, 0, static_cast<LONG>(nWidth), static_cast<LONG>(nHeight)};
    AdjustWindowRect(&windowRect, WS_OVERLAPPEDWINDOW, FALSE);

    HWND hWindow = CreateWindow(wc.lpszClassName,
                                nullptr,
                                WS_OVERLAPPEDWINDOW,
                                CW_USEDEFAULT,
                                CW_USEDEFAULT,
                                windowRect.right - windowRect.left,
                                windowRect.bottom - windowRect.top,
                                NULL,
                                NULL,
                                hInst,
                                NULL);
    if (!hWindow) throw std::runtime_error("Failed to create window");

    ShowWindow(hWindow, SW_SHOW);

    return hWindow;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
    /*
     * http://stackoverflow.com/questions/117792/best-method-for-storing-this-pointer-for-use-in-wndproc/
     * 118412#comment50072321_118412
     * “there are instances when Windows sends the application a message with a handle to a window that was not created
     * by the application” — in which case this assumption that hWnd is always the handle to the window on which
     * SetWindowLongPtr was called breaks down. Perhaps a check should be added before calling DispatchMessage.
     */

    D3D11Graphics *pGraphSystem = reinterpret_cast<D3D11Graphics*>(GetWindowLongPtr(hWnd, 0));

    switch(msg)
    {
    case WM_KEYUP:
        if (VK_SPACE == wparam)
            if (pGraphSystem->IsPaused())
                pGraphSystem->Resume();
            else
                pGraphSystem->Pause();
        else if (VK_ESCAPE == wparam)
    case WM_DESTROY:
            PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, msg, wparam, lparam);
    }
    return 0;
}
