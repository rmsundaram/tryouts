#pragma once

#define WIN32_LEAN_AND_MEAN

#include <D3D11.h>
#include <DirectXMath.h>

template <typename T>
void releaseNclearPtr(T **p)
{
    if (*p)
    {
        (*p)->Release();
        *p = nullptr;
    }
}

class D3D11Graphics
{
public:
    D3D11Graphics(HWND hwindow, unsigned int width, unsigned int height);
    ~D3D11Graphics(void);

    void SetupData();
    void Render();
    void Pause();
    void Resume();
    bool IsPaused() const;

private:
    ID3DBlob* compileShader(const wchar_t* shaderFile,
                            const char *shaderTarget,
                            const char *entryPoint);

    bool                    m_paused;
    DirectX::XMMATRIX       m_projectionMatrix;
    DirectX::XMMATRIX       m_viewMatrix;
    float                   m_angleY;
    const float             m_width, m_height;
    ID3D11PixelShader       *m_pPShader;
    ID3D11VertexShader      *m_pVShader;
    ID3D11Buffer            *m_pTXMatrix;
    ID3D11Buffer            *m_pSunIndicesData;
    ID3D11Buffer            *m_pSunData;
    ID3D11DepthStencilView  *m_pDepthStencilView;
    ID3D11RenderTargetView  *m_pRenderTargetView;
    IDXGISwapChain          *m_pdxgiSwapChain;
    ID3D11DeviceContext     *m_pd3d11DeviceContext;
    ID3D11Device            *m_pd3d11Device;
};
