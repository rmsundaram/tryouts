cbuffer PerspectiveTransforms
{
	matrix world;
	matrix view;
	matrix projection;
};

struct VS_INPUT
{
	vector<float, 4> pos : POSITION;
	float4 color : COLOUR;
};

struct PS_INPUT
{
	vector<float, 4> pos : SV_Position;
	float4 colour : COLOUR;
};

PS_INPUT vsMain(VS_INPUT inp)
{
	inp.pos = mul(projection, mul(view, mul(world, inp.pos)));
    return inp;
}

vector<float, 4> psMain(const PS_INPUT inp) : SV_Target
{
    return inp.colour;
}
