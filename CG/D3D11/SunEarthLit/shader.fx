cbuffer PerspectiveTransforms
{
	matrix world;
	matrix view;
	matrix projection;
};

cbuffer Light
{
	float4 direction;
	float4 colour;
};

struct VS_INPUT
{
	float4 pos : POSITION;
	float3 norm: NORMAL;
};

struct PS_INPUT
{
	float4 pos : SV_Position;
	float3 norm : NORMAL;
};

PS_INPUT vsMain(VS_INPUT inp)
{
	inp.pos = mul(projection, mul(view, mul(world, inp.pos)));
	inp.norm = mul(world, inp.norm);
    return inp;
}

vector<float, 4> psMain(const PS_INPUT inp) : SV_Target
{
    float4 finalColour = saturate(dot((float3)direction, inp.norm) * colour);
	finalColour.a = 1;
	return finalColour;
}
