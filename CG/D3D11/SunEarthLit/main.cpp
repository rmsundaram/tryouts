#include "D3D11Graphics.h"
#include <Windows.h>
#include <stdexcept>

HWND InitializeWindow(HINSTANCE hInst, unsigned int nWidth, unsigned int nHeight);
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void Render();

int CALLBACK WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR lpCmdLine,
                     int nCmdShow)
{
    const auto width = 800, height = 600;
    HWND hWnd = InitializeWindow(hInstance, width, height);
    D3D11Graphics graphSystem(hWnd, width, height);
    SetWindowLongPtr(hWnd, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(&graphSystem));
    graphSystem.SetupData();

    DWORD lastTime = GetTickCount();
    bool playing = true;
    while (playing)
    {
        MSG msg;
        if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
        {
            if (msg.message != WM_QUIT)
                DispatchMessage(&msg);
            else
                playing = false;
        }
        else
        {
            graphSystem.Render();
        }

        // put FPS on title bar
        DWORD currentTime = GetTickCount();
        char fps[45] = "The Sun and The Earth Alighted - ";
        snprintf(&fps[33], ARRAYSIZE(fps) - 33, "%4lu fps", (1000 / (1 + (currentTime - lastTime))));
        SetWindowText(hWnd, fps);
        lastTime = currentTime;
    }
    return 0;
}

HWND InitializeWindow(HINSTANCE hInst, unsigned int nWidth, unsigned int nHeight)
{
    WNDCLASS wc = { 0 };
    wc.style = CS_HREDRAW | CS_VREDRAW;
    wc.hbrBackground = static_cast<HBRUSH>(GetStockObject(WHITE_BRUSH));
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
    wc.hInstance = hInst;
    wc.lpfnWndProc = WndProc;
    wc.lpszClassName = "SunEarth";
    wc.lpszMenuName = NULL;
    if (!RegisterClass(&wc))
        throw std::runtime_error("Failed to register class: SunEarth");

    RECT windowRect = {0, 0, static_cast<LONG>(nWidth), static_cast<LONG>(nHeight)};
    AdjustWindowRect(&windowRect, WS_OVERLAPPEDWINDOW, FALSE);

    HWND hWindow = CreateWindow(wc.lpszClassName,
                                nullptr,
                                WS_OVERLAPPEDWINDOW,
                                CW_USEDEFAULT,
                                CW_USEDEFAULT,
                                windowRect.right - windowRect.left,
                                windowRect.bottom - windowRect.top,
                                NULL,
                                NULL,
                                hInst,
                                NULL);
    if (!hWindow) throw std::runtime_error("Failed to create window");

    ShowWindow(hWindow, SW_SHOW);

    return hWindow;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
    D3D11Graphics *pGraphSystem = reinterpret_cast<D3D11Graphics*>(GetWindowLongPtr(hWnd, GWLP_USERDATA));

    switch(msg)
    {
    case WM_KEYUP:
        if (VK_SPACE == wparam)
            if (pGraphSystem->IsPaused())
                pGraphSystem->Resume();
            else
                pGraphSystem->Pause();
        else if (VK_ESCAPE == wparam)
    case WM_DESTROY:
            PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, msg, wparam, lparam);
    }
    return 0;
}
