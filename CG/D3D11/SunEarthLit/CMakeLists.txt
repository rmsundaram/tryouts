cmake_minimum_required(VERSION 3.16)

project(
  SunEarthLit
  VERSION 0.1.0
  DESCRIPTION "Simple D3D11 test program with lighting"
  HOMEPAGE_URL "https://bitbucket.org/rmsundaram/tryouts"
  )

# Set a default build type if none was specified
if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to 'Debug' as none was specified.")
  set(CMAKE_BUILD_TYPE Debug CACHE STRING "Choose the type of build." FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release"
    "MinSizeRel" "RelWithDebInfo")
endif()

# Prohibit Windows from providing a console by passing `-mwindows` (MinGW) or
# `/SUBSYSTEM:WINDOWS` (MSVC).  MSVC’s CRT expects WinMain, not main, as entry
# point.  Can override explicitly with linker flag /ENTRY (MSVC) or --entry
# (MinGW).
add_executable(${PROJECT_NAME} WIN32
  main.cpp
  D3D11Graphics.cpp)

target_link_libraries(${PROJECT_NAME} PRIVATE
  d3d11 d3dcompiler)

# Except MSVC other compilers lack DirectXMath.h with XMMATRIX type
if (NOT CMAKE_CXX_COMPILER_ID STREQUAL "MSVC")
  include(FetchContent)
  FetchContent_Declare(
    DirectXMath
    GIT_REPOSITORY https://github.com/Microsoft/DirectXMath
    GIT_TAG        main
    GIT_SHALLOW    TRUE)
  FetchContent_MakeAvailable(DirectXMath)
  # disable XMVECTOR operator overloads; unsupported by GCC and Clang
  target_compile_definitions(DirectXMath INTERFACE
    _XM_NO_XMVECTOR_OVERLOADS_)

  FetchContent_Declare(
    NoSAL
    GIT_REPOSITORY https://github.com/legends2k/NoSAL
    GIT_TAG        main
    GIT_SHALLOW    TRUE)
  FetchContent_MakeAvailable(NoSAL)
  cmake_policy(PUSH)
  # Allow calling target_link_libraries on targets not authored here
  cmake_policy(SET CMP0079 NEW)
  target_link_libraries(DirectXMath INTERFACE NoSAL::NoSAL)
  cmake_policy(POP)

  target_link_libraries(${PROJECT_NAME} PRIVATE DirectXMath)
endif ()

# Confirm strictly to C++14.
# https://cliutils.gitlab.io/modern-cmake/chapters/features/cpp11.html
target_compile_features(${PROJECT_NAME} PUBLIC cxx_std_17)
set_target_properties(${PROJECT_NAME} PROPERTIES
  CXX_EXTENSIONS OFF
  CXX_STANDARD_REQUIRED ON
  # place binary at project root as shader.fx is needed in same dir at runtime
  RUNTIME_OUTPUT_DIRECTORY "${CMAKE_SOURCE_DIR}$<0:>"
  # this is needed when debugging from VS
  # https://stackoverflow.com/q/55713475/183120
  VS_DEBUGGER_WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}")
