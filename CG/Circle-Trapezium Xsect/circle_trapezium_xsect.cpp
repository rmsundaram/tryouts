#include <glm/glm.hpp>
#include <iostream>
#include <array>

struct Circle
{
    glm::vec2    centre;
    float        radius;
};

// Line normal-offset form
// normalized normal = <a, b>; c = negated signed distance
// from the origin along the normal (3D Math Primer)
typedef glm::vec3 Line;

// top-down or sideways projection of a frustum
//  \-------/
//   \     /
//    \---/
struct Trapezium
{
    // left, top, right, bottom
    std::array<Line, 4> edges;
};

float point_line_dist(glm::vec3 const &p, Line const &l)
{
    return glm::dot(p, l);
}

enum class X_state : uint8_t
{
    Out,
    Intersect,
    In
};

X_state circle_line_intersect(Circle const &c, Line const &l)
{
    float dist = point_line_dist(glm::vec3(c.centre, 1.0f), l);
    if (std::abs(dist) < c.radius)
        return X_state::Intersect;
    else if (dist < 0.0f)
        return X_state::Out;
    return X_state::In;
}

X_state circle_trapezium_intersect1(Circle const &c, Trapezium const &t)
{
    bool intersecting = false;
    for (auto const& l : t.edges)
    {
        auto const res = circle_line_intersect(c, l);
        // early return if out of any line
        if (res == X_state::Out)
            return res;
        else if (res == X_state::Intersect)
            intersecting = true;
    }
    return (intersecting ? X_state::Intersect : X_state::In);
}

// expand the wall of the trapezium by circle's radius: convolution
// test the containement of circle centre within the convoluted trapezium; refer
// §8.2.1 Skeleton book and §12.4.5 Essential Math - although the latter says
// this method would filter the edge case where a circle intersects two edges
// but not the trapezium, this isn't correct. Any circle passed by method 1 is
// passed by method 2 as well, both are the same as the Skeleton book explains.
// See corner_case.svg for an illustration.
bool circle_trapezium_intersect2(Circle const &c, Trapezium const &t)
{
    for (auto l : t.edges)
    {
        l.z += c.radius;
        auto const d = point_line_dist(glm::vec3(c.centre, 1.0f), l);
        if (d <= 0.0f)          // point behind line
            return false;
    }
    // point in front all lines but we aren't sure if this is intersection
    // or containment; finding that requires testing if distance is within
    // twice the radius - this isn't really needed in most cases
    return true;
}

std::ostream& operator<< (std::ostream &o, X_state x)
{
    char const * const str[] = { "Out", "Intersecting", "In" };
    return o << str[static_cast<size_t>(x)];
}

int main()
{
    Trapezium const t {{{
                           // left - line passing through (2, 0) and (0, 2)
                           { 0.707106781188f, 0.707106781188f, -1.41421356238f },
                           // top - line passing through (0, 2) and (5, 2)
                           { 0.0f, -1.0f, 2.0f },
                           // right - line passing through (4, 0) and (5, 2)
                           { -0.894427191f, 0.4472135955f, 3.577708764f },
                           // bottom - line passing through (2, 0) and (4, 0)
                           { 0.0f, 1.0f, 0.0f }
                      }}};
    Circle const c1 { { 3.0f, 2.0f }, 1.0f };
    Circle const c2 { { 2.0f, 1.0f }, 0.5f };
    Circle const c3 { { 0.0f, 0.0f }, 1.41421356238f };
    Circle const c4 { { 8.0f, 2.0f }, 1.0f };
    Circle const c5 { { -2.0f, 2.75f }, 1.0f };

    std::cout << "METHOD 1\n";
    std::cout << circle_trapezium_intersect1(c1, t) << '\n';
    std::cout << circle_trapezium_intersect1(c2, t) << '\n';
    std::cout << circle_trapezium_intersect1(c3, t) << '\n';
    std::cout << circle_trapezium_intersect1(c4, t) << '\n';
    std::cout << circle_trapezium_intersect1(c5, t) << '\n';

    std::cout << "\nMETHOD 2\n" << std::boolalpha;
    std::cout << circle_trapezium_intersect2(c1, t) << '\n';
    std::cout << circle_trapezium_intersect2(c2, t) << '\n';
    std::cout << circle_trapezium_intersect2(c3, t) << '\n';
    std::cout << circle_trapezium_intersect2(c4, t) << '\n';
    std::cout << circle_trapezium_intersect2(c5, t) << '\n';
}

