// g++ -Wall -std=c++11 -pedantic -ID:/Libs/glm frustum_planes.cpp

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtc/epsilon.hpp>
#include <cmath>
#include <iostream>
#include <iomanip>
#include <limits>
#include <array>

std::ostream& operator<<(std::ostream &os, const glm::vec4 &v)
{
    return os << v.x << ", " << v.y << ", " << v.z << ", " << v.w;
}

std::ostream& operator<<(std::ostream &os, const glm::mat4 &M)
{
    return os << M[0][0] << '\t' << M[1][0] << '\t' << M[2][0] << '\t' << M[3][0] << '\n'
              << M[0][1] << '\t' << M[1][1] << '\t' << M[2][1] << '\t' << M[3][1] << '\n'
              << M[0][2] << '\t' << M[1][2] << '\t' << M[2][2] << '\t' << M[3][2] << '\n'
              << M[0][3] << '\t' << M[1][3] << '\t' << M[2][3] << '\t' << M[3][3] << '\n';
}

// §5.2 in the skeleton book shows that in plane equation P = <A, B, C, D>, the D component is the
// signed distance from the origin only if N = <A, B, C> is normalized; otherwise the signed distance
// is given by D/‖N‖. See Gribb, Hartmann paper on frustum plane extraction for details.
glm::vec4 normalize_plane(const glm::vec4 &v)
{
    const auto inv_len = 1.0f / std::sqrt((v.x * v.x) + (v.y * v.y) + (v.z * v.z));
    // this normalizes the normal N and fixes the signed distance D from the origin too
    return inv_len * v;
}

// based on Fast Extraction of Viewing Frustum Planes from the WorldView-Projection Matrix
// by Gil Gribb and Klaus Hartmann http://www8.cs.umu.se/kurser/5DV051/HT12/lab/plane_extraction.pdf
// if M = P, the planes returned would be in view space
// if M = P * V is passed, they would be in world space
// if M = P * V * M, they would be in model space
std::array<glm::vec4, 6> extract_planes(const glm::mat4 &M)
{
    // although §5.5.3 of skeleton book says that we could use the rows of M instead of transposing M,
    // doing so with GLM might mean creation of 12 intermediate vectors - since GLM stores vectors by
    // column arrays. Hence transpose M and use its columns.
    const auto Mt = glm::transpose(M);

    std::array<glm::vec4, 6> planes;
    // left
    planes[0] = normalize_plane(glm::column(Mt, 3) + glm::column(Mt, 0));
    // right
    planes[1] = normalize_plane(glm::column(Mt, 3) - glm::column(Mt, 0));
    // bottom
    planes[2] = normalize_plane(glm::column(Mt, 3) + glm::column(Mt, 1));
    // top
    planes[3] = normalize_plane(glm::column(Mt, 3) - glm::column(Mt, 1));
    //near
    planes[4] = normalize_plane(glm::column(Mt, 3) + glm::column(Mt, 2));
    // far
    planes[5] = normalize_plane(glm::column(Mt, 3) - glm::column(Mt, 2));

    return planes;
}

int main()
{
    constexpr auto width = 800.0f;
    constexpr auto height = 600.0f;
    constexpr auto a = width / height;
    constexpr auto FoV = 0.78539816339f;    // quarter pi or 45 degrees
    constexpr auto near = 1.0f;
    constexpr auto far = 100.0f;
    constexpr auto d = 1.0f / std::tan(FoV / 2.0f);
    constexpr auto sqrt_d2_a2 = std::sqrt((d * d) + (a * a));
    constexpr auto sqrt_d2_1 = std::sqrt((d * d) + 1.0f);

    // based on §5.3.2 and §5.5.3 of Mathematics for 3D Game Programming and Computer Graphics; although 1 and a here
    // are interchanged between floor/ceil and left/right planes w.r.t the book since it assumes the viewing plane to
    // be at the point where the left and right planes (horizontal FoV) and the line x = 1 meet, while Essential Math
    // assumes it to be the point where bottom and top planes (vertical FoV) and the line y = 1 intersect; hence in
    // the former right/left extremes are ±1 and in the latter top/bottom extremes are ±1, both books assume the other
    // extremes to be ±a, the aspect ratio
    glm::vec4 left_plane(d / sqrt_d2_a2, 0.0f, -a / sqrt_d2_a2, 0.0f);
    glm::vec4 right_plane(-d / sqrt_d2_a2, 0.0f, -a / sqrt_d2_a2, 0.0f);
    glm::vec4 bottom_plane(0.0f, d / sqrt_d2_1, -1.0f / sqrt_d2_1, 0.0f);
    glm::vec4 top_plane(0.0f, -d / sqrt_d2_1, -1.0f / sqrt_d2_1, 0.0f);
    glm::vec4 near_plane(0.0f, 0.0f, -1.0f, -near);
    glm::vec4 far_plane(0.0f, 0.0f, 1.0f, far);

    // Nate Robins OpenGL Tutors interactively demonstrates how glFrustum, glOrtho and gluPerspective params affect
    // the rendered scene; it also has other (fixed-function) demos on textures, transforms, lights, etc.
    // https://user.xmission.com/~nate/tutors.html
    const auto P = glm::perspective(FoV, a, near, far);
    auto planes = extract_planes(P);

    std::cout << "Perspective projection matrix:\n" << P;
    std::cout << "\nPlanes in view space\nExtracted from matrix:\n";
    std::cout << "Left = <" << planes[0] << ">\n";
    std::cout << "Right = <" << planes[1] << ">\n";
    std::cout << "Bottom = <" << planes[2] << ">\n";
    std::cout << "Top = <" << planes[3] << ">\n";
    std::cout << "Near = <" << planes[4] << ">\n";
    std::cout << "Far = <" << planes[5] << ">\n";

    std::cout << "\nPrecomputed:\n";
    std::cout << "Left = <" << left_plane << ">\n";
    std::cout << "Right = <" << right_plane << ">\n";
    std::cout << "Bottom = <" << bottom_plane << ">\n";
    std::cout << "Top = <" << top_plane << ">\n";
    std::cout << "Near = <" << near_plane << ">\n";
    std::cout << "Far = <" << far_plane << ">\n";

    // std::numeric_limits<float>::epsilon() evaluates the last two planes as unequal, so does 0.0000001f
    constexpr auto eps = 0.001f;
    std::cout << std::boolalpha << "Planes equal: ";
    std::cout << glm::all(glm::epsilonEqual(left_plane, planes[0], eps)) << ", ";
    std::cout << glm::all(glm::epsilonEqual(right_plane, planes[1], eps)) << ", ";
    std::cout << glm::all(glm::epsilonEqual(bottom_plane, planes[2], eps)) << ", ";
    std::cout << glm::all(glm::epsilonEqual(top_plane, planes[3], eps)) << ", ";
    std::cout << glm::all(glm::epsilonEqual(near_plane, planes[4], eps)) << ", ";
    std::cout << glm::all(glm::epsilonEqual(far_plane, planes[5], eps));

    // although the frustum planes remain the same in camera space as the camera pans/rotates these planes in
    // world space would keep changing
    const auto V = glm::lookAt(glm::vec3{0.0f, 0.0f, 5.0f},
                               glm::vec3{1.0f, 1.0f, 1.0f},
                               glm::vec3{0.0f, 1.0f, 0.0f});
    const auto M = P * V;
    // extracted planes in world space
    planes = extract_planes(M);

    // V is Mworld->view, we need the planes in camera space to be mapped to world space, thus we need Mview->world,
    // thus we need glm::affineInverse(V); however to transform a plane by M we need it's inverse transpose (see §5.2.3
    // in skeleton book), thus glm::transpose(glm::affineInverse(glm::affineInverse(V)))
    const auto Mw2vT = glm::transpose(V);
    left_plane = Mw2vT * left_plane;
    right_plane = Mw2vT * right_plane;
    bottom_plane = Mw2vT * bottom_plane;
    top_plane = Mw2vT * top_plane;
    near_plane = Mw2vT * near_plane;
    far_plane = Mw2vT * far_plane;

    std::cout << "\n\nProjection-View matrix:\n" << M;
    std::cout << "\nPlanes in world space\nExtracted from matrix:\n";
    std::cout << "Left = <" << planes[0] << ">\n";
    std::cout << "Right = <" << planes[1] << ">\n";
    std::cout << "Bottom = <" << planes[2] << ">\n";
    std::cout << "Top = <" << planes[3] << ">\n";
    std::cout << "Near = <" << planes[4] << ">\n";
    std::cout << "Far = <" << planes[5] << ">\n";

    std::cout << "\nPrecomputed:\n";
    std::cout << "Left = <" << left_plane << ">\n";
    std::cout << "Right = <" << right_plane << ">\n";
    std::cout << "Bottom = <" << bottom_plane << ">\n";
    std::cout << "Top = <" << top_plane << ">\n";
    std::cout << "Near = <" << near_plane << ">\n";
    std::cout << "Far = <" << far_plane << ">\n";

    std::cout << std::boolalpha << "Planes equal: ";
    std::cout << glm::all(glm::epsilonEqual(left_plane, planes[0], eps)) << ", ";
    std::cout << glm::all(glm::epsilonEqual(right_plane, planes[1], eps)) << ", ";
    std::cout << glm::all(glm::epsilonEqual(bottom_plane, planes[2], eps)) << ", ";
    std::cout << glm::all(glm::epsilonEqual(top_plane, planes[3], eps)) << ", ";
    std::cout << glm::all(glm::epsilonEqual(near_plane, planes[4], eps)) << ", ";
    std::cout << glm::all(glm::epsilonEqual(far_plane, planes[5], eps));
}
