Workout to experiment with Box2D.

# Requirements

C++ compiler supporting C++11 and above; tested on GCC and MSVC.

| Library | Version |
|---------|---------|
| Box2D   | 2.x     |
| GLFW    | 3.x     |
| OpenGL  | 2.1     |

Only fixed-function APIs of OpenGL are used; GLU is used for `gluOrtho2D`.

# Build

Use CMake and a decent C++11-compatible compiler to build on Linux, macOS or Windows:

``` shell
cmake -B build -DCMAKE_BUILD_TYPE=Release
cmake --build build
```
