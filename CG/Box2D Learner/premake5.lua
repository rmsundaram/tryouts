-- Deprecated; use CMakeLists.txt

local action = _ACTION or ""

workspace "Box2D Learner"
        location ("build/" .. action)
        configurations { "debug", "release" }
        platforms { "windows", "linux" }
        objdir ("./obj")

        -- library paths
        local box2d_base = "D:/Libs/Box2D-2.3.1/Box2D"
        local box2d_inc = box2d_base
        local glfw_base = "D:/Libs/glfw-3.2.1.bin.WIN32"
        local box2d_lib = box2d_base .. '/Build/gmake2/bin/Debug'
        local glfw_inc = glfw_base .. '/include'
        local glfw_lib = glfw_base .. '/lib-mingw'
        local tux_inc = "/usr/libs/include"
        local tux_lib = "/usr/libs/lib"

        project "stage"
                kind "ConsoleApp"
                language "C++"
                files { "src/stage/*.cpp" }
                        buildoptions { "-Wall", "-pedantic" }
                        cppdialect "C++11"
                        links { "box2d" }
                        targetname "stage"
                        vpaths { ["inc"] = { "**.h", "**.hxx", "**.hpp" }, ["src"] = { "**.c", "**.cxx", "**.cpp" } }

                filter { "platforms:windows" }
                        system "windows"
                        includedirs { box2d_inc, glfw_inc }
                        libdirs { box2d_lib, glfw_lib }
                        links { "glfw3", "opengl32", "glu32", "gdi32", "winmm", "user32" }

                filter { "platforms:linux" }
                        system "linux"
                        includedirs { tux_inc }
                        libdirs { tux_lib }
                        links { "glfw", "GL", "GLU", "pthread", "X11", "Xrandr" }

                filter { "configurations:debug" }
                        defines { "_DEBUG" }
                        symbols "On"
                        targetdir ( "build/" .. action .. "/bin/Debug" )

                filter { "configurations:release" }
                        defines { "NDEBUG" }
                        optimize "On"
                        targetdir ( "build/" .. action .. "/bin/Release" )

	        project "demo"
                kind "ConsoleApp"
                language "C++"
                files { "src/demo/*.cpp" }
                        buildoptions { "-Wall", "-pedantic" }
                        cppdialect "C++11"
                        links { "box2d" }
                        targetname "demo"
                        vpaths { ["inc"] = { "**.h", "**.hxx", "**.hpp" }, ["src"] = { "**.c", "**.cxx", "**.cpp" } }

                filter { "platforms:windows" }
                        system "windows"
                        includedirs { box2d_inc, glfw_inc }
                        libdirs { box2d_lib, glfw_lib }
                        links { "glfw3", "opengl32", "glu32", "gdi32", "winmm", "user32" }

                filter { "platforms:linux" }
                        system "linux"
                        includedirs { tux_inc }
                        libdirs { tux_lib }
                        links { "glfw", "GL", "GLU", "pthread", "X11", "Xrandr" }

                filter { "configurations:debug" }
                        defines { "_DEBUG" }
                        symbols "On"
                        targetdir ( "build/" .. action .. "/bin/Debug" )

                filter { "configurations:release" }
                        defines { "NDEBUG" }
                        optimize "On"
                        targetdir ( "build/" .. action .. "/bin/Release" )
