#ifndef __SHAPE_H__
#define __SHAPE_H__

namespace Box2dLearner
{
    struct ShapeVisitor;

    struct Shape
    {
        virtual void setCentre(const Point &newCentre);
        virtual const Point& getCentre() const;
        virtual void setHollow(bool hollow);
        virtual bool isHollow() const;
        void setColour(const Colour &c);
        Colour getColour() const;
        void setBody(BodyUPtr& body);
        b2Body& getBody() const;
		float getAngle() const;
		void setAngle(float angle);
		virtual void move(const Point &displacement);
        virtual void accept(ShapeVisitor &shapeVisitor) = 0;
        virtual ~Shape() { }

    protected:
        Shape(const Point &centre = Point());

		BodyUPtr _body;
		Colour _colour;
        Point _centre;
		float _angle;
        bool _hollow;
    };
    typedef std::unique_ptr<Shape> ShapeUPtr;

    struct Circle : public Shape
    {
        Circle(const Point &centre, int radius);

        void accept(ShapeVisitor &visitor);
        unsigned getRadius() const;
        void setRadius(unsigned newRadius);

    private:
        unsigned _radius;
    };

    struct Triangle : public Shape
    {
        Triangle(std::vector<Point> &points);

        void accept(ShapeVisitor &visitor);
        const std::vector<Point>& getVertices() const;

    private:
        std::vector<Point> _points;
    };

    struct Polygon : public Shape
    {
        Polygon(std::vector<Point> &points);

		void move(const Point& displacement);
        void accept(ShapeVisitor &visitor);
        const std::vector<Point>& getVertices() const;

    private:
        std::vector<Point> _points;
    };

    struct ShapeVisitor
    {
        virtual void visit(Circle&) = 0;
        virtual void visit(Triangle&) = 0;
        virtual void visit(Polygon&) = 0;
    };

    struct ShapePainter : public ShapeVisitor
    {
        void visit(Circle&);
        void visit(Triangle&);
        void visit(Polygon&);
    };
}

#endif