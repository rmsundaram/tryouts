#ifndef __SECOND_H__
#define __SECOND_H__

const unsigned SCREEN_WIDTH = 960;
const unsigned SCREEN_HEIGHT = 540;

const double DEG_TO_RAD = b2_pi / 180.f;
const double RAD_TO_DEG = 1 / DEG_TO_RAD;

namespace Box2dLearner
{
    struct Point
    {
        Point(int X = 0, int Y = 0) : iX(X), iY(Y)
        {
        }

        int iX, iY;
    };

    struct Colour
    {
        Colour(unsigned char r = 0xff,
               unsigned char g = 0xff,
               unsigned char b = 0xff,
               unsigned char a = 0xff) : red(r), green(g), blue(b), alpha(a)
        {
        }

        unsigned char red, green, blue, alpha;
    };

    typedef std::unique_ptr<b2Body, std::function<void (b2Body*)>> BodyUPtr;

    const float meter_to_pixel_ratio = 0.02f;

    inline float pixels_to_meters(unsigned pixels)
    {
        return meter_to_pixel_ratio * pixels;
    }

    inline unsigned meters_to_pixels(float meters)
    {
        return static_cast<unsigned>(meters / meter_to_pixel_ratio);
    }
}

#endif
