#include "box2d/box2d.h"
#include <memory>
#include <vector>
#include <functional>
#include "Main.h"
#include "Shape.h"
#ifdef _WIN32
#include <Windows.h>
#endif
#include <GL/gl.h>
#include <cmath>
#include <algorithm>

namespace Box2dLearner
{
    Shape::Shape(const Point &centre) : _centre(centre), _angle(0.f), _hollow(false)
    {
    }

    const Point& Shape::getCentre() const
    {
        return _centre;
    }

    void Shape::setHollow(bool hollow)
    {
        _hollow = hollow;
    }

    bool Shape::isHollow() const
    {
        return _hollow;
    }

    void Shape::setCentre(const Point &newCentre)
    {
        _centre = newCentre;
    }

    void Shape::setColour(const Colour &c)
    {
        _colour = c;
    }

    Colour Shape::getColour() const
    {
        return _colour;
    }
    
    void Shape::setBody(BodyUPtr& body)
    {
        _body = std::move(body);
    }
    
    b2Body& Shape::getBody() const
    {
        return *_body.get();
    }
	
	float Shape::getAngle() const
	{
		return _angle;
	}

	void Shape::setAngle(float angle)
	{
		_angle = fmod(angle, 360.f);
	}

	void Shape::move(const Point &displacement)
	{
		_centre.iX += displacement.iX;
		_centre.iY += displacement.iY;
	}

    Circle::Circle(const Point &centre, int radius) :
    Shape(centre), _radius(radius)
    {
    }

    unsigned Circle::getRadius() const
    {
        return _radius;
    }

    void Circle::setRadius(unsigned newRadius)
    {
        _radius = newRadius;
    }

    void Circle::accept(ShapeVisitor &visitor)
    {
        visitor.visit(*this);
    }

    Triangle::Triangle(std::vector<Point> &points) :
    _points(std::move(points))
    {
    }

    const std::vector<Point>& Triangle::getVertices() const
    {
        return _points;
    }

    void Triangle::accept(ShapeVisitor &visitor)
    {
        visitor.visit(*this);
    }

    Polygon::Polygon(std::vector<Point> &points) :
    _points(std::move(points))
    {
		_centre.iX = _points[0].iX + ((_points[2].iX - _points[0].iX) / 2);
		_centre.iY = _points[0].iY + ((_points[2].iY - _points[0].iY) / 2);
    }

    const std::vector<Point>& Polygon::getVertices() const
    {
        return _points;
    }

	void Polygon::move(const Point& displacement)
	{
		std::for_each (_points.begin(), _points.end(), [&displacement](Point &_pt)
		{
			_pt.iX += displacement.iX;
			_pt.iY += displacement.iY;
		});
	}

    void Polygon::accept(ShapeVisitor &visitor)
    {
        visitor.visit(*this);
    }

    void ShapePainter::visit(Circle &circle)
    {
        glPushMatrix();
        const Point& circleCentre = circle.getCentre();
        glTranslatef(static_cast<float>(circleCentre.iX),
                     static_cast<float>(circleCentre.iY),
                     0.f);

        const Colour &colour = circle.getColour();
        glColor4ub(colour.red, colour.green, colour.blue, colour.alpha);

        if (circle.isHollow())
        {
            glBegin(GL_LINE_STRIP);
        }
        else
        {
            glBegin(GL_TRIANGLE_FAN);
            glVertex2i(0, 0);
        }

        const unsigned radius = circle.getRadius();
        glVertex2i(0, radius);

        for(unsigned short angle = 5; angle <= 360; angle += 5)
        {
            const double cosO = std::cos(angle * DEG_TO_RAD);
            const double sinO = std::sin(angle * DEG_TO_RAD);

            glVertex2d(radius * sinO, radius * cosO);
        }
        
        glEnd();
        glPopMatrix();
    }

    void ShapePainter::visit(Triangle &triangle)
    {
        const Colour &colour = triangle.getColour();
        glColor4ub(colour.red, colour.green, colour.blue, colour.alpha);

        const std::vector<Point> &vertices = triangle.getVertices();
        glBegin(triangle.isHollow() ? GL_LINE_LOOP : GL_TRIANGLES);
        std::for_each(vertices.cbegin(), vertices.cend(), [](const Point &pt)
        {
            glVertex2i(pt.iX, pt.iY);
        });
        glEnd();
    }

    void ShapePainter::visit(Polygon &polygon)
    {
        const Colour &colour = polygon.getColour();
        glColor4ub(colour.red, colour.green, colour.blue, colour.alpha);

		auto centre = polygon.getCentre();
		glPushMatrix();
		glTranslatef(static_cast<GLfloat>(centre.iX),
					 static_cast<GLfloat>(centre.iY),
					 0.f);
		glRotatef(polygon.getAngle(), 0.f, 0.f, 1.f);
		glTranslatef(static_cast<GLfloat>(-centre.iX),
					 static_cast<GLfloat>(-centre.iY),
					 0.f);

        const std::vector<Point> &vertices = polygon.getVertices();
        glBegin(polygon.isHollow() ? GL_LINE_LOOP : GL_POLYGON);
        std::for_each(vertices.cbegin(), vertices.cend(), [](const Point &pt)
        {
            glVertex2i(pt.iX, pt.iY);
        });
        glEnd();
		
		glPopMatrix();
    }
}
