#ifndef __DEBUG_DRAW_H__
#define __DEBUG_DRAW_H__

class DebugDraw : public b2Draw
{
public:
    DebugDraw() { m_drawFlags = e_shapeBit; }

    // b2Draw impls
    void DrawPoint (const b2Vec2 &p, float size, const b2Color &color);
	void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& colour);
	void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& colour);
	void DrawCircle(const b2Vec2& center, float radius, const b2Color& colour);
	void DrawSolidCircle(const b2Vec2& center, float radius, const b2Vec2& axis, const b2Color& colour);
	void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& colour);
	void DrawTransform(const b2Transform&)
    {
    }

private:
    void _drawPolygon(const b2Vec2* &vertices, int32 vertexCount, const b2Color& colour, GLenum mode);
    void _drawCircle(const b2Vec2& center, float radius, const b2Vec2 *axis, const b2Color& colour);
};

#endif
