#include <vector>
#include <memory>
#include <GLFW/glfw3.h>
// CALLBACK macro definition on Windows needed for glu.h
#ifdef _WIN32
#  include <minwindef.h>
#endif
#include <GL/glu.h>
#include <cstdio>
#include <ctime>
#include <functional>
#include "box2d/box2d.h"

#include "Main.h"
#include "Shape.h"
#ifdef _DEBUG
#include "DebugDraw.h"
#endif

using std::vector;

using Box2dLearner::Shape;
using Box2dLearner::Point;
using Box2dLearner::Colour;
using Box2dLearner::Circle;
using Box2dLearner::Polygon;
using Box2dLearner::Triangle;
using Box2dLearner::BodyUPtr;
using Box2dLearner::ShapeUPtr;
using Box2dLearner::ShapePainter;
using Box2dLearner::pixels_to_meters;
using Box2dLearner::meters_to_pixels;

#ifdef _DEBUG
bool debugMode = false;
#endif

template <typename _Iter>
void paint(_Iter firstShape,
           _Iter lastShape,
           ShapePainter &painter
#ifdef _DEBUG
          ,b2World &b2dWorld
#endif
          )
{
	glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    glColor4ub(0xff, 0xff, 0xff, 0xff);
#ifdef _DEBUG
if (!debugMode)
{
#endif
	for(; firstShape != lastShape; ++firstShape)
	{
        (*firstShape)->accept(painter);
	}
#ifdef _DEBUG
}
else
{
    b2dWorld.DebugDraw();

    // draw grid
    glColor4ub(0xff, 0xff, 0xff, 75);
    glBegin(GL_LINES);
    const unsigned short gridUnit = 50;
    for (unsigned short i = 0; i < SCREEN_WIDTH; i += gridUnit)
    {
        glVertex2i(i, 0);
        glVertex2i(i, SCREEN_HEIGHT);
    }
    for (unsigned short i = 0; i < SCREEN_HEIGHT; i += gridUnit)
    {
        glVertex2i(0, i);
        glVertex2i(SCREEN_WIDTH, i);
    }
    glEnd();
}
#endif

	GLenum glError = glGetError();
	if (GL_NO_ERROR != glError)
	{
		printf("GLError: %i", static_cast<int>(glError));
	}
}

void updateStates(vector<ShapeUPtr> &rgModels, GLFWwindow* win)
{
    const ShapeUPtr& ball = rgModels[1];
          b2Body &ballBody = ball->getBody();
    const b2Vec2 &ballPos = ballBody.GetPosition();
    ball->setCentre(Point(meters_to_pixels(ballPos.x),
                        meters_to_pixels(ballPos.y)));

	const ShapeUPtr &box = rgModels[2];
	auto &boxBody = box->getBody();
	const b2Vec2 &boxPos = boxBody.GetPosition();
	const Point& oldCentre = box->getCentre();
	box->move(Point(meters_to_pixels(boxPos.x) - oldCentre.iX, meters_to_pixels(boxPos.y) - oldCentre.iY));
	box->setCentre(Point(meters_to_pixels(boxPos.x), meters_to_pixels(boxPos.y)));
	box->setAngle(static_cast<float>(boxBody.GetAngle() * RAD_TO_DEG));

    b2Vec2 linearImpulse(glfwGetKey(win, GLFW_KEY_RIGHT) ? 0.75f : (glfwGetKey(win, GLFW_KEY_LEFT) ? -0.75f : 0.f),
                         glfwGetKey(win, GLFW_KEY_UP) ? 0.75f : (glfwGetKey(win, GLFW_KEY_DOWN) ? -0.75f : 0.f));
    if (linearImpulse.x || linearImpulse.y)
    {
        ballBody.ApplyLinearImpulse(linearImpulse, b2Vec2_zero, true);
    }

    auto &seesaw = rgModels[3];
    auto &seesawBody = seesaw->getBody();
    seesaw->setAngle(static_cast<float>(seesawBody.GetAngle() * RAD_TO_DEG));
}

#ifdef _DEBUG
void keyHandler(GLFWwindow*, int key, int scancode, int action, int mods)
{
	if (('R' == key) && (GLFW_RELEASE == action))
	{
		debugMode = false;
	}
	else if (('D' == key) && (GLFW_RELEASE == action))
	{
		debugMode = true;
	}
}
#endif

void setupScene(vector<ShapeUPtr>& rgModels, b2World &b2dWorld)
{
    std::function<void (b2Body*)> b2BodyDestroyer = [](b2Body *body)
	{
        body->GetWorld()->DestroyBody(body);
	};

    rgModels.reserve(5);

    // setup ground
    const unsigned groundWidth = SCREEN_WIDTH / 2;
    const unsigned groundHeight = SCREEN_HEIGHT / 5;
    const Point groundStartPoint(SCREEN_WIDTH / 4, 0);
    vector<Point> vertexData;
    vertexData.reserve(7);
    vertexData.push_back(groundStartPoint);
    vertexData.push_back(Point(groundStartPoint.iX + groundWidth, groundStartPoint.iY));
    vertexData.push_back(Point(groundStartPoint.iX + groundWidth, groundStartPoint.iY + groundHeight));
    vertexData.push_back(Point(groundStartPoint.iX, groundStartPoint.iY + groundHeight));
    Polygon *ground = new Polygon(vertexData);
    ground->setColour(Colour(0x89, 0xCF, 0xF0));

    // setup ground physics
    b2BodyDef groundDef;
    groundDef.position.Set(pixels_to_meters(groundStartPoint.iX + (groundWidth / 2)), pixels_to_meters(groundStartPoint.iY + (groundHeight / 2)));
    groundDef.fixedRotation = true;
    BodyUPtr groundBody(b2dWorld.CreateBody(&groundDef), b2BodyDestroyer);
    b2PolygonShape groundShape;
    groundShape.SetAsBox(pixels_to_meters(groundWidth / 2), pixels_to_meters(groundHeight / 2));
    groundBody->CreateFixture(&groundShape, 0.f);
    ground->setBody(groundBody);
    rgModels.push_back(ShapeUPtr(ground));

    // setup ball
    Circle *ball = new Circle(Point(SCREEN_WIDTH / 4, SCREEN_HEIGHT - 50), 30);
    ball->setColour(Colour(0xff, 0xcc, 0x33));

    // setup ball physics
    b2BodyDef ballDef;
    ballDef.type = b2_dynamicBody;
    const Point &ballStartPoint = ball->getCentre();
    ballDef.position.Set(pixels_to_meters(ballStartPoint.iX), pixels_to_meters(ballStartPoint.iY));
    ballDef.fixedRotation = true;
    BodyUPtr ballBody(b2dWorld.CreateBody(&ballDef), b2BodyDestroyer);
    b2CircleShape shapeCircle;
    shapeCircle.m_radius = pixels_to_meters(ball->getRadius());
    b2FixtureDef fixDef;
    fixDef.shape = &shapeCircle;
    fixDef.density = 1.f;
    fixDef.friction = 0.2f;
    fixDef.restitution = 0.3f;
    ballBody->CreateFixture(&fixDef);
    ball->setBody(ballBody);
    rgModels.push_back(ShapeUPtr(ball));

    // setup box
	const unsigned char boxSize = 30;
	vertexData.assign(1, Point(groundStartPoint.iX + (groundWidth / 2), groundHeight));
	vertexData.push_back(Point(vertexData[0].iX + boxSize, vertexData[0].iY));
	vertexData.push_back(Point(vertexData[1].iX, vertexData[1].iY + boxSize));
	vertexData.push_back(Point(vertexData[0].iX, vertexData[1].iY + boxSize));
	Polygon *box = new Polygon(vertexData);

    // setup box physics
    b2BodyDef boxDef;
	const Point &boxCentre = box->getCentre();
	boxDef.type = b2_dynamicBody;
	boxDef.position.Set(pixels_to_meters(boxCentre.iX), pixels_to_meters(boxCentre.iY));
	BodyUPtr boxBody(b2dWorld.CreateBody(&boxDef), b2BodyDestroyer);
	b2PolygonShape boxShape;
	boxShape.SetAsBox(pixels_to_meters(boxSize / 2), pixels_to_meters(boxSize / 2));
	boxBody->CreateFixture(&boxShape, 10.f);
	box->setBody(boxBody);
	box->setColour(Colour(0xff, 0, 0));
	rgModels.push_back(ShapeUPtr(box));

    // setup seesaw
    const unsigned short seesawHalfWidth = 50;
    const unsigned short seesawHalfHeight = 5;
    const Point seesawCentre(boxCentre.iX, boxCentre.iY + 70);
    vertexData.assign(1, Point(seesawCentre.iX - seesawHalfWidth, seesawCentre.iY - seesawHalfHeight));
    vertexData.push_back(Point(seesawCentre.iX + seesawHalfWidth, seesawCentre.iY - seesawHalfHeight));
    vertexData.push_back(Point(seesawCentre.iX + seesawHalfWidth, seesawCentre.iY + seesawHalfHeight));
    vertexData.push_back(Point(seesawCentre.iX - seesawHalfWidth, seesawCentre.iY + seesawHalfHeight));
    Polygon *seesaw = new Polygon(vertexData);
    b2BodyDef seesawDef;
#ifdef KINEMATIC_BODY
    seesawDef.type = b2_kinematicBody;
    seesawDef.angularVelocity = 1.3f;
#else
    seesawDef.type = b2_dynamicBody;
#endif
    seesawDef.position.Set(pixels_to_meters(seesawCentre.iX), pixels_to_meters(seesawCentre.iY));
    BodyUPtr seesawBody(b2dWorld.CreateBody(&seesawDef), b2BodyDestroyer);
    b2PolygonShape seesawShape;
    seesawShape.SetAsBox(pixels_to_meters(seesawHalfWidth), pixels_to_meters(seesawHalfHeight));
    seesawBody->CreateFixture(&seesawShape, 1.f);
    b2BodyDef seesawAnchorDef;
    seesawAnchorDef.position = seesawDef.position;
#ifndef KINEMATIC_BODY
    b2Body *seesawAnchor(b2dWorld.CreateBody(&seesawAnchorDef));
    b2RevoluteJointDef seesawJointDef;
    seesawJointDef.Initialize(seesawAnchor, seesawBody.get(), seesawBody->GetWorldCenter());
    seesawJointDef.maxMotorTorque = 7.f;
    seesawJointDef.motorSpeed = 3.f;
    seesawJointDef.enableMotor = true;
    b2dWorld.CreateJoint(&seesawJointDef);
#endif
    seesaw->setColour(Colour(0x0f, 0xff, 0));
    seesaw->setBody(seesawBody);
    rgModels.push_back(ShapeUPtr(seesaw));
}

void setup2D()
{
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_DITHER);
#ifdef GL_MULTISAMPLE
	glDisable(GL_MULTISAMPLE);
#endif
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

    gluOrtho2D(0, SCREEN_WIDTH, 0, SCREEN_HEIGHT);

	glMatrixMode(GL_MODELVIEW);

    // for points, lines and polygons to be smooth
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_FASTEST);
    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_FASTEST);
	glEnable(GL_POINT_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT, GL_FASTEST);
}

int main()
{
    if (glfwInit())
    {
        glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
        auto win = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Box2D Learner", NULL, NULL);
        if (win)
        {
            glfwMakeContextCurrent(win);
            glfwSwapInterval(1);
            setup2D();

            b2World b2dWorld(b2Vec2(0.f, -10.f));
            vector<ShapeUPtr> rgModels;
#ifdef _DEBUG
            DebugDraw debugDrawer;
            b2dWorld.SetDebugDraw(&debugDrawer);
			glfwSetKeyCallback(win, keyHandler);
#endif
            setupScene(rgModels, b2dWorld);
            ShapePainter painter;

#ifdef _DEBUG
			double lastTick = glfwGetTime();
#endif
           	while (!glfwWindowShouldClose(win) && (glfwGetKey(win, GLFW_KEY_ESCAPE) != GLFW_PRESS))
            {
                b2dWorld.Step(1.f / 60.f, 6, 2);
                updateStates(rgModels, win);		// also query the input here for now
                paint(rgModels.cbegin(),
                      rgModels.cend(),
                      painter
#ifdef _DEBUG
                     ,b2dWorld
#endif
                     );
#ifdef _DEBUG
              	glfwSwapBuffers(win);
                glfwPollEvents();

                const double currentTick = glfwGetTime();
                const double currentFps = 1.0 / (currentTick - lastTick);
                std::printf("%.2f\r", currentFps);
                lastTick = currentTick;
                std::fflush(stdout);
#endif
            }
        }
        glfwTerminate();
    }
}
