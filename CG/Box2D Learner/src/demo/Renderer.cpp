#include "Renderer.hpp"
#include <GL/gl.h>

const double DEG_TO_RAD = b2_pi / 180.f;
const double RAD_TO_DEG = 1 / DEG_TO_RAD;

namespace Demo
{
	void ShapePainter::paint(const b2CircleShape *pCircle, const Demo::Body &body) const
	{
		glColor4ubv(body.skinColour.rgba.data());

		const b2Vec2& centre = body.physics->GetWorldCenter();
		glPushMatrix();
		glTranslatef(centre.x + pCircle->m_p.x,
					 centre.y + pCircle->m_p.y,
					 0.f);

		const float &radius = pCircle->m_radius;
		glBegin(GL_TRIANGLE_FAN);
		glVertex2f(0, 0);
		glVertex2f(radius, 0);
		for(unsigned short angle = 5; angle <= 360; angle += 5)
		{
			auto x = radius * std::cos(angle * DEG_TO_RAD);
			auto y = radius * std::sin(angle * DEG_TO_RAD);
			glVertex2f(x, y);
		}
		glEnd();

		glPopMatrix();
	}

	void ShapePainter::paint(const b2PolygonShape *pPolygon, const Demo::Body &body) const
	{
		glColor4ubv(body.skinColour.rgba.data());
		glPushMatrix();
		const b2Vec2& centre = body.physics->GetPosition();
		glTranslatef(centre.x + pPolygon->m_centroid.x,
					 centre.y + pPolygon->m_centroid.y,
					 0.f);
		const float angle = body.physics->GetAngle();
		glRotatef(angle * RAD_TO_DEG, 0.f, 0.f, 1.f);

		glBegin(GL_POLYGON);
		for (auto i = 0; i < pPolygon->m_count; ++i)
		{
			glVertex2f(pPolygon->m_vertices[i].x, pPolygon->m_vertices[i].y);
		}
		glEnd();
		glPopMatrix();
	}

	void ShapePainter::paint(const Demo::Body &body) const
	{
		if (body.visible)
		{
			const auto *pFixture = body.physics->GetFixtureList();
			while (pFixture)
			{
				switch (pFixture->GetType())
				{
					case b2Shape::Type::e_circle:
						paint(static_cast<const b2CircleShape*>(pFixture->GetShape()),
							  body);
						break;
					case b2Shape::Type::e_polygon:
						paint(static_cast<const b2PolygonShape*>(pFixture->GetShape()),
							  body);
					default:
						break;
				}
				pFixture = pFixture->GetNext();
			}
		}
	}
	
	void drawStaticBackGround()
	{
		const Colour wallColour(0xD2, 0xB4, 0x8C, 0xFF);
		const Colour floorColour(0xD3, 0xD3, 0xD3, 0xFF);
		const Colour frontWallColour(0x87, 0xCE, 0xFF, 0xFF);

		const GLshort roomCoords[] = {
										16, 0, 14, 2, 2, 2, 0, 0,		// floor
										0, 0, 2, 2, 2, 10, 0, 12,		// left wall
										0, 12, 2, 10, 14, 10, 16, 12,	// roof
										16, 12, 14, 10, 14, 2, 16, 0,	// right wall
										2, 2, 14, 2, 14, 10, 2, 10		// from wall
									 };
		const GLubyte roomColours[] = {
										floorColour.rgba[0], floorColour.rgba[1], floorColour.rgba[2],
										floorColour.rgba[0], floorColour.rgba[1], floorColour.rgba[2],
										floorColour.rgba[0], floorColour.rgba[1], floorColour.rgba[2],
										floorColour.rgba[0], floorColour.rgba[1], floorColour.rgba[2],
										wallColour.rgba[0], wallColour.rgba[1], wallColour.rgba[2],
										wallColour.rgba[0], wallColour.rgba[1], wallColour.rgba[2],
										wallColour.rgba[0], wallColour.rgba[1], wallColour.rgba[2],
										wallColour.rgba[0], wallColour.rgba[1], wallColour.rgba[2],
										floorColour.rgba[0], floorColour.rgba[1], floorColour.rgba[2],
										floorColour.rgba[0], floorColour.rgba[1], floorColour.rgba[2],
										floorColour.rgba[0], floorColour.rgba[1], floorColour.rgba[2],
										floorColour.rgba[0], floorColour.rgba[1], floorColour.rgba[2],
										wallColour.rgba[0], wallColour.rgba[1], wallColour.rgba[2],
										wallColour.rgba[0], wallColour.rgba[1], wallColour.rgba[2],
										wallColour.rgba[0], wallColour.rgba[1], wallColour.rgba[2],
										wallColour.rgba[0], wallColour.rgba[1], wallColour.rgba[2],
										frontWallColour.rgba[0], frontWallColour.rgba[1], frontWallColour.rgba[2],
										frontWallColour.rgba[0], frontWallColour.rgba[1], frontWallColour.rgba[2],
										frontWallColour.rgba[0], frontWallColour.rgba[1], frontWallColour.rgba[2],
										frontWallColour.rgba[0], frontWallColour.rgba[1], frontWallColour.rgba[2]
									 };

    	glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		// draw all the walls
		glColorPointer(3, GL_UNSIGNED_BYTE, 0, roomColours);
		glVertexPointer(2, GL_SHORT, 0, roomCoords);
		glDrawArrays(GL_QUADS, 0, ARRAYSIZE(roomCoords)/2);

		// from now on just go with solid colours instead of individual colours for vertices
		glDisableClientState(GL_COLOR_ARRAY);

		// set line width since it's all line drawing from here on
		glLineWidth(2.f);

		const Colour wallDemarkerColour(0xff, 0, 0, 0x7f);
		glColor4ubv(wallDemarkerColour.rgba.data());
		const GLshort wallDemarkers[] = {
											0, 0, 2, 2,		 // floor vs left wall
											0, 12, 2, 10,	 // left wall vs roof
											14, 10, 16, 12,	 // roof vs right wall
											14, 2, 16, 0 	 // right wall vs floor
										};
		glVertexPointer(2, GL_SHORT, 0, wallDemarkers);
		glDrawArrays(GL_LINES, 0, ARRAYSIZE(wallDemarkers)/2);

		const GLshort fontWallDemarkers[] = {
												2, 2,     // front wall vs floor
												14, 2,    // front wall vs right wall 
												14, 10,   // front wall vs roof 
												2, 10     // font wall vs left wall
											};
		glVertexPointer(2, GL_SHORT, 0, fontWallDemarkers);
		glDrawArrays(GL_LINE_LOOP, 0, ARRAYSIZE(fontWallDemarkers)/2);

		// collision marker
		// glColor4ub(0, 0, 0, 0x25);
		// const GLshort collisionDemarkers[] = { 1, 1, 15, 1, 15, 11, 1, 11 };
		// glVertexPointer(2, GL_SHORT, 0, collisionDemarkers);
		// glDrawArrays(GL_LINE_LOOP, 0, ARRAYSIZE(collisionDemarkers)/2);

		// disable the vertex array
		glDisableClientState(GL_VERTEX_ARRAY);
	}
}