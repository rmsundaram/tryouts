#ifndef __MODEL_HPP__
#define __MODEL_HPP__

#include <box2d/box2d.h>
#include <memory>
#include <array>
#include <functional>

namespace Demo
{
	const std::function<void(b2Body*)> b2BodyDeleter = [](b2Body *pBody)
	{
		pBody->GetWorld()->DestroyBody(pBody);
	};
	using BodyPtr = std::unique_ptr<b2Body, decltype(b2BodyDeleter)>;
	using ColourValue = std::array<unsigned char, 4>;

	struct Colour
	{
		Colour() = default;
		Colour(unsigned char r,
			   unsigned char g,
			   unsigned char b,
			   unsigned char a) : rgba{{r, g, b, a}}
		{
		}
		ColourValue rgba{{ 0xff, 0xff, 0xff, 0xff }};
	};

	class Body
	{
	public:
		Body(const Body &) = delete;
		Body(BodyPtr body) : physics(std::move(body)) { }
		Body(Body &&other) : visible(other.visible),
							 skinColour(other.skinColour),
							 physics(std::move(other.physics))
		{
		}

		bool   visible = true;
		Colour skinColour;
		BodyPtr physics;
	};
}
#endif
