#ifndef __RENDERER_HPP__
#define __RENDERER_HPP__

#include "Model.hpp"

template <typename T, size_t N>
constexpr inline size_t ARRAYSIZE(const T (&) [N])
{
    return N;
}

namespace Demo
{
	void drawStaticBackGround();
	class ShapePainter
	{
	public:
		void paint(const b2CircleShape *pCircle, const Demo::Body &body) const;
		void paint(const b2PolygonShape *pPolygon, const Demo::Body &body) const;
		void paint(const Demo::Body &body) const;
	};
}

#endif