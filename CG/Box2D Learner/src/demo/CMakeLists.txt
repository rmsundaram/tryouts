add_executable(demo
  DebugDraw.cpp
  DebugDraw.hpp
  Model.hpp
  Main.cpp
  Renderer.cpp
  Renderer.hpp
)

target_compile_definitions(demo PRIVATE
  $<$<CONFIG:DEBUG>:_DEBUG>
)

target_compile_features(demo PRIVATE cxx_std_11)
set_target_properties(demo PROPERTIES
  CXX_STANDARD_REQUIRED ON
  CXX_EXTENSIONS OFF
)

target_compile_options(demo PRIVATE
  $<$<CXX_COMPILER_ID:GNU,AppleClang,Clang>:-Wall -Wextra -pedantic
  -Wno-missing-field-initializers -Wcast-align -Wconversion -Wcast-qual
  -Wno-div-by-zero -Wdouble-promotion>
  $<$<CXX_COMPILER_ID:GNU,Clang,AppleClang>:-fdiagnostics-color>
  $<$<CXX_COMPILER_ID:GNU,Clang,AppleClang>:$<$<CONFIG:Release>:-ffast-math -msse2>>
  # enable macro debugging with GDB level 3
  $<$<AND:$<CXX_COMPILER_ID:GNU,AppleClang,Clang>,$<CONFIG:DEBUG>>:-ggdb3>
  $<$<CXX_COMPILER_ID:GNU>:-Wvector-operation-performance -Wlogical-op>)

target_link_libraries(demo
  box2d
  glfw
  OpenGL::GL
  OpenGL::GLU
)
