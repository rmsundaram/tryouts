#include "box2d/box2d.h"
#include <GL/gl.h>
#include <functional>
#include <memory>
#include "DebugDraw.hpp"

const double DEG_TO_RAD = b2_pi / 180.f;

void DebugDraw::_drawPolygon(const b2Vec2* &vertices, int32 vertexCount, const b2Color& colour, GLenum mode)
{
    glColor3f(colour.r, colour.g, colour.b);
    glPushMatrix();
    glBegin(mode);
    for (unsigned char i = 0; i < vertexCount; ++i)
    {
        glVertex2f(vertices->x, vertices->y);
        ++vertices;
    }
    glEnd();
    glPopMatrix();
}

void DebugDraw::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& colour)
{
    _drawPolygon(vertices, vertexCount, colour, GL_LINE_STRIP);
}

void DebugDraw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& colour)
{
    _drawPolygon(vertices, vertexCount, colour, GL_POLYGON);
}

void DebugDraw::_drawCircle(const b2Vec2& center, float radius, const b2Vec2 *axis, const b2Color& colour)
{
    glColor4f(colour.r, colour.g, colour.b, 0.5f);
    glPushMatrix();
    glTranslatef(center.x, center.y, 0.f);

    if (!axis)
    {
        glBegin(GL_LINE_STRIP);
    }
    else
    {
        glBegin(GL_TRIANGLE_FAN);
        glVertex2i(0, 0);
    }
    glVertex2f(0, radius);
    for (unsigned short angle = 5; angle <= 360; angle += 5)
    {
        const double cosO = std::cos(angle * DEG_TO_RAD);
        const double sinO = std::sin(angle * DEG_TO_RAD);

        glVertex2d(radius * sinO, radius * cosO);
    }
    glEnd();

    if (axis)
    {
		glColor4f(colour.r, colour.g, colour.b, 1.f);
        glBegin(GL_LINES);
        glVertex2i(0, 0);
        auto axisLine = radius * (*axis);
        glVertex2f(axisLine.x, axisLine.y);
        glEnd();
    }

    glPopMatrix();
}

void DebugDraw::DrawCircle(const b2Vec2& center, float radius, const b2Color& colour)
{
    _drawCircle(center, radius, nullptr, colour);
}

void DebugDraw::DrawSolidCircle(const b2Vec2& center, float radius, const b2Vec2& axis, const b2Color& colour)
{
    _drawCircle(center, radius, &axis, colour);
}

void DebugDraw::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& colour)
{
    glColor3f(colour.r, colour.g, colour.b);
    glPushMatrix();
    glBegin(GL_LINES);
    glVertex2f(p1.x, p1.y);
    glVertex2f(p2.x, p2.y);
    glEnd();
    glPopMatrix();
}

void DebugDraw::DrawPoint (const b2Vec2 &p, float size, const b2Color &color)
{
    glColor3f(color.r, color.g, color.b);
    glPushMatrix();
    glPointSize(size);
    glBegin(GL_POINTS);
    glVertex2f(p.x, p.y);
    glEnd();
    glPopMatrix();
}
