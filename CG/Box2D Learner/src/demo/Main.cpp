#include "Renderer.hpp"
#include <GLFW/glfw3.h>
// CALLBACK macro definition on Windows needed for glu.h
#ifdef _WIN32
#  include <minwindef.h>
#endif
#include <GL/glu.h>
#ifdef _DEBUG
#include "DebugDraw.hpp"
#endif
#include <stdexcept>
#include <vector>

using std::vector;
using Demo::BodyPtr;

auto pixelsToMeters = 0.02f;
auto SCREEN_WIDTH = 800;
auto SCREEN_HEIGHT = 600;
auto uScreenWidth = SCREEN_WIDTH * pixelsToMeters;
auto uScreenHeight = SCREEN_HEIGHT * pixelsToMeters;

#ifdef _DEBUG
bool debugMode = false;
#endif
bool paused = false;

struct glfwRaii
{
    glfwRaii()
    {
        if (GL_FALSE == glfwInit())
        {
            throw std::runtime_error("Error initializing GLFW!");
        }
    }
    ~glfwRaii()
    {
        glfwTerminate();
    }
};

template <typename _Iter>
void paint(_Iter beginIter, _Iter endIter, const Demo::ShapePainter &painter
#ifdef _DEBUG
           , b2World &b2dWorld
#endif
          )
{
    glClear(GL_COLOR_BUFFER_BIT);
    glLoadIdentity();
    glColor4ub(0xff, 0xff, 0xff, 0xff);

#ifdef _DEBUG
if (!debugMode)
{
#endif
    Demo::drawStaticBackGround();
    for ( ; beginIter != endIter; ++beginIter)
    {
        painter.paint(*beginIter);
    }
#ifdef _DEBUG
}
else
{
    b2dWorld.DebugDraw();
}
#endif
    assert(GL_NO_ERROR == glGetError());
}

void updateStates(vector<Demo::Body>& rgBodies, GLFWwindow* win)
{
    b2Vec2 linearImpulse(glfwGetKey(win, GLFW_KEY_RIGHT) ? 0.75f : (glfwGetKey(win, GLFW_KEY_LEFT) ? -0.75f : 0.f),
                         glfwGetKey(win, GLFW_KEY_UP) ? 0.75f : (glfwGetKey(win, GLFW_KEY_DOWN) ? -0.75f : 0.f));
    if (linearImpulse.x || linearImpulse.y)
    {
        auto &ball = rgBodies.back();
        ball.physics->ApplyLinearImpulse(linearImpulse, b2Vec2_zero, false);
    }
}

void keyHandler(GLFWwindow*, int key, int /*scancode*/, int action, int /*mods*/)
{
    if (GLFW_RELEASE == action)
    {
        switch (key)
        {
#ifdef _DEBUG
            case 'R':
                debugMode = false;
                break;
            case 'D':
                debugMode = true;
                break;
#endif
            case GLFW_KEY_SPACE:
                paused = !paused;
            default:
                break;
        }
    }
}

void setupScene(vector<Demo::Body> &rgBodies, b2World &b2dWorld)
{
    // create room
    b2BodyDef bodyDef;
    bodyDef.type = b2_staticBody;
    bodyDef.position.Set(1.f, 1.f);
    BodyPtr b2BodyPtr(b2dWorld.CreateBody(&bodyDef), Demo::b2BodyDeleter);
    b2ChainShape wallShape;
    const b2Vec2 wallVertices[] = { {0.f, 0.f}, {0.f, 10.f}, {14.f, 10.f}, {14.f, 0.f} };
    wallShape.CreateLoop(wallVertices, ARRAYSIZE(wallVertices));
    b2BodyPtr->CreateFixture(&wallShape, 0);
    rgBodies.emplace_back(std::move(b2BodyPtr));
    rgBodies.back().visible = false;

    // create spinner
    bodyDef.type = b2_kinematicBody;
    bodyDef.position.Set(4.f, 4.f);
    bodyDef.angularVelocity = 9.f;
    b2BodyPtr.reset(b2dWorld.CreateBody(&bodyDef));
    b2PolygonShape boxShape;
    boxShape.SetAsBox(1.f, 1.f);
    b2BodyPtr->CreateFixture(&boxShape, 1.f);
    rgBodies.emplace_back(std::move(b2BodyPtr));
    rgBodies.back().skinColour = {0xff, 0, 0x43, 0xff};

    // create ball
    bodyDef.type = b2_dynamicBody;
    bodyDef.position.Set(4.5f, 11.f);
    bodyDef.angularVelocity = 0.f;
    b2BodyPtr.reset(b2dWorld.CreateBody(&bodyDef));
    b2CircleShape circleShape;
    circleShape.m_radius = 0.65f;
    b2BodyPtr->CreateFixture(&circleShape, 1.f)->SetRestitution(0.3f);
    rgBodies.emplace_back(std::move(b2BodyPtr));
    rgBodies.back().skinColour.rgba[3] = 0xe0;
}

void Setup2D()
{
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_DITHER);
#ifdef GL_MULTISAMPLE
    glDisable(GL_MULTISAMPLE);
#endif
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // after conversion to meters
    gluOrtho2D(0, uScreenWidth, 0, uScreenHeight);

    glMatrixMode(GL_MODELVIEW);

    // for points, lines and polygons to be smooth
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_LINE_SMOOTH);
    glHint(GL_LINE_SMOOTH_HINT, GL_DONT_CARE);
    glEnable(GL_POLYGON_SMOOTH);
    glHint(GL_POLYGON_SMOOTH_HINT, GL_DONT_CARE);
    glEnable(GL_POINT_SMOOTH);
    glHint(GL_POINT_SMOOTH_HINT, GL_DONT_CARE);
}

int main()
{
    glfwRaii glfwInstance;
    glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
    auto win = glfwCreateWindow(SCREEN_WIDTH, SCREEN_HEIGHT, "Box2D Learner", NULL, NULL);
    if (win)
    {
        glfwMakeContextCurrent(win);
        glfwSwapInterval(1);
        Setup2D();

        b2World b2dWorld(b2Vec2(0.f, -10.f));
        vector<Demo::Body> rgBodies;

        setupScene(rgBodies, b2dWorld);
        Demo::ShapePainter painter;

#ifdef _DEBUG
        double lastTick = glfwGetTime();
        DebugDraw dbgDraw;
        b2dWorld.SetDebugDraw(&dbgDraw);
#endif
        glfwSetKeyCallback(win, keyHandler);

        while (!glfwWindowShouldClose(win) && (glfwGetKey(win, GLFW_KEY_ESCAPE) != GLFW_PRESS))
        {
            if (!paused) b2dWorld.Step(1.f / 120.f, 6, 2);
            updateStates(rgBodies, win);        // also query the input here for now
            paint(rgBodies.cbegin(), rgBodies.cend(), painter
#ifdef _DEBUG
                  , b2dWorld
#endif
                 );
#ifdef _DEBUG
            glfwSwapBuffers(win);
            glfwPollEvents();

            const double currentTick = glfwGetTime();
            const double currentFps = 1.0 / (currentTick - lastTick);
            std::printf("%.2f\r", currentFps);
            lastTick = currentTick;
            std::fflush(stdout);
#endif
        }
    }
}
