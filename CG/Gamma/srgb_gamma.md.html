<meta charset="utf-8">
    **Linear vs Non-linear Colourspaces**

# Linear

In linear colour spaces, the numbers stored have a linear relationship with
colour intensity represented.  So if you, say, double the number and you double
the intensity/brightness; adding two intensities is a simple matter of adding
two numbers.  It’s correct, convenient and intuitive to work with when doing
image processing, lighting calculations, etc.  Use RGB with 16+ bits/channel.

# Non-linear

Human eye is better at distinguishing darker shades than lighter ones i.e. it’s
sensitivity to light is finer at lower intensities[^perception-log].  With `u8`
to represent a colour, you “waste” bits for higher intensities that the eye
cannot distinguish; some of those bits might serve better representing lower
intensities.  sRGB is a non-linear colour space that does this. sRGB standard
specifies a curve that is shallower at the bottom and steeper at the top -- more
dark greys and less light greys.  However, adding two colours yield a colour
lighter than it should be.

!!! Note: sRGB and beyond!
    sRGB is _a_ colour space specifying a gamma transfer function pair.  There
    are other gamma specifications too.  Most are around raising to 2.2.

# Gamma Correction

To fully capture all colour that we can perceive, we need 12 bits/channel with
many useless bits as we cannot differentiate between many lighter tints.  Older
video hardware engineers designed a system taking 8-bits/channel, favouring
darker shades with more bits by gamma-encoding $\eqref{1}$.  If these 8 bits are
allocated (linearly) equally between light and dark, we’d be “wasting” bits for
the lighter shades which the human eye anyways can’t discern.  Most display
systems expect sRGB signals (gamma-encoded).  Internally they’d gamma-decode
\eqref{2} and show with original intensity.

$$
\begin{align*}
V_{linear}^\frac{1}{\gamma} &= V_{gamma}           \tag{1}\label{1}   \\[2ex]
           V_{gamma}^\gamma &= V_{linear}          \tag{2}\label{2}
\end{align*}
$$

$\gamma$ is usually 2.2, in sRGB it’s 2.4 (for good reason [[19](#references)]).

A common misconception is that gamma correction exists to compensate for the
inability of CRT displays to work linearly.  Truly it’s a compression: encoding
with a curve and decoding on display, allowing use of just 8 bits/channel.  This
is true with LCDs too.  Entirely linear signal path would need at least 12
bits/channel [[2](#references)].  Displays expect input be in (gamma-corrected)
sRGB space.

A monitor can display 256 unique levels of intensity; writing [0, 255] as-is
will give a _perceptually linear_ gradient from black to white, thanks to the
nonlinearity of monitors; OTOH in the _truly linear_ ramp (achieved by writing
gamma-encoded [0, 255]) there’re 73 aliasing values (`linear_srgb.lua`) that are
undifferentiable to human eyes and are better utilized by gamma correction.
It’s a form of compression; compressing the interesting states of a 12-bit type
into an 8-bit type [[2](#references)], so that monitors can display 256 unique,
perceptually-linear states with just 8 bit; without gamma correction we’d need
more bits (~11 or 12) to represent those states since counting from 0 to 255
(without correction) gives colours undifferentiable by humans.  Conversely, even
if we use 12 bits many states might be redundant to human eyes.

## Image formats and Gamma

Most images output by devices like camera are gamma-corrected[[9](#references)]
i.e. gamma-encoded, so that the final output from the monitor is linear.
Excerpt from ImageMagick’s Colour Management [[13](#references)]:

> Due to the standardization of sRGB on the Internet, most image formats use
> SRGB as the default working color space. If the color space of an image is
> unknown and it is an 8- to 16-bit image format, assuming it is in the sRGB
> color space is a safe choice. This extends to grayscale as well. We assume
> non-linear grayscale. These assumptions are overridden if a particular image
> format includes color space and / or gamma metadata.

PNG has a special `aAMA` parameter which is sadly unenforced by the standard and
hence seem to be ignored by writers/readers [[15](#references)].  It’s better to
not specify any gamma value in the header and simply gamma-encode and write like
for the rest of the formats.

!!! Note: Convention
    Float RGB values are generally linear; integer RGB values are generally
    gamma-compressed [[21](#references)].

Data read from (gamma-encoded, sRGB) image can be copied as-is to framebuffer;
monitor gamma-decodes and reproduces the right colours.  This is the reason most
image formats store colours in sRGB.  Conversions are needed for processing, as
operations like blending, lighting equation, etc. do not make sense in
non-linear spaces like sRGB.  We need all inputs to be in linear space for
processing equations to be correct; once conversion is done, we gamma-encode and
store in non-linear space.

## Alpha Channel and Gamma

The alpha channel is never rendered on to a display device directly; it’s a
non-colour value only used to influence the intensity of the associated pixel’s
other channels, so encoding alpha with gamma is pointless and would lead to
precision loss.  Alpha is generally stored in linear space [[7](#references)].

# Suitable Workflow

This leads us to a workflow model: use linear RGB for processing as its straight
forward to map values to colours, while store/output in sRGB. As explained
above, however, linear `u8` isn’t enough for the darks human eye can discern, so
process linear, `u16` and store non-linear, `u8`. Perform conversions at image
source read and write [[1](#references)].

``` rust
// conversion functions operating only on greyscales
// using f32 as there isn't a f16 (which is enough here)
// https://en.wikipedia.org/wiki/SRGB#Specification_of_the_transformation
fn srgb_to_linear(srgb: u8) -> f32 {
    let s: f32 = srgb as f32 / 255.0;
    assert!((s >= 0.0) && (s <= 1.0));
    if s <= 0.04045 {
        s / 12.92
    } else {
        ((s + 0.055) / 1.055).powf(2.4)
    }
}

fn linear_to_srgb(l: f32) -> u8 {
    let s = if l <= 0.0031308 {
        l * 12.92
    } else {
        1.055 * l.powf(0.416666666667) - 0.055   // 1 ÷ 2.4 = 0.416666666667
    };
    (s * 255.0).round() as u8
}
```

It’s important to remember to

* Convert input sRGB textures to linear space
* Maintain intermediate textures in linear space
* Never store linear colour data as `u8` (memory/disk) -- use `f16`, `u16`, etc.
* Maintain the entire pipeline in linear space (having one erring component will
  break the system down)
* gamma-encode only at the _very end_ before writing to framebuffer/image

See [[1, 4, 10, 14, 16 and 20](#references)] for tips on working with gamma.

## OpenGL

### Output

OpenGL does no colour conversions by default.  For linear framebuffers, fragment
shader output is untouched; OpenGL doesn’t know or care about output’s space.
Every fragment shader, as a last step, should gamma-encode -- expected by
monitors which gamma-decode [[16](#references)].

For sRGB framebuffers (i.e. `GL_SRGB8{,_ALPHA8}` formats), if
`GL_FRAMEBUFFER_SRGB` is enabled, fragment shader output is assumed to be linear
and is converted to sRGB automatically[^linear-ignored] [[26](#references)].

!!! Note: Linear vs sRGB framebuffers
    Marking a framebuffer linear or sRGB is only for OpenGL to dis-/en-able
    automatic conversion; it isn’t to specify the underlying image format.

By default, depending on platform and drivers, one might get linear (`RGBA8`) or
sRGB (`SRGB8`) framebuffer from windowing APIs (GLFW, SDL, GLUT, …); this can be
influenced by hints like `GLFW_SRGB_CAPABLE`, `SDL_GL_FRAMEBUFFER_SRGB_CAPABLE`.
Once obtained it can be queried[^frame-format].  Since monitors expect sRGB
input, all framebuffers are to store sRGB.  _Even on a linear framebuffer sRGB
values are to be written_.  Just that they’re not marked so at times!

Excerpt from [[20](#references)]:

> OSes expect apps to provide them with a buffer that is declared as a linear
> format buffer, but which will contain gamma-space data. This special case is
> extremely counter-intuitive to many people, and causes much confusion — they
> think that because this is a “linear” buffer format, that it does not contain
> gamma-space data.

### Input

For automatic gamma-decode of input sRGB texture data to linear, use `GL_SRGB*`
internal format [[3](#references)] [[4](#references)].

If both input (texture) and output (framebuffer) aren’t marked sRGB, no implicit
conversions are performed.  Samplers give sRGB data as-is in shaders, can be
written to linear framebuffers as-is.  Monitors gamma-decode and faithfully
reproduce colours.  This breaks down when some image processing, lighting
calculations are done on any sRGB value [[27](#references)].

## Image Processing

Cameras gamma-encode and store data; before blending (check and) gamma-decode,
then blend, gamma-encode and store.  Most image processors don’t to do this by
default when compositing, leading to ugly artefacts [[24](#references)] e.g. red
and green blended doesn’t give yellow but brown!  Some software have settings to
be tweak and make it do the right thing.  See [[22](#references)] for a quick
summary of gamma when image processing/blending.  When a colour picker provides
[0, 255] integer colours or [web colours][] (hex), one can be sure
[[23](#references)] that these are non-linear sRGB colours e.g. GIMP, MS Paint,
Inkscape.  Other software like Blender and Krita clearly show the colour space
and have floating-point inputs for the channels providing linear RGB values.

> "Don’t convert sRGB `u8` to Linear `u8`! [[18](#references)]"

!!! Warning: ÷ 255 isn’t enough!
    Converting an sRGB integer colour doesn’t stop at ÷ 255.  RGB linearization
    is needed too -- $x^\gamma$ .  `[173, 48, 184] ÷ 255 = [0.678, 0.188,
    0.721]`: sRGB float (non-linear); `srgb_to_linear()` rightly gives `[0.42,
    0.03, 0.48]`: RGB float (linear).

[web colours]: https://en.wikipedia.org/wiki/Web_colors

# Why do it?

Since monitor’s output curve counters human eyes’ response curve, why can’t we
simply output in linear space and let the monitor cancel human eyes’
idiosyncrasy?  Because we want linear, not gamma, out.  It’s a space good for
encoding, but the final colour we want needs to be the same as real world where
light transport is linear [[10](#references)].  Human eye perceives this real
world.  The gamma space is a convenient tool to encode and fit more meaningful
data within 8 bits but if we want to match our simulation to the real world, we
want to gamma-encode, let the monitor gamma-decode and let the viewer finally
see in linear space.

The converse question of “If we want the output to be linear, why do monitors
gamma-decode, why not simply output linear signals?”  To have 8 bits/channel.
Read above on why having a completely linear signal path needs 12 bits/channel.

!!! Note: To encode or not to encode?
    If you want to show truly 50% grey then set intensity to `0.5`, gamma-encode
    and write `0.72974` so that the monitor displays a pixel with `0.5`
    intensity.  However, if you want a grey shade that is perceived as the
    median of the all the grey shades your display can emit, then write `0.5`
    directly and the monitor will show a pixel with `0.21763` intensity which
    would be perceived as mid-grey.

In John Novak’s _What every coder should know about gamma_ [[6](#references)],
he shows two colour ramps: Figure 1 doesn’t look evenly gradient, while Figure 2
does.  He uses these images to demonstrate how the human eye perceives darker
shades better.  It’s interesting to see how these are generated.  Both images
start with lerping between 0 and 1, how the intensity is transformed there
after:

1. To show how linear ramp is perceived, the intensity is gamma-encoded
   $\eqref{1}$ and written to the file; as the monitor gamma-decodes $\eqref{2}$
   a linear final output is perceived by the human eye.
2. To show the smoothly-perceived gradient, though John gamma-decodes first,
   just when writing to the file, the values are gamma-encoded i.e. they cancel
   out each other and linear values end up in the file.  Since the monitor
   gamma-decodes and shows darker colours, we perceive it as smoother. Just
   writing linear values directly to the file would suffice here ;)

Implementations in `gamma-ramp.lua`.

!!! Warning
    The real world uses a linear colour space [[17](#references)].  Our visual
    system perceives it differently i.e. we better differentiate between darker
    shades.  A camera captures real world light (linear), gamma-encodes it and a
    monitor gamma-decodes it to display back the original linear data camera
    captured [[11](#references)].  When simulating the real world, we want to do
    the same.  Operate in linear space, perform lighting calculations and lastly
    gamma-encode, to get a linear final output.  Though for the smooth ramp we
    write values linearly to get a non-linear final output, it’s just to
    demonstrate human eyes’ idiosyncrasy.

# References

1. [**What are the practical differences when working with colors in a linear vs. a non-linear RGB space?**](https://stackoverflow.com/a/12894053)
2. [What is the purpose of gamma correction in today's screens?](https://photo.stackexchange.com/q/53725/88300)
3. [Do I need to gamma correct the final color output?](https://stackoverflow.com/q/23026151)
4. [Free Gamma Correction — Learning Modern 3D Graphics Programming](https://paroj.github.io/gltut/Texturing/Tut16%20Free%20Gamma%20Correction.html)
5. [Framebuffer — OpenGL Wiki](https://www.khronos.org/opengl/wiki/Framebuffer)
6. [**What every coder should know about gamma**](https://blog.johnnovak.net/2016/09/21/what-every-coder-should-know-about-gamma/)
7. [Should alpha channel be gamma correct?](https://computergraphics.stackexchange.com/q/4748/1650)
8. [Gamma correction — Wikipedia](https://en.wikipedia.org/wiki/Gamma_correction)
9. [Gamma-correct Rendering](https://blog.molecular-matters.com/2011/11/21/gamma-correct-rendering/)
10. [**Importance of Being Linear - GPU Gems 3**](https://developer.nvidia.com/gpugems/gpugems3/part-iv-image-effects/chapter-24-importance-being-linear)
11. [**Linear-Space Lighting**](http://filmicworlds.com/blog/linear-space-lighting-i-e-gamma/)
12. [What exactly is the need for gamma correction?](https://stackoverflow.com/questions/12975688/what-exactly-is-the-need-for-gamma-correction)
13. [Colour Management - ImageMagick](https://imagemagick.org/script/color-management.php)
14. [Outdoors Lighting - Inigo Quilez](https://www.iquilezles.org/www/articles/outdoorslighting/outdoorslighting.htm)
15. [The Sad Story of PNG Gamma “Correction”](https://hsivonen.fi/png-gamma/)
16. [Gamma Correction - Learn OpenGL](https://learnopengl.com/Advanced-Lighting/Gamma-Correction)
17. [In Favor of Back End Gamma Correction](http://www.anyhere.com/gward/vrml/gamma.html)
18. [Don’t convert sRGB U8 to Linear U8!](https://blog.demofox.org/2018/03/10/dont-convert-srgb-u8-to-linear-u8/)
19. [sRGB - Wikipedia](https://en.wikipedia.org/wiki/SRGB#Specification_of_the_transformation)
20. [**The sRGB Learning Curve**](https://medium.com/game-dev-daily/the-srgb-learning-curve-773b7f68cf7a)
21. [Color spaces of DXGI_FORMATs?](https://stackoverflow.com/a/13492555/183120)
22. [Computer Color is Broken](https://www.youtube.com/watch?v=LKnqECcg6Gw)
23. [sRGB color space in OpenGL](https://unlimited3d.wordpress.com/2020/01/08/srgb-color-space-in-opengl/)
24. [Alpha Compositing](https://ciechanow.ski/alpha-compositing/#linear-values)
25. [Doing Math with RGB (and A) Correctly](http://www.essentialmath.com/GDC2015/VanVerth_Jim_DoingMathwRGB.pdf)
26. [sRGB textures. Is this correct?](https://stackoverflow.com/a/10348719/183120)
27. [sRGB Conversion OpenGL](https://stackoverflow.com/a/65832474/183120)

[when-enable-srgb]: https://stackoverflow.com/a/11386809/183120

[^perception-log]: Human perception is generally logarithmic not linear.

[^linear-ignored]: Enabling `GL_FRAMEBUFFER_SRGB` [won’t affect linear
framebuffers][when-enable-srgb], so it can be left enabled.  sRGB texture
_reads_ are always implicitly converted to linear; this option has no bearing;
refer OpenGL 4.6, §8.24.  Since OpenGL 4.4 (§18.3.1) blending and blitting
operations refer this flag for sRGB buffer reads.

[^frame-format]: Use `glGetFramebufferAttachmentParameteriv` to query
`GL_FRAMEBUFFER_ATTACHMENT_COLOR_ENCODING` parameter.


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'short' };</script>
