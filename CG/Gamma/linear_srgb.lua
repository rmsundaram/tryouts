#!/usr/bin/env lua

local function to_u8(f)
  return math.floor((f * 255) + 0.5)
end

-- https://en.wikipedia.org/wiki/SRGB#Specification_of_the_transformation
local function to_srgb(intensity)
  local s = 0
  if intensity <= 0.0031308 then
    s = intensity * 12.92
  else
    s = 1.055 * (intensity ^ 0.416666666667) - 0.055 -- 1 ÷ 2.4 = 0.416666666667
  end
  return s
end

for i = 0, 255 do
  print(i, to_u8(to_srgb(i/255)))
end
