#!/usr/bin/env lua

if (#arg < 4) then
  print("usage: gammaramp WIDTH HEIGHT BANDS OUTFILE")
  os.exit()
end

local function trunc(f)
  return f - (f % 1.0)
end

local function to_u8(f)
  return math.floor((f * 255) + 0.5)
end

-- https://en.wikipedia.org/wiki/SRGB#Specification_of_the_transformation
local function to_srgb(intensity)
  local s = 0
  if intensity <= 0.0031308 then
    s = intensity * 12.92
  else
    s = 1.055 * (intensity ^ 0.416666666667) - 0.055 -- 1 ÷ 2.4 = 0.416666666667
  end
  return s
end

--[[
  1. Classify pixel’s band
  2. Brighten band accordingly
  3. Transform and append to scanline
]]
local function make_scanline(width, bands, transform)
  -- default transform: identity
  transform = transform or function(f) return f end
  -- adjust for the 0 intensity (first) band
  local intensity_per_band = 1 / (bands - 1)
  local scanline = {}
  for i = 1, width do
    local band = trunc((i - 1) / bands) -- ∈ [0, bands - 1]
    local intensity = transform(intensity_per_band * band) -- ∈ [0, 1]
    assert(((intensity >= 0.0) and (intensity <= 1.0)),
           "Invalid value: intensity")
    table.insert(scanline, to_u8(intensity))
  end
  return scanline
end

local function make_separator(count, colour)
  colour = colour or 255
  local scanline_separator = {}
  for i = 1, count do
    table.insert(scanline_separator, colour)
  end
  return table.concat(scanline_separator, " ")
end

local width_str = arg[1]
local bands_str = arg[3]

local width = tonumber(width_str)
local bands = tonumber(bands_str)

local band_width = width / bands
if (band_width % 1.0) > 0.0 then
  io.output(io.stderr)
    :write("Width isn't evenly divided by the number of bands!\n")
  os.exit()
end

-- 1. Output linear data and let monitor gamma-decode to suit colours to
--    human vision system (favouring darker shades)
-- 2. Output gamma-encoded data (sRGB), which monitor gamma-decodes cancelling
--    out each other giving back linear intensity

-- destination space: linear (uneven perception to human eyes)
local scanline1 = make_scanline(width, bands, to_srgb)
assert((#scanline1 == width), "Incorrect scanline count: " .. #scanline1)
-- destination space: gamma (even perception to human eyes)
local scanline2 = make_scanline(width, bands)
assert((#scanline2 == width), "Incorrect scanline count: " .. #scanline2)

local file_name = arg[4]
local out_file = io.open(file_name .. ".pgm", "w")
local height_str = arg[2]
-- PGM header
out_file:write("P2\n" .. width_str .. " " .. height_str .. "\n255\n")
local height = tonumber(height_str)
for i = 1, ((height / 2) - 1) do
  out_file:write(table.concat(scanline1, " ") .. "\n")
end
local sep_string = make_separator(width, 0)
out_file:write(sep_string .. "\n")
out_file:write(sep_string .. "\n")
for i = 1, ((height / 2) - 1) do
  out_file:write(table.concat(scanline2, " ") .. "\n")
end
out_file:close()
