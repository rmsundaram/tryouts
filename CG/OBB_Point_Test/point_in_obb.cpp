#include <glm/glm.hpp>
#include <glm/gtc/matrix_access.hpp>
#include <glm/gtx/matrix_transform_2d.hpp>
#include <iostream>

struct OBB
{
    glm::vec2 centre;
    glm::vec2 half_extent;
    glm::mat2 axes;
};

typedef glm::vec2 Point;

/*
 * 1. apply transform to world so that OBB centre is at world origin
 * 2. project point on OBB axes to find the offset in X and Y
 * 3. check if their absolute values are within half extents in respective directions
 */
bool is_within1(OBB const &obb, Point p)
{
    // the vector from OBB centre to point is the same displacement vector
    // P' = TP, where T is the translation to bring OBB.centre to origin
    p = p - obb.centre;
    glm::vec2 const d { glm::dot(glm::column(obb.axes, 0), p),
                        glm::dot(glm::column(obb.axes, 1), p) };
    return glm::all(glm::lessThanEqual(glm::abs(d), obb.half_extent));
}

// find P's representation in OBB's space and check if the absolute value of X and Y
// coordinates are within half extents of the OBB in resepective directions
bool is_within2(OBB const &obb, Point p)
{
    // we need Ma→b = Rev(Tb→a), so when seen as transforming coordinate systems
    // first counter rotation and then, w.r.t that inertial space, counter translation
    // Ma→b = RT
    glm::mat3 m_world_obb(glm::transpose(obb.axes));
    m_world_obb = glm::translate(m_world_obb, glm::vec2(-obb.centre.x, -obb.centre.y));
    p = glm::vec2(m_world_obb * glm::vec3(p, 1.0f));
    return glm::all(glm::lessThanEqual(glm::abs(p), obb.half_extent));
}

int main()
{
    OBB const b{{4.0f, 5.0f}, {2.5f, 1.5f}, {{0.707106781185f, 0.707106781185f}, {-0.707106781185f, 0.707106781185f}}};
    Point const p1{3.75f, 3.2f};
    std::cout << std::boolalpha << "Point 1\n"
              << "Method 1: " << is_within1(b, p1) << '\n'
              << "Method 2: " << is_within2(b, p1) << '\n';
    Point const p2{1.0f, 4.0f};
    std::cout << "Point 2\n"
              << "Method 1: " << is_within1(b, p2) << '\n'
              << "Method 2: " << is_within2(b, p2);
}
