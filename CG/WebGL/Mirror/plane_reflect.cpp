#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <iostream>

std::ostream& operator<<(std::ostream& os, const glm::mat4 &m)
{
    return
    os << m[0][0] << '\t' << m[1][0] << '\t' << m[2][0] << '\t' << m[3][0] << '\n'
       << m[0][1] << '\t' << m[1][1] << '\t' << m[2][1] << '\t' << m[3][1] << '\n'
       << m[0][2] << '\t' << m[1][2] << '\t' << m[2][2] << '\t' << m[3][2] << '\n'
       << m[0][3] << '\t' << m[1][3] << '\t' << m[2][3] << '\t' << m[3][3] << '\n';
}

// References
// 1. §11.1.2 Planar Reflectors, Advanced Graphics Programming Techniques Using OpenGL, Siggraph '99 Course by Blythe
// 2. §17.1.2 Planar Reflectors, Advanced Graphics Programming Using OpenGL by McReynolds, Blyte
// 3. §4.3.3 Reflection, Essential Math, Second Edition
// 4. OpenGL Developement Cookbook. Approach 2 was inspired by this, though the book itself has it incorrect
int main() {
    // Both approaches are arrive at the same result, giving different ways to think of and reach it.
    // Approach 1: Mirror the world and look at it from somewhere
    // Both reflection and the translation parts are baked into one mirror-by-arbitrary-plane matrix, which is then
    // concatenated to the view matrix to give the final mirror view matrix. One can think of this approach as rendering
    // the scene from the regular viewpoint, but with the world mirrored; see Figure 55 in [1]. It gives a clear
    // separation between the viewing and mirroring transforms.
    auto const eye = glm::vec3{10, 18, 20};
    auto const at = glm::vec3{0.0f, 5.0f, 0.0f};
    auto const up = glm::vec3{0.0f, 1.0f, 0.0f};
    auto const view1 = glm::lookAt(eye, at, up);
    std::cout << "View\n" << view1 << "\n";
    auto const mirrorZ = -4.0f;
    // bring the mirror to the origin such that Z = 0
    auto const tIn = glm::translate(glm::vec3{0.0f, 0.0f, -mirrorZ});
    // reflect in the XY plane i.e. flip Z coordinates
    auto const reflect = glm::scale(glm::vec3{1.0f, 1.f, -1.0f});
    // send the mirror back to its original location
    auto const tOut = glm::translate(glm::vec3{0.0f, 0.0f, mirrorZ});
    // computing tOut * reflect * tIn can be done in one shot; see [1] or [2] for reflection by arbtrary plane matrix
    auto const mirrorView1 = view1 * tOut * reflect * tIn;
    std::cout << "Mirrored 1\n" << mirrorView1 << "\n";

    // Approach 2: Look at the world from somewhere and mirror it
    // The translation part is pre-baked into the view matrix (lookAt) while the reflection part is tackled separately
    // by concatenating it to the view matrix to give the final mirror view matrix identical to approach 1. One can
    // think of this approach as rendering with the viewpoint mirrored; see Figure 54 in [1]. The sepration between the
    // view and mirror transforms is blurred in this approach.
    auto const mirrorNormal = glm::vec3{0.0f, 0.0f, 1.0f};
    // mirViewDir, what the mirror sees, is the reflection of the vector along which we see the mirror i.e the negated
    // view vector: eye − at
    auto const mirViewDir = glm::reflect(eye - at, mirrorNormal);
    /*
     *        −Z ^
     *           |    p      −3
     *           |           −2
     *  −X       |  -----    −1 = Z
     *  <--------0----------> 0
     *           |    b       1
     *           |            2
     *         Z v
     *
     * This figure shows the reflection of ‘b’ by Z = −1 plane, rightly at Z = −3. If we simply flip the Z coordinate of
     * b, it’d just be reflection by Z = 0 plane, giving us the wrong image of b. For reflection by a plane not at the
     * origin, we’d have to notice that the image should be displaced twice the distance the plane is farther from the
     * origin i.e. −1 −2 = −3.
     */
    auto const mirrorEye = glm::vec3{eye.x, eye.y, -eye.z + (2 * mirrorZ)}; // Zreflect(20) = −20 + 2(−4) = −28
    auto const mirrorAt = mirrorEye + mirViewDir;
    auto const mirrorView2 = reflect * glm::lookAt(mirrorEye, mirrorAt, up);
    std::cout << "Mirrored 2\n" << mirrorView2 << "\n";
}
