<meta charset="utf-8" lang="en">

    **Gilbert-Johnson-Keerthi Algorithm**

The original _GJK_ algorithm was devised to determine the shortest distance between two convex point sets (polytopes, n-spheres, etc).  There are many variants it spawned [[6](#references)].  Many game programming resources assume GJK to be one of its boolean variant; to simply check for a collision, and not bothering with the distance between them.  Usually GJK terminates when there's an intersection; but it could also give the _shortest distance_ for _non-intersecting_ sets.  When _intersecting_, finding the _separation distance_ is usually done with another algorithm called _Expanding Polytope Algorithm_ (see below).

> "GJK works with anything convex -- flat or not; circles work too."

# Definitions

## Polytope
A shape in n dimensions made only of flat sides (facets) e.g. polygon is a 2-polytope, polyhedron is a 3-polytope.

## Polyhedron
A 3D shape made of only flat faces. Plural: polyhedra

## Simplex
A generalisation of the notion of a triangle to arbitrary dimensions.  A 0-simplex is a point, 1-simplex is a line segment, 2-simplex is a triangle, 3-simplex is tetrahedron.  Formally, for a fixed integer $k \geq 0$, a k-simplex is defined as the convex hull of $k + 1$ points in k-dimensional space [[6](#references)].  Intuitively, a simplex in n-dimension, is the minimal geometry in n-dimension that can be tightly tiled to fill up (tessellate) the space; for instance, a triangle is a 2D simplex, and tetrahedron is a 3D simplex [[5](#references)].  A simplex has the property that removing a point from its defining set reduces the dimensionality of the simplex by one [[7](#references)].  So a square isn't a simplex; removing a point, you still have a triangle (2D), while removing a point from a triangle gives a line, 1D.

# Minkowski Sum
There is no GJK without _Minkowski Sum_: an operation on two sets; not just geometric objects.  For each element in set $A$ perform an operation $\oplus$ on all elements in set $B$; essentially we'll end up with *nm* elements (where **A**'s size is `n` and **B**'s size is `m`) in the resulting set, $A \oplus B$.  Geometrically, in 2D, "adding" two convex polygons gives a third convex polygon that looks like one's boundary is traced with the other as the brush; this process is called convolution [[4](#references)].  It's fairly straight forward.  The operation is usually addition, but nothing stops us from doing subtraction, multiplication, etc.  GJK needs us to do subtraction.  Below is a sample Python program that gives out Minkowski sum with $-$ operator for two sets of points [[2](#references)]:

```python
import operator

a = ((1, 2), (3, 2), (3, 4))
b = ((2, 3), (4, 3), (4, 4), (2, 4))

for elem in a:
    print(tuple(map(lambda x: tuple(map(operator.sub, elem, x)), b)))
```

The Minkowski sum resulting by subtraction denoted $C = A − B$ would be called from now on as _Minkowski difference_; a made-up term since it's technically still Minkowski sum with operator $−$ or with $+$ operator but $−B$ i.e. $A + (−B)$.

The Minkowski sum itself goes by another obscure name: _Configuration Space Obstacle_.

# Principle
Given two convex polytopes A and B, their Minkowski difference would give back a convex polytop $C$ which would contain the origin if $A$ and $B$ are intersecting; if they're not, the origin will be outside $C$.  Intuitively, when $A$ and $B$ are intersecting, for the set of points in the overlapping region the Minkowski difference would all end up as the zero vector, $0$ i.e. $A - B$ would map all common points to the same point: origin.  To visualize it is in 2D, imagine two polygons $A$ and $B$ in the $(+, +)$ quadrant.  Each point in both figures can be seen as position vectors shooting from the origin to their respective locations.  Doing a Minkowski sum $A + B$ would give longer vectors pointing into the same quadrant as $A$ and $B$.  Now, $-B$ would point-reflect $B$ across the origin i.e. it'd be rotated 180°, ending up in $(−, −)$ quadrant.  Now $A - B$ may be a lot closer to the origin since we add positive and negative numbers --- $A$'s vectors try to push into $(+, +)$ while $B$'s would push in the opposite $(-, -)$ direction --- we have a very good chance that the result is positive _and_ negative.

Formally, we see the distance between any two sets $A$ and $B$ as [[3](#references)]

$$
Distance(A, B) = min{|X − Y| : X ∈ A, Y ∈ B} = min(|Z| : Z ∈ A − B)
$$

Hence the Minkowski difference, as opposed to sum, is used as it gives the set of all differences between its operands; not just between one pair but all pairs of points.  So even if they overlap just by exactly one point, we'd have the origin somewhere in $C$.  In addition, it can be proved that the separation distance (if overlapping) or shortest distance (if not), between $A$ and $B$ is attained by a point $P \in C$ that's closest to the origin and on the boundary of $C$; so $‖P‖$ is the shortest distance between the polytopes; this is sometimes called the _penetration depth_ [[5](#references)].  It is geometrically clear that only one such point exists and must lie on the boundary of $C$.  However, there can be many points in $A$ and $B$ such that they all map to $P$. This happens, for instance, when two disjoint convex polygons in 2D have their closest features as a pair of parallel edges, one from each polygon.

In essence, it's one of the quickest ways to find if the origin is within a (Minkowski difference) polytope; it does this by avoiding the explicit calculation of $A - B$.  It tries to build a simplex, within the polytope, to contain the origin.  If it succeeds in doing this, it returns `true`.  Beyond this, finding the penetration depth isn't part of GJK; it's usually done with EPA.  If it fails to enclose the origin in the tessellating simplex, it returns `false`, and it can be extended to find the shortest distance between the non-overlapping polytopes [[7](#references)]; this is done by finding the point in $C$ that's closest to the origin.  The distance between these two give the distance between the polytopes.

# Support Functions
Computing the entire set $A − B$ is one way to go about it.  This isn't really necessary and would be inefficient to compute.  For a given direction $D$, getting the point, in this set, that is farthest along $D$ is good enough.  So for each polytope we're working with, we need a support function which takes a (n-D) direction vector and returns a point in it that's farthest in that direction.  With this, constructing $A - B$ should be trivial.  One detail is that $max(D \cdot A_i) − max(D \cdot B_i)$ wouldn't get you the point farthest along $D$ but $max(D \cdot A_i) − max(−D \cdot Bi)$ will.  Visualize this with the 2D example given in the **§ Principle**; this is explained [[1](#references)] with two circles.

# Process
1. Get a point from $P_1$ (the Minkowski difference) in any direction, $D$ from the support function $S$
    + No need to be intelligent here since GJK is fairly quick at converging
    + Vector from centroid of $A$ to centroid of $B$ is one way to arrive at $D$
2. Put it in the simplex list, $L$
3. Update $D$ to be the vector from this point to the origin
4. Get a support point $P_i$ from $S$ with $D$
5. Check if the vector from origin to $P_i$ is similar to $D$ (i.e. the dot product > 0) 
    * What are we doing here?  Think of the situation where the line drawn from the previous support point to this one contains the origin. In that case, the dot product attains its maximum.  However, though the support point was obtained with the direction towards the origin, there's no need for the origin and $P_i$  to be not in the same direction from $P_{i - 1}$.  If so, then how deviant are they?  This is the question answered by the dot product
6. If they're pointing in totally opposite directions (dot product < 0), return _not intersecting_ and stop
    * dot product = 0 case isn't rejected since it might mean the origin is on a facet that has $P_i$ as one of its vertex
7. They're similar, put $P_i$ into the list
8. Test if the origin is within the simplex (whose count would always be n or n + 1 e.g. in 2D it'll always be a line or triangle), if yes, return _intersecting_ and stop
9. Origin not contained in simplex; find the Voronoi region[[9](#references)] the origin lies in w.r.t. the simplex
    + 2D: if it's a line, it can be in line segment region or the region formed by $P_i$ (total 2) but never $P_{i - 1}$ since that's where we started from and we'd never have ended in $P_i$ if this was the case [[1](#references)]
    + 2D: if it's a triangle, it can be on the Voronoi regions formed by the new edges formed by $P_i$ or the region formed by $P_i$ (total 3) [[1](#references)]
10. Find the direction from the simplex's closest feature to origin
    + 2D: from a point region, it's just a vector form point to origin; for a line, it's the line normal towards the origin
11. Remove any vertex in the simplex that's making facets in the opposite direction, away from the origin
    + 2D: this means removing $P_{i - 1}$ or $P_{i - 2}$ in the triangle case; nothing needs to be removed in the line case since we want to enclose the origin within a triangle (simplex in 2D) and a line doesn't have area to enclose it. So from the triangle we drop a point (and keep it as a line) to make a new triangle in the next iteration, while in the line case we just update the direction.
12. Go to **4**

Some materials [[11](#references)] [[6](#references)] [[8](#references)] note that when dealing with curved convex sets, the simplex keeps getting smaller and closer to the origin without ever actually enclosing (if intersecting) or terminating (if non-intersecting) --- it's like the polygon inscribing inside a circle.  In such cases a minimum tolerance distance, ε, is chosen to exit loops.

Once we know that the origin is not within the polytope $C$, we can find the feature closest to the last simplex formed and find the shortest distance to that simplex from the origin [[7](#references)].  This is the shortest distance between the polytopes $A$ and $B$.

# EPA
If the polytopes are intersecting, GJK can only tell us that but not the separation distance or separating vector between them.  Expanding Polytope Algorithm can give us just that.

1. Let the simplex from GJK be the polytope $P$
2. Find $P$'s closest facet to the origin
    + 2D: Find the edge closest to origin
3. Find the facet's normal direction $D$ that's pointing _away_ from the simplex
4. Get a point $P_i$ from the support function with $D$
5. If $P_i$ is an existing point i.e. one of the vertices forming the facet, we've the closest facet, stop
6. Expand the polytope with this point: insert this point between the ones forming the closest facet found
7. Go to **1**

Notice that the algorithm strongly _believes_ that it'll find a facet closest to the origin; it doesn't stop until it find ones.

Once we've the closest facet, we find the normal from the origin to it.  This is the separation normal between $A$ and $B$.  The barycentric coordinates of the point on the facet where the normal intersects the facet w.r.t the last deduced.  Now the support function outputs for the triangle’s vertices are taken as another triangle.  The barycentric coordinates are applied on the resultant triangle to get the contact point.

# References
1. [GJK tutorial video](https://mollyrocket.com/849) by Casey Muratori at Molly Rocket
2. [Python element-wise tuple operations like sum](http://stackoverflow.com/q/497885/183120)
3. §6.4.6, Game Physics, 2nd Edition by David H. Eberly
4. §12.4.5, Essential Mathematics for Games and Interactive Applications, 2nd Edition by Van Verth, Bishop
5. [Game Physics Series](http://allenchou.net/2013/12/game-physics-collision-detection-csos-support-functions/) by Allen Chou
6. [A Geometric Interpretation of the Boolean GJK Algorithm](https://arxiv.org/ftp/arxiv/papers/1505/1505.07873.pdf) by Jeff Linahan
7. §3.8, Real-Time Collision Detection, Christer Ericson
8. Collision Detection in Interactive 3D Environments by Gino van den Bergen
9. [GJK: Fast Convex Hull Collision Detection in 2D and 3D](http://www.cs.sjsu.edu/faculty/pollett/masters/Semesters/Spring12/josh/GJK.html)
10. Computing Distance, GDC 2010 Presentation by Erin Catto
11. [dyn4j series](http://www.dyn4j.org/2010/05/epa-expanding-polytope-algorithm/) of articles on collision detection (GJK and EPA)


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none'};</script>
