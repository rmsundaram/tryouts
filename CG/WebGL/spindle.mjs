"use strict";

// gl-matrix for vector and matrix types.
// https://glmatrix.net/
import {vec2, vec3, vec4, mat3, mat4, quat} from 'https://cdn.jsdelivr.net/npm/gl-matrix@3.4.3/+esm';
// TWGL (Tiny WebGL Helper) for WebGL boilerplate like getting context,
// compiling shaders, looking up and uploading attributes and uniforms.
// http://twgljs.org/
import * as twgl from 'https://cdn.jsdelivr.net/npm/twgl.js@5.5.4/+esm';

// TODO: Drop these and use requestAnimationFrame instead
let redraw = true, remake = true;
let modelParams = {
    segments: 32,
    radius: 0.25,
    height: 0.5,
    heightStub: 0.5,
};
let bezCurve = {
    p0: vec2.create(),
    p1: vec2.create(),
    p2: vec2.create(),
};
let worldToView = mat4.create();
let viewToClip = mat4.create();
let orientation = quat.create();

const FOV_Y = Math.PI / 4;

const CTRL_POINT_COLOR = "black";
const CTRL_POINT_RADIUS = 2;
const EDITABLE_CTRL_POINT_RADIUS = 5;
const EDITABLE_CTRL_POINT_COLOR = "red";

// Steel Blue: #4682b4
const MAIN_CANVAS_COLOR = vec4.fromValues(0.27450980,
                                          0.50980392,
                                          0.70588235,
                                          1.0);
// Lemon Chiffon
const BEZIER_CANVAS_COLOR = "#fffacd";

function render(gl, model, prgm) {
    twgl.resizeCanvasToDisplaySize(gl.canvas);
    gl.viewport(0, 0, gl.drawingBufferWidth, gl.drawingBufferHeight);
    gl.clear(gl.COLOR_BUFFER_BIT);
    gl.useProgram(prgm.program);
    let worldToClip = mat4.create();
    mat4.mul(worldToClip, viewToClip, worldToView);
    let modelToWorld = mat4.create();
    mat4.fromQuat(modelToWorld, orientation);
    let bezP1 = vec2.create();
    let bezP2 = vec2.create();
    vec2.sub(bezP1, bezCurve.p0, bezCurve.p1);
    vec2.sub(bezP2, bezCurve.p0, bezCurve.p2);
    twgl.setUniforms(prgm, {
        uModelToWorld: modelToWorld,
        uWorldToProj: worldToClip,
        uRadius: modelParams.radius,
        uHeight: modelParams.height,
        uHeightStub: modelParams.heightStub,
        uBezP1: bezP1,
        uBezP2: bezP2,
    });
    twgl.setBuffersAndAttributes(gl, prgm, model);
    twgl.drawBufferInfo(gl, model, gl.LINES);
    gl.useProgram(null);
}

function makeModel(gl) {
    // NOTE: Assumes segment count to be even.
    const SEGMENTS = modelParams.segments;
    const HALF_SEGMENTS = SEGMENTS / 2;
    // Top, Bottom, Stub
    const CENTRES = 3;
    // Stub adds one more to the spokes count.
    const VERTEX_COUNT = (SEGMENTS * 2) + (HALF_SEGMENTS + 1) + CENTRES;
    // Attributes
    //   Position (f32 * 3)
    //   Parameter (f32) (applies only to 'Exterior' vertices)
    //   Kind (i8)
    //     0 - Top Boundary
    //     1 - Bottom Boundary
    //     2 - Stub Boundary
    //     3 - Top Centre
    //     4 - Bottom Center
    //     5 - Stub Centre

    // Pack postition and parameter together (3 + 1).
    // Typed arrays are zero-initialized.
    let posParam = new Float32Array(VERTEX_COUNT * 4);
    let kind = new Int8Array(VERTEX_COUNT);
    // Initialize parameter and kind.
    for (let i = 0; i < SEGMENTS; ++i) {
        posParam[i*4+3] = i / SEGMENTS;
        posParam[(SEGMENTS+i)*4+3] = i / SEGMENTS;
        // Skip setting already zero-initialized kind for i.
        kind[SEGMENTS+i] = 1;
    }
    for (let i = 0; i < (HALF_SEGMENTS + 1); ++i) {
        posParam[(2*SEGMENTS+i)*4+3] = i / SEGMENTS;
        kind[2*SEGMENTS+i] = 2;
    }
    // Set location for centre vertices; rest get default location of 0
    kind[VERTEX_COUNT-3] = 3;
    kind[VERTEX_COUNT-2] = 4;
    kind[VERTEX_COUNT-1] = 5;

    // Generate indices for edges.
    // Count of edges (fractions and additions are for stub):
    //   Longitudes: 1½ SEGMENTS + 1
    //   Latitudes: 2½ SEGMENTS
    //   Spokes: 2 SEGMENTS + 2
    //   Central axis: 1
    // Total: 6 SEGMENTS + 4; double for 2 points/line.
    let indices = new Uint16Array(2 * (SEGMENTS * 6 + 4));
    let idx = 0;
    // Longitudes
    for (let i = 0; i < SEGMENTS; ++i, idx += 2) {
        indices[idx] = i;
        indices[idx+1] = i + SEGMENTS;
    }
    for (let i = 0; i < (HALF_SEGMENTS + 1); ++i, idx += 2) {
        indices[idx] = i + SEGMENTS;
        indices[idx+1] = i + SEGMENTS * 2;
    }
    // Latitudes
    for (let i = 0; i < SEGMENTS; ++i, idx += 4) {
        indices[idx] = i;
        indices[idx+1] = (i + 1) % SEGMENTS;
        indices[idx+2] = i + SEGMENTS;
        indices[idx+3] = ((i + 1) % SEGMENTS) + SEGMENTS;
    }
    for (let i = 0; i < HALF_SEGMENTS; ++i, idx += 2) {
        indices[idx] = i + (SEGMENTS * 2);
        indices[idx+1] = i + (SEGMENTS * 2) + 1;
    }
    // Spokes
    for (let i = 0; i < SEGMENTS; ++i, idx += 2) {
        indices[idx] = VERTEX_COUNT - 3;
        indices[idx+1] = i;
    }
    for (let i = 0; i < (HALF_SEGMENTS + 1); ++i, idx += 4) {
        indices[idx] = VERTEX_COUNT - 1;
        indices[idx+1] = i + (SEGMENTS * 2);
        indices[idx+2] = VERTEX_COUNT - 2;
        indices[idx+3] = ((HALF_SEGMENTS + i) % SEGMENTS) + SEGMENTS;
    }
    indices[idx++] = VERTEX_COUNT - 2;
    indices[idx++] = VERTEX_COUNT - 1;

    let arrays = {
        inPosition: { numComponents: 4, data: posParam },
        inKind: { numComponents: 1, data: kind, normalize: false },
        indices: { numComponents: 2, data: indices },
    };
    return twgl.createBufferInfoFromArrays(gl, arrays);
}

function makeProgram(gl) {
    return twgl.createProgramInfo(gl, ["shader-vs", "shader-fs"]);
}

function rotateTrackBall(e, gl, cursor) {
    let x = e.clientX;
    let y = e.clientY;
    let p2 = vec2.fromValues(x, y);
    let p1 = vec2.fromValues(cursor.lastX, cursor.lastY);
    cursor.lastX = x; cursor.lastY = y;

    let viewportDims = vec2.fromValues(gl.drawingBufferWidth,
                                       gl.drawingBufferHeight);
    let versor = getVersor(p1, p2, viewportDims, FOV_Y, worldToView);
    quat.mul(orientation, versor, orientation);
    quat.normalize(orientation, orientation);
    redraw = true;
}

function setupCamera(gl) {
    let eye = vec3.fromValues(2, 1, 2);
    let target = vec3.fromValues(0, 0, 0);
    let up = vec3.fromValues(0, 1, 0);
    mat4.lookAt(worldToView, eye, target, up);
    let aspectRatio = gl.drawingBufferWidth / gl.drawingBufferHeight;
    mat4.perspective(viewToClip, FOV_Y, aspectRatio, 1, 10);
}

function updateParam(slider, valueDiv, prop) {
    valueDiv.innerHTML = slider.value;
    modelParams[prop] = Number(slider.value);
    remake = true;
}

function makeBezierCurve(ctx) {
    let width = ctx.canvas.clientWidth;
    let height = ctx.canvas.clientHeight;
    vec2.set(bezCurve.p0, width * 0.5, height * 0.5);
    vec2.set(bezCurve.p1, width * 0.25, height * 0.25);
    vec2.set(bezCurve.p2, width * 0.75, height * 0.25);
}

function drawPoint(ctx, pos, radius, colour) {
    ctx.fillStyle = colour;
    ctx.beginPath();
    ctx.arc(pos[0], pos[1], radius, 0, 2 * Math.PI);
    ctx.closePath();
    ctx.fill();
}

function render2D(ctx) {
    let width = ctx.canvas.clientWidth;
    let height = ctx.canvas.clientHeight;
    ctx.clearRect(0, 0, width, height);
    ctx.fillStyle = BEZIER_CANVAS_COLOR;
    ctx.fillRect(0, 0, width, height);

    ctx.strokeStyle = "darkgray";
    ctx.beginPath();
    ctx.moveTo(bezCurve.p1[0], bezCurve.p1[1]);
    ctx.lineTo(bezCurve.p0[0], bezCurve.p0[1]);
    ctx.lineTo(bezCurve.p2[0], bezCurve.p2[1]);
    ctx.stroke();

    ctx.strokeStyle = "black";
    ctx.beginPath();
    ctx.moveTo(bezCurve.p0[0], bezCurve.p0[1]);
    ctx.bezierCurveTo(bezCurve.p1[0], bezCurve.p1[1],
                      bezCurve.p2[0], bezCurve.p2[1],
                      bezCurve.p0[0], bezCurve.p0[1]);
    ctx.stroke();

    drawPoint(ctx,
              bezCurve.p0,
              CTRL_POINT_RADIUS,
              CTRL_POINT_COLOR);
    drawPoint(ctx,
              bezCurve.p1,
              EDITABLE_CTRL_POINT_RADIUS,
              EDITABLE_CTRL_POINT_COLOR);
    drawPoint(ctx,
              bezCurve.p2,
              EDITABLE_CTRL_POINT_RADIUS,
              EDITABLE_CTRL_POINT_COLOR);
}

function start() {
    // WebGL canvas setup
    let canvasMain = document.getElementById("canvasMain");
    // https://twgljs.org/docs/module-twgl.html#.getContext
    let gl = twgl.getContext(canvasMain);
    if (!twgl.isWebGL2(gl)) {
        let divWebGL2 = document.getElementById("yesWebGL2");
        let divNoWebGL2 = document.getElementById("noWebGL2");
        divWebGL2.style.display = "none";
        divNoWebGL2.style.display = "block";
        console.error("Browser without WebGL 2 support.");
        return;
    }
    let canvasBezier = document.getElementById("canvasBezier");
    let ctx2d = canvasBezier.getContext("2d");

    // Eventing - Sliders
    // Segments
    let segmentCountSlider = document.getElementById("segmentCountSlider");
    let segmentCount = document.getElementById("segmentCount");
    segmentCountSlider.addEventListener("input", () => {
        updateParam(segmentCountSlider, segmentCount, "segments");
    });
    updateParam(segmentCountSlider, segmentCount, "segments");
    // Radius
    let radiusSlider = document.getElementById("radiusSlider");
    let radius = document.getElementById("radius");
    radiusSlider.addEventListener("input", () => {
        updateParam(radiusSlider, radius, "radius");
    });
    updateParam(radiusSlider, radius, "radius");
    // Height
    let heightMainSlider = document.getElementById("heightMainSlider");
    let heightMain = document.getElementById("heightMain");
    heightMainSlider.addEventListener("input", () => {
        updateParam(heightMainSlider, heightMain, "height");
    });
    updateParam(heightMainSlider, heightMain, "height");
    // Stub Height
    let heightStubSlider = document.getElementById("heightStubSlider");
    let heightStub = document.getElementById("heightStub");
    heightStubSlider.addEventListener("input", () => {
        updateParam(heightStubSlider, heightStub, "heightStub");
    });
    updateParam(heightStubSlider, heightStub, "heightStub");

    // Eventing - Main canvas
    let cursor = { lastX: 0, lastY: 0, down: false };
    window.addEventListener("mouseup", () => cursor.down = false);
    canvasMain.addEventListener("mousedown", e => {
        cursor.down = true;
        cursor.lastX = e.clientX;
        cursor.lastY = e.clientY;
    });
    window.addEventListener("mousemove", (e) => {
        if (cursor.down) {
            rotateTrackBall(e, gl, cursor);
        }
    });

    // Eventing - Bezier canvas
    let selPt = undefined;
    canvasBezier.addEventListener("mousedown", (e) => {
        selPt = hitTest(getCursorPosition(e, canvasBezier));
    });
    canvasBezier.addEventListener("mousemove", (e) => {
        if (selPt)
            dragCtrlPt(getCursorPosition(e, canvasBezier), selPt);
    });
    canvasBezier.addEventListener("mouseup", (e) => {
        selPt = undefined;
    });

    gl.clearColor(MAIN_CANVAS_COLOR[0],
                  MAIN_CANVAS_COLOR[1],
                  MAIN_CANVAS_COLOR[2],
                  MAIN_CANVAS_COLOR[3]);
    let model = makeModel(gl);
    makeBezierCurve(ctx2d);
    setupCamera(gl);
    let prgm = makeProgram(gl);
    let loop = function(/*time*/) {
        if (remake)
            model = makeModel(gl);
        if (remake || redraw) {
            render2D(ctx2d);
            render(gl, model, prgm);
            remake = false;
            redraw = false;
        }
        requestAnimationFrame(loop);
    };
    requestAnimationFrame(loop);
}

// http://stackoverflow.com/a/18053642
function getCursorPosition(event, element) {
    let rect = element.getBoundingClientRect();
    let x = event.clientX - rect.left;
    let y = event.clientY - rect.top;
    return vec2.fromValues(x, y);
}

function hitTest(pt) {
    let d = vec2.sqrDist(bezCurve.p1, pt);
    if (d <= (EDITABLE_CTRL_POINT_RADIUS * EDITABLE_CTRL_POINT_RADIUS))
        return "p1";
    d = vec2.sqrDist(bezCurve.p2, pt);
    if (d <= (EDITABLE_CTRL_POINT_RADIUS * EDITABLE_CTRL_POINT_RADIUS))
        return "p2";
    return undefined;
}

function dragCtrlPt(pt, ctrlPtName) {
    bezCurve[ctrlPtName] = pt;
    redraw = true;
    remake = true;
}

function mapScreenToView(x, y, viewportWidth, viewportHeight, fovY) {
    let ar = viewportWidth / viewportHeight;
    var x_ = ((2 * x / viewportWidth) - 1) * ar;
    var y_ = (-2 * y / viewportHeight) + 1;
    var d = 1 / Math.tan(fovY / 2);
    return vec3.fromValues(x_, y_, -d);
}

function getMidpoint(v1, v2) {
    var m = vec2.create();
    vec2.add(m, v1, v2);
    vec2.scale(m, m, 0.5);
    return m;
}

// cheaper alternative to mat4.invert when x is a rigid-body transform
function invertRigidXform(x) {
    var x_ = mat4.create();
    mat4.transpose(x_, x);
    x_[3] = 0, x_[7] = 0, x_[11] = 0;
    var r_ = mat3.create();
    mat3.fromMat4(r_, x_);
    var t_ = vec3.fromValues(x[12], x[13], x[14]);
    vec3.transformMat3(t_, t_, r_);
    x_[12] = -t_[0], x_[13] = -t_[1], x_[14] = -t_[2];
    return x_;
}

// Source: https://bitbucket.org/rmsundaram/tryouts/dev/src/CG/WebGL/utils.mjs
//
// input: screen space points p1 and p2, viewport dimensions, vertical FoV,
//        view to world transform
// output: versor (quaternion) represeting the rotation of a model from vector
//         w2 to w1, which are model space vectors calculated based on p2 & p1.
//
// Since p1 and p2 are screen space points, transforming them to view space and
// forming position vectors w.r.t. view space origin gives v1 and v2. Rotating
// the camera from v1 to v2 has the same effect visually as rotating the model
// from v2 to v1 i.e. w2 to w1 in world space. See figure below.
function getVersor(p1, p2, viewportDims, fovY, Mw2v) {
    // We need a quaternion that rotates v2 to v1, this will be multiplied to
    // the model's orientation quaternion to get the model's Mm2w. The
    // orientation quaternion, when converted to a matrix, gives the model's
    // basis in terms of the world's, Mm2w. Hence both the vectors v1 and v2
    // used to form the quaternion need also be in world space.

    // Just using the screen points as-is will not give the expected result
    // since we need the camera to always rotate around an axis that's
    // orthogonal to the eye vector i.e. we need the rotation axis to be either
    // view-up, view-right or their linear combination (in world space); the
    // two vectors calculated should be orthogonal to the plane spanned by
    // view-up and view-right; when used as-is it won't be so. Translate the
    // screen points so that the line they form have the screen centre as its
    // midpoint.
    var m = getMidpoint(p1, p2);
    let viewportCentre = vec2.fromValues(viewportDims[0] / 2,
                                         viewportDims[1] / 2);
    vec2.sub(m, viewportCentre, m);
    vec2.add(p1, p1, m);
    vec2.add(p2, p2, m);
    var v1 = mapScreenToView(p1[0], p1[1],
                             viewportDims[0], viewportDims[1],
                             fovY);
    var v2 = mapScreenToView(p2[0], p2[1],
                             viewportDims[0], viewportDims[1],
                             fovY);
    var Mv2w = invertRigidXform(Mw2v);

    /*
      Although glMatrix has a vec3.transformMat4, the w component is implicitly
      assigned 1 i.e. treated as a point, while v1 and v2 are actually vectors.
      Within a space, treating points as position vectors is common; however,
      when transforming to a different space, the distinction is important. Two
      points a1 and a2 in space A, transformed, as points, into b1 and b2 of
      space B and considering their position vectors in that system would
      reveal that they point in very different directions than the vectors a1
      and a2 would. This discrepancy is due to the origin of the systems having
      a bearing in deciding the direction of the position vectors. Transforming
      vectors a1 and a2 with w = 0 to system B would give vectors pointing in
      the same direction but represented in B's terms. This is illustrated in
      the Position_Vectors.jpg calculation. Hence manually convert to maintain
      v1 and v2 as vectors.
    */
    var tv1 = vec4.fromValues(v1[0], v1[1], v1[2], 0);
    var tv2 = vec4.fromValues(v2[0], v2[1], v2[2], 0);
    vec4.transformMat4(tv1, tv1, Mv2w);
    vec4.transformMat4(tv2, tv2, Mv2w);
    v1 = vec3.fromValues(tv1[0], tv1[1], tv1[2]);
    v2 = vec3.fromValues(tv2[0], tv2[1], tv2[2]);

    /*
      Now we've the two view space vectors in terms of world space; this is how
      the situtation looks: (v1 and v2 are vectors so their position in the
      figure doesn't matter)

            transformed v1 \   /  v2 in world space
                             +  -> world origin
                         -*-----*-  viewing plane, where * = click point
               original v1 \   /  v2 in view space
                             +   -> view origin

      Rotating from * to * (i.e. p1 to p2) w.r.t. world origin is the same as
      rotating from v2 to v1 w.r.t. world origin which is visually the same as
      rotating the view's basis from v1 to v2 w.r.t. view origin.
     */

    vec3.scale(v2, v2, 2);
    vec3.sub(v2, v2, v1);

    vec3.normalize(v1, v1);
    vec3.normalize(v2, v2);
    var versor = quat.create();
    quat.rotationTo(versor, v2, v1);
    return versor;
}

window.onload = start;
