1. Pandian suggested an optimisation: why shoot the aux rays when the primary didn't hit the endpoint?  Doing this optimisation is possible in shootRays() but we've to check if the anglePoint is from an endpoint.

2. Another possible optimisation based on the 1D projection done for shooting only the necessary auxiliary rays may be possible: only consider the frontal portion of every polygon when deducing the angle points and blocking edges.

3. Finding the frontal portion of a *convex* polygon is pretty simple: it's the set of edges falling between the vertices where the visibility beyond corners are needed; there should be only two such vertices.  Between these vertices, on one side is the set of edges occluded from the viewer by the set of edges on the other side visible to the viewer.
