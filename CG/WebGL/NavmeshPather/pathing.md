# Basic Pathing Algorithms

A digest of Amit's introduction to A*[\[1\]](#references) and Artificial Intelligence for Games[\[2\]](#references).  Some points to remember before delving into all these algorithms: 

1. All of these are some form of graph traversal algorithms, more specifically BFS derivatives
2. Irrespective of how clever the algorithm is devised, _the lesser the number of nodes in the graph, the faster_ any algorithm would arrive at the path.
3. Since they are breadth first, as we go along searching, we wouldn't be following the final path.  Different nodes are picked up and examined with no apparent linearity in the nodes examined.

## Breadth First Search

* Simple BFS can be used for path finding.
    + Most other pathing algorithm are based off this graph traversal algorithm.
    + Various other usages like distance maps, procedural map generation, flow field path finding, etc.
* Uses a queue (using a stack instead would make it DFS).
* Searches evenly in all directions until the goal node is met.  Imagine a set of concentric circles.
* In graphics terms, it's just the _flood fill_ algorithm.  It expands the "frontier" in all directions evenly.
* Once the goal is found, retracing back to the start using `came_from` gives path.


### Algorithm

```python
frontier = Queue()
frontier.put(source)
came_from = { }
while not frontier.empty():
    current = frontier.get()
    if current == dest:
        return came_from
    for next in graph.neighbours(current):
        if next not in came_from:
            frontier.put(next)
            came_from[next] = current
```


# Dijkstra's Path Finding

* Let's us prioritize which paths to explore using movement costs, instead of searching uniformly in all directions.
* Also called _Uniform Cost Search_.  Imagine contour lines, with bulges in a particular direction.
* The idea is to have a priority queue: when visiting nodes, pick the one with least cost thus far.
* This is still BFS with priority queue and priority (cost-so-far) updates while searching.
* When one would keep the graph static throughout the query phase, Dijkstra  might be useful for pre-calculating paths between all nodes.
    + Even in this case books recommend A* [\[2\]](#references).

### Algorithm

        Push source into a priority queue with cost-so-far as 0    // in this queue, absent nodes have ∞ cost
        Get next node with least cost-so-far
            If this node is the destination, we're done
            For each neighbour of this node
                if cost(neighbour) > (cost(this) + cost(edge))
                    Update cost to neighbour with the new one
                    Update reached-via to this node
                    Update queue to account for neighbour's new priority


## Greedy BEST First Search

* The frontier expands in all directions with both BFS and Dijkstra.  Useful if you're finding path from one point to many/all points, but redundant for point-to-point searches.
* We want to expand only in the direction of the destination.  Define a heuristic function, `h(n)`, which guesses how close n is to destination.
* Dijkstra accounts with actual cost from the source, while this uses the estimated cost to the destination.
* This is just BFS with priority queue but the priority data is no cost-so-far but the estimated cost.
* This works well only for trivial paths.  For non-trivial paths, this gives poor results.

### Algorithm

```python
frontier = PriorityQueue()
frontier.put(source, 0)
came_from = { }
while not frontier.empty():
    current = frontier.get()
    if current == dest:
        return came_from
    for next in graph.neighbours(current):
        if next not in came_from:
            frontier.put(next, heuristic(next, dest))
            came_from[next] = current
return None
```


# A*

* Uses the good parts of both Dijkstra and GBFS. 
    + BFS and Dijkstra are guaranteed to find the shortest path GBFS is not.
    + Like Dijkstra it finds the optimal path, while like GBFS it does it fast(er than Dijkstra).
    + A* is guaranteed to find the shortest _if the heuristic is always shorter than the true distance_.
    + As the heuristic becomes longer it converges to GBFS; if it becomes shorter it converges to Dijkstra.
    + Refer [\[3\]](#references) for a detailed article on the role of heuristics; it also gives a list of commonly employed heuristic functions in grid maps.
* Employs both actual distance from start and estimated distance to goal as priority in the queue.
* Note that this algorithm is as good as the `h(n)` we choose.  A bad heuristic function can lead to poor results.
* Euclidean distance is a decent choice for a heuristic function in most situations.
* Unlike Dijkstra, A* can find better routes to a node in the _Closed_ list since it leans on estimates.
    + This causes a knock-on problem: how to update nodes found through this and their connections?
    + A simple solution: update its data and move it to _Open_.
    + This forces the algorithm to re-examine and update the node and propagate the change to its connections.
    + Due to this problem, termination is slightly involved: run until the node in _Open_ with least cost-so-far
      exceeds goal's cost-so-far. But this makes A* become Dijkstra, so this isn't done.

## Algorithm

```python
frontier = PriorityQueue()
frontier.put(source, 0)
came_from = { }
cost_so_far = { source: 0 }

while not frontier.empty():
    current = frontier.get()
    if current == goal:
        return came_from
    for n in graph.neighbours(current):
        cost_thru_this = cost_so_far[current] + graph.cost(current, n)
        # note: comparison is always between the actual costs only; just
        # the node's priority of getting picked up depends on the estimate
        if n not in cost_so_far or cost_so_far[n] > cost_thru_this:
            cost_so_far[n] = cost_thru_this
            came_from[n] = current
            frontier.put(n, cost_thru_this + estimate(n, goal))
```

Another option is _Hershberger’s Algorithm_: quite complex but yielding highly optimal results.  A comment from [an SO answer](https://stackoverflow.com/q/33464917/33465390#comment106694305_33465390)

> If your environment can be described as a polygonal decomposition, you can do A* on the faces/edges of the polygons themselves, either using a midpoint heuristic or (if you demand absolute optimality at the cost of hugely increased complexity) _Hershberger's algorithm_ described in _"An Optimal Algorithm for Euclidean Shortest Paths in the Plane"_.

# 2D Grid

[Theta*][] algorithm is gives near-optimal results on a 2D grid to find Euclidean shortest paths and has comparable run times as A* and differs from it [only slightly][theta*-wiki].  Theta* [interleaves searching and smoothing][theta-smoothing] while smoothing is generally a separate step done following BFS, A*, Dijkstra, etc.

[Theta*]: https://web.archive.org/web/20190717211246/aigamedev.com/open/tutorials/theta-star-any-angle-paths/
[theta*-wiki]: https://en.wikipedia.org/wiki/Theta*
[theta-smoothing]: https://news.movel.ai/theta-star/

# References

1. [Introduction to A*](http://www.redblobgames.com/pathfinding/a-star/introduction.html) by Amit Patel
2. §4.2 Dijkstra, _Artificial Intelligence for Games_, 2nd edition by Ian Millington and John Funge
3. [A* Heuristics](http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html) by Amit Patel
