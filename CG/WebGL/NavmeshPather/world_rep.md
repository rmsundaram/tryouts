% World Representation

Three important aspects of this:

1. Quantization and Localization
2. Generation
3. Validity

Tile-based
==========

The map is cut into a grid (of same-sized squares).  Leads to lot of nodes but at the cost of better granularity, faster quantization and localization and easier generation (so simple that it can be skipped).

Navmesh
=======

Tessellating the map into polygons (triangles are easier) and using them as nodes and the shared edges as connections.  For 2D most people resort to _poly2tri_, a library based on a line-sweep algorithm introduced by a paper.  There are multiple version of this library on the internet; also multiple papers --- the original and improvements on it --- that are line-sweep based.

Libraries
---------

The Löve forum has [a discussion](https://love2d.org/forums/viewtopic.php?t=81155&start=10) on triangulation with constraints (holes).  [This](https://stackoverflow.com/q/16042940) and its [linked StackOverflow post](https://stackoverflow.com/q/406301), is an aggregation of libraries with review from people who used it.  The Virtual Terrain project also has a list of [Triangulation of Simple Polygons](http://vterrain.org/Implementation/Libs/triangulate.html) that's quite extensive.  A loose list would be

1. [poly2tri](https://github.com/greenm01/poly2tri), a C++ implementation of [V.Domiter's Sweep-line algorithm for constrained Delaunay triangulation](http://www.tandfonline.com/doi/abs/10.1080/13658810701492241), 2006 with BSD license
    * A slightly updated fork: [jhasse/poly2tri](https://github.com/jhasse/poly2tri)
    * JS implementation with more fixes: [r3mi/poly2tri.js](https://github.com/r3mi/poly2tri.js)
    * Lua implementation [lua-tri2poly](https://github.com/TannerRogalsky/lua-poly2tri) based on a older paper: [Liang's algorithm](http://sites-final.uclouvain.be/mema/Poly2Tri/), 2005
2. [PolyPartition](https://github.com/ivanfratric/polypartition/), a tiny polygon partitioning and triangulating library with implementation of different methods
    1. Ear clipping
    2. Optimal triangulation in terms of edge length using dynamic programming algorithm
    3. Triangulation by partition into monotone polygons
    4. Convex partition using Hertel-Mehlhorn algorithm
    5. Optimal convex partition using dynamic programming algorithm by Keil and Snoeyink
3. [Triangles](http://www.cs.cmu.edu/~quake/triangle.html), a 2D mesh generator and Delaunay Triangular by Jonathan Shewchuk which does not have a permissive license
