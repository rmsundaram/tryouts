// Binary heap based on Eloquent JavaScript, 1st Edition
// http://eloquentjavascript.net/1st_edition/appendix2.html
// Some differences w.r.t the book:
// 1. It sports a reseat() needed for on-the-fly element priority change. To
//    know the index on element in its internal array, it allows the client to
//    pass an updater function to be informed of an element’s position updates.
// 2. This implements a binary comparator in contrast to a unary score function
//    allowing it to work both as min and max heap.
// This is discussed as _Index priority queue_ under §2.4 Priority Queues in
// Robert Sedgewick’s Algorithms, 4th Edition

// Keeping the index stored somewhere defeats the purpose of the data structure
// since it means O(n) pop, as all elements following root needs to have its
// index updated.

// //C++/DS/min_heap.cpp implements a recursive solution while most browsers
// are yet to have tail call optimization, so an iterative solution.

// comparator(x, y) should return true if x should be ordered before y; if
// undefined the default is the lesser-than comparator.
// updater(obj, index) is an optional callback function which will be called as
// an element changes position within the array. Useful when client needs to
// update priority of elements dynamically.
export class BinHeap {
    constructor(comparator, notifier) {
        this.data = [];
        this.comp = comparator || function(a, b) { return a < b; };
        this.notify = notifier || function() { };
    }

    size() {
        return this.data.length;
    }

    push(x) {
        let elemIdx = this.data.push(x) - 1;
        // notify new index if bubble up was a no-op
        if (!this.bubbleUp(this.data.length - 1))
            this.notify(x, elemIdx);
    }

    pop() {
        let root = this.data[0];
        let leaf = this.data.pop();
        if (this.data.length > 0) {
            this.data[0] = leaf;
            // notify new index if sink down was a no-op
            if (!this.sinkDown(0))
                this.notify(leaf, 0);
        }
        return root;
    }

    // peek without popping
    top() {
        return this.data[0];
    }

    // If an element’s priority changed, use this to reseat it. This returns
    // true if a reseat happened.
    reseat(id) {
        if (!this.bubbleUp(id))
            return this.sinkDown(id);
        return true;
    }

    child(id) {
        return id * 2 + 1;
    }

    parent(id) {
        return Math.floor((id - 1) * 0.5);
    }

    // returns new index if it bubbled up or undefined otherwise
    bubbleUp(id) {
        let newIndex = undefined;
        let element = this.data[id];
        while(id > 0) {
            let pId = this.parent(id);
            let parent = this.data[pId];
            if (this.comp(parent, element))
                break;
            this.data[id] = parent;
            this.data[pId] = element;
            this.notify(parent, id);
            newIndex = id = pId;
        }
        if (newIndex)
            this.notify(element, newIndex);
        return newIndex;
    }

    // returns new index if it sank down or undefined otherwise
    sinkDown(idx) {
        let newIndex = undefined;
        let n = this.data.length;
        let element = this.data[idx];
        let left = this.child(idx);
        while (left < n) {
            let right = left + 1;
            let chosen =
                ((right < n) && this.comp(this.data[right], this.data[left])) ?
                right : left;
            let child = this.data[chosen];
            if (this.comp(element, child))
                break;
            this.data[idx] = child;
            this.data[chosen] = element;
            this.notify(child, idx);
            newIndex = idx = chosen;
            left = this.child(idx);
        }
        if (newIndex)
            this.notify(element, newIndex);
        return newIndex;
    }
}
