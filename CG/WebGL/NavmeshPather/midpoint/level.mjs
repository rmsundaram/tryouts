/* TODO
 * 1. Steering for smoother turns with radius
 * 2. A better cost function for edges
 * 3. Test A* handling of unreachable nodes
 * 4. Explore variations of A*
 *
 * To avoid blatant bad paths, the graph needs more nodes i.e. more
 * triangles. However, when we add steiner points, something else happens:
 * portal ends are no longer due to real obstacles (polygon coners); so when a
 * path is slightly off as the truly shortest path doesn’t having enough node
 * coverage, the returned path isn’t a straight line (you get \_ insted of \).
 * Smoothing doesn’t help here since the portal really ends prematurely and a
 * false turn seems unavoidable. However, the right way to deal with this is to
 * add more steiner points so that there’s enough node coverage that the path
 * isn’t slightly off in the first place.
 * 5. Automate steiner points positioning; keep subdiving the larger triangles.
 * Other possible approaches
 * 6. Do string pulling beyond nodes in the path would still be walkable.
 * 7. Have two triangulations of the level, with and without steiner points. Use
 * the former for path finding, and latter for path smoothing.
 *
 * ASSUMPTIONS
 * 1. Gaps between polygons are at least as wide as a character’s diameter
 * 2. When positioning a character too close to a building, the radius offset
 *    from the closest wall will be taken care of by some other code; possibly
 *    with the aid of a spatial data structure.
 * 3. Local collision avoidance and steering will iron out corner cases.
 * 4. All characters are of the same size (radius).
 */

import {vec2, vec3, mat2} from "../../gl-matrix.mjs";
import * as AStar from "./a-star.mjs";

let debugRender = false;
const wallColour = "105, 105, 105";
let levelData = [
    {
        coords: [70, 50, 190, 70, 170, 140, 100, 130],
        colour: wallColour,
        stroke: 1,
        fill: true
    },
    {
        coords: [230, 50, 350, 70, 330, 140, 305, 90],
        colour: wallColour,
        stroke: 1,
        fill: true
    },
    {
        coords: [475, 56, 616, 56, 616, 360, 475, 360],
        colour: wallColour,
        stroke: 1,
        fill: true
    },
    {
        coords: [188.57143, 381.89539,
                 167.824635, 304.467285,
                 328.25502, 261.480095,
                 268.205575, 321.529535,
                 330.053215, 357.237285,
                 207.155635, 428.19224,
                 226.618645, 355.55529],
        colour: wallColour,
        stroke: 1,
        fill: true
    },
    {
        coords: [100, 200, 120, 250, 60, 300],
        colour: wallColour,
        stroke: 1,
        fill: true
    }
];
let mouseTri = -1;
let src = { pt: vec2.create(), valid: false };
let dst = { pt: vec2.create(), valid: false };
let plan;
let debugVars = { cost: 0, lastPt: undefined, corners: [] };
// TODO: compare efficiency between heuristics before and after steiner points
const heuristics = [ nullHeuristic, l1NormHeuristic, l2NormHeuristic ];
let curHeuristic = l2NormHeuristic;
let smoothing = { None: 0, Funnel: 1, current: 1 };
const charRadius = 5, charRadiusSqr = charRadius * charRadius;
// this is generally used at multiple places, but there are local epsilons too
const globalEps = 0.0001;
let charSmoothing = true;
let INVALID_DATA = 0xffff;

function drawPolygon(ctx, p) {
    ctx.save();
    ctx.beginPath();
    ctx.moveTo(p.coords[0], p.coords[1]);
    for (let i = 2, n = p.coords.length; i < n; i += 2)
        ctx.lineTo(p.coords[i], p.coords[i + 1]);
    ctx.closePath();
    if (p.fill) {
        ctx.fillStyle = "rgba(" + p.colour + ", 0.5)";
        ctx.fill();
    }
    if (p.stroke) {
        ctx.lineWidth = p.stroke;
        ctx.strokeStyle = "rgb(" + p.colour + ")";
        ctx.stroke();
    }
    ctx.restore();
}

function drawExtrudedPolygon(ctx, p) {
    ctx.save();
    ctx.beginPath();
    ctx.moveTo(p.extrudedCoords[0][0], p.extrudedCoords[0][1]);
    for (let i = 0, n = p.extrudedCoords.length; i < n; ++i)
        ctx.lineTo(p.extrudedCoords[i][0], p.extrudedCoords[i][1]);
    ctx.closePath();
    ctx.lineWidth = 0.5;
    ctx.strokeStyle = "rgba(0, 0, 0, 0.5)";
    ctx.stroke();
    ctx.restore();
}

function drawTriangle(ctx, tri, strokeWidth, strokeCol, fillCol) {
    // set defaults
    strokeWidth = strokeWidth || 0.5;
    strokeCol = strokeCol || "rgba(0, 0, 0, 0.5)";

    let coords = tri.points_;
    ctx.save();
    ctx.beginPath();
    ctx.moveTo(coords[0].x, coords[0].y);
    ctx.lineTo(coords[1].x, coords[1].y);
    ctx.lineTo(coords[2].x, coords[2].y);
    ctx.closePath();
    if (fillCol) {
        ctx.fillStyle = fillCol;
        ctx.fill();
    }
    ctx.lineWidth = strokeWidth;
    ctx.strokeStyle = strokeCol;
    ctx.stroke();
    ctx.restore();
}

function drawCircle(ctx, x, y, radius, strokeWidth, strokeCol, fillCol) {
    strokeWidth = strokeWidth || 0.5;
    strokeCol = strokeCol || "#1d67d6";
    fillCol = fillCol || "rgba(29, 103, 215, 0.25)";

    ctx.save();
    ctx.beginPath();
    ctx.arc(x, y, radius, 0, 2 * Math.PI);
    ctx.closePath();
    ctx.fillStyle = fillCol;
    ctx.fill();
    ctx.lineWidth = strokeWidth;
    ctx.strokeStyle = strokeCol;
    ctx.stroke();
    ctx.restore();
}

function drawCross(ctx, x, y, halfLength, strokeWidth, strokeCol) {
    strokeWidth = strokeWidth || 3;
    strokeCol = strokeCol || "#f00";

    const diag1DProj = 0.707106;
    ctx.save();
    ctx.beginPath();
    ctx.moveTo(x - diag1DProj * halfLength, y - diag1DProj * halfLength);
    ctx.lineTo(x + diag1DProj * halfLength, y + diag1DProj * halfLength);
    ctx.moveTo(x + diag1DProj * halfLength, y - diag1DProj * halfLength);
    ctx.lineTo(x - diag1DProj * halfLength, y + diag1DProj * halfLength);
    ctx.lineWidth = strokeWidth;
    ctx.strokeStyle = strokeCol;
    ctx.stroke();
    ctx.restore();
}

function drawLine(ctx, pt1, pt2, strokeWidth, strokeCol) {
    strokeWidth = strokeWidth || 4;
    strokeCol = strokeCol || "#000";

    ctx.save();
    ctx.lineWidth = strokeWidth;
    ctx.strokeStyle = strokeCol;
    ctx.beginPath();
    ctx.moveTo(pt1[0], pt1[1]);
    ctx.lineTo(pt2[0], pt2[1]);
    ctx.stroke();
    ctx.restore();
}

function drawCurvedPath(ctx, plan, n) {
    // append dst for not special casing the last line in the loop
    let turnPts = plan.turnPts;
    turnPts.push(dst);
    ++n;
    // draw the first leg
    ctx.save();
    ctx.lineWidth = 1;
    ctx.strokeStyle = "#000";
    ctx.beginPath();
    ctx.moveTo(src.pt[0], src.pt[1]);
    // draw the curved turns
    for (let i = 0; i < n; ++i) {
        let p = turnPts[i];
        // turning point but not around a polygon corner; curve unneeded
        if (!p.curved)
            ctx.lineTo(p.pt[0], p.pt[1]);
        else {
            let a1 = p.arcPts[0], a2 = p.arcPts[1];
            let c1 = p.ctrlPts[0], c2 = p.ctrlPts[1];
            ctx.lineTo(a1[0], a1[1]);
            ctx.bezierCurveTo(c1[0], c1[1], c2[0], c2[1], a2[0], a2[1]);
        }
    }
    ctx.stroke();
    ctx.restore();
    // remove dst added for convenience
    turnPts.pop();
}

function drawPath(ctx) {
    let n = plan.turnPts && plan.turnPts.length;
    if (n) {
        if ((smoothing.current === smoothing.Funnel) && charSmoothing)
            drawCurvedPath(ctx, plan, n);
        else {
            let turnPts = plan.turnPts;
            ctx.save();
            ctx.lineWidth = 1;
            ctx.strokeStyle = "#000";
            ctx.beginPath();
            ctx.moveTo(src.pt[0], src.pt[1]);
            for (let i = 0; i < n; ++i) {
                let tp = turnPts[i].pt;
                ctx.lineTo(tp[0], tp[1]);
            }
            ctx.lineTo(dst.pt[0], dst.pt[1]);
            ctx.stroke();
            ctx.restore();
        }
    }
    else
        drawLine(ctx, src.pt, dst.pt, 1);
}

function render(ctx, levelData, triangleData) {
    //clear the screen
    ctx.fillStyle = "White";
    ctx.fillRect(0, 0, 640, 480);

    // draw the builings
    let nPolygons = levelData.length;
    for (let i = 0; i < nPolygons; ++i)
        drawPolygon(ctx, levelData[i]);

    if (plan)
        drawPath(ctx);

    if (src.valid)
        drawCircle(ctx, src.pt[0], src.pt[1],
                   charRadius,
                   1, "#32cd32", "#32cd32");
    if (dst.valid)
        drawCross(ctx, dst.pt[0], dst.pt[1], 6);

    // debug code follows
    if (!debugRender) return;
    if (charSmoothing)
        for (let i = 0; i < nPolygons; ++i)
            drawExtrudedPolygon(ctx, levelData[i]);

    let nTri = triangleData.length;
    for (let i = 0; i < nTri; ++i)
        drawTriangle(ctx, triangleData[i]);

    if (triangleData.g) {
        let nodes = triangleData.g.nodes;
        for (let i = 0; i < nodes.length; ++i) {
            let pt = nodes[i].pos;
            drawCircle(ctx, pt[0], pt[1], 3, 1, "#000000");
        }

        let links = triangleData.g.links;
        ctx.setLineDash([5, 8]);
        for (let i = 0; i < links.length; ++i) {
            let from = nodes[links[i].e1].pos;
            let to = nodes[links[i].e2].pos;
            drawLine(ctx, from, to, 0.5, "#c0c0c0");
        }
        ctx.setLineDash([]);
    }

    if (plan) {
        // draw portal end points and turning point circles
        if ((smoothing.current === smoothing.Funnel) && debugVars.portal) {
            for (let i = 0; i < debugVars.portal.left.length - 1; ++i) {
                let portalLeft = debugVars.portal.left[i];
                let portalRight = debugVars.portal.right[i];
                let ptL = (charSmoothing && portalLeft.extrudedPt) ||
                          portalLeft;
                let ptR = (charSmoothing && portalRight.extrudedPt) ||
                          portalRight;
                drawCircle(ctx, ptL[0], ptL[1], 3, 1, "#ffa500", "#ffa500");
                drawCircle(ctx, ptR[0], ptR[1], 3, 1, "#1e90ff", "#1e90ff");
            }
            for (let i = 0; i < debugVars.corners.length; ++i) {
                let c = debugVars.corners[i];
                drawCircle(ctx, c[0], c[1], charRadius, 1, "#ff0000");
            }
        }
    }

    // highlight interesting triangles
    if (mouseTri != -1) {
        let thisTri = triangleData[mouseTri];
        drawTriangle(ctx,
                     thisTri,
                     0.3,
                     "rgb(0, 255, 255)",
                     "rgba(0, 255, 255, 0.4)");
        let nodeIds = thisTri.nodeIds;
        for (let i = 0; i < nodeIds.length; ++i) {
            let pt = triangleData.g.nodes[nodeIds[i]].pos;
            drawCircle(ctx, pt[0], pt[1], 4, 1, "#ff00ff");
        }
        for (let i = 0; i < 3; ++i) {
            let neighbour = thisTri.neighbors_[i];
            // neighbours may be undefined (in case of triangles sharing
            // an edge with the sweep boundary) or exterior (clipped)
            if (isNodeValid(neighbour))
                drawTriangle(ctx,
                             neighbour,
                             0.3,
                             "rgb(0, 0, 255)",
                             "rgba(0, 0, 255, 0.15)");
        }
    }
}

function init2DCanvas(canvas) {
    let ctx;
    try {
        ctx = canvas.getContext("2d");
    }
    catch (e) {
        alert("Failed to get a canvas. Your browser may not support it");
    }
    return ctx;
}

function flipDebugView() {
    debugRender = !debugRender;
}

// poly2tri leaves neighbour nodes undefined or defined but
// exterior(clipped) to the tesselated figure
function isNodeValid(n) {
    return n && n.interior_;
}

// map neighbour ID to shared edge and return its end points
function getSharedEdgePoints(node, neighbourId) {
    let neighbourEdge2PtIds = [ 1, 2, 2, 0, 0, 1];
    let ptId0 = neighbourEdge2PtIds[neighbourId * 2];
    let ptId1 = neighbourEdge2PtIds[neighbourId * 2 + 1];

    return [ node.points_[ptId0], node.points_[ptId1] ];
}

// return edge end points, given the edge ID
function getEdgePoints(node, edgeId) {
    let edge2PtIds = [0, 1, 1, 2, 2, 0];
    let offset = edgeId * 2;
    return [node.points_[edge2PtIds[offset]],
            node.points_[edge2PtIds[offset + 1]]];
}

function cross2d(a, b) {
    return (a[0] * b[1]) - (a[1] * b[0]);
}

function getMidPoint(pt1, pt2) {
    let mX = pt1.x + ((pt2.x - pt1.x) * 0.5);
    let mY = pt1.y + ((pt2.y - pt1.y) * 0.5);
    return vec2.fromValues(mX, mY);
}

// return 1 if on right, -1 for left and 0 if straight ahead or behind
function onWhichSideOf(target, observer, fwdDir) {
    let obs2Tgt = vec2.create();
    vec2.sub(obs2Tgt, target, observer);
    let c = cross2d(fwdDir, obs2Tgt);
    return (c > 0) ? 1 : ((c < 0) ? -1 : 0);
}

// checks points for value equality
function areEqualPts(a, b) {
    return (a[0] === b[0]) && (a[1] === b[1]);
}

// returns the vector perpendicular to a line from a point
function pointLinePerpVec(x, e1, e2) {
    let v = vec2.create();
    vec2.sub(v, e2, e1);
    let line2Pt = vec2.create();
    vec2.sub(line2Pt, x, e1);
    let s = vec2.dot(line2Pt, v) / vec2.sqrLen(v);
    vec2.scale(v, v, s);
    let perp = vec2.create();
    vec2.sub(perp, line2Pt, v);
    return perp;
}

// Returns parameter t if intersecting, ∞ if parallel else NaN otherwise.
// t corresponds to d1, the direction of the first ray.
function xsectRayRay(o1, d1, o2, d2, epsilon) {
    let d2Perp = vec2.fromValues(-d2[1], d2[0]);
    let proj = vec2.dot(d1, d2Perp);
    if (Math.abs(proj) <= epsilon)
        return Infinity;
    let vecBwOrigins = vec2.create();
    vec2.sub(vecBwOrigins, o2, o1);
    let t = vec2.dot(vecBwOrigins, d2Perp) / proj;
    return (t >= -epsilon) ? t : NaN;
}

function makeEdges(coords) {
    let n = coords.length / 2;
    if (n < 3)
        console.error("Minimum 3 edges are needed for a polygon");
    let e = new Array(n);
    for (let i = 0; i < n; ++i) {
        let j = i * 2;
        let k = (i * 2 + 2) % (n * 2);
        let pts = [vec2.fromValues(coords[j], coords[j + 1]),
                   vec2.fromValues(coords[k], coords[k + 1])];
        let vec = vec2.create();
        vec2.sub(vec, pts[1], pts[0]);
        let norm = vec2.fromValues(vec[1], -vec[0]);
        vec2.normalize(norm, norm);
        e[i] = { pts, vec, norm };
    }
    return e;
}

// input two edges
function blowEdges(e1, e2, radius) {
    let p1 = vec2.create();
    let p2 = vec2.create();
    vec2.copy(p1, e1.pts[0]);
    vec2.copy(p2, e2.pts[0]);
    vec2.scaleAndAdd(p1, p1, e1.norm, radius);
    vec2.scaleAndAdd(p2, p2, e2.norm, radius);
    let v2 = vec2.create();
    vec2.scale(v2, e2.vec, -1);
    let t = xsectRayRay(p1, e1.vec, p2, v2, globalEps);
    if (!isFinite(t))
        console.error("Extruded edge rays don't intersect");
    vec2.scaleAndAdd(v2, p1, e1.vec, t);
    return v2;
}

// There can be situations where none of the portals have any issues in letting
// the character through but another polygon’s corner might interfere with the
// non-zero radius character path. Extruding might be a better fix here, instead
// of doing it for each search.
// Input: polygon with minimum 3 sides and edges points in clockwise order
function extrudePolygon(polygon, radius) {
    let e = makeEdges(polygon.coords);
    let n = e.length;
    let newCoords = new Array(n + 1);
    for (let i = 0; i < n; ++i)
        newCoords[i + 1] = blowEdges(e[i], e[(i + 1) % n], radius);
    // the first point pushed into newCoords would’ve been the second in coords
    // fix it by rotating the last to the first
    newCoords[0] = newCoords.pop();
    return newCoords;
}

function extrudeBuildings() {
    let n = levelData.length;
    for (let i = 0; i < n; ++i) {
        let building = levelData[i];
        building.extrudedCoords = extrudePolygon(building, charRadius);
    }
}

function triangulateInput(width, height, holes) {
    // create all-enclosing level border as sweep context
    let border = [
        new poly2tri.Point(0, 0),
        new poly2tri.Point(width, 0),
        new poly2tri.Point(width, height),
        new poly2tri.Point(0, height)
    ];
    let sweepCtx = new poly2tri.SweepContext(border);

    // add holes; they (polygons) will be automatically closed
    let n = holes.length;
    let holeArray = new Array(n);
    for (let i = 0; i < n; ++i) {
        let coords = holes[i].coords;
        let navCoords = holes[i].extrudedCoords;
        let m = coords.length / 2;
        let pts = new Array(m);
        for (let j = 0; j < m; ++j) {
            let k = j * 2;
            let pt = new poly2tri.Point(coords[k], coords[k + 1]);
            // put the extruded point along with the original for funnel
            // narrowing to refer to it when smoothing
            pt.extrudedPt = navCoords[j];
            pts[j] = pt;
        }
        holeArray[i] = pts;
    }
    sweepCtx.addHoles(holeArray);
    sweepCtx.triangulate();

    // assign IDs to triangles
    let triangles = sweepCtx.getTriangles();
    n = triangles.length;
    for (let i = 0; i < n; ++i)
        triangles[i].id = i;
    return triangles;
}

// http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html clearly
// says that the actual cost and the heuristics need to be of the same units for
// it to work correctly.  So if one is in hours the other should be in hours
// too; if one is in square of distance, the other should be too.  However,
// using the square of the distance gives poor results since they’re not just
// used in an one-off equation but they get accumulated in a node’s cost-so-far
// calculation giving unpredicatable results.  The aforementioned link says that
// the square of distance method “runs into the scale problem”.

function l1NormHeuristic(graph, src, dst) {
    let d = vec2.create();
    vec2.sub(d, graph.nodes[src].pos, graph.nodes[dst].pos);
    return Math.abs(d[0]) + Math.abs(d[1]);
}

function l2NormHeuristic(graph, src, dst) {
    return vec2.dist(graph.nodes[src].pos, graph.nodes[dst].pos);
}

// use this to make A* behave like Dijkstra
function nullHeuristic(graph, src, dst) {
    return 0;
}

// copied from GJK_EPA workout
function isPointInTriangle(verts, pt, epsilon) {
    let triPoints = [vec2.fromValues(verts[0].x, verts[0].y),
                     vec2.fromValues(verts[1].x, verts[1].y),
                     vec2.fromValues(verts[2].x, verts[2].y)];
    let edge = [vec2.create(), vec2.create(), vec2.create()];
    vec2.sub(edge[0], triPoints[1], triPoints[0]);
    vec2.sub(edge[1], triPoints[2], triPoints[1]);
    let normal = vec3.create();
    vec2.cross(normal, edge[0], edge[1]);
    let twoAreaSqr = vec3.sqrLen(normal);
    let ptVecs = [vec2.create(), vec2.create()];
    let ptNormals = [vec3.create(), vec3.create()];

    vec2.sub(ptVecs[1], pt, triPoints[1]);
    vec2.cross(ptNormals[1], edge[1], ptVecs[1]);
    let alpha = vec3.dot(ptNormals[1], normal) / twoAreaSqr;
    if (((alpha >= -epsilon) && (alpha <= (1 + epsilon)))) {
        vec2.sub(ptVecs[0], pt, triPoints[0]);
        vec2.cross(ptNormals[0], edge[0], ptVecs[0]);
        let gamma = vec3.dot(ptNormals[0], normal) / twoAreaSqr;
        if (((gamma >= -epsilon) && (gamma <= (1 + epsilon)))) {
            let beta = 1 - alpha - gamma;
            return (((beta >= -epsilon) && (beta <= (1 + epsilon))));
        }
    }
    return false;
}

// We hit test the point against all triangles in this demo since we
// allow teleportation of the player. In a game, without
// teleportation, just testing neighbouring triangles of the previosuly
// known location will do.
// Returns the node ID containing the point or -1 if none has it.
function hitTest(triangleData, x, y) {
    const epsilon = 0.000001;
    let pt = vec2.fromValues(x, y);
    let n = triangleData.length;
    // all triangles will be interior, so no need to check
    for (let i = 0; i < n; ++i) {
        if (isPointInTriangle(triangleData[i].points_, pt, epsilon)) {
            return i;
        }
    }
    return -1;
}

// Returns sorted pts such that right is at index 0. It also converts points
// to glMatrix vec2 type from poly2tri’s type.  If a point has extruded variant,
// it returns that instead of the original point.
function sortLeftRight(pts, observer) {
    let orgPt0 = pts[0], orgPt1 = pts[1];
    let pt0 = vec2.fromValues(orgPt0.x, orgPt0.y);
    let pt1 = vec2.fromValues(orgPt1.x, orgPt1.y);
    if (charSmoothing && orgPt0.extrudedPt)
        pt0.extrudedPt = vec2.fromValues(orgPt0.extrudedPt[0],
                                         orgPt0.extrudedPt[1]);
    if (charSmoothing && orgPt1.extrudedPt)
        pt1.extrudedPt = vec2.fromValues(orgPt1.extrudedPt[0],
                                         orgPt1.extrudedPt[1]);
    // assume right arm is given by index 0
    // right and left are conventions from the observer’s viewpoint
    let r = vec2.create(); vec2.sub(r, pt0, observer);
    let l = vec2.create(); vec2.sub(l, pt1, observer);
    let normal = cross2d(r, l);
    // bail out if observer is collinear with the portal ends; checking with
    // epsilon leads to incorrect results here, understandably.
    if (normal == 0) return false;
    // canvas has +Z going towards the viewer (left-handed); if already sorted,
    // right × left would be along −Z, so flip if positive
    return (normal < 0) ? [pt0, pt1] : [pt1, pt0];
}

let VecStatus = { BeforeRight: -1, InBetween: 0, BeyondLeft: 1 };

function isPointInFunnel(pt, apex, right, left) {
    // except apex, every other point might have an extruded version; use that
    // if available; apex was already dealt with in narrowFunnel
    let rArm = vec2.create(), lArm = vec2.create(), pArm = vec2.create();

    // when apex == right or left, since apex.extrudedPt isn’t used,
    // (apex − pt) ≠ 0 vector, though it should be; this leads to wrongly
    // returning BeyondRight/Left; Issue src (302, 318), dst (61, 237).

    // Can’t use either equality operators since the same point (value) can be
    // living in multiple portal points, so object references will be unequal.
    if (!areEqualPts(apex, right))
        vec2.sub(rArm, right.extrudedPt || right, apex);
    if (!areEqualPts(apex, left))
        vec2.sub(lArm, left.extrudedPt || left, apex);
    if (!areEqualPts(apex, pt))
        vec2.sub(pArm, pt.extrudedPt || pt, apex);
    // right × point
    let z1 = cross2d(rArm, pArm);
    // though the else case might seem redundant, it is needed to correctly
    // handle cases where pt and/or apex = left or right; e.g. test case
    // (150, 402) → (144, 31), where z1 = 0 (pt = right), but without the else
    // it goes on to calculate z1, which, due to extrusion comes out positive,
    // there by returning BeyondLeft when it’s actually InBetween.
    if (z1 > 0)
        return VecStatus.BeforeRight;
    else if (z1 === 0)
        return VecStatus.InBetween;
    // left × point
    let z2 = cross2d(pArm, lArm);
    return (z2 > globalEps) ? VecStatus.BeyondLeft : VecStatus.InBetween;
}

// The funnel narrowing (string-pulling) algorithm as described in
// http://digestingduck.blogspot.com/2010/03/simple-stupid-funnel-algorithm.html
// http://ahamnett.blogspot.ie/2012/10/funnel-algorithm.html
// originally from the paper Efficient Triangulation-Based Pathfinding
// http://games.cs.ualberta.ca/%7Emburo/ps/thesis_demyen_2006.pdf
// See Also
// https://deepnight.net/tutorial/
//         bresenham-magic-raycasting-line-of-sight-pathfinding/
function narrowFunnel(portalRight, portalLeft) {
    let turnPts = [];
    // make a funnel with src.Pt as apex and first portal points as arm ends
    let apex = src.pt;
    // these index into portalRight/Left denoting the current arm ends
    let idxRight = 0, idxLeft = 0;
    let n = portalRight.length;
    // iterate over all portals except first as it’s the funnel’s arm already
    for (let i = 1; i < n; ++i)
    {
        // cache arm end points for easy access
        let ptRight = portalRight[idxRight];
        let ptLeft = portalLeft[idxLeft];

        // Right Side
        // Check the positioning of the new right point w.r.t the funnel
        let status = isPointInFunnel(portalRight[i],
                                     apex,
                                     ptRight,
                                     ptLeft);
        // if within funnel angles, narrow funnel by making it the new right
        if (status === VecStatus.InBetween) {
            idxRight = i;
            ptRight = portalRight[idxRight];
        }
        // if within right but beyond left, we’ve a turning point, as this
        // indicates an acute change in the direction
        else if (status === VecStatus.BeyondLeft) {
            // copy turning point to output list; we don’t check if this point
            // is a false positive (case 2a) due to polygon extrusion here
            // since we don’t know the next turn point correctly yet.
            turnPts.push({pt: ptLeft});
            // remake funnel; set turning point as new apex; make sure you add
            // the non-extruded point without which some corners which have to
            // be consider mightn’t be. Issue src: (260, 444) dst: (106, 39).
            apex = ptLeft;
            // idxLeft would be pointing to apex; inc to get new arm ends
            ++idxLeft;
            idxRight = idxLeft;
            // reset i to one more than new arm ends for next trial, but since
            // the loop counter would be incremented, we leave one lesser in it
            i = idxRight;
            // since the funnel was remade, start afresh
            continue;
        }
        // else if before right, then this point is ignored as it’s widening

        // Left Side; same as right with directions reversed
        status = isPointInFunnel(portalLeft[i],
                                 apex,
                                 ptRight,
                                 ptLeft);
        if (status === VecStatus.InBetween)
            idxLeft = i;    // skip updating ptLeft as it’s unused hereafter
        else if (status === VecStatus.BeforeRight) {
            turnPts.push({pt: ptRight});
            apex = ptRight;
            ++idxRight;
            i = idxLeft = idxRight;
        }
    }
    return turnPts.length ? turnPts : undefined;
}

// Entry function of the funnel narrowing path smoothing algorithm. It prepares
// portals from rough path and feeds it to narrowFunnel, the actual algorithm.
// Portals are nothing but the edges of a path between nodes.
function funnelSmooth(graph, plan) {
    let portalRight = [], portalLeft = [];
    let nodeIds = plan.nodeIds;
    let nodes = graph.nodes;
    let n = nodeIds.length;
    let observer = src.pt;
    for (let i = 0; i < n; ++i) {
        let node = nodes[nodeIds[i]];
        let pts = sortLeftRight(node.edgePts, observer);
        // when src.pt is the observer, it may be collinear to the first
        // portal’s ends; let it continue to be the observer; we’ve simply moved
        // the observer along the edge from the node to src.pt; it shouldn’t
        // affect the outcome; we simply skip this portal as it’s redundant
        if (pts) {
            portalRight.push(pts[0]);
            portalLeft.push(pts[1]);
            observer = node.pos;
        }
    }
    portalRight.push(dst.pt);
    portalLeft.push(dst.pt);
    // the destination is set as the last portal since it’s needed in cases
    // where the destination pt is just around the corner:
    //
    //              +---+---+
    //              |  /|\  |
    //              | / | \ |
    //              |/  | ×\|
    //              +---+---+
    //              |  /|
    //              | / |
    //              |/ •|
    //              +---+

    debugVars.portal = {right: portalRight, left: portalLeft};
    debugVars.corners.length = 0;
    return narrowFunnel(portalRight, portalLeft);
}

function calcTangentPoint(from, centre, radius) {
    let hypVec = vec2.create();
    vec2.sub(hypVec, centre, from);
    let hypSqr = vec2.sqrLen(hypVec);
    let hyp = charRadius, adj = 0;
    let arcPt = vec2.create();
    // check if the point is within the circle or on its circumference
    if (hypSqr <= (charRadiusSqr + globalEps)) {
        vec2.normalize(hypVec, hypVec);
         // since fromPoint is inside, the vector will be pointing inwards, flip
         // the hypot vector to make the vector outwards from circle centre and
         // calculate the point on the circumference (arcPts)
        vec2.scaleAndAdd(arcPt, centre, hypVec, -charRadius);
        return arcPt;
    }
    let hypot = Math.sqrt(hypSqr);
    adj = Math.sqrt(hypSqr - charRadiusSqr);
    let cosX = adj / hyp;
    // we’ve to rotate the hypotenuse vector clockwise or counter clockwise
    // depending on whether the from point is to the right or left of centre
    let fwd = vec2.create();
    vec2.sub(fwd, centre.extrudedPt, centre);
    let sinX = onWhichSideOf(from, centre, fwd) * radius / hyp;
    let rotor = mat2.fromValues(cosX, sinX, -sinX, cosX);
    let tang = vec2.create();
    vec2.transformMat2(tang, hypVec, rotor);
    let proj = vec2.dot(hypVec, tang) / vec2.sqrLen(tang);
    vec2.scaleAndAdd(arcPt, from, tang, proj);
    return arcPt;
}

// There are three cases of turns:
// 1. A real turn not due to the character radius; this includes turns due to
//    the plan returning having unnatural turns due to bad costing.
// 2. A turn due to the character radius
//   2a. but unneeded since the shortest distance between the corner and the
//       travel path is larger than the character radius
//   2b. needed since the character is really big for the corner
// 2a ones are false positives added due to the polygon extrusion.
// This function removes such turns from turnPts in place. For case 2b, it makes
// simple points into point objects to additionally bear arc and control points.

// TODO: This function works locally; it doesn't deal with the problem globally.
// It looks at two turning points and decides if there's a need for the former.
// However, globally this might lead to a bug. Say a turning point is pruned
// since it's unneeded; now there's a straight line between from and to ignoring
// this point. Say if the next turns was considered unneeded too, this might
// slope the straight line further than it earlier was, invalidating the earlier
// decision that the previous turning point was unneeded.
function augmentCurvedTurns(turnPts) {
    let n = turnPts && turnPts.length;
    if (!n) return undefined;
    let from = src.pt, thru, to;
    let leg1 = vec2.create(), leg2 = vec2.create();
    let fwd = vec2.create();
    let unneeded = false;
    let removed = 0;
    for (let i = 0; i < n; ++i) {
        thru = turnPts[i].pt;
        unneeded = false;
        if (thru.extrudedPt) {
            to = (((i + 1) < n) ? turnPts[i + 1] : dst).pt;
            vec2.sub(fwd, thru.extrudedPt, thru);
            vec2.sub(leg1, from, thru);
            vec2.sub(leg2, to, thru);
            // If the shortest distance from the corner to the line directly
            // connecting the previous and next points is greater than the
            // character radius and if the corner is lying behind this line,
            // then this is a false positive; remove it.
            let perp = pointLinePerpVec(thru, from, to);
            unneeded = (vec2.sqrLen(perp) > charRadiusSqr) &&
                       (vec2.dot(fwd, perp) < 0);
            // if this is turn around a polygon corner make it curved
            turnPts[i].curved = !unneeded;
        }
        if (unneeded) {
            turnPts.splice(i, 1);
            --i;
            --n;
            ++removed;
        }
        else {
            from = thru;
            if (thru.extrudedPt)
                debugVars.corners.push(thru);
        }
    }
    if (removed)
        console.log("Pruned turns:", removed);
    return n ? turnPts : undefined;
}

// This function calculates the tangent points of two turn circles
// simultaneously; it assumes distance(from, to) > 0.
function calcTurnToTurnTangentPoints(from, to, radius, arcPts) {
    // calc vector between centre and its perpendicular
    let c2c = vec2.create();
    vec2.sub(c2c, to, from);
    let n1 = vec2.fromValues(-c2c[1], c2c[0]);
    vec2.normalize(n1, n1);
    let n2 = vec2.create();
    vec2.copy(n2, n1);
    let fwd1 = vec2.create(), fwd2 = vec2.create();
    vec2.sub(fwd1, from.extrudedPt, from);
    vec2.sub(fwd2, to.extrudedPt, to);
    // check if the both normals are pointing outwards from the corner
    // if not, flip and scale it by radius
    vec2.scale(n1, n1, (vec2.dot(n1, fwd1) >= 0) ? radius : -radius);
    vec2.scale(n2, n2, (vec2.dot(n2, fwd2) >= 0) ? radius : -radius);
    // add centre to arrive at point on circumference
    let p1 = vec2.create();
    vec2.add(p1, from, n1);
    let p2 = vec2.create();
    vec2.add(p2, to, n2);
    arcPts.push(p1, p2);
}

// calculate control points for all turns in the path
function calcCtrlPts(turnPts, radius) {
    let n = turnPts.length;
    let r1 = vec2.create(), r2 = vec2.create();
    let bisector = vec2.create();
    let rotor = mat2.create();
    for (let i = 0; i < n; ++i) {
        let turn = turnPts[i];
        // if this isn’t a curved turn, continue
        if (!turn.curved) continue;
        let corner = turn.pt;
        let a1 = turn.arcPts[0];
        let a2 = turn.arcPts[1];
        let ctrlPts = [];
        // calculate control points if arc points aren’t too close
        if (vec2.sqrDist(a1, a2) > globalEps) {
            vec2.sub(r1, a1, corner);
            vec2.sub(r2, a2, corner);
            let z = cross2d(r1, r2);
            // deal with antipodal tangent points separately
            // refer to CornerTackle.html workout
            if (Math.abs(z) > globalEps) {
                vec2.add(bisector, r1, r2);
                vec2.normalize(bisector, bisector);
            }
            else
                vec2.set(bisector, -r1[1], r1[0]);

            // we've besector, calculate sin, cos and control points assuming
            // bisector is the X axis and rotate later.
            let cosX = vec2.dot(bisector, r1) / radius;
            let sinX = cross2d(r1, bisector) / radius;
            let cx = radius * (4 - cosX) / 3;
            let cy = radius * (3 - cosX) * (1 - cosX) / (3 * sinX);
            let c1 = vec2.fromValues(cx, -cy);
            let c2 = vec2.fromValues(cx, cy);
            // rotate both control points by bisector's angle
            mat2.set(rotor, bisector[0], bisector[1],
                           -bisector[1], bisector[0]);
            vec2.transformMat2(c1, c1, rotor);
            vec2.transformMat2(c2, c2, rotor);
            // translate to circle centre
            vec2.add(c1, c1, corner);
            vec2.add(c2, c2, corner);
            ctrlPts.push(c1, c2);
        }
        // arc points are too close; just draw a line
        else
            ctrlPts.push(a1, a2);
        turn.ctrlPts = ctrlPts;
    }
}

function curveTurns(turnPts, plan) {
    let n = turnPts.length;

    // first and last turn points to need curves
    let p, q;
    for (let i = 0; i < n; ++i) {
        if (turnPts[i].curved) {
            // 0 is falsy too, so check explicitly; not as !p
            if (p === undefined)
                p = i;
            q = i;
        }
    }
    // we’ve no curved turns; return
    if (p === undefined) return;
    let arcPts = [];
    let thru = turnPts[p];
    // do first
    arcPts.push(calcTangentPoint((p == 0) ? src.pt : turnPts[p - 1].pt,
                                 thru.pt, charRadius));
    // When there's more than one turn point, each cannot be handled in
    // isolation. When connecting two turn points, one's arc end point will
    // have to be adjusted in unison wit the other's arc begin point. We can’t
    // calculate the first's arc end assuming its to-point (next's arc begin) is
    // going to be stable since it’ll change when we handle it next, moving it
    // to its circle’s circumference, thereby changing the connector's slope.
    thru = turnPts[p];
    for (let i = (p + 1); i <= q; ++i) {
        let to = turnPts[i];
        // if not a curve point continue
        if (to.curved) {
            calcTurnToTurnTangentPoints(thru.pt, to.pt, charRadius, arcPts);
            thru = to;
        }
    }

    // do last
    thru = turnPts[q];
    arcPts.push(calcTangentPoint(((q + 1) == n) ? dst.pt : turnPts[q + 1].pt,
                                 thru.pt, charRadius));
    // we’ve the arc points; assign them to each curve turn point
    for (let i = 0, j = 0; i < n; ++i) {
        let thru = turnPts[i];
        if (thru.curved) {
            thru.arcPts = [ arcPts[j * 2], arcPts[j * 2 + 1] ];
            ++j;
        }
    }
    calcCtrlPts(turnPts, charRadius);
}

function assembleTurningPoints(graph, plan) {
    let nodeIds = plan.nodeIds;
    let turnPoints = [];
    if (smoothing.current == smoothing.None) {
        let n = nodeIds.length;
        for (let i = 0; i < n; ++i)
            turnPoints.push({pt: graph.getNode(nodeIds[i]).pos});
    }
    else {
        turnPoints = funnelSmooth(graph, plan);
        if (charSmoothing) {
            turnPoints = augmentCurvedTurns(turnPoints);
            if (turnPoints)
                curveTurns(turnPoints, plan);
        }
    }
    return turnPoints;
}

function findNearestNode(pt, triangle, graph) {
    let nodes = graph.nodes;
    let n0 = nodes[triangle.nodeIds[0]];
    let d0 = vec2.sqrDist(pt, n0.pos);
    let n1 = nodes[triangle.nodeIds[1]];
    let d1 = vec2.sqrDist(pt, n1.pos);
    let n2 = nodes[triangle.nodeIds[2]];
    let d2 = vec2.sqrDist(pt, n2.pos);
    if ((d0 <= d1) && (d0 <= d2))
        return n0.id;
    else if ((d1 <= d0) && (d1 <= d2))
        return n1.id;
    else
        return n2.id;
}

function setSrcDst(x, y, tri, graph) {
    let loc;
    if ((!src.valid) || (dst.valid)) {
        loc = src;
        dst.valid = false;
    }
    else
        loc = dst;
    // set src if src is invalid or both are set; otherwise set dst
    vec2.set(loc.pt, x, y);
    loc.nodeId = findNearestNode(loc.pt, tri, graph);
    loc.tri = tri;
    loc.valid = true;
}

// A plan might have redundant nodes at the start and end, prune them.
function pruneRedundantNodesInSameTriangle(plan, graph) {
    let nodes = plan && plan.nodeIds;
    if (nodes.length >= 2) {
        let gNodes = graph.nodes;
        let edgeIds = plan.edgeIds;
        let srcTriId = src.tri.id;
        let n1 = gNodes[nodes[0]];
        let n2 = gNodes[nodes[1]];
        // first and second nodes belong to the same triangle
        // remove the first node
        if (((n1.triIds[0] === srcTriId) ||
             (n1.triIds[1] === srcTriId)) &&
            ((n2.triIds[0] === srcTriId) ||
             (n2.triIds[1] === srcTriId))) {
            nodes.splice(0, 1);
            edgeIds.splice(0, 1);
        }

        let dstTriId = dst.tri.id;
        let n = nodes.length;
        n1 = gNodes[nodes[n - 1]];
        n2 = gNodes[nodes[n - 2]];
        // last and last - 1 nodes belong to the same triangle
        // remove the last node
        if (((n1.triIds[0] === dstTriId) ||
             (n1.triIds[1] === dstTriId)) &&
            ((n2.triIds[0] === dstTriId) ||
             (n2.triIds[1] === dstTriId))) {
            nodes.pop();
            edgeIds.pop();
        }
    }
}

function requestPath(triangleData) {
    if (src.valid && dst.valid) {
        if ((src.tri !== dst.tri) && (src.nodeId != dst.nodeId)) {
            plan = AStar.findPath(triangleData.g,
                                  src.nodeId,
                                  dst.nodeId,
                                  curHeuristic);
            pruneRedundantNodesInSameTriangle(plan, triangleData.g);
            plan.turnPts = assembleTurningPoints(triangleData.g, plan);
        }
        else {
            // TODO: handle same triangle / same node case
            plan = { };
            debugVars.portal = undefined;
        }
    }
    else
        plan = undefined;
}

function onClick(x, y, triangleData) {
    let triId = hitTest(triangleData, x, y);
    if (triId != -1) {
        setSrcDst(x, y, triangleData[triId], triangleData.g);
        requestPath(triangleData);
    }
}

// http://stackoverflow.com/a/18053642
function getCursorPosition(event, element) {
    var rect = element.getBoundingClientRect();
    var x = event.clientX - rect.left;
    var y = event.clientY - rect.top;
    return vec2.fromValues(x, y);
}

function makeNode(tri, id, g, triIdx) {
    let nodeId = tri.nodeIds[id];
    if (nodeId === -1) {
        let pts = getEdgePoints(tri, id);
        nodeId = g.nodes.length;
        g.nodes.push({
            pos: getMidPoint(pts[0], pts[1]),
            edgePts: pts,
            id: nodeId,
            triIds: [triIdx]
        });
        tri.nodeIds[id] = nodeId;
    }
    return nodeId;
}

// returns the shared edge ID from the neighbour’s viewpoint
function getSharedEdgeId(triangle, edgeId, neighbour) {
    let ep = getEdgePoints(triangle, edgeId);
    let np = neighbour.points_;
    if (ep[0] === np[0])
        return (ep[1] === np[1]) ? 0 : 2;
    else if (ep[0] === np[1])
        return (ep[1] === np[2]) ? 1 : 0;
    else if (ep[0] === np[2])
        return (ep[1] === np[0]) ? 2 : 1;
    console.error("No points match between these neighbours.");
    return -1;
}

// for all the nodes within a triangle, form connections
function formLinks(triangle, graph) {
    let nodeIds = triangle.nodeIds;
    let n0 = graph.nodes[nodeIds[0]];
    let n1 = graph.nodes[nodeIds[1]];
    let n2 = graph.nodes[nodeIds[2]];
    let c0 = vec2.dist(n0.pos, n1.pos);
    let c1 = vec2.dist(n1.pos, n2.pos);
    let c2 = vec2.dist(n0.pos, n2.pos);
    let lId0 = graph.links.length;
    let lId1 = lId0 + 1;
    let lId2 = lId1 + 1;
    let l0 = { id: lId0, e1: n0.id, e2: n1.id, cost: c0 };
    let l1 = { id: lId1, e1: n1.id, e2: n2.id, cost: c1 };
    let l2 = { id: lId2, e1: n2.id, e2: n0.id, cost: c2 };
    graph.links.push(l0, l1, l2);
    // a node lies on a two triangle’s shared edge; 2 links per triangle
    let o0 = 2, o1 = 2, o2 = 2;
    if (n0.linkIds === undefined) {
        n0.linkIds = new Uint16Array([INVALID_DATA,
                                      INVALID_DATA,
                                      INVALID_DATA,
                                      INVALID_DATA]);
        o0 = 0;
    }
    if (n1.linkIds === undefined) {
        n1.linkIds = new Uint16Array([INVALID_DATA,
                                      INVALID_DATA,
                                      INVALID_DATA,
                                      INVALID_DATA]);
        o1 = 0;
    }
    if (n2.linkIds === undefined) {
        n2.linkIds = new Uint16Array([INVALID_DATA,
                                      INVALID_DATA,
                                      INVALID_DATA,
                                      INVALID_DATA]);
        o2 = 0;
    }
    n0.linkIds[o0] = lId0, n0.linkIds[o0 + 1] = lId2;
    n1.linkIds[o1] = lId0, n1.linkIds[o1 + 1] = lId1;
    n2.linkIds[o2] = lId1, n2.linkIds[o2 + 1] = lId2;
}

// find the midpoint of each edge
// make nodes out of each
// if there’re adjacent non-interior triangles assign the ID
// try to pre-calculate weight and assign it
function makeGraph(triangleData) {
    let n = triangleData.length;
    let g = { nodes: [], links: [] };
    for (let i = 0; i < n; ++i) {
        let t = triangleData[i];
        t.nodeIds = t.nodeIds || new Int16Array([-1, -1, -1]);
        for (let j = 0; j < 3; ++j)
            makeNode(t, j, g, i);
        formLinks(t, g);
        // the mapping between an edge and the neighbour is constant
        // so it’s hard-coded here e.g. edge 0 always comes from neighbour 2
        let neighbourEdgeMap = new Uint8Array([1, 2, 0]);
        // neighbouring assignments
        for (let k = 0; k < 3; ++k) {
            let nbor = t.neighbors_[k];
            if (isNodeValid(nbor)) {
                nbor.nodeIds = nbor.nodeIds || new Int16Array([-1, -1, -1]);
                let thisEdgeId = neighbourEdgeMap[k];
                let nEdge = getSharedEdgeId(t, thisEdgeId, nbor);
                // since pushing isn’t idempotent, set nodeIds and push triId
                // only if this neighbour wasn’t already met
                if (nbor.nodeIds[nEdge] === -1) {
                    let nodeId = t.nodeIds[thisEdgeId];
                    nbor.nodeIds[nEdge] = nodeId;
                    g.nodes[nodeId].triIds.push(nbor.id);
                }
            }
        }
    }
    g.length = g.nodes.length;
    return g;
}

// graph object functions
function getGraphLinks(f) {
    return this.nodes[f].linkIds;
}

function getGraphNode(id) {
    if (id >= this.nodes.length)
        console.error("Node requested not in graph");
    return this.nodes[id];
}

function getGraphLink(id) {
    return (id === INVALID_DATA) ? undefined : this.links[id];
}

function getGraphLinkDst(l, srcId) {
    return (l.e1 !== srcId) ? l.e1 : l.e2;
}

function changeHeuristic(i, triangleData) {
    curHeuristic = heuristics[i];
    requestPath(triangleData);
}

function toggleCharHandling(triangleData) {
    charSmoothing = !charSmoothing;
    if (charSmoothing && plan)
        plan.turnPts = assembleTurningPoints(triangleData.g, plan);
}

function changeSmoothing(idx, triangleData) {
    smoothing.current = idx;
    if (plan)
        plan.turnPts = assembleTurningPoints(triangleData.g, plan);
}

function start() {
    let canvas = document.getElementById("canvas");
    let ctx = init2DCanvas(canvas);
    extrudeBuildings();
    let triangleData = triangulateInput(ctx.canvas.clientWidth,
                                        ctx.canvas.clientHeight,
                                        levelData);
    let g = makeGraph(triangleData);
    g.getLinks = getGraphLinks;
    g.getNode = getGraphNode;
    g.getLink = getGraphLink;
    g.getLinkDst = getGraphLinkDst;
    triangleData.g = g;

    // tie variables to controls before adding event listeners
    document.getElementById("chkDebug").checked = debugRender;
    document.getElementById("smoothList").selectedIndex = smoothing.current;
    document.getElementById("chkCharRadius").checked = charSmoothing;
    document.addEventListener("mousemove", function (e) {
        if (!debugRender) return;
        let pt = getCursorPosition(e, canvas);
        mouseTri = hitTest(triangleData, pt[0], pt[1]);
    }, false);
    canvas.addEventListener("click", function (e) {
        if (e.shiftKey && debugRender) {
            let node = triangleData[mouseTri];
            let c = node.centroid;
            debugVars.lastPt = debugVars.lastPt || c;
            console.log("Centroid: ", node.centroid);
            debugVars.cost += vec2.dist(c, debugVars.lastPt);
            debugVars.lastPt = c;
            console.log("Path cost: ", debugVars.cost);
        } else {
            let pt = getCursorPosition(e, canvas);
            onClick(pt[0], pt[1], triangleData);
        }
    }, false);
    document.getElementById("heuristicList")
        .addEventListener("change", function(e) {
            changeHeuristic(e.target.selectedIndex, triangleData);
        });
    let charSmoothUI = document.getElementById("charSmoothUI");
    document.getElementById("smoothList")
        .addEventListener("change", function(e) {
            changeSmoothing(e.target.selectedIndex, triangleData);
            if (smoothing.current == smoothing.Funnel)
                charSmoothUI.style.visibility = "visible";
            else
                charSmoothUI.style.visibility = "hidden";
        });
    document.getElementById("chkCharRadius")
        .addEventListener("change", function(e) {
            toggleCharHandling(triangleData);
        });
    document.getElementById("chkDebug")
        .addEventListener("change", function (e) {
            flipDebugView();
        });

    let loop = function (/* time*/) {
        render(ctx, levelData, triangleData);
        window.requestAnimationFrame(loop);
    };
    loop(0);
}

window.onload = start;
