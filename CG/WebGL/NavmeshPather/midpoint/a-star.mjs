import {BinHeap} from "../bin-heap.mjs";

const NodeState = { Unseen: 0, Seen: 1, Visited: 2 };

// Record for all nodes in the graph. This might be needless and too costly for
// large graphs but for relatively smaller graphs (< 2 MiB) this is OK. This
// approach is discussed under §4.3.7 Node Array A* in Artificial Intelligence
// for Games, 2nd Edition
function NodeRecord(state, costSoFar) {
    state = state || NodeState.Unseen;
    costSoFar = isNaN(Number(costSoFar)) ? Infinity : costSoFar;

    this.via = null;
    this.cost = costSoFar;
    this.state = state;
    // store record’s index into priority queue to reseat it on priority change
    this.heapIdx = -1;
    this.estimate = undefined;
}

function visitNode(graph, nodeId, nodeRec, pq) {
    let nodeData = nodeRec[nodeId];
    let thisCost = nodeData.cost;
    let links = graph.getLinks(nodeId);
    let n = links.length;
    for (let i = 0; i < n; ++i) {
        let link = graph.getLink(links[i]);
        if (link === undefined) continue;
        let dst = graph.getLinkDst(link, nodeId);
        let newCost = thisCost + link.cost;
        // don’t need to set the new node data here, since it’s cost is ∞
        if (!(dst in nodeRec))
            nodeRec[dst] = new NodeRecord();
        let neighbourData = nodeRec[dst];
        if (newCost < neighbourData.cost) {
            neighbourData.cost = newCost;
            // NOTE: order matters here! Before pushing or reseating an
            // element in the priority queue, make sure the cost is updated.
            if (neighbourData.state == NodeState.Unseen)
                pq.push(dst);
            else
                pq.reseat(neighbourData.idx);
            // since we don’t have global IDs for edges, use local ones
            neighbourData.via = link;
            neighbourData.state = NodeState.Seen;
        }
    }
    nodeData.state = NodeState.Visited;
}

// returns a path plan with n nodes and n − 1 edges; edge ID 0 corresponds to
// node 1, edge ID to node2, …
export function findPath(graph, from, to, h) {
    let n = graph.length;
    let nodeRec = [];
    let pq = new BinHeap(makeHeuristicComparator(graph, nodeRec, to, h),
                         function (id, idx){ nodeRec[id].heapIdx = idx; });
    nodeRec[from] = new NodeRecord(NodeState.Seen, 0);
    pq.push(from);
    let i = 0;
    while (pq.size() && (pq.top() != to)) {
        let id = pq.pop();
        nodeRec[id].heapIdx = -1;
        visitNode(graph, id, nodeRec, pq);
        ++i;
    }
    console.log("Visited: %d / %d", i, n);
    console.log("To visit: %d / %d", pq.size(), n);
    if (pq.size() != 0)
        return retrivePath(graph, nodeRec, from, to);
    return undefined;
}

function retrivePath(graph, nodeRec, from, to) {
    let path = { nodeIds: [], edgeIds: [] };
    let current = to;
    while (current != from) {
        let nodeData = nodeRec[current];
        path.nodeIds.unshift(current);
        let link = nodeData.via;
        path.edgeIds.unshift(link.id);
        current = graph.getLinkDst(link, current);
    }
    path.nodeIds.unshift(from);
    return path;
}

function makeHeuristicComparator(graph, nodeRec, to, h) {
    return function (a, b) {
        let recA = nodeRec[a];
        let recB = nodeRec[b];
        recA.estimate = recA.estimate || h(graph, a, to);
        recB.estimate = recB.estimate || h(graph, b, to);
        return (recA.cost + recA.estimate) < (recB.cost + recB.estimate);
    };
}
