void PlotArc(_In_ const Graphics::CPoint &centre,
	float radius,
	float startAngle,
	float endAngle,
	_In_ PathCommandsPtr &spPathCommands)
{
	// 90 degrees in radians
	const auto rightAngle = static_cast<float>(M_PI_2);
	if (startAngle > endAngle)
	{
		std::swap(startAngle, endAngle);
	}

	while (startAngle < endAngle)
	{
		const auto angle = min(endAngle - startAngle, rightAngle);

		const float ratioX = cos(angle / 2.0f);
		const float ratioY = sin(angle / 2.0f);
		Graphics::CPoint p0((radius * ratioX), (radius * ratioY));
		Graphics::CPoint p3(p0.x, -p0.y);
		// in this method, swap the control points
		//const auto k = 0.5522847498f;
		//Graphics::CPoint p1(p3.x + (k * tan(angle / 2.0f) * p0.y), p3.y + (k * tan(angle / 2.0f) * p0.x));
		Graphics::CPoint p1(radius * (4.0f - ratioX) / 3.0f, radius * (1.0f - ratioX) * (3.0f - ratioX) / (3.0f * ratioY));
		Graphics::CPoint p2(p1.x, -p1.y);

		// position the arc points into the right sector
		Graphics::CMatrix position;
		position.MakeRotate(Graphics::RadToDeg(startAngle + (angle / 2.0f)));
		position.Set(2, 0, centre.x);
		position.Set(2, 1, centre.y);
		spPathCommands->MoveTo(p0 * position);
		spPathCommands->CubicBezierCurveTo(p1 * position, p2 * position, p3 * position);

		startAngle += angle;
	}
}
