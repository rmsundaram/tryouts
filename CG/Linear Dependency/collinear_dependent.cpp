// this file only deals with linear indepedency in ℝ³; Appendix A of 3-D Computer Graphics
// by Samuel R. Buss discusses cross product in ℝ² which may be employed to find linear
// independence between vectors ∈ ℝ². The method is essentially to promote them to ℝ³ and
// take their cross product; since z = 0 for both, the covector's x and y get zeroed out
// and it would have only z as non-zero; when they're linearly dependant, z will also be 0.

#include <iostream>

struct vec3
{
	vec3 operator-(vec3 that)
	{
		that.x = this->x - that.x;
		that.y = this->y - that.y;
		that.z = this->z - that.z;
		return that;
	}

	float x, y, z;
};

bool vectors_dependant(const vec3 &v1, const vec3 &v2)
{
	float kx = 0.f;
	// without this check, kx might become Inf or NaN
	if (v2.x)
	{
		kx = v1.x / v2.x;
	}
	// if v2.x == 0 then v1.x should be 0 for them to be dependant
	else if (v1.x != 0.f)
		return false;

	float ky = 0.f;
	if (v2.y)
	{
		ky = v1.y / v2.y;
	}
	else if (v1.y != 0.f)
		return false;

	float kz = 0.f;
	if (v2.z)
	{
		kz = v1.z / v2.z;
	}
	else if (v1.z != 0.f)
		return false;

	// this is for the degenerate case of vectors parallel
	// to the cardinal axes, where the other components
	// will both be 0; if both ratios of k are zero then
	// they should be excluded from the equality check
	const auto equality_1 = (kx && ky) ? (kx == ky) : true;
	const auto equality_2 = (ky && kz) ? (ky == kz) : true;
	// in production code an epsilon check should be done

	return (equality_1 && equality_2);
}

int main()
{
	vec3 p1 = {1, 1, 0} , p2 = {3, 3, 0}, p3 = {4, 4, 0};
	const auto &v1 = p2 - p1;
	const auto &v2 = p3 - p1;

	// to check for collinearity, one way is to cross v1 and v2
	// v1 = p2 - p1, v2 = p3 - p1 and check if we've a zero vector - Essential Math, James M. Van Verth

	// a more efficient way is to see if v1 = k v2 - Mathematics for Computer Graphics Applications, M.E.Mortenson
	// => v1.x = k v2.x, v1.y = k v2.y, v1.z = k v2.z i.e.
	// => (v1.x / v2.x) = (v1.y / v2.y) = (v1.z / v2.z) = k

	// the 2nd method is dimension-independent while the 1st works only in 3D but seems SIMD-friendly since the 2nd
    // has more `if' checks to avoid corner cases, while the 1st seems to have no corner case
	// the comparison needs to be benchmarked with a large data set

    // Essential Math discusses a 3rd method when the vectors "are prenormalized and speed is of the essence" using
    // the dot product: 1 - |v·w| ≈ 0

	// uses the 2nd method
	if (vectors_dependant(v1, v2))
		std::cout << "Points collinear";
	else
		std::cout << "Points not collinear";
	std::cout << std::endl;
}
