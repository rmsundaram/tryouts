/*
 * Techniques learnt:
 *  1. Free-roaming camera (the other version of rotating the camera around an object
 *                           of interest or crystal ball camera interface was implemented
 *                           as HW 1 in Dr. Ravi's CG course)
 *  2. Depth or z-buffering
 *  3. Polygon face culling
 */

#include "precomp.hpp"
#include "Dataless_wrapper.hpp"
#include "Shader_util.hpp"
#include "main.hpp"

#include <chrono>
#include <numeric>

using GLFW_wrapper = Dataless_wrap<decltype(&glfwTerminate), glfwTerminate>;

const unsigned window_width  = 800u,
               window_height = 600u;
const char* const app_title = "Free-roaming Camera";

GLFWwindow* setup_context(uint32_t width, uint32_t height)
{
    /*
     * without setting the version (1.0) or setting it < 3.2, requesting for core will fail
     * since context profiles only exist for OpenGL 3.2+; likewise forward compatibility
     * only exist from 3.0 onwards. 3.0 marked deprecated, 3.1 removed deprecated (except
     * wide lines), 3.2 reintroduced deprecated under compatibility profile
     */
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE,
                   GLFW_OPENGL_CORE_PROFILE);
    // since core only has undeprecated features, making the context forward-compatible is moot
    // glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

#ifndef NDEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

    auto window = glfwCreateWindow(width,
                                   height,
                                   app_title,
                                   nullptr,
                                   nullptr);
    check(window != nullptr, "Failed to create window");
    glfwMakeContextCurrent(window);
    glfwSwapInterval(1);
#ifdef _WIN32
    // http://stackoverflow.com/q/16285546
    // GLFW doesn't honour call to VSYNC when DWM compositing is enabled, which is practically all machines today;
    // turn it on using the using WGL_EXT_swap_control extension on Win32
    // TODO: do this for other platforms too
    typedef int (APIENTRY *PFNWGLSWAPINTERVALEXTPROC)(int interval);
    auto wglSwapInterval = reinterpret_cast<PFNWGLSWAPINTERVALEXTPROC>(glfwGetProcAddress("wglSwapIntervalEXT"));
    if (wglSwapInterval)
    {
        typedef int (APIENTRY *PFNWGLGETSWAPINTERVALEXTPROC)(void);
        auto wglGetSwapInterval =
            reinterpret_cast<PFNWGLGETSWAPINTERVALEXTPROC>(glfwGetProcAddress("wglGetSwapIntervalEXT"));
        // if the get function isn't there or is returning 0
        if (!wglGetSwapInterval || !wglGetSwapInterval())
            wglSwapInterval(1);
    }
#endif
    return window;
}

#ifndef NDEBUG

APIENTRY
void gl_debug_logger(GLenum source,
                     GLenum type,
                     GLuint id,
                     GLenum severity,
                     GLsizei /*length*/,
                     const char *msg,
                     const void *file_ptr)
{
    std::stringstream ss;
    ss << "GLError 0x"
       << std::hex << id << std::dec
       << ": "
       << msg <<
       " [source=";

    const char *sources[] = {
                                "API",
                                "WINDOW_SYSTEM",
                                "SHADER_COMPILER",
                                "THIRD_PARTY",
                                "APPLICATION",
                                "OTHER",
                                "UNDEFINED"
                            };
    size_t index = array::item_index(source,
                                     GL_DEBUG_SOURCE_API_ARB,
                                     GL_DEBUG_SOURCE_OTHER_ARB,
                                     array::max_index(sources));
    ss << sources[index];
    if (array::max_index(sources) == index)
    {
        ss << " (" << source << ")";
    }

    const char *types[] = {
                              "ERROR",
                              "DEPRECATED_BEHAVIOR",
                              "UNDEFINED_BEHAVIOR",
                              "PORTABILITY",
                              "PERFORMANCE",
                              "OTHER",
                              "UNDEFINED"
                          };
    index = array::item_index(type,
                              GL_DEBUG_TYPE_ERROR_ARB,
                              GL_DEBUG_TYPE_OTHER_ARB,
                              array::max_index(types));
    ss << " type=" << types[index];
    if (array::max_index(types) == index)
    {
        ss << " (" << type << ")";
    }

    const char *severities[] = {
                                   "HIGH",
                                   "MEDIUM",
                                   "LOW",
                                   "UNKNOWN"
                               };
    // as per the extension, HIGH is the base (smaller) value
    index = array::item_index(severity,
                              GL_DEBUG_SEVERITY_HIGH_ARB,
                              GL_DEBUG_SEVERITY_LOW_ARB,
                              array::max_index(severities));
    ss << " severity=" << severities[index];
    if (array::max_index(severities) == index)
    {
        ss << " (" << severity << ")";
    }
    ss << "]";

    const auto log = ss.str();
    FILE *out_file = reinterpret_cast<FILE*>(const_cast<void*>(file_ptr));
    fprintf(out_file, "%s\n", log.c_str());

    check(severity != GL_DEBUG_SEVERITY_HIGH_ARB, "High severity GL error logged");
}

void setup_debug(bool enable)
{
    if(glewIsSupported("GL_ARB_debug_output"))
    {
        glDebugMessageCallbackARB(enable ? gl_debug_logger : nullptr, stderr);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
        check(GL_NO_ERROR == glGetError(), "Unable to set synchronous debug output");
    }
}
#endif  // NDEBUG

void handle_key(GLFWwindow *window,
                int key,
                int /*scancode*/,
                int action,
                int /*mods*/)
{
    if(GLFW_PRESS == action)
    {
        switch(key)
        {
        case GLFW_KEY_ESCAPE:
            glfwSetWindowShouldClose(window, GL_TRUE);
            break;
        case GLFW_KEY_R:
            auto cam = reinterpret_cast<Camera*>(glfwGetWindowUserPointer(window));
            cam->reset();
            break;
        }
    }
}

void handle_input(GLFWwindow *window,
                  Camera *cam,
                  float ticks)
{
    auto constexpr slide_offset_per_ms = 0.007f;
    auto constexpr rot_angle_per_ms = 0.0015f;
    const auto slide_offset = ticks * slide_offset_per_ms;
    const auto rot_angle = ticks * rot_angle_per_ms;

    // rotation
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_UP))
        cam->rotate_X(rot_angle);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_DOWN))
        cam->rotate_X(-rot_angle);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_LEFT))
        cam->rotate_Y(rot_angle);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_RIGHT))
        cam->rotate_Y(-rot_angle);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_PERIOD))
        cam->rotate_Z(rot_angle);
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_COMMA))
        cam->rotate_Z(-rot_angle);

    // translation
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_D))
    {
        const auto mag = cam->X() * (slide_offset);
        cam->slide(mag);
    }
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_A))
    {
        const auto mag = cam->X() * (-slide_offset);
        cam->slide(mag);
    }
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_W))
    {
        const auto mag = cam->Y() * (slide_offset);
        cam->slide(mag);
    }
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_X))
    {
        const auto mag = cam->Y() * (-slide_offset);
        cam->slide(mag);
    }
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_Z))
    {
        const auto mag = cam->Z() * (slide_offset);
        cam->slide(mag);
    }
    if(GLFW_PRESS == glfwGetKey(window, GLFW_KEY_S))
    {
        const auto mag = cam->Z() * (-slide_offset);
        cam->slide(mag);
    }

    glfwPollEvents();
}

template <typename T, size_t N>
GLuint upload_data(const T (&data) [N], GLenum target)
{
    /*
     *  VBOs are “buffers” of video memory – just a bunch of bytes containing any kind of binary data you want.
     *  You can upload 3D points, colors, your music collection, poems to your loved ones – the VBO doesn’t care,
     *  because it just copies a chunk of memory without asking what the memory contains.
     */
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(target, vbo);
    glBufferData(target, sizeof(data), &data, GL_STATIC_DRAW);
    glBindBuffer(target, 0);

    return vbo;
}

GLuint upload_texture(const char *file_name, GLenum texture_unit)
{
    gli::texture tex = gli::load(file_name);
    assert(!tex.empty());

    gli::gl gl_profile{gli::gl::PROFILE_GL33};
    gli::gl::format const format = gl_profile.translate(tex.format(), tex.swizzles());

    GLuint texture_id;
    glGenTextures(1, &texture_id);
    glActiveTexture(texture_unit);
    glBindTexture(GL_TEXTURE_2D, texture_id);

    // without setting GL_TEXTURE_MIN_FILTER to GL_LINEAR the plane was rendered black; the default
    // GL_TEXTURE_MIN_FILTER​ state is GL_NEAREST_MIPMAP_LINEAR​. And because OpenGL defines the default
    // GL_TEXTURE_MAX_LEVEL​ to be 1000, OpenGL will expect there to be mipmap levels defined. Since only a few
    // are defined, OpenGL will consider the texture incomplete until the GL_TEXTURE_MAX_LEVEL​ is properly set,
    // or the GL_TEXTURE_MIN_FILTER​ parameter is set to not use mipmaps e.g. GL_LINEAR. Better code would be to
    // use texture storage functions (on OpenGL 4.2+ or ARB_texture_storage) to allocate the texture's storage,
    // then upload with glTexSubImage2D​. This issue applies only to the minification filter, the magnification
    // filter don't have mipmaps ~ opengl.org/wiki/Common_Mistakes
    const auto max_levels = tex.levels() - 1;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_BASE_LEVEL, 0);       // initial value is 0, hence redundant
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, max_levels);

    // since multiple (mipmap) levels are loaded for the texture, setting it to just
    // GL_LINEAR isn't correct as mipmaps will not be used
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, max_levels ? GL_LINEAR_MIPMAP_LINEAR : GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_R, format.Swizzles[0]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_G, format.Swizzles[1]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_B, format.Swizzles[2]);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_SWIZZLE_A, format.Swizzles[3]);

    for(auto i = 0u; i <= max_levels; ++i)
    {
        auto dim = tex.extent(i);
        // the pixel width and height of a texture should be a power of 2
        assert(is_power_of_2(dim.x) && is_power_of_2(dim.y));
        if(gli::is_compressed(tex.format()))
        {
            glCompressedTexImage2D(GL_TEXTURE_2D,
                                   i,
                                   format.Internal,
                                   dim.x,
                                   dim.y,
                                   0,
                                   static_cast<GLsizei>(tex.size(i)),
                                   tex.data(0 /*layer*/, 0 /*face*/, i));
        }
        else
        {
            glTexImage2D(GL_TEXTURE_2D,
                         i,
                         format.Internal,
                         dim.x,
                         dim.y,
                         0,
                         format.External,
                         format.Type,
                         tex.data(0 /*layer*/, 0 /*face*/, i));
        }
    }
    // TODO: add another function upload_texture_mipamp that generates mipmaps even if absent
    // Existing libraries generate mip-maps on the CPU, which is an horrendously slow process. In the past, to avoid
    // this slow process, games used DDS (Microsoft Direct Draw Surface) images, which had mip-maps pre-built in them.
    // In modern graphics we can use the GPU (glGenerateMipmap) to do this - which is extremely efficient.
    // ~ antongerdelan.net/opengl/texturemaps.html

    glBindTexture(GL_TEXTURE_2D, 0);
    return texture_id;
}

template <typename T, size_t attribs>
GLuint setup_attributes(GLuint vertex_buffer,
                        const size_t (&attrib_length) [attribs],
                        GLuint index_buffer)
{
    /*
     * The second step to rendering our triangle is to send the points from the VBO into the shaders. Remember
     * how the VBOs are just chunks of data, and have no idea what type of data they contain? Somehow you have to tell
     * OpenGL what type of data is in the buffer, and this is what VAOs are for. VAOs are the link between the VBOs and
     * the shader variables. VAOs describe what type of data is contained within a VBO, and which shader variables the
     * data should be sent to. glGetAttribLocation or glBindAttribLocation is used to get/set the attribute (shader)
     * variable index. Alternatively, layout location can be used to set the index of a named attribute variable.
     * 
     * If you have used VBOs without VAOs in older versions of OpenGL, then you might not agree with this description of
     * VAOs. You could argue that “vertex attributes” set by glVertexAttribPointer are the link between the VBO and that
     * shaders, not VAOs. It depends on whether you consider the vertex attributes to be “inside” the VAO (which I do),
     * or whether they are global state that is external to the VAO. Using the 3.2 core profile and ATI drivers, the VAO
     * is not optional – glEnableVertexAttribArray, glVertexAttribPointer and glDrawArrays all cause INVALID_OPERATION
     * error, if there is no VAO bound. This is what leads me to believe that the vertex attributes are inside the VAO,
     * and not global state. The 3.2 core profile spec says that VAOs are required, but it's told that only ATI drivers
     * throw errors if no VAO is bound. Here are some quotes from the OpenGL 3.2 core profile specification:
     * 
     *            All state related to the definition of data used by the vertex processor is encapsulated
     *            in a vertex array object.
     * 
     *            The currently bound vertex array object is used for all commands which modify vertex
     *            array state, such as VertexAttribPointer and EnableVertexAttribArray; all commands which
     *            draw from vertex arrays, such as DrawArrays and DrawElements; and all queries of vertex
     *            array state (see chapter 6).
     * 
     * glVertexAttribPointer predates VAOs, so there was a time when vertex attributes were just global state. You could
     * see VAOs as just a way to efficiently change that global state. I prefer to think of it like this: if you don’t
     * create a VAO, then OpenGL provides a default global VAO. So when you use glVertexAttribPointer you are still
     * modifying the vertex attributes inside a VAO, it’s just that you’re modifying the default VAO instead of one you
     * created yourself.
     */

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
    // if a valid buffer is bound to GL_ARRAY_BUFFER, when a VAO is binded this gets saved in it, while the
    // same isn't true for GL_ELEMENT_ARRAY_BUFFER (although the spec says it should); hence bind any buffer
    // only after binding a VAO for them to be captured in it

    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    const auto vertex_size = attribs ? (std::accumulate(attrib_length, attrib_length + attribs, 0u) * sizeof(T)) : 0u;
    size_t offset = 0u;
    for(auto i = 0u; i < attribs; ++i)
    {
        glEnableVertexAttribArray(i);
        glVertexAttribPointer(i,
                              attrib_length[i],
                              GL::type_id(T{}),
                              GL_FALSE,
                              vertex_size,
                              reinterpret_cast<GLvoid*>(offset * sizeof(T)));
        offset += attrib_length[i];
    }
    /*glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), 0);
    glEnableVertexAttribArray(1);
    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), reinterpret_cast<GLvoid*>(3 * sizeof(float)));*/

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);

    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return vao;
}

void upload_transforms(GLuint program, const glm::mat4 &view_xform)
{
    const auto proj_xform = glm::perspective(glm::quarter_pi<float>(),
                                             static_cast<float>(window_width) / window_height,
                                             1.0f,
                                             100.0f);
    const auto PV = proj_xform * view_xform;
    const auto uni_PV = glGetUniformLocation(program, "PV");
    assert(uni_PV != -1);
    glUniformMatrix4fv(uni_PV, 1, GL_FALSE, &PV[0][0]);
}

void render(GLuint vao,
            const Camera &cam,
            GLuint texture,
            GLint  tex_unit,
            GLenum indices_type,
            GLuint program)
{
    // failing to clear GL_DEPTH_BUFFER_BIT will lead to undefined results (mostly blank screen)
    // when depth testing is enabled
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glUseProgram(program);
    glBindVertexArray(vao);

    // when uploading uniforms like view, projection transforms or samplers
    // make sure there's a valid program bound else GL thorws INVALID_OPERATION
    upload_transforms(program, cam.view_xform);

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, texture);
    glUniform1i(tex_unit, 0);

    glDrawElements(GL_TRIANGLES, 3, indices_type, 0);

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindVertexArray(0);
    glUseProgram(0);
}

void render_plane(GLuint vao,
                  const Camera &cam,
                  GLenum indices_type,
                  GLuint program,
                  const glm::vec3& colour)
{
    glUseProgram(program);
    glBindVertexArray(vao);

    upload_transforms(program, cam.view_xform);
    const auto solid_col = glGetUniformLocation(program, "solid_col");
    assert(solid_col != -1);

    glUniform3fv(solid_col, 1, &colour[0]);
    glDrawElements(GL_TRIANGLES, 6, indices_type, 0);

    glBindVertexArray(0);
    glUseProgram(0);
}

void init_rendering()
{
    // enabling culling culls out the plane when trying to view it from underneath hence disabled
    // the reason is that a plane, unlike a model, has no inside and hence culling doesn't make sense here
    // glEnable(GL_CULL_FACE);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

/*
 * By default, OpenGL draws over the top of whatever was previously drawn. If the back side of an object is
 * drawn after the front, then the back will be drawn over the top of the front. With depth buffering enabled,
 * every pixel that is drawn knows how far away from the screen it is. It stores this distance in the depth
 * buffer. When you draw a pixel over an existing pixel, OpenGL will look at the depth buffer to determine
 * which pixel is closer to the screen (or rather which pixel passes the depth test, which is GL_LESS by default).
 * If the new pixel being drawn is closer to the screen, it will overwrite the existing pixel. If the existing
 * pixel is closer to the screen, then the new pixel being drawn is discarded. That is, an existing pixel only
 * gets overwritten if the new pixel is closer to the screen. This is called depth testing.
 */
    glEnable(GL_DEPTH_TEST);
    // glDepthFunc(GL_LESS);    -- the initial value is GL_LESS, hence it's redundant to call this
}

int main(int /*argc*/, char** /*argv*/)
{
    glfwSetErrorCallback(window_error);
    const auto glfw = std::move(GLFW_wrapper{Check_return<decltype(&glfwInit),
                                                          glfwInit,
                                                          decltype(GL_TRUE),
                                                          GL_TRUE>{}}
                               );
    // RAII warpping isn't mandatory here, since glfwTerminate destroys open windows, if any remain
    std::unique_ptr<GLFWwindow,
                    decltype(&glfwDestroyWindow)> main_wnd_ptr{setup_context(window_width, window_height),
                                                               glfwDestroyWindow};

    // http://www.parashift.com/c++-faq-lite/memfnptr-vs-fnptr.html
    // according to C++ FAQ, you cannot pass a member function to a C callback; passing a functor, lambda, boost::bind,
    // etc. is for a C++ callback implemented with templates; instead use the void* user_data allowed in the callback;
    // GLFW gives GLFWwindow->SetWindowUserPointer for the same
    glfwSetKeyCallback(main_wnd_ptr.get(), handle_key);

    glewExperimental = GL_TRUE;
    auto err = glewInit();
    check(GLEW_OK == err, reinterpret_cast<const char*>(glewGetErrorString(err)));
    glGetError();       // to clear error due to GLEW bug #120

#ifndef NDEBUG
    setup_debug(true);
#endif

    // interleave vertex attributes - perhaps more performant
    // http://stackoverflow.com/questions/14874914/performance-gain-using-interleaved-attribute-arrays-in-opengl4-0
    const GLfloat vertices[] = {
                                     // x,    y,    z,      s,    t
                                     0.0f, 0.8f, 0.0f,   0.5f, 1.0f,
                                    -0.8f,-0.8f, 0.0f,   0.0f, 0.0f,
                                     0.8f,-0.8f, 0.0f,   1.0f, 0.0f,
                               };
    const size_t attrib_lengths[] = { 3u, 2u };
    const GLubyte indices[] = { 0, 1, 2 };

    const auto vbo = upload_data(vertices, GL_ARRAY_BUFFER);
    const auto ibo = upload_data(indices, GL_ELEMENT_ARRAY_BUFFER);
    const auto texture = upload_texture("data/RGB.dds", GL_TEXTURE0);
    const auto vao = setup_attributes<GLfloat>(vbo, attrib_lengths, ibo);
    const auto program = ShaderUtil::setup_program("shaders/pt_vs.glsl",
                                             "shaders/tex_fs.glsl");
    const auto tex_unit = glGetUniformLocation(program, "tex_unit");
    assert(tex_unit != -1);

    const GLfloat plane_vert[] = {
                                    7.0f, 0.0f,  7.0f,
                                    7.0f, 0.0f, -7.0f,
                                   -7.0f, 0.0f,  7.0f,
                                   -7.0f, 0.0f, -7.0f
                                 };
    const GLubyte plane_indices[] = { 0, 1, 2,
                                      2, 1, 3 };
    const size_t plane_attrib_lengths[] = { 3u };
    const auto plane_vbo = upload_data(plane_vert, GL_ARRAY_BUFFER);
    const auto plane_ibo = upload_data(plane_indices, GL_ELEMENT_ARRAY_BUFFER);
    const auto plane_vao = setup_attributes<GLfloat>(plane_vbo, plane_attrib_lengths, plane_ibo);
    const auto plane_prgm = ShaderUtil::setup_program("shaders/pt_vs.glsl",
                                                      "shaders/col_fs.glsl");

    Camera cam;
    glfwSetWindowUserPointer(main_wnd_ptr.get(), reinterpret_cast<void*>(&cam));

    init_rendering();
    std::chrono::steady_clock::time_point now, earlier;
    while(!glfwWindowShouldClose(main_wnd_ptr.get()))
    {
        now = std::chrono::steady_clock::now();
        auto const elapsed = std::chrono::duration<float, std::milli>(now - earlier).count();
        render(vao,
               cam,
               texture,
               tex_unit,
               GL::type_id(std::remove_extent<decltype(indices)>::type{}),
               program);

        render_plane(plane_vao,
                     cam,
                     GL::type_id(std::remove_extent<decltype(plane_indices)>::type{}),
                     plane_prgm,
                     glm::vec3{1.0f, 0.0f, 0.0f});

        glfwSwapBuffers(main_wnd_ptr.get());
        handle_input(main_wnd_ptr.get(), &cam, elapsed);
        earlier = now;
    }

    glDeleteProgram(program);
    glDeleteVertexArrays(1, &vao);
    glDeleteTextures(1, &texture);
    glDeleteBuffers(1, &ibo);
    glDeleteBuffers(1, &vbo);
}
