#ifndef __PRECOMP_H__
#define __PRECOMP_H__

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <gli/gli.hpp>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/matrix_access.hpp>

#include <memory>
#include <vector>
#include <algorithm>
#include <string>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <utility>
#include <functional>
#include <cassert>

#endif  // __PRECOMP_H__
