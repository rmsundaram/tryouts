#ifndef __MAIN_HPP__
#define __MAIN_HPP__

inline void window_error(int, const char *error_msg)
{
    throw std::runtime_error(error_msg);
}

inline void check(bool condition, const char *error_msg)
{
    if(condition != true)
    {
        throw std::runtime_error(error_msg);
    }
}

namespace array
{

template <typename T>
inline constexpr typename std::enable_if<std::is_array<T>::value, size_t>::type
max_index(const T& /*arr*/)
{
    return std::extent<T>::value - 1;
}

inline
size_t item_index(size_t item_id,
                  size_t base_id,
                  size_t max_id,
                  size_t err_index)
{
    const auto max_index = max_id - base_id;
    int index = item_id - base_id;
    return ((index < 0) || (static_cast<unsigned>(index) > max_index)) ?
            err_index :
            static_cast<size_t>(index);
}

}

inline std::ostream& tab(std::ostream &os)
{
    return os << '\t';
}

template <typename T>
typename std::enable_if<std::is_integral<T>::value, bool>::type
is_power_of_2(T v)
{
    return (v && !(v & (v - 1)));
}

namespace GL
{

template <typename T>
constexpr GLenum type_id(T);

template <>
constexpr GLenum type_id(GLfloat)
{
    return GL_FLOAT;
}

template <>
constexpr GLenum type_id(GLubyte)
{
    return GL_UNSIGNED_BYTE;
}

}


// Camera with 6 DoF (free-roaming robot); slide and rotate on all 3 axes
// based on F.S.Hill's CG book, §7.3.1 To Fly the Camera Interactively
// the implmentation here is in a straight-forward way for understanding
// the mapping of change to DoF into transformation; however its not
// efficient as it uses glmLookAt for every manipulation; this can be
// optimized further - lookup cam_manip_sans_lookat.cpp
struct Camera
{
    glm::vec3 eye;
    glm::vec3 at;
    glm::vec3 up;
    glm::mat4 view_xform;

    Camera() :
        eye{0.0f, 0.0f, 5.0f},
        at {0.0f, 0.0f, 0.0f},
        up {0.0f, 1.0f, 0.0f},
        // for the above configuration, below matrix would be the mapping
        // from world to view; this transform, when viewed as applied to the
        // coordinate system (post-multiplication/local coordinate)
        // moves view's frame to align with world's frame; note that GLM
        // takes these as columns
        view_xform{1.0f, 0.0f, 0.0f, 0.0f,  // col 1
                   0.0f, 1.0f, 0.0f, 0.0f,  // col 2
                   0.0f, 0.0f, 1.0f, 0.0f,  // col 3
                   0.0f, 0.0f,-5.0f, 1.0f}  // col 4
    {
        //view_xform = glm::lookAt(eye, at, up);
    }

    // http://stackoverflow.com/questions/24007886/are-there-any-consequences-from-using-this-to-initialise-a-class
    // although there're arguments against initializing an object with the operator=, this isn't building an object
    // but just setting its values back to its original form. Also this isn't illegal since we're not changing the
    // the value of the this pointer but the object in the location pointed to by it.
    void reset()
    {
        *this = Camera();
    }

/*
 * view_xform maps world to view i.e. its columns are world's basis expressed in
 * view's basis; based on Dr. Ravi's lecture on gluLookAt and 3D Math Primer's
 * §8.2.2 Direction Cosines, the rows of an orthogonal matrix (like rotation)
 * gives the axes of the target coordinate system in base system; in this case
 * view's basis in terms of world's basis
 */
    glm::vec3 X() const
    {
        return glm::vec3(glm::row(view_xform, 0));
    }

    glm::vec3 Y() const
    {
        return glm::vec3(glm::row(view_xform, 1));
    }

    glm::vec3 Z() const
    {
        return glm::vec3(glm::row(view_xform, 2));
    }

    void slide(glm::vec3 mag)
    {
        eye += mag;
        at += mag;
        view_xform = glm::lookAt(eye, at, up);
    }

/*
 * Older (now commented out) method was relying on the eye, at and up for knowing the view axes in
 * terms of world axes which was not optimal as the axes wouldn't be normalized, also the up vector
 * could've changed during orthogonalization in lookAt instead using matrix rows solves these problems
 * elegantly; see comments and implementation of Camera::X() and friends
 */
    void rotate_X(float angle)
    {
//        const auto view_dir = eye - at;
//        const auto right = glm::cross(up, view_dir);
//        const auto rot = glm::rotate(angle, right);
        const auto rot = glm::rotate(angle, X());
        at = eye + glm::vec3(rot * glm::vec4{at - eye, 0.0f});
        up = glm::vec3(rot * glm::vec4(up, 0.0f));
        view_xform = glm::lookAt(eye, at, up);
    }

    void rotate_Y(float angle)
    {
//        const auto rot = glm::rotate(angle, up);
        const auto rot = glm::rotate(angle, Y());
        at = eye + glm::vec3(rot * glm::vec4{at - eye, 0.0f});
        view_xform = glm::lookAt(eye, at, up);
    }

    void rotate_Z(float angle)
    {
//        const auto view_dir = eye - at;
//        const auto rot = glm::rotate(angle, view_dir);
        const auto rot = glm::rotate(angle, Z());
        up = glm::vec3(rot * glm::vec4(up, 0.0f));
        view_xform = glm::lookAt(eye, at, up);
    }
};

#endif  // __MAIN_HPP__
