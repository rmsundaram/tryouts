Pre-built Win32 binary available in the [Downloads section](https://bitbucket.org/rmsundaram/tryouts/downloads).

SCREENSHOT
==========
![FreeRoamCam.png](https://bitbucket.org/repo/no48Gg/images/1548247714-FreeRoamCam.png "6 DoF free-look camera")

CONTROLS
========

Action                    | Key
--------------------------|------------
Slide camera ±X           | `D`/`A`
Slide camera ±Y           | `W`/`X`
Slide camera ±Z           | `S`/`Z`
Rotate camera X           | `↑`/`↓`
Rotate camera Y           | `←`/`→`
Rotate camera Z           | `,`/`.`
Reset camera              | `R`
Exit                      | `Esc`

LEARNINGS
=========
1. Free-roaming camera (the other version of rotating the camera around an object of interest or crystal ball camera interface was [implemented as HW 1](../CS184.1x/HW/HW2\ OpenGL) in Dr. Ravi's CG course)
2. Depth or z-buffering
3. Polygon face culling
