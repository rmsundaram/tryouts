#version 330

uniform mat4 PV;

layout (location = 0) in vec3 position;
layout (location = 1) in vec2 vert_tex_coord;

out vec2 frag_tex_coord;

void main()
{
    gl_Position = PV * vec4(position, 1.0f);
    frag_tex_coord = vert_tex_coord;
}
