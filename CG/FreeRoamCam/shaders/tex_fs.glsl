#version 330

uniform sampler2D tex_unit;
in vec2 frag_tex_coord;

out vec4 colour;

void main()
{
    colour = texture(tex_unit, frag_tex_coord);
}
