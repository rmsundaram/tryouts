#version 330

uniform vec3 solid_col;

out vec4 colour;

void main()
{
    colour = vec4(solid_col, 1.0f);
}
