Page 1
HOMEWORK 2: OPENGL

Introduction
In this homework, we will be building a complete scene viewer in OpenGL. See the Handouts (from the Course Updates tab) for links to many OpenGL and GLSL resources. This is a more challenging assignment than homework 1. You are allocated 2 weeks, and please start early. You need the two weeks given, and it is likely not possible to do the homework in the last day or two.

Our goal is to take an input scene description that specifies the camera, scene width and height, lights (up to 10 lights), object geometry (for simplicity, only spheres, teapots and cubes) and material properties. The program should render this scene with correct camera positioning and lighting. The user should be able to rotate the camera viewpoint as in the previous assignment, In addition, they should be able to translate and scale the scene.
_______________________________________________________________________________


Page 2
HOMEWORK 2: ASSIGNMENT SPECIFICATION

Assignment Overview
We provide a complete skeleton code. It is more complicated than in homework 1, and one task very early on is that you should try to understand how it works. As noted in the skeleton code, you will be making changes where indicated in four files below:

Transform.cpp: This is similar to the Transform.cpp file in HW1, but now we add code stubs for translations, scales, and perspective projection.
readfile.cpp: This file is for reading the input file (the parser structure is in place, but you need to deal with various other aspects).
display.cpp: This file is for displaying the scene.
shaders/light.frag.glsl: This file is for the actual fragment shader.
We provide two scene files hw1.txt and demo.txt, the first of which corresponds to the scene seen in homework 1, and the second roughly to the demo OpenGL program.

OpenGL and GLM Limitations
Since the goal is to write a modern OpenGL program with minimal deprecated features, and also to learn the concepts, you should not use any OpenGL and GLM functions, with the following exceptions:

OpenGL functions for low-level drawing, as in homework 1. This includes glut functions for setting up buffers, window management and keyboard events.
Basic GLM matrix and vector functions like multiplication, normalization, dot and cross products. The skeleton code has many helpful routines for multiplication etc. Please see it for clarity of GLM rules on row/column major for matrix creation and multiplication.
Very limited use of OpenGL matrix stacks. In particular, you may set the OpenGL matrix mode to perspective or modelview and load a matrix (using a mat4 from GLM). However, you may not call any standard OpenGL commands like pushing and popping transforms, translating, rotating, multiplying the matrix stack etc. You also may not use their GLM equivalents. One goal of this assignment is to maintain the matrix stack yourself, and indeed, this is what modern OpenGL requires.
In particular, OpenGL and GLM provide standard functions for camera positioning, perspective projection, translation, rotation, scaling etc. Your goal is to learn and re-create this pipeline, so you may not use these commands; moreover many of them are deprecated and so should not be used in modern OpenGL.

Submission
Submission will be done similarly to HW0/1, where we asked you to produce screenshots.

Run your HW2 solution with "demo.txt input.txt" as the command line arguments. The first argument specifies the scene file, while the second argument is our test case file. The second argument is only required if you wish to output the test case images.

In the Visual Studio skeletons, the command line argument is already specified (under Project > Properties > Configuration Properties > Debugging > Command Arguments ). The skeletons already have the "input.txt" and "demo.txt" files in the correct location. On OSX/Linux, this can be done by running ./transforms demo.txt input.txt in the homework directory.

Once the program starts, press i and the program will automatically output all the required images (such as "demo.txt.input.txt.001.png"). Finally, add the images to a zip archive "screenshots.zip" and submit this zip archive.
_______________________________________________________________________________


Page 3
File Format
Your program should be run with a single argument: the scene file (a second argument is used only if you want to output images for the autograder/feedback system; that part is handled by the skeleton). Each line in the scene file should be treated separately and can be of the form below. The skeleton code includes the core of a simple parser, and you only need to fill in a few items.

Blank line: Ignore any blank line (that just has white space).
# comment line: Any line with # as its first character is a comment, and should be ignored
command parameter1 parameter2 ...: The first part of the line is always the command (to allow formatting, it may be preceded with white space, as in the example scene files). Depending on the command, it takes in some number of parameters. If the number of parameters is not appropriate for that command (or the command is not recognized) you may print an error message and skip the line.
In general, the program should respond gracefully to errors in the input, but parsing is not the point of this assignment, so we will test your program with well-formed input. The commands that your program must support are the following. We first start with general commands (you may require these be the first commands in the file, as in our examples, but you really shouldn't need to do that. In any case, you can use the core parser in the skeleton):

size width height specifies the width and height of the scene.
camera lookfromx lookfromy lookfromz lookatx lookaty lookatz upx upy upz fovy specifies the camera in the standard way. zmin and zmax are currently hardcoded. Note that this defines both the perspective (given the aspect ratio of width/height and fovy) and modelview transforms for the camera.
Next, you must support the lighting commands. There can be up to 10 lights, that are enabled simply by adding light commands to the file (your program should also gracefully disable lighting if no light commands are specified; the skeleton already includes support for this):

light x y z w r g b a has 8 parameters, the first 4 of which specify the homogeneous coordinates of the light. It should be treated as directional (distant) if w = 0 and as a point light in homogeneous coordinates otherwise. The colors are specified next; note that this is a 4-vector, not just a 3-vector (for now, just set the a or alpha component to 1).
If a user specifies more than 10 lights, you may skip the light lines after the first 10 lights are input.
Shading also requires material properties. Note that these properties will in general be different for each object. Your program must maintain a state (default values can be black), and update the relevant property each time it is specified in the scene file. (Note that this is a simple state which is over-written. It is not affected by geometric transformation commands like push/pop etc.) When an object is drawn, it takes the current material properties. It is possible for example to change only the diffuse color between objects, keeping the same specular color by using only a diffuse command between objects.

ambient r g b a specifies the ambient color
diffuse r g b a specifies the diffuse color of the surface
specular r g b a specifies the specular color of the surface
emission r g b a gives the emissive color of the surface
shininess s specifies the shininess of the surface
We next need commands to specify object geometry. For simplicity, you can assume a maximum number of objects (this should be at least 10; there is no reason not to have a larger number of objects). For now, we are going to use glut commands for spheres, cubes and teapots, so the commands are simply

teapot size makes a teapot of given size
sphere size makes a sphere of given size
cube size makes a cube of given size
For the sphere, one also needs to specify a tessellation internally when calling glutSolidSphere. I use 20. Please see the skeleton code for the specifics of how these primitives are drawn; you don't need to worry about it for the most part.
Finally, we can specify transforms that should be in effect when executing an object above. Note that this also includes lights, as in standard OpenGL (that is, the lights are also acted on by the modelview matrix in standard OpenGL). However, note that the overall scene translation, camera rotation and scale, specified with the keyboard do not act on the lights; only the transforms in the scene file act on both geometry and lights as in standard OpenGL. We will implement a fairly complete set of transformations.

translate x y z A translation 3-vector
rotate x y z ? A rotation of ? degrees about the axis x y z
scale x y z A non-uniform scaling
Note that the transformations can be combined with a sequence of transform commands, e.g., translation, rotation, translation, scale. Any transformation command should right-multiply the current transformation matrix, following OpenGL convention. This convention is confusing, since the first transformation applied is the last one in the code (or the transformation closest to the object command). For example, if one gives commands for translation, rotation, scale (which is the conventional order), then one scales first, then rotates, and then translates. See the skeleton code for helper functions to do the right multiplication, which may also help explain the concepts.

To allow for a hierarchical scene definition, we also define

pushTransform "pushes" the current transformation onto the stack (after we are done with our transforms, we can retrieve it by popping).
popTransform "pops" the transform from the stack (i.e., discards the current transform and goes to the next one on the stack).
Your program must initially load the transform stack with the identity. Note that there are no commands to explicitly set the transformation to the identity or make it a specific value. This is largely to get you to practice good design. In essence, as in the examples, the commands for each object should lie within a pushTransform ... popTransform block. These blocks may also be nested.
_______________________________________________________________________________


Page 4
User Interaction
When your program is run with a scene file, it must interpret it as above, and draw the scene. Most of the basics for user interaction are already in the skeleton. In particular, the program should recognize the same keys as in homework 1 (namely h, +,-,ESC), as well as the following extensions:

g as before, this switches between the glm::lookAt and the user commands. Now, it should also switch between glm::Perspective and your command, and by default it should call your code.
r should reset the transformation to the initial scene.
v should set the motion to correspond to rotating the camera (this should be the default and what you did for homework 1). Note that the camera need no longer be looking at the origin as the center, but you will still move the camera about the origin, keeping the center or look at point fixed.
t should set the motion to translating the scene. In this case, the arrow keys should now move the scene in the x and y directions respectively. The amount should be scaled by 0.01 for this purpose. (Note that amount is initially set to 5).
s should do the same for scaling the scene, again in x and y directions. Again, amount should be scaled by 0.01. By pressing t, v or s one controls whether you modify translation, scale, or view. If one modifies say view, and then hits t to modify translation, the system should remember and keep the view change, i.e., should not revert to the original view.
_______________________________________________________________________________


Page 5
HOMEWORK 2: IMPLEMENTATION HINTS

The assignment itself has been completely specified in the previous pages, along with skeleton code and some examples. What follows below are hints about how to approach the assignment in a step-by-step fashion (the second and third parts are largely independent and can be done in either order). However, you do not strictly need to proceed per the guidance below; it is only highly recommended (and is the basis for the solution program).

Additional Transforms
You should start with implementing additional transforms, beyond those required in homework 1. The skeleton implements the basic functionality; all you need to do is add the appropriate routines to the Transform class for translation and scale. In addition, you must implement the perspective transform yourself, rather than calling the GLM command. These modifications should not be too hard to do. Note also how the skeleton code handles the translation and scale, accounting for the column-major order in OpenGL. You should now have homework 1, except you've written all the transformations yourself, and you can scale and translate the teapot. (If you can't yet get the teapot to display with the HW 2 framework, you may want to use the HW 1 framework instead for now so you can actually see it).

Lighting (Refer Lighting.jpg)
Your next challenge is to implement lighting. At this point, you may want to fill in just enough of the parser to handle the light command. The important aspect is to develop the fragment shader. The example shaders for the demo (homework 0) and homework 1 already include the basic ability to deal with point and directional lights. You just need to declare a uniform to store all the 10 lights, and loop over them, adding the color to the final output. The skeleton includes the basic framework to set up the fragment shader. The shader also takes uniforms for the material properties. The shading equation you should implement is:

I = A + E + Sum(Lighti[D max(N�L, 0) + S (max(N�H, 0)^s)])

where I is the final intensity, A is the ambient term, E is the self-emission, and the diffuse D and specular S are summed over all lights i with intensity Li. N is the surface normal, L is the direction to the light, H is the half-angle, and s is the shininess. You need to be able to compute the direction to the light and half-angle.

Geometry and Transforms
Finally, you need to implement the full file format. The scene and camera commands are pretty easily implemented, simply by inputting the values and then using them in the initialization routine. (Much of this is already done in the skeleton and the parts you need to code are clearly indicated). Note that the camera up vector need not be orthogonal to the direction connecting the eye and center, and you should create a full coordinate frame. (Support for this is already provided in the skeleton and the helper function in Transform.cpp). The material property parameters are easily implemented simply by keeping the current state of material properties and updating it as needed.

For the transform commands, one must maintain a stack of transformations. I recommend doing so with the C++ standard template library (you should look this up if you don't already know it). In particular, my code says stack <mat4> transfstack ; transfstack.push(mat4(1.0)) ;. This defines a stack of mat4 and sets the initial value to the identity. Then, when you encounter a transform, you set up the corresponding glm vector and right-multiply the stack. This is rather confusing because of the row-column switch, so the code actually left-multiples. My code uses a function with body: mat4 & T = transfstack.top() ; T = M * T ; , where M is the transformation matrix. Push and pop transforms just operate on the stack in the standard way. (Many of these functions are already available to you in the skeleton code).

All of this is relevant when an object definition (or light) is reached. For objects, you will store them in an array that includes the material properties (in effect when the object call happens) and the current transformation. Note that this transformation does not include the camera commands in my implementation; instead I multiply that in properly in the display() function. For lights, you will similarly multiply by the transformation matrix to store the transformed light. Again, remember row and column major for matrix-vector multiplication (see the hint in homework 1).

Finally, the display routine loads the camera look at matrix, then sets up the lights, transforming them by this matrix. It then loops over the objects, setting the reflectance properties in the shader, and setting the correct transformation matrix. To set the correct transformation matrix, one needs to consider the overall translation, scaling, as well as camera matrix and the object transform, and concatenate them all properly. Do this yourself, and load it into OpenGL. Finally, you actually draw the object using glutSolidCube, glutSolidSphere, glutSolidTeapot with the size argument (the drawing is already in the skeleton code).

The one remaining element is parsing the scene file, for which the skeleton already provides an almost complete framework. You may use any method, this is not the core part of the assignment. I simply start with the code to read the shader in the demo, and turn each line into a string stream in C++. I check for blank and comment lines using:

if ((str.find_first_not_of("\t\r\n") != string::npos) && (str[0] != '#'))

and if so I just do stringstream s(str) ; s >> cmd ; where cmd is a string that is the command. A sequence of if statements then deals with each command. For parsing the remaining parameters, I have a simple function to read a specified number of parameters from a string and return a failure code if not.
_______________________________________________________________________________


Page 6
Homework 2: FAQ

Below are some FAQs collected from previous iterations of the local Berkeley class.
Feedback System

As in homework 1, the autograder is also intended for use as a feedback system, with the difference images it outputs helping you to pinpoint problems and debug. As with all the assignments, you can submit as many times as you want for feedback; only your final submission will count towards the grade.
GLSL Function Restrictions

The restrictions on OpenGL and GLM functions do not apply to the use of GLSL in GLSL shaders. There are currently no restrictions on using glsl functions in the fragment shader.
Initial Skeleton Code

Until you actually parse the input via readfile.cpp the program doesn't really know what to do, and may not show anything. If you can't yet get the teapot to display with the HW 2 framework, you may want to use the HW 1 framework instead for now so you can actually see it.
Perpsective fovy

The perspective fovy should be specified in degrees.
Passing Lights to Shader

There are two kinds of data being passed to shader program: uniforms and attributes. Attributes are things attached per vertex, whereas uniforms are more like a constant, so things like lights, modelview and projection transforms, or some switches you defined are uniforms.

When you declare a uniform in shader program, to access them from the CPU program you need first get a handle = glGetUniformLocation, and then when you want to set values(mostly during display, especially you have multiple objects whose model transforms are different), you use that handle to specify which variable in the shader program to work with using glUniform()

See this for documentation.
glUniform with arrays

Take a look at this.
On Row and Column Major

Note that the same caveats on row and column major in openGL and GLM still apply. However, if you look in the code, you'll see that there are functions like "rightmultiply" and comments that are provided to help you. Think carefully about the way the matrices are multiplied but if you're not sure, just try it out. you can always switch from row to column major without too much work.
Near and Far plane values

The near and far plane values have been set for you to use in main.cpp as zNear=0.1 and zFar=99.0
Initializing shader values

Please initialize shader values, such as using:

vec4 finalcolor = vec4(0,0,0,1);

If you are experiencing issues with shaders on either your local machine or on the feedback server
How to Debug

Debugging HW 2 can be a bit tricky. If you're having difficulties and don't know how to start debugging, our best advice to you is to proceed systematically . Here are some suggestions to get you started

    First, debug your transformations using the HW 1 as much as possible.
    Try to draw one object at a time with minimal or no transforms and a simple shader (that only returns a constant color or a color calculated from normals only)
    Make sure the silhouette (overall shape) of the scene matches the feed back server
    Then slowly add more transformations and objects, again matching geometry before attempting to match the lighting
    Add the lighting. Try with one light first, then build up from there. Try to match the HW 1 scene first.
===============================================================================
