#include <array>
#include <bitset>
#include <vector>
#include <algorithm>
#include <iostream>

// axis-aligned
// mathematical coordinate system: bottom < top
struct Rect {
    int left, bottom, right, top;
};

struct Point {
    int x, y;
};

enum class PointStatus : unsigned char {
    Outside,
    Inside,
    OnTheLine
};

PointStatus isPointInside(Rect const &rect, Point pt)
{
    if ((rect.left > pt.x) || (rect.right < pt.x) ||
            (rect.bottom > pt.y) || (rect.top < pt.y))
        return PointStatus::Outside;
    if ((rect.left == pt.x) || (rect.right == pt.x) ||
            (rect.bottom == pt.y) || (rect.top == pt.y))
        return PointStatus::OnTheLine;
    return PointStatus::Inside;
}

Point getCentre(Rect const &r)
{
    return { r.left + ((r.right - r.left) / 2),
             r.bottom + ((r.top - r.bottom) / 2) };
}

bool isNull(Rect const &r)
{
    return ((r.right - r.left) <= 0) || ((r.top - r.bottom) <= 0);
}

// the reason for flip in Y-coordinates is we use mathematical coordinate system
// for the rectangle but screen coordinate system for matrix indices
Rect getInnerRect(std::array<int, 4> const &x,
                  std::array<int, 4> const &y,
                  unsigned row,
                  unsigned col)
{
    return { x[col], y[2 - row], x[col + 1], y[3 - row] };
}

/*
 * any two axis-aligned rectangles that intersect can be fit into a 3 × 3
 * matrix; some cases are illustrated here:
 *
 *  A-----+-----A-----\   A-----+-----A-----\   A-----+-----+-----A
 *  |     |     |     |   |     |     |     |   |     |     |     |
 *  |     |     |     |   |     |     |     |   |     |     |     |
 *  +-----B-----------B   +-----B-----------B   +-----B-----B-----+
 *  |     |     |     |   |     |     |     |   |     |     |     |
 *  |     |     |     |   |     |     |     |   |     |     |     |
 *  A-----|-----A-----|   +-----B-----------B   +-----B-----B-----+
 *  |     |     |     |   |     |     |     |   |     |     |     |
 *  |     |     |     |   |     |     |     |   |     |     |     |
 *  \-----B-----------B   A-----+-----A-----/   A-----+-----+-----A
 *
 *  /-----A-----A-----\
 *  |     |     |     |
 *  |     |     |     |
 *  B-----------------B
 *  |     |     |     |
 *  |     |     |     |
 *  B-----------------B
 *  |     |     |     |
 *  |     |     |     |
 *  \-----A-----A-----/
 *
 * Case 1 above is where one vertex of a rectangle is within another; in case 2
 * we've two vertices contained, while case 3 has all four vertices contained.
 * In case 4, although the rectangles are intersecting, no vertex of a triangle
 * is within another.  In all these cases, the common thing is that the centre
 * cell is the intersection region of the rectangles.  The union of each case
 * would be the collection of all cells with vertices not denoted by a slash.
 * Stitching the smaller, inner boxes isn't done but can be done, if needed.
 *
 * The method is to iterate over all 9 cells and check if its centre is within
 * one of the two given rectangles.  However, note that in all these cases the
 * cells of the middle row and column would always belong to one of the two
 * input rectangles, hence the check is needed only for the four corner cells.
 * In case stitching is needed, this information can be used to make bigger
 * rectangles by choosing the better axis of stitch along with the corner which
 * goes best with it.
 *
 * This method is order-agnostic, swapping the order of r1 and r2 should've no
 * effect on the result.  Cases where sides of r1 and r2 are touching, cells
 * will turn out to be null rectangles which should also work fine.
 */

Rect makeMatrix(Rect const &r1,
                Rect const &r2,
                std::array<int, 2> *x_stops,
                std::array<int, 2> *y_stops)
{
    // the rectangle that bounds both r1 and r2
    Rect bbox;
    // use the min left as bbox's left; max left becomes the first X stop
    std::pair<int, int> p = std::minmax(r1.left, r2.left);
    bbox.left = p.first;
    int x1 = p.second;
    // max right is bbox's right; min right becomes the second X stop
    p = std::minmax(r1.right, r2.right);
    bbox.right = p.second;
    int x2 = p.first;
    // do the same with Y too
    p = std::minmax(r1.bottom, r2.bottom);
    bbox.bottom = p.first;
    int y1 = p.second;
    p = std::minmax(r1.top, r2.top);
    bbox.top = p.second;
    int y2 = p.first;
    *x_stops = { x1, x2 };
    *y_stops = { y1, y2 };
    return bbox;
}

template <typename T>
bool intersects(Rect const &r1,
                Rect const &r2,
                T *unionRects,
                size_t *indexOfIntersection)
{
    bool xsect = !((r1.right <= r2.left) ||
                   (r1.left >= r2.right) ||
                   (r1.top <= r2.bottom) ||
                   (r1.bottom >= r2.top));

    if (xsect && unionRects) {
        std::array<int, 2> x_stops, y_stops;
        Rect const &bbox = makeMatrix(r1, r2, &x_stops, &y_stops);
        std::array<int, 4> const x {
            bbox.left,
            x_stops[0],
            x_stops[1],
            bbox.right
        };
        std::array<int, 4> const y {
            bbox.bottom,
            y_stops[0],
            y_stops[1],
            bbox.top
        };
        std::array<int, 9> containerIndices;
        containerIndices.fill(-1);
        std::bitset<9> checkedCell(0x145);    // 101000101
        for (auto i = 0u; i < 3; ++i)
            for (auto j = 0u; j < 3; ++j) {
                Rect const r = getInnerRect(x, y, i, j);
                if (!isNull(r)) {
                    auto const idx = i * 3u + j;
                    constexpr auto In = PointStatus::Inside;
                    if (!checkedCell[idx] ||
                            ((isPointInside(r1, getCentre(r)) == In) ||
                             (isPointInside(r2, getCentre(r)) == In))) {
                        containerIndices[idx] = unionRects->size();
                        unionRects->push_back(r);
                    }
                }
            }
        // cell (1, 1) is always the intersection
        *indexOfIntersection = containerIndices[4];

#ifndef NDEBUG
        std::cout << "  " << ((containerIndices[0] == -1) ? 0 : 1) << ' '
                  << "  " << ((containerIndices[1] == -1) ? 0 : 1) << ' '
                  << "  " << ((containerIndices[2] == -1) ? 0 : 1) << '\n'
                  << "  " << ((containerIndices[3] == -1) ? 0 : 1) << ' '
                  << "  " << ((containerIndices[4] == -1) ? 0 : 1) << ' '
                  << "  " << ((containerIndices[5] == -1) ? 0 : 1) << '\n'
                  << "  " << ((containerIndices[6] == -1) ? 0 : 1) << ' '
                  << "  " << ((containerIndices[7] == -1) ? 0 : 1) << ' '
                  << "  " << ((containerIndices[8] == -1) ? 0 : 1) << "\n\n";
#endif
        /*
         * Some ideas for stitching:
         *
         * 1. In all possible intersection configurations, the central
         * row and column are always part of the union set forming a + shape.
         * The four (checked) corner cells are the four quadrants of the +.
         * Based on which of the corners end up in the union set, a bigger
         * rectangle of size 2 × 2 cells can always be formed.  In some
         * configurations (like case 4 above), more can be added to it based on
         * how many more such corner cells end up being in the union set.
         *
         * 2. If the first or last two elements of arrays x and y are the same,
         * then we know that we've touching rects and hence prune them.
         */
    }

    return xsect;
}

std::ostream& operator<<(std::ostream &os, Rect const& r)
{
    return os << r.left << ", "
           << r.bottom << ", "
           << r.right << ", "
           << r.top;
}

template <typename T>
void printResults(T const &unionRects, size_t intersectionIndex)
{
    for (auto i = 0u; i < unionRects.size(); ++i) {
        std::cout << (i == intersectionIndex ? '*' : ' ')  << ' ';
        std::cout << unionRects[i] << '\n';
    }
}

void testCase(Rect const &r1, Rect const &r2)
{
    std::vector<Rect> unionRects;
    size_t intersectionIndex = std::numeric_limits<size_t>::max();
    if (intersects(r1, r2, &unionRects, &intersectionIndex))
        printResults(unionRects, intersectionIndex);
    else
        std::cout << "No intersection\n";
    std::cout << "____________________\n";
}

int main()
{
    // one vertex within
    testCase({ 14, 130, 66, 214 }, { 48, 102, 83, 174 });
    std::cout << '\n';
    // no vertex within
    testCase({ 48, 102, 483, 174 }, { 114, 8, 248, 647 });
    std::cout << '\n';
    // all vertices within
    testCase({ 0, 0, 100, 100 }, { 25, 25, 50, 50 });
    std::cout << '\n';
    // two vertices within
    testCase({ 0, 0, 100, 100 }, { 25, 25, 180, 50 });
    std::cout << '\n';
    // two vertices within, ceilings touching
    testCase({ 0, 0, 100, 100 }, { 25, 25, 180, 100 });
}
