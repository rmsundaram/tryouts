"use strict";

let redraw = true;
let bg = createRect(0, 0, 800, 600, "White");
let rects = [createRect(130, 140, 160, 180, "Blue"),
             createRect(230, 240, 360, 320, "Green")];
let hitColour = "Coral";
let selectedRect = undefined;
let mouseAt = undefined;
// contact points
let cp1 = undefined, cp2 = undefined;
let needConfiguration = false;

// Dimension-independent Boolean intersection test; unused here.
// Returns true if intersecting, false if disjoint or touching.
function areRectsIntersecting(r1, r2) {
    let r1HalfDim = [getWidth(r1) * 0.5, getHeight(r1) * 0.5];
    let r2HalfDim = [getWidth(r2) * 0.5, getHeight(r2) * 0.5];
    let halfSize = vec2Add(r1HalfDim, r2HalfDim);
    let centreDiff = vec2Abs(vec2Sub(getCentre(r1, r1HalfDim),
                                     getCentre(r2, r2HalfDim)));
    return (centreDiff[0] < halfSize[0]) &&
           (centreDiff[1] < halfSize[1]);
}

// Returns the intersection rectangle between r1 and r2, or false if disjoint.
// Assumes top < bottom i.e. screen coordinates.
// order-independent, dimension-agnostic, trivally exteneds to higher dimensions
// Note: if penetration depth/vector is needed, use Minkowski difference instead
// of deducing it with min(result.width, result.height) which will break if an
// edge of one rect is inside another -- two points inside case.
//                          +---+ r1
//                    +-----•---+------+ r2
//                    |     |   |      |
//                    |     |   |      |
//                    |     +---O      |
//                    |                |
//                    +----------------+
function getIntersection(r1, r2) {
    // 1D intersection between two intervals, cases
    //   ---(------------)-------[-----]----  disjoint
    //   ---(------[-----)-----]------------  intersecting
    //   ---(---[-----------]-----)---------  contained

    // check X intersection; left should be obtained by max, right with min
    let l = Math.max(r1.l, r2.l);
    let r = Math.min(r1.r, r2.r);
    // disjoint in any dimension implies they’re non-intersecting
    if ((r - l) < 0)            // in other words, l > r
        return false;
    let t = Math.max(r1.t, r2.t);
    let b = Math.min(r1.b, r2.b);
    if ((b - t) < 0)
        return false;
    return createRect(l, t, r, b);
}

let RectRectConfiguration = {
    Disjoint                  : 0,   // rectangles are disjoint
    PointInsideOther          : 1,
    TwoPointsInsideOther      : 2,
    OneFullyInsideOther       : 3,
    TwoEdgesInsideOneAnother  : 4,
};

//
// RECT-RECT INTERSECTION WITH CONFIGURATION
// Logic itself is simple but there are many cases that make it hairy. It’s
// complexity is compounded by the anti-commutative requirement.
//
// These many cases only if the actual configuration is to be deduced; if only
// the intersection rectangle is needed, it’s simple and elegant, see
// getIntersection.  Also arriving at the solution becomes lot easier if parsed
// dimension by dimension instead of rect after rect. getIntersection does the
// former, rectRectIntersectionWithConfiguration does the latter.

function rectRectIntersectionOrdered(r1, r2) {
    let r1LInR2 = isPointOnLine(r1.l, [r2.l, r2.r]);
    let r1RInR2 = isPointOnLine(r1.r, [r2.l, r2.r]);
    let r1TInR2 = isPointOnLine(r1.t, [r2.t, r2.b]);
    let r1BInR2 = isPointOnLine(r1.b, [r2.t, r2.b]);

    // at least one point of r1 inside r2
    if ((r1LInR2 || r1RInR2) && (r1TInR2 || r1BInR2)) {
        if (r1LInR2 && r1RInR2) {  // both X points of r1 in r2
            if (r1TInR2 && r1BInR2) {  // both Y points of r1 in r2
                // basically r1 contained by r2
                cp1 = [r1.l, r1.t], cp2 = [r1.r, r1.b];
                return RectRectConfiguration.OneFullyInsideOther;
            }
            // two point inside, but lower or upper edge?
            else if (r1TInR2)  // upper edge
                cp1 = [r1.l, r1.t], cp2 = [r1.r, r2.b];
            else if (r1BInR2)  // lower edge
                cp1 = [r1.l, r2.t], cp2 = [r1.r, r1.b];
            return RectRectConfiguration.TwoPointsInsideOther;
        }
        // order-independent one point cases
        else if (r1LInR2) {
            if (r1TInR2 && r1BInR2) {
                cp1 = [r1.l, r1.t], cp2 = [r2.r, r1.b];
                return RectRectConfiguration.TwoPointsInsideOther;
            }
            else if (r1TInR2) {
                cp1 = [r1.l, r1.t], cp2 = [r2.r, r2.b];
            }
            else {
                cp1 = [r1.l, r2.t], cp2 = [r2.r, r1.b];
            }
            return RectRectConfiguration.PointInsideOther;
        }
        else if (r1RInR2) {
            if ((r1TInR2) && (r1BInR2)) {
                cp1 = [r2.l, r1.t], cp2 = [r1.r, r1.b];
                return RectRectConfiguration.TwoPointsInsideOther;
            }
            else if (r1TInR2)
                cp1 = [r2.l, r1.t], cp2 = [r1.r, r2.b];
            else
                cp1 = [r2.l, r2.t], cp2 = [r1.r, r1.b];
            return RectRectConfiguration.PointInsideOther;
        }
    }
    // X contained and Y not disjoint
    else if ((r1LInR2 && r1RInR2) && ((r1.t <= r2.t) & (r1.b >= r2.b))) {
        cp1 = [r1.l, r2.t], cp2 = [r1.r, r2.b];
        return RectRectConfiguration.TwoEdgesInsideOneAnother;
    }
    // Reaches here if
    //   disjoint
    //   r2 containing r1
    //     entirely (OneFullyInsideOther)
    //     in X (TwoPointsInsideOther or TwoEdgesInsideOneAnother)
    return false;
}

// Returns the current configuration of r1 and r2.
// Sets cp1 such that it’s always inside and cp2 outside w.r.t r1.
// Assuming r1 to be the body that would be moved to resolve the collision.
function rectRectIntersectionWithConfiguration(r1, r2) {
    let result = rectRectIntersectionOrdered(r1, r2);
    if (!result)  // disjoint or false
        result = rectRectIntersectionOrdered(r2, r1);  // swap and try again
    // if false was returned twice, the rectangles are disjoint
    return result || RectRectConfiguration.Disjoint;
}

// driver function
function rectRectIntersection(r1, r2) {
    cp1 = cp2 = undefined;
    if (!needConfiguration) {
        let resultRect = getIntersection(r1, r2);
        if (resultRect) {
            cp1 = [resultRect.l, resultRect.t];
            cp2 = [resultRect.r, resultRect.b];
        }
        return resultRect ? true : false;
    }
    return rectRectIntersectionWithConfiguration(r1, r2);
}

// UTILITIES

function hitTest(pt, rect) {
    return (pt[0] >= rect.l) && (pt[0] <= rect.r) &&
        (pt[1] >= rect.t) && (pt[1] <= rect.b);
}

function vec2Add(v1, v2) {
    return [v1[0] + v2[0],
            v1[1] + v2[1]];
}

// v1 - v2, a vector from v2 to v1
function vec2Sub(v1, v2) {
    return [v1[0] - v2[0],
            v1[1] - v2[1]];
}

function vec2Abs(v) {
    return [Math.abs(v[0]), Math.abs(v[1])];
}

function getWidth(rect) {
    return rect.r - rect.l;
}

function getHeight(rect) {
    return rect.b - rect.t;
}

function getCentre(r, halfDims) {
    return [(r.l + halfDims[0]), (r.t + halfDims[1])];
}

function isPointOnLine(pt, line) {
    return (pt >= line[0]) && (pt <= line[1]);
}

function createRect(left, top, right, bottom, fill) {
    return { colour: fill, l: left, b: bottom, r: right, t: top };
}

function fillRect(ctx, rect, colour) {
    ctx.fillStyle = colour || rect.colour;
    ctx.fillRect(rect.l, rect.t, rect.r - rect.l, rect.b - rect.t);
}

function drawRect(ctx, rect) {
    ctx.strokeStyle = rect.colour;
    ctx.strokeRect(rect.l, rect.t, rect.r - rect.l, rect.b - rect.t);
}

function drawCircle(ctx, pt, radius, colour) {
    radius = radius || 3;
    ctx.beginPath();
    ctx.arc(pt[0], pt[1], radius, 0, 2 * Math.PI);
    // without closePath the circle isn’t completed properly
    ctx.closePath();
    colour = colour || "Blue";
    ctx.fillStyle = colour;
    ctx.fill();
}

function drawLine(ctx, p1, p2, colour) {
    ctx.beginPath();
    ctx.moveTo(p1[0], p1[1]);
    ctx.lineTo(p2[0], p2[1]);
    colour = colour || "Red";
    ctx.strokeStyle = colour;
    ctx.stroke();
}

function render(ctx, depthElement, xsectResult) {
    fillRect(ctx, bg, "white");
    drawRect(ctx, rects[0]);
    drawRect(ctx, rects[1]);

    if (xsectResult) {
        fillRect(ctx, createRect(Math.min(cp1[0], cp2[0]),
                                 Math.min(cp1[1], cp2[1]),
                                 Math.max(cp1[0], cp2[0]),
                                 Math.max(cp1[1], cp2[1]),
                                 "rgba(255, 0, 0, 0.3)"));
        ctx.setLineDash([10, 5]);
        drawLine(ctx, cp1, cp2, "black");
        ctx.setLineDash([]);
        drawCircle(ctx, cp1, undefined, "red");
        drawCircle(ctx, cp2);

        // update depth information here
        let w = (cp2[0] - cp1[0]);
        let h = (cp2[1] - cp1[1]);
        depthElement.innerHTML = "<" + w + ", " + h + ">";
    }
    else
        depthElement.innerHTML = "infinity";
}

function init2DCanvas(canvas) {
    let ctx;
    try {
        ctx = canvas.getContext("2d");
    }
    catch (e) {
        alert("Unable to initialize Canvas. Your browser may not support it.");
    }
    return ctx;
}

function start() {
    let canvas = document.getElementById("canvas");
    let depthElement = document.getElementById("depth");
    let ctx = init2DCanvas(canvas);
    let loop = function(/* time*/) {
        if (redraw) {
            let xsectResult = rectRectIntersection(rects[0], rects[1]);
            render(ctx, depthElement, xsectResult);
            redraw = false;
        }
        window.requestAnimationFrame(loop);
    };

    canvas.addEventListener("mousemove", handleMouseMove, false);
    canvas.addEventListener("mousedown", handleMouseDown, false);
    canvas.addEventListener("mouseup", handleMouseUp, false);

    loop(0);
}

function moveRect(r, x, y) {
    r.l += x;
    r.r += x;
    r.b += y;
    r.t += y;
}

function sub(p1, p2) {
    return [p1[0] - p2[0], p1[1] - p2[1]];
}

function handleMouseMove (e) {
    let pt = getCursorPosition(e);
    if (selectedRect === undefined)
        e.target.style.cursor =
        (hitTestRects(pt) !== undefined) ? "move" : "auto";
    else {
        let delta = sub(pt, mouseAt);
        moveRect(rects[selectedRect], delta[0], delta[1]);
        mouseAt = pt;
        redraw = true;
    }
}

function hitTestRects(pt) {
    for (let i = 0, len = rects.length; i < len; ++i)
        if (hitTest(pt, rects[i])) return i;
    return undefined;
}

function handleMouseDown(e) {
    let pt = getCursorPosition(e);
    selectedRect = hitTestRects(pt);
    mouseAt = pt;
}

function handleMouseUp(e) {
    selectedRect = undefined;
}

// http://stackoverflow.com/a/18053642
function getCursorPosition(event) {
    let canvas = event.target;
    let rect = canvas.getBoundingClientRect();
    let x = event.clientX - rect.left;
    let y = event.clientY - rect.top;
    return [x, y];
}
