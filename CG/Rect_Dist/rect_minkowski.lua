#! /usr/bin/env lua

require 'math'

-- Assumes top > bottom i.e. math, not screen, coordinates.
-- Sorting unneeded as Minkowski difference is order-independent.
-- Of course, the resulting figure will be shifted if the order is flipped
-- but the overall result will be the same.
-- Notice that diff’s width = sum of both rect widths; likewise for height.
function minkowski_diff(a, b)
  -- compute the left, right, top and bottom most values
  -- by doing A - B; note: opposite coordinate in B gives the maximum
  local left = a.left - b.right
  local right = a.right - b.left
  local top = a.top - b.bottom
  local bottom = a.bottom - b.top
  print("Minkowski difference: ", left, top, right, bottom)

  -- swap bottom and top to change to screen coordinates
  if (left < 0) and (right > 0) and (bottom < 0) and (top > 0) then
    io.write("  Rectangles intersecting, ")
    print("penetration vector: ", penetration_vector(make_rect {left,
                                                                top,
                                                                right,
                                                                bottom}))
  elseif (left == 0) or (right == 0) or (bottom == 0) or (top == 0) then
    print("  Rectangles touching but not intersecting")
  else
    print("  Rectangles disjoint")
  end
end

-- penetration vector to be applied on B to resolve its collision with A
function penetration_vector(minkowski_diff)
  -- flip signs on bottom and top to change to screen coordinates
  local magnitude = math.min(-minkowski_diff.left,
                             -minkowski_diff.bottom,
                             minkowski_diff.right,
                             minkowski_diff.top)
  if (-magnitude == minkowski_diff.left) then
    return "<" .. minkowski_diff.left .. ", 0>"
  elseif (magnitude == minkowski_diff.right) then
    return "<" .. minkowski_diff.right .. ", 0>"
  elseif (-magnitude == minkowski_diff.bottom) then
    return "<0, " .. minkowski_diff.bottom .. ">"
  else
    return "<0, " .. minkowski_diff.top .. ">"
  end
end

function make_rect(t)
  return {left = t[1], top = t[2], right = t[3], bottom = t[4]}
end

-- point inside
a = make_rect {2, 4, 5, 2}
b = make_rect {4, 3, 6, 1}
minkowski_diff(a, b)

-- two points inside
a = make_rect {4, 5, 7, 3}
b = make_rect {5, 4, 6, 2}
minkowski_diff(a, b)

-- total containment
a = make_rect {-5, 5, 5, -5}
b = make_rect {0, 4, 3, -2}
minkowski_diff(a, b)

-- crossed
a = make_rect {3, 3, 6, 1}
b = make_rect {4, 4, 5, 0}
minkowski_diff(a, b)

-- touching
a = make_rect {2, 4, 5, 2}
b = make_rect {4, 2, 6, 0}
minkowski_diff(a, b)

-- disjoint
a = make_rect {3, 3, 6, 1}
b = make_rect {0, 4, 2, 0}
minkowski_diff(a, b)

-- two points inside case, screen coordinates case
a = make_rect {2, 2, 8, 7}
b = make_rect {4, 4, 6, 9}
minkowski_diff(a, b)
