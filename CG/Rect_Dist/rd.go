// shortest distance between two 2D AABBs;
// see rect_dist.html for an interactive workout

package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
)

// Rect is a 2D AABB; left < right, bottom < top
type Rect struct {
	left, bottom, right, top float64
}

// TOPL §2.1 says “new” is a predeclared name which can be used for tokens
func new(dims []string) Rect {
	l, _ := strconv.ParseFloat(dims[0], 64)
	b, _ := strconv.ParseFloat(dims[1], 64)
	r, _ := strconv.ParseFloat(dims[2], 64)
	t, _ := strconv.ParseFloat(dims[3], 64)
	return Rect{left: l, bottom: b, right: r, top: t}
}

func dist(x1, y1, x2, y2 float64) float64 {
	xSqr := (x1 - x2) * (x1 - x2)
	ySqr := (y1 - y2) * (y1 - y2)
	return math.Sqrt(xSqr + ySqr)
}

// ShortestDist returns the least distance to reach a rectangle from another
func ShortestDist(r1, r2 Rect) float64 {
	r2Beyondr1X := r1.right < r2.left
	r1Beyondr2X := r2.right < r1.left
	r2Beyondr1Y := r1.top < r2.bottom
	r1Beyondr2Y := r2.top < r1.bottom

	// When there’s no distance between them in both dimensions, they’re
	// intersecting.  When they overlap in one direction, the distance is
	// calculated as the difference in the other dimension.  Finally, when they
	// are apart in both dimensions, we find the Euclidean norm using distances
	// in both dimensions.
	d := 0.0 // the rectangles overlap
	switch {
	// r2 in r1’s right top corner
	case r2Beyondr1X && r2Beyondr1Y:
		d = dist(r1.right, r1.top, r2.left, r2.bottom)
		// r2 in r1’s right bottom corner
	case r2Beyondr1X && r1Beyondr2Y:
		d = dist(r1.right, r1.bottom, r2.left, r2.top)
		// r2 in r1’s left bottom corner
	case r1Beyondr2X && r1Beyondr2Y:
		d = dist(r1.left, r1.bottom, r2.right, r2.top)
		// r2 in r1’s left top corner
	case r1Beyondr2X && r2Beyondr1Y:
		d = dist(r1.left, r1.top, r2.right, r2.bottom)
		// r2 to r1’s right; overlaps in Y
	case r2Beyondr1X:
		d = r2.left - r1.right
		// r2 to r1’s left; overlaps in Y
	case r1Beyondr2X:
		d = r1.left - r2.right
		// r2 to r1’s top; overlaps in X
	case r2Beyondr1Y:
		d = r2.bottom - r1.top
		// r2 to r1’s bottom; overlaps in X
	case r1Beyondr2Y:
		d = r1.bottom - r2.top
	}
	return d
}

func main() {
	if len(os.Args) == 9 {
		r1 := new(os.Args[1:5])
		r2 := new(os.Args[5:])
		fmt.Println(r1, "and", r2, "are", ShortestDist(r1, r2), "units apart")
	} else {
		fmt.Println("Prints the shortest distance" +
			" between two axis-aligned rectangles")
		fmt.Println("usage: rd l1 b1 r1 t1 l2 b2 r2 t2")
	}
}
