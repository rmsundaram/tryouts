_Nila_, [Tamil][] for moon in my mother tongue, is a toy program plotting the
course of Moon around Sun.  It’s cross-platform: Linux, macOS and Windows are
supported.

This isn’t a physically correct simulation; [Moon’s course around Sun is
actually convex][moon-orbit-sun] -- more or less a circle 🤩.

![Nila](./data/screenshot.png "screenshot")

# Options

| Short | Long            | Description                                                     |
|-------|-----------------|-----------------------------------------------------------------|
| `-p`  | `--plot`        | Plot courses of Moon and Earth (default: true)                  |
| `-d`  | `--dots`        | Plot courses with points, not (default) lines; implies `-p`     |
| `-c`  | `--circular`    | Make Earth's course circular instead of elliptical              |
| `-r`  | `--course arg`  | Choose courses to plot; 0: both, 1: Moon, 2: Earth (default: 0) |
| `-e`  | `--earth`       | Plot only Earth's course                                        |
| `-m`  | `--multicolour` | Draw each celestial body in two colours                         |
| `-h`  | `--help`        | Print usage                                                     |

# Build

``` shell
cmake -B build -DCMAKE_BUILD_TYPE=Release
cmake --build build -t run
```

should fetch code, build and run the program.

Debug builds are built without optimizations and symbols.  Export
`compile_commands.json` to use with your editor/LSP.

``` shell
cmake -B build -DCMAKE_EXPORT_COMPILE_COMMANDS=1
```

You can build with sanitizers (`-DWITH_SANITIZERS=1`).

# Dependencies

| Tool         | Dependency Type |
|--------------|-----------------|
| C++ compiler | Build           |
| CMake        | Build           |
| OpenGL       | Run             |
| Git          | Configure       |
| _Internet_   | Configure       |

Third-party library dependencies that’re automatically fetched:

* [tigr][] -- windowing, drawing and eventing
* [cute_math][] -- 2D math (vector, matrix and transforms)
* [cxxopts][] [optional] -- command-line arguments

# 2D Drawing Libraries

Following simple, cross-platform 2D graphics C++ libraries were surveyed:

1. [tigr][] [C]
    - Linux, Windows, macOS, iOS, Android
    - Fragment shader access
    - PNG load/save
    - Mouse/keyboard input
    - Bitmap font rendering
    - Direct (lockless) access to bitmaps

2. [fbg][] [C]
    - Developed with only Linux in mind; include GLFW backend for cross-platform story
    - Supports multi-core parallelism; software GPU in a sense
    - Needs [lodepng][] and [nanojpeg][] sources along with its own source and header

* [minifb][] [C]
    - Linux, Windows, macOS, FreeBSD, iOS, Android
    - Very bare bones; only gives a framebuffer pointer `uint32_t*`
    - Window, keyboard/mouse event callbacks
    - Timers and FPS target

## Vector Rasterizers

Non-windowing APIs rendering to an image/backend.

1. [canvas-ity][] [C++]
    - Linux, Windows and macOS
    - Single-header
    - Closely matches HTML5 canvas API
    - Depends on C++ stdlib
    - Trapezoidal anti-aliasing
    - Gamma-correct since it linearizes all colours
    - Premultiplies alpha on input and unpremultiplied sRGB to output
    - Zero dynamic memory
    - Works with RTTI and exceptions OFF.
  
2. [tarp][] [C]
    - Almost single-header

## Misc: Non-C++

| Library    | Language |
|------------|----------|
| [Canvas][] | HTML5/JS |
| [Löve2D][] | Lua      |
| [notan][]  | Rust     |

[Canvas][] is awesome; requires zero setup.  Just a text editor and a browser.

## Misc: Engines

Missing from above options are non-single header, serious options like

| Library     | Facilities                                |
|-------------|-------------------------------------------|
| [GLFW][]    | Windowing, input handling                 |
| [SDL][]     | Windowing, input handling                 |
| [Allegro][] | Windowing, input handling                 |
| [Cairo][]   | 2D drawing on multiple backends           |
| [FLTK][]    | Windowing, intput handling and 2D drawing |
| [raylib][]  | 2D game engine; graphics, audio and more  |

# TODO

1. Expose options to
    - Animate only Moon, Earth or Both
    - Set number of Moon revolutions w.r.t Earth’s
    - Set offset of Moon from Earth
    - Alter rotation speed
    - Mimic reality!
2. Write man pages
3. Installation story
4. Add glow to Sun and Moon

## Build

Integrate following tools:

- IWYU
- clang-tidy
- clang-format


[fbg]: https://github.com/grz0zrg/fbg
[minifb]: https://github.com/emoon/minifb
[canvas-ity]:  https://github.com/a-e-k/canvas_ity
[lodepng]: https://lodev.org/lodepng/
[nanojpeg]: https://keyj.emphy.de/nanojpeg/
[tigr]: https://github.com/erkkah/tigr
[cute_math]: https://github.com/RandyGaul/cute_headers
[cairo]: https://www.cairographics.org/
[fltk]: https://www.fltk.org/
[tarp]: https://github.com/mokafolio/Tarp
[Tamil]: https://en.wikipedia.org/wiki/Tamil_language
[moon-orbit-sun]: https://physics.stackexchange.com/q/266426
[GLFW]: https://www.glfw.org/
[SDL]: https://libsdl.org/
[Allegro]: https://liballeg.org/
[allegro-primitives]: https://liballeg.org/a5docs/trunk/primitives.html
[raylib]: https://www.raylib.com/
[canvas]: https://developer.mozilla.org/en-US/docs/Web/API/CanvasRenderingContext2D
[Löve2D]: https://love2d.org/
[notan]: https://github.com/Nazariglez/notan
[cxxopts]: https://github.com/jarro2783/cxxopts

[^1]: Allegro has a [Primitives][allegro-primitives] add-on for drawing.
