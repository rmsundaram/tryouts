cmake_minimum_required(VERSION 3.16)

project(
  nila
  VERSION 0.4.7
  DESCRIPTION "Display animation of Moon's course around Earth and Sun."
  HOMEPAGE_URL "https://bitbucket.org/rmsundaram/tryouts"
)

# Deter in-source builds
if (CMAKE_SOURCE_DIR STREQUAL CMAKE_BINARY_DIR)
  message(FATAL_ERROR
    "In-source builds are not supported.  "
    "Please read documentation before trying to build this project. "
    "Delete 'CMakeCache.txt' and 'CMakeFiles/' before proceeding.")
endif()

# Set a default build type if none was specified
if (NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to 'Debug' as none was specified.")
  set(CMAKE_BUILD_TYPE Debug CACHE STRING "Choose the type of build." FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release"
    "MinSizeRel" "RelWithDebInfo")
endif()

include(CheckIPOSupported)
check_ipo_supported(RESULT lto_supported)
if (lto_supported)
  # Enable LTO for all toolchains.
  # CMake 3.9-introduced CMP0069, enforce IPO when enabled, defaults to NEW as
  # minimum required version is 3.9+. Generating CMake’s version is irrelevant.
  # https://stackoverflow.com/q/42922905/183120
  set(CMAKE_INTERPROCEDURAL_OPTIMIZATION_RELEASE TRUE)
  set(CMAKE_INTERPROCEDURAL_OPTIMIZATION_RELWITHDEBINFO TRUE)
endif ()
# Needs CMake 3.24; works with Makefile and Ninja generators only
set(CMAKE_COLOR_DIAGNOSTICS ON)

find_package(OpenGL REQUIRED)

# Download tigr source
include(FetchContent)
# Set DOWNLOAD_EXTRACT_TIMESTAMP OFF on CMake < 3.24.
if (POLICY CMP0135)
  cmake_policy(SET CMP0135 NEW)
endif()
FetchContent_Declare(tigr
  URL         https://github.com/erkkah/tigr/archive/refs/tags/v3.0.0.tar.gz
  URL_HASH    MD5=eabe698ecb57ca534696e5c10f07eb3f
)
FetchContent_MakeAvailable(tigr)

# Define library: tigr
add_library(tigr
  ${tigr_SOURCE_DIR}/tigr.c
  ${tigr_SOURCE_DIR}/tigr.h
)
target_link_libraries(tigr PUBLIC
  OpenGL::GL
  $<$<PLATFORM_ID:Windows>:gdi32>
)
if (CMAKE_SYSTEM_NAME STREQUAL "Linux")
  find_package(X11 REQUIRED)
  target_link_libraries(tigr PUBLIC OpenGL::GLU X11::X11)
elseif (CMAKE_SYSTEM_NAME STREQUAL "Darwin")
  find_library(COCOA_FRAMEWORK Cocoa)
  target_link_libraries(tigr PUBLIC Cocoa)
endif ()
target_include_directories(tigr SYSTEM PUBLIC
  $<BUILD_INTERFACE:${tigr_SOURCE_DIR}>
  $<INSTALL_INTERFACE:include>)
add_library(tigr::lib ALIAS tigr)

# Get cute math from cute_headers and setup a header-only library
# https://jonathanhamberg.com/post/2019-12-27-cmake-header-dependencies/
file(DOWNLOAD
  https://raw.githubusercontent.com/RandyGaul/cute_headers/master/cute_math.h
  ${CMAKE_CURRENT_BINARY_DIR}/cute_math.h
)
add_library(cute_math INTERFACE)
target_include_directories(cute_math SYSTEM INTERFACE
  ${CMAKE_CURRENT_BINARY_DIR}
)
add_library(cute::math ALIAS cute_math)

option(WITH_ARGS "Build with command-line arguments." ON)
if (WITH_ARGS)
FetchContent_Declare(cxxopts
  URL         https://github.com/jarro2783/cxxopts/archive/refs/tags/v3.0.0.tar.gz
  URL_HASH    MD5=4c4cb6e2f252157d096fe18451ab451e
)
FetchContent_MakeAvailable(cxxopts)
endif ()

configure_file(src/config.h.in config.h NEWLINE_STYLE UNIX)
# Define executable: nila
add_executable(${PROJECT_NAME}
  src/main.cpp
  src/body.h
  src/scene.h
  src/scene.cpp
  src/types.h
  src/config.h.in
  ${CMAKE_CURRENT_BINARY_DIR}/config.h
)
target_link_libraries(${PROJECT_NAME} PRIVATE
  tigr::lib
  cute::math
  # https://stackoverflow.com/q/46793464/183120
  $<$<BOOL:${WITH_ARGS}>:cxxopts::cxxopts>
)
target_compile_definitions(${PROJECT_NAME} PRIVATE
  CXXOPTS_NO_EXCEPTIONS
  CXXOPTS_NO_RTTI
)
target_compile_options(${PROJECT_NAME} PRIVATE
  # Turn off RTTI and exceptions
  $<$<CXX_COMPILER_ID:GNU,Clang,AppleClang>:-fno-rtti -fno-exceptions>
  $<$<CXX_COMPILER_ID:MSVC>:/GR->  # MSVC has exceptions OFF by default
  # Enable warnings: https://foonathan.net/2018/10/cmake-warnings/
  $<$<CXX_COMPILER_ID:GNU,Clang,AppleClang>:-Wall -Wextra -pedantic -Wshadow
  -pedantic-errors -Wold-style-cast -Wnull-dereference -Wsuggest-override
  -Wextra-semi -Wdisabled-optimization -Woverloaded-virtual -Wundef -Wformat=2
  -Wsign-promo -Wsign-conversion -Wcast-align -Wconversion -Wcast-qual
  -Wdouble-promotion -Wno-div-by-zero -Wctor-dtor-privacy -Wredundant-decls
  -Wstrict-overflow=2>
  # GCC-specific warnings
  $<$<CXX_COMPILER_ID:GNU>:-Wvector-operation-performance -Wlogical-op
  -Wuseless-cast -Wstrict-null-sentinel -Wnoexcept -Wsuggest-final-types
  -Wsuggest-final-methods>
  # MSVC-specific warnings
  $<$<CXX_COMPILER_ID:MSVC>:/W4>
  # Release-specific flags: https://stackoverflow.com/a/50433007/183120
  # -ffast-math isn’t automatically enabled by any -O variants in GCC
  # https://gcc.gnu.org/onlinedocs/gcc/Optimize-Options.html#Optimize-Options
  $<$<CXX_COMPILER_ID:GNU,Clang,AppleClang>:$<$<CONFIG:Release>:-ffast-math>>
  # Disable optimizations for Debug builds
  $<$<CONFIG:Debug>:$<$<CXX_COMPILER_ID:GNU,Clang,AppleClang>:-O0>>
  $<$<CONFIG:Debug>:$<$<CXX_COMPILER_ID:MSVC>:/Od>>
)
target_compile_features(${PROJECT_NAME} PRIVATE cxx_std_17)
set_target_properties(${PROJECT_NAME} PROPERTIES
  CXX_EXTENSIONS OFF
  CXX_STANDARD_REQUIRED ON
  # Prohibit Windows from providing a console window if built without WITH_ARGS
  # and it’s not a Release build; pass /SUBSYSTEM:WINDOWS (MSVC) /
  # -mwindows (MinGW).
  # Refer: https://stackoverflow.com/a/74491601/183120
  WIN32_EXECUTABLE $<AND:$<PLATFORM_ID:Windows>,$<CONFIG:Release>,$<NOT:$<BOOL:${WITH_ARGS}>>>
)
# MSVC’s CRT expects WinMain, not main, as entry point.  Override /ENTRY (MSVC)
# or --entry (MinGW).
target_link_options(${PROJECT_NAME} PRIVATE
  $<$<AND:$<CXX_COMPILER_ID:MSVC>,$<CONFIG:Release>,$<NOT:$<BOOL:${WITH_ARGS}>>>:/ENTRY:mainCRTStartup>
)
# Set executable project as startup instead of the ALL_BUILD in solution.
# https://github.com/genpfault/sdl-base/blob/main/CMakeLists.txt
set_property(DIRECTORY PROPERTY VS_STARTUP_PROJECT ${CMAKE_PROJECT_NAME})

# TODO: Enable clang-tidy and clang-format

# https://github.com/a-e-k/canvas_ity/blob/main/test/CMakeLists.txt
option(WITH_SANITIZERS
  "Build with undefined behavior, address, and/or integer sanitizer")
if (WITH_SANITIZERS)
  # MinGW doesn’t have sanitizers; uptil Clang 15.0.2 on Windows
  # doesn’t have leak sanitizer.
  # https://stackoverflow.com/q/31144000/183120
  target_compile_options(${PROJECT_NAME} PRIVATE
    $<$<OR:$<CXX_COMPILER_ID:Clang,AppleClang>,$<AND:$<CXX_COMPILER_ID:GNU>,$<NOT:$<PLATFORM_ID:Windows>>>>:-fsanitize=undefined -fsanitize=address>
    $<$<AND:$<CXX_COMPILER_ID:Clang,GNU>,$<NOT:$<PLATFORM_ID:Windows>>>:-fsanitize=leak>
    $<$<CXX_COMPILER_ID:Clang,AppleClang>:-fsanitize=integer>
    $<$<CXX_COMPILER_ID:MSVC>:/fsanitize=address /MTd>
  )
  target_link_options(${PROJECT_NAME} PRIVATE
    $<$<OR:$<CXX_COMPILER_ID:Clang,AppleClang>,$<AND:$<CXX_COMPILER_ID:GNU>,$<NOT:$<PLATFORM_ID:Windows>>>>:-fsanitize=undefined -fsanitize=address>
    $<$<AND:$<CXX_COMPILER_ID:Clang,GNU>,$<NOT:$<PLATFORM_ID:Windows>>>:-fsanitize=leak>
    $<$<CXX_COMPILER_ID:Clang,AppleClang>:-fsanitize=integer>
    $<$<CXX_COMPILER_ID:MSVC>:/INCREMENTAL:NO>
  )
endif()

# Ameliorate single-header library compile times with pre-compiled headers.
# Refer manual about $<ANGLE-R> quirk:
# https://cmake.org/cmake/help/v3.24/command/target_precompile_headers.html
target_precompile_headers(${PROJECT_NAME} PRIVATE
  <tigr.h>
  <cute_math.h>
  $<$<BOOL:${WITH_ARGS}>:<cxxopts.hpp$<ANGLE-R>>
)

add_custom_target(run
  DEPENDS ${PROJECT_NAME}
  COMMAND ${PROJECT_NAME}
  WORKING_DIRECTORY "${CMAKE_BINARY_DIR}"
  VERBATIM)
