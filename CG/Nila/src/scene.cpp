#include "scene.h"

Scene Scene::make(bool circular_orbit) {
  std::vector<Body> bodies{3};
  // Moon
  bodies[2].m_colour1 = tigrRGB(0xFF, 0xFF, 0xE0);  // light yellow
  bodies[2].m_colour2 = tigrRGB(0xFA, 0xFA, 0xD2);  // pale goldenrod
  bodies[2].m_offset_x = 15;
  bodies[2].m_offset_y = circular_orbit ? bodies[2].m_offset_x : 7;
  bodies[2].m_size = 3;
  // Earth
  bodies[1].m_colour1 = tigrRGB(0x20, 0xB2, 0xAA);  // light sea green
  bodies[1].m_colour2 = tigrRGB(0x98, 0xFB, 0x98);  // dark sea green
  bodies[1].m_offset_x = 300;
  bodies[1].m_offset_y = circular_orbit ? bodies[1].m_offset_x : 150;
  bodies[1].m_child = 2;
  bodies[1].m_size = 10;
  // Sun
  bodies[0].m_colour1 = tigrRGB(0xFF, 0xA5, 0x00);  // orange
  bodies[0].m_colour2 = tigrRGB(0xFF, 0x8C, 0x00);  // dark orange
  bodies[0].m_size = 50;
  bodies[0].m_child = 1;

  Course c_earth; c_earth.m_colour = tigrRGB(0x6E, 0x7B, 0x8B);
  Course c_moon; c_moon.m_colour = tigrRGB(0xDB, 0xDB, 0xC1);

  return { bodies, { c_earth, c_moon } };
}
