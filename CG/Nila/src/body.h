#pragma once

#include "types.h"

#include <cstdint>
#include <optional>
#include <cmath>

#include "tigr.h"

using BodyId = std::optional<uint8_t>;

struct Body {
  float    m_angle    = 0.0f;
  TPixel   m_colour1  = tigrRGB(0, 0, 0);
  TPixel   m_colour2  = tigrRGB(0, 0, 0);
  BodyId   m_child    = std::nullopt;
  uint16_t m_size     = 0;
  uint16_t m_offset_x = 0;
  uint16_t m_offset_y = 0;

  Vec pos() const {
    return Vec(m_offset_x * cos(m_angle),
               m_offset_y * sin(m_angle),
               0);
  }
};
