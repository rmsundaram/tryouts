#pragma once

#include "types.h"
#include "body.h"

#include <vector>
#include <array>

#include "tigr.h"

struct Course {
  std::vector<Point> m_points_solar;  // points in solar space
  TPixel             m_colour = tigrRGB(0xFF, 0xFF, 0xFF);

  Course() { m_points_solar.reserve(1000); }
};

struct Scene {
  std::vector<Body>     m_bodies;
  std::array<Course, 2> m_courses;
  bool                  m_points_collected = false;

  static Scene make(bool circular_orbit);
};

