#include "config.h"
#include "types.h"
#include "body.h"
#include "scene.h"

#include <bitset>

#include "tigr.h"
#ifdef WITH_ARGS
#  include "cxxopts.hpp"
#endif

using T = cute::transform;

constexpr int kWorldWidth = 1024;
constexpr int kWorldHeight = 768;

enum class CmdLineOption : uint8_t {
  kOrbitCircular        = 0,
  kPlotCourseNone       = 1,
  kPlotCourseWithPoints = 2,
  kPlotCourseMoonOnly   = 3,
  kPlotCourseEarthOnly  = 4,
  kDrawBodyMulticolour  = 5,
};
constexpr size_t kTotalCommandLineOpts = 6;

struct Settings {
  bool operator()(CmdLineOption opt) const {
    return m_bits[static_cast<size_t>(opt)];
  }
  std::bitset<kTotalCommandLineOpts>::reference
  operator()(CmdLineOption opt) {
    return m_bits[static_cast<size_t>(opt)];
  }

  std::bitset<kTotalCommandLineOpts> m_bits = 0;
  float m_new_moons_per_year = 13.0f;
};

namespace {

void draw(Tigr* g,
          const std::vector<Body>& bodies,
          size_t idx,
          T parent,
          Settings settings) {
  const Body& b = bodies[idx];
  // Xform_1 * Xform_2 applies Xform_2.p and then Xform_2.r both in Xform_1
  // space i.e. p' = RTp.
  T child = T{
    b.pos(),
    cute::identity_m3
  };
  T local = cute::mul(parent, child);
  auto p = to_point(cute::mul(local, Vec(0, 0, 1)));
  auto const three_quaters = b.m_size * 3 / 4;
  tigrFillCircle(g,
                 p[0],
                 p[1],
                 b.m_size,
                 b.m_colour1);
  if (settings(CmdLineOption::kDrawBodyMulticolour))
    tigrFillCircle(g,
                   p[0],
                   p[1],
                   three_quaters,
                   b.m_colour2);
  if (b.m_child)
    draw(g, bodies, static_cast<size_t>(*b.m_child), local, settings);
}

void draw_line(Tigr* g,
               Point p1_solar,
               Point p2_solar,
               TPixel colour,
               T solar_to_screen) {
  // Transform points from solar to screen space
  auto const p1 = to_point(cute::mul(solar_to_screen, to_vec(p1_solar)));
  auto const p2 = to_point(cute::mul(solar_to_screen, to_vec(p2_solar)));
  tigrLine(g, p1[0], p1[1], p2[0], p2[1], colour);
}

void draw(Tigr* g, const Scene& s, T parent, Settings settings) {
  if (!settings(CmdLineOption::kPlotCourseNone)) {
    if (!settings(CmdLineOption::kPlotCourseWithPoints)) {
      for (auto i = 0u; i < s.m_courses.size(); ++i) {
        if ((i == 0 && settings(CmdLineOption::kPlotCourseMoonOnly)) ||
            (i == 1 && settings(CmdLineOption::kPlotCourseEarthOnly))) {
          continue;
        }
        for (auto j = 1u; j < s.m_courses[i].m_points_solar.size(); ++j) {
          draw_line(g,
                    s.m_courses[i].m_points_solar[j - 1],
                    s.m_courses[i].m_points_solar[j],
                    s.m_courses[i].m_colour,
                    parent);
        }
        if (s.m_points_collected) {
          // not checking point count > 2 before vector::{front,back} since
          // we’re done collecting points i.e. full circle; at least 2 points.
          draw_line(g,
                    s.m_courses[i].m_points_solar.back(),
                    s.m_courses[i].m_points_solar.front(),
                    s.m_courses[i].m_colour,
                    parent);
        }
      }
    }
    else {
      for (auto i = 0u; i < s.m_courses.size(); ++i) {
        if ((i == 0 && settings(CmdLineOption::kPlotCourseMoonOnly)) ||
            (i == 1 && settings(CmdLineOption::kPlotCourseEarthOnly))) {
          continue;
        }
        const auto& c = s.m_courses[i];
        for (const auto point : c.m_points_solar) {
          auto const p = to_point(cute::mul(parent, to_vec(point)));
          tigrPlot(g, p[0], p[1], c.m_colour);
        }
      }
    }
  }

  draw(g, s.m_bodies, 0, parent, settings);
}

void update(Scene* s, Settings settings) {
  // animate angle
  float ticks = tigrTime();                             // seconds
  float constexpr kRadsPerTick = 0.436332313f;          // 25° / second
  float const earth_turns = ticks * kRadsPerTick;
  float const angle_delta_moon = earth_turns * settings.m_new_moons_per_year;
  std::vector<Body> &b = s->m_bodies;
  b[2].m_angle += angle_delta_moon;
  b[2].m_angle = fmod(b[2].m_angle, 2 * CUTE_MATH_PI);  // % 360°

  float const angle_delta_earth = earth_turns;
  b[1].m_angle += angle_delta_earth;
  bool final_point_collection = b[1].m_angle > (2.0f * CUTE_MATH_PI);
  if (final_point_collection) {
    b[1].m_angle = fmod(b[1].m_angle, 2 * CUTE_MATH_PI);
  }

  if (!settings(CmdLineOption::kPlotCourseNone)) {
    // collect plot points
    if (!s->m_points_collected) {
      T xform_earth { b[1].pos(), cute::identity_m3 };
      T xform_moon = cute::mul(xform_earth,
                               T { b[2].pos(), cute::identity_m3 });
      Vec const origin = Vec(0, 0, 1);
      // final positions of Earth and Moon in Solar space
      Vec origin_earth_solar = cute::mul(xform_earth, origin);
      Vec origin_moon_solar = cute::mul(xform_moon, origin);
      s->m_courses[0].m_points_solar.push_back(to_point(origin_earth_solar));
      s->m_courses[1].m_points_solar.push_back(to_point(origin_moon_solar));
    }
    if (final_point_collection)
      s->m_points_collected = true;
  }
}

Settings parse_opts(int argc, char** argv) {
  Settings s;
#ifdef WITH_ARGS
  cxxopts::Options options("nila",
                           "Display animation of Moon's course around "
                           "Earth and Sun.");
  options.add_options()
    ("n,no-plot", "Plot courses of Moon and Earth")
    ("d,dots", "Plot courses with points, not (default) lines")
    ("c,circular", "Make courses circular, not (default) elliptical")
    ("r,course", "Choose courses to plot; 0: both, 1: Moon, 2: Earth",
     cxxopts::value<int>()->default_value("0"))
    ("o,moons", "New moons in a year",
     cxxopts::value<float>()->default_value("13.0"))
    ("m,multicolour", "Draw each celestial body in two colours")
    ("h,help", "Print usage");
  auto result = options.parse(argc, argv);
  int const courses = result["course"].as<int>();
  if (result.count("help") || (!((courses >= 0) && (courses <= 2))))
  {
    std::cout << options.help() << std::endl;
    exit(0);
  }
  s(CmdLineOption::kOrbitCircular) = result["circular"].as<bool>();
  s(CmdLineOption::kPlotCourseWithPoints) = result["dots"].as<bool>();
  s(CmdLineOption::kPlotCourseNone) = result["no-plot"].as<bool>();
  s(CmdLineOption::kPlotCourseMoonOnly) = result["course"].as<int>() == 1;
  s(CmdLineOption::kPlotCourseEarthOnly) = result["course"].as<int>() == 2;
  s(CmdLineOption::kDrawBodyMulticolour) = result["multicolour"].as<bool>();
  s.m_new_moons_per_year = result["moons"].as<float>();
#else
  UNUSED(argc);
  UNUSED(argv);
#endif  // WITH_ARGS
  return s;
}

}  // namespace

int main(int argc, char** argv)
{
  Settings settings = parse_opts(argc, argv);

  Tigr *screen = tigrWindow(kWorldWidth, kWorldHeight, "Nila", TIGR_FIXED);
  auto scene = Scene::make(settings(CmdLineOption::kOrbitCircular));
  auto const screen_2_world = cute::transform {
    Vec { kWorldWidth * 0.5, kWorldHeight * 0.5, 0.0f },
    cute::identity_m3
  };
  tigrTime();
  while (!tigrClosed(screen) && !tigrKeyDown(screen, TK_ESCAPE))
  {
    tigrClear(screen, tigrRGB(0x28, 0x2C, 0x34));  // light gray
    draw(screen, scene, screen_2_world, settings);
    tigrUpdate(screen);
    update(&scene, settings);
  }
  tigrFree(screen);
}
