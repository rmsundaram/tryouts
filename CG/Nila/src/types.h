#pragma once

#include <array>

#include "cute_math.h"

#define UNUSED(_x) (void)(_x)

// needed for cute_math and tigr
using Vec = cute::v3;
using Point = std::array<int, 2>;

// avoid ODR violation with inline
inline
Point to_point(Vec p) {
  return { static_cast<int>(p.x()),
           static_cast<int>(p.y()) };
}

inline
Vec to_vec(Point p) {
  return Vec(static_cast<float>(p[0]),
             static_cast<float>(p[1]), 0);
}
