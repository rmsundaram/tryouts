#include "common.hpp"
#include "main.hpp"

const unsigned window_width = 1024, window_height = 768;

GLFWwindow* setup_context(uint32_t width, uint32_t height)
{
    // without setting the version (1.0) or setting it < 3.2, requesting for core will fail
    // since context profiles only exist for OpenGL 3.2+; likewise forward compatibility only
    // exist from 3.0 onwards. 3.0 marked deprecated, 3.1 removed deprecated (except wide
    // lines), 3.2 reintroduced deprecated under compatibility profile
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE,
                   GLFW_OPENGL_CORE_PROFILE);
    // since core only has undeprecated features, making the context forward-compatible is moot
    //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

    glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);

#ifndef NDEBUG
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, GL_TRUE);
#endif

    return glfwCreateWindow(width, height, "Simple", nullptr, nullptr);
}

#ifndef NDEBUG

APIENTRY
void gl_debug_logger(GLenum source,
                     GLenum type,
                     GLuint id,
                     GLenum severity,
                     GLsizei /*length*/,
                     const GLchar *msg,
                     const void *user_data)
{
    std::stringstream ss;
    ss << "GLError 0x"
       << std::hex << id << std::dec
       << ": "
       << msg <<
       " [source=";

    const char *sources[] = {
                                "API",
                                "WINDOW_SYSTEM",
                                "SHADER_COMPILER",
                                "THIRD_PARTY",
                                "APPLICATION",
                                "OTHER",
                                "UNDEFINED"
                            };
    size_t index = array::item_index(source,
                                     GL_DEBUG_SOURCE_API_ARB,
                                     GL_DEBUG_SOURCE_OTHER_ARB,
                                     array::max_index(sources));
    ss << sources[index];
    if (array::max_index(sources) == index)
    {
        ss << " (" << source << ")";
    }

    const char *types[] = {
                              "ERROR",
                              "DEPRECATED_BEHAVIOR",
                              "UNDEFINED_BEHAVIOR",
                              "PORTABILITY",
                              "PERFORMANCE",
                              "OTHER",
                              "UNDEFINED"
                          };
    index = array::item_index(type,
                              GL_DEBUG_TYPE_ERROR_ARB,
                              GL_DEBUG_TYPE_OTHER_ARB,
                              array::max_index(types));
    ss << " type=" << types[index];
    if (array::max_index(types) == index)
    {
        ss << " (" << type << ")";
    }

    const char *severities[] = {
                                   "HIGH",
                                   "MEDIUM",
                                   "LOW",
                                   "UNKNOWN"
                               };
    // as per the extension, HIGH is the base (smaller) value
    index = array::item_index(severity,
                              GL_DEBUG_SEVERITY_HIGH_ARB,
                              GL_DEBUG_SEVERITY_LOW_ARB,
                              array::max_index(severities));
    ss << " severity=" << severities[index];
    if (array::max_index(severities) == index)
    {
        ss << " (" << severity << ")";
    }
    ss << "]";

    const auto log = ss.str();
    FILE *out_file = reinterpret_cast<FILE*>(const_cast<void*>(user_data));
    fprintf(out_file, "%s\n", log.c_str());

    check(severity != GL_DEBUG_SEVERITY_HIGH_ARB, "High severity GL error logged");
}

void setup_debug(bool enable)
{
    if(glewIsSupported("GL_ARB_debug_output"))
    {
        glDebugMessageCallbackARB(enable ? gl_debug_logger : nullptr, stderr);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS_ARB);
        check(GL_NO_ERROR == glGetError(), "Unable to set synchronous debug output");
    }
}
#endif

void handle_key(GLFWwindow *window,
                int key,
                int /*scancode*/,
                int action,
                int /*mods*/)
{
    if((GLFW_KEY_ESCAPE == key) && (GLFW_PRESS == action))
    {
        glfwSetWindowShouldClose(window, GL_TRUE);
    }
}

template <typename T, size_t N>
GLuint upload_data(const T (&data) [N], GLenum target)
{
    /*
     *  VBOs are “buffers” of video memory – just a bunch of bytes containing any kind of binary data you want.
     *  You can upload 3D points, colors, your music collection, poems to your loved ones – the VBO doesn’t care,
     *  because it just copies a chunk of memory without asking what the memory contains.
     */
    GLuint vbo;
    glGenBuffers(1, &vbo);
    glBindBuffer(target, vbo);
    glBufferData(target, sizeof(data), &data, GL_STATIC_DRAW);
    glBindBuffer(target, 0);

    return vbo;
}

GLuint setup_attributes(GLuint vertex_buffer, GLuint index_buffer)
{
    /*
     * The second step to rendering our triangle is to send the points from the VBO into the shaders. Remember
     * how the VBOs are just chunks of data, and have no idea what type of data they contain? Somehow you have to tell
     * OpenGL what type of data is in the buffer, and this is what VAOs are for. VAOs are the link between the VBOs and
     * the shader variables. VAOs describe what type of data is contained within a VBO, and which shader variables the
     * data should be sent to. glGetAttribLocation or layout location is used to find the shader variable location.
     * 
     * If you have used VBOs without VAOs in older versions of OpenGL, then you might not agree with this description of
     * VAOs. You could argue that “vertex attributes” set by glVertexAttribPointer are the link between the VBO and that
     * shaders, not VAOs. It depends on whether you consider the vertex attributes to be “inside” the VAO (which I do),
     * or whether they are global state that is external to the VAO. Using the 3.2 core profile and ATI drivers, the VAO
     * is not optional – glEnableVertexAttribArray, glVertexAttribPointer and glDrawArrays all cause a INVALID_OPERATION
     * error if there is no VAO bound. This is what leads me to believe that the vertex attributes are inside the VAO,
     * and not global state. The 3.2 core profile spec says that VAOs are required, but it's told that only ATI drivers
     * throw errors if no VAO is bound. Here are some quotes from the OpenGL 3.2 core profile specification:
     * 
     *            All state related to the definition of data used by the vertex processor is encapsulated
     *            in a vertex array object.
     * 
     *            The currently bound vertex array object is used for all commands which modify vertex
     *            array state, such as VertexAttribPointer and EnableVertexAttribArray; all commands which
     *            draw from vertex arrays, such as DrawArrays and DrawElements; and all queries of vertex
     *            array state (see chapter 6).
     * 
     * glVertexAttribPointer predates VAOs, so there was a time when vertex attributes were just global state. You could
     * see VAOs as just a way to efficiently change that global state. I prefer to think of it like this: if you don’t
     * create a VAO, then OpenGL provides a default global VAO. So when you use glVertexAttribPointer you are still
     * modifying the vertex attributes inside a VAO, it’s just that you’re modifying the default VAO instead of one you
     * created yourself.
     */

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);

    glBindBuffer(GL_ARRAY_BUFFER, vertex_buffer);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

    // When a VAO is bound, the GL_ARRAY_BUFFER stored in it is decided by glVertexAttribPointer, hence GL_ARRAY_BUFFER
    // saving is implicit, while saving GL_ELEMENT_ARRAY_BUFFER is explicit via glBindBuffer. However, if a buffer is
    // bound to GL_ELEMENT_ARRAY_BUFFER and then a VAO is bound, it doesn't get saved (although the spec says it
    // should), so the order seems important; bind buffers only after binding a VAO for the buffers to be captured.
    // Refer Chapter 5 of Jason's tutorial for details.
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, index_buffer);

    glBindVertexArray(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    return vao;
}

void render(GLuint vao, GLuint program)
{
    glClear(GL_COLOR_BUFFER_BIT);

    glUseProgram(program);
    glBindVertexArray(vao);

    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    glBindVertexArray(0);
    glUseProgram(0);
}

void init_rendering()
{
    glEnable(GL_CULL_FACE);
    glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
}

int main(int /*argc*/, char** /*argv*/)
{
    glfwSetErrorCallback(window_error);
    const auto glfw = raii_wrap(check_return(glfwInit,
                                             GL_TRUE,
                                             "Failed to initialize GLFW"),
                                glfwTerminate);

    auto main_window = setup_context(window_width, window_height);
    check(main_window != nullptr, "Failed to create window");
    glfwMakeContextCurrent(main_window);
    // http://www.parashift.com/c++-faq-lite/memfnptr-vs-fnptr.html
    // according to C++ FAQ, you cannot pass a member function to a C callback; passing a functor, lambda, boost::bind,
    // etc. is for a C++ callback implemented with templates; instead use the void* user_data allowed in the callback;
    // GLFW gives GLFWwindow->SetWindowUserPointer for the same
    glfwSetKeyCallback(main_window, handle_key);

    glewExperimental = GL_TRUE;
    auto err = glewInit();
    check(GLEW_OK == err, reinterpret_cast<const char*>(glewGetErrorString(err)));
    glGetError();       // to clear error due to GLEW bug #120

#ifndef NDEBUG
    setup_debug(true);
#endif

    const float vertices[] = {
                                -0.5f,  0.5f, 0.0f,
                                -0.5f, -0.5f, 0.0f,
                                 0.5f, -0.5f, 0.0f,
                                 0.5f,  0.5f, 0.0f
                             };
    const unsigned indices[] = {
                                  0, 1, 2,
                                  2, 3, 0
                               };
    auto vbo = upload_data(vertices, GL_ARRAY_BUFFER);
    auto ibo = upload_data(indices, GL_ELEMENT_ARRAY_BUFFER);
    auto vao = setup_attributes(vbo, ibo);
    auto program = ShaderUtil::setup_program("shaders/simple.vert",
                                             "shaders/per_obj.frag");

    init_rendering();
    while(!glfwWindowShouldClose(main_window))
    {
        render(vao, program);

        glfwSwapBuffers(main_window);
        glfwPollEvents();
    }

    glDeleteProgram(program);
    glDeleteVertexArrays(1, &vao);
    glDeleteBuffers(1, &ibo);
    glDeleteBuffers(1, &vbo);
}

GLuint ShaderUtil::compile_shader(GLenum shader_type, const std::string &shader_file_path)
{
    const std::ifstream shader_file{shader_file_path};
    std::ostringstream shader_data;
    shader_data << shader_file.rdbuf();

    const std::string &str_shader_code = shader_data.str();
    const GLchar *shader_code = static_cast<const GLchar*>(str_shader_code.c_str());
    const GLint shader_data_len = static_cast<GLint>(str_shader_code.size());
    if(shader_data_len <= 0)
    {
        throw std::invalid_argument(shader_file_path);
    }

    const GLuint shader_id = glCreateShader(shader_type);
    if(0 == shader_id)
    {
        throw std::runtime_error("Failed to create new shader");
    }
    glShaderSource(shader_id, 1, &shader_code, &shader_data_len);
    glCompileShader(shader_id);

    GLint status;
    glGetShaderiv(shader_id, GL_COMPILE_STATUS, &status);
    if(GL_FALSE == status)
    {
        GLint log_length;
        glGetShaderiv(shader_id, GL_INFO_LOG_LENGTH, &log_length);
        const std::unique_ptr<GLchar[]> err_log(new GLchar[log_length]);
        glGetShaderInfoLog(shader_id, log_length, nullptr, err_log.get());
        throw std::runtime_error(err_log.get());
    }   

    return shader_id;
}

// although in the interface prog_id isn't const, making it const in the implementation
// prevents accidental overwrites
void ShaderUtil::link_program(const GLuint prog_id, const std::vector<GLuint> &shaders)
{
    for(auto shader_id : shaders)
    {
        glAttachShader(prog_id, shader_id);
    }

    glLinkProgram(prog_id);
    GLint status;
    glGetProgramiv(prog_id, GL_LINK_STATUS, &status);
    if(GL_FALSE == status)
    {
        GLint log_length;
        glGetProgramiv(prog_id, GL_INFO_LOG_LENGTH, &log_length);
        const std::unique_ptr<GLchar[]> err_log(new GLchar[log_length]);
        glGetProgramInfoLog(prog_id, log_length, nullptr, err_log.get());
        throw std::runtime_error(err_log.get());
    }

    for(GLuint shader_id : shaders)
    {
        glDetachShader(prog_id, shader_id);
    }
}

GLuint ShaderUtil::setup_program(const std::string &vert_shader_file, const std::string &frag_shader_file)
{
    const GLuint prog_id = glCreateProgram();
    if(0 == prog_id)
    {
        throw std::runtime_error("Failed to create new program");
    }

    const std::vector<GLuint> shader_ids
    {
        compile_shader(GL_VERTEX_SHADER, vert_shader_file),
        compile_shader(GL_FRAGMENT_SHADER, frag_shader_file)
    };
    link_program(prog_id, shader_ids);
    std::for_each(shader_ids.cbegin(), shader_ids.cend(), glDeleteShader);

    return prog_id;
}
