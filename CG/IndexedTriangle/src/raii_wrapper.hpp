#ifndef __RAII_WRAPPER_H__
#define __RAII_WRAPPER_H__
 
template <typename T>
struct return_type;

template <typename ReturnType, typename... Args>
struct return_type<ReturnType (*) (Args...)>
{
    typedef ReturnType type;
};

template <typename UninitFuncType>
class RAIIWrapper
{
public:
    // enable_if makes sure that check/ignore_return is used and function pointers are
    // not passed as-is
    template <typename InitFunctorType, typename... Args,
      class = typename std::enable_if<!std::is_pointer<InitFunctorType>::value>::type>
    RAIIWrapper(InitFunctorType init_functor,
                UninitFuncType fp_uninit_func,
                Args&&... args)
        : fp_uninit{std::move(fp_uninit_func)}
    {
        init_functor(std::forward<Args>(args)...);
    }

    // movable
    RAIIWrapper(RAIIWrapper &&that) : fp_uninit{std::move(that.fp_uninit)}
    {
        that.fp_uninit = nullptr;
    }
    RAIIWrapper& operator=(RAIIWrapper&& that)
    {
        if (*this != that)
        {
            fp_uninit = std::move(that.fp_uninit);
            that.fp_uninit = nullptr;
        }
        return *this;
    }
    
    // non-copyable
    RAIIWrapper(const RAIIWrapper&) = delete;
    RAIIWrapper& operator=(const RAIIWrapper&) = delete;

    ~RAIIWrapper()
    {
        fp_uninit();
    }

private:
    UninitFuncType fp_uninit;
};

template <typename InitFunctorType, typename UninitFuncType, typename... Args>
RAIIWrapper<UninitFuncType>
raii_wrap(InitFunctorType init_functor,
          UninitFuncType fp_uninit_func,
          Args&&... args)
{
    return RAIIWrapper<typename std::decay<UninitFuncType>::type>(std::move(init_functor),
                                                                  std::move(fp_uninit_func),
                                                                  std::forward<Args>(args)...);
}

template <typename InitFuncType, typename SuccessValueType>
struct ReturnChecker
{
    InitFuncType func;
    SuccessValueType success;
    const char *error_string;

    ReturnChecker(InitFuncType func,
                  SuccessValueType success,
                  const char *error_string)
        : func {std::move(func)}
        , success {std::move(success)}
        , error_string {error_string}
    {
    }

    template <typename... Args>
    void operator()(Args&&... args)
    {
        if (func(args...) != success)
        {
            throw std::runtime_error(error_string);
        }
    }
};

template <typename InitFuncType, typename SuccessValueType,
	       typename Ret = ReturnChecker<InitFuncType, SuccessValueType>>
Ret check_return(InitFuncType func, SuccessValueType success, const char *error_string)
{
    return Ret(func, success, error_string);
}

template <typename InitFuncType>
struct ReturnIgnorer
{
    InitFuncType func;

    ReturnIgnorer(InitFuncType func)
        : func(std::move(func))
    {
        static_assert(std::is_void<typename return_type<InitFuncType>::type>::value,
                      "Non-void return value ignored");
    }
    
    template <typename... Args>
    void operator()(Args&&... args)
    {
        func(args...);
    }
};

template <typename InitFuncType>
ReturnIgnorer<InitFuncType>
ignore_return(InitFuncType func)
{
    return ReturnIgnorer<InitFuncType>(func);
}

#endif
