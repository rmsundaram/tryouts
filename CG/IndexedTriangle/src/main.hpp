#ifndef __MAIN_H__
#define __MAIN_H__

inline void window_error(int, const char *error_msg)
{
    throw std::runtime_error(error_msg);
}

inline void check(bool condition, const char *error_msg)
{
    if(condition != true)
    {
        throw std::runtime_error(error_msg);
    }
}

namespace array
{

template <typename T>
inline constexpr typename std::enable_if<std::is_array<T>::value, size_t>::type
max_index(const T& /*arr*/)
{
	return std::extent<T>::value - 1;
}

inline
size_t item_index(long item_id,
                  long base_id,
                  long max_id,
                  size_t err_index)
{
    const auto max_index = max_id - base_id;
    auto index = item_id - base_id;
    if ((index < 0) || (index > max_index))
        index = err_index;
    return index;
}

}

inline std::ostream& tab(std::ostream &os)
{
    return os << '\t';
}

namespace ShaderUtil
{
    /**
     * @brief Takes shader code file and type and returns a compiled shader handle.
     */
    GLuint compile_shader(GLenum shader_type, const std::string &shader_file_path);
    
    /**
     * @brief Takes shader and links them into the given program handle
     */
    void link_program(GLuint prog_id, const std::vector<GLuint> &shaders);

    /**
    * @brief Takes shader file paths, compiles and links them in to a program; returns program handle.
    */
    GLuint setup_program(const std::string &vert_shader_file, const std::string &frag_shader_file);
}

#endif  // __MAIN_H__
