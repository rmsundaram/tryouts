#ifndef __PRECOMP_H__
#define __PRECOMP_H__

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <memory>
#include <vector>
#include <algorithm>
#include <string>
#include <fstream>
#include <sstream>
#include <stdexcept>
#include <iostream>
#include <utility>
#include <functional>

#include "raii_wrapper.hpp"

#endif  // __PRECOMP_H__
