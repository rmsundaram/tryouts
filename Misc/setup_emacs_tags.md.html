<meta charset="utf-8">

    **Navigating Large Codebases**
         *Emacs + Tags*

Below are the different options of setting up tags on a codebase for fast navigation.  Using [Universal Ctags][] via [citre][] or [Global][] is the most efficient:

* Very minimal setup
* Super fast on large codebases
* Uses well-maintained tools

# Universal Ctags + citre

citre is an Emacs plugin that works with plain Universal Ctags.  It uses latter’s `ctags` to generate and `readtags` to read `tags` at project root.  This method’s advantage over Global: direct access to `ctags`; Global has no means to pass options to underlying tagger, like Ctags’s `-I` or `-D` (ignore or define macros).  Everything can be done from with Emacs; thanks to citre.

## Generation

citre rids one of directly interacting with `ctags`.  `citre-update-this-tags-file` on an untagged project’s source will present with options to generate a tags file for the project.  `citre-mode` enables `completion-at-point`, xref and imenu integration; it gets auto-enabled when visiting a file whose project has `tags`.

!!! Warning: Defaults
    Invoking `readtags` directly?  It reads `tags` by default; use `-t` if different.  citre looks for `{,.}tags`.

!!! Tip: Ctags’ auto-exclusions
    Directories like `.git` are automatically excluded.  Refer `ctags --list-excludes`

Manually invoking `ctags` is simple; citre’s front-end (`citre-create-tags-file`) is more convenient and also remembers the recipe.  Append to existing DB with `-a`; `citre-update-tags-file`.

``` shell
ctags --languages=C,C++ --kinds-all='*' --fields='*' --extras='*' -R --exclude={third_party,external,build}
```

Indexing a large codebase?  Lessen the metadata harvested; [citre recommends][citre-huge-base]:

``` shell
ctags --languages=C,C++ --kinds-all='*' --fields='+KESflnt' --extras='+fr' -R
```

!!! Note: Latest `ctags`
    Citre recommends using the latest Universal Ctags build.  MSYS2’s package is mostly old.  At the time of writing this a year old.

## Ergonomics

Once generated, `citre-jump{,-back}` and `citre-peek` are mostly sufficient.  Peek window can be scrolled with `M-{n,p}` and results can be switched with `M-{N,P}`.

`citre-ace-peek` is particularly useful to peek definition of any symbol in buffer without moving the point.  What’s even better is `citre-peek-through` (`M-l p`) which allows peeking inside the peek window!  Follow a function chain with ease 🤓  Navigate through history with `<left>` and `<right>` 🥳.  citre lets you [navigate your tree-like code reading history][citre-nav-tree] and also save/restore peek sessions!

# Universal Ctags + Global

The Global project consists of two components: `global`, the viewer and `gtags`, the tag generator.

Universal Ctags is the modern (supports C++11), maintained version of Ctags which is better than Exuberant Ctags which is also unmaintained 🤦.  More importantly, it supports parsing modern C++.

!!! Note
    Global should be built with `--with-universal-ctags`.  `gtags --gtagslabel=new-ctags` shouldn’t throw errors.

On macOS, since Homebrew doesn’t have Global with Universal Ctags support, build Global from source.

``` shell
./configure --with-universal-ctags=$(which ctags) --prefix=/usr/local/
make
make install
```

If `cscope` is needed, Global also packages a `gtags-cscope`, a Global-variant of the original CScope.

## Tag Generation

1. Make sure Universal Ctags’ `ctags` and `gtags` are in `PATH`
  + Other `ctags` binaries shouldn’t override this e.g. Emacs ctags, (macOS) BSD ctags, etc.
2. `find -type f -iname '*.cpp' -o -iname '*.c' -o -iname '*.h' -o -iname '*.hpp' -o -iname '*.inl' > gtags.files`
  - Use `-f` if not named `gtags.files`
3. Setup `gtags.conf` in project root
  - `cp /usr/local/share/gtags/gtags.conf gtags.conf`
  - Set the `default:` properly e.g. `:tc=new-ctags:`
  - Add `skip`ped glob patterns to the chosen parser
  - `skip` works even when `gtags.files` (or `-f`) is in action
4. `gtags`
  - If project root doesn’t have `gtags.conf`, then `--gtagslabel new-ctags` and `--gtagsconf '/usr/local/share/gtags/gtags.conf'` might be handy

Run `global -u` periodically to update tag DBs incrementally.

## Lookup Symbols

``` shell
global -e REGEX   # lookup symbol
global -P         # lists files indexed
global -f FILE    # symbols in a source
```

## Emacs

1. Install [ggtags][] Emacs package
2. Add `(ggtags-mode 1)` in `.emacs`; make sure `global` is in `exec-path`

Once setup, see [ggtags][] for key bindings in Emacs buffers.

## Troubleshooting

* Global doesn’t work on Objective-C files
  - Check if `gtags` is using the right language parser
  - Passing `-v --explain` shows the language detected and parser used

* `.m` files were deduced as Matlab sources!
  - Fix at `gtags.conf` by commenting `:langmap=MatLab\:.m:\` entry under `universal-ctags`.

!!! Warning
    A comment _starts_ with  `#`.  Adding hash elsewhere in the line doesn’t comment it out!

* `global` doesn’t list any symbols parsed!?
  - Global needs the same plugin (e.g. `--gtagslabel new-ctags`) used to run `gtags`

* Possible to have per-project global setting?
  - Yes!  Project root’s `gtags.conf` > `~/.globalrc`; see `man gtags.conf`
  - This renders `--gtagslabel` and `--gtagsconf` redundant

# Nonviable Options

## Plain Global

This is simplistic while modern C++ mightn’t be supported since the original (now unmaintained) `ctags` is used by Global.

1. Use `gtags` to generate tags
2. Install [ggtags](https://github.com/leoliu/ggtags)
3. Add `(ggtags-mode 1)` in `.emacs`; make sure `global` is in `exec-path`

## Plain Universal Ctags

Very straight-forward [setup][univ-ctags-emacs].

``` shell
ctags --languages=C++ -e -R --exclude=third_party --exclude=.git --exclude=build --exclude=out -o .tmp_tags
ctags --languages=ObjectiveC -e -a -R --exclude=third_party --exclude=.git --exclude=build --exclude=out -o .tmp_tags
mv -f .tmp_tags TAGS
```

This works but is very slow.  Not too beneficial; the default `TAGS` interface of Emacs is too minimal.

[univ-ctags-emacs]: https://stackoverflow.com/a/46921922

## CScope + XCScope.el
Works but slow; no jump to decl./defn.  Not very different from `TAGS`.

# References

* [macOS: Install Global with Universal Ctags support](https://gist.github.com/alexshgov/7e5ed7841667c66ef5ca4f31664714a9)
* [ggtabs Wiki](https://github.com/leoliu/ggtags/wiki)

[Global]: https://www.gnu.org/software/global
[citre]: https://github.com/universal-ctags/citre
[Universal Ctags]: https://github.com/universal-ctags/ctags
[citre-huge-base]: https://github.com/universal-ctags/citre/blob/master/docs/user-manual/about-tags-file.md
[citre-nav-tree]: https://github.com/universal-ctags/citre/blob/master/docs/user-manual/citre-peek.md#browse-in-the-tree-history
[ggtags]: https://github.com/leoliu/ggtags


<link rel="stylesheet" href="https://morgan3d.github.io/markdeep/latest/apidoc.css?">
<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'medium' };</script>
