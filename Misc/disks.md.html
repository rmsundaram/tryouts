<meta charset="utf-8">

    **Disks and Partitions**

MBR (Master Boot Record) allows you to have only 4 primary partitions (rest becomes logical/extended partitions) and addresses only up to 2 TiB of disk space; no such restrictions in GPT (GUID[^uuid] Partition Table).  Since MBR is on its way out, GPT is the preferred partition table layout for new disks.

BIOS (Basic Input/Output System) needs MBR to boot while UEFI (Unified Extensible Firmware Interface) machines prefer GPT but most support MBR disks too.  Many UEFI machines support a _Legacy Boot_ option[^csm] to emulate a BIOS boot.

# Check Available Partitions

## Linux

``` bash
# display physical disks hierarchically with partitions, file system, mount points and free space
lsblk -f

# display total, used and free space in each partition containing FILE(s)
df -h [FILE]...
# show filesystem types; skip temporary filesystems
df -Thx tmpfs -x devtmpfs

# disk(/directory) usage summary in human-readable format
du -sh [PATH] ...

# directory usage summary per directory
du -hd1 [PATH] ...

# curses-based disk usage analyzer
ncdu [PATH]
```

## Windows

```
# list partition letters (includes non-local ones)
fsutil fsinfo drives

# free space in a partition
fsutil volume diskfree e:

# list drives letter detailing type
wmic logicaldisk get name,description

diskpart
list volume
```

## macOS

```
# display disks, partitions, their type and size
diskutil list
```

# Check GPT or MBR

``` bash
sudo fdisk -l

Disk /dev/nvme0n1: 238.47 GiB, 256060514304 bytes, 500118192 sectors
Disk model: SAMSUNG MZVLW256HEHP-000L2
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
``` none output
Disklabel type: gpt
``` bash
Disk identifier: 2FB1F46F-6E29-4A6A-B62E-18BCEAB6E867


Disk /dev/sdb: 14.88 GiB, 15977152512 bytes, 31205376 sectors
Disk model: Flash Disk
Units: sectors of 1 * 512 = 512 bytes
Sector size (logical/physical): 512 bytes / 512 bytes
I/O size (minimum/optimal): 512 bytes / 512 bytes
``` none output
Disklabel type: dos
``` bash
Disk identifier: 0x003d3232
```

Another option is to use `parted`.

``` bash
sudo parted -l

Model: SanDisk Ultra (scsi)
Disk /dev/sdb: 15.7GB
Sector size (logical/physical): 512B/512B
``` none output
Partition Table: gpt
``` bash
Disk Flags:

Number  Start   End     Size    File system  Name   Flags
 1      17.4kB  15.7GB  15.7GB               Purse  msftdata


Model: Generic Flash Disk (scsi)
Disk /dev/sdc: 16.0GB
Sector size (logical/physical): 512B/512B
``` none output
Partition Table: msdos
``` bash
Disk Flags:

Number  Start   End     Size    Type     File system  Flags
 1      1049kB  16.0GB  16.0GB  primary  ntfs         boot
```

!!! Note: GPT fdisk
    `gdisk -l /dev/sdb` also works but auto-corrects issues in the disk.  This isn’t preferable if you don’t want any changes made to the disk.  `gdisk` ([GPT fdisk][gdisk]) was created for managing GPT-based drives back when `fdisk` _had_ only MBR capabilities [^prefix_sc]; since [util-linux][] 2.23, `fdisk` supports GPT too.

[gdisk]: https://rodsbooks.com/gdisk/
[util-linux]: https://github.com/util-linux/util-linux

# Make Partition Table and Partitions

Prefer GPT for new disks as it supports [more partitions and seems more reliable, robust][gpt-vs-mbr].  Losslessly convert MBR to GPT using `gdisk` [[4](#references)] or `MBR2GPT.exe` [[26](#references)].

!!! Note: `fdisk` vs `cfdisk`
    `cfdisks` is curses-based front-end of `fdisk`.  `fdisk` is more advanced; exposes operations `cfdisk` doesn’t e.g. creating a new _partition table_ [[8](#references)].  `cfdisk` shows free space upfront while `fdisk` doesn’t.  `sfdisk` is for scripting.  Use [GParted][] if you need GUI.

``` bash
sudo fdisk /dev/sdb
g  # create a new GPT disklabel
n  # create a new partition
d  # delete a partition
t  # change partition type (default 'Linux filesystem')
p  # print partition table including unwritten changes
w  # write changes (without which nothing happens)
```

Another notable _partition type_ option is `Microsoft basic data`; set when partitioning as NTFS, exFAT, etc.  Of course, this _partition type code_ only identifies the intended purpose of a partition [[9](#references)].  The real deal happens with `mkfs` though.

!!! Tip: _Bootable_
    Bootable flag is an MBR concept; doesn’t apply to partitions drives with GPT.  Set the _Partition Type_ of the FAT32 partition containing `/EFI` to _EFI System_ [[17](#references)].

!!! Tip: Probe partitions
    When you change the partition table type or the schema, you may have to `partprobe MY_DEV` for kernel to be informed of the changes.

[gpt-vs-mbr]: https://www.diskpart.com/gpt-mbr/mbr-vs-gpt-1004.html
[GParted]: https://gparted.org/

## Backup/Restore Partition Table

Use `sfdisk` to [back up a drive’s partition table][pt-backup] (MBR and GPT with util-linux 2.26+):

``` bash
# using fdisk; to/from a file
sfdisk -d /dev/sda > part_table    # backup; remove GUID manually
sfdisk /dev/sda < part_table       # restore

# using sgdisk; to/from a file
sgdisk -b=part_table /dev/sdX
sgdisk -l=part_table /dev/sdY

# using sgdisk; directly between devices
sgdisk /dev/sdX -R /dev/sdY
```

!!! Note: `sfdisk -d` vs `sgdisk -b`
    `sgdisk` copies protective MBR (part of GPT), main GPT header, backup GPT header and the partition table -- binary.  `sfdisk` copies only the partition table information as plain text.  This is okay as, unlike MBR, booting with GPT is usually with files from a partition (see _ESP_, Bootable USB section) and not sectors.  Use `sgdisks` to backup a GPT disk (with hybrid MBR) used to boot a machine with BIOS, or UEFI with legacy boot.

!!! Warning: Cloning duplicates UUIDs
    A device clone will have the original’s UUIDs (both for disk and partitions)!  This potentially leads to issues as the uniqueness property of UUIDs is violated.  Fix by editing out GUIDs (both partition table and per-partition) from the dump; `sfdisk` will auto-generate new ones.  Another way after the fact: `sgdisk --randomize-guids /dev/sda`.  `fdisk`’s _Expert Mode_ + `uuidgen` works too.  Make sure to [fix filesystem UUIDs][fs-uuid-change] too; for NTFS [use `ntfslabel`’s `--new-half-serial` for safety][ntfs-uuid-change].

!!! Tip: Different UUIDs
    UUID is a general concept; it’s used for various things: (1) disk (`/dev/sda`), (2) partition (`/dev/sda1`), (3) filesystem (ones under `/dev/disk/by-uuid/`; NFTS and FAT UUIDs aren’t 128-bits, but 64 and 32 bits) and (4) partition type (e.g. _Linux filesystem data_).  `blkid DISK_OR_PARTITION` shows 1-3; use `blkid -p /dev/sda1` to see (4).  (1) `PTUUID`, (2) `PARTUUID`, (3) `UUID`, (4) `PART_ENTRY_TYPE`.

!!! Tip: GPT Partition Name
    `lsblk -f` (and _Windows Explorer_) shows partition labels.  However GPT also has a concept of _partition name living outside the filesystem_.  This can be seen in `fdisk`’s _Expert Mode_ as `Name` or as `PARTLABEL` in `blkid`.

[pt-backup]: https://unix.stackexchange.com/a/12988/30580
[fs-uuid-change]: https://unix.stackexchange.com/q/12858/30580
[ntfs-uuid-change]: https://askubuntu.com/a/642627/43759

# Format Partition

Once the partition table and the partition has been laid out, choose a file system. Format partition before use:

``` bash
sudo mkfs.exfat -L Capsule /dev/sdb1
```

!!! Warning
    After `mkfs` (formatting), one might have to unmount and mount to reflect changes.

!!! Tip: exFAT
    [exFAT][] is optimized for flash memory like pen drives and SD cards.  Read/Write support by Windows, Linux and macOS.  Gives best of both worlds: NTFS (max file size > 4 GiB) and FAT32 (read and write on macOS).  Use GPT scheme (with _Protective MBR_) and choose exFAT (`Microsoft basic data (0700)`) partition type; refer [[2](#references)].  exFAT, however, works only on Windows, Linux and macOS; most appliances, devices like musical keyboard, TVs, etc., still don’t recognise it; refer [`appliance_fat32.md.html`](./appliance_fat32.md.html).

- **ext4**
  * Use `-b` to change the default block size

- **exFAT**
  * Block Size[^aus]
    - Use `-c` to set sectors/cluster
    - See `man mkexfatfs` or [[5](#references)] for default sizes
    - Check current block size with `ls -hAgo` on an empty directory [[6](#references)]
  * Label
    - Use `-L` to set label
    - `exfatlabel` changes label after the fact

- **vFAT**
  * Use `-F 32` to specify FAT32
  * Use `-S` to set bytes/sector; stick to `512` for wider recognition [[25](#references)]
  * Use `-s` to set sectors/cluster; stick to `128` for wider recognition [[25](#references)]
  * Use `-n` to set label

- **NTFS**
  * Block/Cluster size[^aus]
    - Use `-c` to set cluster size
    - `sudo ntfsinfo -m /dev/sdb1` gives current size [[11](#references)]
    - Default is 4 KiB till 16 TiB partitions [[5](#references)]
  * Label
    - Use `-L` to set label
    - `ntfslabel` changes label (and/or serial number of device) after the fact
  * Use `-n` to dry run

!!! Tip: FAT details
    Use `fsck.vfat -nv /dev/sdXY` to describe FAT parameters like {sector,cluster} size of a partition.

Use `tune2fs` to view and tune file system parameters [[10](#references)].

Prefer _ext4_ over _ntfs_ as [_ext4_ minimizes fragmenting][ext4-frag].

[exFat]: https://en.wikipedia.org/wiki/ExFAT
[ext4-frag]: https://superuser.com/q/536788/50345

# Bootable USB

Points to remember about UEFI boot:

- [Check if computer booted in EFI mode][efi-check]
  * Linux: `[ -d /sys/firmware/efi ] && echo EFI || echo BIOS`
  * Windows: Run `msinfo32`, check _BIOS Mode_: _UEFI_ or _Legacy_
- UEFI specification mandates EFI System Partition (ESP) to be FAT12/16/32
  * **GPT**: `fdisk -l` shows `Disklabel type: gpt` and a partition with type `EFI System`
  * MBR: FAT32 partition with bootable flag
  * ESP contains `/EFI` directory with sub-directories for each OS
- **Highly recommended for a disk to contain only one ESP**
  * Installing Linux alongside Windows?
    - Don’t format existing ESP!
    - Mount ESP on `/boot/efi` [[13](#references)]
    - Create a new ESP only if there’s none[^esp_order]

> "When installing to use UEFI it is important to boot the installation media in UEFI mode, otherwise `efibootmgr` will not be able to add the GRUB UEFI boot entry."
>    -- ArchWiki/[GRUB][]

!!! Warning: [Strict Windows Installer][win-uefi-mbr]
    Bootable Windows USB disks needs to match machine’s HDD partition scheme.  You can’t install Windows 10 on a MBR HDD with a GPT USB or vice-versa.  Windows 11 only supports UEFI + GPT.  Reason: Windows assumes the _boot-kind_ would be maintained i.e. a UEFI + GPT style boot to install Windows would expect the same in successive boots; it would stop working if switched to UEFI + MBR.

Though Linux isn’t picky, it’s safer/easier to use a GPT disk (with ESP) to install on a UEFI-booted machine.

## `dd`

A USB created using `dd` may end up having GPT or MBR depending on source ISO.  A non-hybrid ISO wouldn’t make the USB disk bootable; treat ISO with [isohybrid][].  Most Linux distro images are hybrid MBR images.

!!! Info: [Cloning vs Extraction][clone-vs-extract]
    Using `dd` is basically _cloning_; another method is _extraction_ (_[Rufus][]_’ method) -- format, extract and install boot loader like _syslinux_ ([BIOS mode booting needs loader][iso2usb]).  Extraction works even for non-hybrid ISOs as they don’t depend on the ISO for a loader.

## UEFI

!!! Tip: UEFI-Sure
    Booting with a disk with GPT scheme and an ESP is a sure shot confirmation that the machine booted in UEFI, not Legacy Boot (BIOS), mode.

Create a GPT disk with a partition (of type `EFI System`) FAT32[^exfat_esp] formatted and copy ISO contents [[15](#references)].  Most UEFIs support booting from MBR disks, format FAT32 partition with boot flag set and copy ISO contents.  Windows 10 ISOs require 2 partitions -- FAT32 ESP and NTFS `sources` [[16](#references)] -- since some files may be larger than 4 GiB.


[GRUB]: https://wiki.archlinux.org/index.php/GRUB#UEFI_systems
[efi-check]: https://legends2k.github.io/note/grub2_recovery
[win-uefi-mbr]: https://docs.microsoft.com/en-us/windows-hardware/manufacture/desktop/windows-setup-installing-using-the-mbr-or-gpt-partition-style
[isohybrid]: https://help.ubuntu.com/community/mkusb/isohybrid
[iso2usb]: https://help.ubuntu.com/community/Installation/iso2usb#For_an_UEFI_only_boot_flash_drive_you_need_no_installer
[clone-vs-extract]: https://unix.stackexchange.com/a/450675/30580
[Rufus]: https://rufus.ie

# New Disks

Before using new disks, while it’s still in warranty, it’s a good idea to run `badblocks` and stress test the disk [[21](#references)].  Drives, after their first scan error, are 39 times more likely to fail within 60 days than drives with no such errors, so it’s better to run it [[23](#references)].

1. `smartctl -a /dev/sdX`
  - Check general stats of the drive
  - View background test results
2. `smartctl -t short /dev/sdX`
  - Initiate short, offline test; note the estimated completion time (usually few minutes)
  - `smartctl -a /dev/sdX` should give the results; keep log copy
3. `badblocks -b BLOCK_SIZE -t random -wsvo bad_block_ids.txt /dev/sdX` to make sure [any bad sectors are remapped to spare sectors][badblocks-stress-test]
  - Does a r+w pass with 4 random patterns; can be a long time, around 4 times `smartctl -a`’s long test estimate
  - `-n` is slower, non-destructive, `-w` is faster, destructive; both are r/w tests
  - S.M.A.R.T’s _Reallocated Sector Count_ difference before and after shows found bad sector count
  - Generated `bad_block_ids.txt` can be fed to `e2fsck`/`mkfs`’s `-l` flag if block size tallies
    + Use `-c` if in doubt; these tools will run `badblocks` internally with right block size
4. `smartctl -t long /dev/sdX`
  - Run periodically; usually non-destructive
  - Captive runs abort due to [a bug][tux-captive]; run non-captive test (without `-C`)
  - You can `watch -d -n 60 smartctl --log=selftest /dev/sdX` [if needed][smartctl-aborts]

[[24](#references)] details testing new disks.  [[21](#references)] says that S.M.A.R.T only passively awaits errors while `badblocks` actively checks for damaged areas; `badblocks` is to HDD as `memtest86` is to RAM.  This is because `badblocks` tries to write different patterns to every block and reads it back again to verify the goodness of that block.  [See differences between the two][badblocks-stress-test].

A decent set of tests before using a new disk:

1. Destructive R+W `badblocks` test (3)
2. Long `smartctl` test (4)
3. Inspect for health and error counts in _Crystal Disk Info_

!!! Tip: [`badblocks` Performance][badblocks-optimal]
    Make sure to set the right block size (`-b`) to avoid wasting days.  Perhaps [benchmarking with a script][badblocks-bench-script] is a good idea.  Generally [leaving block count (`-c`) to default (64) seems okay][badblocks-c-default].  For one pass it took 1 day for a 4 TB drive!  `-w` without `-t` is going to run 4 passes with different patterns like `0xaa`, `0x55`, etc.; it’s better to pass n `-t PATTERN` to limit it n patterns.

If `smartctl` errors out with `scsi error unsupported field in scsi command`; [try disabling kernel module USB attached SCSI][disable-uas].  Alternatively, _CrystalDisk Info_ is a good tool to show S.M.A.R.T stats on Windows.  See [Understanding SMART Reports][smartctl-understand] and [What current, worst and threshold values mean?][smartctl-values].

!!! Note: Order Matters!
    Running S.M.A.R.T on a pristine drive may not show up anything.  It’s better to complete `badblocks` tests and then run the short, offline test to see if something shows up.  Use `smartctl -c /dev/sdX` to know time estimates of various tests.

!!! Warning: Age Matters!
    All drives eventually fail due to old age.  After 3 years or in the 3-5 year band it’s better to create a copy of the drive and retire it.

[badblocks-stress-test]: https://serverfault.com/a/746887/139619
[badblocks-optimal]: https://unix.stackexchange.com/q/202541/30580
[badblocks-bench-script]: https://askubuntu.com/a/400685/43759
[badblocks-c-default]: https://askubuntu.com/a/59453/43759
[smartctl-rundown]: https://www.thomas-krenn.com/en/wiki/SMART_tests_with_smartctl
[smartctl-understand]: https://docs.unraid.net/legacy/FAQ/understanding-smart-reports
[smartctl-values]: https://superuser.com/q/1127100/50345
[disk-failures]: https://static.googleusercontent.com/media/research.google.com/en//archive/disk_failures.pdf
[disable-uas]: https://askubuntu.com/q/637450/43759
[tux-captive]: https://superuser.com/a/1782898/50345
[smartctl-aborts]: https://superuser.com/q/766943/#comment2797765_1479731

# Recovery

!!! Warning
    A failing drive tends to develop more bad sectors leading to permanent data loss as time passes.  Recover drive at first sign of trouble.  Be diligent as every time a physically damaged disk powers up and is able to emit some data, it may be the very last time!

* Always backup (GPT or MBR) partition table of a hard disk once established
* Keep it in long-term storage for future reference/salvaging
* Monitor S.M.A.R.T status using _CrystalDisk Info_ or `smartctl` periodically[^when-smart]
* For partition table recovery try [TestDisk][]; useful if you
  - forgot to take a backup, or backup is corrupted
  - accidentally deleted some partition
  - corrupted the partition table
* If your disk/card/media shows any signs of weakness/failure, clone it with [ddrescue][]
  - Immediately unmount and never mount it (neither RW nor RO)
    + Boot from a [SystemRescue][][^sysres-info] USB and run `ddrescue`
    + A Linux machine with [disabled auto-mounting][SE-no-mount] works too
  - Even file system repairs, integrity checks or defrags are going to hurt recovery
  - Run `e2fsck` or similar tools only on the recovered copy
  - Run tools like [PhotoRec][] only on the recovered copy
  - If the recovered copy has partition table corruption
    - Use partition table backup to fix that
    - Use TestDisk on the copy if backup isn’t available
    - For NTFS partitions use `chkdsk`; if that doesn’t work [use TestDisk][testdisk-ntfs]
    - Check ddrescue’s manual -- _§3 Important Advice_ and _§10 A small tutorial with examples_

One might need multiple retries of `ddrescue` to recover everything:

``` bash
ddrescue -R -a 50MiB -K 64KiB,1MiB -B -d -nN -b4096 -f /dev/sdX /dev/sdY /mnt/Backup/Rescue_Op/mapfile
```

Once done, check results in mapfile; try recovering untried pockets if any.  A helper to sort them by size:

``` bash
grep '\?$' my_mapfile | tr -s ' ' | gawk '{ addr = strtonum($1); sz = strtonum($2); offset = and((addr + (sz/DIV)), 0xFFFFFFFFFFFF0000); printf("%d 0x%X %s\n", sz, offset, $0) }' DIV=$2 | sort -nr
```

_ddrescue_ can take in and out files that are

* devices e.g. `/dev/sda`
* partitions e.g. `/dev/sda1`
* files i.e. a file from a partition mounted somewhere

For drives with 4096 physical sectors and 512 logical sectors [set `-b4096`][phy-sec-rules]; check `fdisk -l /dev/sdX`.  Use `-i` in combination with `-s` to jump directly to interesting portions of the disk skipping the others; ddrescue calls this _rescue domain_.  Monitor `mapfile` periodically, change current phase, direction (`-R`) and resuce domain as needed.  Prioritize disk’s important parts with `ddru_findmap`.  `-a` to only recovery parts which can be read at a minimum speed and `-K` to specify how much to skip.

!!! Note: Checks
    Make sure you give the parameters (a) in the right order -- reversal will permanently damage the to-be-recovered data (b) the correct devices based on output from `lsblk -f`.  Also confirm if `OUTFILE` is ≥ `INFILE` in size before issuing recovery.  Make sure to give the same mapfile in _all_ the passes, without which a new retrival is started, than continuing from the old session.

Oneliner to work with map file (make a script)

``` bash
# non-tried blocks sorted in size by descending order; field 2 is offset to seek to
# ARG1: map file, ARG2: size divisor
grep '\?$' $1 | tr -s ' ' | gawk '{ addr = strtonum($1); sz = strtonum($2); offset = and((addr + (sz/DIV)), 0xffffffffffff0000); printf("%d 0x%x %s\n", sz, offset, $0) }' DIV=$2 | sort -nr

# sum of all non-tried block
grep '\?$' $1 | tr -s ' ' | gawk '{ addr = strtonum($1); sz = strtonum($2); offset = and((addr + (sz/DIV)), 0xffffffffffff0000); printf("%d 0x%x %s\n", sz, offset, $0) }' DIV=$2 | sort -nr | cut -d' ' -f1 | paste -sd+ | bc

# view overall status; also takes -i and -s
ddrescuelog -Bt mapfile
```

Supplement programs invaluable to a ddrescue operation:

* [ddrutility][]
  - findbad: from a recovered disk image, list files that don’t have corresponding data recovered
  - ntfsfindbad: same as above but specialized for NTFS filesystem
  - ntfsbitmap: _before_ recovery, find the areas that are recovery-worthy by reading NTFS’ MFT
  - diskutility: useful to send some pass-through messages and read data from the disk
* [ddrescueview][]: graphical representation of mapfile; useful to quickly asses situation

Once recovered, use tools like `e2fsck`, [TestDisk][] or `photorec` to recovery partition table, entries or files respectively.

!!! Warning: NTFS
    When it comes to NTFS, tools in Linux may not be as correct/efficient as Windows ones.  It’s preferable to fix NTFS issues with `chkdsk` or the like.  If `chkdsk` log is truncated in Event Viewer, view entire file through PowerShell `get-winevent -FilterHashTable @{logname="Application"; id="1001"}| ?{$_.providername –match "wininit"} | fl timecreated, message | out-file Desktop\CHKDSK_SCAN.txt`.

Use `ddrescue -b4096 --fill-mode='+' --force /dev/zero /dev/sdX MAPFILE` to wipe the recovered sectors of the drive; useful before returning for warranty claim.


[TestDisk]: https://www.cgsecurity.org/wiki/TestDisk
[PhotoRec]: https://www.cgsecurity.org/wiki/PhotoRec
[testdisk-ntfs]: https://www.cgsecurity.org/wiki/Advanced_NTFS_Boot_and_MFT_Repair
[SystemRescue]: https://www.system-rescue.org/
[ddrescue]: https://www.gnu.org/software/ddrescue/
[man-ddrescue]: https://www.gnu.org/software/ddrescue/manual/ddrescue_manual.html#Important-advice
[phy-sec-rules]: https://unix.stackexchange.com/a/524068/30580
[ddrutility]: https://sourceforge.net/p/ddrutility
[ddrescueview]: https://sourceforge.net/p/ddrescueview/
[SE-no-mount]: https://askubuntu.com/questions/1062719/how-do-i-disable-the-auto-mounting-of-internal-drives-in-ubuntu-or-kubuntu-18-04

# Wipe Disks

!!! Warning: Don’t Wipe SSDs!
    Following tips are for spinning hard disks; not for SSDs.  SSD get worn out by sector-by-sector wipes.  Use [ATA Secure Erase](#transferase).

`dd` works but may abort on write errors; may not be useful on dying disks.  Be surgical in specifying the `-of` parameter.  Data in there gets nuked!

``` bashp
dd if=/dev/zero of=/dev/sdX bs=4M conv=sync,noerror status=progress
```

`badblocks` is the most resilient and interface-agnostic option to wipe spinning disks; [continues despite errors][zero-despite-errors].

``` bash
badblocks -svb BLOCK_SIZE -t random -o badblocks.log -w /dev/sdX
```

Use `-t 0` for zeroing; passing multiple `-t` runs each as a pass sequentially.

If you’re worried filling with zeros isn’t enough, use `/dev/urandom`.  Beware; it’s a lot slower.  Want even more variety of randomness costing more time?  [DBAN][] has different PRNG options like [DoD 5220.22-M, Gutmann][lifewire-dban], etc.  Magnetic media may need more than one pass due to [stickiness][sticky-magent] ([data remanence][]); so this can be combined with zeroing.  In modern drives, [multi-pass wipes are perhaps unneeded][multi-pass-myth].

[ArchWiki lists more tools][erasue-tools] like `shred` and `wipe` for the paranoid.

You can verify if the disk got zeroed by

``` bash
dd if=/dev/sdX bs=8K status=progress | hexdump
0000000 0000 0000 0000 0000 0000 0000 0000 0000
*
1000157208576 bytes (1.0 TB, 931 GiB) copied, 5708 s, 175 MB/s
122095323+0 records in
122095323+0 records out
1000204886016 bytes (1.0 TB, 932 GiB) copied, 5710.29 s, 175 MB/s
e8e0db6000
```

## ATA Secure Erase

!!! Warning: Don’t try this on USB drives
    Don’t try this on non-ATA drives.  USB drives may get bricked due to this method.

For spinning ATA drives [Secure Erase][] is an alternative to sector-by-sector wiping.  Some firmware don’t implement full erasure so this isn’t as good as `dd` and friends.  Moreover, [reports say][ata-erase-no-faster] it isn’t faster than `dd`.

For SSDs, _ATA Secure Erase_ is the way to go.  SSDs have a limited number of write cycles, and wiping them will use up write cycles with no benefit.  Secure Erase instead sends a voltage spike to all available flash memory in unison, causing only a small amount of wear [[22](#references)].  Don’t do this if the interface isn’t (S)ATA.

[zero-despite-errors]: https://unix.stackexchange.com/q/229354/30580
[sticky-magent]: https://linux.m2osw.com/low-level-formatting-hard-drives
[data remanence]: https://en.wikipedia.org/wiki/Data_remanence
[DBAN]: https://sourceforge.net/projects/dban
[lifewire-dban]: https://www.lifewire.com/how-to-erase-a-hard-drive-using-dban-2619148
[erasue-tools]: https://wiki.archlinux.org/index.php/Securely_wipe_disk
[Secure Erase]: https://ata.wiki.kernel.org/index.php/ATA_Secure_Erase
[multi-pass-myth]: https://www.howtogeek.com/115573/htg-explains-why-you-only-have-to-wipe-a-disk-once-to-erase-it/
[ata-erase-no-faster]: https://tinyapps.org/docs/wipe_drives_hdparm.html

## Linux NTFS3 Driver

If you’re unable to mount an NTFS partition - the driver bails out with

> Error mounting /dev/sda7: GDBus.Error:org.freedesktop.UDisks2.Error.Failed: Error mounting /dev/sda7 at /mnt/Backup: wrong fs type, bad option, bad superblock on /dev/sda7, missing codepage or helper program, or other error

Check `dmesg`’s logs:

> [ 4990.686372] ntfs3: sda7: It is recommened to use chkdsk.
> [ 4990.734846] ntfs3: sda7: volume is dirty and "force" flag is not set!

`ntfsfix` (part of `ntfs-3g`) fixes this [if possible][man-ntfsfix] by clearing the dirty flag:

``` bash
ntfsfix -d /dev/sda7
Mounting volume... OK
Processing of $MFT and $MFTMirr completed successfully.
Checking the alternate boot sector... OK
NTFS volume version is 3.1.
NTFS partition /dev/sda7 was processed successfully.
```

If this fails the next resort would be to use `chkdsk` on a Windows machine; refer [Unix SE post][se-ntfs-fix].  It might be more advisable to first try `chkdsk`; [refer][se-ntfs-chkdsk].

[man-ntfsfix]: https://man.archlinux.org/man/ntfsfix.8.en
[se-ntfs-fix]: https://unix.stackexchange.com/a/763185/30580
[se-ntfs-chkdsk]: https://unix.stackexchange.com/a/748483/30580

# References

1. [GPT or MBR: How do I know?](https://unix.stackexchange.com/q/120221/30580)
2. [Formatting a Universal Drive](https://komputerwiz.net/blog/2015/12/13/formatting-a-universal-drive/)
3. [Fdisk Command in Linux](https://linuxize.com/post/fdisk-command-in-linux/)
4. [GPT fdisk: Convert between MBR and GPT - ArchWiki](https://wiki.archlinux.org/index.php/GPT_fdisk#Convert_between_MBR_and_GPT)
5. [Default cluster size for NTFS, FAT, and exFAT](https://support.microsoft.com/en-us/help/140365/default-cluster-size-for-ntfs-fat-and-exfat)
6. [How to consult FAT32 cluster size on Ubuntu](https://askubuntu.com/a/1231102/43759)
7. [What Should I Set the Allocation Unit Size to When Formatting?](https://www.howtogeek.com/136078/what-should-i-set-the-allocation-unit-size-to-when-formatting/)
8. [Fdisk command in Linux](https://linuxize.com/post/fdisk-command-in-linux/)
9. [GDisk Hex Codes](https://askubuntu.com/a/717250/43759)
10. [tune2fs Command Examples for EXT2, EXT3, EXT4](https://linux.101hacks.com/unix/tune2fs/)
11. [Block size with NTFS](https://askubuntu.com/a/306947/43759)
12. [UEFI - Ubuntu](https://help.ubuntu.com/community/UEFI)
13. [Where to install bootloader on ubuntu?](https://askubuntu.com/a/1201456/43759)
14. [EFI system partition - Wikipedia](https://en.wikipedia.org/wiki/EFI_system_partition)
15. [Creating a Bootable Ubuntu UEFI USB Drive](https://techbit.ca/2018/09/creating-a-bootable-ubuntu-uefi-usb-drive/#creating-the-uefi-bootable-usb)
16. [Creating a Bootable Windows 10 UEFI USB using Linux](https://techbit.ca/2019/02/creating-a-bootable-windows-10-uefi-usb-drive-using-linux/)
17. [How to mark a GPT Partition Bootable under Linux](https://superuser.com/questions/1462446/how-to-mark-a-gpt-partition-bootable-under-linux)
18. [USB Flash Installation Medium - ArchWiki](https://wiki.archlinux.org/index.php/USB_flash_installation_medium)
19. [Managing Partition Flags - GParted](https://gparted.org/display-doc.php?name=help-manual#gparted-manage-partition-flags)
20. [Bad block HOWTO for smartmontools](https://www.smartmontools.org/wiki/BadBlockHowto)
21. [badblocks - ArchWiki](https://wiki.archlinux.org/index.php/Badblocks)
22. [Securely Erase SSD](https://www.makeuseof.com/tag/securely-erase-ssd-without-destroying/)
23. [Failure Trends in a Large Disk Drive Population](https://static.googleusercontent.com/media/research.google.com/en//archive/disk_failures.pdf)
24. [Best Way to Test New HDDs](https://serverfault.com/q/501838/139619)
25. [How to format with 64 kB cluster size?](https://askubuntu.com/a/830326/43759)
26. [MBR2GPT tool](https://docs.microsoft.com/en-us/windows/deployment/mbr-to-gpt)


[^uuid]: More generally [UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier): Universally Unique Identifier

[^csm]: UEFI with CSM: Compatibility Support Module

[^esp_order]: Microsoft recommends ESP to be the first partition on a disk

[^prefix_sc]: Both utilities have scriptable and curses-based variants with `s` and `c` prefixes respectively.

[^aus]: a.k.a _cluster size_ or _allocation unit size_

[^when-smart]: Usually when you plug-in to backup something on it.

[^sysres-info]: An Arch Linux-based recovery distro with utilities like _fdisk_, _gdisk_, _parted_, _ddrescue_, _memtest_, _GRUB_ etc. that boots into a nice terminal.  Make sure to label USB’s FAT32 (ESP) as per the `.iso` for the boot script that mounts `/dev/disk/by-label/` to succeed e.g. `SYSRCD618`

[^exfat_esp]: exFAT doesn’t work as ESP on some machines


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><style>.md code > .output { font-weight: bold; background: #FF7; margin-left: -20px; padding-left: 20px}</style><link rel="stylesheet" href="https://morgan3d.github.io/markdeep/latest/apidoc.css?"><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'medium'};</script>
