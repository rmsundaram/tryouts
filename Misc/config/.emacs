;;; package --- Summary  -*- lexical-binding: t -*-

;;; Commentary:
;;; Personalized Emacs Configuration

;;; Code:
;;
;; Benchmark .emacs with
;; http://www.emacswiki.org/emacs/ProfileDotEmacs
;;
;; https://stackoverflow.com/a/5414033/183120
;; uncomment to debug issues in .emacs
;; (setq debug-on-error t)

;; References
;; 1. https://github.com/MatthewZMD/.emacs.d
;; 2. https://jamiecollinson.com/blog/my-emacs-config
;; 3. https://blog.d46.us/advanced-emacs-startup/ (for reducing launch time)

;; A higher GC threshold is need for a good LSP experience. Also, on Windows
;; Emacs 25+, some package triggers garbage collection extremely frequently;
;; this is to work around that by having saner defaults.  To check if this is
;; still the case currently, disable the below three lines and keep the fourth
;; line.  Check how frequently the echo area shows garbage collection messages.
;; https://www.reddit.com/r/emacs/comments/55ork0/
;; is_emacs_251_noticeably_slower_than_245_on_windows/d8cmm7v/
;; http://emacs.stackexchange.com/q/28214#comment43225_28214
;; Setting these earlier is better as per [1]
(setq gc-cons-threshold (* 512 1024 1024)
      gc-cons-percentage 0.5
      garbage-collection-messages t)
(run-with-idle-timer 10 t #'garbage-collect)

;; https://emacs-lsp.github.io/lsp-mode/page/performance/
;; on GNU/Linux it shouldn’t exceed /proc/sys/fs/pipe-max-size.
(setenv "LSP_USE_PLISTS" "true")
(if (string-equal system-type "gnu/linux")
    (with-temp-buffer
      (insert-file-contents "/proc/sys/fs/pipe-max-size")
      (let ((max-pipe-size (string-to-number (buffer-string))))
        (setq read-process-output-max (min max-pipe-size (* 1024 1024))))
      )
  (setq read-process-output-max (* 1024 1024))
  )


;; Garbage collect when losing focus. It is not recommended to bind
;; lambda functions to hooks (and advices), so make a function.
;; https://emacs.stackexchange.com/q/17316/42325#comment25772_17317
(defun my/gc-unless-frame-focus ()
  "Garbage collect if ‘frame-focus-state’ is nil."
  (unless (frame-focus-state)
                        (garbage-collect)))
(defun my/gc-on-losing-focus ()
  "Gargage collect only when focus was lost."
  (if (boundp 'after-focus-change-function)
      (add-function :after after-focus-change-function
                    #'my/gc-unless-frame-focus)
    (add-hook 'after-focus-change-function #'garbage-collect)))
;; use #' instead of ' for funtion names as flycheck shows warnings
;; https://stackoverflow.com/q/2701698#comment42867249_2701743
(when (display-graphic-p)
    (add-hook 'emacs-startup-hook #'my/gc-on-losing-focus))

(require 'package)
(with-eval-after-load 'package
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
  (add-to-list 'package-archives '("nongnu" . "https://elpa.nongnu.org/nongnu/")))
(setq package-archive-priorities
      '(("melpa" . 5)
        ("nongnu" . 2)))
;; Windows (MSYS2) signature verification issue fix; refer https://emacs.stackexchange.com/a/53142/4106
;; M-: (setq package-check-signature nil) RET
;; M-x package-install RET gnu-elpa-keyring-update RET
;; M-: (setq package-check-signature "allow-unsigned") RET
;; Run `gpg --homedir ~/.emacs.d/elpa/gnupg --receive-keys 645357D2883A0966` from MSYS2
;; M-: (setq package-check-signature t) RET
;; Windows (MSYS2) fix for “Failed to verify signature archive-contents.sig”
;; https://emacs.stackexchange.com/q/46302/4106
(when (string-equal system-type "windows-nt")
  (setq package-gnupghome-dir
        ;; replace “c:/” with “/c/”
        (replace-regexp-in-string "\\([a-z]\\):" "/\\1"
                                  package-gnupghome-dir)))
;; put at the top as use-package is unneeded at runtime per its README.md
(unless (bound-and-true-p package--initialized)
  (setq package-enable-at-startup nil)          ; prevent initializing twice
  (package-initialize))

(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; set use-package-verbose to t for interpreted .emacs and nil for byte-compiled
;; .emacs.elc. Note that use-package-expand-minimally also affects verbosity.
(eval-and-compile
  (setq use-package-verbose (not (bound-and-true-p byte-compile-current-file))))

(eval-and-compile
  ;; install all packages by default; turn off piecemeal if needed
  (setq use-package-always-ensure t
        use-package-expand-minimally t
        use-package-compute-statistics t
        ;; use-package-verbose t    ;; uncomment to debug use-package loads
        use-package-enable-imenu-support t))

(eval-when-compile
  (require 'use-package))
(require 'bind-key)        ; use-package dependency

;; http://emacs.stackexchange.com/a/6081/4106
(load "server")
(unless (server-running-p) (server-start))

;; http://www.aaronbedra.com/emacs.d
(setq user-full-name "Sundaram Ramaswamy"
      user-mail-address "legends2k@yahoo.com")

(when (string-equal system-type "windows-nt")
  ;; http://ergoemacs.org/emacs/emacs_env_var_paths.html
  ;; http://stackoverflow.com/a/8607382/183120
  (setq exec-path (append exec-path '("C:/Apps/msys64/usr/bin"
                                      "C:/Apps/msys64/mingw64/bin"
                                      "C:/Apps/msys64/mingw32/bin")))
  ;; set INFOPATH if emacs wasn't executed from msys2
  ;; the last path-separator isn't optional; removing it would lead to
  ;; emacs' own info path not getting appended to Info-directory-list
  (unless (getenv "INFOPATH")
    (setenv "INFOPATH" (concat "C:/Apps/msys64/usr/share/info" path-separator
                               "C:/Apps/msys64/mingw32/share/info" path-separator
                               "C:/Apps/msys64/mingw64/share/info" path-separator))))

;; stop leaving backup~ files scattered everywhere
;; Refer: https://emacs.stackexchange.com/a/72555/4106
;; See Also: https://stackoverflow.com/q/3964715/183120
(defvar --backup-directory (concat user-emacs-directory "backups/")
  "Directory to save backups of edited files.")
(unless (file-exists-p --backup-directory)
  (make-directory --backup-directory t))
(setq backup-directory-alist `(("." . ,--backup-directory)))

;; http://xahlee.blogspot.in/2012/05/emacs-change-cursor-shape-and-highlight.html
(setq-default cursor-type 'bar
              abbrev-mode t
;; http://www.emacswiki.org/emacs/NoTabs
;; https://www.emacswiki.org/emacs/TabStopList
;; show a tab character as 4 spaces
              indent-tabs-mode nil
              tab-width 4)

;; https://superuser.com/a/480786/50345
(when (display-graphic-p)
  (menu-bar-mode -1)
  (scroll-bar-mode -1)
  (tool-bar-mode -1)
  ;; on macOS skip moving Emacs to a seperate desktop on fullscreening
  (if (string-equal system-type "darwin")
      (setq ns-use-native-fullscreen nil
            ;; move to System Trash instead of ~/.local/share/Trash
            ;; https://emacs.stackexchange.com/a/15012/4106
            trash-directory "~/.Trash")
    (setq visible-bell t))
  ;; use ‘maximized’ instead of ‘fullboth’ on macOS if fullscreen isn’t needed
  (add-to-list 'default-frame-alist '(fullscreen . fullboth)))

;; http://www.emacswiki.org/emacs/DisplayTime
(display-time-mode)

;; https://tecosaur.github.io/emacs-config/config.html
(unless (equal "Battery status not available"
               (battery))
  (display-battery-mode 1))                ;; Useful on laptops

(mouse-avoidance-mode 'jump)               ;; mouse avoidance
(show-paren-mode 1)
(desktop-save-mode 1)                      ;; http://ergoemacs.org/emacs/emacs_make_modern.html
(delete-selection-mode 1)                  ;; http://ergoemacs.org/emacs/modernization.html
(electric-quote-mode)                      ;; get curved quotes as you type; see 25.1 NEWS
(pixel-scroll-precision-mode)              ;; smooth mouse wheel scrolling
;; http://stackoverflow.com/questions/144983#comment49048563_144986
(setq inhibit-startup-echo-area-message "sundaram")

(setq
 ;; make it easier to quit help windows
 help-window-select 'other                 ;; https://emacs.stackexchange.com/a/60406/4106
 ;; http://pragmaticemacs.com/emacs/view-mode-makes-for-great-read-only-reading/
 view-read-only t
 ;; insert 2 spaces on a tab key press
 tab-stop-list (number-sequence 2 120 2)
 column-number-mode t
 ;; http://www.masteringemacs.org/articles/2010/10/04/beginners-guide-to-emacs/
 inhibit-startup-message t
 line-number-display-limit-width 20000     ;; helps when reading files with very long lines
 display-time-day-and-date t
 blink-cursor-blinks 0
 mouse-yank-at-point t
 uniquify-buffer-name-style 'forward       ;; https://tecosaur.github.io/emacs-config/config.html
 confirm-nonexistent-file-or-buffer nil    ;; http://www.masteringemacs.org/article/disabling-prompts-emacs
 vc-handled-backends nil                   ;; http://stackoverflow.com/q/5748814/183120
 initial-major-mode 'text-mode
 initial-scratch-message nil               ;; http://stackoverflow.com/a/10110113/183120
 ;; backup options http://stackoverflow.com/a/18330742/183120
 make-backup-files t                       ;; backup of a file the first time it is saved
 backup-by-copying t                       ;; don't clobber symlinks
 version-control t                         ;; version numbers for backup files
 delete-old-versions t                     ;; delete excess backup files silently
 delete-by-moving-to-trash t               ;; delete to system trash
 kept-old-versions 6                       ;; oldest versions to keep when a new numbered backup is made (default: 2)
 kept-new-versions 9                       ;; newest versions to keep when a new numbered backup is made (default: 2)
 auto-save-default t                       ;; auto-save every buffer that visits a file
 auto-save-timeout 20                      ;; number of seconds idle time before auto-save (default: 30)
 auto-save-interval 200                    ;; number of keystrokes between auto-saves (default: 300)
;; http://endlessparentheses.com/new-in-emacs-25-1-easily-search-non-ascii-characters.html
 search-default-mode #'char-fold-to-regexp ;; Emacs 25; easy search for non-ASCII chars
 show-paren-context-when-offscreen 'overlay
 delete-selection-temporary-region t
 )

;; https://gist.github.com/mnp/2f01eb1ce6267f610b4774bbb6515629
;; https://www.n16f.net/blog/using-units-in-emacs-calc/
(setq
 math-additional-units '(
                         (bit nil "bit")

                         ;; Decimal units (bits)
                         (kb  "1000 * bit"  "kilobit")
                         (Mb  "1000^2 bit"  "megabit")
                         (Gb  "1000^3 bit"  "gigabit")
                         (Tb  "1000^4 bit"  "terabit")
                         (Pb  "1000^5 bit"  "petabit")

                         ;; Binary units (bits)
                         (Kib  "1024 * bit"  "kibibit")
                         (Mib  "1024^2 bit"  "mebibit")
                         (Gib  "1024^3 bit"  "gibibit")
                         (Tib  "1024^4 bit"  "tebibit")
                         (Pib  "1024^5 bit"  "pebibit")

                         (B "8 * bit" "byte")
                         ;; Decimal units (bytes)
                         (kB  "1000   B"  "kilobyte")
                         (MB  "1000^2 B"  "megabyte")
                         (GB  "1000^3 B"  "gigabyte")
                         (TB  "1000^4 B"  "terabyte")
                         (PB  "1000^5 B"  "petabyte")
                         (EB  "1000^6 B"  "exabyte")
                         (ZB  "1000^7 B"  "zettabyte")
                         (YB  "1000^8 B"  "yottabyte")

                         ;; Binary units (bytes)
                         (KiB  "1024   B"  "kibibyte")
                         (MiB  "1024^2 B"  "mebibyte")
                         (GiB  "1024^3 B"  "gibibyte")
                         (TiB  "1024^4 B"  "tebibyte")
                         (PiB  "1024^5 B"  "pebibyte")
                         (EiB  "1024^6 B"  "exbibyte")
                         (ZiB  "1024^7 B"  "zebibyte")
                         (YiB  "1024^8 B"  "yobibyte")

                         ;; https://en.wikipedia.org/wiki/Data-rate_units
                         (bps "bit / s" "Bits per second")
                         (Bps "B / s" "Bytes per second")
                         (kbps "kb / s" "Kilobit per second")
                         (kBps "kB / s" "Kilobyte per second")
                         (Kibps "Kib / s" "Kibibit per second")
                         (KiBps "KiB / s" "Kibibyte per second")
                         (Mbps "Mb / s" "Megabit per second")
                         (MBps "MB / s" "Megabyte per second")
                         (Mibps "Mib / s" "Mebibit per second")
                         (MiBps "MiB / s" "Mebibyte per second")
                         (Gbps "Gb / s" "Gigabit per second")
                         (GBps "GB / s" "Gigabyte per second")
                         (Gibps "Gib / s" "Gibibit per second")
                         (GiBps "GiB / s" "Gibibyte per second")
                         (Tbps "Tb / s" "Terabit per second")
                         (TBps "TB / s" "Terabyte per second")
                         (Tibps "Tib / s" "Tebibit per second")
                         (TiBps "TiB / s" "Tebibyte per second")
                         (Pbps "Pb / s" "Petabit per second")
                         (PBps "PB / s" "Petabyte per second")
                         (Pibps "Pib / s" "Pebibit per second")
                         (PiBps "PiB / s" "Pebibyte per second")
                         )
 ;; As advised by `C-h f math-additional-units` to rebuild.
 math-units-table nil
)

;; http://stackoverflow.com/a/21837875/183120
(prefer-coding-system 'utf-8-unix)

;; http://ergoemacs.org/emacs/emacs_list_and_set_font.html
(when (member "Mononoki Nerd Font Mono" (font-family-list))
  (set-frame-font "Mononoki Nerd Font Mono 12" t t))
;; Fix face of quoted text in Info documents
;; https://emacs.stackexchange.com/q/49027
(set-face-attribute 'fixed-pitch-serif nil :family "Mononoki Nerd Font Propo")

;; http://www.emacswiki.org/emacs/ElispCookbook
;; http://ergoemacs.org/emacs/elisp_idioms.html
(defun my/copy-symbol-at-point ()
  "Copy symbol at point, if available."
  (interactive)
  (let (sym-str)
    (setq sym-str (or (thing-at-point 'symbol) (error "No symbol at point")))
    (kill-new sym-str)))

(defun my/kill-symbol-at-point ()
  "Delete symbol at point, if available."
  (interactive)
  (let (bounds pos1 pos2)
    (setq bounds (or (bounds-of-thing-at-point 'symbol) (error "No symbol at point"))
          pos1 (car bounds)
          pos2 (cdr bounds))
    (goto-char pos2)
    (push-mark pos1)
    (kill-region pos1 pos2)))

(defun my/mark-symbol-at-point ()
  "Mark symbol at point, if available."
  (interactive)
  (let (bounds pos1 pos2)
    (setq bounds (or (bounds-of-thing-at-point 'symbol) (error "No symbol at point"))
          pos1 (car bounds)
          pos2 (cdr bounds))
    (goto-char pos2)
    (push-mark pos1)
    (setq mark-active t)))

;; http://stackoverflow.com/a/9414763/183120
(defun my/copy-file-path-to-clipboard (arg)
  "Copy current buffer's file path to the clipboard; return path with backslashes if ARG non-null."
  (interactive "p")
  (let ((filename (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name))))
    (if filename
        (progn
          ;; default to slash; if there’re arguments convert to backslash
          (if (equal arg 1)
              (subst-char-in-string ?\\ ?/ filename t)
            (subst-char-in-string ?/ ?\\ filename t))
          (kill-new filename)
          (message "Copied '%s' to the clipboard." filename))
      (message "Buffer not associated to any file."))))

(defun my/copy-file-path-as-url (start end)
  "Copy current buffer's file path as URL with START and END."
  (interactive "r")
  (let ((filename (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name)))
        ;; set a path without the trailing slash like most ENV path vars
        (project-path (getenv "PROJECT_ROOT")))
    (if filename
        (progn
          (when (string-equal system-type "windows-nt")
            (subst-char-in-string ?/ ?\\ filename t))
          ;; if project path is defined and buffer file is in that path
          (if (and project-path
                   (string-prefix-p project-path
                                    (file-name-directory buffer-file-name)))
              (let (line1 line2 col1 col2) ; use these in format, if needed
                (setq line1 (line-number-at-pos start)
                      line2 (line-number-at-pos end))
                (save-excursion (goto-char start)
                                (setq col1 (+ 1 (current-column)))
                                (goto-char end)
                                (setq col2 (+ 1 (current-column))))
                (kill-new
                 (format "%s%s"
                         "https://bitbucket.org/rmsundaram/tryouts/src/dev"
                         ;; trim project-path prefix; keep leading ‘/’
                         (url-hexify-string (substring filename
                                                       (length project-path))))))
            ;; make it a local URL if not under project
            (kill-new (format "file:///%s" filename)))
          (message "URL Copied to the clipboard."))
      (message "Buffer not associated to any file."))))

;; http://stackoverflow.com/a/6541072/183120
(defun my/func-region (start end func)
  "Pass the region between START and END in current buffer to FUNC."
  (save-excursion
    (let ((text (delete-and-extract-region start end)))
      (insert (funcall func text)))))

(defun my/url-enc-region (start end)
  "URL encode the region between START and END in current buffer."
  (interactive "r")
  (my/func-region start end #'url-hexify-string))

(defun my/url-dec-region (start end)
  "URL decode the region between START and END in current buffer."
  (interactive "r")
  (my/func-region start end #'url-unhex-string))

(defun my/new-empty-buffer ()
  "Create a new empty buffer.
New buffer will be named “untitled” or “untitled<2>”, “untitled<3>”, etc.

URL `http://ergoemacs.org/emacs/emacs_new_empty_buffer.html'
Version 2017-11-01"
  (interactive)
  (let ((buf (generate-new-buffer "untitled")))
    (switch-to-buffer buf)
    (funcall initial-major-mode)
    (setq buffer-offer-save t)
    buf
    ))

(defun my/yank-primary ()
  "Yank from primary selection (*nix systems)."
  (interactive)
  (insert (gui-get-primary-selection)))

(defun my/touch ()
     "Update current buffer’s backing file mtime."
     (interactive)
     (shell-command (concat "touch " (shell-quote-argument (buffer-file-name))))
     (clear-visited-file-modtime))

;; custom bindings
(keymap-global-set "<f7>" #'my/new-empty-buffer)
(keymap-global-set "C-x C-b" #'ido-switch-buffer)
(keymap-global-set "C-x b" #'ibuffer)
;; https://emacs.stackexchange.com/a/60302/4106
;; for dired-jump _before_ dired or dired-x are loaded
(keymap-global-set "C-x j" #'dired-jump)

;; frequently used, need a single combo: replace hardly used set face
(keymap-global-set "M-o" #'other-window)
;; https://stackoverflow.com/q/1030392/183120
(keymap-global-set "M-O" (lambda () (interactive) (other-window -1)))

(keymap-global-set "C-x C-k" #'kill-current-buffer)
(keymap-global-set "C-c c" #'my/copy-symbol-at-point)
(keymap-global-set "C-c k" #'my/kill-symbol-at-point)
(keymap-global-set "C-c f" #'my/copy-file-path-to-clipboard)
(keymap-global-set "C-c C-y" #'my/yank-primary)

(keymap-global-set "C-<" #'scroll-down-line)
(keymap-global-set "C->" #'scroll-up-line)

(keymap-global-set "M-z" #'zap-up-to-char)
(keymap-global-set "M-i" #'imenu)
(keymap-global-set "C-d" #'delete-forward-char)
(keymap-global-set "C-x C-j" #'duplicate-dwim)
(keymap-global-set "C-c t" #'my/touch)

;; http://home.thep.lu.se/~karlf/emacs.html
(define-abbrev-table 'global-abbrev-table '(
                                            ("8del"     "Δ")
                                            ("8alp"     "α")
                                            ("8bet"     "β")
                                            ("8gam"     "γ")
                                            ("8the"     "θ")
                                            ("8lam"     "λ")
                                            ("8pi"      "π")
                                            ("8eps"     "ε")
                                            ("8in"      "∈")
                                            ("8nin"     "∉")
                                            ("8inf"     "∞")
                                            ("8fra"     "⁄")
                                            ("8min"     "−")
                                            ("௮இந்ர்"     "௹")
                                            ("8inr"     "₹")
                                            ("8abs"     "|")
                                            ("8etc"     "…")
                                            ))

(if (>= emacs-major-version 25)
    (remove-hook 'find-file-hooks #'vc-refresh-state)
  (remove-hook 'find-file-hooks #'vc-find-file-hook))

;; replace yes/no with y/n
(if (boundp 'use-short-answers)  ;; introduced in 28.1
    (setq use-short-answers t)
  (fset 'yes-or-no-p 'y-or-n-p))

;; 28-specific features
(when (>= emacs-major-version 28)
  (setq next-error-message-highlight t)
  (repeat-mode))

;; http://www.emacswiki.org/emacs/DisabledCommands
(defun my/no-prompt-enable-commands ()
  "Called when a disabled command is invoked.  Enable and re-execute it."
  (put this-command 'disabled nil)
  (message "Enable and run %s (%s)"
           this-command (key-description (this-command-keys)))
  (sit-for 0)
  (call-interactively this-command))
(setq disabled-command-function #'my/no-prompt-enable-commands)

;; TODO
;;   a. set up LSP for Lua and Go
;;   b. use-packages for company-lua

;; http://stackoverflow.com/a/9472367/183120
(use-package gruvbox-theme
  :config (load-theme 'gruvbox-dark-medium t))

;; Make sure (custom-set-* …) sexps are near file beginning to avoid
;; lisp warnings in loading this package; that’s the right place too.
;; Refer FAQ at https://github.com/Malabarba/smart-mode-line
(use-package smart-mode-line
  :init (setq sml/theme 'dark)
  :config (sml/setup))

;; http://www.masteringemacs.org/articles/2010/10/04/beginners-guide-to-emacs/
;; http://whattheemacsd.com/setup-ido.el-01.html
;; https://github.com/DarwinAwardWinner/ido-completing-read-plus
(use-package ido
  :config
  ;; no need wrap :config’s multiple statements in progn
  ;; https://emacs.stackexchange.com/q/18570#comment27764_18570
  (ido-mode 1)
  (ido-everywhere 1)
  (setq ido-enable-flex-matching t
        ido-create-new-buffer 'always))

(use-package ido-completing-read+
  :after ido
  :config
  (ido-ubiquitous-mode 1)
  (setq ido-cr+-auto-update-blacklist t))

;; https://github.com/gempesaw/ido-vertical-mode.el
(use-package ido-vertical-mode
  :after ido
  :config
  (setq ido-vertical-define-keys 'C-n-C-p-only)
  (ido-vertical-mode 1))

;; https://github.com/nonsequitur/smex
(use-package smex
  :config (smex-initialize)
  :bind (("M-x" . smex)
         ("M-X" . smex-major-mode-commands)))

;; make q kill buffer irrespective of how view-mode was entered
(use-package view
  :ensure nil
  :config
  (add-hook 'view-mode-hook
            (lambda ()
              (setq view-exit-action #'kill-buffer-if-not-modified))))

;; https://www.bytedude.com/gpg-in-emacs
(use-package epa
  :config (setq epa-file-select-keys nil
                epa-file-encrypt-to user-mail-address))

(use-package undo-tree
  :config
  (setq undo-tree-auto-save-history nil)
  (global-undo-tree-mode t))

(use-package browse-kill-ring
  ;; Binds to M-y to show kill ring
  :config (browse-kill-ring-default-keybindings))

(use-package volatile-highlights
  :hook (after-init . volatile-highlights-mode))

;; http://www.emacswiki.org/emacs/RecentFiles
(use-package recentf
  :ensure nil
  :config
  (setq recentf-max-menu-items 200)
  (recentf-mode 1)
  ;; don’t use :bind as deferring recentf → losing recent file history
  (keymap-global-set "C-c C-r" 'recentf-open))

(use-package guru-mode
  :hook (after-init . guru-global-mode))

;; Alternative to evil mode to run commands sans `C-` prefix when reading and
;; editing but not authoring code.
(use-package god-mode
  :init (setq god-mode-enable-function-key-translation nil)
  :bind (:map god-local-mode-map
              ("z" . #'repeat)
              ("C-x C-1" . #'delete-other-windows)
              ("C-x C-2" . #'split-window-below)
              ("C-x C-3" . #'split-window-right)
              ("[" . #'backward-paragraph)
              ("]" . #'forward-paragraph)
              )
  :config
  (keymap-global-set "<escape>" #'god-local-mode)
  ;; (keymap-set god-local-mode-map "z" #'repeat)

  (defun my/god-mode-update-cursor ()
    (setq cursor-type (if god-local-mode 'box 'bar)))
  (add-hook 'post-command-hook #'my/god-mode-update-cursor)
  )

;; Enable god-mode in isearch; ESC + s s s .. to keep searching, RETURN to exit
(use-package god-mode-isearch
  :ensure god-mode
  :after god-mode
  :config
  (keymap-set isearch-mode-map "<escape>" #'god-mode-isearch-activate)
  (keymap-set god-mode-isearch-map "<escape>" #'god-mode-isearch-disable)
  )

(use-package drag-stuff
  :config
  (drag-stuff-global-mode t)   ;; http://stackoverflow.com/a/19378355/183120
  (drag-stuff-define-keys))

;; https://stackoverflow.com/a/10546694/183120
(use-package transpose-frame)

(use-package dired
  :ensure async
  :bind (:map dired-mode-map
              ("s" . my/xah-dired-sort)
              ("z" . my/dired-get-size)
              ("<left>" . dired-up-directory))
  :config
  (setq dired-recursive-copies 'always
        dired-recursive-deletes 'top
        dired-ls-F-marks-symlinks t
        dired-dwim-target t)
  (dired-async-mode 1)
  ;; macOS’s BSD ls doesn’t support --dired, switch to GNU ls or don’t pass flag
  (if (not (string-equal system-type "darwin"))
      (setq dired-listing-switches "-hAgo --group-directories-first")
    (if (executable-find "gls")
        (setq insert-directory-program "/usr/local/bin/gls"
              dired-listing-switches "-hAgo --group-directories-first")
      (setq dired-use-ls-dired nil
            dired-listing-switches "-hAgo")))

  ;; https://oremacs.com/2015/01/12/dired-file-size/
  (if (executable-find "du")
      (defun my/dired-get-size ()
        (interactive)
        (let ((files (dired-get-marked-files)))
          (with-temp-buffer
            (apply 'call-process "du" nil t nil "-sch" files)
            (message
             "Size: %s"
             (progn
               ;; alternative expr for 0-sized file; use shy groups for just one capture
               (re-search-backward "^\\s *\\(\\(?:[0-9.,]+[A-Za-z]+\\)\\|\\(?:0\\)\\)\\s +total$")
               (match-string 1))))))
    (defun my/dired-get-size ()
      (interactive)
      (message "du not in ‘exec-path’"))
    )

  ;; http://ergoemacs.org/emacs/dired_sort.html
  (defun my/xah-dired-sort ()
    "Sort dired dir listing in different ways.
Prompt for a choice.
URL `http://ergoemacs.org/emacs/dired_sort.html'
Version 2018-12-23"
    (interactive)
    (let (sort-by arg)
      (setq sort-by (ido-completing-read "Sort by:" '("date" "size" "name")))
      (cond
       ((equal sort-by "name") (setq arg dired-listing-switches))
       ((equal sort-by "date") (setq arg (concat dired-listing-switches " -t")))
       ((equal sort-by "size") (setq arg (concat dired-listing-switches " -S")))
       ;; ((equal sort-by "dir") (setq arg "-hAgo --group-directories-first"))
       (t (error "Logic error 09535" )))
      (dired-sort-other arg))))

;; make 7z archives with dired; aux is part of dired; an Emacs built-in
(use-package dired-aux
  :ensure nil
  :after dired
  :config
  (add-to-list 'dired-compress-files-alist '("\\.7z\\'" . "7z a %o %i"))
  (add-to-list 'dired-compress-files-alist '("\\.zip\\'" . "7z a %o %i")))

;; use / to filter and g to revert
;; https://github.com/Fuco1/dired-hacks#dired-narrow
(use-package dired-narrow
  :after dired
  :bind (:map dired-mode-map ("/" . dired-narrow-regexp)))

;; colourize by category
;; https://github.com/Fuco1/dired-hacks#dired-rainbow
(use-package dired-rainbow
  :config
    ;; https://github.com/Fuco1/dired-hacks/issues/148#issuecomment-1299187068
    (when (string-equal system-type "windows-nt")
      (setq dired-hacks-datetime-regexp "\\(?:[0-9][0-9]-[0-9][0-9] [0-9][0-9]:[0-9][0-9]\\|[0-9]\\{4\\}-[0-9][0-9]-[0-9][0-9]\\)"))
    (dired-rainbow-define-chmod directory "#6cb2eb" "d.*")
    (dired-rainbow-define html "#eb5286" ("css" "less" "sass" "scss" "htm" "html" "jhtm" "mht" "eml" "mustache"
                                          "xhtml"))
    (dired-rainbow-define xml "#f2d024" ("xml" "xsd" "xsl" "xslt" "wsdl" "bib" "json" "msg" "pgn" "rss" "yaml" "yml"
                                         "rdata"))
    (dired-rainbow-define document "#9561e2" ("docm" "doc" "docx" "odb" "odt" "pdb" "pdf" "ps" "rtf" "djvu" "epub" "odp"
                                              "ppt" "pptx"))
    (dired-rainbow-define markdown "#ffed4a" ("org" "etx" "info" "markdown" "md" "mkd" "nfo" "pod" "rst" "tex" "adoc"
                                              "textfile" "txt"))
    (dired-rainbow-define database "#6574cd" ("xlsx" "xls" "csv" "accdb" "db" "mdb" "sqlite" "nc"))
    (dired-rainbow-define media "#de751f" ("mp3" "mp4" "MP3" "MP4" "avi" "mpeg" "mpg" "flv" "ogg" "mov" "mid" "midi"
                                           "wav" "aiff" "flac"))
    (dired-rainbow-define image "#f66d9b" ("tiff" "tif" "cdr" "gif" "ico" "jpeg" "jpg" "png" "psd" "eps" "svg" "bmp"))
    (dired-rainbow-define log "#c17d11" ("log"))
    (dired-rainbow-define shell "#f6993f" ("awk" "bash" "bat" "sed" "sh" "zsh" "vim"))
    (dired-rainbow-define interpreted "#38c172" ("lua" "py" "ipynb" "rb" "pl" "t" "msql" "mysql" "pgsql" "sql" "r" "clj"
                                                 "cljs" "scala" "js" "ts" "jsx" "tsx"))
    (dired-rainbow-define compiled "#4dc0b5" ("asm" "cl" "m" "mm" "lisp" "el" "c" "h" "c++" "h++" "hpp" "hxx" "m" "cc"
                                              "cs" "cp" "cpp" "go" "f" "rs" "hi" "hs" "pyc" "java"))
    (dired-rainbow-define executable "#8cc4ff" ("exe" "msi" "com"))
    (dired-rainbow-define compressed "#51d88a" ("7z" "zip" "bz2" "tgz" "txz" "gz" "xz" "z" "Z" "jar" "war" "ear" "rar"
                                                "sar" "xpi" "apk" "xz" "tar"))
    (dired-rainbow-define packaged "#faad63" ("deb" "rpm" "apk" "jad" "jar" "cab" "pak" "pk3" "vdf" "vpk" "bsp"))
    (dired-rainbow-define encrypted "#ffed4a" ("gpg" "pgp" "asc" "bfe" "enc" "signature" "sig" "p12" "pem"))
    (dired-rainbow-define fonts "#6cb2eb" ("afm" "fon" "fnt" "pfb" "pfm" "ttf" "otf"))
    (dired-rainbow-define partition "#e3342f" ("dmg" "iso" "bin" "nrg" "qcow" "toast" "vcd" "vmdk" "bak"))
    (dired-rainbow-define vc "#0074d9" ("git" "gitignore" "gitattributes" "gitmodules"))
    (dired-rainbow-define-chmod executable-unix "#38c172" "-.*x.*"))

;; use same dired buffer for further navigations
;; https://github.com/kaushalmodi/.emacs.d/blob/master/setup-files/setup-dired.el
(use-package dired-single
  :bind (:map dired-mode-map
              ("<return>"         . dired-single-buffer)
              ("<double-mouse-1>" . dired-single-buffer-mouse)
              ("6"                . dired-single-up-directory)
              )
  :config
  (defun dired-single-magic-buffer-current-dir ()
    "Open a single magic dired buffer for the current buffer directory."
    (interactive)
    (dired-single-magic-buffer default-directory))

  (defun dired-single-up-directory ()
    (interactive)
    (dired-single-buffer "..")))

;; show unique nested paths upfront
(use-package dired-collapse
  :config
  (add-hook 'dired-mode-hook #'dired-collapse-mode))

;; make regexp builder user single backslash regex syntax
;; https://masteringemacs.org/article/re-builder-interactive-regexp-builder
(use-package re-builder
  :commands (re-builder)
  :config
  (setq reb-re-syntax 'string))

(use-package which-key
  :hook (after-init . which-key-mode))

;; https://github.com/rolandwalker/unicode-fonts
;; Skip for GNU/{Linux,FreeBSD,Hurd} OSes; they support colour emojis since 28.
;; https://www.masteringemacs.org/article/unicode-ligatures-color-emoji
;; Do not use `:if` as it only predicates load and init, not download.
;; Preclude even `:ensure` and `:preface` with this form; refer manual
;; § 3.4 Loading packages conditionally.
(when (or (< emacs-major-version 28)
          (not (string-prefix-p "gnu" (prin1-to-string system-type))))
  (use-package unicode-fonts
    :config
    (when (eq system-type 'darwin)
      (setq unicode-fonts-block-font-mapping
            '(("Emoticons"
               ("Apple Color Emoji" "Symbola" "Quivira")))
            unicode-fonts-fontset-names '("fontset-default")))
    (unicode-fonts-setup)
    )
  )

(use-package eww
  :ensure nil
  :bind (:map eww-mode-map ("I" . my/eww-toggle-images))
  :config
  ;; disable images by default
  (setq-default shr-inhibit-images t)
  (defun my/eww-toggle-images ()
    "Toggle whether images are loaded and reload the current page fro cache."
    (interactive)
    (setq-local shr-inhibit-images (not shr-inhibit-images))
    (eww-reload t)
    (message "Images: %s" (if shr-inhibit-images "OFF" "ON"))))

(use-package winner
  :ensure nil
  :custom
  (winner-boring-buffers
   '("*Completions*"
     "*Compile-Log*"
     "*inferior-lisp*"
     "*Fuzzy Completions*"
     "*Apropos*"
     "*Help*"
     "*cvs*"
     "*Buffer List*"
     "*Ibuffer*"
     "*esh command on file*"))
  :config
  (winner-mode 1))

(use-package subword
  :ensure nil
  :hook ((prog-mode . subword-mode)))

(defconst my/line-limit 80)
(use-package whitespace
  :ensure nil
  :hook ((prog-mode . whitespace-mode))
  :config
  ;; http://stackoverflow.com/a/18855782
  ;; http://emacs.stackexchange.com/a/16661/4106
  (setq whitespace-line-column my/line-limit ; highlight text beyond my/line-limit
        whitespace-style '(face trailing empty tabs lines-tail)
        whitespace-global-modes nil
        fill-column my/line-limit))

;; save the window configuration before and restore after diff

;; Do not use-package; packge refresh errors ensue if done.
;; Split windows horizontally in ediff (instead of vertically)
;; Avoid showing a seperate diff control frame
(require 'ediff)
(setq ediff-split-window-function #'split-window-horizontally
      ediff-window-setup-function #'ediff-setup-windows-plain)
(defun my/kill-ediff-buffers-restore-frames ()
  "Remove redundant ediff buffers."
  ;; https://emacs.stackexchange.com/a/42325/4106
  (ediff-kill-buffer-carefully "*Ediff Control Panel*")
  (ediff-kill-buffer-carefully "*Ediff Registry*")
  (ediff-kill-buffer-carefully "*ediff-diff*")
  (ediff-kill-buffer-carefully "*ediff-fine-diff*")
  (ediff-kill-buffer-carefully "*ediff-errors*")
  (winner-undo))
(add-hook 'ediff-quit-hook #'my/kill-ediff-buffers-restore-frames)

(use-package magit
  :bind (("C-x g" . magit-status))
  :config
  (setq magit-save-repository-buffers 'dontask))

(use-package flycheck
  :hook ((prog-mode . flycheck-mode)))

(use-package flycheck-pos-tip
  :after flycheck
  :hook ((flycheck-mode . flycheck-pos-tip-mode)))

;; better company settings
;; github.com/mdempsky/gocode/emacs-company
(use-package company
  :hook ((prog-mode . company-mode))
  :config
  (push 'company-capf company-backends)
  (setq company-tooltip-limit 20                        ; bigger popup window
        company-idle-delay 0.3                          ; decrease delay before suggestions pop up
        company-echo-delay 0                            ; remove annoying blinking
        company-begin-commands '(self-insert-command))) ; start autocompletion only after typing

;; Make ff-find-other-file also understand Objective-C++
;; Related: https://emacs.stackexchange.com/q/52496/4106
(use-package find-file
  :ensure nil
  :commands ff-find-other-file
  :defines my/cc-other-file-alist
  :config
  (setq my/cc-other-file-alist (copy-alist cc-other-file-alist))
  ;; newly associate .mm -> .h and .inl -> .hpp
  (add-to-list 'my/cc-other-file-alist '("\\.inl\\'" (".hpp" ".h" ".hxx" ".hh")) t)
  (add-to-list 'my/cc-other-file-alist '("\\.mm\\'" (".h")) t)
  ;; add to existing maps .h -> .mm, .hpp -> .inl
  (setcdr (assoc "\\.h\\'" my/cc-other-file-alist)
          (list (append (car (cdr (assoc "\\.h\\'"
                                         my/cc-other-file-alist)))
                        '(".mm"))))
  (setcdr (assoc "\\.hpp\\'" my/cc-other-file-alist)
          (list (append (car (cdr (assoc "\\.hpp\\'"
                                         my/cc-other-file-alist)))
                        '(".inl"))))
  ;; https://www.emacswiki.org/emacs/FindOtherFile
  (setq-default ff-other-file-alist 'my/cc-other-file-alist))

(use-package cc-mode
  :ensure nil
  ;; defer loading through commands; one of these will autoload cc-mode
  ;; https://emacs.stackexchange.com/q/54309#comment84765_54309
  :commands (c-mode c++-mode c-or-c++-mode objc-mode)
  ;; https://www.emacswiki.org/emacs/AutoModeAlist
  ;; \\' is to match end-of-string. while $ matches end-of-line
  :mode (("\\.inl\\'" . c++-mode)
         ("\\.mm\\'" . objc-mode))
  ;; Auto-format C++ project code inside a specific directory; expects
  ;; clang-format package loaded. Uncomment hooking snippet below.
  :functions my/c-common-setup
  ;;:preface
  ;; http://ergoemacs.org/emacs/emacs_byte_compile.html
  ;; Trailing slash to interpret as directory not a file; see file-name-as-directory
  ;; (defconst my/project-dir "~/tryouts/Rust/mandelbrot/")
  :config
  ;; http://truongtx.me/2013/03/10/emacs-setting-up-perfect-environment-for-cc-programming/
  ;; http://www.emacswiki.org/emacs/IndentingC
  (setq-default c-default-style "k&r"
                c-basic-offset 2
                c-offsets-alist `((innamespace . [0])))

  ;; http://stackoverflow.com/a/25960236/183120
  ;; http://home.thep.lu.se/~karlf/emacs.html
  (defun my/c-common-setup ()
    (keymap-local-set "C-c C-o" 'ff-find-other-file)

    (when (> (display-color-cells) 16)
      (font-lock-add-keywords nil '(("\\<\\(__func__\\|__FUNCTION__\\|__PRETTY_FUNCTION__\\|__LINE__\\|__FILE__\\)"
                                     1 font-lock-preprocessor-face prepend))))

    ;; ;; set auto-formatting hook for files within project-dir, having the right extension
    ;; (when (and (stringp buffer-file-name)
    ;;            (string-prefix-p (getenv "PROJECT_ROOT")
    ;;                             (file-name-directory buffer-file-name))
    ;;            (string-match "cc\\|mm\\|h\\|c\\|m\\|cpp\\|hpp\\|cxx\\|inl"
    ;;                          (file-name-extension buffer-file-name)))
    ;;   ;; https://emacs.stackexchange.com/q/5452/4106
    ;;   ;; cc mode itself doesn’t have before-save-hook, make a buffer-local hook
    ;;   (add-hook 'before-save-hook #'clang-format-buffer t 'local))
    )

  ;; though :hook works, don’t use it as c-mode-common-hook gets marked as
  ;; autoload entry points for cc package; doesn’t make sense.
  (add-hook 'c-mode-common-hook #'my/c-common-setup))

(use-package web-mode
  :mode ("\\.html?\\'" "\\.css\\'"))

(use-package markdown-mode
  :mode (("\\.md\\(\\.html?\\)?\\'" . gfm-mode)
         ("\\.markdown\\'" . markdown-mode))
  ;; put functions to be exported in preface
  ;; https://www.masteringemacs.org/article/spotlight-use-package-a-declarative-configuration-tool
  :commands (markdown-mode gfm-mode my/markdeepify)
  :preface
  (defun my/markdeepify ()
    "Insert Markdeep tags into current buffer and change to gfm-mode."
    (interactive)
    (point-to-register ?M)
    (goto-char (point-min))
    (insert "<meta charset=\"utf-8\">\n\n")
    (goto-char (point-max))
    (insert "\n\n<!-- Markdeep: --><style class=\"fallback\">\
body{visibility:hidden;white-space:pre;font-family:monospace}</style>\
<script src=\"markdeep.min.js\" charset=\"utf-8\"></script>\
<script src=\"https://morgan3d.github.io/markdeep/latest/markdeep.min.js\" charset=\"utf-8\"></script>\
<script>window.alreadyProcessedMarkdeep||(document.body.style.visibility=\"visible\")</script>\
<script>window.markdeepOptions = { tocStyle: 'none' };</script>")
    (register-to-point ?M)
    (gfm-mode))

  (defun my/markdeepify-insert-api-style ()
    "Insert Markdeep tags, with API stylesheet, into current buffer and change to gfm-mode."
    (interactive)
    (insert "<link rel=\"stylesheet\" href=\"https://casual-effects.com/markdeep/latest/apidoc.css?\">"))
  :config
  ;; https://devilgate.org/blog/2012/07/02/tip-using-pandoc-to-create-truly-standalone-html-files/
  ;; moving to Pandoc 2.x from 1.x rids of -S (smart); it’s an input format extension now
  ;; TODO: move to templates instead of CSS
  (setq markdown-command "pandoc -f markdown+smart -t html --standalone --self-contained"
        markdown-italic-underscore t
        markdown-asymmetric-header t
        markdown-header-scaling t
        markdown-enable-math t
        markdown-hr-strings
        '("--------------------------------------------------------------------------------"
          "* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * "
          "----------------------------------------"
          "* * * * * * * * * * * * * * * * * * * * "
          "----------"
          "* * * * * ")))

(use-package js2-mode
  :mode ("\\.m?js\\'" "\\.json\\'"))

;; http://stackoverflow.com/a/25022273
(use-package conf-mode
  :ensure nil
  :mode ("\\.[^/]*rc\\'" "\\.toml\\'"))

(defun my/prog-mode-setup ()
  "Basic setup for all programming modes."
  (electric-pair-mode)
  (display-line-numbers-mode)
  (auto-fill-mode)
  (when (> (display-color-cells) 16)
    (font-lock-add-keywords
     nil '(("\\<\\(TEST\\|TODO\\|FIXME\\|BUG\\|NOTE\\|HACK\\)"
            1 font-lock-warning-face prepend)))))
(add-hook 'prog-mode-hook #'my/prog-mode-setup)

;; LSP setup
;; https://github.com/gicmo/dot-emacs/blob/master/init.el
;; https://www.mortens.dev/blog/emacs-and-the-language-server-protocol/
(use-package lsp-mode
  :commands (lsp lsp-mode lsp-deferred)
  :hook ((rust-mode python-mode go-mode lua-mode) . lsp-deferred)
  :config
  (setq lsp-prefer-flymake nil
        lsp-enable-indentation nil
        lsp-enable-on-type-formatting nil
        lsp-log-io nil
        lsp-lua-runtime-version "LuaJIT"
        lsp-eldoc-render-all t
        lsp-rust-analyzer-cargo-watch-command "clippy"
        lsp-rust-analyzer-display-lifetime-elision-hints-enable t
        lsp-rust-analyzer-display-chaining-hints t
        lsp-rust-analyzer-closure-return-type-hints t
        lsp-rust-analyzer-display-reborrow-hints t
        lsp-rust-analyzer-display-parameter-hints t
        rustic-lsp-server 'rust-analyzer)
  (lsp-modeline-code-actions-mode)
  (add-hook 'lsp-mode-hook #'lsp-enable-which-key-integration)
  (add-to-list 'lsp-file-watch-ignored "\\.vscode\\'"))

;; To fill args placeholders upon auto-complete candidate selection set
;; lsp-enable-snippet (or company-lsp-enable-snippet), enable yas-minor-mode.
;; https://emacs.stackexchange.com/q/53104
;; https://github.com/joaotavora/yasnippet/issues/708
(use-package yasnippet
  :hook(lsp-mode . yas-minor-mode))

(use-package lsp-ui
  :commands lsp-ui-mode
  :after lsp-mode
  :hook (lsp-mode . lsp-ui-mode)
  :bind (:map lsp-ui-mode-map
              ("C-c r ." . lsp-ui-peek-find-definitions)
              ("C-c r ?" . lsp-ui-peek-find-references)
              ("C-c r d" . lsp-ui-peek-find-definitions)
              ("C-c r r" . lsp-ui-peek-find-references)
              ("C-c r i" . lsp-ui-imenu)
              ("C-c r F" . lsp-ui-sideline-apply-code-actions)
              ("C-c r R" . lsp-rename))
  :config
  (setq lsp-ui-sideline-show-hover nil)
)

;; https://github.com/seagle0128/.emacs.d/blob/master/lisp/init-ui.el
;; NOTE: install fonts manually on Windows
(use-package all-the-icons
  :if (display-graphic-p)
  :init (unless (or (string-equal system-type "windows-nt")
                    (find-font (font-spec :name "all-the-icons")))
          (all-the-icons-install-fonts t)))

(use-package company-box
  :if (display-graphic-p)
  :after (company all-the-icons)
  :hook (company-mode . company-box-mode)
  :config
  ;; https://github.com/seagle0128/.emacs.d/blob/master/lisp/init-company.el
    (declare-function all-the-icons-faicon 'all-the-icons)
    (declare-function all-the-icons-material 'all-the-icons)
    (declare-function all-the-icons-octicon 'all-the-icons)
    (setq company-box-icons-all-the-icons
          `((Unknown . ,(all-the-icons-material "find_in_page" :height 0.85 :v-adjust -0.2))
            (Text . ,(all-the-icons-faicon "text-width" :height 0.8 :v-adjust -0.05))
            (Method . ,(all-the-icons-faicon "cube" :height 0.8 :v-adjust -0.05 :face 'all-the-icons-purple))
            (Function . ,(all-the-icons-faicon "cube" :height 0.8 :v-adjust -0.05 :face 'all-the-icons-purple))
            (Constructor . ,(all-the-icons-faicon "cube" :height 0.8 :v-adjust -0.05 :face 'all-the-icons-purple))
            (Field . ,(all-the-icons-octicon "tag" :height 0.8 :v-adjust 0 :face 'all-the-icons-lblue))
            (Variable . ,(all-the-icons-octicon "tag" :height 0.8 :v-adjust 0 :face 'all-the-icons-lblue))
            (Class . ,(all-the-icons-material "settings_input_component" :height 0.85 :v-adjust -0.2 :face 'all-the-icons-orange))
            (Interface . ,(all-the-icons-material "share" :height 0.85 :v-adjust -0.2 :face 'all-the-icons-lblue))
            (Module . ,(all-the-icons-material "view_module" :height 0.85 :v-adjust -0.2 :face 'all-the-icons-lblue))
            (Property . ,(all-the-icons-faicon "wrench" :height 0.8 :v-adjust -0.05))
            (Unit . ,(all-the-icons-material "settings_system_daydream" :height 0.85 :v-adjust -0.2))
            (Value . ,(all-the-icons-material "format_align_right" :height 0.85 :v-adjust -0.2 :face 'all-the-icons-lblue))
            (Enum . ,(all-the-icons-material "storage" :height 0.85 :v-adjust -0.2 :face 'all-the-icons-orange))
            (Keyword . ,(all-the-icons-material "filter_center_focus" :height 0.85 :v-adjust -0.2))
            (Snippet . ,(all-the-icons-material "format_align_center" :height 0.85 :v-adjust -0.2))
            (Color . ,(all-the-icons-material "palette" :height 0.85 :v-adjust -0.2))
            (File . ,(all-the-icons-faicon "file-o" :height 0.85 :v-adjust -0.05))
            (Reference . ,(all-the-icons-material "collections_bookmark" :height 0.85 :v-adjust -0.2))
            (Folder . ,(all-the-icons-faicon "folder-open" :height 0.85 :v-adjust -0.05))
            (EnumMember . ,(all-the-icons-material "format_align_right" :height 0.85 :v-adjust -0.2 :face 'all-the-icons-lblue))
            (Constant . ,(all-the-icons-faicon "square-o" :height 0.85 :v-adjust -0.05))
            (Struct . ,(all-the-icons-material "settings_input_component" :height 0.85 :v-adjust -0.2 :face 'all-the-icons-orange))
            (Event . ,(all-the-icons-octicon "zap" :height 0.8 :v-adjust 0 :face 'all-the-icons-orange))
            (Operator . ,(all-the-icons-material "control_point" :height 0.85 :v-adjust -0.2))
            (TypeParameter . ,(all-the-icons-faicon "arrows" :height 0.8 :v-adjust -0.05))
            (Template . ,(all-the-icons-material "format_align_center" :height 0.85 :v-adjust -0.2)))
          company-box-icons-alist 'company-box-icons-all-the-icons))

;; Rust setup; make sure you’ve run
;;     rustup update
;;     rustup component add rls rust-analysis rust-src
;;     install rust-analyzer from your pacakge manager
;; https://christian.kellner.me/2017/05/31/language-server-protocol-lsp-rust-and-emacs
(use-package rust-mode
  :mode "\\.rs\\'"
  :config
  (setq rust-indent-offset (car tab-stop-list))
  (when (executable-find "rust-analyzer")
    (setq-default flycheck-disabled-checkers '(rust-cargo))))

(use-package rustic
  :after (rust-mode)
  :config
  (setq rustic-format-on-save 'on-save)
  )

(use-package cargo
  :after rust-mode
  :commands cargo-minor-mode
  ;; key bindings usable in both Rust sources and Cargo.toml
  :hook (rust-mode . cargo-minor-mode)
  ;; :mode unusable here as it’s only for major modes; see this if you want to
  ;; enable this minor mode for Cargo.toml
  ;; https://stackoverflow.com/q/13945782
)

(use-package go-mode
  :mode "\\.go\\'"
  :config
  (defun my/go-mode-setup ()
    "Basic Go mode setup."
  (add-hook 'before-save-hook #'lsp-format-buffer t t)
  (add-hook 'before-save-hook #'lsp-organize-imports t t))
  (add-hook 'go-mode-hook #'my/go-mode-setup))

(use-package lua-mode
  :mode "\\.lua\\'"
  :config (setq lua-indent-level 2))

(use-package glsl-mode
  :mode ("\\.vert\\'" "\\.frag\\'" "\\.geom\\'" "\\.glsl\\'"))

(use-package elfeed
  :commands elfeed
  :defines elfeed-feeds
  :config
  (setq elfeed-feeds
        '(("https://hnrss.org/show.atom" tools)
          ("https://hnrss.org/frontpage.atom" news)
          ("http://ithare.com/rssfeed/" news)
          ("http://sachachua.com/blog/feed/" emacs)
          ("https://foonathan.net/feed.xml" cpp)
          ("https://blog.demofox.org/atom" gamedev)
          ("https://preshing.com/feed" gamedev)
          ("https://randygaul.github.io/feed.xml" gamedev)
          ("https://seanmiddleditch.com/feed" gamedev)
          ("https://probablydance.com/feed/" gamedev)
          ("https://www.jeremyong.com/feed.xml" gamedev)
          ("http://www.reedbeta.com/feed" cg)
          ("https://ciechanow.ski/atom.xml" cg)
          ("http://www.joshbarczak.com/blog/?feed=rss2" cg)
          ("https://www.jendrikillner.com/tags/weekly/index.xml" cg)
          ("https://nullprogram.com/feed/" ideas)
          ("https://eli.thegreenplace.net/feeds/all.atom.xml" ideas)
          ("http://feeds.feedburner.com/scotthyoung/HAHx" ideas))))

;; Install hunspell package and do `hunspell -D`.  It should show dict files
;; search path and some dictionaries i.e. the aff and dic files of some
;; language should be present in its search path for it to be accessible.
;; Dictionary files are usually available here:
;; https://cgit.freedesktop.org/libreoffice/dictionaries/plain/en/
;; on MSYS2 use `mingw-w64-x86_64-hunspell-en` package for lot of en options
;; https://emacs.stackexchange.com/a/19985/4106
;; https://emacs.stackexchange.com/q/7481/4106 for elisp quasiquoting
(use-package flyspell
  :ensure nil
  ;; run flyspell-prog-mode manually as code is read more often than edited
  :hook ((org-mode markdown-mode gfm-mode) . flyspell-mode)
  :commands flyspell-mode
  :config
  (let ((my/dict-locale "en_GB"))
    (setq ispell-program-name "hunspell"
          ispell-local-dictionary my/dict-locale
          ispell-local-dictionary-alist
          `((,my/dict-locale "[[:alpha:]]" "[^[:alpha:]]" "[']" nil nil nil
                             utf-8)))))

(use-package flyspell-lazy
  :ensure flyspell
  :config (flyspell-lazy-mode 1))

;; needed by Org-mode export to syntax highlight code blocks
(use-package htmlize
  :commands (htmlize-buffer htmlize-region))

;; http://orgmode.org/worg/org-tutorials/orgtutorial_dto.html
;; https://alhassy.github.io/init/
(use-package org
  :ensure org-contrib
  :mode ("\\.org\\'" . org-mode)
  :bind (:map org-mode-map (("C-c l" . org-store-link)
                            ("C-c a" . org-agenda)))
  :defines (org-html-validation-link
            org-html-checkbox-type
            org-latex-listings
            org-latex-pdf-process)
  :config
  (setq org-log-done t
        org-startup-truncated nil
        org-adapt-indentation nil       ; avoid auto-indenting
        org-pretty-entities t           ; show \pi as π in buffer
        org-preview-latex-default-process 'dvisvgm
        org-html-validation-link nil
        org-html-checkbox-type 'html
        ;; use default syntax highlighting; not current Emacs theme’s
        ;; https://stackoverflow.com/q/12169667/183120
        org-html-htmlize-output-type 'css
        org-src-fontify-natively t
        ;; syntax highlight source block (needs pygmentize), set all margins to
        ;; 1.5 cm, warn about widows and orphans and automated integreation of
        ;; SVGs in latex (needs inkspace)
        org-latex-packages-alist '(("" "minted") ("" "fullpage")
                                   ("avoid-all" "widows-and-orphans") ("" "svg"))
        ;; use latex package minted to syntax highlight exported source blocks
        org-latex-listings 'minted
        org-latex-pdf-process
        '("lualatex -shell-escape -interaction nonstopmode -output-directory %o %f"
          "biber %b"
          "lualatex -shell-escape -interaction nonstopmode -output-directory %o %f"
          "lualatex -shell-escape -interaction nonstopmode -output-directory %o %f"))
  ;; https://stackoverflow.com/q/22988092/183120
  ;; Uncomment to enable markdown export backend
  ;; (require 'ox-md nil t)
  ;; https://stackoverflow.com/a/17925855/183120
  ;; https://orgmode.org/worg/org-contrib/babel/languages/index.html
  (org-babel-do-load-languages
   'org-babel-load-languages
   '((C . t)
     (awk . t)
     (emacs-lisp . t)
     (js . t)
     (latex . t)
     (ledger . t)
     (lua . t)
     (makefile . t)
     (org . t)
     (perl . t)
     (python . t)
     (sed . t)
     (sql . t)
     (sqlite . t)
     (shell . t)))

  (defun my/off-longline-highlighting ()
    "Disable `whitespace-mode' by setting a large value."
    (whitespace-mode -1))
  (add-hook 'org-src-mode-hook 'my/off-longline-highlighting)

  ;; prevent org-reveal overiding recentf-open-files
  (unbind-key "C-c C-r" org-mode-map)

  (require 'ox-extra)
  ;; for :ignore: tag to NOT export section heading ONLY as :noexport: ignores
  ;; entire subtree (with contents). See
  ;; https://emacs.stackexchange.com/q/9492/4106
  ;; https://emacs.stackexchange.com/q/8182/4106
  (ox-extras-activate '(ignore-headlines)))

;; in LaTeX mode run pdflatex instead of tex on C-c C-f
;; https://superuser.com/a/142469/50345
(use-package tex-mode
  :ensure nil
  :mode ("\\.tex\\'" . latex-mode)
  :config (setq latex-run-command "lualatex -shell-escape"))

(use-package python
  :mode ("\\.py\\'" . python-mode)
  :config
  (setq-default python-indent-offset 2)
  (setq python-shell-interpreter "python3"))

;; Enable virtual environments for Emacs with `pyvenv-activate` and restart
;; Python with `pyvenv-restart-python`.  If Flycheck isn’t working, kill and
;; restart LSP server after pyvenv-activate.
(use-package pyvenv)

(use-package sh-script
  :ensure nil
  :config
  (unless (executable-find "shellcheck")
    (warn "‘shellcheck’ not found in $PATH to flycheck")))

;; useful for quick lookups
(use-package define-word
  :config
  ;; (setq define-word-default-service 'webster)
  (setq define-word-default-service 'wordnik))

(use-package wiki-summary)

;; (pdf-tools-install) builds epdfinfo and activates the package if already
;; built. Build dependencies (imagemagick, poppler-glib, automake, autoconf,
;; libpng, zlib, etc.) are to be present beforehand.
;; https://github.com/vedang/pdf-tools
(use-package pdf-tools
  :config
  (pdf-loader-install))

;; Universal Ctags frontend
(use-package citre
  :hook ((c-mode c++-mode c-or-c++-mode objc-mode) . citre-mode)
  :config
  (require 'citre-config)
  (defun citre-jump+ ()
    "Use citre-jump; fallback to xref if tags DB is missing e.g. elisp"
    (interactive)
    (condition-case _
        (citre-jump)
      (error (let* ((xref-prompt-for-identifier nil))
               (call-interactively #'xref-find-definitions)))))
  (setq citre-prompt-language-for-ctags-command t
        citre-use-project-root-when-creating-tags t
        ;; override citre-{ctags,readtags}-program if not in $PATH
        )
  (keymap-local-set "M-." 'citre-jump+)
  (keymap-local-set "M-," 'citre-jump-back)
  (keymap-local-set "C-c j" 'citre-ace-peek)
  )

;; customize meson-markdown-docs-dir and point it to the directory containing
;; Reference-manual.md{,.gz} for meson-lookup-doc to work
(use-package meson-mode
  :ensure nil)

(use-package cmake-mode
  :ensure nil)

;; C++ LSP server
;; https://www.sandeepnambiar.com/setting-up-emacs-for-c++/
;; Two options to generate compile_commands.json
;;     ninja -t compdb cc cxx objc objcxx > compile_commands.json
;;     gn gen --export-compile-commands=chrome
(use-package ccls
  :config
  ;; prefer flycheck-lsp over other C++ checkers
  (setq-default flycheck-disabled-checkers
                '(c/c++-clang c/c++-cppcheck c/c++-gcc)))

(use-package with-venv)

(provide '.emacs)
;;; .emacs ends here
