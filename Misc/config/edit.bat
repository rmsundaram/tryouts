@echo off

set argC=0
for %%x in (%*) do Set /A argC+=1

if %argC%==0 goto NO_ARGS

emacsclientw.exe -nqa "runemacs.exe" %*
goto :eof

:NO_ARGS
tasklist /FI "IMAGENAME eq emacs.exe" 2>NUL | find /I /N "emacs.exe">NUL
if "%ERRORLEVEL%" NEQ "0" start runemacs.exe
