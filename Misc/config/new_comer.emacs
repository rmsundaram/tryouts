;;; package --- Summary

;;; Commentary:
;;; See also [1] and [2] for alternatives
;;; [1]: https://github.com/rdallasgray/graphene
;;; [2]: https://github.com/technomancy/better-defaults
;;; Configuration for Emacs 25.1

;;; Code:

;; http://www.emacswiki.org/emacs/ProfileDotEmacs

;; http://www.aaronbedra.com/emacs.d
(setq user-full-name "Sundaram Ramaswamy"
      user-mail-address "legends2k@yahoo.com")

(require 'package)
(add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(add-to-list 'package-archives '("marmalade" . "https://marmalade-repo.org/packages/") t)
(package-initialize)

(when (not package-archive-contents)
  (package-refresh-contents))

(defvar my-packages '(markdown-mode web-mode ido-vertical-mode ido-completing-read+ undo-tree smex srcery-theme
                                    volatile-highlights drag-stuff which-key smart-mode-line)
  "A list of packages to ensure are installed at launch.")

(dolist (p my-packages)
  (when (and (not (package-installed-p p))
             (assoc p package-archive-contents))  ;; http://stackoverflow.com/a/14838150/183120
    (package-install p)))

;; http://stackoverflow.com/a/9472367/183120
(load-theme 'srcery t)
(toggle-frame-fullscreen)

(defvar --backup-directory (concat user-emacs-directory "backups"))
(if (not (file-exists-p --backup-directory))
    (make-directory --backup-directory t))
(setq backup-directory-alist `(("." . ,--backup-directory))) ;; stop leaving backup~ files scattered everywhere

(setq sml/theme 'dark)
(sml/setup)

;; enable for laptops
; (display-battery-mode t)
(menu-bar-mode -1)
(scroll-bar-mode -1)
(tool-bar-mode -1)
;; http://xahlee.blogspot.in/2012/05/emacs-change-cursor-shape-and-highlight.html
(setq-default
 cursor-type 'bar
 abbrev-mode t)
(mouse-avoidance-mode 'jump)                ;; mouse avoidance
(show-paren-mode 1)
(desktop-save-mode 1)                       ;; http://ergoemacs.org/emacs/emacs_make_modern.html
(recentf-mode 1)                            ;; http://www.emacswiki.org/emacs/RecentFiles
(delete-selection-mode 1)                   ;; http://ergoemacs.org/emacs/modernization.html
(drag-stuff-global-mode t)                  ;; http://stackoverflow.com/a/19378355/183120
(drag-stuff-define-keys)                    ;; apply default key bindings
(electric-quote-mode)                       ;; get curved quotes as you type; see 25.1 NEWS
(global-undo-tree-mode t)
;; http://stackoverflow.com/questions/144983#comment49048563_144986
(setq inhibit-startup-echo-area-message "sundaram")

(setq
 column-number-mode t
 ;; http://www.masteringemacs.org/articles/2010/10/04/beginners-guide-to-emacs/
 inhibit-startup-message t
 line-number-display-limit-width 20000     ;; helps when reading files with very long lines
 visible-bell t
 display-time-day-and-date t
 blink-cursor-blinks 0
 confirm-nonexistent-file-or-buffer nil    ;; http://www.masteringemacs.org/article/disabling-prompts-emacs
 vc-handled-backends nil                   ;; http://stackoverflow.com/q/5748814/183120
 initial-major-mode 'text-mode
 initial-scratch-message nil               ;; http://stackoverflow.com/a/10110113/183120
 recentf-max-menu-items 25
 ;; backup options http://stackoverflow.com/a/18330742/183120
 make-backup-files t                       ;; backup of a file the first time it is saved
 backup-by-copying t                       ;; don't clobber symlinks
 version-control t                         ;; version numbers for backup files
 delete-old-versions t                     ;; delete excess backup files silently
 delete-by-moving-to-trash t
 kept-old-versions 6                       ;; oldest versions to keep when a new numbered backup is made (default: 2)
 kept-new-versions 9                       ;; newest versions to keep when a new numbered backup is made (default: 2)
 auto-save-default t                       ;; auto-save every buffer that visits a file
 auto-save-timeout 20                      ;; number of seconds idle time before auto-save (default: 30)
 auto-save-interval 200                    ;; number of keystrokes between auto-saves (default: 300)
 )

(if (>= emacs-major-version 25)
    (remove-hook 'find-file-hooks 'vc-refresh-state)
    (remove-hook 'find-file-hooks 'vc-find-file-hook))

;; http://www.emacswiki.org/emacs/DisplayTime
(display-time-mode)
(which-key-mode)

;; http://www.emacswiki.org/emacs/DisabledCommands
(defun no-prompt-enable-commands (&rest args)
  "Called when ARGS is a disabled command.  Enable and re-execute it."
  (put this-command 'disabled nil)
  (message "You typed %s. %s was disabled. It no longer is."
           (key-description (this-command-keys)) this-command)
  (sit-for 0)
  (call-interactively this-command))
(setq disabled-command-function 'no-prompt-enable-commands)

;; http://stackoverflow.com/a/21837875/183120
(prefer-coding-system 'utf-8-unix)

;; avoid typing yes or no
(fset 'yes-or-no-p 'y-or-n-p)

;; http://www.masteringemacs.org/articles/2010/10/04/beginners-guide-to-emacs/
;; http://whattheemacsd.com/setup-ido.el-01.html
;; https://github.com/DarwinAwardWinner/ido-completing-read-plus
(ido-mode 1)
(ido-everywhere 1)
(setq ido-enable-flex-matching t
      ido-create-new-buffer 'always)

;; https://github.com/gempesaw/ido-vertical-mode.el
(setq ido-vertical-define-keys 'C-n-C-p-only)
(ido-vertical-mode 1)

(require 'ido-completing-read+)
(ido-ubiquitous-mode 1)

;; https://github.com/nonsequitur/smex
(smex-initialize)
(global-set-key (kbd "M-x") 'smex)
(global-set-key (kbd "M-X") 'smex-major-mode-commands)
;; This is the original M-x
(global-set-key (kbd "C-c C-c M-x") 'execute-extended-command)

;; save the window configuration before and restore after diff
(add-hook 'ediff-load-hook
          (lambda ()
            (add-hook 'ediff-before-setup-hook
                      (lambda ()
                        (setq ediff-saved-window-configuration (current-window-configuration))))
            (let ((restore-window-configuration
                   (lambda ()
                     (set-window-configuration ediff-saved-window-configuration))))
              (add-hook 'ediff-quit-hook restore-window-configuration 'append)
              (add-hook 'ediff-suspend-hook restore-window-configuration 'append))))

;; http://www.emacswiki.org/emacs/NoTabs
;; https://www.emacswiki.org/emacs/TabStopList
;; show a tab character as 4 spaces
(setq-default indent-tabs-mode nil tab-width 4)
;; insert 2 spaces on a tab key press
(setq tab-stop-list (number-sequence 2 120 2))

;; http://truongtx.me/2013/03/10/emacs-setting-up-perfect-environment-for-cc-programming/
;; http://www.emacswiki.org/emacs/IndentingC
(setq-default c-default-style "bsd"
              c-basic-offset 2)

;; open .html files in web-mode
(add-to-list 'auto-mode-alist '("\\.htm$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.html$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.css$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.js$" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.json$" . js2-mode))
(add-to-list 'auto-mode-alist '("\\.md.html\\'" . gfm-mode))

;; http://stackoverflow.com/a/25022273
(add-to-list 'auto-mode-alist '("\\.[^/]*rc" . conf-mode) t)

;; open .inl files in C++ mode
(add-to-list 'auto-mode-alist '("\\.inl$" . c++-mode))

;; open .mm files in Obj-C mode
(add-to-list 'auto-mode-alist '("\\.mm$" . objc-mode))

;; http://stackoverflow.com/a/25960236/183120
;; http://home.thep.lu.se/~karlf/emacs.html
(defun my-c-common-setup ()
  (local-set-key (kbd "C-c C-o") 'ff-find-other-file)

  (when (> (display-color-cells) 16)
    (font-lock-add-keywords nil '(("\\<\\(__func__\\|__FUNCTION__\\|__PRETTY_FUNCTION__\\|__LINE__\\|__FILE__\\)"
                                   1 font-lock-preprocessor-face prepend)))))

(defun my-c++-setup ()
  (when (> (display-color-cells) 16)
    (font-lock-add-keywords nil '(("\\<\\(constexpr\\|decltype\\|alignas\\|alignof\\|static_assert\\|\
thread_local\\|nullptr\\|noexcept\\|char16_t\\|char32_t\\)"
                                   1 font-lock-preprocessor-face prepend)))))

(add-hook 'c-mode-common-hook #'my-c-common-setup)
(add-hook 'c++-mode-hook #'my-c++-setup)

(setq my-line-limit 120)
(add-hook 'prog-mode-hook (function (lambda()
                                      (electric-pair-mode)
                                      (display-line-numbers-mode)
                                      ;; http://stackoverflow.com/a/18855782
                                      ;; http://emacs.stackexchange.com/a/16661/4106
                                      (setq whitespace-line-column my-line-limit ; highlight text beyond my-line-limit
                                            whitespace-style '(face trailing empty tabs lines-tail)
                                            fill-column my-line-limit)
                                      (whitespace-mode)
                                      ;; automatically wrap lines beyond my-line-limit
                                      (auto-fill-mode)
                                      (subword-mode)
                                      (when (> (display-color-cells) 16)
                                        (font-lock-add-keywords nil '(("\\<\\(TEST\\|TODO\\|FIXME\\|BUG\\|NOTE\\)"
                                                                       1 font-lock-warning-face prepend))))
                                      )))

(setq markdown-italic-underscore t
      markdown-asymmetric-header t
      markdown-header-scaling t
      markdown-enable-math t
      markdown-hr-strings
      '("--------------------------------------------------------------------------------"
        "* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * "
        "----------------------------------------"
        "* * * * * * * * * * * * * * * * * * * * "
        "----------"
        "* * * * * "))

;; http://www.emacswiki.org/emacs/ElispCookbook
;; http://ergoemacs.org/emacs/elisp_idioms.html
(defun copy-symbol-at-point ()
  "Copy symbol at point, if available."
  (interactive)
  (setq myStr (or (thing-at-point 'symbol) (error "No symbol at point")))
  (kill-new myStr))

(defun kill-symbol-at-point ()
  "Delete symbol at point, if available."
  (interactive)
  (setq bounds (or (bounds-of-thing-at-point 'symbol) (error "No symbol at point")))
  (setq pos1 (car bounds))
  (setq pos2 (cdr bounds))
  (goto-char pos2)
  (push-mark pos1)
  (kill-region pos1 pos2))

(defun mark-symbol-at-point ()
  "Mark symbol at point, if available."
  (interactive)
  (setq bounds (or (bounds-of-thing-at-point 'symbol) (error "No symbol at point")))
  (setq pos1 (car bounds))
  (setq pos2 (cdr bounds))
  (goto-char pos2)
  (push-mark pos1)
  (setq mark-active t))

;; http://stackoverflow.com/a/9414763/183120
(defun copy-file-path-to-clipboard ()
  "Copy the current buffer's file path to the clipboard."
  (interactive)
  (let ((filename (if (equal major-mode 'dired-mode)
                      default-directory
                    (buffer-file-name))))
    (if filename
        (progn
          (when (string-equal system-type "windows-nt")
            (subst-char-in-string ?/ ?\\ filename t))
          (kill-new filename)
          (message "Copied '%s' to the clipboard." filename))
      (message "Buffer not associated to any file."))))

;; http://stackoverflow.com/a/6541072/183120
(defun func-region (start end func)
  "Pass the region between START and END in current buffer to FUNC."
  (save-excursion
    (let ((text (delete-and-extract-region start end)))
      (insert (funcall func text)))))

(defun url-enc-region (start end)
  "URL encode the region between START and END in current buffer."
  (interactive "r")
  (func-region start end #'url-hexify-string))

(defun url-dec-region (start end)
  "URL decode the region between START and END in current buffer."
  (interactive "r")
  (func-region start end #'url-unhex-string))

(defun new-empty-buffer ()
  "Create a new empty buffer.
New buffer will be named “untitled” or “untitled<2>”, “untitled<3>”, etc.

URL `http://ergoemacs.org/emacs/emacs_new_empty_buffer.html'
Version 2017-11-01"
  (interactive)
  (let (($buf (generate-new-buffer "untitled")))
    (switch-to-buffer $buf)
    (funcall initial-major-mode)
    (setq buffer-offer-save t)
    $buf))

(global-set-key (kbd "<f7>") 'new-empty-buffer)
(global-set-key (kbd "C-c C-r") 'recentf-open-files)

;; http://stackoverflow.com/q/91071#comment11739171_96540
(windmove-default-keybindings)

;; custom bindings
(global-set-key (kbd "C-x C-k") 'kill-this-buffer)
(global-set-key (kbd "C-c c") 'copy-symbol-at-point)
(global-set-key (kbd "C-c k") 'kill-symbol-at-point)

(global-set-key (kbd "C-<") 'scroll-down-line)
(global-set-key (kbd "C->") 'scroll-up-line)

;; https://stackoverflow.com/a/10546694/183120
(global-set-key (kbd "C-x t") 'transpose-frame)

;; http://home.thep.lu.se/~karlf/emacs.html
(define-abbrev-table 'global-abbrev-table '(
                                            ("8del"     "Δ")
                                            ("8the"     "θ")
                                            ("8lam"     "λ")
                                            ("8mu"      "µ")
                                            ("8pi"      "π")
                                            ("8in"      "∈")
                                            ("8nin"     "∉")
                                            ("8inf"     "∞")
                                            ("8fra"     "⁄")
                                            ("8min"     "−")
                                            ("8abs"     "|")
                                            ))

(defun markdeepify ()
  "Insert Markdeep tags into current buffer and change to gfm-mode."
  (interactive)
  (gfm-mode)
  (point-to-register ?M)
  (end-of-buffer)
  (insert "\n\n<!-- Markdeep: --><style class=\"fallback\">\
body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src=\"markdeep.min.js\"></script>\
<script src=\"https://casual-effects.com/markdeep/latest/markdeep.min.js\"></script>\
<script>window.alreadyProcessedMarkdeep||(document.body.style.visibility=\"visible\")</script>\
<script>window.markdeepOptions = { tocStyle: 'none'};</script>")
  (beginning-of-buffer)
  (insert "<meta charset=\"utf-8\">\n\n")
  (register-to-point ?M))

(when (require 'volatile-highlights nil 'noerror)
  (volatile-highlights-mode t))

(provide '.emacs)
;;; .emacs ends here
