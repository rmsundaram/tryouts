# https://www.soberkoder.com/better-zsh-history/
export HISTFILE=~/.zsh_history
export HISTFILESIZE=1000000000
export HISTSIZE=1000000000
export HISTTIMEFORMAT="[%F %T] "

setopt INC_APPEND_HISTORY
setopt EXTENDED_HISTORY
setopt HIST_FIND_NO_DUPS
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt HIST_IGNORE_ALL_DUPS
setopt hist_ignore_space      # ignore commands that start with space

# Aliases
#
# Some people use a different file for aliases
# [ -r "${HOME}/.bash_aliases" ] && . "${HOME}/.bash_aliases"
#
# Some example alias instructions
# If these are enabled they will be used instead of any instructions
# they may mask.  For example, alias rm='rm -i' will mask the rm
# application.  To override the alias instruction use a \ before, ie
# \rm will call the real rm not the alias.
#
# Interactive operation...
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
#
# Default to human readable figures
# alias df='df -h'
# alias du='du -h'
#
# Misc :)
# alias less='less -r'                          # raw control characters
# alias whence='type -a'                        # where, of a sort
# alias grep='grep --color'                     # show differences in colour
# alias egrep='egrep --color=auto'              # show differences in colour
# alias fgrep='fgrep --color=auto'              # show differences in colour
#
# Some shortcuts for different directory listings
# classify files, directories, link, … in colour with symbols
# https://unix.stackexchange.com/questions/2897/clicolor-and-ls-colors-in-bash
# also see man page of ls (both GNU and BSD)
# set LS_COLORS to tweak the colour scheme
# macOS’s BSD ls may be expected by build scripts so don’t alias ls to gls
[[ "$OSTYPE" != "darwin"* ]]; then
    # GNU’s ls
    my_ls='ls -F --color'
else
    # BSD’s ls on macOS; use -G instead of --color or CLICOLOR
    my_ls='ls -F -G'
fi
alias ls="${my_ls}"

# alias dir='ls --color=auto --format=vertical'
# alias vdir='ls --color=auto --format=long'
long_ls="${my_ls} -hAgo"                        # long list sans owner & group
# add GNU ls-specific options
if [[ "$OSTYPE" != "darwin"* ]]; then
    long_ls="${long_ls} --group-directories-first"
elif [[ "$OSTYPE" == "darwin"* ]] && hash gls 2>/dev/null; then
    long_ls="g${long_ls} -F --color --group-directories-first"
fi
alias ll="${long_ls}"
# alias la='ls -A'                              # all but . and ..
# alias l='ls -CF'                              #
alias gcc="gcc -fdiagnostics-color=auto"
alias g++="g++ -fdiagnostics-color=auto"

# Previous directory shorthands

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

# or use pre_cmd, see man zshcontrib
vcs_info_wrapper() {
  vcs_info
  if [ -n "$vcs_info_msg_0_" ]; then
    echo "%{$fg[grey]%}${vcs_info_msg_0_}%{$reset_color%}$del"
  fi
}
RPROMPT=$'$(vcs_info_wrapper)'
# https://scriptingosx.com/2019/07/moving-to-zsh-06-customizing-the-zsh-prompt/
PROMPT='%F{cyan}%m:%F{yellow} %B%30<..<%~%b %(!.#.>)%f '

setopt prompt_subst
autoload -Uz vcs_info
zstyle ':vcs_info:*' actionformats \
    '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{3}|%F{1}%a%F{5}]%f '
zstyle ':vcs_info:*' formats       \
    '%F{5}(%f%s%F{5})%F{3}-%F{5}[%F{2}%b%F{5}]%f '
zstyle ':vcs_info:(sv[nk]|bzr):*' branchformat '%b%F{1}:%F{3}%r'
zstyle ':vcs_info:*' enable git
