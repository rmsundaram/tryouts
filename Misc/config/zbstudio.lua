--[[--
  Use this file to specify User preferences.
  Review [examples](+C:\Program Files (x86)\ZeroBrane Studio\cfg\user-sample.lua) or check [online documentation](http://studio.zerobrane.com/documentation.html) for details.
--]]--

-- to specify full path to lua interpreter if you need to use your own version
path.lua53 = 'F:/Apps/Lua/bin/lua.exe'

-- from user-sample.lua
styles = loadfile('cfg/tomorrow.lua')('Monokai')
stylesoutshell = styles
-- from scheme-picker.lua
styles.auxwindow = styles.text
-- let the calltips be contrasting :)
styles.calltip = loadfile('cfg/tomorrow.lua')('SolarizedLight').text
-- switch off local variable indicator
styles.indicator.varlocal = nil

-- from user-sample.lua
editor.fontname = "Ubuntu Mono"
editor.fontsize = 12
editor.checkeol = true
editor.defaulteol = wxstc.wxSTC_EOL_LF

-- Edit menu
keymap[ID.SHOWTOOLTIP] = "Ctrl-Shift-Space"
keymap[ID.AUTOCOMPLETE] = "Ctrl-Space"

-- Project menu
keymap[ID.RUN]              = "Ctrl-F5"
keymap[ID.BREAKPOINTTOGGLE] = "F9"
keymap[ID.BREAKPOINTNEXT]   = "Ctrl-F9"
keymap[ID.BREAKPOINTPREV]   = "Shift-F9"
keymap[ID.STEP]             = "F11"
keymap[ID.STEPOVER]         = "F10"
keymap[ID.STEPOUT]          = "Shift-F11"
