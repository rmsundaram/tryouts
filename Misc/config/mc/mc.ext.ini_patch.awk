#! /usr/bin/awk -f
# ./patch.awk /etc/mc/mc.ext.ini > ~/.config/mc/mc.ext.ini

BEGIN {
    FS = "=";
    # Assume mc 3; only 4+ has ‘Version=x.y’ under ‘[mc.ext.ini]’.
    version = 3;
}

$1 == "Version" {
    version = substr($2, 1, 1) + 0
}

($0 == "[Default]") || ($0 == "default/*") {
    sec_default = 1
}

{
    if (sec_default && ($1 ~ /^[[:space:]]*Open$/))
        if (version >= 4)
            print "Open=xdg-open %f";
        else
            print "\tOpen=xdg-open %f";
    else
        print;
}

END {
    if (version < 4) {
        print "";
        print "include/editor";
        print "\tOpen=%var{EDITOR:vi} %f";
    }
}
