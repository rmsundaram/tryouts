# ~/.bashrc: executed by bash(1) for interactive shells.
# Difference between .bashrc and .bash_profile
# https://superuser.com/q/183870/50345

# If not running interactively, don't do anything
[[ "$-" != *i* ]] && return

# Make sure .bash_profile is absent for Bash to source .profile

# Shell Options
#
# See man bash for more options...
#
# Don't wait for job termination notification
# set -o notify
#
# Don't use ^D to exit
# set -o ignoreeof
#
# Warn when unset parameter expansion is attempted; exits non-interactive shells.
set -o nounset
#
# Use case-insensitive filename globbing
# shopt -s nocaseglob
#
# When changing directory small typos can be ignored by bash
# for example, cd /vr/lgo/apaache would find /var/log/apache
# shopt -s cdspell
#
# enable extended glob patterns
shopt -s extglob

shopt -s dotglob         # include hidden files in filename expansion results
shopt -s checkjobs       # warn about pending jobs on issuing exit; issue exit again to force
shopt -s progcomp_alias  # enable programmable completion for aliased commands
shopt -s histverify      # expand history; don’t execute e.g. `!!` don’t execute but expands

# Completion options
#
# These completion tuning parameters change the default behavior of bash_completion:
#
# Define to access remotely checked-out files over passwordless ssh for CVS
# COMP_CVS_REMOTE=1
#
# Define to avoid stripping description in --option=description of './configure --help'
# COMP_CONFIGURE_HINTS=1
#
# Define to avoid flattening internal contents of tar files
# COMP_TAR_INTERNAL_PATHS=1
#
# Uncomment to turn on programmable completion enhancements.
# Any completions you add in ~/.bash_completion are sourced last.
# Linux/MSYS2
#     install bash-completion
#     source bash_completion only if /etc/bash.bashrc already doesn’t
# macOS Homebrew: for Bash > 4, install bash-completion@2, not bash-completion.
# https://stackoverflow.com/a/18898614/183120
[ -r "${HOMEBREW_PREFIX:-}/etc/profile.d/bash_completion.sh" ] &&
. "${HOMEBREW_PREFIX:-}/etc/profile.d/bash_completion.sh" ||
( [[ "$OSTYPE" == "darwin"* ]] && echo "Couldn't source bash_completion.sh" )

# History Options
#
# https://stackoverflow.com/a/19533853
# https://unix.stackexchange.com/q/6501
# Make bash append rather than overwrite the history on disk
shopt -s histappend
export HISTSIZE=
export HISTFILESIZE=
export HISTTIMEFORMAT='[%F %T] '
# Don't put duplicate lines or lines starting with space in the history.
export HISTCONTROL=${HISTCONTROL:-}${HISTCONTROL+,}ignoreboth
# ${HISTCONTROL+,} is a nice construct; if HISTCONTROL is set, it'll evaluate
# to a comma else to an empty string (http://blog.pancho.name/cgi-bin/blosxom.cgi)
# See Bash manual section Shell Parameter Expansion for details.
#
export HISTFILE=~/.bash_eternal_hist
#
# Ignore some controlling instructions
# HISTIGNORE is a colon-delimited list of patterns which should be excluded.
# The '&' is a special pattern which suppresses duplicate entries.
# Ignore commands like ls as well
export HISTIGNORE=\
$'[ \t]*:&:[fb]g:kill:jobs:dirs:exit:ls*:ll*:lsd:pwd*:*--help:*--version:*top:mpv*:moc*:which*:pi*'
#
# Whenever displaying the prompt, write the previous line to disk
PROMPT_COMMAND="history -a; ${PROMPT_COMMAND:-}"
#
# Prevent Rewriting History with Ctrl+C
# https://stackoverflow.com/q/2458793/183120

# Aliases
#
# Some people use a different file for aliases
# [ -r "${HOME}/.bash_aliases" ] && . "${HOME}/.bash_aliases"
#
# Some example alias instructions
# If these are enabled they will be used instead of any instructions
# they may mask.  For example, alias rm='rm -i' will mask the rm
# application.  To override the alias instruction use a \ before, ie
# \rm will call the real rm not the alias.
#
# Interactive operation...
alias rm='rm -i'
alias cp='cp -ip'
alias mv='mv -i'
#
# Disable background colour and distinguish from regular directories the
# others-writable directories w/o sticky bit with different foreground colours
# https://askubuntu.com/q/466198#comment1450191_466203
# https://stackoverflow.com/a/64590378/183120
LS_COLORS=${LS_COLORS:-}:'tw=00;33:ow=01;33:'; export LS_COLORS
#
# Also See
#   1. https://stackoverflow.com/a/64590378/183120
#   2. LS_COLORS Generator: https://geoff.greer.fm/lscolors/.
#
# Some aliases for different directory listings.
# Classify files, directories, link, … in colour with symbols.
# https://unix.stackexchange.com/questions/2897/clicolor-and-ls-colors-in-bash
# Also see ‘man ls’ (both GNU and BSD).
# OK to alias ls=gls on macOS as expand_aliases is default ON for interactive
# shells only; scripts not explicitly ‘shopt -s expand_aliases’ are unaffected.
# https://unix.stackexchange.com/q/524254/30580
if [[ "$OSTYPE" != "darwin"* ]] || hash gls 2>/dev/null; then
    # GNU’s ls
    my_ls='ls -F --color=auto'       # show classification indicators like */=>@|
    # hAgo: human-readable, all entires (except . and ..), long list sans owner & group
    my_ll="${my_ls} -hAgo --group-directories-first"
    # prefix g on macOS since brew does it for GNU coreutils package
    [[ "$OSTYPE" == "darwin"* ]] && my_ls="g${my_ls}" && my_ll="g${my_ll}"
else
    # BSD’s ls on macOS; use -G instead of --color or CLICOLOR
    my_ls='ls -F -G'
    my_ll="${my_ls} -hAgo"
fi
alias ls='${my_ls}'
alias ll='${my_ll}'
# list all directories under dir(s)
# https://stackoverflow.com/a/29265672/183120
function lsd {
    ls -l --color=always "$@" | grep --color=never '^d'
}
alias gcc='gcc -fdiagnostics-color=auto'
alias g++='g++ -fdiagnostics-color=auto'

# We want mc and e to edit its file argument(s) in Emacs _without blocking_.
# However, $EDITOR should be blocking for programs like git, visudo, etc. Moreover,
# Midnight Commander assumes $EDITOR is an executable, so using a command or function
# is ruled out; also it launches $EDITOR with a single file argument; hence passing
# additional arguments to emacsclient -- which is used as $EDITOR -- is ruled out.
# Using a script (.editor.sh) solves all of these. Make sure it has eXecute permission.
if [[ "$OSTYPE" != "darwin"* ]]; then
    alias mc='EDITOR=${HOME}/.editor.sh . /usr/lib/mc/mc-wrapper.sh'
else
    alias mc='EDITOR=${HOME}/.editor.sh . ${HOMEBREW_PREFIX:-}/opt/mc/libexec/mc/mc-wrapper.sh'
fi
# mc-wrapper.sh to exit to its current directory
# https://unix.stackexchange.com/a/146796/30580
alias e='source ${HOME}/.editor.sh'

# Ensure /etc/pacman.conf has ‘Color’ enabled for pacman and yay to shows colours
if [ -x "$(command -v yay)" ]; then
    function pi {
      yay -Qi "$@" | grep --color -E '(Name|Description|URL)'
    }
    # print pkg files
    alias yl='yay -Ql'
    # print pkg binaries
    function ylb {
      yay -Ql "$@" | grep -E '/bin/[[:print:]]+[^/]$'
    }
    # print pkg libraries
    function yll {
      yay -Ql "$@" | grep -E '/lib/[[:print:]]+[^/]$'
    }
    # print pkg headers
    function ylh {
      yay -Ql "$@" | grep -E '/include/[[:print:]]+[^/]$'
    }
elif [ -x "$(command -v pacman)" ]; then
    function pi {
      pacman -Qi "$@" | grep --color -E '(Name|Description|URL)'
    }
    alias yl='pacman -Ql'
    function ylb {
      pacman -Ql "$@" | grep -E '/bin/[[:print:]]+[^/]$'
    }
    function yll {
      pacman -Ql "$@" | grep -E '/lib/[[:print:]]+[^/]$'
    }
    function ylh {
      pacman -Ql "$@" | grep -E '/include/[[:print:]]+[^/]$'
    }
elif [ -x "$(command -v brew)" ]; then
    alias pi='brew info'
    alias yl='brew list'
    function ylb {
      brew list "$@" | grep -E '/bin/[[:print:]]+[^/]$'
    }
    function yll {
      brew list "$@" | grep -E '/lib/[[:print:]]+[^/]$'
    }
    function ylh {
      brew list "$@" | grep -E '/include/[[:print:]]+[^/]$'
    }
fi
alias ncdu='ncdu --color dark'
alias exer='exercism'
alias mpa='mpv --no-video'
alias ip='ip -c=auto'
alias diff='diff --color=auto'

# Previous directory shorthands

alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

# macOS already has open command
if [[ "$OSTYPE" == "linux-gnu" ]]; then
    alias open='xdg-open'
elif [[ "$OSTYPE" =~ ^(msys|cygwin|win32)$ ]]; then
    alias open='start'
fi
# tree of source-controlled files only; ‘.gitignore’ isn’t always at PWD
alias gt='tree --gitignore -I "build/" -I "target/"'

# Use UNIX-style paths on MSYS2/Windows too.
if [[ "$OSTYPE" =~ ^(msys|cygwin|win32)$ ]]; then
    alias rg="rg --path-separator='//'"
    alias fd="fd --path-separator='//'"
fi

alias ffmpeg='ffmpeg -hide_banner'
alias ffprobe='ffprobe -hide_banner'

# Umask
#
# /etc/profile sets 022, removing write perms to group + others.
# Set a more restrictive umask: i.e. no exec perms for others:
# umask 027
# Paranoid: neither group nor others have any perms:
# umask 077

# Functions
#
# Some people use a different file for functions
# [ -r "${HOME}/.bash_functions" ] && . "${HOME}/.bash_functions"
#
# Some example functions:
#
# a) function settitle
# settitle ()
# {
#   echo -ne "\e]2;$@\a\e]1;$@\a";
# }
#
# b) function cd_func
# This function defines a 'cd' replacement function capable of keeping,
# displaying and accessing history of visited directories, up to 10 entries.
# To use it, uncomment it, source this file and try 'cd --'.
# acd_func 1.0.5, 10-nov-2004
# Petar Marinov, http:/geocities.com/h2428, this is public domain
# cd_func ()
# {
#   local x2 the_new_dir adir index
#   local -i cnt
#
#   if [[ $1 ==  "--" ]]; then
#     dirs -v
#     return 0
#   fi
#
#   the_new_dir=$1
#   [[ -z $1 ]] && the_new_dir=$HOME
#
#   if [[ ${the_new_dir:0:1} == '-' ]]; then
#     #
#     # Extract dir N from dirs
#     index=${the_new_dir:1}
#     [[ -z $index ]] && index=1
#     adir=$(dirs +$index)
#     [[ -z $adir ]] && return 1
#     the_new_dir=$adir
#   fi
#
#   #
#   # '~' has to be substituted by ${HOME}
#   [[ ${the_new_dir:0:1} == '~' ]] && the_new_dir="${HOME}${the_new_dir:1}"
#
#   #
#   # Now change to the new dir and add to the top of the stack
#   pushd "${the_new_dir}" > /dev/null
#   [[ $? -ne 0 ]] && return 1
#   the_new_dir=$(pwd)
#
#   #
#   # Trim down everything beyond 11th entry
#   popd -n +11 2>/dev/null 1>/dev/null
#
#   #
#   # Remove any other occurence of this dir, skipping the top of the stack
#   for ((cnt=1; cnt <= 10; cnt++)); do
#     x2=$(dirs +${cnt} 2>/dev/null)
#     [[ $? -ne 0 ]] && return 0
#     [[ ${x2:0:1} == '~' ]] && x2="${HOME}${x2:1}"
#     if [[ "${x2}" == "${the_new_dir}" ]]; then
#       popd -n +$cnt 2>/dev/null 1>/dev/null
#       cnt=cnt-1
#     fi
#   done
#
#   return 0
# }
#
# alias cd=cd_func

# WARNING: exported functions shouldn’t have Bashisms
# https://unix.stackexchange.com/q/659253/30580
# Print source to STDOUT with syntax highlights and line numbers
function bat {
    local _
    OPTIND=1
    getopts "S:" _ "$@"
    shift $((OPTIND-1))

    for f in "$@"
    do
        if [ $# -gt 1 ] && [ -r "${f}" ]; then
            printf "==> %s <==\n" "${black_on_blue}${f}${default}"
        fi
        if [ -v OPTARG ]; then
            highlight --out-format=truecolor --style=molokai --line-numbers --syntax="${OPTARG}" "${f}"
        elif ! highlight --out-format=truecolor --style=molokai --line-numbers "${f}" 2>/dev/null; then
            highlight --out-format=truecolor --style=molokai --line-numbers --syntax=txt "${f}"
        fi
        [ $# -gt 1 ] && echo
    done
}
export -f bat

# https://stackoverflow.com/a/1655488/183120
# https://stackoverflow.com/a/18457573/183120
# https://stackoverflow.com/a/19075707/183120
function leny {
  if [ -t 0 ]; then
      awk -v green="${green}" -v orange="${orange}" -v normal="${default}" \
          '{ if (length($0) > n) { n = length($0); i = NR; l = $0 } } '`
          `'END { printf "%d chars, %sline %s%s%s: %s\n", n, green, orange, i, normal, l }' \
          "$1"
  else
      awk '{ if (length($0) > n) { n = length($0); i = NR; l = $0 } } '`
      `'END { printf "%d chars, line %s: %s\n", n, i, l}' "$1"
  fi
}
export -f leny

function wr {
    local wr_loc="'$*'"
    curl wttr.in/"${wr_loc}"
}
export -f wr

function scanx {
    # Get latest scan_NNN.jpg; accepts filenames with NUL; needs ‘tr’ to strip
    # NUL as Bash strings can’t contain NUL.
    local latest=$(find . -maxdepth 1 -type f -iname 'scan_*.jpg' -print0 |
      sort -z -t_ -k2 -n | tail -z -n1 | tr -d '\0')
    # Strip prefix: `scan_`.
    latest=${latest##*_}
    # Strip suffix: `.jpg`, add one and compile new name.
    latest='scan_'$(( ${latest%%.*} + 1 ))'.jpg'
    local mode=${1:-Gray}
    local device='hpaio:/net/DeskJet_3630_series?ip=192.168.1.215'
    scanimage -d ${device} -p --resolution 200 --mode ${mode} \
      -x 210 -y 297 --format=jpeg -o ${latest}
}
export -f scanx

function someline {
    if [ $# -lt 1 ]; then
        echo "usage: someline FILENAME"
    elif ! [ -r "${1}" ]; then
        echo "Error: File ${1} not found."
    else
        local n=$( wc -l "${1}" | cut -d' ' -f1 )
        n=$((n))  # convert string to number
        if [ $n -le 0 ]; then
            echo "Empty file: ${1}"
            return
        fi
        # https://stackoverflow.com/a/62445609/183120
        # NOTE: SRANDOM requires Bash 5.1+; for alternative with RANDOM see
        # https://stackoverflow.com/a/65207245/183120
        local i=$(( 1 + SRANDOM % n ))
        # Display line number if connected to terminal
        if [ -t 1 ]; then
            echo -n "${i}: "
        fi
        tail -n +${i} "${1}" | head -1
    fi
}
export -f someline

# added by FZF’s install script; make sure it follows bash_completion
[ -r "${HOME}/.fzf.bash" ] && . "${HOME}/.fzf.bash"

function parse_git_branch {
  git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'
}
export -f parse_git_branch
export PS1='😋 \[\033[01;35m\]\u@\h:\[\033[01;34m\]$(parse_git_branch)'$' \[\033[01;32m\]\w \[\033[01;34m\]\n ↳\[\e[0m\] '

if [[ "$OSTYPE" =~ ^(msys|cygwin|win32)$ ]]; then
    export WORKSPACE=/f/work
    alias dsk='pushd ${USERPROFILE}/Desktop'
    alias dls='pushd ${USERPROFILE}/Downloads'
    alias tmp='pushd ${WORKSPACE}/../tmp'
    alias try='pushd ${WORKSPACE}/tryouts'
    alias blog='pushd ${WORKSPACE}/blog'
else
    alias dsk='pushd ${HOME}/Desktop'
    alias dls='pushd ${HOME}/Downloads'
    alias tmp='pushd ${HOME}/tmp'
    alias try='pushd ${HOME}/work/tryouts'
    alias blog='pushd ${HOME}/work/blog'
fi
