#! /usr/bin/env bash

# Emacs-edit arguments without blocking the caller.
# Tries to open on an existing emacs server or launches a new Emacs instance.

backup_desktop() {
    local count=$1
    local max_idx=$((count-1))
    if [ -r "${HOME}/.emacs.d/.emacs.desktop" ]; then
        local backups
        backups=$( find "${HOME}/.emacs.d" -type f -name '.emacs.desktop.[0-9]' | wc -l )
        backups="${backups##*( )}" # ltrim for "${backups}" to just be a number
        if [[ $backups -lt $count ]]; then
            cp "${HOME}/.emacs.d/.emacs.desktop" "${HOME}/.emacs.d/.emacs.desktop.${backups}"
        else
            \rm -f "${HOME}/.emacs.d/.emacs.desktop.0"
            local i=1
            for ((;i<=max_idx;i++)); do
                mv "${HOME}/.emacs.d/.emacs.desktop.${i}" "${HOME}/.emacs.d/.emacs.desktop.$((i-1))"
            done
            cp "${HOME}/.emacs.d/.emacs.desktop" "${HOME}/.emacs.d/.emacs.desktop.${max_idx}"
        fi
    fi
}

emacs_edit() {
    local running

    # if not windows
    if ! [[ "$OSTYPE" =~ ^(msys|cygwin|win32)$ ]]; then
        # BSD’s pgrep on macOS doesn’t support --count :(
        running=$( pgrep -i emacs | wc -l )
    else
        # pgrep isn’t available by default in MSYS 2
        # `-W` show Windows processes too
        # `-s` show only summary
        running=$( ps -Ws | grep -E --count -i '[^[:alnum:]]emacs(\.exe)?$' )
    fi

    if [[ $running -ne 0 ]]; then
        if [[ $# -ne 0 ]]; then
            ( emacsclient -nqa "emacs" "$@" & ) &> /dev/null
        else
            ( emacsclient -e  "(select-frame-set-input-focus (selected-frame))" ) &>/dev/null
        fi
    else
        # desktop lock file
        rm -rf "${HOME}/.emacs.d/.emacs.desktop.lock"
        # server lock file seen only on Windows
        rm -rf "${HOME}/.emacs.d/server/server"
        # backup desktop file; keep 5 LRU copies
        backup_desktop 5
        ( emacs --directory "${PWD}" "$@" & ) &> /dev/null
        # Windows Terminal MSYS2 tab close terminates running Emacs without
        # waiting; help ~/.bash_logout handle it.
        if [[ "$OSTYPE" =~ ^(msys|cygwin|win32)$ ]]; then
            sleep 3  # $emacs_pid ends up empty without this at times
            emacs_pid=$( ps -s | grep 'bin/emacs' | tr -s ' ' | cut -d ' ' -f2 )
            export emacs_pid
        fi
    fi
}

emacs_edit "$@"

# REFERENCES
# http://stackoverflow.com/a/5571983/183120
# http://stackoverflow.com/a/7131683/183120
# https://stackoverflow.com/a/31519669/183120
# https://stackoverflow.com/a/3904522/183120
# https://stackoverflow.com/a/8193009/183120
# https://emacs.stackexchange.com/a/34415/4106
# http://www.unix.com/shell-programming-and-scripting/30648-suppress-bashs-done-message.html
