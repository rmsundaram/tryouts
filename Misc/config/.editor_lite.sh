#! /usr/bin/env sh

exec emacs --no-window-system --no-desktop "$@"
