# If not running interactively, don't do anything
[[ "$-" != *i* ]] && return

# Skip checking if this session didn’t originally spawn Emacs
[[ ! -v emacs_pid ]] && return

function delay_exit_for_emacs {
    # check if the Emacs process we created is still running
    # https://stackoverflow.com/a/3044045/183120
    while kill -n 0 "${emacs_pid}" &> /dev/null; do
        local sleep_mins=10
        printf "Emacs running... will logout after a nap.\nSleeping %d mins..." ${sleep_mins}
        sleep "${sleep_mins}m"
    done
}

delay_exit_for_emacs
