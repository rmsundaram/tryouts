# Make sure to fzf, fd, highlight and tree are in $PATH
# Source this from .bashrc

# This file is the customization of fzf’s install script output
# Fix paths based on what pacman installs
# git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
# install --no-fish --key-bindings --completion

run_scripts() {
    # fzf’s scripts locations in Linux, Windows and macOS
    local possibilities=("/usr/share/fzf/"
                         "${MINGW_PREFIX:-}/share/fzf/"
                         "${HOMEBREW_PREFIX:-}/opt/fzf/shell/")
    local fzf_share
    for p in "${possibilities[@]}"; do
        [[ -d "${p}" ]] && fzf_share="${p}" && break
    done

    # Auto-completion
    # ---------------
    [[ $- == *i* ]] && source "${fzf_share}/completion.bash" 2> /dev/null

    # Key bindings
    # ------------
    # Include for Ctrl-R, Ctrl-T and Alt-C to work
    source "${fzf_share}/key-bindings.bash"
}

run_scripts

# ------------------------------------------------------------------------------
# User Customizations

# OVERRIDE DEFAULTS
# Setting fd as the default source for fzf.
# Params to fd taken from _fzf_compgen_path in completion.bash shipped with fzf.
# Params for repos with submodules: `--no-ignore-vcs --no-ignore-parent`.
export FZF_DEFAULT_COMMAND='fd --type f --type d --type l -u --follow --exclude ".git"'

# https://unix.stackexchange.com/a/403917/30580
# When selecting files with fzf, we show file content with syntax highlighting,
# or without highlighting if it's not a source file. If the file is a directory,
# we use tree to show the directory's contents.
# We only load the first 200 lines of the file which enables fast previews
# of large text files.
# Requires highlight and tree: pacman -S highlight tree
export FZF_DEFAULT_OPTS='--cycle --marker="*" --prompt="$ " --no-hscroll
 --preview "([[ -d {} ]] && tree -C {} || ([[ $(file --mime {}) =~ binary ]] &&
 echo binary || highlight -O truecolor -s molokai -l {} ||
 cat -n {})) 2> /dev/null | head -100"'


# SHORTCUTS OVERRIDE

# Use --type d variant for changing directory
export FZF_ALT_C_COMMAND='fd --type d -u --follow --exclude ".git"'

export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"


# COMPLETION FUNCTIONS OVERRIDE
# Use fd (https://github.com/sharkdp/fd) instead of the default find
# command for listing path candidates.
# - The first argument to the function ($1) is the base path to start traversal
# - See the source code (completion.{bash,zsh}) for the details.
# TODO: Consider directly using the variables ${FZF_DEFAULT_COMMAND}; avoid duplication.
_fzf_compgen_path() {
    command fd --type f --type d --type l --hidden --follow --exclude ".git" "$1"
}

# Use fd to generate the list for directory completion
_fzf_compgen_dir() {
    command fd --type d --hidden --follow --exclude ".git" "$1"
}


# enable ** for other commands with _fzf_path_completion or _fzf_dir_completion
# completion.bash shipped with fzf already sets this for a bunch of popular commands.

# `emacs **` works but for custom alias `e **` it doesn’t
complete -o bashdefault -o default -F _fzf_path_completion e
complete -o bashdefault -o default -F _fzf_path_completion nano
complete -o bashdefault -o default -F _fzf_path_completion ll
complete -o bashdefault -o default -F _fzf_path_completion bat
complete -o bashdefault -o default -F _fzf_path_completion mpv

complete -o bashdefault -o default -F _fzf_dir_completion rg
complete -o bashdefault -o default -F _fzf_dir_completion fd
complete -o bashdefault -o default -F _fzf_dir_completion tree

# USAGE GIST
#
# Ctrl-T    Searches in $PWD; keeps typed contents
#  Alt-C    Search in $PWD and changes directory; wipes out contents invariably
#     **    Uses preceeding string as search root; keeps typed contents
# Ctrl-R    Searches history
#
# Commands like `kill` need just TAB; while others like `unset` needs `**` too

# ConEmu sets TERM to xterm-256color for MSYS; fzf works only when it’s unset
if [[ -n "${ConEmuBaseDir:-}" ]]; then
    fzf() {
        local term_bkp=$TERM
        TERM=
        $( command -v fzf ) "$@"
        TERM=$term_bkp
    }
    # since fzf.bash is sourced in .bashrc, this needn’t be exported
fi
