module.exports = {
  defaultBrowser: "Firefox",
  rewrite: [
    {
      // Redirect all urls to use https
      match: ({ url }) => url.protocol === "http",
      url: { protocol: "https" }
    }
  ],
  handlers: [
    // https://github.com/johnste/finicky/wiki/Configuration-ideas
    {
      // Get bundle ID: osascript -e 'id of app "Word"'
      // https://stackoverflow.com/a/39464824/183120
      // Open any link clicked on Microsoft apps in Edge
      match: ({opener}) =>
      ["com.microsoft.Outlook",
       "com.microsoft.teams",
       "com.microsoft.Word",
       "com.microsoft.onenote.mac",
       "com.microsoft.Excel",
      ].includes(opener.bundleId),
      browser: "Microsoft Edge Canary"
    },
    {
      // GlobalProtect VPN works only with Safari
      match: ({ opener }) =>
      opener.bundleId.startsWith("com.paloaltonetworks"),
      match: [ /^https:\/\/login.microsoftonline\.com\/.*$/, ],
      browser: "Safari"
    },
    {
      match: [
      /^https?:\/\/.*visualstudio\.com.*$/,
      /^https?:\/\/.*chromium\.org.*$/,
      /^https?:\/\/.*azurewebsites\.net.*$/,
      /^https?:\/\/.*azure\.com.*$/,
      /^https?:\/\/.*sharepoint\.com.*$/,
      /^https?:\/\/.*microsoft\.com.*$/,
      /^https?:\/\/.*microsofticm\.com.*$/,
      /^https?:\/\/.*microsoftstream\.com.*$/,
      /^https?:\/\/crbug\.com.*$/,
      /^https?:\/\/.*pluralsight\.com.*$/,
      /^https?:\/\/.*aka\.ms.*$/,
      /^https?:\/\/.*windows\.net.*$/,
      /^https?:\/\/.*edgeteam\.ms.*$/,
      /^https?:\/\/.*googlesource\.com.*$/,
      /chromium/,],
      browser: "Microsoft Edge Canary"
    }
  ]
};
