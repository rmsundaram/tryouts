# .profile is read by many environments like Bash, Xfce, etc.  It’s
# sourced by graphical, interactive and non-interactive logins.  Bash
# reads this too when ~/.bash_{profile,login} is missing; having a
# .bash_profile is redundant.  Symlink .zprofile to .profile for Zsh
# to source this.  Make sure this script has no Bashisms; use checkbashisms.
# https://unix.stackexchange.com/q/4621/30580
# https://unix.stackexchange.com/q/3052/30580
# https://unix.stackexchange.com/q/45684/30580
# https://stackoverflow.com/a/9915302/183120
# https://askubuntu.com/a/121075/43759
# https://superuser.com/q/187639/50345
# http://mywiki.wooledge.org/Bashism

# System wide environmental variables needed in Bash and Xfce.
# Setting these here, makes sure Emacs gets the right exec-path
# even when launched through menus.

# if go is present and GOPATH isn’t set, set it if available at home
[ -x "$(command -v go)" ] && [ -z "${GOPATH}" ] && [ -d "${HOME}/.go" ] &&
    export GOPATH="${HOME}/.go" && PATH="${PATH}:${GOPATH}/bin"

# if cargo isn’t in PATH then look for a local installation
! [ -x "$(command -v cargo)" ] && [ -r "${HOME}/.cargo/env" ] &&
    . "${HOME}/.cargo/env"

# https://stackoverflow.com/a/18434831/183120
case $(uname | tr '[:upper:]' '[:lower:]') in
    darwin*)
        # set brew’s env variables
        if [ -x "/opt/homebrew/bin/brew" ]; then
            eval "$(/opt/homebrew/bin/brew shellenv)"
        elif [ -x "/usr/local/bin/brew" ]; then
            eval "$(/usr/local/bin/brew shellenv)"
        fi
        ;;
esac

export PATH=$PATH:${HOME}/.local/bin

# https://unix.stackexchange.com/a/87763
# https://discourse.brew.sh/t/failed-to-set-locale-category-lc-numeric-to-en-ru/5092/26
export LANG=en_IN.UTF-8

# source Bash customizations; ensure PATH is set
[ -n "${BASH_VERSION}" ] && [ -r "${HOME}/.bashrc" ] && . "${HOME}/.bashrc"

# Set PATH so it includes user's private bin if it exists
# if [ -d "${HOME}/bin" ] ; then
#   PATH="${HOME}/bin:${PATH}"
# fi

# Set MANPATH so it includes users' private man if it exists
# if [ -d "${HOME}/man" ]; then
#   MANPATH="${HOME}/man:${MANPATH}"
# fi

# Set INFOPATH so it includes users' private info if it exists
# if [ -d "${HOME}/info" ]; then
#   INFOPATH="${HOME}/info:${INFOPATH}"
# fi

# https://github.com/jeaye/stdman
# http://www.linuxcommand.org/lc3_adv_tput.php
# http://www.cyberciti.biz/faq/bash-shell-change-the-color-of-my-shell-prompt-under-linux-or-unix/
# http://stackoverflow.com/a/4931773/183120

# Colours
# Define as constants directly instead of respective tput commands as tput
# doesn’t seem to work when .profile is first sourced by a graphical login
# (Xfce4) environment; it works when logged straight into Bash (Ctrl + Alt + 2,
# 3, …), not via an emulator; quoted printing of, say $LESS_TERMCAP_mb prints
# nothing. Use `printf '%q\n' $(tput sgr0)` to obtain these constants.
# https://unix.stackexchange.com/q/417746/30580

# $(tput sgr0)
default="$(printf '\E(B\E[m')"
# $(tput bold; tput setaf 1)
red="$(printf '\E[1m\E[31m')"
# $(tput setaf 2)
green="$(printf '\E[32m')"
# $(tput setaf 3)
orange="$(printf '\E[33m')"
# $(tput bold; tput setaf 3)
yellow="$(printf '\E[1m\E[33m')"
# $(tput bold; tput setab 1; tput setaf 3)
yellow_on_red="$(printf '\E[1m\E[41m\E[33m')"
# $(tput setab 4; tput setaf 0)
black_on_blue="$(printf '\E[44m\E[30m')"

export default red green orange yellow yellow_on_red black_on_blue

# Less colors for man pages
export PAGER=less
# Begin blinking
export LESS_TERMCAP_mb=$yellow_on_red
# Begin bold
export LESS_TERMCAP_md=$red
# End mode
export LESS_TERMCAP_me=$default
# End standout-mode
export LESS_TERMCAP_se=$default
# Begin standout-mode - info box
export LESS_TERMCAP_so=$black_on_blue
# End underline
export LESS_TERMCAP_ue=$default
# Begin underline
export LESS_TERMCAP_us=$green
# Fix for grotty 1.23 (2023 Jul 7) breaking man page colours; refer `man grotty`
# https://unix.stackexchange.com/q/6010#comment1428988_6357
export GROFF_NO_SGR=1

# When $LESS is set, Git pages even when contents is less than a screen long.
# Avoid this with `-FX` but only for Git by setting DELTA_PAGER, setting it here
# would mean `man ls` leaving paged contents in shell, above the prompt.
# https://stackoverflow.com/a/26069/183120
# https://stackoverflow.com/a/2183920/183120
# See also `man git-config` on `core.pager`.
export LESS="${LESS:-} --ignore-case --RAW-CONTROL-CHARS"
# Avoid showing <C2><A0> instead of quotes on macOS / iTerm
# https://stackoverflow.com/q/19435071/183120
export LESSCHARSET=utf-8

# https://unix.stackexchange.com/q/107315/30580
# less -iR -F
export DELTA_PAGER="$PAGER $LESS --quit-if-one-screen"

# $EDITOR should be blocking; most terminal programs expect this. Programs like
# git, visudo, … create a temporary, pass it to a new instance of $EDITOR and
# await process closure to read the file. This is explained under ‘man sudo’ for
# the -e option. Try connecting to Emacs server with emacsclient; if it fails,
# create a slim Emacs session inside terminal.
#
# Programs like mc, sudoedit, visudo, … expect $EDITOR to be pointing to an
# executable, not a command; so having a command with arguments won’t work;
# having an executable script is workaround.
#
# On Windows, if Emacs isn’t already running, emacsclient launches the alternate
# editor non-blockingly, so the waiting process gets an empty file! Even passing
# nano doesn’t work; on Windows, emacsclient is useful only if Emacs is running!
export EDITOR="emacsclient -qa $HOME/.editor_lite.sh"
export VISUAL="$EDITOR"

export RIPGREP_CONFIG_PATH="$HOME/.config/.ripgreprc"
