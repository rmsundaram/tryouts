RAID-1 is for _high-availability_ and isn’t the right choice for backups.  Having separate backups -- independent copies -- is the right way to mitigate drive failures.  We don’t want data to be available readily despite a hard disk failure; rather only data being present is enough.  Another issue with RAID: if there’s an accidental delete of a file on one disk, the other copy is gone _automatically_; RAID-1 disks are literally mirrors -- they show up as on file (e.g. `/dev/md0`).  A software glitch, virus or a wrong command (e.g. `dd` with wrong `of`) kills both “copies”.

Having separate (decoupled), periodic backups is the way to go.  This can be done with software like

* FreeFileSync (compare by: modified time + size and content; local database)
* Unison (compare by content/hash - good for consistency checks; per-user database)
* rdiff-backup + rsync (one way only)
* git-annex

Deleting one drive accidentally means the other drive still has that data safely.  What’s potentially losable is the diff between the two drives since the day of last sync.

# Advantages

* Cross-platform: with RAID if both drives are made _Linux file systems_, Windows can’t read them.
* Different filesystems: One drive can be ext4 and other NTFS in case of backups
* Be at different geo-locations
* Drive sizes can be very different; not approximately same like RAID

# See Also

* [Synchronization and Backup Programs - Arch Wiki](https://wiki.archlinux.org/index.php/Synchronization_and_backup_programs)
* [A utility to keep two separate backup drives](https://www.reddit.com/r/DataHoarder/comments/dz035f/a_utility_to_keep_two_separate_backup_drives_in/)
* [Sync Data b/w Two Disks](https://superuser.com/questions/320888/sync-data-between-two-disks/320897)
* [5 Reasons why RAID is not Backup](https://blog.storagecraft.com/5-reasons-raid-not-backup/)
* [Comparison of file sync programs](https://blog.grahampoulter.com/2012/08/comparison-of-file-sync-programs.html)
