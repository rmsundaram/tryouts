#include <iostream>
#include <iterator>
#include <algorithm>
#include <stdexcept>

using std::cout;
using std::cin;
using std::endl;
using std::ostream_iterator;
using std::copy;
using std::runtime_error;

void checkCudaError(const cudaError_t &err);

// when the number of threads is less, the computation isn't done
// for example giving 3 threads the kernel didn't perform any operation

const unsigned NUM_CHANNELS = 3u;
const size_t IMAGE_DIM = 30u;
const size_t IMAGE_SIZE = IMAGE_DIM * NUM_CHANNELS;
const size_t BLOCK_COUNT = 2u;
const size_t THREAD_COUNT = IMAGE_SIZE / BLOCK_COUNT;

template <typename T>
void cudaAlloc(T* &ptr, size_t amount)
{
	cudaError_t err = cudaMalloc(&ptr, amount * sizeof(*ptr));
	checkCudaError(err);
}

template <typename T, size_t N>
void cudaAllocCopy(T* &dest, T (&src)[N])
{
	cudaAlloc(dest, N);
	cudaMemcpy(dest, src, N * sizeof(T), cudaMemcpyHostToDevice);
}

template <typename T, size_t N>
void cudaCopyBack(T (&dest)[N], T* &src)
{
	cudaMemcpy(dest, src, N * sizeof(T), cudaMemcpyDeviceToHost);
}

template <typename T, size_t N>
const unsigned char (&ArrayLen(const T (&strLiteral) [N]))[N];
#define arrayLength(arrName) sizeof(ArrayLen(arrName))

__global__ void transpose(float *mixed, float* transposed)
{
	const size_t i = (blockIdx.x * blockDim.x) + threadIdx.x;
	const float val = i % NUM_CHANNELS;
    mixed[i] = val;

	const size_t offset = (gridDim.x * blockDim.x / NUM_CHANNELS) * val;
	const size_t j = offset + (i / NUM_CHANNELS);
	transposed[j] = val;
}

int main(int argc, char* argv[])
{
	float *d_mixed, *d_sep;
	cudaAlloc(d_mixed, IMAGE_SIZE);
	cudaAlloc(d_sep, IMAGE_SIZE);

	transpose<<<BLOCK_COUNT, THREAD_COUNT>>>(d_mixed, d_sep);
	cudaError_t err = cudaDeviceSynchronize();
	checkCudaError(err);

    float h_mixed[IMAGE_SIZE] = { };
	float h_sep[IMAGE_SIZE] = { };
	cudaCopyBack(h_mixed, d_mixed);
	cudaCopyBack(h_sep, d_sep);

	ostream_iterator<float> osi(cout, " ");
	cout << "Mixed:" << endl;
	copy(h_mixed, h_mixed + arrayLength(h_mixed), osi);
	cout << endl;
	cout << "Transposed:" << endl;
	copy(h_sep, h_sep + arrayLength(h_sep), osi);
	cout << endl;

	cudaFree(d_mixed);
	cudaFree(d_sep);
    d_mixed = d_sep = NULL;

    // Destroy all allocations and reset all state on the current device in the current process explicitly
	err = cudaDeviceReset();
	checkCudaError(err);

	cin.get();
}

void checkCudaError(const cudaError_t &err)
{
	if (err != cudaSuccess)
	{
		cout << cudaGetErrorString(err) << endl;
	}
}
