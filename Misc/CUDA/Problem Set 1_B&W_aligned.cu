// Homework 1
// Color to Greyscale Conversion

//A common way to represent color images is known as RGBA - the color
//is specified by how much Red, Grean and Blue is in it.
//The 'A' stands for Alpha and is used for transparency, it will be
//ignored in this homework.

//Each channel Red, Blue, Green and Alpha is represented by one byte.
//Since we are using one byte for each color there are 256 different
//possible values for each color.  This means we use 4 bytes per pixel.

//Greyscale images are represented by a single intensity value per pixel
//which is one byte in size.

//To convert an image from color to grayscale one simple method is to
//set the intensity to the average of the RGB channels.  But we will
//use a more sophisticated method that takes into account how the eye 
//perceives color and weights the channels unequally.

//The eye responds most strongly to green followed by red and then blue.
//The NTSC (National Television System Committee) recommends the following
//formula for color to greyscale conversion:

//I = .299f * R + .587f * G + .114f * B

//Notice the trailing f's on the numbers which indicate that they are 
//single precision floating point constants and not double precision
//constants.

//You should fill in the kernel as well as set the block and grid sizes
//so that the entire image is processed.

#include "reference_calc.cpp"
#include "utils.h"
#include <stdio.h>

__global__
void rgba_to_greyscale(const uchar4* const rgbaImage, unsigned char* greyImage)
{
  //TODO
  //Fill in the kernel to convert from color to greyscale
  //the mapping from components of a uchar4 to RGBA is:
  // .x -> R ; .y -> G ; .z -> B ; .w -> A
  //
  //The output (greyImage) at each pixel should be the result of
  //applying the formula: output = .299f * R + .587f * G + .114f * B;
  //Note: We will be ignoring the alpha channel for this conversion

  //First create a mapping from the 2D block and grid locations
  //to an absolute 2D location in the image, then use that to
  //calculate a 1D offset
    const size_t offset = ((blockDim.x * gridDim.x) * ((blockDim.y * blockIdx.y) + threadIdx.y)) + (blockDim.x * blockIdx.x);
    const size_t pixelID = offset + threadIdx.x;
    greyImage[pixelID] = (rgbaImage[pixelID].x * 0.299f) + (rgbaImage[pixelID].y * 0.587f) + (rgbaImage[pixelID].z * 0.114f);
}

void your_rgba_to_greyscale(const uchar4 * const h_rgbaImage, uchar4 * const d_rgbaImage,
                            unsigned char* const d_greyImage, size_t numRows, size_t numCols)
{
    // SUNDARAM
    // the assumption here is that numRows and numCols are multiples of thread_x and thread_y
    // this should be checked for irregular sized (non-aligned) images and padded accordingly
    // but constructing a padded image and getting back the unpadded part from it back would be costly
    // the GPU (Tesla M2050) on the grading server has 1536 threads per SM.
    // The limit on a thread block is 1024 on this GPU (older ones have 512)
    // hence 32 x 32 is the maximum possible, while alloting 32 x 32 isn't optimal. 16 x 16 gives
    // the maximum performance since it allows maximum usage of all available SMs optimally.
    // 1536 / (16 * 16) = whole number, while 1536 / (32 * 32) = 1.5, although
    // 1536 / (8 x 8) is a whole number it's slightly slower than 16 x 16
    const size_t thread_x = 16, thread_y = 16;
    const size_t block_x = static_cast<size_t>(ceilf(numRows / static_cast<float>(thread_x)));
    const size_t block_y = static_cast<size_t>(ceilf(numCols / static_cast<float>(thread_y)));

  //You must fill in the correct sizes for the blockSize and gridSize
  //currently only one block with one thread is being launched
    const dim3 blockSize(thread_x, thread_y, 1);  //TODO
    const dim3 gridSize(block_x, block_y, 1);  //TODO
    rgba_to_greyscale<<<gridSize, blockSize>>>(d_rgbaImage, d_greyImage);
    
    cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
}

