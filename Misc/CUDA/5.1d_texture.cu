// comes around 0.679328 ms with 1d texturing, to avoid making a CUDA array copy
// there's no such thing as texture memory in CUDA, texture object is mapped to either linear (global) array
// or to CUDA array again in global memory

#include "reference_calc.cpp"
#include "utils.h"

const size_t MAX_FILTER_SIZE = 31 * 31;
__constant__ float c_filter[MAX_FILTER_SIZE];
__constant__ size_t c_filterWidth;
__constant__ int c_halfFilterWidth;
__constant__ int c_numRows;
__constant__ int c_numCols;

// using a 1d texture seems to give slightly higher perf.
// than just using global memory due to spatial caching policy
texture<uchar4, 1> imgTex;

__global__
void gaussian_blur(uchar4* const d_outputImageRGBA)
{
  const int2 pos = make_int2((blockDim.x * blockIdx.x) + threadIdx.x, (blockDim.y * blockIdx.y) + threadIdx.y);
  // early exit for irregular sized image boundary
  if (pos.x >= c_numCols || pos.y >= c_numRows) return;

  float3 pixel;
  int2 neighbourPos = make_int2(0, 0);
  for (int row = -c_halfFilterWidth; row <= c_halfFilterWidth; ++row)
  {
     neighbourPos.y = min(max(pos.y + row, 0), c_numRows - 1);
     for (int col = -c_halfFilterWidth; col <= c_halfFilterWidth; ++col)
     {
       // clamping for border pixels since the filter is a square
       neighbourPos.x = min(max(pos.x + col, 0), c_numCols - 1);
       const uchar4 neighbourPixel = tex1Dfetch(imgTex, neighbourPos.y * c_numCols + neighbourPos.x);
       const float filterVal = c_filter[((row + c_halfFilterWidth) * c_filterWidth) + (col + c_halfFilterWidth)];
       pixel.x += filterVal * neighbourPixel.x;
       pixel.y += filterVal * neighbourPixel.y;
       pixel.z += filterVal * neighbourPixel.z;
     }
  }
  const size_t pixelIndex = (pos.y * c_numCols) + pos.x;
  d_outputImageRGBA[pixelIndex] = make_uchar4(pixel.x, pixel.y, pixel.z, 255);
}

void allocateMemoryAndCopyToGPU(const size_t numRowsImage, const size_t numColsImage,
                                const float* const h_filter, const size_t filterWidth)
{
  const size_t filterSize = filterWidth * filterWidth * sizeof(*h_filter);  
  checkCudaErrors(cudaMemcpyToSymbol(c_filter, h_filter, filterSize));
  checkCudaErrors(cudaMemcpyToSymbol(c_filterWidth, &filterWidth, sizeof(filterWidth)));
  const int halfFilterWidth = filterWidth / 2;
  checkCudaErrors(cudaMemcpyToSymbol(c_halfFilterWidth, &halfFilterWidth, sizeof(halfFilterWidth)));
  const int nCols = (int) numColsImage;
  const int nRows = (int) numRowsImage;
  checkCudaErrors(cudaMemcpyToSymbol(c_numRows, &nRows, sizeof(c_numRows)));
  checkCudaErrors(cudaMemcpyToSymbol(c_numCols, &nCols, sizeof(c_numRows)));
}

void your_gaussian_blur(const uchar4 * const h_inputImageRGBA, uchar4 * const d_inputImageRGBA,
                        uchar4* const d_outputImageRGBA, const size_t numRows, const size_t numCols,
                        unsigned char *d_redBlurred, 
                        unsigned char *d_greenBlurred, 
                        unsigned char *d_blueBlurred,
                        const int filterWidth)
{
  const dim3 blockSize(16, 16, 1);
  const dim3 gridSize(ceilf(numCols / static_cast<float>(blockSize.x)), ceilf(numRows / static_cast<float>(blockSize.y)), 1);

  cudaChannelFormatDesc channelDesc = cudaCreateChannelDesc<uchar4>();
  imgTex.addressMode[0] = cudaAddressModeClamp;
  imgTex.filterMode = cudaFilterModePoint;
  imgTex.normalized = false;
  checkCudaErrors(cudaBindTexture(NULL,
                                  imgTex,
                                  d_inputImageRGBA,
                                  channelDesc,
                                  sizeof(*d_inputImageRGBA) * numRows * numCols));

  gaussian_blur<<<gridSize, blockSize>>>(d_outputImageRGBA);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
}

void cleanup()
{
}
