#include <iostream>
#include <cuda_runtime.h>

struct vec2 {
  float x;
  float y;
};

struct vec3 {
  float x;
  float y;
  float z;
};

__global__ void cylinder(vec3* f, float radius, vec2* ctrlPts) {
  int idx = threadIdx.y * blockDim.x + threadIdx.x;
  f = f + blockDim.x * threadIdx.y + threadIdx.x;
  float t = (float)threadIdx.x / blockDim.x;
  f->x = radius * cos(2.0 * 3.14159 * t);
  f->y = radius * sin(2.0 * 3.14159 * t);
  f->z = threadIdx.y * 10;
  // Top boundary as defined by the Bezier curve with control point P1 and P2
  // (ctrlPts) and end points P0 = P3 = (0, 0).
  if ((threadIdx.y + 1) == blockDim.y) {
    // Evaluate cubic Bezier in Y alone assuming P0 = P3 = (0, 0).
    float bezY = (3.0 * (1.0 - t) * (1.0 - t) * t * ctrlPts[0].y) +
      (3.0 * (1.0 - t) * t * t * ctrlPts[1].y);
    float d = fmax(abs(ctrlPts[0].y), abs(ctrlPts[1].y));
    f->z += 10 * ((d != 0.0f) ? (bezY / d) : d);
  }
}

int main() {
  const size_t rows = 3;
  const size_t cols = 6;
  const size_t bytes = rows * cols * sizeof(vec3);
  const size_t numBlocks = 1;
  const dim3 numThreads(cols, rows);

  // Number of vertices: 2 rows, 6 cols, sizeof(vec3)
  vec3* h_msg = new vec3[rows * cols];
  vec3* d_msg;
  cudaMalloc((void**)&d_msg, bytes);

  // Refer //CG/WebGL/spindle.mjs.
  vec2 bez[2] = { {80.0f, 60.0f}, {-80.0f, 60.0f} };
  cylinder<<<numBlocks, numThreads>>>(d_msg, 7.0f, bez);

  cudaMemcpy(h_msg, d_msg, bytes, cudaMemcpyDeviceToHost);

  for (auto r = 0; r < rows; ++r) {
    for (auto c = 0; c < cols; ++c)
      printf("(%f,%f,%f)\t",
             h_msg[r * cols + c].x,
             h_msg[r * cols + c].y,
             h_msg[r * cols + c].z);
    printf("\n");
  }

  delete [] h_msg;
  cudaFree(d_msg);

  return 0;
}
