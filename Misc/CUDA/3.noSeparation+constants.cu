// doing away with channel separation gives a huge boost
// comes around 0.707616 ms without texturing; just global memory and constants

#include "reference_calc.cpp"
#include "utils.h"

const size_t MAX_FILTER_SIZE = 31 * 31;
__constant__ float c_filter[MAX_FILTER_SIZE];
__constant__ size_t c_filterWidth;
__constant__ int c_halfFilterWidth;
__constant__ int c_numRows;
__constant__ int c_numCols;

__global__
void gaussian_blur(const uchar4* const d_inputImageRGBA,
                         uchar4* const d_outputImageRGBA)
{
  const int2 pos = make_int2((blockDim.x * blockIdx.x) + threadIdx.x, (blockDim.y * blockIdx.y) + threadIdx.y);
  // early exit for irregular sized image boundary
  if (pos.x >= c_numCols || pos.y >= c_numRows) return;

  const size_t pixelIndex = (pos.y * c_numCols) + pos.x;
  float3 pixel;
  for (int row = -c_halfFilterWidth; row <= c_halfFilterWidth; ++row)
  {
     for (int col = -c_halfFilterWidth; col <= c_halfFilterWidth; ++col)
     {
       // clamping for border pixels since the filter is a square
       const int2 neighbourPos = make_int2(min(max(pos.x + col, 0), c_numCols - 1), min(max(pos.y + row, 0), c_numRows - 1));
       const size_t neighbourPos1d = neighbourPos.y * c_numCols + neighbourPos.x;
       const uchar4 neighbourPixel = d_inputImageRGBA[neighbourPos1d];
       pixel.x += c_filter[((row + c_halfFilterWidth) * c_filterWidth) + (col + c_halfFilterWidth)] * neighbourPixel.x;
       pixel.y += c_filter[((row + c_halfFilterWidth) * c_filterWidth) + (col + c_halfFilterWidth)] * neighbourPixel.y;
       pixel.z += c_filter[((row + c_halfFilterWidth) * c_filterWidth) + (col + c_halfFilterWidth)] * neighbourPixel.z;
     }
  }
  d_outputImageRGBA[pixelIndex] = make_uchar4(pixel.x, pixel.y, pixel.z, 255);
}

void allocateMemoryAndCopyToGPU(const size_t numRowsImage, const size_t numColsImage,
                                const float* const h_filter, const size_t filterWidth)
{
  const size_t filterSize = filterWidth * filterWidth * sizeof(*h_filter);  
  checkCudaErrors(cudaMemcpyToSymbol(c_filter, h_filter, filterSize));
  checkCudaErrors(cudaMemcpyToSymbol(c_filterWidth, &filterWidth, sizeof(filterWidth)));
  const int halfFilterWidth = filterWidth / 2;
  checkCudaErrors(cudaMemcpyToSymbol(c_halfFilterWidth, &halfFilterWidth, sizeof(halfFilterWidth)));
  const int nCols = (int) numColsImage;
  const int nRows = (int) numRowsImage;
  checkCudaErrors(cudaMemcpyToSymbol(c_numRows, &nRows, sizeof(c_numRows)));
  checkCudaErrors(cudaMemcpyToSymbol(c_numCols, &nCols, sizeof(c_numRows)));
}

void your_gaussian_blur(const uchar4 * const h_inputImageRGBA, uchar4 * const d_inputImageRGBA,
                        uchar4* const d_outputImageRGBA, const size_t numRows, const size_t numCols,
                        unsigned char *d_redBlurred, 
                        unsigned char *d_greenBlurred, 
                        unsigned char *d_blueBlurred,
                        const int filterWidth)
{
  const dim3 blockSize(16, 16, 1);
  const dim3 gridSize(ceilf(numCols / static_cast<float>(blockSize.x)), ceilf(numRows / static_cast<float>(blockSize.y)), 1);
  gaussian_blur<<<gridSize, blockSize>>>(d_inputImageRGBA,
                                         d_outputImageRGBA);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
}

void cleanup()
{
}
