// very slow because of the copy to CUDA array 1.8 s

#include "reference_calc.cpp"
#include "utils.h"

const size_t MAX_FILTER_SIZE = 31 * 31;
__constant__ float c_filter[MAX_FILTER_SIZE];
__constant__ size_t c_filterWidth;
__constant__ int c_halfFilterWidth;
__constant__ int c_numRows;
__constant__ int c_numCols;

cudaArray *cuArray;
texture<uchar4, 2> imgTex;

__global__
void gaussian_blur(uchar4* const d_outputImageRGBA)
{
  const int2 pos = make_int2((blockDim.x * blockIdx.x) + threadIdx.x, (blockDim.y * blockIdx.y) + threadIdx.y);
  // early exit for irregular sized image boundary
  if (pos.x >= c_numCols || pos.y >= c_numRows) return;

  const size_t pixelIndex = (pos.y * c_numCols) + pos.x;
  float3 pixel;
  for (int row = -c_halfFilterWidth; row <= c_halfFilterWidth; ++row)
  {
     for (int col = -c_halfFilterWidth; col <= c_halfFilterWidth; ++col)
     {
       const int2 neighbourPos = make_int2(pos.x + col, pos.y + row);
       const uchar4 neighbourPixel = tex2D(imgTex, neighbourPos.x, neighbourPos.y);
       pixel.x += c_filter[((row + c_halfFilterWidth) * c_filterWidth) + (col + c_halfFilterWidth)] * neighbourPixel.x;
       pixel.y += c_filter[((row + c_halfFilterWidth) * c_filterWidth) + (col + c_halfFilterWidth)] * neighbourPixel.y;
       pixel.z += c_filter[((row + c_halfFilterWidth) * c_filterWidth) + (col + c_halfFilterWidth)] * neighbourPixel.z;
     }
  }
  d_outputImageRGBA[pixelIndex] = make_uchar4(pixel.x, pixel.y, pixel.z, 255);
}

void allocateMemoryAndCopyToGPU(const size_t numRowsImage, const size_t numColsImage,
                                const float* const h_filter, const size_t filterWidth)
{
  const size_t filterSize = filterWidth * filterWidth * sizeof(*h_filter);  
  checkCudaErrors(cudaMemcpyToSymbol(c_filter, h_filter, filterSize));
  checkCudaErrors(cudaMemcpyToSymbol(c_filterWidth, &filterWidth, sizeof(filterWidth)));
  const int halfFilterWidth = filterWidth / 2;
  checkCudaErrors(cudaMemcpyToSymbol(c_halfFilterWidth, &halfFilterWidth, sizeof(halfFilterWidth)));
  const int nCols = (int) numColsImage;
  const int nRows = (int) numRowsImage;
  checkCudaErrors(cudaMemcpyToSymbol(c_numRows, &nRows, sizeof(c_numRows)));
  checkCudaErrors(cudaMemcpyToSymbol(c_numCols, &nCols, sizeof(c_numRows)));
}

void your_gaussian_blur(const uchar4 * const h_inputImageRGBA, uchar4 * const d_inputImageRGBA,
                        uchar4* const d_outputImageRGBA, const size_t numRows, const size_t numCols,
                        unsigned char *d_redBlurred, 
                        unsigned char *d_greenBlurred, 
                        unsigned char *d_blueBlurred,
                        const int filterWidth)
{
  const dim3 blockSize(16, 16, 1);
  const dim3 gridSize(ceilf(numCols / static_cast<float>(blockSize.x)), ceilf(numRows / static_cast<float>(blockSize.y)), 1);

  cudaChannelFormatDesc channelDesc =
      cudaCreateChannelDesc(8, 8, 8, 8, cudaChannelFormatKindUnsigned);
  checkCudaErrors(cudaMallocArray(&cuArray, &channelDesc, numCols, numRows));
  checkCudaErrors(cudaMemcpyToArray(cuArray,
                                    0,
                                    0,
                                    d_inputImageRGBA,
                                    sizeof(*d_inputImageRGBA) * numRows * numCols,
                                    cudaMemcpyDeviceToDevice));
  imgTex.addressMode[0] = cudaAddressModeClamp;
  imgTex.addressMode[1] = cudaAddressModeClamp;
  imgTex.normalized = false;
    
  checkCudaErrors(cudaBindTextureToArray(imgTex, cuArray, channelDesc));

  gaussian_blur<<<gridSize, blockSize>>>(d_outputImageRGBA);
  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());
}

void cleanup()
{
    cudaFreeArray(cuArray);
}
