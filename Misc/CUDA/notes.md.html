<meta charset="utf-8">

         **GPGPU with CUDA**
    *Digest of Introduction to Parallel Programming*

Resume https://www.youtube.com/watch?v=bMI7FnMKYV4&list=PLAwxTw4SYaPnFKojVQrmyOGFCqHTxfdv2&index=167

# Introduction

- CPUs have been clocked at faster rates but they’re now plateauing
  + Power consumption and keeping CPUs cool has been a challenge
- Processor manufacturers have turned to putting more processors instead of making one faster
- CPUs have complex controls, expensive power/thermal needs but offer higher flexiblity and performance
- GPUs have simpler controls, better power/thermal needs and offer higher throughput for restrictive programming model
- GPUs have low latency but high throughput (a.k.a bandwidth)
  + Ploughing a field with two oxens (CPUs) vs many chickens (GPU)
  + Racing car (latency 200 kmph, throughput 0.089 persons/hour) vs Bus (latency 50 kmph, throughput 0.44)
- Good to solve problems requiring high throughput but latency per element/task isn’t crucial
- GPUs are designed with
  + lots of simple compute units; trade control for more compute
  + explicitly parallel programming model
  + optimize for throughput
- Parallel programming is all about many threads solving a problem by working together
  + Communication is key to any cooperative work
  + Communication happens through memory
  + Really all about how to map threads/tasks and memory together

# CUDA Basics

- Device: GPU, Host: CPU
- Written like serial programs
- CUDA code / kernel looks like C but with extensions
  + Functions start with `__global__` or `__device__` declspec
- Kernel is written as if it runs on one thread
- CPU code controls _threads/block_ (x, y, z) and _blocks/grid_ (a, b, c) counts
  leading to n threads = (x * y * z) * (a * b * c)
  + Threads in a block cooperate to solve a sub-problem
- Has parallels to C’s functions
  + `cudaMalloc`
  + `cudaMemcpy`
  + `cudaMemset`
- Dereferencing GPU pointers in C is undefined; pass the right pointers to the right kind of functions
  + Tip: `d_` and `h_` prefixes for host and device memory pointers
- A bunch of threads live in a block; every device has a limitation of max threads/block
- A bunch of blocks live in a grid
- Every kernel thread can know its block and thread indices
- CUDA’s programming model is a hierarchy of 
  + Computation: Threads, Thread Blocks, Kernels
  + Memory: Local, Shared and Global
  + Sync: barrier, `__syncthreads()` (a barrier within a block), implicit barrier between kernels
- Kernel call looks like `fn<<<b ,t, shmem>>>()`
  + b = blocks/grid
  + t = threads/block
  + shmem = bytes of shared memory to allocate (declare array with `extern` in kernel to use it)
    * Another option is to drop this param and directly declare a shared array in kernel
  + Count dims are 3D

## GPU Hardware

- GPU contains many SMs (streaming multiprocessors)
  + SMs contain many CUDA cores (scalar processors)
  + Cores are organized into warps executing in lockstep; sharing the same IP
  + Warp = 32 threads running the same instruction
- It’s GPU’s responsibility to assign a thread block to an SM
  + It’s the programmer’s to specify the threads/block and blocks/grid counts
- An SM may run more than one block concurrently
  + All threads of an SM may not be trying to solve the same (sub)problem as it may
    be running two blocks each with its own (sub)problem
  + Threads in different blocks shouldn’t attempt to cooperate
- A block never runs on more than one SM; runs on same SM same time
- Restrictions
  + Cannot specify block -> SM mapping i.e. where a task is performed or what order tasks are picked up
  + No communication between blocks
- Guarantees
  + Offers high scalability; GPUs from one to several SMs can be scale to solve a problem with many elements
  + When an SM is done running a task it can be repurposed to run another without awaiting other SMs

# Parallel Communication Patterns

## Map

- 1:1
- function needn’t lookup elements other than input
- Reads an input array, writes to an output array of the same isze
- Every element in input is mapped (transformed) to output using a function
- Simplest and used in embarrassingly parallel problems; GPUs are extremely good at maps

## Gather

- n:1
- Read from different locations, write to one
  + Task needs to read from a different place or multiple places
- E.g. Average, blur, etc.; averages n elements by reading n locs and write to one
- Amenable to data reuse due to overlap in adjacent cells

## Scatter

- 1:n
- Read from one location, write to many (or a different location)
  + Task needs to write to a different place or multiple places
- E.g. Average, blur, etc.; read one element and write value/n to n locs using `+=`
- Amenable to data race due to multiple threads writing to same cell

## Stencil

- 1:n or n:1
- Read input from a fixed neighbourhood (stencil) in an array
- A superset of {gather/scatter} (depending on implementation) but unlike a
  gather/scatter it always reads/writes from/to every location in its neighbourhood

### 2D von Neumann

- Every element would be read 5 times
- Every element would be read 7 times in the 3D variant

*****************************
*           +---+           *
*           |   |           *
*       +---+---+---+       *
*       |   |   |   |       *
*       +---+---+---+       *
*           |   |           *
*           +---+           *
*****************************

### 2D Moore

- Every element would be read 9 times

*****************************
*       +---+---+---+       *
*       |   |   |   |       *
*       +---+---+---+       *
*       |   |   |   |       *
*       +---+---+---+       *
*       |   |   |   |       *
*       +---+---+---+       *
*****************************

## Transpose

- 1:1
- Transforms rows to columns and vice versa
- Reorders data elements in memory
- It’s a kind of scatter/gather; writes/reads to/from a loc ≠ previous read/write loc
- Useful to convert AOS (array of structs) to SOA (struct of arrays)

## Reduce

- all:1
- Binary partitioning solution; requires reduction operator that is
  + Binary
  + Associative (e.g. addition, product, logical OR/AND, min, etc.)
- Step and Work complexity analysis
  + Parallel reduce requires log₂n steps
  + E.g. total number of additions = work count; number of loops to complete
    them in parallel is step count; 1024 elements need 1023 additions,
    10 steps 512 + 512, 256 + 256, 128 + 128, …
- Unsurprising as partitioning of steps gives a binary tree
- Complexity
  + Step: O(log₂ n)
  + Work: O(n) (n-1 operations like serial implementation)

## Scan

- all:all
- Canonical example: Running total of elements e.g. [1 2 3 4] -> [1 3 6 10]
- Very useful in parallel world as opposed to the serial world
- Lot of applications; _compaction_ and _allocation_ are the most popular
  + Others include quick sort, sparse matrix computation, data compression, etc.
- Addresses problems otherwise difficult to parallelize
- Inputs: array, binary-associative operator and identity element
  + Similar to reduce in that it takes a binary, associative operator
- Two kinds (easily changeable; see code)
  + Inclusive scan includes current: [1 2 3 4] -> [1 3 6 10]
  + Exclusive scan doesn’t: [1 2 3 4] -> [0 1 3 6] (use identity for O₀)

``` c
// Exclusive scan
int acc = identity;

for (size_t i = 0; i < elements.length(); ++i) {
  // Swap these lines for inclusive scan
  out[i] = acc;
  acc = acc op elements[i];
}
```

- Algorithm 1: Hillis/Steele Scan (inclusive)
  + At step n, a[i] ⊕ a[i-2ⁿ] if (i-2ⁿ) >= 0 else 0
  + Step complexity: O(log₂ n)
  + Work complexity: O(nlog₂ n)
- Algorithm 2: Blelloch Scan (exclusive)
  + Reduce and Reverse Sweep
  + Step complexity: 2 log₂ n
  + Work complexity: O(n)
- Prefer Blelloch if you’re thread-poor

## Histogram

- Bucketize samples into predefined bins
  + Increment respective bin-counter per sample
- Naïve atomics-based implementation will become bin-count-bound
  + Even if the GPU can spawn 1 million threads they’ll wait for their turn to
    increment the bin atomically; essentially serializing all the read-modify-writes
- Two alternatives
  + Alternative 1
    * Compute thread-local histogram using local memory; no contention
    * _Reduce_ bins into corresponding bin of global histogram
      + Bin count isn’t prohibitively big?  Reduce histogram-wise instead of bin-wise.
  + Alternative 2
    * Sort and reduce by key

## Compact / Filter

- Produce a dense array of interesting items, throwing away uninteresting ones
- Map every items to 0 or 1 based on a unary predicate say `isInteresting()` to an array
  + Sometimes the output can be an integer (not just bool)
  + e.g. number of output triangles from an input triangle (clipped by a rectangle)
  + **Aside**: Maximum number of triangles resulting from a triangle × rectangle intersection: 5
- Run exclusive scan to on resulting array giving the _scatter addresses_ for the compacted array
- Scatter input into output using addresses
- A shorter output array → fewer memory ops → faster kernel

# Synchronization

## Barrier

All threads should reach the barrier to proceed in their own separate ways/speeds; `__syncthreads()`.
Synchronize reads/writes amongst threads in a block with barrier.

## Atomics

Atomic operations like `atomicAdd`, `atomicCAS`, `atomicMin`, `atomicXor`, etc. prevent data races with
special hardware by serializing mutations of a (shared/global) memory location by multiple threads.

Only some datatypes (mostly integrals) are supported; only `atomicAdd` and `atomicExch` for floats.
Workaround is to use Compare-And-Swap (CAS); refer [3](#references).

Takes a bit more time but better than barriers.

# Optimizations

- Maximize arithmetic intensity (math / memory ratio)
  + Maximize compute ops per thread / Minimize time spent on memory ops per threads
- Prefer local (registers) > shared (L1 cache) > global > host memory
- _Coalesce_ global memory access (cache coherency by accessing contiguous memory locations) e.g. `i`, `k+i`, etc.
  + _Strided_ access patten is not so good e.g. `k*i`
  + _Random_ is the worst
- Copy frequently accessed data in global memory to faster (local/shared) memory
- Avoid thread divergence
  + Branching
  + Loops with different limits (threads in a warp share IP; threads with lower loop limit awaits ones with higher limit)

# References

1. [Intro to Parallel Programming](https://www.youtube.com/playlist?list=PLAwxTw4SYaPnFKojVQrmyOGFCqHTxfdv2)
2. [Dive Into Systems](https://diveintosystems.org/book/C15-Parallel/gpu.html#_gpu_architecture_overview)
3. [CUDA Programming Guide](https://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html)


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js" charset="utf-8"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js" charset="utf-8"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none', inlineCodeLang: 'c' };</script>
