<meta charset="utf-8">

           **Bit Wizardry**
    *Tips, Tricks and Explanations*

<!-- Make Markdeep play nicely with angle brackets -->
<script type="preformatted">

# Concepts

## Two’s Complement

The method of complements was commonly used in mechanical calculators to subtract using addition gear; it’s still used in modern computers.[4]  N's complement of a number is obtained by replacing each digit with `N − digit`.

!!! Tip: Why 2’s complement?
    Like all complements, it simplifies subtraction into addition.  _Adding_ minuend to the two’s complement of subtrahend gives their difference.

Two’s complement is commonly used to represent signed numbers; it divides a datatype’s states into positive and negative space without wasting a state (zero has two states in sign-magnitude representation) e.g. `i8` can hold one of `[-128, 127]` values.  **Positive numbers are represented as-is, their two’s complement = _negative_ representation** e.g. `32 = 0b0010_0000` in two’s complement is `0b1110_0000 = -32`.

``` rust
let a = 0b1110_0000_u8 as i8;
println!("{} = 0b{:08b}", a, a);     // -32 = 0b11100000
println!(" {} = 0b{:08b}", -a, -a);  //  32 = 0b00100000
```

!!! Note: One’s vs Two’s complement
    One’s complement flips bits; two’s complement flips sign (negates).  Two’s complement of a positive number is the representation of its _negative_.  Likewise, two’s complement of a negative number is the representation of _its_ negative i.e. positive.  Why?  Negation is its own inverse.

### Decimal → Two’s Complement

Two's complement of an n-bit number is its complement with respect to 2ⁿ.  These methods give a number’s two’s complement:

1.  Subtract number from 2ⁿ
  - Subtract an 8-bit number from `1_0000_0000` to get its two's complement
2. Flip all the bits (one's complement) and add one
3. Negate

### Two’s Complement → Decimal

Steps [3]

1. Check if the MSB is set
2. Unset? Positive; interpret as unsigned number
3. Set? Negative; two’s complement and prefix with `−`

Basically the MSB’s weight is considered negative[2] e.g. unsigned 4-bit number, 1100 = 2³ + 2² + 0 + 0 = 12; signed, two's complement 4-bit number, 1100 = −2³ + 2² + 0 + 0 = −4.

!!! Warning: -ve of most -ve
    Negation of the most negative number representable in a given bit-width will again be negative i.e. in a 4-bit two's complement system, abs(−8) = −8; not only 0 but also −2^(N−1) is equal to its negative[5].

With two’s complement signed type

* Bitwise `NOT` = increase by 1 and flip sign: `!4 = -5`, `!-5 = 4`, `!-1 = 0`, `!0 = -1`
  - Python does this as it has only signed numbers
* `-1` is represented with all bits set e.g. `-1_i8 = 0b1111_1111`, `-1_i16 = 0xff_ff`
* For POTs ones grow from LSB to MSB for positives, zeros grow for negatives
  + `-2 = 0b1111_1110`, `-4 = 0b1111_1100`, `-8` = `0b1111_1000`, `-16 = 0b1111_0000`
  + Largest negative is just MSB set e.g. `-128 = 0b1000_0000`
  + Smaller negatives grow from LSB e.g. `-127 = 0b1000_0001`, `-126 = 0b1000_0010`, `-125 = 0b1000_0011`

# Tricks

## `- 1` sets lower bits

Subtracting 1 from a bit pattern would lead to everything from LSB to the first set bit be flipped.  This effect is well-pronounced for a power of 2 (POT). E.g.

``` c
00010000 - 1 = 00001111
11010100 - 1 = 11010011
01010111 - 1 = 01010110
```

The technique of checking if a number is a power of 2 by checking `x & (x - 1) == 0` arises from this observation.

## `+ 1` clears lower bits

Adding 1 to a bit pattern would lead to everything from LSB to the first cleared bit to be flipped.  This effect is well-pronounced for a number ending with a continuous sequence of 1s. E.g.

``` c
01010111 - 1 = 01011000
00010000 + 1 = 00010001
01010100 + 1 = 01010101
```

## `−` flips higher bits

Negating (not to be confused with flipping/complementing) a number in a two's complement system flips everything after the first set bit.  Conversely, flipping everything after the first set bit, negates the number[1].

``` c
−01010100 = 10101100
−00010000 = 11110000
−01010111 = 10101001
```

!!! Tip: Easy Two’s Complement
    Flipping everything after the first set bit i.e. negating it is (mentally) an easy way to find a number’s two’s complement.

## {multiply,divide} by POT

In the decimal system multiplying by 10 (the radix) would mean suffixing a zero and dividing by 10 would be to shift the radix point left by one digit. This is works due to the place-value system's basic nature.

Similarly in binary, multiplying by 2 would mean suffixing a zero, or left shifting e.g. `i * 2 = i << 1`.  The converse, division by 2, would be achieved by right shifting i.e. reducing the exponent of 2 by one for all weights e.g. `i / 2 = i >> 1`.

### Quotient

Generalizing above to higher POTs, quotient of power of two division = right shift log₂(divisor) times = right shift by count of divisor’s trailing zeros.

C++20’s `std::countr_zero()` and Rust’s `u32::trailing_zeros()` are useful.  `u32::ilog2()` also works as it’s equivalent to trailing zeros _for POT_.

``` rust
println!("{} {}", 8_u8.trailing_zeros(), 8_u8.ilog2());  // 3 3
println!("{} {}", 9_u8.trailing_zeros(), 9_u8.ilog2());  // 0 3
```

Refer `//C++/log2.cpp` (for an intrinsic-based implementation), `//C++/idiv_pow2.cpp` and [6].

### Remainder

Remainder for power of two divisors _regardless of sign_ is given by `x & (x - 1)` e.g. `i % 4` can be obtained by `i & (4 - 1)`.  This fast modulo is useful to index into arrays with indices beyond bounds (or even negative) [7].  Refer `//CG/Noise/value_noise.md.html` for usage in creating seamless textures.

``` rust
// Returns wrapped around index for slices of POT length
fn wrap_idx_pot_slice(count: isize, i: isize) -> isize
{
  i & (count - 1)
}

// Returns wrapped around index for slices of any length; this is the Knuth
// modulo (refer //C++/modulo.cpp).
fn wrap_idx_slice(count: isize, i: isize) -> isize
{
  ((i % count) + count) % count
}

fn main() {
  println!("{} -> {}", 15, wrap_idx_pot_slice(16, 15));  // 15 -> 15
  println!("{} -> {}", 16, wrap_idx_pot_slice(16, 16));  // 16 ->  0
  println!("{} -> {}", 31, wrap_idx_pot_slice(16, 31));  // 31 -> 15
  println!("{} -> {}", 32, wrap_idx_pot_slice(16, 32));  // 32 ->  0
  println!("{} -> {}", 33, wrap_idx_pot_slice(16, 33));  // 33 ->  1
  println!("{} -> {}", 0, wrap_idx_pot_slice(16, 0));    //  0 ->  0
  println!("{} -> {}", -1, wrap_idx_pot_slice(16, -1));  // -1 -> 15
  println!("{} -> {}", -2, wrap_idx_pot_slice(16, -2));  // -2 -> 14
  println!("{} -> {}", -3, wrap_idx_pot_slice(16, -3));  // -3 -> 13
}
```

### `ilog2`

`ilog2`, index of the highest bit set, gives the rounded down value of log₂(x).  However, for the bit width required to represent a number in binary ⌈log₂(x + 1)⌉ = 1 + ⌊log₂(x)⌋ = 1 + `ilog2()`.

| Value | log₂   | `ilog2` | Bits |
|:-----:|:------:|:-------:|:----:|
| 7     | 2.8074 | 2       | 3    |
| 8     | 3      | 3       | 4    |
| 9     | 3.1699 | 3       | 4    |
  [bit width = 1 + ⌊log₂(x)⌋]

Rust has `ilog2`; C++20 has `std::bit_width()`; both lacks the other.  `std::bit_width() - 1` gives `ilog2`.  Rust implementation using `PrimInt::leading_zeros()`; not using `ilog2` as it’ll panic if input is `0`.

``` rust
use num::{PrimInt, Unsigned};

// Returns the bit width to represent an unsigned integer as usize.
// Returns 0 if |x| = 0.
fn bit_width<T: PrimInt + Unsigned>(x: T) -> usize {
  const BITS: usize = std::mem::size_of::<T>() * 8;  // u32::BITS
  BITS - x.leading_zeros() as usize
}
```

### Multiply 8-bit colours (Blinn Fix)

Multiplying 8-bit colour values where one usually wants 0 × 0 = 0 and 255 × 255 = 255 can be done with

``` rust
let temp: u32 = a * b + 128;
let result: u8 = (temp + (temp >> 8)) >> 8;
```

### Lerp-Extend Lower Bits

A 5-bit value can be extended to 8-bits with lower 3 bits linear interpolating between 0 and max; can be useful with colour types like RGB565.

``` rust
v = (v << 3) | (v >> 2);
```

#### Result

| 5 bits | 8 bits        |
|--------|---------------|
| 00000  | 00000 **000** |
| 00001  | 00001 **000** |
| 00011  | 00011 **000** |
| 00100  | 00100 **001** |
| 00101  | 00101 **001** |
| 11011  | 11011 **110** |
| 11111  | 11111 **111** |

Unsigned overflow wrap around (modulo 2ⁿ) can also be seen as correct result’s N least significant bits e.g. with `u8` values `0b1111_1111` + `0b100` = `0b1_0000_0011` -> `0b0000_0011`.

# References

1. [Signed Number Representations](http://en.wikipedia.org/wiki/Signed_number_representations)
2. [Two’s Complement](http://en.wikipedia.org/wiki/Two%27s_complement)
3. [How can I convert 2's complement to decimal?](http://math.stackexchange.com/a/285468/51968)
4. [Method of Complements](http://en.wikipedia.org/wiki/Method_of_complements)
5. Matters Computational (fxtbook), §1.1.4 A pitfall (two's complement)
6. [Fast computing of log2 for 64-bit integers](https://stackoverflow.com/a/64297768/183120)
7. [Fastest way to get a positive modulo in C/C++](https://stackoverflow.com/q/14997165/183120)
8. [Bit Twiddling - Jari Komppa](https://solhsa.com/boolean.html)

# See Also

* [Bit Twiddling Hacks](https://graphics.stanford.edu/~seander/bithacks.html)
* _Write Great Code_, Volume 1
* `./binary_ops.c`
* [That XOR Trick](https://florian.github.io/xor-trick/) enumerates bunch of XOR tricks

<link rel="stylesheet" href="https://casual-effects.com/markdeep/latest/apidoc.css?">
<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js" charset="utf-8"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js" charset="utf-8"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'long', inlineCodeLang: 'rust' };</script>
