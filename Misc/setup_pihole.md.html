<meta charset="utf-8">

    **Pi-hole Setup**
     *Nitty-Gritties*

# Machine Names in Logs

Your Pi-hole may be logging all queries as coming from the router.  Almost all machines use DHCP method of obtaining IP address when connecting to a network.  A side effect of this is the DHCP server assigns the DNS values too; setting DNS server in each machine is tedious, cumbersome and un-scalable.  The DHCP server by default is Netgear; Netgear routers have [DNS Relay][] and [there’s no way to turn it off][netgear-dns-relay].  So all machines get both the IP address and DNS server values from the router; due to _DNS Relay_ they all get the router as DNS server.

To make all devices directly call the DNS server (RPi), enable Pi-hole’s DHCP server and disable router’s.  This sets Pi-hole as the DNS server on machines requesting an IP address.  Pi-hole’s DHCP server also has the feature of assigning constant IPs to devices based on MAC[^assign-ip].  It additionally saves one from the hassle of keeping `/etc/hosts` updated with IP addresses and hostnames if a router allowed disabling _DNS Relay_ and we’ve constant IP allocations for different machines on the router [[1](#references)] [[2](#references)].

!!! Tip: Chicken or Egg?
    How would RPi get its IP address when it is the DHCP server?  It’d be configured to request a static IP.

## Raspbian OS

Raspbian OS uses `/etc/dhcpcd.conf`; configure static IP:

``` config
interface eth0
  static ip_address=192.168.1.10/24
  static routers=192.168.1.1
  static domain_name_servers=1.1.1.1 1.0.0.1
```

For the secondary interface, edit `/etc/network/interfaces.d/00-eth0-subs`.

``` config
auto eth0:1
allow-hotplug eth0:1
iface eth0:1 inet static
  vlan-raw-device eth0
  address 192.168.1.28
  netmask 255.255.255.0
  gateway 192.168.1.1
```

If you want a pure isc-dhcp-based (`/etc/network/interfaces`) configuration (without dhcpcd), follow DietPi section after disabling and removing dhcpcd; [refer][raspbian-static-ip]:

``` bash
systemctl disable --now dhcpcd
apt remove dhcpcd5
```

## DietPi

DietPi’s configuration lies at `/etc/network/interfaces` (as it uses `ifupdown`); this can be set via `dietpi-config` interface too.

``` config
allow-hotplug eth0
iface eth0 inet static
    address 192.168.1.10
    netmask 255.255.255.0
    gateway 192.168.1.1
    #dns-nameservers 9.9.9.9 149.112.112.112
```

Refer Secondary DNS Server Choices section to setup the secondary IP.

!!! Note: Ignore `/etc/dhcpcd.conf`
    DietPi uses `isc-dhcp-client` and not `dhcpcd` [[3](#references)].

!!! Tip: Unbound
    It’s a good idea to use `unbound` as Pi-hole’s upstream DNS server.  Follow [official guide][unbound] but ignore the section on disabling `unbound-resolvconf.service` and enabling `dhcpcd.service`.  Set Pi-hole’s Upstream DNS Servers for both _Custom {1, 2} (IPv4)_ to `127.0.0.1#5335` (i.e. Unbound).

Test unbound using

``` shell
dig cnn.com @127.0.0.1 -p5335
dig sigfail.verteiltesysteme.net @127.0.0.1 -p 5335
dig sigok.verteiltesysteme.net @127.0.0.1 -p 5335
```

## Fix Date & Time

Unbound DNS doesn’t work if date and time is incorrect.  Check current time with `date`.  If incorrect, ensure `dietpi-config` -> `Advanced Options` -> `Time sync mode` is set to `[Boot + Daily]` and `NTP Mirror` is set to `debian.pool.ntp.org`.  Run `/boot/dietpi/func/run_ntpd 1` to correct time.  If it time sync fails, set time manually

``` shell
timedatectl set-time "2023-05-20 22:37:13"
# set-time fails if NTP service is active; deactivate and set-time again
timedatectl set-ntp 0
# Activate NTP service back if originally active
timedatectl set-ntp 1
/boot/dietpi/func/run_ntpd 1
# Date and Time will be correct and NTP service will be deactivated now
```

`timedatectl status` having NTP service inactive is expected as DietPi runs `systemd-timesyncd` service (at boot and) daily and stops it after time sync to conserve resources.  Hence `systemctl status systemd-timesyncd` will be inactive but enabled.

[DNS Relay]: http://forums.dlink.com/index.php?topic=9283.0
[netgear-dns-relay]: https://community.netgear.com/t5/Nighthawk-WiFi-Routers/Disable-DNS-Relay-on-R9000/td-p/1190459
[raspbian-static-ip]: https://raspberrypi.stackexchange.com/q/78510
[unbound]: https://docs.pi-hole.net/guides/dns/unbound/

[^assign-ip]: Irrespective of your DHCP server choice, assigning IPs to some devices like wireless printers, file servers is essential to save client from having to locate it every time before use e.g. Android devices searching for wireless printers.

# Secondary DNS

OSes have two DNS servers.  Some treat secondary as fallback while other as alternative where both are tried for speed [[6](#references)].  Linux and Windows does the first [[5](#references)], while some may be doing the second.

!!! Warning: Android
    When DHCP server only suggests one DNS server, secondary server falls back to Google DNS!  Workaround is to modify Android’s Wi-Fi settings to use Static IP (instead of DHCP) which allows assigning DNS servers.  Proper fix is to have DHCP server issue both servers.

Pi-hole’s DHCP server only issues the primary DNS server to clients.  In order to avoid the possibility of a client’s OS using some other secondary DNS and thereby defeating the purpose of using a DNS sinkhole, set Pi-hole DHCP server’s option 6 (DNS Server [[7](#references)]) to also issue a secondary [[4](#references)].  Create `/etc/dnsmasq.d/99-second-DNS.conf` with

``` config
dhcp-option=option:dns-server,192.168.1.10,192.168.1.11
```

and `sudo pihole restartdns`; verify what the DHCP server issues to clients with `pihole-FTL dhcp-discover`.

## Secondary DNS Server Choices

1. Another DNS server (2 RPis each acting as other’s secondary; costly)
2. Use router as secondary DNS
  - Make sure router’s primary DNS is set to Pi-hole
3. Add another IP to RPi’s physical network adapter [[8](#references)]
  - Edit `/etc/network/interfaces`

``` config
auto eth0:0

# leave `iface eth0` stanza as-is

iface eth0:0 inet static
    address 192.168.1.11
    netmask 255.255.255.0
    gateway 192.168.1.1
```

!!! Warning: Don’t set `auto eth0`
    `eth0` interface is manually defined as static; making it `auto` leads to the [bug of `eth0` never becoming up][eth0-bug] if modem and RPi power-up together; needs rebooting RPi after modem is booted completely to fix.

With Pi-hole set to _Listen only on interface eth0_ (web interface -> _Settings_ -> _DNS_) it listens on all IP alias interface labels [[9](#references)].  Verify the server with `ss -tulpn | grep ':53'`; `pihole-FTL` should be listening to port `53` of all IP addresses.

To verify on a client:

1. Check DNS server: `resolvectl`
2. `nslookup flurry.com 192.168.1.11` ([[10](#references)])
  - Return `0.0.0.0`; similar to querying primary (`192.168.1.10`); unlike non-sinkhole DNS e.g. CloudFare

[eth0-bug]: https://dietpi.com/forum/t/wired-ethernet-on-connecting-at-power-up-on-raspberry-pi-3/5071/24?u=legends2k

# IPv6

They’re 128-bit addresses hoping to replace 32-bit IPv4 addresses, more broadly [NAT][], so that every node gets a unique IP to communicate directly without hierarchies.  They’re eight groups of four hex digits [[16](#references)].  The `/` following an address is [CIDR][] similar to IPv4: it specifies the bit-length of an address’ _network prefix_, remaining bits are available for the _interface identifier_ e.g. `128.0.0.1/24` mean `128.0.0` is the network prefix while the last octet is available for interfaces.

[NAT]: https://en.wikipedia.org/wiki/Network_address_translation
[CIDR]: https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing

## SLAAC

IPv6 address are not assigned like IPv4; they’re auto-configured.  Every client comes up with a unique link-local address (LL); with LL issues a router solicitation (RS) to which the router responds with a router advertisement (RA).  Client, using the address prefix, comes up with a global unicast address (GUA).  This is called Stateless Address Autoconfiguration (SLAAC).

A DHCP server is unneeded for IPv6 strictly speaking since SLAAC is part of the standard.  However adoption is slow; not all support SLAAC.  Some routers don’t allow to override DNS server broadcast; in such cases it’s better to leave IPv6 completely OFF at the router level[[11](#references)]

## Steps

1. DietPi: Enable _IPv6_ under `dietpi-config` -> _Network Options: Adapters_
2. Pi-hole: Check _Enable IPv6 support (SLAAC + RA)_ in WebUI -> _Settings_ -> _DHCP_
3. Router: Turn off _DHCPv6 Server_ and _Router Advertisements (RA) Service_ [[12](#references)]

!!! Tip: Static IPv6
    Using SLAAC, identifying the network prefix and deriving the rest from device’s own MAC is like static.  It won’t change as long as it’s in the same network (ISPs can change the prefix through the router but we’ve disabled router’s RA; see above).  If it has to absolutely be made static, it can be done [[14](#references)].  In general IPv6 addresses are not static [[15](#references)].

!!! IPv6 Secondary DNS
    This doesn’t seem to be a problem since Pi-hole DHCP offers 3 (2 IPv4 + 1 IPv6) DNS servers.

!!! Note
    Contrary to many guides [[13](#references)] Pi-hole’s (5.11) `/etc/pihole/setupVars.conf` doesn’t have `IPV4_ADDRESS` or `IPV6_ADDRESS` specified.  Likewise `/etc/pihole/pihole-FTL.conf` doesn’t have `AAAA_QUERY_ANALYSIS=yes` entry.  Leaving them to unspecified works; test with `dig -t AAAA`.

``` shell
dig -t AAAA ipv6.google.com
dig -t AAAA flurry.com @::
dig -t AAAA flurry.com
dig -t A flurry.com
```

`ping6` command can also be used to test connectivity via IPv6.

# References

1. [Pi-hole: How do I show hostnames instead of IPs](https://discourse.pi-hole.net/t/how-do-i-show-hostnames-instead-of-ip-addresses-in-the-dashboard/3530)
2. [Pi-hole: Device name instead of IPs](https://discourse.pi-hole.net/t/names-custom-or-device-name-instead-of-ips/3459/3)
3. [DietPi: doesn’t use `dhcpcd`](https://github.com/MichaIng/DietPi/issues/2731#issuecomment-486017859)
4. [Pi-hole: Secondary DNS Server for DHCP](https://discourse.pi-hole.net/t/secondary-dns-server-for-dhcp/1874)
5. [What happens when specifying multiple DNS servers with DHCP Option 6?](https://serverfault.com/q/1017503/139619)
6. [When is a secondary DNS server used?](https://stackoverflow.com/q/10207640/183120)
7. [DHCP Options](https://www.iana.org/assignments/bootp-dhcp-parameters/bootp-dhcp-parameters.xhtml)
8. [Binding multiple IPs to one interface in Debian](https://www.racksrv.com/knowledgebase/view/19/binding-multiple-ips-to-one-interface-in-debianorubuntu)
9. [Pi-hole: Interfaces](https://docs.pi-hole.net/ftldns/interfaces/)
10. [Effective ways to test Pi-hole](https://discourse.pi-hole.net/t/effective-ways-to-test-pihole/36748)
11. [IPv6 SLAAC 101](https://discourse.pi-hole.net/t/how-to-set-up-ipv6-for-pihole-correctly/42901/8)
12. [IPv6 RA: DHCPv6 vs SLAAC](https://docs.netgate.com/pfsense/en/latest/services/dhcp/ipv6-ra.html)
13. [How to get Pi-Hole to work with IPv6?](https://unix.stackexchange.com/questions/449412/how-to-get-pi-hole-to-work-with-ipv6)
14. [DietPi Forum - DHCPv6](https://dietpi.com/forum/t/dhcpv6/3885/3)
15. [Are all IPv6 addresses static?](https://www.quora.com/Are-all-IPv6-addresses-static?share=1)
16. [Breaking down an IPv6 address](https://www.techrepublic.com/article/breaking-down-an-ipv6-address-what-it-all-means/)


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none' };</script>
