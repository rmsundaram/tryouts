<meta charset="utf-8">

    **PostgreSQL Setup**

# Setup

1. Install PostgreSQL: `sudo pacman -S --needed postgresql`
  - Status won't be running; it needs a cluster: `systemctl status postgresql.service`
  - Starting will fail since there's no cluster yet: `sudo systemctl start postgresql.service`
  - Checking errors tell about cluster missing: `journalctl -xeu postgresql.service`

```
Mar 12 13:01:52 legion postgres[13049]: "/var/lib/postgres/data" is missing or empty. Use a command like
Mar 12 13:01:52 legion postgres[13049]:   su - postgres -c "initdb --locale en_US.UTF-8 -D '/var/lib/postgres/data'"
Mar 12 13:01:52 legion postgres[13049]: with relevant options, to initialize the database cluster.
Mar 12 13:01:52 legion systemd[1]: postgresql.service: Control process exited, code=exited, status=1/FAILURE
```

2. Log in as user `postgres`: `sudo -iu postgres`
  - Confirm if user is present: `awk -F: '{print $1}' /etc/passwd | grep postgres`
  - `chown postgres:postgres /var/lib/postgres`; if `root` is the owner, `.psql_history*` files can’t be created
3. Create a cluster: `initdb --locale en_IN.UTF-8 -D '/var/lib/postgres/data'`
  - Enable checksums if needed: `--data-checksums`
4. Log out: `exit`
5. Start postgresql service: `sudo systemctl start postgresql.service`
6. Log in as user `postgresql`
  - Confirm if the server is accepting connections: `pg_isready`
    + If service start failed we’d get `no response`
7. Create a non-admin user: `createuser --interactive`
8. Create a database: `createdb mydb`
  - Create a schema (like namespace) as needed; default `public` works fine too
9. Connect to DB and use: `psql -d mydb`

At `psql` logout you might get:

> could not save history to file "/var/lib/postgres/.psql_history": No such file or directory

This is due to `postgres` user having a home directory of `/var/lib/postgres`.  Remedy this by creating a
`.psql_history` file here and make user `postgres` its owner.

``` shell
sudo touch /var/lib/postgres/.psql_history
sudo chown postgres /var/lib/postgres/.psql_history
sudo chgrp postgres /var/lib/postgres/.psql_history
```

# Open LAN connections

1. Locate `postgresql.conf`: `SHOW config_file;` in `psql` prompt
  - Your distro may expect separate override e.g. DietPi’s `/etc/postgresql/13/main/conf.d/99local.conf`
2. Set `listen_addresses='*'` (usually it’s `localhost`)
3. Open `pg_hba.conf`
  - Mostly in the same directory or try `SHOW data_directory;`
4. Add an entry for entire LAN IP range like

```
# TYPE    DATABSAE     USER    ADDRESS              METHOD
  host    testdb       all     192.168.1.0/24       md5
```

5. Restart service: `systemctl reload postgresql`
  - Alternatively `SELECT pg_reload-conf();`
  - With DietPi only machine restart worked

# Useful commands

* List databases: `\l`
* Connect to database: `\c DBNAME`
  - Create if missing: `CREATE DATABASE my_db;`
* List users: `\du`
* Create user with login: `CREATE ROLE 'portal' WITH LOGIN PASSWORD 'abracadabra';`
  - Drop `PASSWORD ...` to create without password
* List tables: `\dt`
  - List all tables: `\dt *.*`
* Show stored function/procedure: `\df+`
  - List only trigger/normal functions: `\dft`/`\dfn`
* Describe table schema: `\d TABLENAME`
* Run script: `\i SCRIPT`
  - If permission denied, it might be the script path is unreachable to user `postgres`: copy to `/tmp`
* Run script from Bash: `psql -d DBNAME -f SCRIPT`
* Show help: `\?`
* Show number of active connections: `SELECT sum(numbackends) FROM pg_stat_database;`
* Backup DB: `pg_dump MY_DB_NAME > output-file.sql`
  - Use `pg_restore` to restore dump
  - You can backup/restore `--schema-only`
  - Use `-Fc` for custom (compressed, non-encrypted) format for greater flexibility with `pg_restore`
  - Convert custom format to plain SQL: `pg_restore -f out.sql bin.dump`
* Drop database: `dropdb DATABASE`

# Notes

* Store date, time or timestamp using `timestamp{,tz}`; [avoid Unix epoch][postgres-time]
* Use `NOT NULL` for foreign key columns too (with `REFERENCES`) as they’re orthogonal concepts
* For collation mismatch warning due to PostgreSQL update, refer [Collation version mismatch][bump-collation]
  - A [collation][postgres-collations] is the set of rules that describe how strings are compared and ordered (unicode-aware).  Postgres doesn’t have its own collation; it uses OS’ C library by default.
* Use `CONSTRAINT` triggers `DEFERRABLE INITIALLY DEFERRED` that run at transaction end `AFTER` operations like
  `INSERT`, etc. to maintain data integrity requiring more than per-row constraints
* Be aware: [`PUBLIC` gets `EXECUTE` permission on all functions by default][public-exec]; revoke if needed in a single transaction

[postgres-time]: https://stackoverflow.com/q/21499504#comment135410790_21499988
[bump-collation]: https://dba.stackexchange.com/a/330184/259438
[postgres-collations]: https://www.cybertec-postgresql.com/en/icu-collations-against-postgresql-data-corruption/
[public-exec]: https://www.postgresql.org/docs/15/sql-createfunction.html#SQL-CREATEFUNCTION-SECURITY

# SQL Command Examples

``` sql
-- Add constraint to a column; for CONSTRAINTNAME check:
-- https://stackoverflow.com/q/4107915/183120
ALTER TABLE raw_materials ADD CONSTRAINT CONSTRAINTNAME UNIQUE (name);

-- Show shortest data of a column; use DESC for longest.
SELECT name, LENGTH(name) FROM raw_materials ORDER BY LENGTH(name) LIMIT 5;

-- Add non-null constraint to a column.  Empty string is not null!  Set a length check.
ALTER TABLE raw_materials ALTER COLUMN mycolumn SET NOT NULL;
ALTER TABLE boughtout ADD CONSTRAINT boughtout_name_check CHECK (char_length(name)>=5);

-- Drop non-null constraint to column
ALTER TABLE raw_materials ALTER COLUMN mycolumn DROP NOT NULL;

-- Add new column
ALTER TABLE boughtout ADD COLUMN mynewcol bigint;

-- Change column type
ALTER TABLE units ALTER COLUMN name VARCHAR(32);

-- Set default for a column; use DROP to remove default
ALTER TABLE boughtout ALTER COLUMN mynewcol SET DEFAULT 0;

-- Set column based on expression
UPDATE boughtout SET mynewcol = (oldcol * 100);

-- Update using another table; ensure join produces max 1 row to avoid indeterminacy:
-- refer _Notes_ in UPDATE's documentation
UPDATE items AS t0 SET unit=COALESCE(t1.unit, 'NO') FROM consumables t1 WHERE id=t1.code AND t0.subtype='c';

-- Delete column
ALTER TABLE boughtout DROP COLUMN oldcol;

-- Rename column
ALTER TABLE boughtout RENAME COLUMN mynewcol TO col;

-- Rename table
ALTER TABLE old RENAME TO new;

-- Clear table
TRUNCATE TABLE mytable;

-- Copy table; doesn’t copy constraints and checks.
CREATE TABLE boughtout_bak AS (SELECT * FROM boughtout);

-- Copy columns from another table
INSERT INTO norms_wip (wip, raw, norms) SELECT code, raw_material, norms FROM wips;

-- Update a '(,)' tstzrange to '(,now())'
UPDATE norms_wip SET period = tstzrange(NULL, now(), '(]') WHERE wip = 'KNC' AND raw = 'BRM8';

-- Insert a '(now(), )' tstzrange; last optional param defaults to `[)`
-- https://stackoverflow.com/q/12477957/183120
INSERT INTO norms_wip (wip, raw, period, norms) VALUES ('KNC', 'BRM8', tstzrange('2023-04-19 10:50:33.071587+05:30', NULL, '()'), 1.111);

-- Select by checking timestamptz containment in a tstzrange; use timestamp from column or function
SELECT norms FROM norms_wip WHERE wip='KNC' AND raw='BRM8' AND period @> now();
-- cast string literals if manually specifying time
SELECT norms FROM norms_wip WHERE wip='KNC' AND raw='BRM8' AND period @> '2022-01-01'::timestamptz;

-- Select duplicate rows based on a column
SELECT COUNT(*), code FROM prodmast GROUP BY code HAVING COUNT(*) > 1;

-- Aggregate functions are disallowed in the WHERE clause, use HAVING.  Say a table has
-- items and rates, this returns 1 queried item has just one row and it's zero.
SELECT 1 FROM items WHERE id=? HAVING count(rate)=1 AND sum(rate)=0;

-- Select rows from a table and aggregate of related rows from another; try it
-- without GROUP BY and COUNT to see what gets condensed.
SELECT t1.id, t1.fy, t1.supplier, COUNT(t2.item) FROM purchase_orders t1
JOIN map_purchase_order_item t2 ON t1.id = t2.po AND t1.fy = t2.fy
WHERE status = 0 GROUP BY t1.id, t1.fy;

-- Select t1 ∩ t2 (intersection); INNER is optional.
SELECT t1.id, t2.name FROM items t1 INNER JOIN semis t2 ON t1.id=t2.code;

-- Select t1 🛆 t2 (symmetric difference i.e. (t1 ∪ t2) \ (t1 ∩ t2)).
-- Dropping WHERE clause yeilds (t1 ∪ t2).  OUTER is optional.
SELECT t1.key, t2.key FROM t1 FULL OUTER JOIN t2 ON t1.key = t2.key WHERE t1.key IS NULL OR t2.key IS NULL;

-- Select t1 \ t2 (relative complement of t2 in t1 i.e. t1 not in t2).
-- Dropping WHERE clause yeilds all of t1.  LEFT OUTER JOIN means the same.
SELECT * FROM t1 LEFT JOIN t2 ON t1.key = t2.key WHERE t2.key IS NULL;

-- Number results
SELECT row_number() OVER (ORDER BY some_col), * FROM some_table;

-- WHERE clause on function result; LATERAL is optional for function exprs.  When a set-returning subquery/fn is used,
-- we get an inner joined row per function row; use LEFT JOIN LATERAL to get rows even when function set is empty.
SELECT id, name, stock FROM items, LATERAL get_stock(id) stock WHERE stock > 0;

-- Delete rows from t1 based on a column matching t2
DELETE FROM t1 USING t2 WHERE t1.id = t2.id;

-- Delete table
DROP TABLE mytable;

-- Rename constraint
ALTER TABLE boughtout RENAME CONSTRAINT bought_out_pkey TO boughtout_pkey;

-- Change table owner
ALTER TABLE raw_materials OWNER TO postgres;

-- Grant all but TRUNCATE permission to a role (user).
GRANT SELECT, INSERT, UPDATE, DELETE, REFERENCES, TRIGGER ON raw_materials, boughtout TO rms;

-- Current time zone set
SHOW timezone;

SELECT routine_schema, routine_name FROM information_schema.routines WHERE routine_type='FUNCTION';
SELECT grantor, table_name, privilege_type, is_grantable FROM information_schema.role_table_grants WHERE grantee='merp';
SELECT grantor, routine_name, privilege_type, is_grantable FROM information_schema.role_routine_grants WHERE grantee='merp';
SELECT table_name, is_updatable FROM information_schema.views;

-- Dump query output to CSV with colon delimiter.
COPY (your query) To '/tmp/t.csv' With CSV DELIMITER ':' HEADER;
```

# Procedural Language Functions

Prefer views > plain SQL functions > PL/pgSQL; [reasons][func-choice].

[Plain `SQL` functions][plain-sql-func] can’t use transactional commands like `BEGIN`, `COMMIT`, etc. but if one of the commands in the body fails the entire lot is rolled back i.e. the function is atomic in that sense.

PostgreSQL 14 PL/pgSQL supports `BEGIN ATOMIC` which might be preferable over `$$ BEGIN END; $$` when defining functions.  [`BEGIN ATOMIC` has dependency tracking][begin-atomic].

[plain-sql-func]: https://www.postgresql.org/docs/current/xfunc-sql.html
[func-choice]: https://dba.stackexchange.com/a/23875/259438
[begin-atomic]: https://jkatz05.com/post/postgres/postgres-begin-atomic/

# References

1. [PostgreSQL - ArchWiki](https://wiki.archlinux.org/title/PostgreSQL)
2. [Access PostgreSQL server from LAN](https://stackoverflow.com/questions/22080307/access-postgresql-server-from-lan)
3. [How to Configure PostgreSQL to Allow Remote Connections](https://tecadmin.net/postgresql-allow-remote-connections/)
4. [DietPi - PostgreSQL](https://dietpi.com/docs/software/databases/#postgresql)
5. [What’s the difference between a user and role?](https://stackoverflow.com/q/27709456/183120)

# See Also

1. [Implementing State Machines in PostgreSQL](https://felixge.de/2017/07/27/implementing-state-machines-in-postgresql/): Uses triggers to implement a FSM


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js" charset="utf-8"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js" charset="utf-8"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none' };</script>
