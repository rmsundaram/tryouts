<meta charset="utf-8">

        **Chrome University Learnings**

# Anatomy of a Browser

* Multi-process arch introduced by Chromium
* The 4 S
  - Simplicity
  - Speed: misbehaving tab (process) doesn’t impact other tabs (processes)
  - Stability: tab crash doesn’t kill entire browser
  - Security: untrusted web content shouldn’t access file system
* IPC primarily by message processing; shared memory for large data
  - Heavily discouraged synchronized communication
  - Process crossing IPC layer has to go through a security review
  - Most processes have main thread and an IPC thread, plus workers to offload work
  - IO thread is a misnomer; actually _non-blocking IO_ thread e.g. networking, IPC, etc.
  - Mojo: IDL-based
    + message pipes (cheap two-way channel), shared buffers and data pipes
    + process/thread-agnostic (no different code paths due to sender/receiver location)
* Sandboxing for security since renderer no longer renders static data but webapps
  - Run untrusted web content in locked-down process with no access to file system
  - Rendered is the most locked-down
  - Site isolation: multi-process even within a tab; process per document origin
* Plugin Process
* Browser process: central coordinator
  - Draws UI around the content
  - Handles networking and storage
  - User data (profiles, settings, etc.)

Refer [Anatomy of a Browser 101](https://www.youtube.com/watch?v=PzzNuCk-e0Y&list=PLNYkxOF6rcICgS7eFJrGDhMBwWtdTgzpx&index=2).

# Structure

* `//chrome` (application) embeds `//content` (library)
  - AndroidWebView, Electron, etc. are other embedders
* Browser process is the parent of all other processes
  - Renderer
  - GPU
  - Utility
* Objects of Browser Process
  - `BrowserProcess`
  - `content::BrowserMainLoop`
  - `content::BrowserMainParts` (extended by Chrome’s `ChromeBrowserMainParts`)
* Avoiding globals makes testing easier
  - Need global-like objects? Attach it to `ChromeBrowserMainParts` or a top-level browser objects
* `BrowserContext` embodies the user profile data
  - `Profile` is for `//chrome`, `BrowserContext` is for `//content`
* Each window is a `Browser` object (not the entire Browser process)
* Every feature shouldn’t put its data inside `BrowserContext`
  - Instead make the data adhere to `base::SupportsUserData::Data`
  - `BrowserContext` has a `base::SupportsUserData` key-value store
  - Store and retrieve with `Set/GetUserData`
* `KeyedService` is similar to `SupportsUserData` but has two-phase shutdown
  - Notification issued before kill

Refer [Anatomy of a Browser 201](https://www.youtube.com/watch?v=u7berRU9Qys&list=PLNYkxOF6rcICgS7eFJrGDhMBwWtdTgzpx&index=3).

## WebContents

`WebContents` (class in `//content`) embodies HTML page data:

* Tabs are made of `WebContents`
* Not all `WebContents` are tabs; they can be showed in say pop-ups, extensions, etc.
* `SupportsUserData` in the name of tab helpers
* Supports `WebContentsObserver`
* Responsible for drawing contents but doesn’t know about higher-order function
* Delegates/Clients are the embedders of `//content`
* Different chromes implement `WebContentsDelegate` differently
* Higher-order functions like, say, showing a file-picker is done by `WebContentsDelegate`
* Every `WebContents` has an associated `NavigationController`
* `NavigationController` maintains session history i.e. per-web-contents history
  - `NavigationEntry` is a stop in back/forward list
* Every document loaded for a `WebContents` will have a `RenderFrameHost`
  - `iframe`s don’t get their own `WebContents` (or `NavigationController`)
  - `iframe`s loading documents with different origin get their own `RenderFrameHost`

> Document runs in a frame; it can have sub-frames that run other documents

## `base`

This is a very approachable equivalent of standard library that can be used independent of Chromium code.
It is an excellent C++ reference material too.  It has many interesting abstractions:

* Allocators
* Memory wrappers
* Seralizer-Deserializer for formats like JSON
* Numerics
* Traits
* Logging
* Containers
  + Flat Map/Set: sorted vector + binary search
  + Fixed Flat Map/Set: sorted array on stack + binary search
  + Circular Deqeue
  + Intrusive Heap
  + LRU Cache
  + Queue
  + Ring Buffer
  + Small Map: unsorted array-backed `std::map`-like interface with fallback on overgrowth
* Shared/Dynamic library loading
* Type Conversions
* Threading, tasks and processes
* Endian
* Processor and machine specs
* Callbacks, closures and bind
* `Singleton`, `NoDestructor`
* `StringTokenizer`
* `FilePath`
* `Timer`
* `CommandLine`
* `SupportsUserData`
* `RunLoop`
* `Location`
* `Observer`
* `AtExitManager`

Hash tables are not recommended in general over flat maps as it’s slower for
Chromium codebase’s common case of 4-entry set/map.

# Navigation

BeginNavigation -> Redirects -> FindRenderer -> FrameCommitted -> LoadStop

* `WebContentsObserver` has a set of callbacks to monitor various navigation stages
  - `DidStartNavigation`
  - `DidRedirectNavigation`
  - `ReadyToCommitNavigation`
  - `DidFinishNavigation` - ultimate end of navigation irrespective of success
* `NavigationHandle`: read-only object exposing navigation properties until commit (UI thread)
* `NavigationUrlLoader` manages network request (network thread)
* `NavigationThrottle` used to interpose on navigation (UI thread)
  - A throttler usually has a static method that returns an instance or `nullptr` depending on the `NavigationHandle`
  - Embedder throttlers are registered at `chrome/browser/chrome_content_browser_client.cc`
  - Per-frame throttlers are registered at `content/browser/frame_host/navigation_throttle_runner.h`

Refer [Life of a Navigation (2018)](https://www.youtube.com/watch?v=mX7jQsGCF6E).

# Rendering

* Browser’s `RenderFrameHost` is counterpart of Renderer’s `RenderFrame`
  - Suffix `Host` -- should ideally be `Service` -- means its the owning-process side of IPC
  - For instance `RenderFrameHost` and `RenderFrame` have a shared fate (across IPC)
  - When a `RenderFrame` dies `RenderFrameHost` dies _almost_ at the same time
* `//content` embeds Blink (rendering engine)
  - `//content`’s renderer process owns that in a `RenderFrame`
  - `RenderFrame` has `blink::WebLocalFrame` wrapping `blink::LocalFrame`
  - Every page with a unique origin will have its own `RenderProcess`
* `//content` manages all these render processes and figures out where to draw content
* `FrameTree` where every tab has one `MainFrame` having nodes with additional `RenderFrames`
  - Frames are a content of the tab (Browser), not process
  - Render processes just cares about which rect to draw into
  - Compositor decides where to put it
  - Browser cares about the hierarchy
  - Not all `RenderProcess` have an associated `MainFrame` e.g. `iframe`s with different origin

# Mojo IPC

* Similar to [protobuf][]; RPC serializing but light-weight, faster than XML
  - Define interfaces (messages) and data structures in IDL
  - `protoc some_idl.proto` spits C++ (or other) code for sender/receiver
* Primary types
  - `Remote` (remote implementation of an interface) sends messages to `Receiver`
  - `Receiver` receives and services the request (through some implementation of interface e.g. `class BatteryMonitorImpl: public mojom::BatteryMonitor` houses `mojom::Receiver<mojom::BatteryMonitor>`)
  - Message pipes is the primitive enabling this
* Example
  - Renderer has remote object `mojom::Remote<BatteryMonitor> m;`
  - Renderer invokes interface method `m.query_status([](){})`
* Browser process brokers IPC connections since it’s the most-privileged

Refer [Mojo - Chrome’s IPC system](https://www.youtube.com/watch?v=o-nR7enXzII).

[protobuf]: https://en.wikipedia.org/wiki/Protocol_Buffers

## Protocol Buffers (`protobuf`)

- Google’s serialization format lighter than JSON and XML; MojoM is a fork
- Forward- and backward-compatible but not self-describing _binary wire format_
- Define an IDL (`.proto`) and `protoc` compiles it into `.h` and `.cc` for both sender and receiver
- Basis for [gRPC][]: custom RPC system used in nearly all inter-machine communication at Google
  + gRPC is a concrete RPC protocol stack to define services

[gRPC]: https://www.grpc.io/docs/what-is-grpc/
[protobuf]: https://developers.google.com/protocol-buffers/docs/overview

## Flat Buffers

- Efficient (faster than JSON, CSV, XML) cross-platform, pure C++ serialization library authored by Wouter, open-sourced by Google
- Used primarily in performance-critical apps like games
- Supports zero-copy de-serialization i.e. accessing serialized data requires no copying into a separate part of memory
  + Requires 0 additional memory allocations
  + Suitable to use with `mmap` (or streaming), requiring only part of the buffer to be in memory
  + Access close to speed of raw `struct` access with only one extra indirection
- Tiny footprint, strongly typed (no runtime checks needed) and flexibility in designing data structures
  + Optional fields, forward-/backward-compatible: long-lived games don’t have to update all data with each new version
- Faster than `protobuf` as it doesn’t need parsing/unpacking to a secondary representation before accessing data
  + Code is an order of magnitude smaller
- Schema compiler `flatc`
- Schema-less (self-describing) version available too
- [Usable with gRPC][flatbuffers_grpc], like `protobuf`, to create services

[flatbuffers]: https://google.github.io/flatbuffers/
[flatbuffers_grpc]: https://google.github.io/flatbuffers/flatbuffers_grpc_guide_use_cpp.html

# Life of a Process

* Startup: from launch to first non-empty paint of a tab
  - Part 1: sync init
  - Part 2: run main thread loop (which get tasks from queue and run)
* Steady
* Shutdown

!!! Tip
    Read comments in `//content/public/browser/browser_main_parts.h` explaining various stages of `BrowserMain()`.
    Each stage is represented by a `BrowserMainParts` method called from corresponding method in `BrowserMainLoop`.

## Start-up

* `//content` houses the multi-process architecture; drives init (`content::BrowserMainLoop`)
  - Brings up `{content,chrome}::BrowserMainParts`
  - `chrome::BrowserMainParts` brings up `ChromeBrowserMainExtraParts`
* Integrate your component at startup by implementing an interface (hook) at the right startup stage
* Init `base::CommandLine` - used by many threads
* Create task executor and thread pool
  - Allows code to post (but not run) task using `base::PostTask` and `base::ThreadTaskRunnerHandle`
* `PostEarlyInitialization`
  - Read non-profile-specific state from disk
  - Setup locale, field trails and experiments (`base::Feature`)
* `RunServiceManager`
  - Start `ThreadPool`: adds threads
  - Can run tasks on other threads now
  - Potential of data races start here
  - Start ~~IO~~ message loop thread
* `PreCreateThreads`
  - Pause thread pool (cannot init while other threads run tasks)
  - `PreCreateThreads` performs init (needing inter-thread shared state write)
  - Unpause pool
  - Start accepting BrowserThread::IO tasks
* `PostCreateThreads`
  - Initialize stuff initializable while other threads are running
* `PreMainMessageLoopRun`
  - Longest initialization function
  - Create user profile and keyed services
  - Load all extensions (since they can alter navigation, they need early init)
  - Figure out which tabs to open in the first browser window and navigate (non-trivial work done by `StartupBrowserCreator`)
  - Disallow slow calls on main thread

## Steady

Following Browser threads exist during steady state operation:

* UI thread
  - Run infinite loop alternating between
    + Fetch task from its queue and run task
    + Get messages from OS (e.g. input event)
  - These _should_ happen on `CrBrowserMain` (Browser’s UI thread) since there’re no locks protecting their states; so avoid data races and access these from the same (UI) thread
    + UI modifications
    + Accessing to profile
    + Starting navigation
    + Calling `WebContents` methods
* IO Thread (misnomer)
  - Receive IPC messages; sometimes send
  - Does very important work but fairly transparently so many are unaware of internals
  - Runs infinite loop alternating between
    + Fetch task from its queue and run task
    + Read IPCs from message pipe
* Compositer thread
* Thread-pool threads
  - General purpose threads used for tasks unassociated to above threads

Both UI and IO threads have unresponsive tasks disabled since they mean janks in user experience.  Say a 100ms task run on UI or IO thread then IO thread won’t handle IPC messages from other processes, while UI won’t process user’s input events until task is complete.

Synchronization (primitives `WaitableEvent`, condition variable, etc.) is disallowed outside of low-level constructs (`//base`); they can cause priority inversion or dead locks.  Likewise, expensive computation, disk accesses, etc. are prohibited too.

## Performing costly tasks

`base::PostTaskAndReplyWithResult` will offload work from UI to worker thread.  It takes functors for thread and callback.  A task is posted to the thread pool with functor.  Once done, its return value is forwarded to caller thread by calling callback function.

The same (non-thread-safe) task can be posted multiple times to the thread pool.  Since locking is prohibited, there’ll be memory writes with no synchronization.  However, a data race is avoided since all tasks posted on the same _virtual thread_ are guaranteed to run sequentially, one at a time.

A virtual thread (`base::SequencedTaskRunner`) uses different physical threads to run its tasks sequentially in the order received.

* `CreateSequencedTaskRunner` returns `task_runner`
* `PostTaskAndReplyWithResult` takes a task runner argument along with the thread start and callback function bindings
* Two tasks posted to the same task runner is run on the same virtual thread; no concurrency, no data races

### Thread Pools and Task Runners

* Thread pool is a set of physical threads with a shared task queue; exactly one instance per-process.  `TaskRunner` is an interface to post tasks to a queue.  Different kinds exist i.e. `SequencedTaskRunner`, `SingleThreadTaskRunner`, etc.
* `task_runner->PostTaskAndReplyWithResult` is used to run task without pool choice
* `base::ThreadPool::PostTaskAndReplyWithResult` can be used to post task with pool choice via `TaskTraits`
* `base::PostTaskAndReplyWithResult` allows pool choice or a specific thread too e.g. `BrowserThread::UI`

### Class-level Thread Safety

* Classes needing sequential execution, instead of having a lock, have `SEQUENCE_CHECKER(sequence_checker_)` ensuring tasks accessing their state are posted to the same `base::SequencedTaskRunner`
* Every method’s prologue has `DCHECK_CALLED_ON_VALID_SEQUENCE(sequence_checker_)`
* Use `base::WeakPtr` and `base::Bind*` when specifying callback
  - Latter cancels calling callback if object destroyed when task reply is received
  - Auto-cancelling, non-capturing, move-only lambdas and `Unretained` annotation for raw pointers are why `base::Bind` is preferred over `std::bind`

**Note**: All weak pointers issued by same factory, _must be_ dereferenced and invalidated on the same virtual thread to prevent races; allowing concurrent invalidation with dereferencing entails data races, leading to use-after-free

Refer [Life of a Process (2019)](https://www.youtube.com/watch?v=5im7SGmJxnA).

## Shutdown

* Exit main thread loop; tasks posted to browser and inputs ignored
* Main runner `Shutdown` joining IO thread; no IPC with child processes
* Thread pool shutdown
* Synchronous teardown (OK to delete stuff on main thread except contention with tasks having `CONTINUE_ON_SHUTDOWN`)
* `BLOCK_SHUTDOWN` task shutdown behaviour is good for persisting state to disk as they’re guaranteed to run (even after thread pool shutdown starts)
* Usually state is saved as Chromium is running to avoid (user) data loss on a crash
  - Clean shutdown isn’t mandatory
  - Shutdown usually performs tasks to speed up the next startup
  - Free up few objects (not very necessary)

## Test

Unit tests needing time control needs `base::test::TaskEnvironment`.

# Life of a Pixel

* `content::WebContents` is primary interface exposed to embedder (of `//content`)
* Content area is represented by `WebContents`
  - Encapsulates creating and managing renderer processes
* Blink encapsulates the semantics of the web platform
  - Only a subset of rendering process underneath `//content` layer
* Rendering is essentially converting document (HTML / JS / CSS) in to right OpenGL calls
* Renderer’s pipeline builds right intermediate data structures of document such that it
  - Facilitates efficient updates (animation)
  - Answers queries about document from scripts

## Stages of pipeline

* Parse: convert HTML from network into DOM (_object model_ reflects containment relationships)
  - DOM is both internal representation of document and API exposed to JS
  - APIs like `createElement`, `appendChild`, etc. are thin wrappers around the C++ DOM tree through _binding_
  - A document can have multiple DOM trees as HTML supports custom elements leading to shadow roots and slots
  - Flat tree traversal is appropriate to traverse DOM with custom elements
* Style
  - Parse CSS into its object model
  - Digested object model has selectors and property-value mappings (indexed for efficient lookup)
  - Build time: all CSS properties are put up in `css_properties.json5` -> `make_css_property_subclasses.py` -> `border_left_color.cc`
  - Style rules are selecting complicated, overlapping sets of nodes with precedent sematics; The `StyleResolver` figures out applying them to DOM elements
* Layout
  - Figure out rectangles of elements
  - Simple block layout objects are place one after another downwards; block flow
  - Inline flow for text runs is from left to right for LTR language; otherwise of RTL
  - _Text Shaping_: compute run start/end and line break using _HarfBuzz_; letters -> glyphs -> size + placement accounting for computed font with ligatures and kerning
  - Multiple bounding boxes are computed for a single element e.g. overflow due to element box > border box (possibly `scrollable` needing to compute `scroll_min/max` and reserving space for scroll bars)
  - Layout has its own tree linked to DOM’s.  Each layout pass walks this tree computing the visual geometry of each `LayoutObject`.
* Paint
  - Builds a list of paint operations (paint ops)
  - DIV -> `LayoutObject::Paint()` -> `PaintCanvas::DrawRect`
  - `DisplayItem`s are put in a `PaintArtefact` list
  - Different Z-order (stacking order) is taken care by different paint phases (a separate traversal of a stacking context)
* Rasterization
  - Converts paintops in the paint artefacts (display item list) into pixel data (bitmap)
    + [Display List][] is classic OpenGL’s implementation of _Command Buffer_; [no modern OpenGL equivalent but offered by Vulkan][Command Buffers]
  - Includes image decoding of image sources (PNG, JPEG, etc.)
  - Bitmaps and raster are in GPU memory referenced by a texture
  - Bitmap of pixels generated in some memory (mostly GPU, in some cases CPU); not yet on screen
  - Raster issues OpenGL calls through Skia; `SkCanvas`
* GPU
  - GPU process decodes command buffers, issues real GL calls through its own function pointer set
  - Skia, being in (sandboxed) renderer process, can’t issue GL (system) calls directly
  - At initialization Skia is fed a table of OpenGL function pointers that don’t map to actual OpenGL function addresses but stubs redirecting them to GPU process; parcelling data format: _command buffers_
  - Another reason for GPU process is instability/insecure of GPU drivers that may crash; browser wouldn’t crash and simply restart the GPU process in the background
  - On most platforms GL pointers inside GPU process are real OpenGL function pointers, while on Windows it’s Angle: OpenGL -> D3D convertor library
* Frames
  - Renderer produces _animation frames_
  - Anything less than 60 fps (typical V-Sync internal on modern hardware) makes animation janky/stutter
* Invalidation
  - Each pipeline stage tracks granular asynchronous invalidation
  - Outputs reused from previous frame when possible
  - Only do work on entities marked as _needing layout_
* Repaint
  - Paint + raster remain expensive if a large region is transformed (e.g. scroll)
  - Everything on Renderer’s main thread competes with JavaScript; jank if the script is doing expensive stuff before rendering even starts
* Compositing
  - Layer is a piece of document that can be transformed and rasterized independently of other layers
  - Idea
    + decompose page into layers which can be rasterized independently
    + combine layers in another thread
  - Main thread builds layers and commits, compositor -- a.k.a `impl` thread (legacy) -- draws layers
  - Developer Tools has _Layers_ that’s useful to understand and debug various layers in a document
* Layers
  - Point of layers is they raster independently
  - A simple layer captures a subtree of content
  - Example: instead of computing the rotated coordinates of each entity in the DOM, the layer itself is rotated and rastered
  - All important cases of fluid motion -- scroll (clip), pinch zoom (scale) and animation (translate) -- can be expressed in terms of composited layers
  - Compositor thread has everything to handle input events while main thread is busy with other things like Javascript
  - Browser process gets input from the OS (like touch-drag), forwards to Renderer process where Compositor gets first crack at handling it -- handles scroll, move, etc. without even talking to Renderer’s main thread
  - Mightn’t work in some cases: Compositor forwards event to main thread (slower)
  - Layers are created by promoting layout tree elements with certain style properties (compositing triggers) e.g. PaintLayer is a compositing _candidate_
  - Scroll containers create a set of special layers managed by `CompoistedLayerMapping` maps to main layer, scroll layer, scrolling contents layer, scrollbar container layer, ...
* Compositing Update
  - Separate stage that builds layer tree, happens after layout, before paint on the main thread
  - Each layer is painted separately with its own display item list with all the paint ops produced when layer is painted
* Property Trees
  - When compositing draws a layer, it applies various properties like matrix, clip, scroll offset, opacity, reflection, etc. stored in property trees
  - Decoupled from layers
  - Prepaint builds the property trees; in future may build after paint
* Commit
  - Updating copies of the layers and property trees on the compositor thread to match main thread
  - Commit runs on the compositor thread with main block ensuring thread safety on these data
* Tiling
  - Layers can be big and most of it mightn’t be visible
  - Compositor thread divides layers into tiles
  - Tiles are rastered with a pool of dedicated raster threads
  - Compositor’s Tile Manager creates the tiles and raster tasks on the worker pool prioritized based on the distance from the viewport
* Draw Layers
  - Compositor generates draw quads for tiles; created taking into account transforms, effect, etc. applied by property trees
  - Each quad references tile’s rastered output in memory
  - All quads are wrapped up into `CompositorFrame` which gets submitted to the GPU process
* Activation
  - Raster and Drawing have contention for the layer tree
  - Two copies are maintained; solution: back-buffering
* Display (viz)
  - Multiple renderer process (due to iframes with different origin) would be submitting frames to the GPU process
  - Their outputs have to be stitched together
  - Browser process has its own compositor generating frames for its UI outside content area
  - All these surfaces are submitting frames (`CompositorFrame`) to GPU process’ Viz compositor thread’s `DisplayCompositor`: combines frames for multiple surfaces
  - Viz: GPU process’ compositing service
  - `DisplayCompositor` synchronizes frames and understands dependencies between surfaces
  - Viz issues proxied OpenGL calls to display quads from the compositor frame
  - Viz’s output is double buffered: draws into the back buffer
  - Display/Viz runs on its own thread in the GPU process; GL calls are proxied from the viz thread to the main thread over command buffers similar to raster
  - End result of compositor is pixels are finally on the screen!

### Recap

Renderer Process

* main thread (Blink)
  - DOM -> style -> layout -> compositing update (layers and property trees) -> paint -> commit (layer’s paint ops and property trees) to compositor thread
* impl thread (Compositor)
  - tiling -> raster (Skia worker threads) -> activate (copy pending to active tree) -> draw (generate quads from rastered output) -> submit to Viz

GPU Process

* Viz does compositing of layers
* main thread issues actual OpenGL calls
* OpenGL renders stuff on screen

Most of the pipeline runs in Renderer. GL for raster and display are in GPU process.

Refer [Life of a Pixel](https://www.youtube.com/watch?v=m-J-tbAlFic&list=PLNYkxOF6rcICgS7eFJrGDhMBwWtdTgzpx&index=6).

[Display Lists]: http://www.songho.ca/opengl/gl_displaylist.html
[Command Buffers]: https://computergraphics.stackexchange.com/a/1573/1650

## Ponder

* Do we have non-UI work to be offloaded to thread pool?

<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none', inlineCodeLang: 'c++' };</script>
