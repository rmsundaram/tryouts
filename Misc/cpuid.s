#!/usr/bin/env gcc -o cpuid.exe cpuid.s

#
# REFERENCES
# Professional Assembly Language by Richard Blum
# http://www.chocolatesparalucia.com/2010/09/colinux-int-0x80-on-windows-and-other-rants/
# http://www.chocolatesparalucia.com/2010/09/hello-world-c-and-gnu-as/
#

#cpuid.s Sample program to extract the processor Vendor ID
    .section .data
output:
    .ascii "The processor Vendor ID is 'xxxxxxxxxxxx'."
    .section .text
    .global _main
_main:
    movl $0, %eax
    cpuid
    movl $output, %edi
    movl %ebx, 28(%edi)
    movl %edx, 32(%edi)
    movl %ecx, 36(%edi)
    movl $output, (%esp)
    call _printf
    movl $0, %eax
    leave
    ret

