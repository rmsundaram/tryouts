<meta charset="utf-8">

    **Design Patterns**
    Principles, Idioms and Techniques

This is where I document my learnings when working with different code bases.

# SOLID Principles

1. Single responsibility: Do one thing; do it well.
2. Open/closed: Open for extensions but closed for modification.  Such an entity can allow its behaviour to be extended without modifying its source code.  For example, it should be possible to add fields to the data structures it contains, or new elements to the set of functions it performs.  A module will be said to be closed if it is available for use by other modules.  This assumes that the module has been given a well-defined, stable description (the interface in the sense of information hiding).
3. Liskov's substitution: If walks like a duck, quacks like a duck, it shouldn't need battery to run.  An object should exhibit other behaviour that people come to expect from a duck.  If there're three different kinds of ducks and the client code has a duck interface, then each should be a drop-in substitution for the other and things should work seamlessly after the substitution.
4. Interface segregation: Have granular interfaces instead of monolithic interfaces that force clients to use methods unneeded for them.  This promotes decoupling and they only pay for what they want.
5. Dependency inversion: Conventionally, the high-level (decision-making) layers depend on low-level (utility) layers.  Instead both should depend on abstractions and not concretes.  Details should depend on abstractions and not the other way around.

# Creational Patterns

## Singleton
Only a single object can be instantiated from a class.

## Factory Method
One unknown e.g. `createPizza` will return a pizza depending on the parameter - Onion & Capsicum, Delux or Hawaiian.  This is a specialization of [Template Method](#template-method).  [sourcemaking.com](http://www.sourcemaking.com/) says

> Factory Method is to creating objects as Template Method is to implementing an algorithm. A superclass specifies all standard and generic behavior (using pure virtual "placeholders" for creation steps), and then delegates the creation details to subclasses that are supplied by the client.

## Abstract Factory
Two unknowns, two dimensions of decoupling -- abstract factory & abstract product e.g. `GUIFactory` will produce `Button`; `GUIFactory` can be either `TuxGUIFactory` or `WinGUIFactory` and `Button` can be `GtkButton` or `WinButton`.

## Builder
This is useful when various source formats of the same object (JSON, text, etc.) needs to be converted to build different output objects. Example: This decouples a PDF reader + (TeX/HTML/ASCII) writer.  The reader reads the complex input representation and fill in a data structure which the builder expects.  Once available, it builds one of the output formats.  There's usual a _Director_ which invokes builder services as it interprets the external format.

## Lazy Initialization
Delayed initialization e.g. don't initialize a member in the constructor but wait till its first usage [[1](#references)] or some threshold is reached.  It's really useful during performance improvements, particularly if the object built expects its data from network access or has shoestring initialization budgets.

## Object Pools
Create, initialize and keep some objects ready in a pool; offer one when requested and get back when returned; this takes the same time as far as initialization is considered; reason is, when returned back, earlier set members should be reset and should again be reinit when requested; it's a few advantages like memory defragmentation on numerous news and deletes are saved. Some books advice against this pattern (refer [Wikipedia](https://en.wikipedia.org/wiki/Object_pool_pattern)).


# Structural design patterns

## Flyweight
This is a mechanism to share objects; when a large number of objects are needed, then a distinction of sharable (intrinsic / independent) and non-sharable (extrinsic / dependant) properties is made e.g. in a document, graphical font characters have glyph properties like font outline, metrics and formatting data, etc. which are sharable & independent, but the index/place-of-occurrence is variant. The former props are stored in the object and the client programmer passes the location (variable, extrinsic prop) to draw it in a given line & column.

## Composite
Composition of one or more similar objects exhibiting similar functionality e.g. `Draw()` a button, text box and window (composite).

## Facade
One class that deals with underlying complex process calling several other classes e.g. customer service in real world.

## Adapter
Similar to an adapter plug.  Have a concrete class that can be adapted to an expected interface?  Write an adapter.

## Proxy
Create a level of in-direction so that the real object's degree of accessibility can be controlled via the proxy e.g. a bank cheque is a proxy for hard cash.

## Decorator
Attach additional behaviour to an object without affecting the behaviour of other objects from the same class.  Adapter provides a different interface to its subject. Proxy provides the same (or trimmed-down) interface. Decorator provides an enhanced interface.


# Behavioral design patterns

## Observer
Watch out for events by registering listeners with the doer; the doer will have a vector of such observers who all get notified.

## Visitor
Without changing the structure of a class, add more functionalities to it; this is achieved with the help of double dispatch (resolving 2 unknowns @ runtime - object base class and operation base class e.g. animal <- dog, cat & activity <- run, bark).  Decent [rule of thumb][virtual-vs-visitor] to solve [Expression Problem][] in C++:

> If you have _fixed set of operations, but keep adding types_, use `virtual` functions.
> If you have _fixed set of types, but keep adding operations_, use the _visitor pattern_.

[virtual-vs-visitor]: https://stackoverflow.com/a/9949170/183120
[Expression Problem]: http://en.wikipedia.org/wiki/Expression_problem

## State
A common interface, which will be implemented by different states, will be held as a pointer by the state manager; this will be pointing to the current state (each state is a singleton); manager'll just call `update()`, `paint()`, etc. Transition will be done from one state to another smoothly e.g. `State::Update(Manager *mgr)` will be implemented by every state; so `m_pState->Update(this)`; will internally call `mgr->ChangeState(stateGameOver::GetInstance() /* singleton pattern */);`

## Chain of responsibility
Instead of coupling the generator/sender of an event/message to a handler/receiver, it's given to (head of) a chain it'll handle its part of the message and pushes it to the next handler down the line e.g. ATM machine the currency denomination will be forwarded to each bundle 1000s, 100s, 10s etc.

## Template Method
Implement known functionalities and pure virtualize unknown ones; let the implementor of this abstract class give those implementations.  This becomes like a template i.e. the abstract creates a template on what-to and lets derivatives implement the how-to. This is what abstract classes, virtual functions, etc. do in C++.

## RAII
A technique widely used in C++ which emphasises on initializing resources in the constructor and releasing them in the destructor; when using pointers, wrapping them up with a smart pointer makes them an object too, so making everything an object makes sure that all get cleaned up properly.

## Iterator
Sequentially access the elements of a collection class; this is required since not all data structures have random access (e.g. linked list).

## Strategy
Make a family of algorithms, encapsulate each one, and make them interchangeable.

# References

1. TICPP Volume 1, Chapter 16

# Misc C++ Idioms

Most of them can be seen in the [More C++ Idioms](http://en.wikibooks.org/wiki/More_C%2B%2B_Idioms) Wikibook.

* Template Method
* [Non-virtual Interface (NVI)](http://www.gotw.ca/publications/mill18.htm): public overloaded non-virtuals call protected non-overloaded virtuals -- [C++ FAQs](https://isocpp.org/wiki/faq/strange-inheritance#private-virtuals)
* Tag dispatch -- by instance and by type
* SFINAE
* Delegates -- impossibly fast C++ delegates
* Explicit Template Specialization
* [Curiously Recurring Template Pattern (CRTP)](http://stackoverflow.com/a/262984/183120): static/compile-time polymorphism
* Named Constructor
* Covariant and Contravariant types: example virtual constructor and virtual copy/clone method
* Top-level `const`
* Argument Dependant Lookup (ADL)
* [Named Parameter using method chaining][fluent] / Fluent API / _sometimes_ [Builder][rs-builder]
* Pimpl or cheshire cat or opaque handle/pointer
* Copy-on-Write (COW)
* Object Pool: instead of deleting object, call reset and reuse it
* Memory Pool: to reduce fragmentation when lots of small object are created and destroyed. Boost's object pool is actually a memory pool.
* Type Erasure: refer BoostCon presentation slides
* Copy-Swap Idiom
* [Erase-Remove Idiom](https://stackoverflow.com/a/41155595/183120)
* Safe-bool idiom (obsolete with C++11 by `explicit operator bool() const`)
* Scope Guard

As Bjarne says, do not hide when the interface _is_ the implementation e.g. `struct Point { float x, y; }`

[fluent]: https://legends2k.github.io/cpp-idioms-chrome/#13
[rs-builder]: https://rust-unofficial.github.io/patterns/patterns/builder.html

# Common Patterns in Chromium

## Delegate

A higher-level object can know the existence of, thereby talk, to a lower-level layer.  The reverse, however, isn’t allowed or is bad design.  To counter this, a lower-level layer can declare a `Delegate` interface with methods that’d give the data it needs from the higher-level object and expose a `SetDelegate(Delegate* d)`.  Then for its normal operations needing data from the higher-level layer, it calls the delegate’s methods and gets the data.

> It delegates the responsibility of decision making to its creator/manager, the higher-level object.

The higher-level object implements `Delegate`, sets it to the lower-level object before using it normally.

The advantage of delegate pattern shows when you build the lower-level class code.  The lower-level code shouldn’t have/take a dependency on the higher-level code.  Thanks to the delegate interface that the lower-level code is unaware of the existence of a higher-level code.  So it builds without any dependencies.

## Dependency Injection

Instead of a class create its dependency on its own (thereby coupling construction and user), its injector/creator sets its dependencies during creation/initialization.  Thus a class `User`, instead of taking a dependency on a concrete class `DiskUtility`, just takes a dependency on an interface `Utility` and doesn’t know about how to create it or even what exact utility is needed.  It simply uses its dependencies without knowing how/what is created to support the dependencies.

> Instead of a client specifying which service it will use, something tells the client what service to use.

It’s part of a broader technique: _inversion of control_.  The client delegates the responsibility of getting its dependencies to external code than taking a dependencies.  It needn’t know about the injector code either.

The intent behind dependency inject is to achieve _Separation of Concerns_: construction vs usage of a dependency/service.

## Animator

This is prevalent throughout Cocoa/AppKit’s `NSView`’s where setting the property of an object just sets it, while setting it via the object’s [animator proxy][] animates the property to a destination value instead of setting it in one shot.

The animator of an object though technically a separate one, it should be treated as a part of the object itself.

E.g. `ui::Layer` and `ui::LayerAnimator` similar to `NSView`’s `AnimatorProxy`.

[animator proxy]: https://www.swwritings.com/post/2010-05-23-core-animation-the-animator-proxy

## Arbitrary Key-Value Store

An object with high availability (like `Application`, `Document`, `BrowserContext`) exposes an arbitrary key-value store.

``` c++
class SupportsUserData {
 public:
  SupportsUserData();

  // Derive from this class and add your own data members to associate extra
  // information with this object. Alternatively, add this as a public base
  // class to any class with a virtual destructor.
  class Data {
   public:
    virtual ~Data() = default;

    // Returns a copy of |this|; null if copy is not supported.
    virtual std::unique_ptr<Data> Clone();
  };

  // The user data allows the clients to associate data with this object.
  // |key| must not be null--that value is too vulnerable for collision.
  // NOTE: SetUserData() with an empty unique_ptr behaves the same as
  // RemoveUserData().
  Data* GetUserData(const void* key) const;
  void SetUserData(const void* key, std::unique_ptr<Data> data);
  void RemoveUserData(const void* key);

 private:
  std::map<const void*, std::unique_ptr<Data>> user_data_;
};
```

The exposing class would inherit from [`SupportsUserData`][sud].  An object needing to store some data (consumer) would wrap it in a class inheriting `SupportsUserData::Data` and put it in with `app->SetUserData(kMyDataIdentiferStr, my_data)`.

`SupportsUserData` is a mixin-type class that allows consumer classes to stash data on the class with the mixin (central class inheriting `SupportsUserData`).

[sud]: https://source.chromium.org/chromium/chromium/src/+/master:base/supports_user_data.h

### Associated Data Pattern

Allows a central class `C` to store data that is logically per-instance of `C` but is intelligible only to its consumers.  This is useful when consumers don’t control or know the lifetime of `C`.

Say `Pokemon` has two consumer classes `PokemonGroomer` and `PokemonNurse`.  Each consumer needs to store some data associated to `Pokemon`, so the usual solution would be

``` c++
class Pokemon {
  ...
  Time last_bath_;
  Time last_checkup_;
  ...
};

bool PokemonNurse::NeedsCheckup(Pokemon* p) {
  return Time::Now() - p->last_checkup_ > kCheckupInterval;
}
```

However, `Pokemon` needs to be modified every time a new consumer wants to have a new set of data littering it with data whose semantics are unbeknownst to `Pokemon`.  The inverse of storing consumer data with the consumer -- `PokemonGroomer` having `map<Pokemon*, Time>` -- isn’t straightforward either, since consumers have to be notified of the death of a pokemon to delete unneeded data.

The solution is to hold data related to pokemon with it, but managed by the consumer who knows its semantics.

``` c++
class Pokemon {
  ...
  struct Data {
    virtual ~Data();
  };
  map<Key, unique_ptr<Data>> user_data_;
  ...
};

class PokemonNurse::PokemonData : public Pokemon::Data {
  Time last_checkup_;
};

bool PokemonNurse::NeedsCheckup(Pokemon* p) {
  if (!p->user_data_[PokemonNurse::kDataKey])
    p->user_data_[PokemonNurse::kDataKey] = make_unique<PokemonData>();
  return Time::Now() - p->user_data_[PokemonNurse::kDataKey].last_checkup_
      > kCheckupInterval;
}
```

This pattern decouples lifetimes from invariants.  The lifetime is managed by the central class while the invariants of the data is managed by the respective consumer.

Refer [Chromium’s write-up][associated-data] for details.

[associated-data]: https://source.chromium.org/chromium/chromium/src/+/master:docs/patterns/associated-data.md

## Passkey Pattern

A pattern to expose a subset of methods to another class in a granular way than simply friending the other class.  A dummy key parameter is put in as the guarded function’s argument.  This key can be created only by some classes.

Say only `Foo` and `Bar` should be allowed to call a particular method of `Baz`

``` c++
class Key;
struct Baz { void special(int a, Key /*dummy*/) {} };

struct Foo { void special() { Baz().special(1, {}); } };
struct Bar { void special() { Baz().special(1, {}); } };

class Key { friend Foo; friend Bar; Key() {} Key(Key const&) {} };

int main() {
  Foo().special();      // works
  Baz().special(1, {});
}
```

While [Chromium explains this pattern][passkey-chromium], its intricacies are explained well in [StackOverflow][passkey], there’s also [a more generic version][passkey-generic].  It’s closely related to the [attorney-client pattern][attorney-client].

[passkey-chromium]: https://source.chromium.org/chromium/chromium/src/+/master:docs/patterns/passkey.md
[passkey]: https://stackoverflow.com/a/3218920/183120
[passkey-generic]: https://stackoverflow.com/a/3324984/183120
[attorney-client]: https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Friendship_and_the_Attorney-Client

## Skip destruction of global objects

When there're many static variables[^fn3], perhaps with inter-dependencies, their destruction order becomes important.  To completely circumvent the static **de**-_initialization order fiasco_ at program-exit, ~~Chromium~~ Google C++ Style Guide [calls out][google-static-globals]

> Objects with [static storage duration][static-bss] are forbidden unless they are [trivially destructible][tv-dtor].
> [...]
> When destructors are trivial, their execution is not subject to ordering at all (they are effectively not "run"); otherwise we are exposed to the risk of accessing objects after the end of their lifetime. Therefore, we only allow objects with static storage duration if they are trivially destructible.

Trivially destructible types are not destroyed in the object-sense; their backing memory is just reclaimed.

### `base::NoDestructor`

To facilitate non-trivially destroyed types[^indestructible] to be (function local static) singletons, Chromium defines [`base::NoDestructor<T>` template][no-dtor].  Basically it does placement new in constructor and nothing in destructor.

``` c++
template <typename T>
struct NoDestructor {
  alignas(T) char storage_[sizeof(T)];

  static_assert(
      !std::is_trivially_destructible_v<T>,
      "T is trivially destructible; please use a function-local static "
      "of type T directly instead");

  // Not constexpr; just write static constexpr T x = ...; if the value should
  // be a constexpr.
  template <typename... Args>
  explicit NoDestructor(Args&&... args) {
    new (storage_) T(std::forward<Args>(args)...);
  }

  // Allows copy and move construction of the contained type, to allow
  // construction from an initializer list, e.g. for std::vector.
  explicit NoDestructor(const T& x) { new (storage_) T(x); }
  explicit NoDestructor(T&& x) { new (storage_) T(std::move(x)); }

  NoDestructor(const NoDestructor&) = delete;
  NoDestructor& operator=(const NoDestructor&) = delete;

  ~NoDestructor() = default;
}
```

Since C++11 static local variable initialization is thread-safe and so is this pattern.

[google-static-globals]: https://google.github.io/styleguide/cppguide.html#Static_and_Global_Variables
[static-bss]: http://en.cppreference.com/w/cpp/language/storage_duration#Storage_duration
[tv-dtor]: http://en.cppreference.com/w/cpp/types/is_destructible
[no-dtor]: https://source.chromium.org/chromium/chromium/src/+/main:base/no_destructor.h

[^fn3]: (a) global variables, (b) class static members, (c) function-local static variables.  (a) and (b) are initialized at program-start (order unspecified); all of them are destroyed at program-exit in reverse order of initialization.

[^indestructible]: includes indestructible types (`!std::is_destructible_v<T>` by doing `~T() = delete;`) constructible only by placement-new.

## Disable Operators for Enum in C++

This isn’t a design pattern, but a C++ idiom, to prohibit certain operators for an enum; this is a non-issue for classes, as the compiler anyways don’t supply these operators implicitly.

From C++11 and upwards, you can simply delete the operator. [Live Example][enum-op-del].

``` c++
enum class Shape
{
    Triangle,
    // ...
};

bool operator==(Shape s1, Shape s2) = delete;

```

In classic C++, when you don’t want an enum to support `operator==` or similar, create an undefined class and make it the return type of these operators.

``` c++
enum PageTransition {
  // …
};
// Use this function instead of operator== to properly decode flags in PageTransition.
bool PageTransitionCoreTypeIs(PageTransition lhs, PageTransition rhs);

// Declare a dummy class that is intentionally never defined.
class DontUseOperatorEquals;

// Ban operator== and operator!=, use PageTransitionCoreTypeIs() instead.
DontUseOperatorEquals operator==(PageTransition, PageTransition);
DontUseOperatorEquals operator!=(PageTransition, PageTransition);
```

[enum-op-del]: http://coliru.stacked-crooked.com/a/550800a9897cedcf

## Disable Waiting on a Thread

Every Chromium process generally has a main thread (e.g. Browser process’s UI thread) and a message loop thread[^fn1] (handling IPC messages).  These are kept responsive by disallowing blocking calls on this thread.

A blocking call refers to any call causing calling thread to wait off-CPU.  Includes but not limited to synch file I/O operations like read, write, rename, delete, enumerate files in a directory, interacting with a pipe, etc.  Acquiring a low contention lock isn’t considered blocking.

Generally the app code uses `base` facilities to make such (potentially) blocking calls e.g. `base::LoadNativeLibrary`, `base::FileEnumerator`, etc.  In the implementation of these methods, `base::ScopedBlockingCall` is instantiated as an annotation of the scope that may/will block.  `ScopedBlockingCall`’s constructor asserts if the current thread is allowed to make such a call; if it isn’t call stack is printed to `STDOUT`.

Every thread has a set of thread-local storage `bool`s marking which family of such calls are disallowed e.g. `DisallowUnresponsiveTasks` sets `g_blocking_disallowed`, `g_base_sync_primitives_disallowed` and `g_cpu_intensive_work_disallowed`.

Additionally, as a last resort, scoped-allowance mechanisms are offered to make an exception within a scope for a call that’s usually disallowed e.g. `base::ScopedAllowBlocking`.

!!! Note
    Every object is bound to a single thread[^fn2]; inter-thread message passing is the means of communication.  This is also [Golang’s concurrency principle][go-concurrency] based on Tony Hoare’s [CSP][].

> "Don’t communicate by sharing memory, share memory by communicating."

### Refer

* [`base/threading/thread_restrictions.h`][thread-restrict]
* [`base/threading/thread_restrictions.cc`][thread-restrict-impl]
* [`base/threading/scoped_blocking_call.h`][scoped-blocking]
* [`base/native_library_posix.h`][load-native-library]
* [Anatomy of a Process (2019) - Chromium University][process-anatomy]


[CSP]: https://en.wikipedia.org/wiki/Communicating_sequential_processes
[go-concurrency]: https://blog.golang.org/codelab-share
[thread-restrict]: https://cs.chromium.org/chromium/src/base/threading/thread_restrictions.h
[thread-restrict-impl]: https://cs.chromium.org/chromium/src/base/threading/thread_restrictions.cpp
[scoped-blocking]: https://cs.chromium.org/chromium/src/base/threading/scoped_blocking_call.h
[load-native-library]: https://cs.chromium.org/chromium/src/base/native_library_posix.cc
[process-anatomy]: https://youtu.be/5im7SGmJxnA?list=PLNYkxOF6rcICgS7eFJrGDhMBwWtdTgzpx&t=356

[^fn1]: Has a misoner “I/O thread” but does no blocking I/O.  It handles messages of the process, offloading busy work to other worker threads.

[^fn2]: Often these are virtual, but not physcial, threads.  Virtual threads are chrome-managed thread of execution called sequences.  Tasks are executed sequentially, so preceding tasks side-effects are visible to succeeding ones.  Tasks may hop physcial threads between each one.


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'medium', inlineCodeLang: 'c++' };</script>
