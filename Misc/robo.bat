@echo off

rem usage: robo.bat source/ dest/
rem http://www.geekality.net/2010/11/15/how-to-backup-a-folder-with-robocopy/
rem /XA:S to exclude directories like "$RECYCLE.BIN" and "System Volume Information"
rem see http://serverfault.com/a/34711/139619 for attribute map

robocopy %1 %2 /J /XJ /NP /V /E /Z /R:3 /W:3 /TEE /XA:S /LOG:%3.log
