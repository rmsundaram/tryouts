MSYS2’s Emacs is a decent auto-updated Windows distribution.  Running Emacs outside MSYS2 isn’t a very good option as packages may expect `coreutils` to be present in `PATH`.  A better option is to launch Emacs from MSYS2 (with `(server-start)`) and let Windows programs use the ever-running editor for viewing/editing needs.

# File Association

* Launch Emacs from MSYS2
* Right-click some file (`c`, `cpp`, etc.) and choose _Open With_
* Choose `C:\Apps\msys64\mingw64\bin\emacsclientw.exe`; it should open in already-running Emacs
    - If not running, as the `ALTERNATE_EDITOR` isn’t set to `runemacs.exe`, it wouldn't be able to find it[1][]
* Goto `HKEY_CURRENT_USER\Software\Classes\Applications\emacsclientw.exe\shell\open\command`
* Change `(Default)` string value to `"C:\apps\msys64\mingw64\bin\emacsclientw.exe" -nf "C:\apps\msys64\home\sundaram\.emacs.d\server\server" "%1"`
* Test by opening files with Emacs which reuses the existing process
    + Doing a `kill-buffer` shouldn't prompt or alert you that the client is still waiting
* From now on, select _Open With_ for any extension you'd like to associate Emacs with and choose _Emacs_ if it's in the list; if it isn't there, just give the path like you did earlier and it'll do the needful (i.e. call with the arguments)

[1]: https://www.gnu.org/software/emacs/manual/html_node/efaq-w32/Associate-files-with-Emacs.html
