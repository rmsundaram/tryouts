#!/usr/bin/env bash

input="${1:-resume.html}"
output="${input%.html}.pdf"

# if wkhtmltopdf isn't available, use Chromium as fallback; it does a decent job of producing a PDF
# wkhtmltopdf doesn't support setting the fields author, subject and keywords; use cpdf to do these
wkhtmltopdf --page-size Letter --dpi 150 --margin-top 0.6in --margin-bottom 0.7in --margin-left 0.7in --margin-right 0.7in --footer-spacing 7 --footer-font-size 8 --footer-center '[page]/[toPage]' --allow "./" "${input}" "${output}"
