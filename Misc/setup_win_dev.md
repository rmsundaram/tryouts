# Setup
This covers the steps involved in setting up a Unix-like terminal environment on a Windows machine for building/running projects in this repository.  All tools and libraries used are cross-platform, so setting up the same on a Linux box shouldn't be a problem.

# Tools

* Shell
    + Bash
    + MSYS2
* Editors
    + Emacs (associated to all text-related files in Windows shell; running as server)
    + Nano (light editing within terminal; ncurses Emacs doesn't work inside MSYS2's mintty)
* Compilers
    + [MinGW](http://sourceforge.net/projects/mingw-w64/files/Toolchains%20targetting%20Win32/Personal%20Builds/mingw-builds/)
    + [Clang](http://www.llvm.org/releases/3.9.0/LLVM-3.9.0-win32.exe) (for code completion on Emacs)
* Interpreters
    + Lua
    + Perl
    + Python
* Libraries
    + GLFW
    + GLM
    + GLI
    + Löve 2D
    + glMatrix
    + TWGL
    + [glLoadGen](https://bitbucket.org/alfonse/glloadgen/wiki/Home) to generate OpenGL headers with requested profile and extensions
    + FreeGLUT
    + Boost
* Documentators
    + Markdown
    + Pandoc
    + Hugo
* Graphics
    + GIMP
    + Inkscape
    + Blender
* Misc
    + 7-zip
    + CMake (use `$MINGW_PACKAGE_PREFIX-cmake`; not `cmake`!)
      - `cmake` won’t set CMake variables like `WIN32`; `CMAKE_SYSTEM_NAME` = `MSYS` not `Windows`
    + Git
    + Global
    + Hunspell
    + Lua Rocks
    + MikTeX
    + QtCreator (not the entire Qt framework) — when an IDE is felt necessary
    + WinMerge
    + wkhtmltopdf
    + ZeroBrane Studio

Although MSYS2 has most of the package listed above (MinGW, Emacs, Lua, Python, Blender, etc.), it's constantly updated (bleeding-edge) and at times has only specific versions.  Having them as separate installations give greater stability and control.  For instance, MSYS2's

* Lua Rocks is available only for Lua 5.1 not for 5.3
* Python documentation is missing
* pip3 cannot be updated through pip3
* Emacs can only be launched from Bash and not from Command Prompt
* Make needs its compiler but most cross-platfrom Windows builds work on _MinGW Makefiles_ not very well _MSYS Makefiles_

So we choose MSYS2 for the core, find, diff and other utilities (Git, ag, lftp, Hunspell, etc.) but have independent first-class Windows installations (or extracts) for the rest, if available at project source.  These have the advantage of working on both Bash and Command Prompt.  However, when a tool is known to be stable and mature for quite sometime (Perl, Git, etc.) we go with the MSYS2 package since we get painless updates.

An alternative to MSYS2 and MinGW is [winlibs][].

!!! Tip
    On Windows EOF is issued by <kbd>Ctrl + Z, ⮐</kbd>, not <kbd>Ctrl + D</kbd>.

[winlibs]: http://www.winlibs.com

## MinGW
### Bit width

* 32-bit ✓
* 64-bit

32-bit since it's easier to get the binaries for supporting libraries and tools in 32-bit; however the code should be the same for both bit widths so that compiling for 64-bit target should work with no changes to the code; see [MinGW-w64 for the Uninitiated][mingw-uninit] for details.  MinGW-W64 [isn't really dual target][mingw-non-duality] thus far.

[mingw-uninit]: http://www.rioki.org/2013/10/16/mingw-w64.html
[mingw-non-duality]: https://stackoverflow.com/q/16304804

### Threading Mechanism
* POSIX threads ✓
* Win32 Threads — faster but [no C++11 concurrency](https://stackoverflow.com/q/17242516) features

### Exception-handling Mechanism
For details on each, see [What is the difference between SJLJ, Dwarf and SEH?](http://stackoverflow.com/q/15670169)

* Dwarf ✓ — although [will fail propagating the exception through non-dwarf parts](http://stackoverflow.com/a/15685229), if one can guarantee propagation of exceptions only through code that is compiled with the same compiler, or another DW2-enabled GCC toolchain, it's faster than SJLJ
* SJLJ — slow but works on almost all cases except general protection faults
* SEH — Support is available for 64-bit only. It features the `finally` mechanism not present in standard С++ exceptions

The same choices are opted by MSYS2's MinGW builds too. See [TDM GCC's quirks](http://tdm-gcc.tdragon.net/quirks) for a good deal of information on these choices.

### Debugger
Windows 11 has experimental UTF-8 option for system locale which [meddles with GDB](https://github.com/microsoft/vscode-cpptools/issues/6790#issuecomment-1302036168).  One can either disable this option or set GDB to use UTF-8; latter is better.  `set charset UTF-8` solves the issue.

Recent MSYS2 GDBs have pretty printing enabled by default, so `python register_libstdcxx_printers(None)` mightn’t be needed anymore.

In case you want to confirm if your build of GDB has Python within, check with `python print(sys.version)`.  The [STL Support Tools](https://sourceware.org/gdb/wiki/STLSupport) page has more details.

### Installation
Extract it somewhere.  Append it's `bin` to system's `PATH` e.g. `C:\mingw32\bin` (user local `PATH` would do).  Now giving `gcc` command on a new command prompt would give you `gcc: fatal error: no input files`.

Extract GLM, GLFW and all the future libraries onto a directory like `D:/Libs/glm` (without any version number is usually better when one upgrades the library); avoid spaces to play nice with MSYS2 (Cygwin).

Prefer static over dynamic/shared libraries since

* [Shared Libraries are Bad for You](https://geometrian.com/programming/tutorials/sharedlib/index.php)
* having dependencies in-tree are easier to build
* dependencies are version-locked
  - shouldn’t upgrade without the developer’s knowledge
  - constant anxiety if something is broken due to the upgrade
  - regular time loss in fixing issues due to upgrade
  - dependency upgrade is a conscious decision warranting a separate task

## MSYS2
* Get the latest version of [MSYS2](http://sourceforge.net/projects/msys2/) from [here](http://sourceforge.net/projects/msys2/files/Base/x86_64/) and extract it to a path with no spaces. `<drive>:/msys64` is ideal e.g. `D:/msys64`.
* Follow the [installation instructions](http://sourceforge.net/p/msys2/wiki/MSYS2%20installation/).  Usually launching it for the first time makes it do preliminary stuff and once that is done, it'll prompt you to restart itself; do it, _do not_ skip this step.
* Update packages using `pacman -Syuu`; restart as instructed.
* Repeat until no updates are available.
* Choose packages to install
    + `coreutils`
    + `diffutils`
    + `findutils`
    + `file`
    + `gawk`
    + `git`
    + `grep`
    + `info`
    + `less`
    + `lftp`
    + `markdown`
    + `mc`
    + `nano`
    + `perl`
    + `sed`
    + `subversion`
    + `tree`
    + `which`

Once done, `man git` or `man find` should serve you man pages of the respective commands.  You should be able to browse Info documents from within Emacs.

Periodically run

``` shell
updatedb
mandb
```

to keep `locate` and `man` shipshape.

To TRAMP-edit remote files on Emacs

* Install `$MINGW_PACKAGE_PREFIX-putty`
* Make sure `plink.exe` is in Emacs’ `exec-path` `PATH` and is accessible by Emacs
* Use `/plink:USER@HOST:PATH` as argument to `find-file`

### Fix Username

Windows 11 MSYS2 installation’s username has hostname prepended to it.  To rename the user

``` shell
mkpasswd -l -c > /etc/passwd
mkgroup -l -c > /etc/group
nano /etc/passwd
# fix first field of required user
```

In `/etc/nsswitch.conf`, remove `db` from `passwd` and `group` ; refer [Msys2_FAQs.md][./Msys2_FAQs.md] on .
