REM Print a process’ command-line arguments
REM https://www.addictivetips.com/windows-tips/command-line-arguments-for-an-app-on-windows-10/
wmic path Win32_Process where handle='%1' get Commandline /format:list
