// http://codingcastles.blogspot.com/2009/03/floating-point-exceptions.html

#include <iostream>
#include <cmath>

#ifdef _WIN32
 #undef __STRICT_ANSI__ // _controlfp is a non-standard function documented in MSDN
 #include <cfloat>
 #ifndef _EM_OVERFLOW
  #define _EM_OVERFLOW EM_OVERFLOW
 #endif
#else
 #define _GNU_SOURCE    // gives us feenableexcept on older GCCs
 #define __USE_GNU      // gives us feenableexcept on newer GCCs
 #include <cfenv>
#endif

int main(int argc, char **argv)
{
#ifdef _WIN32
    _clearfp(); // always call _clearfp before enabling/unmasking a FPU exception
/*
unsigned _controlfp(new_ctrl_word, mask);

<MSDN>
When a floating-point exception is enabled, the FPU will halt execution of the offending instruction and then signal an exceptional condition by setting the FPU status word. When the CPU reaches the next floating-point instruction, it first checks for any pending FPU exceptions. If there is a pending exception, the processor traps it by calling an exception handler provided by the Operating System. This means that when a floating-point operation encounters an exceptional condition, the corresponding exception won't be detected until the next floating-point operation is executed.
http://msdn.microsoft.com/en-us/library/aa289157%28v=vs.71%29.aspx

For the _MCW_EM mask, clearing the mask sets the exception, which allows the hardware exception; setting the mask hides the exception. If a _EM_UNDERFLOW or _EM_OVERFLOW occurs, no hardware exception is thrown until the next floating-point instruction is executed. To generate a hardware exception immediately after _EM_UNDERFLOW or _EM_OVERFLOW, call the FWAIT MASM instruction.
http://msdn.microsoft.com/en-us/library/e9b52ceh.aspx
</MSDN>

_MCW_EM comprises of _EM_INVALID, _EM_DENORMAL, _EM_ZERODIVIDE, _EM_OVERFLOW, _EM_UNDERFLOW, _EM_INEXACT flags. Thus calling calling _controlfp(0, _MCW_EM) should raise an exception for all of the above since all of them bits (flags) are cleared, while calling _controlfp(_EM_ZERODIVIDE, _MCW_EM); should set the divide-by-zero bit thereby masking that exception.
*/

    //_controlfp(0, _EM_OVERFLOW | _EM_ZERODIVIDE);   // clear the required bits
    unsigned unused_old_word;   // using _controlfp_s, the more secure version of _controlfp
    _controlfp_s(&unused_old_word, 0, _EM_OVERFLOW | _EM_ZERODIVIDE);  // clear the required bits

/*
Another way to do it

    unsigned cw = _controlfp(0, 0) & _MCW_EM;       // passing 0 for the mask gets the control word, while passing non-zero sets it
    cw &= ~(_EM_ZERODIVIDE | _EM_OVERFLOW);         // unset the required flags to raise an exception for those cases
    _controlfp(cw, _MCW_EM);                        // copy back the word with cleared bits while the other bits are left intact

Alternatively, an exception filter that translates the hardware exception to a C++ exception (be sure to be selective, only translate the FPU exceptions) can be written and set via _set_se_translator(), which provides a way to handle Win32 exceptions (C structured exceptions) as C++ typed exceptions.
<MSDN>
As with all hardware exceptions, floating-point exceptions do not intrinsically cause a C++ exception, but instead trigger a structured exception. To map floating-point structured exceptions to C++ exceptions, users can introduce a custom SEH exception translator.
http://msdn.microsoft.com/en-us/library/aa289157%28v=vs.71%29.aspx
</MSDN>

*/
#else
// http://stackoverflow.com/q/2941611/183120
// fesettrapenable seems to be a non-standard function available on most POSIX platforms
    feenableexcept(FE_OVERFLOW | _EM_ZERODIVIDE);
#endif

    // GCC will compute these expressions at compile-time (which is
    // actually kinda cool, but it ruins the example) if we just do
    // pow(10.0, 50) and fabs(-13).
    int exponent = 50;
    float a = pow(10.0, exponent);

    std::cout << "1e50 is too big for a float, but we got this far,\n"
                 "so we must be okay, right?\n";

    float b = 1.0f;
    // will signal SIGFPE only here mostly
    b *= 2.0f;

    std::cout << "You'll never see this, because we just got an overflow trying\n"
                 "to process a really small number!  How can this be!?!?\n"
                 "The world is coming to an end!\n";

    std::cout << a << " " << b << std::endl;
}
