#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <utility>

template <typename T>
#ifdef MODERN_ITERATOR
struct simpleItr
#else
struct simpleItr : public std::iterator<std::random_access_iterator_tag, T, T, T*, T&>
#endif
{
#ifdef MODERN_ITERATOR
    typedef std::random_access_iterator_tag iterator_category;
    typedef T value_type;
    typedef T difference_type;
    typedef T* pointer;
    typedef T& reference;
#endif

    simpleItr(T uVal) : _Value(uVal) { }

    T operator *() const
    {
        return _Value;
    }
    
    T operator-(const simpleItr &that) const
    {
        return this->_Value - that._Value;
    }

    bool operator == (const simpleItr &that) const
    {
        return (this->_Value == that._Value);
    }

    bool operator < (const simpleItr &that) const
    {
        return this->_Value < that._Value;
    }

    const T& operator++()
    {
        return ++_Value;
    }

private:
    T _Value;
};

// this is to tackle a bug in MSVC++
#ifdef __GNUG__
using namespace std::rel_ops;
#elif defined _MSC_VER
template <typename T>
bool operator!= (const simpleItr<T> &lhs, const simpleItr<T> &rhs)
{
    return !(lhs == rhs);
}
#endif

int main()
{
    simpleItr<unsigned int> first(0), last(11);
    std::vector<unsigned int> v(first, last);
    auto it = v.begin();
    it = v.emplace(it, 12);
    v.emplace(it, 13);

    std::ostream_iterator<unsigned int> oit(std::cout, " ");
    std::copy_n(v.cbegin(), v.size(), oit);
}
