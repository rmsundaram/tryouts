/*
 *  TODO:
 *  0. In the constructor use log2 and reserve before using std::vector
 *  1. The current constructor converts to integer on its own; use strtoull
 *     instead thereby avoiding manual handling of overflow with built-ins.
 *  2. The current constructor is such that the result of the first (inner)
 *     iteration is needed for the second iteration (to form its quotient).  It
 *     can be redesigned so that they are independent loops and hence
 *     parallelized.  Cut into floor(uintmax_t::digits10)-sized substrings and
 *     make n uintmax_t’s.  For all, except the last of these, compute remainder
 *     and put as the most significant digit in next uintmax_t, now perform
 *     division on all, in parallel, append quotients to get grand quotient.
 *  3. Write formal unit tests to check edge cases; use Catch / doctest perhaps;
 *     github.com/JorjBauer/lua-bigint seems to be a good option too
 *  4. Support negative numbers and subtraction
 *  5. Write a sign-magnitude-based comparator
 *  6. Implement binary and unary minus operators
 *  7. The second widest type should be BucketType and widest (uintmax_t) would
 *     be BucketOpType (bucket ⊕ bucket); see if this can be codified.  A sane
 *     default would be unsigned and unsigned long long, hoping uint is the
 *     processor’s natural bit width.  Forming a bucket in constructor needs
 *     appropriate bit shifts to position remainders in the right zones within.
 *  8. Alternative for ctor is to use simple atoi algorithm with the first digit
 *     made into a BigNum easily. For every new digit, this BigNum is multipled
 *     by 10 using BigNum::operator*
 *  9. Benchmark exisiting split division ctor with the two others (2 and 8).
 * 10. Write about max bits needed (a ^ b)
 * 11. Explore Karatsuba algorithm weighing its applicability in this type.
 *
 * An alternate BigNum design is storing buckets of decimal, not binary, numbers
 *
 *  CASES TESTED:
 *  123 + 948223
 *  180 + 6340
 *  6340 + 948223
 *  255 + 255
 *  0 + 1
 */

#include <vector>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <bitset>
#include <memory>
#include <limits>
#include <type_traits>

#ifdef _MSC_VER
    #include <safeint.h>

    using msl::utilities::SafeMultiply;
    using msl::utilities::SafeAdd;
#endif

constexpr char dtoc(uint8_t d) {
    // TODO: check if d is in [0, 9]
    return d + '0';
}

constexpr uint8_t ctod(char c) {
    // TODO: check if c is in ['0', '9']
    return c - '0';
}

// Performs number = (number * 10) + digit with an eye for overflow returns true
// if T could hold the result and false otherwise.  Number is changed only if
// the operation was completely successful.
// TODO: Why can’t these checks be done in a compiler-agnostic way with just C++
// expressions like x > max / y?
template <typename T>
bool place_at_units_digit(T* number, uint8_t digit) {
    T new_num;
#ifdef __GNUG__
    auto const placed = (!(__builtin_mul_overflow(*number, 10u, &new_num) ||
                           __builtin_add_overflow(new_num, digit, &new_num)));
#elif defined(_MSC_VER)
    auto const placed = (SafeMultiply(*number, 10u, new_num) &&
                         SafeAdd(new_num, digit, new_num));
#endif
    if (placed) *number = new_num;
    return placed;
}

template <typename T>
constexpr std::enable_if_t<std::is_unsigned<T>::value, const char*>
format_str(T /*unum*/) {
    return "%u";
}

// since types lesser than unsigned (int) would get promoted to it, specialize
// only types wider than that; ignore unsigned char and unsigned short
template <>
constexpr char const* format_str(unsigned long /*unum*/) {
    return "%lu";
}

template <>
constexpr char const* format_str(unsigned long long /*unum*/) {
    return "%llu";
}

template <typename T>
std::enable_if_t<std::is_unsigned<T>::value, unsigned>
constexpr leading_zeros(T x) {
    auto constexpr T_bits = std::numeric_limits<T>::digits;
#ifdef __GNUC__
    auto constexpr promo_T_bits = std::numeric_limits<unsigned>::digits;
    static_assert(T_bits <= promo_T_bits, "Error: Use a wider promoted type");
    return __builtin_clz(x) - (promo_T_bits - T_bits);
#elif defined _MSC_VER
    unsigned long set_bit_idx = 0u;
    _BitScanReverse(&set_bit_idx, x);
    // scans left → right, returns the index of the found bit addressed from the
    // LSB, starting at 0
    return (T_bits - 1) - set_bit_idx;
#endif
}

struct BigNum
{
    using BucketType = uint8_t;
    constexpr auto static bucket_bits = std::numeric_limits<BucketType>::digits;
    using BucketOpType = unsigned;
    // §2.6, Volume 1, Write Great Code
    // 7. Multiplying two n-bit binary values together may require as many as
    //    2 * n bits to hold the result.
    // 8. Adding or subtracting two n-bit binary values never requires more than
    //    n + 1 bits to hold the result.
    static_assert(sizeof(BucketOpType) >= 2 * sizeof(BucketType),
                  "Error: Need wider type for operations");

    // Convert (string) decimal to binary number by repeated ÷ 16 and harvest
    // the remainder; cut big number string into holdable uints and divide,
    // concatenate quotient to get dividend for next division; store remainder.
    // Both the outer and inner loops can be made faster.  Using a larger
    // divisor would reduce the outer loop iterations.  The inner loop can be
    // optimised in two ways: the number of divisions can be minimized by using
    // larger data types to hold the dividend (part_dividend's type).  Likewise,
    // increasing the divisor also reduces the number of inner loop iterations.
    // So divisor can be 2, 16, etc. but 256 is perhaps the fastest for byte
    // since it directly gives a byte-sized remained, while 16 would give two
    // nibbles to be ORed to give a byte.
    BigNum(char const *num) {
        size_t quot_len = 0u;
        if (!num || !(quot_len = strlen(num))) {
            v.push_back(0u);
            return;
        }
        // delegate to integral constructor if it can accommodate it
        else if (quot_len <= std::numeric_limits<uintmax_t>::digits10)
            BigNum(std::strtoull(num, nullptr, 0));

        // num is the largest dividend and all further (quotients) dividends
        // would be smaller than this
        quot_len += 1u;
        std::unique_ptr<char[]> dividend(new char[quot_len]);
        // len(quotient) <= len(dividend) always
        std::unique_ptr<char[]> quotient(new char[quot_len]{});
        std::copy_n(num, quot_len, dividend.get());
#ifdef NDEBUG
        auto constexpr divisor = 256u;
        uintmax_t part_dividend = 0ull;
#else
        auto constexpr divisor = 16u;
        uint8_t part_dividend = 0u;
        bool add_bucket = true;
#endif
        // loop over multiple divisions
        do {
            auto i = 0u, j = 0u;
            // use longest unsigned type for lesser inner division loops;
            // uint8_t used for debugging, as it's simpler to understand
            part_dividend = 0u;
            // part division loop: single big int divided here in loop
            while (isdigit(dividend[i])) {
                auto const digit = ctod(dividend[i++]);
                // handle cases needing 0 insertion between partial divide
                // quotients e.g when using uint8_t, for input 3217, the
                // quotient would be 201; here manual 0 insertion is needed
                if (j && (part_dividend < divisor))
                    quotient[j++] = '0';
                if (!place_at_units_digit(&part_dividend, digit)) {
                    // not using cstdlib's std::div since there's no unsigned
                    // variant; doing /16 and %16 directly will be fast as 16 is
                    // a power of 2, and they'll be made into shift operations
                    // even in GCC's O0; see bit_array.cpp
                    auto const part_quot = part_dividend / divisor;
                    part_dividend = part_dividend % divisor;
                    // place j at NUL character put by printf to extend quotient
                    // in the next loop by overwriting it
                    j += snprintf(&quotient[j],
                                  quot_len - j,
                                  format_str(part_quot),
                                  part_quot);
                    // no check as part_dividend would be able to hold the
                    // result for all values of old part_dividend and digit
                    // e.g. when using uint8_t, say part_dividend = 255, then
                    // part_dividend = 255 % 16 = 15 → 150 + 9 = 159 (still ok)
                    part_dividend = (part_dividend * 10u) + digit;
                }
            }
            j += snprintf(&quotient[j],
                          quot_len - j,
                          format_str(part_dividend),
                          part_dividend / divisor);
#ifdef NDEBUG
            v.push_back(part_dividend % divisor);
#else
            if (add_bucket)
                v.push_back(part_dividend % divisor);
            else
                v.back() |= ((part_dividend % divisor) << 4u);
            add_bucket = !add_bucket;
#endif
            swap(dividend, quotient);
        } while (dividend[0] != '0');
    }

    BigNum(std::vector<BucketType> input) : v(std::move(input)) { }

    // trick to use enable_if in situations not having a return type
    // http://stackoverflow.com/a/20710008
    template <typename T>
    explicit BigNum(T num = 0u,
                    std::enable_if_t<std::is_convertible<T, uintmax_t>::value>*
                    dummy = nullptr) {
        auto constexpr n = sizeof(num);
        v.resize(n);
        auto src = reinterpret_cast<uint8_t*>(&num);
#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
        auto dst = v.begin();
#else
        auto dst = v.rbegin();
#endif
        std::copy(src, src + n, dst);
        trim_leading_zero_buckets();
    }

    void trim_leading_zero_buckets() {
        auto it = v.rbegin();
        // iterate till one before first since the vector should’ve atleast one
        // element even if the contained value is 0
        // beware: for loop without body
        for (; (it != v.rend() - 1) && (*it == 0u); ++it);
        // conversion from reverse to forward iterator has its quirks
        // rbegin becomes end; rend becomes begin; other map as-is
        // http://stackoverflow.com/a/2038101
        v.erase(it.base(), v.end());
    }

    BigNum& operator<<(unsigned int n) {
        // do nothing if zero
        if ((n == 0) || ((v.size() == 1) && (v.front() == 0)))
            return *this;

        auto new_buckets = n / bucket_bits;
        BucketType const shifts = n % bucket_bits;
        auto const lz = leading_zeros(v.back());
        auto const overflowing = (shifts > lz);
        new_buckets += overflowing;
        v.resize(v.size() + new_buckets);
        // get iterator to old v.back() to source from
        auto src_it = v.crbegin() + new_buckets;
        auto dst_it = v.rbegin();
        if (overflowing)
            *dst_it++ = *src_it >> (bucket_bits - shifts);
        // iterate with dst to shift this and OR (from previous), or iterate
        // with src to do OR (to next) and shift this; eitherways there’d be an
        // extra statement to deal with the last or first element; the latter
        // lets both loops be adjacent (no intervening statement) while the
        // former is intuitive; chose intuitive for readability
        for (; src_it != v.crend() - 1; ++src_it, ++dst_it) {
            *dst_it = *src_it << shifts;
            *dst_it |= *(src_it + 1) >> (bucket_bits - shifts);
        }
        *dst_it = *src_it << shifts;
        // fill extra buckets, if any, with zeros
        std::fill(++dst_it, v.rend(), BucketType());
        return *this;
    }

    BigNum& operator>>(unsigned int n) {
        if (n == 0)
            return *this;

        // zero out if n >= total bits in this number
        if (n < (bucket_bits * v.size())) {
            auto const drained_buckets = n / bucket_bits;
            auto const shifts = n % bucket_bits;
            auto src_it = v.cbegin() + drained_buckets;
            auto dst_it = v.begin();
            for (; src_it != v.cend() - 1; ++src_it, ++dst_it) {
                *dst_it = *src_it >> shifts;
                BucketType t = *(src_it + 1);
                t <<= (bucket_bits - shifts);
                *dst_it |= t;
            }
            *dst_it = *src_it >> shifts;
            v.resize(v.size() - drained_buckets);
        }
        else
            v.assign(1, 0u);
        return *this;
    }

    std::vector<BucketType> v;


    /*
     * BUCKETS
     *       0     1     2     3                             n
     *    +-----+-----+-----+-----+-----------------------+-----+
     *    | B0  | B1  | B2  | B3  |           …           | Bn  |
     *    +-----+-----+-----+-----+-----------------------+-----+
     *     least significant bucket  →  most significant bucket
     *
     * BITS
     *      7   6   5   4   3   2   1   0
     *    +---+---+---+---+---+---+---+---+
     *    | 1 | 0 | 0 | 1 | 1 | 1 | 0 | 0 |
     *    +---+---+---+---+---+---+---+---+
     *             MSB  ←  LSB
     *
     * Though the numbering is in opposite directions, both are stored such that
     * lower index value denotes lower significance.  Both of these figures are
     * made with no implementation assumptions.  Buckets is just a vector of
     * uint8_t values and bits inside each uint8_t is such that LSB is at bit 0.
     * The actual direction of drawing is immaterial.
     *
     * This scheme has an advantage: a number in [0, 255] interval will be
     * stored in a vector with single uint8_t as though it were a normal uint8_t
     * and operations on it tend to be as good as on that of a simple uint8_t.
     *
     * Likewise, storing the buckets in this order allows two BigNums with
     * differing vector sizes to be operated on in a simple fashion, with a
     * single iterator.
     *
     * EXAMPLE
     *              0     1     2    3
     *           +-----+-----+-----+-----+
     *     N1 =  | B0  | B1  | B2  | B3  |
     *           +-----+-----+-----+-----+
     *           +-----+-----+
     *     N2 =  | B0  | B1  |
     *           +-----+-----+
     *
     * for (i = 0; i <= N2.size(); ++i) add(N1[i], N2[i]);
     *
     * Had it been stored the other way around, the situation would be
     *
     *              0     1     2    3
     *           +-----+-----+-----+-----+
     *     N1 =  | B3  | B2  | B1  | B0  |
     *           +-----+-----+-----+-----+
     *           +-----+-----+
     *     N2 =  | B1  | B0  |
     *           +-----+-----+
     *
     * auto i = N2.size() - 1;
     * auto j = N1.size() - 1;
     * for(; i >= 0; --i, --j) add(N2[i], N1[j]);
     */
};

// carry is an inout param
BigNum::BucketType add(BigNum::BucketType l,
                       BigNum::BucketType r,
                       BigNum::BucketType* carry) {
    BigNum::BucketOpType res = l + r + *carry;
    *carry = res > 0xff;
    return res & 0xff;
}

char add(char l, char r, char *carry) {
    auto const result = (ctod(l) + ctod(r) + ctod(*carry));
    auto const d = div(result, 10);
    *carry = dtoc(d.quot);
    return dtoc(d.rem);
}

// adds two operands; expects them to be a container type C, containing elements
// of type T, with some null value nil
template <typename C, typename T, T nil = T(0)>
C add(C const &l, C const &r) {
    auto const &big = (l.size() >= r.size()) ? l : r;
    auto const &small = (l.size() >= r.size()) ? r : l;
    // take big as the result; iterate over small adding to big bucket-wise
    auto result = big;
    T carry = nil;
    unsigned i;
    for (i = 0u; i < small.size(); ++i) {
        // deal with the carry that might get generated
        result[i] = add(small[i], big[i], &carry);
    }
    // the last carry will either go into the next bucket in big or if it's
    // full, it'll get its own bucket; do it until there's no carry
    while (carry != nil) {
        if (i < big.size())
            result[i] = add(big[i], nil, &carry);
        else {
            result.push_back(carry);
            carry = nil;
        }
        ++i;
    }
    return result;
}

inline BigNum operator+(BigNum const &l, BigNum const &r) {
    return add<std::vector<uint8_t>, uint8_t>(l.v, r.v);
}

// implement the non-trivial operator== and operator<
// implement all other relational operators using them
// http://en.cppreference.com/w/cpp/language/operators#Relational_operators
inline bool operator==(BigNum const &lhs, BigNum const &rhs) {
    return lhs.v == rhs.v;
}

inline bool operator<(BigNum const &lhs, BigNum const &rhs) {
    // manual short-circuit for different-sized numbers since this won’t be done
    // by std::lexicographical_compare e.g. it’d deem v1 < v2:
    // v1 = { '1', '0', '0' }, v2 = { '7' };
    if (lhs.v.size() != rhs.v.size())
        return lhs.v.size() < rhs.v.size();

    // although vector’s std::operator< is lexicographical_compare, we can’t use
    // it since it iterates from begin to end while we want it in reverse
    return std::lexicographical_compare(lhs.v.crbegin(),
                                        lhs.v.crend(),
                                        rhs.v.crbegin(),
                                        rhs.v.crend());
}

inline bool operator!=(const BigNum& lhs, const BigNum& rhs) {
    return !(lhs == rhs);
}
inline bool operator>(const BigNum& lhs, const BigNum& rhs) {
    return rhs < lhs;
}
inline bool operator<=(const BigNum& lhs, const BigNum& rhs) {
    return !(lhs > rhs);
}
inline bool operator>=(const BigNum& lhs, const BigNum& rhs) {
    return !(lhs < rhs);
}

// unused reference implementation computing large powers of 2
std::string pow2(unsigned n) {
    std::string r = "1";
    while (n) {
        r = add<std::string, char, '0'>(r, r);
        --n;
    }
    return std::string(r.crbegin(), r.crend());
}

std::ostream& operator<<(std::ostream &os, BigNum const &n) {
#ifndef NDEBUG
    char bin_prefixes[] = { 'b', '\'' };
    char dec_prefixes[] = { ' ', ',' };
    os << "0";
    std::ostringstream oss;
    int i = 0;
    for (auto it = n.v.crbegin(); it != n.v.crend(); ++it, ++i) {
        std::bitset<std::numeric_limits<uint8_t>::digits> bits(*it);
        os << bin_prefixes[static_cast<bool>(i)] << bits;
        oss << dec_prefixes[static_cast<bool>(i)] << ' ' << +*it;
    }
    os << '\t' << oss.str();
#else
    // traditional binary to decimal conversion done with ASCII numbers
    auto constexpr num_digits = BigNum::bucket_bits;
    std::string r = "0";
    std::string pow_two = "1";
    for (auto it = n.v.cbegin(); it != n.v.cend(); ++it) {
        std::bitset<num_digits> const bits(*it);
        for (auto i = 0u; i < num_digits; ++i) {
            if (bits[i]) r = add<std::string, char, '0'>(r, pow_two);
            pow_two = add<std::string, char, '0'>(pow_two, pow_two);
        }
    }
    os << std::string(r.crbegin(), r.crend());
#endif
    return os;
}

void test_left_shift() {
    BigNum const b3("2859808214");
    auto b31 = b3;
    b31 << 1;
    std::cout << '\n' << b3 << '\n' << b31 << '\n';
    b31 = b3;
    b31 << 2;
    std::cout << b31 << '\n';
    b31 = b3;
    b31 << 3;
    std::cout << b31 << '\n';
    b31 = b3;
    b31 << 4;
    std::cout << b31 << '\n';
    b31 = b3;
    b31 << 5;
    std::cout << b31 << '\n';
    b31 = b3;
    b31 << 6;
    std::cout << b31 << '\n';
    b31 = b3;
    b31 << 7;
    std::cout << b31 << '\n';
    b31 = b3;
    b31 << 8;
    std::cout << b31 << '\n';
    b31 = b3;
    b31 << 9;
    std::cout << b31 << '\n';
    b31 = b3;
    b31 << 10;
    std::cout << b31 << '\n';

    BigNum t1(733);
    BigNum t2 = t1;
    BigNum t3("733");
    std::cout << t1 << '\n';
    std::cout << (t1 << 14) << '\n' << (t2 << 15) << '\n' << (t3 << 16);
}

void test_right_shift() {
    BigNum t1(0xff);
    std::cout << '\n' << t1;
    t1 >> 6;
    std::cout << '\n' << t1;

    BigNum t2(0b100'10100101'01010001);
    std::cout << '\n' << t2;
    auto t3 = t2;
    t3 >> 1;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 2;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 3;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 4;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 5;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 6;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 7;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 8;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 9;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 10;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 11;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 12;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 13;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 14;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 15;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 16;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 17;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 18;
    std::cout << '\n' << t3;
    t3 = t2;
    t3 >> 19;
    std::cout << '\n' << t3;
}


// test uintmax_t ctor and trim_leading_zeros; should run without sigsegv
void test_ullint_ctor() {
    BigNum t4(0xFF00FF00FF00FF00);
    BigNum t5(0b10100100'11111111'00000000);
    BigNum t6(0b10100100);
    BigNum t7(unsigned(0));
    BigNum t8(0xA4FF01);  // = t5 + 1
    // test comparator and equalto operator
    std::cout << std::boolalpha
              << '\n'
              << (t5 < t6)
              << '\n'
              << (t7 < t6)
              << '\n'
              << (t8 == t5)
              << '\n'
              << (t5 < t8)
              << '\n';
}

int main(int argc, char **argv) {
    if (argc < 3)
        std::cerr << "usage: lbn num1 num2";
    else {
        BigNum b1(argv[1]);
        BigNum b2(argv[2]);
        std::cout << b1 << " + " << b2 << " = " << b1 + b2;
    }
#ifndef NDEBUG
    std::cout << "\n\nTesting left shift...";
    test_left_shift();
    std::cout << "\n\nTesting right shift...";
    test_right_shift();
    std::cout << "\n\nTesting integral constructor and opertors...";
    test_ullint_ctor();
    std::cout << "\nAll tests passed successfully!";
#endif
}
