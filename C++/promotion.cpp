#include <cstdio>
#include <climits>
#include <iostream>

/*
 *    C DATA MODEL NOTATION
 *
 *    Of late, I've run across several articles that employ a compact notation for
 *    describing the C language data representation on different target platforms. I have yet to find the
 *    origins of this notation, a formal syntax, or even a name for it, but it appears to be simple enough
 *    to be usable without a formal definition. The general form of the notation appears to be:
 *
 *                                     I nI L nL LL nLL P nP
 *
 *    where each capital letter (or pair thereof) represents a C data type, and each corresponding n is
 *    the number of bits that the type occupies. I stands for int, L stands for long, LL stands for long
 *    long, and P stands for pointer (to data, not pointer to function). Each letter and number is
 *    optional.
 *
 *    For example, an I16P32 architecture supports 16-bit int and 32-bit pointers, without describing
 *    whether it supports long or long long. If two consecutive types have the same size, you typically
 *    omit the first number. For example, you typically write I16L32P32 as I16LP32, which is an
 *    architecture that supports 16-bit int, 32-bit long, and 32-bit pointers.
 *
 *    The notation typically arranges the letters so their corresponding numbers appear in ascending
 *    order. For example, IL32LL64P32 denotes an architecture with 32-bit int, 32-bit long, 64-bit long
 *    long, and 32-bit pointers; however, it appears more commonly as ILP32LL64.
 *
 *    Source:
 *    http://web.archive.org/web/20081006073410/http://www.embedded.com/columns/programmingpointers/200900195
 *
 *    Wikipedia's article on 64-bit computing also uses S for short e.g.: SILP64
 */

void a(int) { printf("int"); }
void a(unsigned short) { printf("unsigned short"); }

int main()
{
    int val = 10;    // C++ gotcha
    if(true == val)  // val should be casted to bool for this to work; 1 == 2010 is false, since bool is promoted to int
    {                // an alternative is to use if(val) or if(val != 0)
        printf("I think Microsoft named .Net so it wouldn't show up in a Unix directory listing.");
    }

    // http://stackoverflow.com/a/6770371/183120
    unsigned int c = 5;
    signed int s = -5;
    if (c < s) printf("What!\n%u\n", UINT_MAX);     // C++ gotcha
    // both operands are normalized into unsigned; -5 becomes UINT_MAX + 1 - 5
    // on a LP64 compiler it'd be (2³² - 1) + 1 - 5 = 2³² - 5
    // this is based on the "usual arithmetic conversions" in [expr]
    // on a two's complement machine, there's no conversion that is really happening here
    // it's just reinterpretation of the same bit-pattern: 0xFFFFFFFFB = -5 = 2³² - 5
    // http://stackoverflow.com/a/3668797/183120

    a((short)1);
    // C++ gotcha
    // this doesn't call the unsigned short overload but the int overload since
    // exact match > promotion > conversion; here short is promoted to int while
    // it has to be converted to unsigned short
    // http://stackoverflow.com/q/4640543/183120

    char a = 5;
    std::cout << '\n' << +a;
    // C++ gotcha inherited from C
    // The unary + operator would perform the usual arithmetic conversions and promotion, thereby giving out a wider
    // integer[1][2]. This would print the integer value of a and not the equivalent value of the variable as character.
    // Additionally, it turns an lvalue into an rvalue[3], decays an array to a pointer[4].
    // [1]: http://stackoverflow.com/a/3182557
    // [2]: http://stackoverflow.com/a/3903114
    // [3]: http://stackoverflow.com/a/3903429
    // [4]: http://stackoverflow.com/q/23012070
}
