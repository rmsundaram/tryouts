#include <iostream>
#include <vector>
#include <utility>

/*
http://stackoverflow.com/a/9552880/183120
http://stackoverflow.com/a/13939439/183120

Values can be one three types: lvalue, xvalue and prvalue (leaf nodes).
Others are greater types (generalisations): glvalue and rvalue.

       Expression
        /      \
    glvalue  rvalue
    /    \  /    \
lvalue  xvalue  prvalue

* Named rvalue references are lvalues; those whose addresses can be
  taken are lvalues
* Unnamed rvalue references (result of std::move) are xvalues; its
  life can be lengthened by assigning it to named rvalue reference
  (lvalue), without which the result is implicitly moved
* Remaining objects: temporaries, subobjects thereof, literals,
  etc. are prvalues

It all comes down to xvalues and the need to restrict movement to
exactly and only certain places. Those places are defined by the
rvalue category; prvalues are the implicit moves, and xvalues are the
explicit moves (std::move returns an xvalue).
*/

using namespace std;

struct Obj
{
	Obj() = default;
	Obj(const Obj &)
	{
		cout << "copied" << endl;
	}
};

void steal_and_copy(vector<Obj> &&objects)
{
	cout << "Copy start" << endl;
	vector<Obj> objCopies(objects);
	cout << "Before move" << endl;
	cout << "Object copies count: " << objCopies.size() << endl;
	cout << "Objects count: " << objects.size() << endl;
	objCopies.clear();
	objCopies = move(objects);
	cout << "After move" << endl;
	cout << "Object copies count: " << objCopies.size() << endl;
	cout << "Objects count: " << objects.size() << endl;
}

int main()
{
	vector<Obj> v(4);
	steal_and_copy(move(v));
}
