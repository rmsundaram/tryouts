#include <cmath>
#include <iostream>

using namespace std;

const auto PI = 3.141592654;

long double deg2rad(long double degrees)
{
	return (degrees * PI) / 180;
}

long double operator"" _deg(long double degrees)
{
	return deg2rad(degrees);
}

int main(int argc, char* argv[])
{
	auto angle = (argc == 1) ? 90.0_deg : deg2rad(atof(argv[1]));
	cout << "cos(" << angle << ") = " << cos(angle) << endl;
}
