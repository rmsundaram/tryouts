// Print optimal block size for a path querying the kernel; useful with dd
// https://stackoverflow.com/a/6161892

#include <sys/stat.h>
#include <stdio.h>

int main(int argc, char **argv)
{
  if (argc >= 2) {
    struct stat stats;
    if (!stat(argv[1], &stats))
      printf("%lu\n", stats.st_blksize);
    else
      printf("Invalid file path\n");
  } else
    printf("Usage: bs FILE\n");
}
