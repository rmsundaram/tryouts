#include <lua5.2/lua.hpp>

#include <stdexcept>
#include <iostream>
#include <memory>

// Lua interpreter smart pointer type with custom deleter type as lua_close's type
using LuaInterpreterPtr = std::unique_ptr<lua_State, void (*) (lua_State*)>;

class LuaInterpreter
{
public:
    LuaInterpreter() : spLuaInterpreter(luaL_newstate(), lua_close)
    {
        if (spLuaInterpreter)
        {
            luaL_openlibs(spLuaInterpreter.get());
        }
        else
        {
            throw std::runtime_error("Lua new interpreter state creation failed!");
        }
    }

    void run_script(const char *file_name)
    {
        luaL_dofile(spLuaInterpreter.get(), file_name);
    }

private:
    LuaInterpreterPtr spLuaInterpreter;
};

int main(int argc, char **argv)
{
    if (argc < 2)
        std::cout << "Pass a Lua script file to execute!" << std::endl;
    else
    {
        LuaInterpreter intrerpreter;
        intrerpreter.run_script(argv[1]);
    }
}
