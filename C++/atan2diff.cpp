#include <iostream>
#include <cmath>
#include <complex>

const auto pi = 3.14159265358979323846;
const auto st_angle_by_pi = 180 / pi;
const auto pi_by_st_angle = 1 / st_angle_by_pi;

constexpr float rad_2_deg(float rads)
{
	return st_angle_by_pi * rads;
}

// returns radians taking degrees
constexpr long double operator "" _degs (long double degrees)
{
	return pi_by_st_angle * degrees;
}

int main()
{
	using namespace std;

	complex<float> z;
	cin >> z;
	// since atan takes a single argument instead of a fraction, it cannot differentiate
	// based on the argument's sign; for e.g. an input of (-1, -1) lies in the 3rd quadrant
	// and makes an angle of 135 degrees with the positive X axis, but atan gives 45 since
	// -1 / -1 = 1 / 1 = 1; any point in quadrant I and III (+ve) will be mapped to [0, 90)
	// or [0, π/2) and any point in II and IV (-ve) will be mapped to (-90, 0] or (-π/2, 0]
	// thus the range is (-90, 90) or (-π/2, π/2); atan2 remedies this by taking the input
	// as a fraction and maps it to (-π, π], which can be mapped to [0, 2π) by adding 2π to
	// negative results; or add 2π always and mod by 2π to avoid the check for < 0
	const auto atanrads = atan(z.imag() / z.real());
	cout << "Degrees: " << rad_2_deg(atanrads) << ", Radians = " << atanrads << endl;
	const auto atan2rads = atan2(z.imag(), z.real());
	cout << "Degrees: " << rad_2_deg(atan2rads) << ", Radians = " << atan2rads << endl;
	// additionally, the ordinary arctangent method breaks down when required to produce an
	// angle of ±π/2 (or ±90°). For example, an attempt to find the angle between the x-axis
	// and the vector (0, 1) requires evaluation of arctan(1/0), which fails on division by
	// zero. In contrast, atan2(1, 0) gives the correct answer of π/2.

	// tan is positive in the III quadrant (apart from I) but that shouldn't be confused
	// with inverse tan being negative for a negative x & y coordinate value (a point in
	// the III quadrant); the rotation angle returned by tan inverse will be positive if
	// the angle is in quad. I or II and negative if it is in quad. III or IV and not the
	// tan of that angle, which will always be positive when the point is in quad I and III
	const auto test_angle = 181.0_degs;	// or -179; both give the same +ve result
	cout << tan(test_angle) << endl;
}
