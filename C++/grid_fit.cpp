#include <cmath>
#include <iostream>

/*
This covers all cases (taking into account that D2D's pixel centre
is @ x + 0.5, where x belongs to Z); also when a figure's left and right
passes through two adjacent pixel's centre, the tie-breaking is done by
choosing the left-bottom pixel. In left, right, former is inclusive and 
latter is exclusive i.e. [left, right). All this is for the aliased case,
where enabling a pixel happens only when its centre is covered by the figure.

l -= (n - delta) / 2
r += (n - delta) / 2

where
l, r = left, right
n = pixels_to_shade
delta = abs(r - l)

E.g. cases of pixels_to_shade function

-.5       -1
-1.2      -1
-1.5      -2
-1.4999   -1
-1.6      -2
-1.9      -2
-2.42     -2
-2.501    -3
3.8        4
3.45       3
-0.178    -1
-0.75     -1
-1.345    -1
-1.5      -2
0          1
*/
float pixels_to_shade(float x)
{
    const float y = std::round(std::abs(x));
    return std::copysign((y == 0.0f) ? 1.0f : y, x);
    // copysign can be removed if pixels to be light should always be a positive value
    // which it should be mathematically; but further computations are simple when
    // having it signed

    // case 1
    // left = 2.5, right = 1.3, distance = r - l = -1.2, pixels = -1, delta = 0.2/2 = 0.1
    // left = 2.5 - 0.1 = 2.4, right = 1.3 + 0.1 = 1.4

    // case 2  (same as previous, just that left and right are sorted)
    // left = 1.3, right = 2.5, distance = 1.2, pixels = 1, delta = -0.2/2 = -0.1
    // left = 1.3 + 0.1 = 1.4, right = 2.5 - 0.1 = 2.4

    // case 3
    // left = -1.3, right = -2.5, distance = -1.2, pixels = -1, delta = (-1 - (-1.2)) / 2 = 0.1
    // left = -1.3 - 0.1 = -1.4, right = -2.4
}

void grid_fit(float &left, float &right)
{
    const auto distance = right - left;
    const auto pixels = pixels_to_shade(distance);
    std::cout << "Pixels to shade: " << pixels << '\n';
    const auto delta = (pixels - distance) * 0.5f;
    left -= delta;
    right += delta;
    std::cout << "Grid fitted left, right: " << left << ", " << right << '\n';
}

// given left and right values this returns the number of pixels that will be lit, based on
// D2D rules of enabling pixels only when its centre is covered; this can be used to validate (assert)
// left and right values after fitting them, pixels_shaded should equal abs(pixels_to_shade)
float pixels_shaded(float left, float right)
{
    if(left > right)
        std::swap(left, right);

    const auto delta = left - std::floor(left);
    float n = ((delta != 0.0f) && (delta <= 0.5f)) ? 1.0f : 0.0f;  // != 0.0 to handle inputs like 13, 14
    n += std::floor(right) - std::ceil(left);
    n += (right - std::floor(right) > 0.5f) ? 1.0f : 0.0f;
    return n;
}

int main(int argc, char **argv)
{
    if(argc < 2)
        std::cerr << "insufficient data\nusage: grid_fit <left> <right>";
    else
    {
        auto left = strtof(argv[1], nullptr), right = strtof(argv[2], nullptr);
        grid_fit(left, right);
        std::cout << "Pixels shaded: " << pixels_shaded(left, right) << std::endl;
    }
}
