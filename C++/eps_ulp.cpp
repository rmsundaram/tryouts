#include <iostream>
#include <limits>
#include <cstdint>
#include <cmath>

static_assert(std::numeric_limits<float>::is_iec559,
              "float is not IEEE 754's binary32!");

// http://randomascii.wordpress.com/2012/02/25/
// comparing-floating-point-numbers-2012-edition
unsigned int_dist(float a, float b)
{
    uint32_t i, j;
    memcpy(&i, &a, sizeof(a));
    memcpy(&j, &b, sizeof(b));
    return std::abs(i - j);
}

// this function is from cppreference.com under std::numeric_limits::epsilon's
// example section
template<class T>
typename std::enable_if<!std::numeric_limits<T>::is_integer, bool>::type
almost_equal(T x, T y, int ulp)
{
    // the machine epsilon has to be scaled to the magnitude of the larger value
    // and multiplied by the desired precision in ULPs (units in the last place)
    return std::abs(x-y) <=   std::numeric_limits<T>::epsilon()
                            * std::max(std::abs(x), std::abs(y))
                            * ulp;
}

int main()
{
    std::cout.precision(20);
    // also known as a unit roundoff, is the difference between 1.0 and the next
    // representable value of the given floating-point type; for the definitions
    // of binary precision, machine epsilon and ULP refer Chapter 3 of Numerical
    // Computing with IEEE Floating-Point Arithmetic which follows the
    // definition that the C standard mandates and not other popular one where
    // the epsilon is half of this one. Also see the minifloat workout.
    std::cout << "Machine epsilon = " << std::numeric_limits<float>::epsilon();
    float f = 1.0f;
    uint32_t i;
    memcpy(&i, &f, sizeof(f));
    ++i;
    memcpy(&f, &i, sizeof(f));
    std::cout << "\nInteger Distance (1.0f, float(++int(1.0f)) = "
              << int_dist(f, 1.0f);
    // float difference gives the machine epsilon = ULP for binade 2⁰ = 2^k
    // where k is number of significand digits excluding the hidden bit; for
    // binary32 it is 2⁻²³
    std::cout << "\nFloat difference between 1.0f and next float = " << f - 1.f;
    const auto half_eps = std::numeric_limits<float>::epsilon() / 2.0f;
    std::cout << "\nHalf epsilon = " << half_eps;
    std::cout << "\n1.0f + half epsilon = " << 1.0f + half_eps;
    std::cout << "\n1.0f + epsilon = " << 1.0f + (half_eps * 2.0f);
    std::cout << "\nFloat after 1.0f = " << f << '\n';

    constexpr auto a = 0.9999999f;
	constexpr auto b = 1.0f;
	std::cout << std::boolalpha << (a == b) << '\n';
	// passing 0 to last parameter (ULP) fails
	std::cout << almost_equal(a, b, 1) << '\n';
}
