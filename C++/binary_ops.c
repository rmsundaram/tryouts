#include <stdio.h>
#include <stdlib.h>

void print_binary(unsigned int x)
{
	int i = 0;
	for(i = (sizeof(int) * 8) - 1; i >= 0; --i)
	{
		printf("%i", (x & (1 << i))?1:0);
	}
	printf("\n");
}

int reverse_bits(unsigned int n)
{
	unsigned int rn = 0, i = (sizeof(int) * 8);

	for(; i; --i, n >>= 1)
	{
		rn |= (n & 1);
		if(i > 1) rn <<= 1;
	}

	return rn;
}

void is_power_of_2(int number)
{
	int count = 0;

	/* naive way
	for(;number; number >>= 1)
	{
		count += number & 1;
	}*/

	/* Brian Kernighan way */
	for(count = 0; number; ++count)
		number &= number - 1;

	if(count == 1)
		printf("A power of 2!\n");
	else
		printf("Not a power of 2!\n");
}

void fastest_power_of_2(int number)
{
	if(number && ((number & (number - 1)) == 0))
		printf("A power of 2!\n");
	else
		printf("Not a power of 2!\n");
}

int main(int argc, char *argv[])
{
	int x = 0;

	if(argc > 1)
	{
		x = atoi(argv[1]);
		print_binary(x);

		print_binary(reverse_bits(x));

		is_power_of_2(x);
		fastest_power_of_2(x);
	}

	return 0;
}
