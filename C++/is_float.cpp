// in reply to http://stackoverflow.com/a/23070800/183120
// C++03 trick to find if a given type is floating-point

#include <limits>
#include <iostream>

template <typename T>
struct is_floating_point
{
  static const bool value;
};

template <typename T>
const bool is_floating_point<T>::value = (!std::numeric_limits<T>::is_integer) && std::numeric_limits<T>::is_bounded;

struct Obj
{
};

int main()
{
    std::cout << is_floating_point<float>::value << std::endl;
    std::cout << is_floating_point<int>::value << std::endl;
    std::cout << is_floating_point<Obj>::value << std::endl;
    std::cout << is_floating_point<bool>::value << std::endl;
    std::cout << is_floating_point<double>::value << std::endl;
    std::cout << is_floating_point<long double>::value << std::endl;

    // this would compile since it's an array of size 1
    int Y[is_floating_point<float>::value] = { };
    // this wouldn't compile since it's an array of size 0
    int N[is_floating_point<int>::value] = { };
}
