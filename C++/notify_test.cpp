extern "C" {
#include "libnotify.h"
};
#include <windows.h>

class NotificationLib
{
public:
	NotificationLib() : initialized(false)
	{
		if(!initialized && notify_init("test"))
		{
			initialized = true;
		}
	}

	~NotificationLib()
	{
		if(initialized)
		{
			notify_uninit();
			initialized = false;
		}
	}

private:
	bool initialized;
};

int main()
{
	const char *notify_icon = "gtk-dialog-info";
	const char *notify_summary = "Libnotify Test";
	const char *notify_body = "This is a test body";
	NotificationLib nl;

	NotifyNotification* nn = notify_notification_new(notify_summary, notify_body, notify_icon);
	if(nn)
	{
		// notify_notification_update(nn, notify_summary, notify_body, notify_icon);
		notify_notification_show(nn, NULL);
		Sleep(5000);
	}
}
