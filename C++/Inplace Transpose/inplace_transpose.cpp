#include <iostream>
#include <bitset>

template <typename T, size_t N>
constexpr size_t array_length(const T (&a)[N])
{
	return N;
}

template <typename T, size_t N, size_t Rows, size_t Cols>
void print(const T (&m)[N])
{
	static_assert(N == Rows * Cols, "Invalid array size!");
	for (auto i = 0; i < Rows; ++i)
	{
		for (auto j = 0; j < Cols; ++j)
			std::cout << m[(i * Cols) + j] << "\t";
		std::cout << std::endl;
	}		
}

// http://en.wikipedia.org/wiki/In-place_matrix_transposition
// http://stackoverflow.com/questions/9227747/in-place-transposition-of-a-matrix

template <typename T, size_t N, size_t Rows, size_t Cols>
void transpose(T (&m)[N])
{
	static_assert(N == Rows * Cols, "Invalid array size!");
	// nothing to transpose, should we check Cols too?
	if ((Rows == 1) || (Cols == 1)) return;

	std::bitset<N - 2> transposed;
	// naming it visited will be confusing since from is
	// always visited first and replaced last

	// since 0 and N - 1 remain in the same place always
	for (auto i = 1; i < N - 1; ++i)
	{
		if (!transposed[i - 1])
		{
			auto from = i;
			T moved_value = m[from];
			do
			{
				auto to = (from * Rows) % (N - 1);
				std::swap(moved_value, m[to]);
				transposed[to - 1] = true;
				from = to;
			}
			while (!transposed[i - 1]);
		}
	}
}

int main()
{
// canonical case
	int a[] = { 1, 4, 2, 5,
				6, 8, 7, 3 };
	std::cout << "Before transpose" << std::endl;
	print<int, array_length(a), 4, 2>(a);
	transpose<int, array_length(a), 4, 2>(a);
	std::cout << "After transpose" << std::endl;
	print<int, array_length(a), 2, 4>(a);

/*
	this is the worst case of the above algorithm
	since there's only one cycle starting @ 1 and
	it transposes all elements in that cycle, but
	the for loop above will iterate over all N - 1
	items skipping them since they're already visited
*/
   int b[] = { 1, 2, 8, 7, 5, 10,
				12, 13, 19, 4, 6, 3,
				50, 71, 33, 42, 40, 53 };
	std::cout << "Before transpose" << std::endl;
	print<int, array_length(b), 3, 6>(b);
	transpose<int, array_length(b), 3, 6>(b);
	std::cout << "After transpose" << std::endl;
	print<int, array_length(b), 6, 3>(b);

// trivial case	
	int c[] = { 1, 4, 2, 5 };
	std::cout << "Before transpose" << std::endl;
	print<int, array_length(c), 4, 1>(c);
	transpose<int, array_length(c), 4, 1>(c);
	std::cout << "After transpose" << std::endl;
	print<int, array_length(c), 1, 4>(c);

/*
	a trick would be to never actually transpose
	but read with the indices inverted as though
	transposed, if a member variable flags to "act
	transposed"
*/
}
