<meta charset="utf-8">

# Universal Reference[^fn1]

This is my own digestion of Scott Meyer's [lecture video on Universal Reference](https://isocpp.org/blog/2012/11/universal-references-in-c11-scott-meyers).

When you see `T&&`, it's not always an rvalue reference; when `T` is a deduced type, it's a _universal reference_ (**URef**). Here're the requirements for URef:

1. `T&&`
2. `T` should be deduced

If these requirements aren't met, it's an rvalue reference.  Just because `T&&` is present doesn't mean it's a URef e.g. `void std::vector::push_back(T&&)`; here `T` isn't a URef since there's no type deduction happening; the deduction already happened when the `vector` class template was instantiated and not when the call to `push_back` is resolved.

!!! Note `T&&` is a must
    If you add `const`, `volatile` or `*` you don't have a URef but an RRef e.g. `const T&&` is an RRef.

# Nature

* Lvalue references can be bound to
    + lvalues
    + **`const`** rvalues e.g. `const int &r = 3`
* Rvalue references bind only to rvalues
* Universal references can bind to *anything*
    + lvalues
    + rvalues
    + `const`
    + non-`const`

Rvalue references facilitate move, while a URef may facilitate a move or copy depending on context.  Universal references are possible in four contexts:

1. Function template parameters
2. Auto declarations
3. `typedef` declarations
4. `decltype` expressions

# Behaviour

If initialized with an lvalue, a URef becomes an lvalue; it becomes an rvalue if initialized with an rvalue.

``` c++
std::vector<int> v = { 1, 2, 3 };
auto &&element = v[1];            // element is URef, v[1] returns an LRef, element is LRef
```
    
Now, don't cringe saying "who uses `auto&&` anyways"?  It has its uses e.g. the range-based `for` implementation explained in the standard.

In templates, URefs are essentially forwarding references e.g. `emplace_back(Args&& ... args)`; here `Args&&` is a collection of deduced `T&&`s.

URefs and overloading together is almost always a mistake, since the URef overload would bind to everything, leaving other overloads no chance.  Normally, without type deduction, one writes a `const T&` for copying and `T&&` for moving, like the two variants of `push_back`, but again here the type is deduced much before the function call happens.  But it's a mistake if `T` is deduced during overload resolution.

!!! Note Function Overloading
    Overloading on RRef & LRef — typically OK.  _Overloading on URef — typically wrong_.

# Value Category

With an lvalue reference the parameter is just used; OTOH an rvalue reference param is almost always wrapped with a `std::move` call.  With a universal reference, typically the parameter is `std::forward`ed.  The *rvalueness* tells the type system that one can steal the object contents safely.

Type and value category are two different, independent aspects of an expression.  Anything with a name is actually an lvalue, but its type may be an rvalue reference e.g. an integer object's type is `int` but one can have an lvalue or an rvalue `int`.

``` c++
int lval = 5;    // type: int; value category: lvalue
```

Integer returned from a function is of type `int` but is of rvalue categeory.  Whether something is an lvalue or rvalue is independent of its type:

``` c++
void f(T&& param)
```

Here the type of `param` is rvalue reference to `T` but `param` is an lvalue.  So the original argument passed to match `param` would've been an rvalue since only those can bind to rvalue references but now `param` is an lvalue due to the name.  To make it an rvalue, knowing that it was the original object's status, we use `std::move`[^fn2].

# Reference Collapsing

References to references (like pointer to pointer) is disallowed to the programmer but occurs during type deduction and evaluation.  For a function template taking a URef, there are special rules.  If you pass

1. an lvalue, the `T` becomes `T&`
2. an rvalue, the `T` becomes `T`

e.g. passing `Widget w` to `void f(T&& param)`, `T` becomes `Widget&`, while passing `std::move(w)`, `T` is `Widget` only, not `Widget&&`.

Thus lvalues yeild a reference-to-reference.  The reference collapsing rules are then used to deduce the resultant type:

    T&  &  → T&
    T&& &  → T&
    T&  && → T&
    T&& && → T&&

In other words, when there's an lvalue reference, the result is an lvalue, else (when both are rvalue references) it's an rvalue.

# Perfect Forwarding

For pass-through functions taking URefs, the URef is wrapped in `std::forward` for two things:

1. if whatever was passed in was an lvalue, transport it to wherever it's going to be used as an lvalue
2. if whatever was passed was an rvalue, transport it by turning it back into an rvalue (so that people can continue to use move semantics on it).

Perfect forwarding means if you've a URef and you wish to forward (pass) it to some other function as either an lvalue if it was originally an lvalue or an rvalue if it was an rvalue, then `std::forward` lets you do it.  Perfect forwarding is a way to recover from an lvalue — because all (named) parameters are lvalues — whether it was originally an rvalue.  The syntax very strongly matters: only `T&&` and `T` should be deduced. `std::forward` preserves the lvalues-ness or rvalue-ness of its argument.

Reference collapsing rules only come into the picture if you've a URef.  For `typedef`s, we should apply the right reference collapsing rule when there's more than one `&` to get the effective type.  In templates and auto type deduction, references become non-references, before lvalue/rvalue analysis is done.  If the type is a reference, you strip-off the reference and then add it back if object passed was an lvalue.

1. `decltype(var)` - the declared type
2. `decltype(non-id lvalue expr)` - LRef to expression's type `T&`
3. `decltype(non-id rvalue expr)` - the expression's type `T`

Example

``` c++
Widget w;
decltype(w)&&   r1;    // Widget&&
decltype((w))&& r2;    // Widget& due to rule 2 and reference collapsing
```

# Aside: Type, Value Category and Binding

_**Note**: Not part of Scott Meyer’s presentation._

Objects have a type.  Expressions have a (non-reference, without cv) _type_ and _value category_.

``` c++
int i = 0;                 // i is an object of type int; rhs expressions's type int, value category rvalue
int &j = i;                // r is an lvalue-reference to int; rhs expression's type int, value category lvalue
int &&k = std::move(i);    // k is an rvalue-reference to int; rhs expression's type int, value categroy xvalue

const int foo2 = 7;        // foo2 is a const int; foo2 expression value categroy lvalue
```

!!! Warning Type vs Value Category
    Terms with _-reference_ suffix like _lvalue reference_, _rvalue reference_, … means an object’s _type_; the terms _lvalue_, _rvalue_, … (without the suffix) denotes an expression’s value category. They’re two completely different concepts.

Expressions only have a non-reference type and a value category i.e. one of _lvalue_, _prvalue_ or _xvalue_ — the primary categories. _glvalue_ (lvalue + xvalue) and _rvalue_ (prvalue + xvalue) are mixed categories used for better understanding but actual expressions will be having one of the primary value categories only.

## Identity and Movability

An expression can have

* **Identity**: expression’s value has a name/pointer/reference using which we can take the _expression_’s address with `&`
* **Movability**: expression whose value can be moved

!!! Note Broad Categories
    _glvalues_ have identity. _rvalues_ are movable.

Conversely, prvalues don’t have an identity (not all rvalues, since xvalues do have identity), lvalues are not movable (not glvalues, since xvalues are movable).

## Primary Value Categories

### lvalue category

1. Expressions having an identity i.e. it has a name or memory location; you can take the address of the _expression_ using `&`.

``` c++
Widget* button = new Widget;
// button is an lvalue since &button gives the memory location of button, its type is Widget*
// *button is also an lvalue too; it's an expression denoting something that doesn't have a name,
// but you can take its address `&*button`

int&& foo;    // foo's type: rvalue reference to int; expression's value categoy: lvalue
```

2. Expressions that are NOT movable.

### prvalue category

1. Expressions having no identity i.e. no name or memory location; you can’t take its address.

``` c++
str.substr(1, 3)  // an expression returning a temporary and hence its address can't be taken
int i = 7;    // 7 is a constant, its address cannot be taken; though 7 is rvalue, not prvalue
```

2. Expressions that are movable.

### xvalue category

Expressions having both identity and movability.

``` c++
Circle c;
std::move(c)  // what's returned from move is an xvalue
```

## Binding Rules

1. **lvalue reference** (`&`) binds to an lvalue expression, not an rvalue expression (as caller cannot observe modifications to temporary)
    - callee can modify data
    - caller can observe modifications
2. **lvalue const reference** (`const &`) can bind to an lvalue or rvalue expression (latter prolongs temporary’s life)
    - caller cannot modify data
3. **rvalue reference** (`&&`) binds to an rvalue expression (latter prolongs temporary’s life), not an lvalue expression (as caller can observe modifications)
    - callee can modify
    - caller cannot observe modifications
4. cv-qualifiers don’t apply to rvalue references since that defies the whole concept

During overload resolution, an rvalue _prefers_ to bind to rvalue reference over lvalue const reference.

Also, a variable value of type `T` (no reference involved, but value) can bind to both an lvalue and rvalue expression, depending on which, the right `T` constructor (copy or move) would be called.

See [binding.cpp](./binding.cpp) for a comprehensive example.

# See Also

1. [Rvalue References Explained](http://thbecker.net/articles/rvalue_references/section_01.html)
2. [What does `T&&` (double ampersand) mean in C++11?](http://stackoverflow.com/q/5481539)
3. [Is pass-by-value a reasonable default in C++11?](http://stackoverflow.com/q/7592630)
4. [What are rvalues, lvalues, xvalues, glvalues, and prvalues?](http://stackoverflow.com/q/3601602)
5. [Is returning by rvalue reference more efficient?](http://stackoverflow.com/q/1116641)
6. [Should I write constructors using rvalues for `std::string`?](http://stackoverflow.com/q/10836221)
7. [Can I typically/always use std::forward instead of `std::move`?](http://stackoverflow.com/q/13219484)
8. [Vector construction and move sematics in `std::vector`](http://stackoverflow.com/q/4986673)
9. [`push_back` vs `emplace_back`](http://stackoverflow.com/q/4303513)
10. [Expressions and value categories](https://youtu.be/wkWtRDrjEH4)
11. [Value categories, and references to them](https://docs.microsoft.com/en-us/windows/uwp/cpp-and-winrt-apis/cpp-value-categories#rvalue-references-and-reference-binding-rules)

<link rel="stylesheet" href="https://morgan3d.github.io/markdeep/latest/apidoc.css?">
<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'short', inlineCodeLang: 'c++' };</script>

[^fn1]: official name _forwarding reference_ since C++14.

[^fn2]: Exactly equivalent to `static_cast` to an rvalue reference type.
