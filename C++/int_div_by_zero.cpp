#include <iostream>

int main()
{
    float x = 12.0f, y = 0.0f;
    std::cout << x / y << std::endl;

 	constexpr int n = 12U, d = 0U;
	auto q = n / d;
	// this will crash without throwing any C++ exception since integral
	// division by zero is undefined and at an OS level it'll raise a
	// SIGFPE. This is unlike (IEEE 754) floating point arithmetic where
	// quotient is set to ±INF.

    // Unix signal, Wikipedia: The SIGFPE signal is sent to a process when it
    // executes an erroneous arithmetic operation, such as division by zero (the
    // name "FPE", standing for floating-point exception, is a misnomer as the
    // signal covers integer-arithmetic errors as well).

	std::cout << "Exit successful" << std::endl;
}
