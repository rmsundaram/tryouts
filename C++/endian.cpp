#include <cstdint>
#include <iostream>

#if __cplusplus >= 202002L && __has_include(<bit>)
#  include <bit>
#endif

void neutral_read();

int main()
{
    // http://en.wikipedia.org/wiki/Endianness
    // http://stackoverflow.com/a/7349839/183120; "little endian" - 'little' is associated to the address and the
    // 'endian' is associated to the digits i.e. ending digit (LSB) will be in the lower address.
    // http://stackoverflow.com/questions/23583995/why-bytes-of-one-word-has-opposite-order-in-binary-files
    uint32_t constexpr d{0x0D0C0B0A};
    /*
       LITTLE ENDIAN                                      BIG ENDIAN
       Writing order of 0x0D0C0B0A
          +----+----+----+----+                       +----+----+----+----+
          | 0D | 0C | 0B | 0A |                       | 0D | 0C | 0B | 0A |
          +----+----+----+----+                       +----+----+----+----+
            |     |    |    |                           |     |    |    \
            +-----|----|----|---+                       |     |    |     \
           +------|----|----+   |                       |     |    |      \
           |      +----|--+     |                       |     |    |       \
           |    +------+  |     |                       |     |    |        \
           |    |         |     |                       |     |    |         \
       +------+------+------+------+               +------+------+------+------+
       | 1000 | 1001 | 1002 | 1002 |               | 1000 | 1001 | 1002 | 1003 |
       +------+------+------+------+               +------+------+------+------+

       The little-endian system has the property that gives itself for an optimization which although is rarely used
       directly by high-level programmers, it is often employed by code optimizers as well as by assembly language
       programmers. Say the above 32-bit value starts at 1000, reading one, two or 4 bytes requires no offsetting of
       the base address: READ(1000, len) while for big-endian one needs to offset from the base address thus:
       READ(1000 + max_len - len, len) e.g. one byte is read as READ(1000 + 3, 1), for two bytes it would be
       READ(1000 + 2, 2), etc. This addition can be avoided.

       If you are writing in a western language the hex value 0x0A0B0C0D you are writing the bytes from left to right,
       you are implicitly writing Big-Endian style. 0x0A at 0, 0x0B at 1, 0x0C at 2, 0x0D at 3. On the other hand the
       output of memory is normally also printed out bytewise from left to right, first memory address 0, then memory
       address 1, then memory address 2, then memory address 3. So on a Big-Endian system when you write a 32-bit value
       (from a register) to an address in memory and after that output the memory, you "see what you have written"
       (because you are using the left to right coordinate system for the output of values in registers as well as the
       output of memory). However on a Little-Endian system the logical 0 address of a value in a register (for 8-bit,
       16-bit and 32-bit) is the least significant byte, the one to the right. 0x0D at 0, 0x0C at 1, 0x0B at 2, 0x0A
       at 3. If you write a 32 bit register value to a memory location on a Little-Endian system and after that output
       the memory location (with growing addresses from left to right), then the output of the memory will appear
       reversed (byte-swapped).

       Memory aid: Little Endian - The Least significant byte goes into the Lowest value slot.

       http://www.cs.umd.edu/class/sum2003/cmsc311/Notes/Data/endian.html
       Endianness only applies to byte-addressing and makes no sense when the value is in a register. If you have a
       32-bit register storing a 32-bit value, it makes no sense to talk about endianness. The register is neither
       big-endian nor little-endian. It's just a register holding a 32-bit value. The rightmost bit is the least
       significant bit, and the leftmost bit is the most significant bit.
     */
    std::cout << ((*reinterpret_cast<unsigned char const*>(&d) == 0x0A) ? "Little-endian" : "Big-endian") << '\n';

#ifdef __cpp_lib_endian  // C++20
    std::cout << (std::endian::native == std::endian::little ? "Little-endian" : "Big-endian") << "\n\n";
#else
	unsigned int constexpr i = 1;
    // Well-defined: aliasing is allowed when the type is char* or unsigned char*.
    // http://stackoverflow.com/a/7005988/183120
    std::cout << (*((char*) &i) == 1 ? "Little-endian" : "Big-endian") << "\n\n";
#endif
    /*
	 * When writing portable code, usually htonl and ntohl (or equivalent) may be used; little and mid-endian platforms
	 * would implement them to do the appropriate conversion, while a big-endian platform's implementation would be a
	 * no-op since network order is the host's order: big-endian. Without worring about the target platform the code is
	 * going to be compiled on, the programmer may use these functions in network programming without knowing the
	 * context. However, when dealing with situations having a set byte order, say BMP, one needs to know the context
	 * on where byte swapping is needed to convert to a different endianness. On a big-endian machine, decoding an
	 * integer in a BMP header needs byte swapping, but using neither htonl or ntohl wouldn't help here, since both
	 * would be no-ops; C++20’s std::byteswap should do; compiler intrinsics are another option.
	 * http://stackoverflow.com/a/105339/183120
	 * http://stackoverflow.com/questions/105252/how-do-i-convert-between-big-endian-and-little-endian-values-in-c/
	 * 105339#comment13284638_105339
	 * 105339#comment45062625_105339
	 * 105339#comment13284791_105410
     */

    unsigned int constexpr x = 0xCC123456;	//0xCC = 204, 0x12 = 18, 0x34 = 52, 0x56 = 86
	unsigned char const *y = reinterpret_cast<unsigned char const *>(&x);
    std::cout << std::hex;
	std::cout << "*(y + 0) = 0x" << (int) *y << '\n';
	std::cout << "*(y + 1) = 0x" << (int) *(y + 1) << '\n';
	std::cout << "*(y + 2) = 0x" << (int) *(y + 2) << '\n';
	std::cout << "*(y + 3) = 0x" << (int) *(y + 3) << '\n' << std::dec;

    neutral_read();

	return 0;
}

/*
 * using union's for type-punning leads to undefined behaviour in C++
 * [1]: http://stackoverflow.com/q/2310483
 * [2]: http://stackoverflow.com/q/6725809
 */
void endian_test()
{
	union
	{
		uint16_t i;
		char ch[2];
	}
    num = {0x0102};

    std::cout << std::hex << num.i << std::dec << std::endl;
}

/*
 * ROB PIKE'S BYTE ORDER FALLACY (http://commandcenter.blogspot.fr/2012/04/byte-order-fallacy.html)
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * The byte-ordering of the processing machine never matters, only the data's byte-ordering does. We know that
 * byte-ordering matters only when reading from the memory; on registers and on paper data is always in big-endian
 * ordering (if your language's writing direction is from left to right). Strictly speaking, endianness inside a
 * register makes no sense since endianness describes if the byte order is from low to high memory address or from
 * high to low memory address. Registers are not byte addressable so there is no low or high address within a
 * register; only bits from MSB to LSB (C gives this abstraction, even if in reality it's any different).
 *
 * E.g.
 * Data needed to be represented = 0xAABBCCDD
 * In memory it is in big-endian ordering:
 * 2001 2002 2003 2004
 * 0xAA 0xBB 0xCC 0xDD
 *
 * Now reading it as int on a big-endian would result in the correct value read into the register but on a
 * little-endian it'd end up in the register as 0xDDCCBBAA. However, reading it as an array of 4 chars neutralizes
 * the issue, leading to the same result on both machines: 4 chars with 4 bytes of data read in the increasing order
 * from memory.
 *
 * b[0] = 0xAA
 * b[1] = 0xBB
 * b[2] = 0xCC
 * b[3] = 0xDD
 *
 * The order in which the chars are filled is the same as the data's order in memory/stream and NOT on the machine's
 * byte ordering. Now assembling the integer back with binary operators would be portable too. On both machines, the
 * chars would be promoted (before shifting) to ints where bits [31, 8] would be filled with zeros and the actual
 * value would be in the lower-order bits [7, 0].
 *
 * int i = (c[0] >> 0) | (c[1] >> 8) | (c[2] >> 16) | (c[3] >> 24);
 *
 * This works on any-endian machine the same. The issue is primarily diffused when the data is read as chars
 * instead of an int directly, since endianness doesn't apply to chars. Once we have the characters read from the
 * memory we get neutrality since endianness doesn't apply to registers. It's just a matter of assembling the value
 * from these characters using the bit-shift operators; within the register the data always spans from left to right
 * b₃₁ … b₀ (MSB to LSB) irrespective of the endianness all bit operations work atop this system.
 */

void neutral_read()
{
    // data in big-endian format
    unsigned char constexpr b[4] = { 0xAA, 0xBB, 0xCC, 0xDD };
    int constexpr i = (b[0] << 24) | (b[1] << 16) | (b[2] << 8) | (b[3] << 0);
    std::cout << "0x" << std::hex << i << '\n';

    // data in little-endian format
    unsigned char constexpr l[4] = { 0xDD, 0xCC, 0xBB, 0xAA };
    int constexpr j = (l[0] << 0) | (l[1] << 8) | (l[2] << 16) | (l[3] << 24);
    std::cout << "0x" << std::hex << j << '\n';
}
