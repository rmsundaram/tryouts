#include <iostream>
#include <functional>
#include <memory>

struct simple
{
	simple()
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}

	void work()
	{
		std::cout << "simple value = " << x << std::endl;
	}

	~simple()
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	}

	int x = 0;
};

std::function<void()>
call_me()
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;

	std::shared_ptr<simple> spA = std::make_shared<simple>();
	// capturing it by value to make sure it lives inside lambda
	auto func = [spA]()
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		spA->work();
	};
	return func;
}

int main()
{
	auto fn = call_me();
	fn();
	auto str = R"(She said she'll come with me, I said, "of course".)";
	std::cout << str << std::endl;

    // to convert a lambda to equivalent function pointer use unary + operator
    // e.g. +[]{} https://stackoverflow.com/q/18889028/183120
}
