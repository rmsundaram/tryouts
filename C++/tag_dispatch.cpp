#include <type_traits>
#include <iostream>

// http://stackoverflow.com/a/15598994/183120
// Andy explains 3 ways of doing function template specialization
// 1 and 3 using tag dispatching and 2 using SFINE, more specifically
// 1 is tag dispatching by instance (see: tag_dispatch_instance.cpp)
// 3 is tag dispatching by type (see: tag_dispatch_type.cpp)
// this is another example of tag dispatching by instance, where a dummy of type std::integral_constant is created
// for overloading based on that type's variants - std::true_type and std::false_type

// C++17 allows static if constexpr, hence this trick is no longer needed!

#if __cplusplus >= 201703L

    template <typename T>
    void func(T t)
    {
        if constexpr(std::is_integral_v<T>)
            std::cout << "whole" << std::endl;
        else
            std::cout << "rational" << std::endl;
    }

#else

    template <typename T>
    void func(T t, std::true_type)
    {
    	std::cout << "whole" << std::endl;
    }

    template <typename T>
    void func(T t, const std::false_type&)	// although this is possible too, pass by value looks/is better
    {
    	std::cout << "rational" << std::endl;
    }

    template <typename T>
    void func(T t)
    {
        func(t, typename std::is_integral<T>::type());
    }

#endif // C++17 and above

int main()
{
	func(1);
	func(1.2);
}
