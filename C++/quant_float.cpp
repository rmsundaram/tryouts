// A better way to normalize is to divide by n[1]. While for lerp it’d be to
// divide by (n − 1) for n steps e.g. for 5 steps, we need to go over
// 0/4, 1/4, 2/4, 3/4 and 4/4.
// [1]: https://stackoverflow.com/q/599976

#include <iostream>
#include <cstdint>
#include <limits>
#include <cstdlib>

template <typename T>
float equiFloat(T v){
    return (static_cast<float>(v) / std::numeric_limits<T>::max());
}

template <typename T>
T quantUnityFloat(float f) {
    return static_cast<T>(f * std::numeric_limits<T>::max());
}

int main(int argc, char** argv) {
    if (argc >= 2) {
        float f = std::strtof(argv[1], nullptr);
        uint8_t v = quantUnityFloat<uint8_t>(f);
        // promote to int for printing a numeric value
        std::cout << +v << "\t --> \t";
        std::cout << equiFloat(v) << '\n';
    }
}
