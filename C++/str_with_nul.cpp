#include <iostream>
#include <string>
#include <cstring>

// C++ doesn't care about null terminators for strings.
// They are just used for C compatibility.
// https://stackoverflow.com/q/24453128/183120

int main()
{
    // std::string initialized with C-style string literal that
    // has a terminating \0 NUL character
    std::string s = "hello world";
    std::cout << s << '\t'
              << s.size() << '\n';

    //  insert a NUL character but not terminating
    s[5] = '\0';

    // size still same and printing works correctly
    std::cout << s << '\t'
              << s.size() << "\n\n";

    // C-style accessing: incorrect size and print
    // https://stackoverflow.com/q/2524611/183120
    printf("%s\t%zu\n", s.c_str(), strlen(s.c_str()));
}
