#include <stdio.h>
#include <windows.h>

struct _NotifyNotification
{
	char summary[10];
	char body[100];
	int icon;
};

typedef struct _NotifyNotification NotifyNotification;

int main()
{
	NotifyNotification a = {{0}}, b = {"summary", "body", 1};
	errno_t err = memcpy_s(&a, sizeof(NotifyNotification), &b, sizeof(NotifyNotification));
	/* easier and more readable alternative a = b; */
	printf("%d, %s", err, a.summary);
	
	return 0;
}