#ifndef __SDKSMARTPOINTER__
#define __SDKSMARTPOINTER__

template<typename T>
class SDKSmartPointer 
{
public:
    SDKSmartPointer() : pointer(NULL)
	{
	}

	SDKSmartPointer(T* ptr) : pointer(ptr)
	{
		if(pointer)
		{
			pointer->__refCount++;
		}
	}

	SDKSmartPointer(const SDKSmartPointer& other) : pointer(other.pointer)
	{
		if(pointer)
		{
			pointer->__refCount++;
		}
	}
    
    ~SDKSmartPointer()
	{
		if(pointer)
		{
			pointer->__refCount--;
			if(pointer->__refCount==0) {
				delete pointer;
			}
		}
	}

    SDKSmartPointer& operator = ( const SDKSmartPointer& other )
    {
        if( pointer == other.pointer )
		{
            return *this;
		}

        T* tmp = pointer;
        pointer = other.pointer;
		if(pointer)
		{
			pointer->__refCount++;
		}
        if(tmp)
		{
			tmp->__refCount--;
			if(tmp->__refCount==0) {
				delete tmp;
			}
		}
        return *this;
    }

    SDKSmartPointer& operator = ( T* ptr )
    {
        if( pointer == ptr )
		{
            return *this;
		}

        T* tmp = pointer;
        pointer = ptr;
		if(pointer)
		{
			pointer->__refCount++;
		}
        if(tmp)
		{
			tmp->__refCount--;
			if(tmp->__refCount==0) {
				delete tmp;
			}
		}
        return *this;
    }

    bool operator == ( const SDKSmartPointer& other ) const 
    {
		return pointer==other.pointer;
	}
    
	bool operator != ( const SDKSmartPointer& other ) const
    {
		return pointer!=other.pointer;
	}
    
    bool operator ! () const
	{
		return pointer==NULL;
	}

    bool operator == ( const T* other ) const
	{
		return pointer==other;
	}
    
	bool operator != ( const T* other ) const
	{
		return pointer!=other;
	}
    
    T* operator->()
	{
		return pointer;
	}

	const T* operator->() const
	{
		return pointer;
	}
    
	T& operator*()        
    {
		return *pointer;
	}

    const T& operator*() const  
    {
		return *pointer;
	}
    
    T* get()
	{
		return pointer;
	}

private:
    T* pointer;
};

#endif
