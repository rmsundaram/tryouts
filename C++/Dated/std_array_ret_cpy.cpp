#include <array>
#include <iostream>
#include <type_traits>

using namespace std;

struct SimpleDebugger
{
    SimpleDebugger(int val = 0) : x(val) {
        cout << "created" << endl;
    }

    SimpleDebugger(const SimpleDebugger &that) : x(that.x) {
        cout << "copied" << endl;
    }

    SimpleDebugger& operator=(const SimpleDebugger &rhs)
    {
	if (this != &rhs)
	{
		x = rhs.x;
		std::cout << "assigned" << endl;
	}
	return *this;
    }

    ~SimpleDebugger() {
        cout << "killed!" << endl;
    }

    int getX() const {
        return x;
    }

    void setX(int val) {
        x = val;
    }

private:
    int x;
};

array<SimpleDebugger, 3> getInts(int i)
{
    array<SimpleDebugger, 3> a;
    a[0].setX(i);
    a[1].setX(i + 1);
    a[2].setX(i + 2);
    cout << "exiting getInts" << endl;
    return a;
}

using SimpleDebugger3ElemArray = SimpleDebugger[3];
// getIntsArray returns a pointer to an array of 3 SDs i.e.
// SimpleDebugger (*getIntsArray(int i)) [3] {
SimpleDebugger3ElemArray* getIntsArray(int i) {
    SimpleDebugger3ElemArray *sd = new SimpleDebugger3ElemArray[1];
    (*sd)[0].setX(i);
    (*sd)[1].setX(i + 1);
    (*sd)[2].setX(i + 2);
    cout << "exiting getIntsArray" << endl;
    return sd;
}

ostream& operator << (ostream& os, const SimpleDebugger &sd) {
    return (cout << sd.getX());
}

int main() {
    auto x = getInts(5);
    cout << "std::array = " << x[0] << x[1] << x[2] << endl;
    auto y = getIntsArray(8);
    cout << "Raw array = " << (*y)[0] << (*y)[1] << (*y)[2] << endl;
    delete [] y;

    array<SimpleDebugger, 3> a;
    a[0].setX(10);
    a[1].setX(11);
    a[2].setX(12);
    array<SimpleDebugger, 3> b;
    b[0].setX(13);
    b[1].setX(14);
    b[2].setX(15);
    a = b;
    cout << a[0] << endl;
    {
    	array<SimpleDebugger, 3> c;
	swap(a, c);
    }
    cout << a[0] << endl;
}

