#include <stdio.h>
#include <vector>

struct myRect
{
    long a, b, c, d;
};

int main()
{
	using std::vector;

	vector<int> *ptr = NULL;
	ptr = new vector<int>();

	ptr->reserve(5);

	ptr->push_back(1);
	ptr->push_back(2);
	/*ptr->push_back(3);
	ptr->push_back(4);
	ptr->push_back(5);*/

	// capacity increases to 10 from 5 I.e. doubled
	ptr->push_back(6);

    ptr->erase(ptr->begin());

	printf("Element At (0): %d\n", (*ptr)[0]);
	printf("Number of Elements: %d\n", ptr->size());
	printf("Capacity: %d\n", ptr->capacity());
	printf("Max Size: %d\n", ptr->max_size());

	delete ptr;
	ptr = NULL;
	
	myRect a;
	memset(&a, -1, sizeof(myRect));
	
	printf("%ld, %ld, %ld, %ld\n", a.a, a.b, a.c, a.d);

	return 0;
}
