#include <windows.h>
#include <tchar.h>
#include <strsafe.h>
#include <iostream>

int main()
{
	WIN32_FIND_DATA ffd = {0};
	HANDLE hFind = NULL, hReportCSVFile = NULL;

	if(INVALID_HANDLE_VALUE != (hFind = FindFirstFile(TEXT("*"), &ffd)))
	{
		do
		{		
			std::cout << ffd.cFileName;
			std::cout << std::endl;
			
		} while(FindNextFile(hFind, &ffd));
	}
	
	if(INVALID_HANDLE_VALUE != (hReportCSVFile = CreateFile(TEXT("TestReport.csv"), GENERIC_WRITE, FILE_SHARE_READ, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL)))
	{
		TCHAR outString[256] = TEXT("");
		DWORD bytesWritten = 0;

		int x = _stprintf_s(outString, 256, TEXT("%s"), TEXT("hehe!"));
		std::cout << outString << "\t" << x << std::endl;

		WriteFile(hReportCSVFile, outString, x, &bytesWritten, NULL);
		CloseHandle(hReportCSVFile);
	}
}
