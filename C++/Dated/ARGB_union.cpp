#include <stdint.h>
#include <stdio.h>

union ARGB
{
    uint32_t argbData;
    struct componentsTag
    {
        uint8_t b;
        uint8_t g;
        uint8_t r;
        uint8_t a;
    } components;
};

typedef struct abcd
{
    uint8_t b:5;
    uint8_t g:6;
    uint8_t r:5;
};

union RGB565
{
    uint16_t rgbData;
    struct componentsTag
    {
        unsigned short b:5;
        unsigned short g:6;
        unsigned short r:5;
    } components;
};

typedef union OCTET  
{
   unsigned char BYTE;
   struct
   {
      unsigned char  B7:1;
      unsigned char  B6:1;
      unsigned char  B5:1;
      unsigned char  B4:1;
      unsigned char  B3:1;
      unsigned char  B2:1;
      unsigned char  B1:1;
      unsigned char  B0:1;  
   }  BIT;
};

int main()
{
    ARGB a = {0};
    a.argbData = 0x01F81CFF;
    printf("%d\t %d\t %d\t %d\t", a.components.a, a.components.r, a.components.g, a.components.b);
    printf("%x\n", a.argbData);
    
    RGB565 b = {0};
    b.rgbData = 0x1F36;
    printf("%d\t %d\t %d\t", b.components.r, b.components.g, b.components.b);
    printf("%x\n", b.rgbData);
    OCTET c;
    abcd d;
    printf("%d, %d, %d, %d\n", sizeof(a), sizeof(b), sizeof(c), sizeof(d));

    return 0;
}
