void pixel_at(BYTE *data, UINT x, UINT y, UINT stride, unsigned char (*pixel)[4], unsigned char bytesPerPixel)
{
    auto start = data + (y * stride) + (x * bytesPerPixel);
    std::copy(start, start + bytesPerPixel, *pixel);
}

unsigned int max_difference(const unsigned char (&a)[4], const unsigned char (&b)[4])
{
    int diff[] = { abs(a[0] - b[0]), abs(a[1] - b[1]), abs(a[2] - b[2]), abs(a[3] - b[3]) };
    return static_cast<unsigned int>(*std::max_element(diff, diff + 4));
}

bool process_pixel(bool *colour1Found,
                   bool *colour2Found,
                   unsigned char (*colour1)[4],
                   unsigned char (*colour2)[4],
                   const unsigned char (&pixel)[4],
                   unsigned char bytesPerPixel)
{
    const unsigned char allowed_max_diff = 38;  // 15 % of 255
    const unsigned char allowed_min_diff = 217; // 85 % of 255
    bool res = true;
    if (!*colour1Found)
    {
        std::copy(pixel, pixel + bytesPerPixel, *colour1);
        *colour1Found = true;
    }
    else if(!*colour2Found)
    {
        const auto diff = max_difference(*colour1, pixel);
        // make sure this pixel is far off from colour 1
        if (diff >= allowed_min_diff)
        {
            std::copy(pixel, pixel + bytesPerPixel, *colour2);
            *colour2Found = true;
        }
        // if it's not far off or real close to colour 1
        // then it's deviant, hence no barcode
        else if(!(diff <= allowed_max_diff))
        {
            res = false;
        }
    }
    else
    {
        const auto col1Diff = max_difference(*colour1, pixel);
        if (col1Diff > allowed_max_diff)
        {
            const auto col2Diff = max_difference(*colour2, pixel);
            if (col2Diff > allowed_max_diff)
            {
                res = false;
            }
        }
    }
    return res;
}

struct WICBITMAP_DATA
{
    WICBITMAP_DATA() {}

    WRL::ComPtr<IWICBitmapLock> spBitmapLock;
    UINT uWidth;
    UINT uHeight;
    UINT cbStride;
    UINT cbBufferSize;
    BYTE *pImageData;
};

void sniff_barcode(shared_ptr<WICBITMAP_DATA> &spData, unsigned char id)
{
    // below DDA algorithm inspects pixels along the two lines of an image
    // at every step it analyses 4 pixels
    int x0 = 0, x1 = spData->uWidth - 1, y0 = 0, y1 = spData->uHeight - 1;
    // instead of checking at the end if width and height are both > 2, it could
    // have been made an early return but the algorithm wouldn't be generic, so
    // it was written without special caseing (the most trivial case of 1x1 image
    // would work too) and then early return was later instered into the actual code
    // handed to the test team
    auto diffX = 0, diffY = 0;
    if (spData->uWidth >= spData->uHeight)
    {
        diffX = 1;
        const div_t res = div(static_cast<int>(spData->uWidth), static_cast<int>(spData->uHeight));
        diffY = res.quot + ((static_cast<unsigned>((res.rem * 2)) >=  spData->uHeight) ? 1 : 0);
    }
    else
    {
        diffY = 1;
        const div_t res = div(static_cast<int>(spData->uHeight), static_cast<int>(spData->uWidth));
        diffX = res.quot + ((static_cast<unsigned>((res.rem * 2)) >=  spData->uWidth) ? 1 : 0);
    }

    UINT bufferSize, stride;
    BYTE *data;
    spData->spBitmapLock->GetDataPointer(&bufferSize, &data);
    spData->spBitmapLock->GetStride(&stride);
    const unsigned char bytesPerPixel = (id == 0) ? 1u : 4u;

    bool colour1Found = false, colour2Found = false, res;
    unsigned char a[4] = {}, b[4] = {}, pixel[4] = {};
    int i = diffX, j = diffY;
    do
    {
        // check pixels
        pixel_at(data, x0, y0, stride, &pixel, bytesPerPixel);
        res = process_pixel(&colour1Found, &colour2Found, &a, &b, pixel, bytesPerPixel);
        if (!res) return;
        if (y0 != y1)
        {
            pixel_at(data, x0, y1, stride, &pixel, bytesPerPixel);
            res = process_pixel(&colour1Found, &colour2Found, &a, &b, pixel, bytesPerPixel);
            if (!res) return;
        }
        if (x0 != x1)
        {
            pixel_at(data, x1, y0, stride, &pixel, bytesPerPixel);
            res = process_pixel(&colour1Found, &colour2Found, &a, &b, pixel, bytesPerPixel);
            if (!res) return;
            if (y0 != y1)
            {
                pixel_at(data, x1, y1, stride, &pixel, bytesPerPixel);
                res = process_pixel(&colour1Found, &colour2Found, &a, &b, pixel, bytesPerPixel);
                if (!res) return;
            }
        }

        // update pointers
        --i, --j;
        if (i == 0)
        {
            ++x0;
            --x1;
            i = diffX;
        }
        if (j == 0)
        {
            ++y0;
            --y1;
            j = diffY;
        }
    } while ((x0 <= x1) && (y0 <= y1));

    // if it reaches here, we mostly have a barcode
    // but check if it's not a single line image (strip)
    if ((spData->uWidth > 1u) && (spData->uHeight > 1u))
    {
        const char *colourSpaces[] = { "A8" , "RGBA", "CMYK" };
        std::stringstream ss;
        ss << colourSpaces[id] << " image may be a bar code!";
        std::string errStr = ss.str();
        throw CException(errStr);
    }
}
