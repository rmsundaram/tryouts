// http://stackoverflow.com/q/2161462/183120

#include <iostream>
using std::cout;
using std::endl;

class A {};
class B {};

class X
{
public:
	int mem_val;
	X() {mem_val = 0; cout << "X()" << endl;}
	void spray(int i)
	{
		cout << "Class A: " << i << endl;
	}
	
	virtual ~X() {cout << "~X()" << endl;}
};

class Y : public X
{
public:
	float mem_val;
	Y() {mem_val = 1.1; cout << "Y()" << endl;}

	using X::spray;
	void spray(char c)
	{
		cout << "Class B: " << c << endl;
	}
	
	~Y() {cout << "~Y()" << endl;}
};

int main()
{
	X *x = new Y();
	delete x;
	Y y;

	y.spray(1);
	y.spray('a');
	
	cout << y.mem_val << endl;
	
	return 0;
}
