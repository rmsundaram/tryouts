#include <iostream>
#include <cstdint>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <iterator>

using namespace std;

//#define FACT_METHOD

#ifdef FACT_METHOD
size_t fact(size_t n);
size_t nCr(size_t n, size_t r);
#endif

// both methods can further be optimized by exploiting
// the symmetry of every order; thus storing half the
// numbers and printing them in reverse order will do
int main(int argc, const char *argv[])
{
	uint32_t order = atoi(argv[1]);

#ifndef FACT_METHOD
	// allocated order + 1 as that many values are printed
	vector<uint32_t> v(order + 1, 0);
	for(uint32_t n = 0; n <= order; ++n)
	{
		const uint32_t begin = 0, end = n;
		v[begin] = v[end] = 1;
		uint32_t prev = v[begin];
		for(uint32_t j = 1; j < end; ++j)
		{
			uint32_t cur = v[j], sum = prev + cur;
			v[j] = sum;
			prev = cur;
		}
		ostream_iterator<uint32_t> out_it(cout, "\t");
		copy(v.cbegin(), v.cbegin() + end + 1, out_it);
		cout << endl;
	}
#else
	// since pascal triangle is made of Binomial Coefficients
	for (uint32_t n = 0; n <= order; ++n)
	{
		for (uint32_t i = 0; i <= n; ++i)
		{
			cout << nCr(n, i) << "\t";
		}
		cout << endl;
	}
#endif
}

#ifdef FACT_METHOD
size_t fact(size_t n)
{
	if ((n == 0) || (n == 1)) return 1;

	size_t fact = 2;
	for (size_t i = 3; i <= n; ++i)
	{
		fact *= i;
	}
	return fact;
}

size_t nCr(size_t n, size_t r)
{
	if (r == 0) return 1;
	else return fact(n) / (fact(n-r) * fact(r));
}
#endif

/*
     1
    1 1
   1 2 1
  1 3 3 1  => is derived from 1 2 1 1 in storage method
 1 4 6 4 1
*/
