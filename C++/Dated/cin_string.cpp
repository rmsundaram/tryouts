#include <iostream>
#include <cstdlib>

using std::string;
using std::cin;
using std::cout;

int main()
{
    string str;

    getline(cin, str, '\n');
    cout << "Welcome, " << str;

    return EXIT_SUCCESS;
}
