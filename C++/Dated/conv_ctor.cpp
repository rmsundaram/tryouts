// these 2 classes are to demonstrate conversion between 2 types
// one using conversion operator and the other using conversion ctor
// change the class order and enable a macro to see the other option working
//#define CONV_OPERATOR

struct Rectangle
{
#ifdef CONV_OPERATOR
	operator Rect()
	{
		cout << "Came to conversion operator!" << endl;
		return Rect(x, y, width, height);
	}
#endif
	
	Rectangle(float x, float y, float width, float height) : x(x), y(y), width(width), height(height)
	{
	}

	float x, y, width, height;
};

struct Rect
{
	Rect(int x, int y, int width, int height) : x(x), y(y), width(width), height(height)
	{
	}

#ifndef CONV_OPERATOR
	Rect(const Rectangle &rect) : x(rect.x), y(rect.y), width(rect.width), height(rect.height)
	{
		cout << "Came to conversion ctor!" << endl;
	}
#endif

	int x, y, width, height;
};

int main()
{
	Rect x = Rectangle(1.f, 1.f, 10.f, 10.f);
}
