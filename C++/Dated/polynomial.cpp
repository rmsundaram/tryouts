#include <iostream>
#include <map>

using namespace std;

class Polynomial
{
public:
	int& coefficient(unsigned degree)
	{
		return deg2coeff[degree];
	}

	const int& coefficient(unsigned degree) const
	{
		return deg2coeff.at(degree);
	}

	size_t size() const
	{
		return deg2coeff.size();
	}

	Polynomial operator+(const Polynomial &that)
	{
		Polynomial result;
		result.deg2coeff = (this->size() > that.size()) ? deg2coeff : that.deg2coeff;
		const auto &smaller = (this->size() <= that.size()) ? *this : that;
		for (const auto &it : smaller.deg2coeff)
		{
			result.coefficient(it.first) += smaller.coefficient(it.first);
		}
		return result;
	}

	friend std::ostream& operator<<(std::ostream& ostr, const Polynomial &poly);

private:
	map<unsigned, int> deg2coeff;
};

void read_polynomial(Polynomial &poly)
{
	cout << "Number of degrees: ";
	unsigned n;
	cin >> n;
	int degree, coeff;
	for (unsigned i = 0; i < n; ++i)
	{
		cout << "Enter Degree, Coefficient: ";
		cin >> degree >> coeff;
		if (coeff)
		{
			poly.coefficient(degree) = coeff;
		}
	}
}

std::ostream& operator<<(std::ostream& ostr, const Polynomial &poly)
{
	for (auto it = poly.deg2coeff.crbegin(); it != poly.deg2coeff.crend(); ++it)
	{
		if (it->second == -1)
			ostr << '-';
		else if (it->first == 0 || it->second != 1)
		{
			if (it->second > 0)
				ostr << "+";
			ostr << it->second;
		}

		if (it->first)
		{
			ostr << "x";
			if (it->first != 1)
				ostr << "^" << it->first;
		}
	}
	return ostr << endl;
}

int main()
{
	Polynomial p1;
	read_polynomial(p1);
	cout << p1;
	
	Polynomial p2;
	read_polynomial(p2);
	cout << p2;

	cout << (p1 + p2);
}
