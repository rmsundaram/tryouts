#include <iostream>
#include <vector>

using namespace std;

struct X
{
	vector<int> GetElements() const
	{
		return elements;
	}
	vector<int> elements;
};

int main()
{
	X obj;
	obj.elements.assign(5, 1);
	vector<int> v = move(obj.GetElements()); // while move(obj.elements); will make obj.elements.size 0
	cout << v. size() << endl;
	cout << obj.elements.size() << endl;
}
