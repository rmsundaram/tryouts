#include <iostream>
#include <ostream>

template <typename T, size_t N>
struct alignas(16) vec
{
	// this too is handled by the variadic version
	// vec() : components{}
	// {
	// }

	template <typename... Args>
	vec(const Args&... args) : components{args...}
	{
	}

	const T& operator[](size_t i) const
	{
		return components[i];
	}
	
	T& operator[](size_t i)
	{
		return components[i];
	}

	T components[N];
};

template <typename T, size_t N, size_t M>
struct mat
{
	mat() : rows{}
	{
	};

	mat(const T &diagonal_val) : mat()
	{
		for (auto i = 0; i < N; ++i)
		{
			rows[i][i] = diagonal_val;
		}
	}
	
	vec<T, M>& operator[](size_t i)
	{
		return rows[i];
	}
	
	const vec<T, M>& operator[](size_t i) const
	{
		return rows[i];
	}

	// try to do this non-uniform scale in variadic templates
	// template<typename T, size_t N, size_t M, typename... Args>
	// static mat scale(const Args&... args)
	// {
		// mat<T, N, M> scalar{};
		// for (auto i = 0; i < N; ++i)
		// {
			
		// }
		// return scalar;
	// }

	vec<T, M> rows[N];
};

template <typename T, size_t N>
T
dot_product(const vec<T, N> &v1, const vec<T, N> &v2)
{
	T result{};
	for (auto i = 0; i < N; ++i)
	{
		result += v1[i] * v2[i];
	}
	return result;
}

template <typename T, size_t M, size_t N>
vec<T, N>
operator*(const mat<T, M, N> &m, const vec<T, N> &v)
{
	vec<T, N> result;
	for (auto i = 0; i < N; ++i)
	{
		result[i] = dot_product(m.rows[i], v);
	}
	return result;
}

template <typename T, size_t N>
std::ostream& operator<<(std::ostream& ostr, const vec<T, N> &v)
{
	for (auto i = 0; i < N; ++i)
	{
		ostr << v[i] << "\t";
	}
	return ostr;
}

template <typename T, size_t M, size_t N>
std::ostream& operator<<(std::ostream& ostr, const mat<T, M, N> &m)
{
	for (auto i = 0; i < N; ++i)
	{
		ostr << m[i] << "\n";
	}
	return ostr;
}

int main()
{
	mat<float, 4, 4> m(1.0f);
	vec<float, 4> v{3.2f, 8.3f, 2.9f, 1.0043f};
	std::cout << "Vector:" << std::endl << v << std::endl;
	std::cout << "Matrix:" << std::endl << m << std::endl;
	auto pv = m * v;
	std::cout << "Transformed Vector:" << std::endl << pv << std::endl;
}
