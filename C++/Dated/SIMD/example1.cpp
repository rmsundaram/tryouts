#include <iostream>
#include <type_traits>

typedef float v4sf __attribute__ ((vector_size(16)));
union /*alignas(16)*/ f4vector
// C++11's alignas isn't required for the compiler to generate vectorised code; just vector_size attribute is enough
{
  v4sf v;
  float f[4]; // = { } will make this type non-POD;
  // but f4vector a; will lead to garbage in f[] since it'd be uninitialized
  // hence call f4vector a{}; will make f have 0s (zero initialized due to f4vector's value initialization)
  // for further info on initializations http://en.cppreference.com/w/cpp/language/initialization

  // this renders the type non-POD
  f4vector(float x, float y, float z, float w) : f{x, y, z, w}
  {
  }

  // if an default construtor is provided by the union then it becomes POD again
  // however declaring a default ctor otherwise i.e. f4vector() {} doesn't work, since it's user-provided and thus non-trivial
  f4vector() = default;
};

int main()
{
  f4vector a{}, b, c{1.2f, -2.3f, 3.4f, -4.5f};

  std::cout << std::boolalpha << std::is_pod<f4vector>::value << std::endl;
  std::cout << "b.f[0] = " << b.f[0] << std::endl << "a.f[0] = " << a.f[0] << std::endl;

  a.f[0] = 1; a.f[1] = 2; a.f[2] = 3; a.f[3] = 4;
  b.f[0] = 5; b.f[1] = 6; b.f[2] = 7; b.f[3] = 8;

  c.v = a.v + b.v;

  std::cout << c.f[0] << ", " << c.f[1] << ", " << c.f[2] << ", " << c.f[3] << std::endl;
}
