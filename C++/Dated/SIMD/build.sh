g++ -fverbose-asm -std=c++11 -I/d/Libs/eigen -msse4.1 -Wa,-adhlns=eigen_dot.s eigen_dot.cpp -o eigen_dot.exe

# -c to simply compile and produce object file; -S to stop after compilation proper and not assemble; -E to stop after the preprocessing stage and not run the compiler proper. The output is in the form of preprocessed source code, which is sent to the standard output.

# -O3 or -ftree-vectorize and -ftree-vectorizer-verbose=7 to see which loops were vectorized