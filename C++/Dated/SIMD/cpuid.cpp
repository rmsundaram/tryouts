#define cpuid(func,ax,bx,cx,dx)\
	__asm__ __volatile__ ("cpuid":\
	"=a" (ax), "=b" (bx), "=c" (cx), "=d" (dx) : "a" (func));

#include <iostream>

int main()
{
	int a, b, c, d;
	cpuid(1, a, b, c, d);
	std::cout << std::hex << "ax = 0x" << a << " bx = 0x" << b << " cx = 0x" << c << " dx = 0x" << d << std::endl;
}
