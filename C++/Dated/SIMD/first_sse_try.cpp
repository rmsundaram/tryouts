__inline matrix3x2 multiply(const matrix3x2 &a, const matrix3x2 &b)
{
	__m128 ar = _mm_load_ps(a.value[0].m128_f32);
	__m128 br = _mm_load_ps(b.value[0].m128_f32);

	__m128 mr1 = _mm_load_ps(br.m128_f32);
	_mm_shuffle_ps(mr1, mr1, _MM_SHUFFLE(2, 0, 2, 0));
	_mm_mul_ps(mr1, ar);

	__m128 mr2 = _mm_load_ps(br.m128_f32);
	_mm_shuffle_ps(mr2, mr2, _MM_SHUFFLE(3, 1, 3, 1));
	_mm_mul_ps(mr2, ar);

	_mm_shuffle_ps(br, br, _MM_SHUFFLE(3, 1, 2, 0));
	__m128 mr3 = _mm_load_ps(a.value[1].m128_f32);
	_mm_movelh_ps(mr3, mr3);
//	_mm_shuffle_ps(mr3, mr3, _MM_SHUFFLE(1, 0, 1, 0));
	_mm_mul_ps(mr3, br);

	_mm_shuffle_ps(mr1, mr1, _MM_SHUFFLE(3, 1, 2, 0));
	_mm_shuffle_ps(mr2, mr2, _MM_SHUFFLE(3, 1, 2, 0));
	__m128 r;
	_mm_movehl_ps(r, mr1);
	_mm_movelh_ps(r, mr2);
	_mm_shuffle_ps(mr1, mr2, _MM_SHUFFLE(3, 2, 1, 0));
	_mm_add_ps(r, mr1);

	__m128 br1 = _mm_load_ps(b.value[1].m128_f32);
	_mm_shuffle_ps(mr3, mr3, _MM_SHUFFLE(3, 1, 2, 0));
	__m128 r1;
	_mm_movehl_ps(r1, mr3);
	_mm_add_ps(r1, br1);
	_mm_add_ps(r1, mr3);

	// r and r1 should contain the components of c
	return matrix3x2(r, r1);
}