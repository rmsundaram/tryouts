#include <iostream>

template <typename T, size_t N>
struct alignas(16) vec
{
	// this too is handled by the variadic version
	// vec() : components{}
	// {
	// }

	template <typename... Args>
	vec(const Args&... args) : components{args...}
	{
	}

	const T& operator[](size_t i) const
	{
		return components[i];
	}
	
	T& operator[](size_t i)
	{
		return components[i];
	}

	T components[N];
};

template <typename T, size_t N>
vec<T, N> add(const vec<T, N> &v1, const vec<T, N> &v2)
{
	vec<T, N> v3;
	for (auto i = 0; i < N; ++i)
	{
		v3.components[i] = v1.components[i] + v2.components[i];
	}
	return v3;
}

template <typename T, size_t N>
vec<T, N> mul(const vec<T, N> &v1, const vec<T, N> &v2)
{
	vec<T, N> v3;
	for (auto i = 0; i < N; ++i)
	{
		v3.components[i] = v1.components[i] * v2.components[i];
	}
	return v3;
}

template <typename T, size_t N>
T dot(const vec<T, N> &v1, const vec<T, N> &v2)
{
	T x{};
	for (auto i = 0; i < N; ++i)
	{
		x += v1[i] * v2[i];
	}
	return x;
}

template <typename T, size_t N>
std::ostream& operator<<(std::ostream& ostr, const vec<T, N> &v)
{
	for (auto i = 0; i < N; ++i)
	{
		ostr << v[i] << "\t";
	}
	return ostr;
}

int main()
{
	vec<float, 4> v1 = {4.50f, 1.0f, 1.0f, 1.0f};
	vec<float, 4> v2 = {8.4f, 1.0f, 1.0f, 1.0f};
	auto v3 = add(v1, v2);
	auto v4 = mul(v1, v2);
	std::cout << v3 << std::endl;
	std::cout << v4 << std::endl;
	std::cout << dot(v3, v4) << std::endl;
}
