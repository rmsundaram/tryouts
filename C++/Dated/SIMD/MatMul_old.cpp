// MatMul.cpp : Defines the entry point for the console application.
//

#include <iostream>
#include <string>

using std::ostream;
using std::cout;
using std::cin;
using std::endl;

#include <windows.h>

double PCFreq = 0.0;
LONGLONG CounterStart = 0;

void StartCounter()
{
    LARGE_INTEGER li;
    if(!QueryPerformanceFrequency(&li))
	cout << "QueryPerformanceFrequency failed!\n";

	// microseconds
	PCFreq = double(li.QuadPart)/1000000.0;

	// milliseconds
	//PCFreq = double(li.QuadPart)/1000.0;

	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);

    QueryPerformanceCounter(&li);
    CounterStart = li.QuadPart;

	SetThreadAffinityMask(GetCurrentThread(), oldmask);
}

double GetCounter()
{
    LARGE_INTEGER li;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
    QueryPerformanceCounter(&li);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
    return double(li.QuadPart-CounterStart)/PCFreq;
}

#ifndef SPMD
struct matrix3x2
{
	matrix3x2(float a00 = 1.0f, float a01 = 0.0f, float a10 = 0.0f, float a11 = 1.0f, float a20 = 0.0f, float a21 = 0.0f)
	{
		_value[0][0] = a00;
		_value[0][1] = a01;
		_value[1][0] = a10;
		_value[1][1] = a11;
		_value[2][0] = a20;
		_value[2][1] = a21;
	}
	float _value[3][2];
};

ostream& operator<<(ostream& os, const matrix3x2 &mat)
{
	return
		os << mat._value[0][0] << "\t" << mat._value[0][1] << "\t0" << endl <<
		mat._value[1][0] << "\t" << mat._value[1][1] << "\t0" << endl <<
		mat._value[2][0] << "\t" << mat._value[2][1] << "\t1" << endl;
}

matrix3x2 multiply(const matrix3x2 &a, const matrix3x2 &b)
{
	return matrix3x2((a._value[0][0] * b._value[0][0]) + (a._value[0][1] * b._value[1][0]),
					 (a._value[0][0] * b._value[0][1]) + (a._value[0][1] * b._value[1][1]),
					 (a._value[1][0] * b._value[0][0]) + (a._value[1][1] * b._value[1][0]),
					 (a._value[1][0] * b._value[0][1]) + (a._value[1][1] * b._value[1][1]),
					 (a._value[2][0] * b._value[0][0]) + (a._value[2][1] * b._value[1][0]) + b._value[2][0],
					 (a._value[2][0] * b._value[0][1]) + (a._value[2][1] * b._value[1][1]) + b._value[2][1]);
}

#else

#include <emmintrin.h>

struct matrix3x2
{
	static matrix3x2 makeIdentity()
	{
		__m128 v[2];
		v[0] = _mm_set_ps(1.0f, 0.0f, 0.0f, 1.0f);
		v[1] = _mm_setzero_ps();
		return matrix3x2(v[0], v[1]);
	}

	static matrix3x2 makeScale(float x, float y)
	{
		__m128 v[2];
		v[0] = _mm_set_ps(y, 0.0f, 0.0f, x);
		v[1] = _mm_setzero_ps();
		return matrix3x2(v[0], v[1]);
	}

	static matrix3x2 makeTranslate(float x, float y)
	{
		__m128 v[2];
		v[0] = _mm_set_ps(1.0f, 0.0f, 0.0f, 1.0f);
		v[1] = _mm_set_ps(0.0f, 0.0f, y, x);
		return matrix3x2(v[0], v[1]);
	}

	matrix3x2(const __m128 &v0, const __m128 &v1)
	{
		value[0] = v0;
		value[1] = v1;
	}
#ifndef _MSC_VER
	matrix3x2() : matrix3x2(matrix3x2::makeIdentity())
	{
	}
#else
	matrix3x2()
	{
		value[0] = _mm_set_ps(1.0f, 0.0f, 0.0f, 1.0f);
		value[1] = _mm_setzero_ps();
	}
#endif

	__m128 value[2];
};

inline matrix3x2 /*__fastcall*/ multiply(const matrix3x2 &a, const matrix3x2 &b)
{
	__m128 ar = a.value[0];
	__m128 br = b.value[0];

	__m128 mr1 = br;
	mr1 = _mm_shuffle_ps(mr1, mr1, _MM_SHUFFLE(2, 0, 2, 0));
	mr1 = _mm_mul_ps(mr1, ar);

	__m128 mr2 = _mm_shuffle_ps(br, br, _MM_SHUFFLE(3, 1, 3, 1));
	mr2 = _mm_mul_ps(mr2, ar);

	br = _mm_shuffle_ps(br, br, _MM_SHUFFLE(3, 1, 2, 0));
	__m128 mr3 = a.value[1];
	mr3 = _mm_movelh_ps(mr3, mr3);
//	mr3 = _mm_shuffle_ps(mr3, mr3, _MM_SHUFFLE(1, 0, 1, 0));
	mr3 = _mm_mul_ps(mr3, br);

	mr1 = _mm_shuffle_ps(mr1, mr1, _MM_SHUFFLE(3, 1, 2, 0));
	mr2 = _mm_shuffle_ps(mr2, mr2, _MM_SHUFFLE(3, 1, 2, 0));
	__m128 r;
	r = _mm_movehl_ps(r, mr1);
	r = _mm_movelh_ps(r, mr2);
	mr1 = _mm_shuffle_ps(mr1, mr2, _MM_SHUFFLE(3, 2, 1, 0));
	r = _mm_add_ps(r, mr1);

	__m128 br1 = b.value[1];
	mr3 = _mm_shuffle_ps(mr3, mr3, _MM_SHUFFLE(3, 1, 2, 0));
	__m128 r1;
	r1 = _mm_movehl_ps(r1, mr3);
	r1 = _mm_add_ps(r1, br1);
	r1 = _mm_add_ps(r1, mr3);

	// r and r1 should contain the components of c
	return matrix3x2(r, r1);
}

// inline matrix3x2 multiply(const __m128 a[2], const __m128 b[2])
// {
	// __m128 mr1 = _mm_shuffle_ps(b[0], b[0], _MM_SHUFFLE(2, 0, 2, 0));
	// mr1 = _mm_mul_ps(mr1, a[0]);

	// __m128 mr2 = _mm_shuffle_ps(b[0], b[0], _MM_SHUFFLE(3, 1, 3, 1));
	// mr2 = _mm_mul_ps(mr2, a[0]);

	// __m128 br = _mm_shuffle_ps(b[0], b[0], _MM_SHUFFLE(3, 1, 2, 0));
	// __m128 mr3 = _mm_movelh_ps(a[1], a[1]);
// //	mr3 = _mm_shuffle_ps(mr3, mr3, _MM_SHUFFLE(1, 0, 1, 0));
	// mr3 = _mm_mul_ps(mr3, br);

	// mr1 = _mm_shuffle_ps(mr1, mr1, _MM_SHUFFLE(3, 1, 2, 0));
	// mr2 = _mm_shuffle_ps(mr2, mr2, _MM_SHUFFLE(3, 1, 2, 0));
	// __m128 r;
	// r = _mm_movehl_ps(r, mr1);
	// r = _mm_movelh_ps(r, mr2);
	// mr1 = _mm_shuffle_ps(mr1, mr2, _MM_SHUFFLE(3, 2, 1, 0));
	// r = _mm_add_ps(r, mr1);

	// mr3 = _mm_shuffle_ps(mr3, mr3, _MM_SHUFFLE(3, 1, 2, 0));
	// __m128 r1;
	// r1 = _mm_movehl_ps(r1, mr3);
	// r1 = _mm_add_ps(r1, b[1]);
	// r1 = _mm_add_ps(r1, mr3);

	// // r and r1 should contain the components of c
	// return matrix3x2(r, r1);
// }

ostream& operator<<(ostream& os, const matrix3x2 &mat)
{
	float value[2][4] = {{}};
	_mm_store_ps(value[0], mat.value[0]);
	_mm_store_ps(value[1], mat.value[1]);

	return
		os << value[0][0] << "\t" << value[0][1] << "\t0" << endl <<
		value[0][2] << "\t" << value[0][3] << "\t0" << endl <<
		value[1][0] << "\t" << value[1][1] << "\t1" << endl;
}

#endif

int main(int argc, char* argv[])
{
#ifndef SPMD
	matrix3x2 scale(2.0f, 0.0f, 0.0f, 3.0f);
	matrix3x2 translate(4.0f, 0.0f, 0.0f, 5.0f, 3.0f, 3.0f);
#else
	auto scale = matrix3x2::makeScale(2.0f, 3.0f);
	auto translate = matrix3x2::makeTranslate(4.0f, 5.0f);
#endif

	matrix3x2 composite;

	StartCounter();
	composite = multiply(scale, translate);

	cout << GetCounter() << " microseconds" << endl;
	cout << composite;
	cout << "Sizeof(matrix3x2) = " << sizeof(composite) << endl;
	cin.get();

	return 0;
}
