#include <iostream>

class matrix3x2
{
public:
	matrix3x2() : _val{{0}}
	{
		_val[0][0] = _val[1][1] = 1;
	}

	matrix3x2(float x11, float x12, float x21, float x22, float x31, float x32)
	{
		_val[0][0] = x11;
		_val[0][1] = x12;
		_val[1][0] = x21;
		_val[1][1] = x22;
		_val[2][0] = x31;
		_val[2][1] = x32;
	}

	void set(int i, int j, float x)
	{
		_val[i][j] = x;
	}
	
	matrix3x2 multiply(const matrix3x2 &that)
	{
		return matrix3x2(this->_val[0][0] * that._val[0][0] + this->_val[0][1] * that._val[1][0],
						 this->_val[0][0] * that._val[0][1] + this->_val[0][1] * that._val[1][1],
						 this->_val[1][0] * that._val[0][0] + this->_val[1][1] * that._val[1][0],
						 this->_val[1][0] * that._val[0][1] + this->_val[1][1] * that._val[1][1],
						 this->_val[2][0] * that._val[0][0] + this->_val[2][1] * that._val[1][0],
						 this->_val[2][0] * that._val[0][1] + this->_val[2][1] * that._val[1][1]);
	}

private:
	float _val[3][2];
};

int main()
{
	matrix3x2 a, b;
	matrix3x2 c = a.multiply(b);
}
