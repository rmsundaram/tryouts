#include <Eigen/Dense>
#include <iostream>

using Matrix4f = Eigen::Matrix<float, 4, 4>;
using Vector4f = Eigen::Matrix<float, 4, 1>;

int main()
{
	Vector4f vec1(1.0f, 3.0f, 4.5f, 2.6f), vec2(7.0f, 1.0f, 1.4f, 0.0f);
	auto scalar = vec1.dot(vec2);
	std::cout << scalar << std::endl;
}
