// MatMul.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

using std::ostream;
using std::cout;
using std::cin;
using std::endl;

#ifdef QPC
double PCFreq = 0.0;
LONGLONG CounterStart = 0;

void StartCounter()
{
    LARGE_INTEGER li;
    if(!QueryPerformanceFrequency(&li))
        cout << "QueryPerformanceFrequency failed!\n";

	// microseconds
	PCFreq = double(li.QuadPart)/1000000.0;

	// milliseconds
	//PCFreq = double(li.QuadPart)/1000.0;

	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);

    QueryPerformanceCounter(&li);
    CounterStart = li.QuadPart;

	SetThreadAffinityMask(GetCurrentThread(), oldmask);
}
double GetCounter()
{
    LARGE_INTEGER li;
	DWORD_PTR oldmask = SetThreadAffinityMask(GetCurrentThread(), 0);
    QueryPerformanceCounter(&li);
	SetThreadAffinityMask(GetCurrentThread(), oldmask);
    return double(li.QuadPart-CounterStart)/PCFreq;
}
#endif

//#define SIMD

#ifndef SIMD
struct matrix3x2
{
	matrix3x2(float a00 = 1.0f, float a01 = 0.0f, float a10 = 0.0f, float a11 = 1.0f, float a20 = 0.0f, float a21 = 0.0f)
	{
		_value[0][0] = a00;
		_value[0][1] = a01;
		_value[1][0] = a10;
		_value[1][1] = a11;
		_value[2][0] = a20;
		_value[2][1] = a21;
	}
	float _value[3][2];

	friend ostream& operator <<(ostream& os, const matrix3x2 &mat);
};

ostream& operator<<(ostream& os, const matrix3x2 &mat)
{
	return
		os << mat._value[0][0] << "\t" << mat._value[0][1] << "\t0" << endl <<
		mat._value[1][0] << "\t" << mat._value[1][1] << "\t0" << endl <<
		mat._value[2][0] << "\t" << mat._value[2][1] << "\t1" << endl;
}

matrix3x2 multiply(const matrix3x2 &a, const matrix3x2 &b)
{
	return matrix3x2((a._value[0][0] * b._value[0][0]) + (a._value[0][1] * b._value[1][0]),
					 (a._value[0][0] * b._value[0][1]) + (a._value[0][1] * b._value[1][1]),
					 (a._value[1][0] * b._value[0][0]) + (a._value[1][1] * b._value[1][0]),
					 (a._value[1][0] * b._value[0][1]) + (a._value[1][1] * b._value[1][1]),
					 (a._value[2][0] * b._value[0][0]) + (a._value[2][1] * b._value[1][0]) + b._value[2][0],
					 (a._value[2][0] * b._value[0][1]) + (a._value[2][1] * b._value[1][1]) + b._value[2][1]);
}

#elif defined XMATH			// end of SISD

struct matrix3x2
{
	matrix3x2(_In_reads_(16) const float *pArray) : value(pArray)
	{
	}
	matrix3x2(const DirectX::XMMATRIX &that) : value(that)
	{
	}
	DirectX::XMMATRIX value;
};

ostream& operator<<(ostream& os, const matrix3x2 &mat)
{
	return
		os << mat.value.r[0].m128_f32[0] << "\t" << mat.value.r[0].m128_f32[1] << "\t0" << endl <<
		mat.value.r[1].m128_f32[0] << "\t" << mat.value.r[1].m128_f32[1] << "\t0" << endl <<
		mat.value.r[3].m128_f32[0] << "\t" << mat.value.r[3].m128_f32[1] << "\t1" << endl;
}

matrix3x2 multiply(const matrix3x2 &a, const matrix3x2 &b)
{
	return matrix3x2(a.value * b.value);
}

#else		// end of XMATH

struct matrix3x2
{
	static matrix3x2 makeIdentity()
	{
		//__m128 v[2];
		//v[0] = _mm_set_ps(1.0f, 0.0f, 0.0f, 1.0f);
		//v[1] = _mm_setzero_ps();
		__m128 v[2] = {
						{ 1.0f, 0.0f, 0.0f, 1.0f },
						{ 0.0f, 0.0f, 0.0f, 0.0f }
					  };
		return matrix3x2(v[0], v[1]);
	}

	static matrix3x2 makeScale(float x, float y)
	{
		/*__m128 v[2];
		v[0] = _mm_set_ps(x, 0.0f, 0.0f, y);
		v[1] = _mm_setzero_ps();*/
		__m128 v[2] = {
						{ 1.0f, 0.0f, 0.0f, 1.0f },
						{ 0.0f, 0.0f, 0.0f, 0.0f }
					  };
		v[0].m128_f32[0] = x;
		v[0].m128_f32[3] = y;
		return matrix3x2(v[0], v[1]);
	}

	static matrix3x2 makeTranslate(float x, float y)
	{
		/*__m128 v[2];
		v[0] = _mm_set_ps(1.0f, 0.0f, 0.0f, 1.0f);
		v[1] = _mm_set_ps(0.0f, 0.0f, y, x);*/
		__m128 v[2] = {
						{ 1.0f, 0.0f, 0.0f, 1.0f },
						{ 0.0f, 0.0f, 0.0f, 0.0f }
					  };
		v[1].m128_f32[0] = x;
		v[1].m128_f32[1] = y;
		return matrix3x2(v[0], v[1]);
	}

	matrix3x2(const __m128 &v0, const __m128 &v1)
	{
		value[0] = v0;
		value[1] = v1;
	}

	matrix3x2()
	{
		value[0] = _mm_set_ps(1.0f, 0.0f, 0.0f, 1.0f);
		value[1] = _mm_setzero_ps();
	}

	__m128 value[2];
};

inline matrix3x2 /*__fastcall*/ multiply(const matrix3x2 &a, const matrix3x2 &b)
{
	__m128 ar = a.value[0];
	__m128 br = b.value[0];

	__m128 mr1 = br;
	mr1 = _mm_shuffle_ps(mr1, mr1, _MM_SHUFFLE(2, 0, 2, 0));
	mr1 = _mm_mul_ps(mr1, ar);

	__m128 mr2 = br;
	mr2 = _mm_shuffle_ps(mr2, mr2, _MM_SHUFFLE(3, 1, 3, 1));
	mr2 = _mm_mul_ps(mr2, ar);

	br = _mm_shuffle_ps(br, br, _MM_SHUFFLE(3, 1, 2, 0));
	__m128 mr3 = a.value[1];
	mr3 = _mm_movelh_ps(mr3, mr3);
//	mr3 = _mm_shuffle_ps(mr3, mr3, _MM_SHUFFLE(1, 0, 1, 0));
	mr3 = _mm_mul_ps(mr3, br);

	mr1 = _mm_shuffle_ps(mr1, mr1, _MM_SHUFFLE(3, 1, 2, 0));
	mr2 = _mm_shuffle_ps(mr2, mr2, _MM_SHUFFLE(3, 1, 2, 0));
	__m128 r;
	r = _mm_movehl_ps(r, mr1);
	r = _mm_movelh_ps(r, mr2);
	mr1 = _mm_shuffle_ps(mr1, mr2, _MM_SHUFFLE(3, 2, 1, 0));
	r = _mm_add_ps(r, mr1);

	__m128 br1 = b.value[1];
	mr3 = _mm_shuffle_ps(mr3, mr3, _MM_SHUFFLE(3, 1, 2, 0));
	__m128 r1;
	r1 = _mm_movehl_ps(r1, mr3);
	r1 = _mm_add_ps(r1, br1);
	r1 = _mm_add_ps(r1, mr3);

	// r and r1 should contain the components of c
	return matrix3x2(r, r1);
}

//inline matrix3x2 multiply(const __m128 a[2], const __m128 b[2])
//{
//	__m128 mr1 = _mm_shuffle_ps(b[0], b[0], _MM_SHUFFLE(2, 0, 2, 0));
//	mr1 = _mm_mul_ps(mr1, a[0]);
//
//	__m128 mr2 = _mm_shuffle_ps(b[0], b[0], _MM_SHUFFLE(3, 1, 3, 1));
//	mr2 = _mm_mul_ps(mr2, a[0]);
//
//	__m128 br = _mm_shuffle_ps(b[0], b[0], _MM_SHUFFLE(3, 1, 2, 0));
//	__m128 mr3 = _mm_movelh_ps(a[1], a[1]);
////	mr3 = _mm_shuffle_ps(mr3, mr3, _MM_SHUFFLE(1, 0, 1, 0));
//	mr3 = _mm_mul_ps(mr3, br);
//
//	mr1 = _mm_shuffle_ps(mr1, mr1, _MM_SHUFFLE(3, 1, 2, 0));
//	mr2 = _mm_shuffle_ps(mr2, mr2, _MM_SHUFFLE(3, 1, 2, 0));
//	__m128 r;
//	r = _mm_movehl_ps(r, mr1);
//	r = _mm_movelh_ps(r, mr2);
//	mr1 = _mm_shuffle_ps(mr1, mr2, _MM_SHUFFLE(3, 2, 1, 0));
//	r = _mm_add_ps(r, mr1);
//
//	mr3 = _mm_shuffle_ps(mr3, mr3, _MM_SHUFFLE(3, 1, 2, 0));
//	__m128 r1;
//	r1 = _mm_movehl_ps(r1, mr3);
//	r1 = _mm_add_ps(r1, b[1]);
//	r1 = _mm_add_ps(r1, mr3);
//
//	// r and r1 should contain the components of c
//	return matrix3x2(r, r1);
//}

ostream& operator<<(ostream& os, const matrix3x2 &mat)
{
	return
		os << mat.value[0].m128_f32[0] << "\t" << mat.value[0].m128_f32[1] << "\t0" << endl <<
		mat.value[0].m128_f32[2] << "\t" << mat.value[0].m128_f32[3] << "\t0" << endl <<
		mat.value[1].m128_f32[0] << "\t" << mat.value[1].m128_f32[1] << "\t1" << endl;
}

#endif

int main(int argc, char* argv[])
{
#ifndef SIMD
	matrix3x2 scale(2.0f, 0.0f, 0.0f, 2.0f);
	matrix3x2 translate(1.0f, 0.0f, 0.0f, 1.0f, 3.0f, 3.0f);
#elif defined XMATH
	__declspec(align(16)) float const scale[16] = {
												2.0f, 0.0f, 0.0f, 0.0f,
												0.0f, 2.0f, 0.0f, 0.0f,
												0.0f, 0.0f, 1.0f, 0.0f,
												0.0f, 0.0f, 0.0f, 1.0f
											};
	__declspec(align(16)) float const translate[16] = {
												1.0f, 0.0f, 0.0f, 0.0f,
												0.0f, 1.0f, 0.0f, 0.0f,
												0.0f, 0.0f, 1.0f, 0.0f,
												3.0f, 3.0f, 0.0f, 1.0f
											};
	matrix3x2 scaler(scale);
	matrix3x2 translater(translate);
#else
	auto scale = matrix3x2::makeScale(1.0f, 2.0f);
	auto translate = matrix3x2::makeTranslate(3.0f, 4.0f);
#endif

	auto tbegin = __rdtsc();
	matrix3x2 composite = multiply(scale, translate);
	auto tend = __rdtsc();

	cout << "Time taken: " << tend - tbegin << " cycles" << endl;

	cout << composite;
	cout << "Sizeof(matrix3x2) = " << sizeof(composite) << endl;
	cin.get();

	return 0;
}

