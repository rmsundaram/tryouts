#include <xmmintrin.h>

class vmatrix3x2
{
public:
	// vmatrix3x2() : value{{0}}
	// {
		// _val[0][0] = _val[1][1] = 1;
	// }

	// vmatrix3x2(float thisValue[4][2]) : value(thisValue)
	// {
	// }
	
	// void set(int i, int j, float x)
	// {
		// value[i][j] = x;
	// }
	
	vmatrix3x2 multiply(const vmatrix3x2 &that) const
	{
		//__attribute__ ((aligned(16))) float result[4][2];

		auto bt1   = _mm_shuffle_ps(that.value[0], that.value[0], _MM_SHUFFLE(0, 2, 0 , 2));
		auto mr1 = _mm_mul_ps(this->value[0], bt1);

		auto bt2 = _mm_shuffle_ps(that.value[0], that.value[0], _MM_SHUFFLE(1, 3, 1 , 3));
		auto mr2 = _mm_mul_ps(this->value[0], bt2);

		auto at1 = _mm_shuffle_ps(this->value[1], this->value[1], _MM_SHUFFLE(0, 1, 0, 1));
		bt1 = _mm_movelh_ps(bt1, bt2);
		auto mr3 = _mm_mul_ps(at1, bt1);
	}

private:
	//__attribute__ ((aligned(16))) float value[4][2];
	__m128 value[2];
};

int main()
{
	vmatrix3x2 a, b, c;
	c = a.multiply(b);
}
