#include <iostream>
#include <boost/smart_ptr.hpp>

using std::cout;
using std::endl;

class testee
{
public:
    testee()
    {
        std::cout<<"Default constructor"<<std::endl;
        test = new int;
        *test = 0;
    }
    explicit testee(int val)
    {
        std::cout<<"Single value constructor: "<<val<<std::endl;
        test = new int;
        *test = val;
    }
    void setVal(int val)
    {
        *test = val;
    }
    void printme()
    {
        std::cout<<*test<<std::endl;
    }
    ~testee()
    {
        if(test)
        {
            std::cout<<*test<<", Yay destructed!"<<std::endl;
            delete test;
            test = NULL;
        }
    }

private:
    int *test;
    static const int a[][2];
};
const int testee::a[][2] = {{1, 2}, {3, 4}};
testee* my_testee_creator(int x)
{
    return new testee(x);
}

int main()
{
    int *temp = new int;
    *temp = 12;
    boost::scoped_ptr<int> a;
    if(NULL == a.get())
        cout<<"right"<<endl;
    a.reset(temp);
    //temp = NULL;
    *a = 1023;
    (*temp)++;
    cout<<*a<<endl<<*temp<<endl;
    
    testee *t1 = new testee(120);
    boost::scoped_ptr<testee> abc(new testee);
    abc.reset(t1);
    t1 = NULL;

    {
        boost::scoped_ptr<testee> secptr(abc.get());
        secptr->printme();
    }
    if(abc)
    cout<<"Right!"<<endl;
    abc->printme();
    
	return 0;
}
