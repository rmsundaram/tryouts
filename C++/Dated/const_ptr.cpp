#include <iostream>
#include <memory>

using std::cout;
using std::endl;
using std::shared_ptr;

struct A
{
	A() : p(new int(0)) { }

	~A()
	{
		if (p)
		{
			delete p;
			p = nullptr;
		}
	}

	int getA()
	{
		return 1;
	}

	// although this is a const function
	// editing *p is still allowed, since
	// only the pointer is the member of
	// the class and not the pointee
	// http://stackoverflow.com/q/293857
	void putP(int x) const
	{
		*p = x;
	}

private:
	int *p;
};

int main()
{
    // both pointer and pointee will be const
	// shared_ptr<const A> spA(new const A);
	// this will fail to compile without getA being const

	const shared_ptr<A> spA(new A);
	cout << spA->getA() << endl;
	spA->putP(10);
}
