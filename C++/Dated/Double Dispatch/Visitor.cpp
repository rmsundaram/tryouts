#include <iostream>
#include <vector>

struct Wheel;
struct Steering;
struct Body;
struct Car;

// “Operation” is a better term than “Visitor”.  Advantage of this pattern: for
// every new operation introduced (e.g. fix, clean, etc.) one needn’t edit
// base/dervied sources to implement them.  You separate the operations code
// from the hierarchy itself.
//
// A scene graph with nodes in it just describes the organizational structure
// and corresponding data in each node; it’s structure is to be kept fairly
// stable.  Operations like update, paint, etc. are done by visitors.  Double
// dispatch is the usual technique used to implement this in C++.
//
// https://stackoverflow.com/q/255214

struct CarVisitor
{
	virtual void visit(Car&) = 0;
	virtual void visit(Wheel&) = 0;
	virtual void visit(Steering&) = 0;
	virtual void visit(Body&) = 0;

	virtual ~CarVisitor() {}
};

struct CarPart
{
	virtual void accept(CarVisitor&) = 0;

	virtual ~CarPart() {}
};

struct Wheel : public CarPart
{
	void accept(CarVisitor &visitor) override
	{
		visitor.visit(*this);
	}
};

struct Steering : public CarPart
{
	void accept(CarVisitor &visitor) override
	{
		visitor.visit(*this);
	}
};

struct Body : public CarPart
{
	void accept(CarVisitor &visitor) override
	{
		visitor.visit(*this);
	}
};

struct Car : public CarPart
{
    void accept(CarVisitor& v) override
    {
        v.visit(*this);
        v.visit(m_body);
        v.visit(m_steering);
        for (auto& w: m_wheels)
            v.visit(w);
    }

    Body m_body;
    Steering m_steering;
    std::vector<Wheel> m_wheels{5};
};

struct Check : public CarVisitor
{
	void visit(Car&) override
	{
        std::cout << "Checking car...\n";
	}
	void visit(Wheel&) override
	{
        std::cout << "  Checking wheel...\n";
	}
	void visit(Steering&) override
	{
        std::cout << "  Checking steering...\n";
	}
	void visit(Body&) override
	{
        std::cout << "  Checking body...\n";
	}
};

struct Clean : public CarVisitor
{
	void visit(Car&) override
	{
        std::cout << "Cleaning car...\n";
	}
	void visit(Wheel&) override
	{
        std::cout << "  Cleaning wheel...\n";
	}
	void visit(Steering&) override
	{
        std::cout << "  Cleaning steering...\n";
	}
	void visit(Body&) override
	{
        std::cout << "  Cleaning body...\n";
	}
};

int main()
{
    Car c;
    Clean cleaner;
    c.accept(cleaner);
	Check mechanic;
    c.accept(mechanic);

	return EXIT_SUCCESS;
}
