#include <iostream>
using std::cout;
using std::endl;

class SpaceShip;
class GiantSpaceShip;

struct Asteroid
{
	virtual void CollideWith(SpaceShip&)
	{
		cout << "Asteroid collided with SpaceShip" << endl;
	}

	virtual void CollideWith(GiantSpaceShip&)
	{
		cout << "Asteroid collided with GiantSpaceShip" << endl;
	}
};
 
struct ExplodingAsteroid : public Asteroid
{
	virtual void CollideWith(SpaceShip&)
	{
		cout << "ExplodingAsteroid collided with SpaceShip" << endl;
	}

	virtual void CollideWith(GiantSpaceShip&)
	{
		cout << "ExplodingAsteroid collided with GiantSpaceShip" << endl;
	}
};

struct SpaceShip
{
	virtual void CollideWith(Asteroid &s)
	{
		s.CollideWith(*this);
	}
};

struct GiantSpaceShip : public SpaceShip
{
	virtual void CollideWith(Asteroid &s)
	{
		s.CollideWith(*this);
	}
};

int main(void)
{
	Asteroid theAsteroid;
	SpaceShip theSpaceShip;
	GiantSpaceShip theGiantSpaceShip;

	theAsteroid.CollideWith(theSpaceShip); 
	theAsteroid.CollideWith(theGiantSpaceShip);

	ExplodingAsteroid theExplodingAsteroid;
	theExplodingAsteroid.CollideWith(theSpaceShip); 
	theExplodingAsteroid.CollideWith(theGiantSpaceShip);

	Asteroid& theAsteroidReference = theExplodingAsteroid;
	theAsteroidReference.CollideWith(theSpaceShip); 
	theAsteroidReference.CollideWith(theGiantSpaceShip);

	SpaceShip& theSpaceShipReference = theGiantSpaceShip;
	theAsteroid.CollideWith(theSpaceShipReference); 
	theAsteroidReference.CollideWith(theSpaceShipReference);

	return 0;
}
