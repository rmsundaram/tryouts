#include <type_traits>
#include <iostream>

namespace Animal
{
	struct Rat;
	struct Cat;

	struct Animal
	{
		virtual bool operator==(const Animal& that) const = 0;
		virtual bool operator==(const Cat& that)    const = 0;
		virtual bool operator==(const Rat& that)    const = 0;
		virtual ~Animal() { }
	};

	namespace OpImpl
	{
		template <typename T>
		typename std::enable_if<std::is_base_of<Animal, T>::value, bool>::type operator==(const T &lhs, const T& rhs)
		{
			std::cout << "T vs T" << std::endl;
			return true;
		}

		bool operator==(const Cat &cat, const Rat &rat)
		{
			std::cout << "Cat vs Rat" << std::endl;
			return false;
		}

		bool operator==(const Rat &rat, const Cat &cat)
		{
			return OpImpl::operator==(cat, rat);
		}
	}

	struct Cat : Animal
	{
		bool operator==(const Animal& that) const override
		{
			return that == *this;
		}

		bool operator==(const Cat &cat) const
		{
			return OpImpl::operator==(cat, *this);
		}

		bool operator==(const Rat &rat) const
		{
			return OpImpl::operator==(*this, rat);
		}
	};

	struct Rat : Animal
	{
		bool operator==(const Animal& that) const override
		{
			return that == *this;
		}

		bool operator==(const Rat& rat) const
		{
			return OpImpl::operator==(rat, *this);
		}

		bool operator==(const Cat& cat) const
		{
			return OpImpl::operator==(cat, *this);
		}
	};
}

int main()
{
	Animal::Cat cat1, cat2;
	Animal::Rat rat1, rat2;

	cat1 == cat1;
	rat1 == rat2;

	cat1 == rat1;
	rat1 == cat1;

	Animal::Animal &cat_ref = cat1;
	Animal::Animal &rat_ref = rat2;

	cat_ref == cat_ref;
	rat_ref == rat_ref;

	cat_ref == rat_ref;
	rat_ref == cat_ref;
}
