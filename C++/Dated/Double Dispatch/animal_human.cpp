#include <iostream>

using namespace std;

class human;
class warrior;

class wildanimal{
public:
    virtual void attack(human*);
    virtual void attack(warrior*);
};

class human{
  public:
    virtual void attackedbywildanimal(wildanimal *animal){
        animal->attack(this);
    }
};

class warrior : public human{
public:
    virtual void attackedbywildanimal(wildanimal *animal){
        animal->attack(this);
    }
};

void wildanimal::attack(human* h){
      cout<<"Animal attacks human\n";
}
void wildanimal::attack(warrior* w){
      cout<<"Animal attacks warrior\n";
}

class boar: public wildanimal{
public:
    void attack(human* h){
        cout<<"Boar attacks human\n";
    }
    void attack(warrior* w){
        cout<<"Boar attacks warrior\n";
    }
};

int main(int argv, char** argc){
  
    human * h = new warrior();
    wildanimal * w = new boar();
 
    //Problem
    w->attack(h);

    //Double Dispatch solution
    h->attackedbywildanimal(w);
}
