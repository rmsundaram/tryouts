#include <iostream>
using std::cout;
using std::endl;

class SpaceShip;
class GiantSpaceShip;

class Asteroid
{
public:
	virtual void CollideWith(SpaceShip&)
	{
		cout << "Asteroid hit a SpaceShip" << endl;
	}

	virtual void CollideWith(GiantSpaceShip&)
	{
		cout << "Asteroid hit a GiantSpaceShip" << endl;
	}
};

class ExplodingAsteroid : public Asteroid
{
public:
	virtual void CollideWith(SpaceShip&)
	{
		cout << "ExplodingAsteroid hit a SpaceShip" << endl;
	}

	virtual void CollideWith(GiantSpaceShip&)
	{
		cout << "ExplodingAsteroid hit a GiantSpaceShip" << endl;
	}
};

class SpaceShip 
{
public:
	virtual void CollideWith(Asteroid &inAsteroid)
	{
		inAsteroid.CollideWith(*this);
	}
};

class GiantSpaceShip : public SpaceShip
{
public:
	virtual void CollideWith(Asteroid &inAsteroid)
	{
		inAsteroid.CollideWith(*this);
	}
};

int main(void)
{
	Asteroid theAsteroid;
	GiantSpaceShip theGiantSpaceShip;
	ExplodingAsteroid theExplodingAsteroid;

	Asteroid &theAsteroidReference = theExplodingAsteroid;
	SpaceShip &theSpaceShipReference = theGiantSpaceShip;

	// problem
	theAsteroid.CollideWith(theSpaceShipReference);
	theAsteroidReference.CollideWith(theSpaceShipReference);

	// solution
	theSpaceShipReference.CollideWith(theAsteroid);
	theSpaceShipReference.CollideWith(theAsteroidReference);

	return 0;
}
