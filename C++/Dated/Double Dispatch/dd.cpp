#include <iostream>
#include <typeinfo>
using std::cout;

class Complex;
class Integer;

class Math
{
public:
	virtual void add(Math &rhs) = 0;
	/*
		commenting the below two functions in the base class (Math) 
		will lead to an infinite looping of function calls - Reason: 
		i->(*c) is resolved @ runtime and will reach Integer::add(Math &rhs) 
		as expected. Now the this pointer will be Integer and the call 
		rhs.add(*this), where rhs is still a Math reference and not yet 
		resolved as Complex since only for executing the call (@ runtime)
		the vptr will be looked-up and the function will be called from 
		the function pointer. Where as here, rhs is still a Math ref which 
		will not have anything other than Math::add(Math&), since we've 
		commented out the below Math::add(Integer&), the only choice 
		compiler has is to call Math::add(Math&) which leads to an 
		infinite recursion.
	 */
	virtual void add(Complex &rhs) = 0;
	virtual void add(Integer &rhs) = 0;
	virtual ~Math() {}
};

class Complex : public Math
{
public:
	void add(Math &rhs)
	{
		cout << "Complex::add(Math&)\n";
		rhs.add(*this);
	}
	
	void add(Integer &rhs)
	{
		cout << "Complex * Integer\n";
	}

	void add(Complex &rhs)
	{
		cout << "Complex * Complex\n";
	}
};

class Integer : public Math
{
public:
	void add(Math &rhs)
	{
		cout << "Integer::add(Math&)\n";
		rhs.add(*this);
	}

	void add(Integer &rhs)
	{
		cout << "Integer * Integer\n";
	}

	void add(Complex &rhs)
	{
		cout << "Integer * Complex\n";
	}
};

int main()
{
	Math *i = new Integer, *c = new Complex;
	i->add(*c);
}
