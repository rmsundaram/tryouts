#include <iostream>

using namespace std;

class wildanimal;
class boar;

class human{
  public:
    virtual void attackedbywildanimal(wildanimal*)
	{
        cout << "Human attacked by Wild Animal!\n";
    }

    virtual void attackedbywildanimal(boar*)
	{
        cout << "Human attacked by Boar!\n";
    }
};

class warrior : public human{
public:
    virtual void attackedbywildanimal(wildanimal *animal)
	{
        cout << "Warrior attacked by Wild Animal!\n";
    }

    virtual void attackedbywildanimal(boar*)
	{
        cout << "Warrior attacked by Boar!\n";
    }
};

class wildanimal{
public:
    virtual void attack(human *h)
	{
      h->attackedbywildanimal(this);
	}
};

class boar: public wildanimal{
public:
    virtual void attack(human *h)
	{
      h->attackedbywildanimal(this);
	}
};

int main(int argv, char** argc){
  
    human * h = new warrior();
    wildanimal * w = new boar();
 
    w->attack(h);
}
