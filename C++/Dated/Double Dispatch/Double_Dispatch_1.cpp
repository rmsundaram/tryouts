#include <iostream>
using std::cout;
using std::endl;

class Creature {};
class TrollCreature : public Creature {};

class Potion
{
public:
	virtual void spray(Creature &)
	{
		cout << "Generic Spray on Generic Creature" << endl;
	}
};

class TrollPotion : public Potion
{
public:
	virtual void spray(TrollCreature &)
	{
		cout << "Troll Spray on Troll Creature" << endl;
	}
};

int main()
{
	TrollCreature c;
	TrollPotion p;

	p.spray(c);
}
