#include <iostream>
using std::cout;
using std::endl;

struct SpaceShip {};
struct GiantSpaceShip : public SpaceShip {};
 
struct Asteroid {
	virtual void CollideWith(SpaceShip&)
	{
		cout << "Asteroid hit a SpaceShip" << endl;
	}
	virtual void CollideWith(GiantSpaceShip&)
	{
		cout << "Asteroid hit a GiantSpaceShip" << endl;
	}
};
 
struct ExplodingAsteroid : public Asteroid
{
	virtual void CollideWith(SpaceShip&)
	{
		cout << "ExplodingAsteroid hit a SpaceShip" << endl;
	}
	virtual void CollideWith(GiantSpaceShip&)
	{
		cout << "ExplodingAsteroid hit a GiantSpaceShip" << endl;
	}
};


int main()
{
	Asteroid theAsteroid;
	SpaceShip theSpaceShip;
	GiantSpaceShip theGiantSpaceShip;

	theAsteroid.CollideWith(theSpaceShip); 
	theAsteroid.CollideWith(theGiantSpaceShip);

	ExplodingAsteroid theExplodingAsteroid;
	theExplodingAsteroid.CollideWith(theSpaceShip); 
	theExplodingAsteroid.CollideWith(theGiantSpaceShip);

	Asteroid& theAsteroidReference = theExplodingAsteroid;
	theAsteroidReference.CollideWith(theSpaceShip); 
	theAsteroidReference.CollideWith(theGiantSpaceShip);

	SpaceShip& theSpaceShipReference = theGiantSpaceShip;
	// below two are incorrect
	theAsteroid.CollideWith(theSpaceShipReference);
	theAsteroidReference.CollideWith(theSpaceShipReference);
}
