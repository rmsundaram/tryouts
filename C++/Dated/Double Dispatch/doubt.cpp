// from http://stackoverflow.com/questions/429849/double-dispatch-multimethods-in-c

#include <iostream>
#include <string>
using namespace std;

class BaseClass;
class Derived1;
class Derived2;
class Derived3;

class Processor {
public:
       void processObj(BaseClass *bc);
       void processObj(Derived1 *d1);
       void processObj(Derived2 *d2);
};

class BaseClass {
public:
       virtual void ProcessThis(Processor &p) { p.processObj(this); }
       virtual void myFunction(){cout << "base myFunction called" << endl;}
};

class Derived1: public BaseClass {
public:
       void ProcessThis(Processor &p) { p.processObj(this); }
       void myFunction(){cout << "Derived1 myFunction called" << endl;}
};

class Derived2: public BaseClass {
public:
       void ProcessThis(Processor &p) { p.processObj(this); }
       void myFunction(){cout << "Derived2 myFunction called" << endl;}
};

class Derived3: public BaseClass {
public:
       void ProcessThis(Processor &p) { p.processObj(this); }
       void myFunction(){cout << "Derived3 myFunction called" << endl;}
};

void Processor::processObj(BaseClass *d2) {cout << "got a base object - "; d2->myFunction(); }
void Processor::processObj(Derived1 *d1) {cout << "got a derived1 object - "; d1->myFunction(); }
void Processor::processObj(Derived2 *d2) {cout << "got a derived2 object - "; d2->myFunction(); }

int main() {
   Processor p;

   BaseClass *bcp=new BaseClass();
   Derived1 *dc1p=new Derived1();
   Derived2 *dc2p=new Derived2();
   Derived3 *dc3p=new Derived3();

   //first set results

   bcp->ProcessThis(p);
   dc1p->ProcessThis(p);
   dc2p->ProcessThis(p);
   dc3p->ProcessThis(p);

   BaseClass *bcp1=bcp;
   BaseClass *dc1p1=dc1p;   
   BaseClass *dc2p1=dc2p;
   BaseClass *dc3p1=dc3p;

   //second set results

   bcp1->ProcessThis(p);
   dc1p1->ProcessThis(p);
   dc2p1->ProcessThis(p);
   dc3p1->ProcessThis(p);

   return 0;
}
