#include <iostream>

// reference to pointer, little used feature of C++
void set_ptr(int* &x)
{
    *x = 1;
}

int main()
{
    std::cout << "fun!\n";
    // initializing dynamically allocated variable
    int *ptr = new int(0);
    // different from new int [0]
    // http://stackoverflow.com/q/13797926
    set_ptr(ptr);
    std::cout << *ptr << std::endl;
}
