#include <memory>
#include <iostream>

using namespace std;

int main()
{
	shared_ptr<int> spA = make_shared<int>(1);
	auto spB = spA;
	shared_ptr<int> spC = make_shared<int>(2);

	cout << "Values before swap:" << endl;
	cout << *spA << endl;
	cout << *spB << endl;
	cout << *spC << endl;

	swap(spA, spC);
	spB = spA;

	cout << "Values after swap:" << endl;
	cout << *spA << endl;
	cout << *spB << endl;
	cout << *spC << endl;
}

