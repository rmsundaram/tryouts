#include <iostream>
#include <memory>
#include <utility>

using std::cout;
using std::endl;

using std::shared_ptr;
using std::enable_shared_from_this;

#pragma warning(push)
#pragma warning(disable:4250)
class IA
{
public:
    virtual void printA() const = 0;
};

class IB : virtual public IA
{
public:
    virtual void printB() const = 0;
};

struct A : virtual public IA, enable_shared_from_this<A>
{
    void printA() const
    {
        cout << "A::printA" << endl;
    }
};

struct B : public IB, public A
{
    void printB() const
    {
        shared_ptr<const A> spA = shared_from_this();
        spA->printA();
        cout << "B::printB" << endl;
    }
};
#pragma warning(pop)

int __cdecl main()
{
    shared_ptr<B> b(new B);
    b->printB();
}
