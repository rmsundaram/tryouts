#include <iostream>
using std::cout;

struct Fee
{
public:
  Fee(int)
  {
	cout << "Fee(int)\n";
  }
  
  Fee(const Fee &rhs)
  {
	cout << "Fee(const Fee&)\n";
  }
};

class Fo
{
  int i;
public:
  Fo(int x = 0) : i(x) {}
  operator Fee() const
  {
	cout << "Auto conversion!\n";
	return Fee(i);
  }
};

int main()
{
  Fo fo;
  Fee fee = fo;
}
