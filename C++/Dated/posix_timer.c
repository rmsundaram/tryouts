#include <signal.h>
#include <sys/time.h>
#include <stdio.h>
#include <stdlib.h>

/* 1 second = 1000 milliseconds = 1000 * 1000 microseconds */
#define INTERVAL (1000 * 1000)

void alarm_cb(int sig_id)
{
	signal(SIGALRM, alarm_cb);
	printf("Timer called back!!!\n");
	/*exit(0);*/
}

int main()
{
	struct itimerval timer_params = {{0}};
	timer_params.it_interval.tv_sec = 1;
	timer_params.it_interval.tv_usec = 0;
	timer_params.it_value.tv_sec = 1;
	timer_params.it_value.tv_usec = 0;

	setitimer(ITIMER_REAL, &timer_params, NULL);
	signal(SIGALRM, alarm_cb);

	for(;;){}

	return 0;
}

