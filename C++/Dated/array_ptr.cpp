#include <iostream>

int main()
{
	// http://stackoverflow.com/questions/1810083/c-pointers-pointing-to-an-array-of-fixed-size/1810295#1810295
	// whereas in C one can do int (*p) [5] = malloc(sizeof *p);
	// here p cannot be declared int (*p) [5]!
	// the standard specifically says new "returns a pointer to the initial element of the array" if an array is allocated
	int *p = new int[5] { };
	delete [] p;

	int array[10] { };
	int (*p2) [10] = &array;	// had '&' not been part of RHS, then the compiler silently gives int* back (decay)
	// p2 += 1 would not move p2 by sizeof int bytes but by sizeof array bytes
	std::cout << sizeof array << std::endl;
	std::cout << p2 << std::endl;
	p2 += 1;
	std::cout << p2 << std::endl;
}
