#include <vector>
#include <algorithm>

template <class C>			// this line is for the generic type test takes as a template argument
typename C::iterator		// this line is for the return type of test()
test()
{
	C c(100000);
	c[95000] = true;
	return std::find(c.begin(), c.end(), true);
}


int main()
{
	test<std::vector<bool>>();
}
