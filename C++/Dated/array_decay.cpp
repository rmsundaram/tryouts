#include <iostream>

int main()
{
	int a[] = {0, 1, 2, 3};
	int *p = a;							// silent decay into int*
	std::cout << (a + 1) << std::endl;	// silent decay into int* here too there by (a + 1) == (p + 1)
	std::cout << (p + 1) << std::endl;	// both expressions would be x + sizeof(int), where x is the base address

	
	int b[][2] = {{0, 1}, {2, 3}};
	p = b[0];							// b would decay into int (*) [2]; using b[0] which would decay into int*
	int (*pp) [2] = b;
	std::cout << "Sizeof(int (*)[2]) = " << sizeof pp << '\t' << (b + 1) << std::endl;
	std::cout << (b + 1) << std::endl;	// b + 1 will be x + (sizeof(int) * 2), where 2 is the 2nd dimension
	std::cout << (p + 1) << std::endl;	// p + 1 will be x + sizeof(int)
	// hence a difference of sizeof(int) will be seen between the expressions (b + 1) and (p + 1)
}
