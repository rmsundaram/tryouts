#ifndef __SDKSmartArrayPointer__
#define __SDKSmartArrayPointer__

template<typename T>
class SDKSmartArrayPointer 
{
public:
    SDKSmartArrayPointer() : pointer(NULL)
	{
	}

	SDKSmartArrayPointer(T* ptr) : pointer(ptr)
	{
		if(pointer)
		{
			pointer->__refCount++;
		}
	}

	SDKSmartArrayPointer(const SDKSmartArrayPointer& other) : pointer(other.pointer)
	{
		if(pointer)
		{
			pointer->__refCount++;
		}
	}
    
    ~SDKSmartArrayPointer()
	{
		if(pointer)
		{
			pointer->__refCount--;
			if(pointer->__refCount==0) {
				delete [] pointer;
			}
		}
	}

    SDKSmartArrayPointer& operator = ( const SDKSmartArrayPointer& other )
    {
        if( pointer == other.pointer )
		{
            return *this;
		}

        T* tmp = pointer;
        pointer = other.pointer;
		if(pointer)
		{
			pointer->__refCount++;
		}
        if(tmp)
		{
			tmp->__refCount--;
			if(tmp->__refCount==0) {
				delete [] tmp;
			}
		}
        return *this;
    }

    SDKSmartArrayPointer& operator = ( T* ptr )
    {
        if( pointer == ptr )
		{
            return *this;
		}

        T* tmp = pointer;
        pointer = ptr;
		if(pointer)
		{
			pointer->__refCount++;
		}
        if(tmp)
		{
			tmp->__refCount--;
			if(tmp->__refCount==0) {
				delete [] tmp;
			}
		}
        return *this;
    }

    bool operator == ( const SDKSmartArrayPointer& other ) const 
    {
		return pointer==other.pointer;
	}
    
	bool operator != ( const SDKSmartArrayPointer& other ) const
    {
		return pointer!=other.pointer;
	}
    
    bool operator ! () const
	{
		return pointer==NULL;
	}

    bool operator == ( const T* other ) const
	{
		return pointer==other;
	}
    
	bool operator != ( const T* other ) const
	{
		return pointer!=other;
	}
    
    T* operator->()
	{
		return pointer;
	}

	const T* operator->() const
	{
		return pointer;
	}
    
	T& operator*()        
    {
		return *pointer;
	}

    const T& operator*() const  
    {
		return *pointer;
	}
    
    T* get()
	{
		return pointer;
	}

private:
    T* pointer;
};

#endif
