#include <stdio.h>

class single
{
public:
	static single& init()
	{
		static single theSingleton;
		return theSingleton;
	}

	void hey(const 	char *s)
	{
		printf("Amazing %s!\n", s);
	}

private:
	~single()
	{
		printf("Single destroyed\n");
	}
};

int main()
{
	single &a = single::init();
	a.hey();
	single &b = single::init();
	b.hey();
	return 0;
}
