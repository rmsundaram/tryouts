#include <iostream>

using std::cout;
using std::endl;

void b()
{
	throw std::bad_alloc();
}

void a()
{
	try
	{
		b();
		int x = 0;
		++x;
	}
	catch(std::bad_cast &exp)
	{
		cout << "Caught!" << endl;
	}
}

int main()
{
	try
	{
		a();
		int x = 0;
		++x;
	}
	catch(std::bad_alloc &exp)
	{
		cout << "Caught in main!" << endl;
	}
}
