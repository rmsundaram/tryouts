#include <iostream>
#include <memory>

using std::cout;
using std::auto_ptr;

struct A
{
    // pure virtual is only to enforce the inheritor to provide its own
    // implementation; that doesn't mean the base cannot give a default
    // implementation
	virtual void add() = 0;
};

// pure virtual function implementation
void A::add()
{
	cout << "A::add()\n";
}

struct B : public A
{
	void add()
	{
        // invokes the pure virtual impl.
		A::add();
		cout << "B::add()\n";
	}
	virtual ~B()
	{
		cout << "~B()\n";
	}
};

int main()
{
	B b;
	A *a = &b;
	a->add();

	auto_ptr<B> hehe(new B());
}
