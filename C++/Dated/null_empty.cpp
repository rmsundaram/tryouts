#include <iostream>
using namespace std;

int main()
{
    // nullptr vs empty string
    const char *str = "";
    const char *strN = NULL;

    if(strN)
    {
        cout << "null pointer" << endl;
    }

    if(str)
    {
        cout << "pointer to a location containing null char" << endl;
        cout << "hello " << str[0] << " hello" << endl;
    }
}
