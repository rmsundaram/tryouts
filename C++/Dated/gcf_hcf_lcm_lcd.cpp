#include <iostream>
#include <stdexcept>

using std::cout;
using std::endl;

// Euclid's GCD Algorithm
// http://www.azillionmonkeys.com/qed/asmexample.html
unsigned int gcd (unsigned int a, unsigned int b)
{
    if (a == 0 && b == 0)
        b = 1;
    else if (b == 0)
        b = a;
    else if (a != 0)
        while (a != b)
            if (a < b)
                b -= a;
            else
                a -= b;

    return b;
}

int lcm(int a, int b)
{
	return ((a * b) / gcd(a, b));
}

unsigned int mod(unsigned int a, unsigned int b) throw (std::runtime_error)
{
	if(b == 0)
		throw std::runtime_error("Divide by zero error!");

	while(a >= b)
		a -= b;

	return a;
}

int main()
{
	cout << gcd(56, 32) << endl;
	cout << lcm(56, 32) << endl;

	cout << gcd(48, 180) << endl;
}

/*	doesn't work -> e.g. try 48, 180
int gcd(int a, int b)
{
	int count = 0, backup = 0;
	
	if(a == 0 || b == 0)
		return 1;
	else if(a == 1 || b == 1)
		return 0;
	else
	{
		do
		{
			a /= 2;
			++count;
		} while (((a % 2) == 0) && (a > 1));

		backup = count;

		do
		{
			b /= 2;
			--count;
		} while(((b % 2) == 0) && (b > 1) && (count));
	}

	return pow(2, (backup - count));
}
*/

