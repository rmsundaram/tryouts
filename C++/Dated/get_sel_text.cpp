#include <windows.h>
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

const WORD inputKey[2] = {VK_CONTROL, VK_INSERT};

inline bool isAltPressed()
{
	return (0x8000 & GetAsyncKeyState(VK_MENU));
}

inline void printLastError()
{
	cout << "Error: " << GetLastError() << endl;
}

int main(int argc, char *argv[])
{
	MSG msg = {0};
	UINT hotKeyChar = 0x58; // 'X'

	if(RegisterHotKey(NULL, 1, MOD_CONTROL | MOD_ALT /*| MOD_NOREPEAT*/, hotKeyChar))
	{
		cout << "Hotkey (CTRL + ALT + W) register succeeded!" << endl;
		cout << "Use CTRL + C to terminate app." << endl;

		while (GetMessage(&msg, NULL, 0, 0))
		{
			if(msg.message == WM_HOTKEY)
			{
				GUITHREADINFO guiThreadInfo = {0};
				guiThreadInfo.cbSize = sizeof(GUITHREADINFO);

				cout << "Hotkey Pressed!" << endl;

				if(GetGUIThreadInfo(NULL, &guiThreadInfo))
				{
					if(isAltPressed())
					{
						cout << "Virtually released ALT!" << endl;
						keybd_event(VK_MENU, 0, KEYEVENTF_KEYUP, 0);
					}

#ifndef USE_KEYBD_EVENT
					const int keyCount = 4;
					INPUT input[keyCount] = {{INPUT_KEYBOARD}, {INPUT_KEYBOARD}, {INPUT_KEYBOARD}, {INPUT_KEYBOARD}};

					input[0].ki.wVk = input[2].ki.wVk = inputKey[0];
					input[1].ki.wVk = input[3].ki.wVk = inputKey[1];

					input[2].ki.dwFlags = (input[3].ki.dwFlags |= KEYEVENTF_KEYUP);
					//BringWindowToTop(guiThreadInfo.hwndFocus);
					SendInput(keyCount, input, sizeof(INPUT));
#else
					//BringWindowToTop(guiThreadInfo.hwndActive);
					keybd_event(inputKey[0], 0, 0, 0);
					keybd_event(inputKey[1], 0, 0, 0);
					keybd_event(inputKey[1], 0, KEYEVENTF_KEYUP, 0);
					keybd_event(inputKey[0], 0, KEYEVENTF_KEYUP, 0);
#endif
					// take a nap when the key strokes are actually processed
					Sleep(100);

					if(OpenClipboard(NULL))
					{
						HGLOBAL hglb = GetClipboardData(CF_TEXT);

						if(hglb)
						{
							LPSTR charArrayFromClipboard = static_cast<LPSTR>(GlobalLock(hglb));

							if(charArrayFromClipboard)
							{
								string selString = charArrayFromClipboard;
								GlobalUnlock(hglb);
								CloseClipboard();
								
								cout << "Selected Text: \"" << selString << "\"" << endl;
							}
							else
								printLastError();	// GlobalLock
						}
						else
							printLastError();	// GetClipboardData
					}
					else
						printLastError();	// OpenClipboard
				}
				else
					printLastError();	// GetGUIThreadInfo
			}
		}
	}
	else
	{
		cout << "Hotkey register failed!" << endl;
	}
}
