#include <windows.h>
#include <stdio.h>

struct abc
{
	int a[2];
	char b;
	int *ptr;
};

int main()
{
	const char *strByte = "abcde";
	wchar_t strWide[4] = L"";

	int written_chars = MultiByteToWideChar(CP_ACP, 0, strByte, -1, strWide, 4);
	printf("%d\nErr: %ld\n", written_chars, GetLastError());
	
	printf("Size of struct abc = %d and int = %d\n", sizeof(struct abc), sizeof(int));

	return 0;
}
