#include <iostream>
#include <string>
#include <cstring>
using std::cout;
using std::string;

class sstr
{
	string x;
public:
	sstr(const char *cstr) : x(cstr) {}
	operator const char*()
	{
		return x.c_str();
	}
};

int main()
{
	sstr s = "India", t = "India";
	cout << strcmp(s, t);
	
	return 0;
}
