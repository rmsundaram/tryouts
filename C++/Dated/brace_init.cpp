#include <iostream>

struct abc
{
	int a;
	float b;
	char c;
	union
	{
		int p;
		char q;
	} d;
};

int main()
{
	// without { } it'd be uninitialized
	abc x = { };
	std::cout << x.a << std::endl;
	std::cout << x.b << std::endl;
	std::cout << "'" << x.c << "'"  << std::endl;
	std::cout << x.d.p<< std::endl;
}
