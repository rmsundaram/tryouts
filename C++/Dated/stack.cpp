#include <iostream>
using std::cout;
using std::endl;

void sub(bool &a)
{
	bool b;
	int x = reinterpret_cast<int>(&a);
	int y = reinterpret_cast<int>(&b);
	
	cout << "Stack grows " << ((y > x) ? "up!" : "down!") << endl;
}

int main()
{
	bool a, b;
	int x = reinterpret_cast<int>(&a);
	int y = reinterpret_cast<int>(&b);

	cout << "Stack grows " << ((y > x) ? "up!" : "down!") << endl;
	sub(a);
}
