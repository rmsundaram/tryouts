#include <iostream>

using std::cout;
using std::endl;
using std::swap;

template <int N>
void transpose(int arr[N][N])
{
	for(int i = 0; i < N; ++i)
	{
		for(int j = i + 1; j < N; ++j)
		{
			swap(arr[i][j], arr[j][i]);
		}
	}
}

template <int col>
void display(int (*arr)[col], int row)
{
	for(int i = 0; i < row; ++i)
	{
		for(int j = 0; j < col; ++j)
			cout << arr[i][j] << "\t";

		cout << endl;
	}
}

int main()
{
	int a[3][3] = {{0, 1, 2}, {3, 4, 5}, {6, 7, 8}};

	// to demonstrate pointer to arrays
	int (*ptr)[3][3] = &a;
	cout << ***ptr << endl;

	transpose(a);
	display(a, 3);

	int b[5][2] = {{0, 1}, {2, 3}, {4, 5}, {6, 7}, {8, 9}};
	display(b, 5);

	return 0;
}

