#include <iostream>
using std::cout;
using std::endl;
using std::hex;

int main()
{
	unsigned int num = 0x01020304;

	unsigned short msw = num >> 16, lsw = num;// & 0x0000FFFF;

	cout << "0x" << hex << num << endl;
	cout << "0x" << hex << msw << endl;
	cout << "0x" << hex << lsw << endl;

	
	return 0;
}
