#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <string>

#include <memory>
#include <set>

int main()
{
    std::vector<std::string> v1;
    v1.push_back("a");
    v1.push_back("b");
    v1.push_back("c");
    v1.push_back("d");

    std::vector<std::string> v2;
    v2.push_back("e");
    v2.push_back("f");
    v2.push_back("g");
    v2.push_back("h");

    std::move(v2.begin(), v2.end(), inserter(v1, v1.end()));

    std::ostream_iterator<std::string> out_it(std::cout, ", ");
    std::copy(v1.cbegin(), v1.cend(), out_it);
    std::cout << std::endl;
    std::copy(v2.cbegin(), v2.cend(), out_it);
    std::cout << std::endl << v2.size();	// this shows that the 4 string objects are still there with no data in them
    std::cout << std::endl;

    std::shared_ptr<int> spOne(new int(7)), spTwo(spOne);
    std::set<std::shared_ptr<int>> checkSet;
    bool fInserted = checkSet.insert(spOne).second;
    std::cout << std::boolalpha << fInserted << std::endl;
    fInserted = checkSet.insert(spTwo).second;
    std::cout << std::boolalpha << fInserted << std::endl;
}