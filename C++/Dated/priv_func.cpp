#include <iostream>
using std::cout;
using std::endl;

class A
{
public:
	void add()
	{
	}
};

class B : public A
{
	using A::add;
};

int main()
{
	B obj;
	obj.add();                  // inacessible private member, oops!
}
