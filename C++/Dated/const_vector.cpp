#include <vector>

int main()
{
	std::vector<int> v(10, 0);
	// const is not only for the container, but the contents it contains
	const auto *p = &v;
	p->push_back(1);
	(*p)[0] = 1;
}
