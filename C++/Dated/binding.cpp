#include <iostream>
#include <string>
#include <algorithm>
#include <functional>

#define ARRAYSIZE(x) (sizeof(x) / sizeof(x[0]))

struct a
{
    void hehe(int) { }
};

int main(int argc, const char *argv[])
{
    const char *pStringArray[] = {"India", "Russia" , "Japan"};
    if (argc > 1)
    {
        std::string strInput(argv[1]);

        const bool isCountry = std::any_of(pStringArray,
                                           pStringArray + ARRAYSIZE(pStringArray),
                                           bind1st(std::equal_to<std::string>(), strInput));
        std::cout << std::boolalpha << isCountry << std::endl;
    }

    return 0;
}