// http://stackoverflow.com/q/2757986

#include <vector>
#include <cassert>
#include <cstdarg>
#include <iostream>
#include <cstdio>

using std::vector;
using std::cout;
using std::endl;

template <typename T, size_t N>
class smart_array
{
    vector<smart_array<T, N - 1>> vec;

public:

//explicit is put up since without that 
//smart_array <float, 2> y(2,  3); y = 3.0f;
// will be allowed with no compiler errors

    explicit smart_array(vector <size_t> &dimensions)
    {
        assert(N == dimensions.size());

        vector <size_t>::iterator it = ++dimensions.begin();
        vector <size_t> dimensions_remaining(it, dimensions.end());

        smart_array <T, N - 1> temp_smart_array(dimensions_remaining);
        vec.assign(dimensions[0], temp_smart_array);
    }

    explicit smart_array(size_t dimension_1 = 1, ...)
    {
        static_assert(N > 0, "Error: smart_array expects 1 or more dimension(s)");
        assert(dimension_1 > 1);

        va_list dim_list;
        vector <size_t> dimensions_remaining(N - 1);

        va_start(dim_list, dimension_1);
            for(size_t i = 0; i < N - 1; ++i)
            {
                size_t dimension_n = va_arg(dim_list, size_t);
                assert(dimension_n > 0);
                dimensions_remaining[i] = dimension_n;
            }
        va_end(dim_list);

        smart_array <T, N - 1> temp_smart_array(dimensions_remaining);
        vec.assign(dimension_1, temp_smart_array);
    }

    smart_array<T, N - 1>& operator[](size_t index)
    {
        assert(index < vec.size() && index >= 0);
        return vec[index];
    }

    size_t length() const
    {
        return vec.size();
    }
};

template <typename T>
class smart_array<T, 1>
{
    vector <T> vec;

public:
    explicit smart_array(vector <size_t> &dimension) : vec(dimension[0])
    {
        assert(dimension[0] > 0);
    }

    explicit smart_array(size_t dimension_1 = 1) : vec(dimension_1)
    {
        assert(dimension_1 > 0);
    }

    T& operator[](size_t index)
    {
        assert(index < vec.size() && index >= 0);
        return vec[index];
    }

    size_t length()
    {
        return vec.size();
    }
};


int main()
{
	smart_array <int, 1> x(3);
	x[0] = 0;
	x[1] = 1;
	x[2] = 2;

	std::cout << "x.length(): " << x.length() << std::endl;

	smart_array <float, 2> y(2, 3);
	y[0][0] = y[0][1] = y[0][2] = 0;
	y[1][0] = y[1][1] = y[1][2] = 1;

	std::cout << "y.length(): " << y.length() << std::endl;
	std::cout << "y[0].length(): " << y[0].length() << std::endl;

	smart_array <char, 3> z(2, 4, 5);
	std::cout << "z.length(): " << z.length() << std::endl;
	std::cout << "z[0].length(): " << z[0].length() << std::endl;
	std::cout << "z[0][0].length(): " << z[0][0].length() << std::endl;
	z[0][0][4] = 'c';
	std::cout << z[0][0][4] << std::endl;

	smart_array <bool, 4> r(2, 3, 4, 5);
	std::cout << "r.length(): " << r.length() << std::endl;
	std::cout << "r[0].length(): " << r[0].length() << std::endl;
	std::cout << "r[0][0].length(): " << r[0][0].length() << std::endl;
	std::cout << "r[0][0][0].length(): " << r[0][0][0].length() << std::endl;

	smart_array <float, 2> copy_y(y);
	std::cout << "copy_y.length(): " << copy_y.length() << std::endl;
	std::cout << "copy_y[0].length(): " << copy_y[0].length() << std::endl;

	std::cout << copy_y[0][0] << "\t" << copy_y[1][0] << "\t" << copy_y[0][1] << "\t" << copy_y[1][1] << "\t" << copy_y[0][2] << "\t" << copy_y[1][2] << std::endl;


	getchar();
}
