#include<design.h>

#define LEFT 19200
#define RIGHT 19712
#define UP 18432
#define DOWN 20480

#define COLOUR BROWN
#define FOOD YELLOW
#define BELLY RED

struct point
{
	int x,y,dx,dy,key,flag;
	struct point *next;
}	*first;

typedef struct point bend;
int x1=200,y1=300,x2=250,y2=300,dx1=1,dx2=1,dy1=0,dy2=0;
int x,y,bends=0,cur_key=RIGHT,points=0,pre_x,pre_y;

void putpoints()
{
	char str[4];
	itoa(points,str,10);
	settextstyle(0,0,0);
	setfillstyle(0,BLACK);
	bar(555,20,600,27);
	setcolor(WHITE);
	outtextxy(555,20,str);
}

void growstomach()
{
	pre_x=x,pre_y=y;
	putpixel(x-1,y,BELLY);
	putpixel(x+1,y,BELLY);
	putpixel(x,y,BELLY);
	putpixel(x,y+1,BELLY);
	putpixel(x,y-1,BELLY);
}

void clearstomach()
{
	if(getpixel(pre_x-1,pre_y) == BELLY) putpixel(pre_x-1,pre_y,BLACK);
	if(getpixel(pre_x,pre_y-1) == BELLY) putpixel(pre_x,pre_y-1,BLACK);
	if(getpixel(pre_x,pre_y) == BELLY) putpixel(pre_x,pre_y,BLACK);
	if(getpixel(pre_x+1,pre_y) == BELLY) putpixel(pre_x+1,pre_y,BLACK);
	if(getpixel(pre_x,pre_y+1) == BELLY) putpixel(pre_x,pre_y+1,BLACK);
}

void putfood()
{
	x=random(615);
	y=random(445);
	if(x<=30) x=x+25;
	if(y<=110) y=y+105;
	if(getpixel(x-1,y)==COLOUR || getpixel(x,y-1)==COLOUR || getpixel(x+1,y)==COLOUR || getpixel(x,y+1)==COLOUR)
	   putfood();
	putpixel(x+1,y,FOOD);
	putpixel(x-1,y,FOOD);
	putpixel(x,y,FOOD);
	putpixel(x,y+1,FOOD);
	putpixel(x,y-1,FOOD);
}

void grow()
{
	setfillstyle(1,COLOUR);
	if(dx1==-1)
    {
	bar(x1-1,y1,x1+10,y1);
	x1=x1+11;
    }
	else if(dx1==1)
    {
	bar(x1+1,y1,x1-10,y1);
	x1=x1-11;
    }
	else if(dy1==-1)
    {
	bar(x1,y1-1,x1,y1+10);
	y1=y1+11;
    }
	else
    {
	bar(x1,y1+1,x1,y1-10);
	y1=y1-11;
    }
	points=points+10;
	putpoints();
	growstomach();
	putfood();
/*	sound(100);
	delay(20);
	sound(50);
	delay(20);
	nosound();*/
}

void setbend(int key)
{
	bend *add,*new;
	if(key == DOWN)
	   dx2=0,dy2=1;
	else if(key == UP)
	   dx2=0,dy2=-1;
	else if(key == RIGHT)
	   dx2=1,dy2=0;
	else if(key == LEFT)
	   dx2=-1,dy2=0;
	if(first->flag==0)
   {
	   if(key == DOWN)
	      first->dx=0,first->dy=1;
	   else if(key == UP)
	      first->dx=0,first->dy=-1;
	   else if(key == RIGHT)
	      first->dx=1,first->dy=0;
	   else if(key == LEFT)
	      first->dx=-1,first->dy=0;
	   first->x=x2,first->y=y2;
	   first->key=key;
	   first->flag=1;
	   first->next=NULL;
   }
	else
   {
	   new=(bend *)malloc(sizeof(bend));
	   new->key=key,new->flag=1;
	   new->x=x2,new->y=y2;
	   new->next=NULL;
	   if(key == DOWN)
	      new->dx=0,new->dy=1;
	   else if(key == UP)
	      new->dx=0,new->dy=-1;
	   else if(key == RIGHT)
	      new->dx=1,new->dy=0;
	   else if(key == LEFT)
	      new->dx=-1,new->dy=0;
	   add=first;
	   while(add->next!=NULL)
		 add=add->next;
	   add->next=new;
   }
	cur_key=key;
	bends++;
}

void movebend()
{
	bend *temp;
	temp=first;
	first=first->next;
	free(temp);
}

void snake()
{
	setfillstyle(1,BLACK);
	bar(x1,y1,x1+dx1,y1+dy1);
	setfillstyle(1,COLOUR);
	bar(x2,y2,x2+dx2,y2+dy2);
}

void engine(int speed)
{
	int key;
	while(!kbhit())
   {
	if(getpixel(x2+dx2,y2+dy2) == COLOUR) return;
	if(bends>0)
    {
	if(first->x == x1 && first->y == y1)
     {
	dx1=first->dx,dy1=first->dy;
	bends--;
	if(bends!=0)
	   movebend();
	else
	   first->flag=0;
     }
    }
	if(x1>=618 && dx1>0) x1=22;
	if(x2>=618 && dx2>0) x2=22;
	if(y1>=448 && dy1>0) y1=102;
	if(y2>=448 && dy2>0) y2=102;
	if(x1<=22 && dx1<0) x1=618;
	if(x2<=22 && dx2<0) x2=618;
	if(y1<=102 && dy1<0) y1=448;
	if(y2<=102 && dy2<0) y2=448;
	snake();
	delay(speed);
	if((x2==x+1 || x2==x  || x2==x-1) && (y2==y+1 || y2==y || y2==y-1))
	    grow();
	x1=x1+dx1,x2=x2+dx2;
	y1=y1+dy1,y2=y2+dy2;
	if((x1>=pre_x-1 && x1<=pre_x+1) && (y1>=pre_y-1 && y1<=pre_y+1))
	    clearstomach();
   }
	key=bioskey(0);
	if(key == 283)
	   exit(0);
	else if(!(key == cur_key) && !(key == RIGHT && cur_key == LEFT) && !(key == LEFT && cur_key == RIGHT) && !(key == UP && cur_key == DOWN) && !(key == DOWN && cur_key == UP))
	   setbend(key);
	engine(speed);
}

void terminate()
{
	settextstyle(6,0,5);
	while(!kbhit())
   {
	setcolor(LIGHTRED);
	outtextxy(212,50,"GAME OVER");
	delay(250);
	setcolor(BLACK);
	outtextxy(212,50,"GAME OVER");
	delay(250);
   }
	closegraph();
}

void initialize()
{
	first=(bend *)malloc(sizeof(bend));
	first->flag=0;
	randomize();
	settextstyle(4,0,5);
	setcolor(LIGHTMAGENTA);
	outtextxy(200,10,"S N A K E");
	line(200,65,430,65);
	setlinestyle(0,0,3);
	setcolor(LIGHTBLUE);
	rectangle(20,100,620,450);
	setcolor(LIGHTGREEN);
	settextstyle(0,0,0);
	outtextxy(500,20,"Points");
	setlinestyle(0,0,2);
	setcolor(WHITE);
	rectangle(495,16,610,30);
	putpoints();
	putfood();
	setfillstyle(1,COLOUR);
	bar(x1,y1,x2,y2);
}

void main()
{
	int gmode,gdriver = DETECT;
	initgraph(&gdriver,&gmode,"..\\EGA");
	cleardevice();
	initialize();
	engine(10);
	terminate();
}