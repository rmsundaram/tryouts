#include<design.h>

#define LEFT 19200
#define RIGHT 19712
#define UP 18432
#define DOWN 20480

struct point
{
	int x,y,dx,dy,key,flag;
	struct point *next;
}	*first;

typedef struct point bend;
int x1=200,y1=100,x2=250,y2=100,dx1=1,dx2=1,dy1=0,dy2=0,bends=0,cur_key=RIGHT;

void setbend(int key)
{
	bend *add,*new;
	if(key == DOWN)
	   dx2=0,dy2=1;
	else if(key == RIGHT)
	   dx2=1,dy2=0;
	else if(key == LEFT)
	   dx2=-1,dy2=0;
	else if(key == UP)
	   dx2=0,dy2=-1;
	if(first->flag==0)
   {
	   if(key == DOWN)
	      first->dx=0,first->dy=1;
	   else if(key == UP)
	      first->dx=0,first->dy=-1;
	   else if(key == RIGHT)
	      first->dx=1,first->dy=0;
	   else if(key == LEFT)
	      first->dx=-1,first->dy=0;
	   first->x=x2,first->y=y2;
	   first->key=key;
	   first->flag=1;
	   first->next=NULL;
   }
	else
   {
	   new=(bend *)malloc(sizeof(bend));
	   new->key=key,new->flag=1;
	   new->x=x2,new->y=y2;
	   new->next=NULL;
	   if(key == DOWN)
	      new->dx=0,new->dy=1;
	   else if(key == UP)
	      new->dx=0,new->dy=-1;
	   else if(key == RIGHT)
	      new->dx=1,new->dy=0;
	   else if(key == LEFT)
	      new->dx=-1,new->dy=0;
	   add=first;
	   while(add->next!=NULL)
		 add=add->next;
	   add->next=new;
   }
	cur_key=key;
	bends++;
}

void movebend()
{
	bend *temp;
	temp=first;
	first=first->next;
	free(temp);
}

void snake()
{
	setfillstyle(1,BLACK);
	bar(x1,y1,x1+dx1,y1+dy1);
	setfillstyle(1,WHITE);
	bar(x2,y2,x2+dx2,y2+dy2);
}

void engine(int speed)
{
	int key;
	while(!kbhit())
   {
	if(bends>0)
    {
	if(first->x == x1 && first->y == y1)
     {
	dx1=first->dx,dy1=first->dy;
	bends--;
	if(bends!=0)
	   movebend();
	else
	   first->flag=0;
     }
    }
	snake();
	delay(speed);
	x1=x1+dx1,x2=x2+dx2;
	y1=y1+dy1,y2=y2+dy2;
   }
	key=bioskey(0);
	if(key == 283)
	   exit(0);
	else if(!(key == cur_key) && !(key == RIGHT && cur_key == LEFT) && !(key == LEFT && cur_key == RIGHT) && !(key == UP && cur_key == DOWN) && !(key == DOWN && cur_key == UP))
	   setbend(key);
	engine(speed);
}

void main()
{
	int gmode,gdriver = DETECT;
	first=(bend *)malloc(sizeof(bend));
	first->flag=0;
	initgraph(&gdriver,&gmode,"..\\EGA");
	cleardevice();
	setfillstyle(1,WHITE);
	bar(x1,y1,x2,y2);
	engine(15);
}