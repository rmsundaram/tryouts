#include<design.h>

#define LEFT 19200
#define RIGHT 19712
#define UP 18432
#define DOWN 20480
#define CHEATKEY 11776
#define ESC 283

#define SINTERVAL 3
#define SDURATION 5

#define GROWTH 21
#define SGROWTH 41

#define BORDER LIGHTBLUE
#define COLOUR BROWN
#define FOOD YELLOW
#define SPECIAL LIGHTRED
#define BELLY WHITE

struct point
{
	int x, y, dx, dy, key, flag;
	struct point *next;
}	*first;

typedef struct point bend;

int speed=3, wall_opacity=0, paused=0, cheat = 0;;

int x1, y1, x2, y2, dx1, dx2, dy1, dy2;
int x, y, bends, cur_key, points;

int counter, sflag, duration, sx, sy;
clock_t start, end;

void putpoints()
{
	char str[4];
	itoa(points,str,10);
	settextstyle(0,0,0);
	setfillstyle(0,BLACK);
	bar(555,20,600,27);
	setcolor(WHITE);
	outtextxy(556,20,str);
}

void clearbyte()
{
	if(getpixel(x-1,y) == FOOD) putpixel(x-1,y,BLACK);
	if(getpixel(x,y-1) == FOOD) putpixel(x,y-1,BLACK);
	if(getpixel(x,y) == FOOD) putpixel(x,y,BLACK);
	if(getpixel(x+1,y) == FOOD) putpixel(x+1,y,BLACK);
	if(getpixel(x,y+1) == FOOD) putpixel(x,y+1,BLACK);
}

void clear_sfood()
{
	putpixel(sx+1,sy,BLACK);
	putpixel(sx-1,sy,BLACK);
	putpixel(sx,sy+1,BLACK);
	putpixel(sx,sy-1,BLACK);
	putpixel(sx,sy,BLACK);
	putpixel(sx+1,sy+1,BLACK);
	putpixel(sx+1,sy-1,BLACK);
	putpixel(sx-1,sy+1,BLACK);
	putpixel(sx-1,sy-1,BLACK);
	sflag=0;
}

void growstomach()
{
	if(sflag==2) clear_sfood();
	else clearbyte();

	putpixel(x2-1,y2,BELLY);
	putpixel(x2+1,y2,BELLY);
	putpixel(x2,y2,BELLY);
	putpixel(x2,y2+1,BELLY);
	putpixel(x2,y2-1,BELLY);
	if(getpixel(x2-1,y2-1) != BLACK) putpixel(x2-1,y2-1,BELLY);
	if(getpixel(x2+1,y2+1) != BLACK) putpixel(x2+1,y2+1,BELLY);
	if(getpixel(x2+1,y2-1) != BLACK) putpixel(x2+1,y2-1,BELLY);
	if(getpixel(x2-1,y2+1) != BLACK) putpixel(x2-1,y2+1,BELLY);
}

void putspecialfood()
{
	sx=random(615);
	sy=random(445);
	if(sx<=30) sx=sx+25;
	if(sy<=110) sy=sy+105;
	if(getpixel(sx-1,sy)==COLOUR || getpixel(sx,sy-1)==COLOUR || getpixel(sx+1,sy)==COLOUR || getpixel(sx,sy+1)==COLOUR)
	   putspecialfood();
	putpixel(sx+1,sy,SPECIAL);
	putpixel(sx-1,sy,SPECIAL);
	putpixel(sx,sy+1,SPECIAL);
	putpixel(sx,sy-1,SPECIAL);
	putpixel(sx,sy,SPECIAL);
	putpixel(sx+1,sy+1,SPECIAL);
	putpixel(sx+1,sy-1,SPECIAL);
	putpixel(sx-1,sy+1,SPECIAL);
	putpixel(sx-1,sy-1,SPECIAL);
	start=clock();
}

void putfood()
{
	x=random(615);
	y=random(445);
	if(x<=30) x=x+25;
	if(y<=110) y=y+105;
	if(getpixel(x-1,y)==COLOUR || getpixel(x,y-1)==COLOUR || getpixel(x+1,y)==COLOUR || getpixel(x,y+1)==COLOUR)
	   putfood();
	putpixel(x+1,y,FOOD);
	putpixel(x-1,y,FOOD);
	putpixel(x,y,FOOD);
	putpixel(x,y+1,FOOD);
	putpixel(x,y-1,FOOD);
	if(counter == SINTERVAL && sflag == 0)
   {
	counter=0;
	sflag=1;
	putspecialfood();
   }
	counter++;
}

void sgrow()
{
	if(cheat == 0)
 {
	setlinestyle(0,0,3);
	setcolor(COLOUR);
	if(dx1==-1)
    {
	line(x1-1,y1,x1+10,y1);
	x1=x1+SGROWTH;
    }
	else if(dx1==1)
    {
	line(x1+1,y1,x1-10,y1);
	x1=x1-SGROWTH;
    }
	else if(dy1==-1)
    {
	line(x1,y1-1,x1,y1+10);
	y1=y1+SGROWTH;
    }
	else
    {
	line(x1,y1+1,x1,y1-10);
	y1=y1-SGROWTH;
    }
 }
	setlinestyle(0,0,3);
	setcolor(BORDER);
	rectangle(20,100,620,450);
	points=points+30;
	sflag=2;
	putpoints();
	growstomach();
	sound(1200);
	delay(5);
	sound(900);
	delay(10);
	nosound();
}

void grow()
{
	if(cheat == 0)
 {
	setlinestyle(0,0,3);
	setcolor(COLOUR);
	if(dx1==-1)
    {
	line(x1-1,y1,x1+10,y1);
	x1=x1+GROWTH;
    }
	else if(dx1==1)
    {
	line(x1+1,y1,x1-10,y1);
	x1=x1-GROWTH;
    }
	else if(dy1==-1)
    {
	line(x1,y1-1,x1,y1+10);
	y1=y1+GROWTH;
    }
	else
    {
	line(x1,y1+1,x1,y1-10);
	y1=y1-GROWTH;
    }
 }
	setlinestyle(0,0,3);
	setcolor(BORDER);
	rectangle(20,100,620,450);
	points=points+10;
	putpoints();
	growstomach();
	putfood();
	sound(1200);
	delay(5);
	sound(900);
	delay(10);
	nosound();
}

void gameover()
{
	settextstyle(6,0,5);
	putpixel(x2+dx2,y2+dy2,COLOUR);
	if(cur_key == RIGHT || cur_key == LEFT)
     {
	putpixel(x2+dx2,y2+dy2-1,COLOUR);
	putpixel(x2+dx2,y2+dy2+1,COLOUR);
     }
	else
     {
	putpixel(x2+dx2-1,y2+dy2,COLOUR);
	putpixel(x2+dx2+1,y2+dy2,COLOUR);
     }

	setcolor(LIGHTRED);
	settextstyle(1,0,10);
	outtextxy(160,100,"GAME");
	outtextxy(160,250,"OVER");

	sound(1000);
	delay(100);

	sound(1050);
	delay(100);

	sound(1100);
	delay(100);

	sound(1150);
	delay(100);

	sound(1200);
	delay(100);


	sound(1250);
	delay(100);

	sound(1300);
	delay(100);

	sound(1350);
	delay(100);

	sound(1400);
	delay(100);

	sound(1450);
	delay(100);

	sound(1500);
	delay(100);

	sound(1550);
	delay(100);

	sound(1600);
	delay(100);

	sound(1650);
	delay(100);

	sound(1700);
	delay(100);

	sound(1750);
	delay(100);

	sound(1800);
	delay(100);

	sound(1850);
	delay(100);

	sound(1900);
	delay(100);

	sound(1950);
	delay(100);

	sound(2000);
	delay(100);

	nosound();
	cleardevice();
}

void pausegame()
{
	while(!kbhit())
   {
	setcolor(LIGHTRED);
	settextstyle(6,0,5);
	outtextxy(215,50,"GAME PAUSED");
	delay(250);
	setcolor(BLACK);
	outtextxy(215,50,"GAME PAUSED");
	delay(250);
   }
}

void setbend(int key)
{
	bend *add, *new;
	if(key == DOWN)
	   dx2=0,dy2=1;
	else if(key == UP)
	   dx2=0,dy2=-1;
	else if(key == RIGHT)
	   dx2=1,dy2=0;
	else if(key == LEFT)
	   dx2=-1,dy2=0;
	if(first->flag==0)
   {
	   if(key == DOWN)
	      first->dx=0,first->dy=1;
	   else if(key == UP)
	      first->dx=0,first->dy=-1;
	   else if(key == RIGHT)
	      first->dx=1,first->dy=0;
	   else if(key == LEFT)
	      first->dx=-1,first->dy=0;
	   first->x=x2,first->y=y2;
	   first->key=key;
	   first->flag=1;
	   first->next=NULL;
   }
	else
   {
	   new=(bend *)malloc(sizeof(bend));
	   new->key=key,new->flag=1;
	   new->x=x2,new->y=y2;
	   new->next=NULL;
	   if(key == DOWN)
	      new->dx=0,new->dy=1;
	   else if(key == UP)
	      new->dx=0,new->dy=-1;
	   else if(key == RIGHT)
	      new->dx=1,new->dy=0;
	   else if(key == LEFT)
	      new->dx=-1,new->dy=0;
	   add=first;
	   while(add->next!=NULL)
		 add=add->next;
	   add->next=new;
   }
	cur_key=key;
	bends++;
}

void releasebend()
{
	bend *temp;
	temp=first;
	first=first->next;
	free(temp);
}

void snake()
{
	setlinestyle(0,0,3);
	setcolor(BLACK);
	line(x1,y1,x1+dx1,y1+dy1);
	setcolor(COLOUR);
	line(x2,y2,x2+dx2,y2+dy2);
	putpixel(x1+dx1+dx1,y1+dy1+dy1,BLACK);
	if(cur_key == LEFT || cur_key == RIGHT)
    {
	   putpixel(x2+dx2,y2+dy2-1,BLACK);
	   putpixel(x1+dx1+dx1,y1+dy1+dy1+1,BLACK);
    }
	else
    {
	   putpixel(x2+dx2-1,y2+dy2,BLACK);
	   putpixel(x1+dx1+dx1+1,y1+dy1+dy1,BLACK);
    }
}

int engine()
{
	int key;
restart:
	while(!kbhit())
   {
	if(((cur_key == DOWN || cur_key == RIGHT) && (getpixel(x2+dx2+1,y2+dy2+1) == COLOUR)) || ((cur_key == UP || cur_key == LEFT) && (getpixel(x2+dx2-1,y2+dy2-1) == COLOUR)))
     {
	     gameover();
	     return;
     }
	if(wall_opacity == 1)
	   if(((cur_key == DOWN || cur_key == RIGHT) && (getpixel(x2+dx2+1,y2+dy2+1) == BORDER)) || ((cur_key == UP || cur_key == LEFT) && (getpixel(x2+dx2-1,y2+dy2-1) == BORDER)) )
     {
	     gameover();
	     return;
     }
	if(bends>0)
    {
	if(first->x == x1 && first->y == y1)
     {
	dx1=first->dx,dy1=first->dy;
	bends--;
	if(bends!=0)
	   releasebend();
	else
	   first->flag=0;
     }
    }
	if(x1>=617 && dx1>0) x1=23;
	if(x2>=617 && dx2>0) x2=23;
	if(y1>=447 && dy1>0) y1=103;
	if(y2>=447 && dy2>0) y2=103;
	if(x1<=23 && dx1<0) x1=617;
	if(x2<=23 && dx2<0) x2=617;
	if(y1<=103 && dy1<0) y1=447;
	if(y2<=103 && dy2<0) y2=447;
	snake();
	delay(11-speed);

	if(((x2-1==x+1 || x2-1==x  || x2-1==x-1) && (y2-1==y+1 || y2-1==y || y2-1==y-1)) || ((x2==x+1 || x2==x  || x2==x-1) && (y2==y+1 || y2==y || y2==y-1)) || ((x2+1==x+1 || x2+1==x  || x2+1==x-1) && (y2+1==y+1 || y2+1==y || y2+1==y-1)))
	     grow();

	if(sflag==1)
    {
	if(((x2-1==sx+1 || x2-1==sx  || x2-1==sx-1) && (y2-1==sy+1 || y2-1==sy || y2-1==sy-1)) || ((x2==sx+1 || x2==sx  || x2==sx-1) && (y2==sy+1 || y2==sy || y2==sy-1)) || ((x2+1==sx+1 || x2+1==sx  || x2+1==sx-1) && (y2+1==sy+1 || y2+1==sy || y2+1==sy-1)))
	    sgrow();
	end=clock();
	duration=(end-start)/CLK_TCK;
	if(duration>=SDURATION) clear_sfood();
    }
	x1=x1+dx1,x2=x2+dx2;
	y1=y1+dy1,y2=y2+dy2;
   }

	key=bioskey(0);

	if(key == ESC || key == LEFT || key == RIGHT || key == UP || key == DOWN || key == CHEATKEY)
 {
	if(key == ESC)
   {
	   paused=1;
	   return;
   }

	else if(key == CHEATKEY)
   {
	cheat = !cheat;
	setfillstyle(1, BLACK);
	bar(20,75,250,95);
	if(cheat == 1)
     {
	setcolor(LIGHTRED);
	outtextxy(20,75,"CHEAT: Anti - Growth Enabled!");
     }
   }
	else if((key != cur_key) && (!(key == RIGHT && cur_key == LEFT)) && (!(key == LEFT && cur_key == RIGHT)) && (!(key == UP && cur_key == DOWN)) && (!(key == DOWN && cur_key == UP)))
		 setbend(key);
 }
	goto restart;
}

void initialize()
{
	first=(bend *)malloc(sizeof(bend));
	first->flag=0;
	x1=200, y1=300, x2=250, y2=300, dx1=1, dx2=1, dy1=0, dy2=0;
	bends=0, cur_key=RIGHT, points=0;
	counter = sflag = 0;
	cheat = 0;
	randomize();
}
void paint()
{
	cleardevice();
	settextstyle(4,0,5);
	setcolor(LIGHTMAGENTA);
	outtextxy(200,10,"S N A K E");
	line(200,65,430,65);
	setlinestyle(0,0,3);
	setcolor(BORDER);
	rectangle(20,100,620,450);
	setcolor(LIGHTGREEN);
	settextstyle(0,0,0);
	outtextxy(500,20,"Points");
	setlinestyle(0,0,1);
	setcolor(WHITE);
	rectangle(495,16,610,30);
	line(551,16,551,30);
	putpoints();
}

void options()
{
	int i,key,exit=1;
	cleardevice();
	do
   {
	setcolor(LIGHTBLUE);
	settextstyle(4,0,5);
	outtextxy(200,10,"OPTIONS");
	setlinestyle(0,0,3);
	line(200,65,396,65);
	settextstyle(7,0,4);
	setcolor(YELLOW);
	outtextxy(100,100,"Speed (");

	setcolor(LIGHTGRAY);
	setlinestyle(0,0,3);

	line(235,110,235,135);
	line(235,110,225,120);
	line(235,110,245,120);

	line(270,110,270,135);
	line(270,135,260,125);
	line(270,135,280,125);

	setcolor(YELLOW);
	outtextxy(285,100,")");

	setlinestyle(0,0,1);
	setfillstyle(1,WHITE);
	setcolor(LIGHTRED);
	for(i=1;i<=10;i++)
	    bar3d((i*50)+50,275,(i*50)+75,275-(i*15),4,1);
	setfillstyle(1,LIGHTRED);
	setcolor(YELLOW);
	for(i=1;i<=speed;i++)
   {
	    if(i<=3) setfillstyle(1,CYAN);
	    else if(i>3 && i<=5) setfillstyle(1,LIGHTGREEN);
	    else if(i>5 && i<=7) setfillstyle(1,LIGHTRED);
	    else setfillstyle(1,RED);
	    bar3d((i*50)+50,275,(i*50)+75,275-(i*15),4,1);
   }

	setcolor(YELLOW);
	outtextxy(100,300,"Wall Opaque (");
	setcolor(LIGHTGRAY);
	setlinestyle(0,0,3);

	line(325,325,360,325);
	line(325,325,335,315);
	line(325,325,335,335);

	line(370,325,405,325);
	line(395,315,405,325);
	line(395,335,405,325);

	setcolor(YELLOW);
	outtextxy(408,300,")");

	setfillstyle(1,BLACK);
	bar(525,300,625,400);
	if(wall_opacity == 0)
    {
	setcolor(CYAN);
	outtextxy(525,300,"No");
    }
	else
    {
	setcolor(LIGHTRED);
	outtextxy(525,300,"Yes");
    }

	while(!kbhit());
	key=bioskey(0);

	if(key == DOWN && speed > 1)
	   speed--;
	else if(key == UP && speed < 10)
	   speed++;
	else if(key == LEFT || key == RIGHT)
    {
	if(wall_opacity == 0) wall_opacity=1;
	else wall_opacity=0;
    }
	else if(key == ESC) exit=0;
   }	while(exit!=0);
	cleardevice();
}

void mainmenu()
{
	int key,exit = 1,i;
	int temp_x,temp_y;
	bend *temp;

	do
 {
	setcolor(LIGHTRED);
	settextstyle(4,0,5);
	outtextxy(200,10,"S N A K E");
	setlinestyle(0,0,3);
	line(200,65,430,65);
	settextstyle(7,0,4);
	setcolor(WHITE);
	outtextxy(200,150,"1 - ");
	outtextxy(200,250,"2 - ");
	outtextxy(200,350,"3 - ");
	setcolor(YELLOW);
	outtextxy(270,150,"New Game");
	outtextxy(270,250,"Options");
	outtextxy(270,350,"Exit");
	setcolor(LIGHTBLUE);
	rectangle(50,100,590,450);
	setlinestyle(0,0,1);
	rectangle(55,105,585,445);

	while(!kbhit());

	key=bioskey(0);

	if(key == 561)   // Key Code for number 1
   {
	initialize();
	paint();
	setcolor(COLOUR);
	setlinestyle(0,0,3);
	line(x1,y1,x2,y2);
	putfood();
	engine();
	cleardevice();
   }

	else if(key == 818) // Key Code for Number 2
		options();

	else if(key == 1075) // Key Code for Number 3
   {
	closegraph();
	exit=0;
   }

	else if(key == ESC && paused == 1)
   {
	paint();
	setlinestyle(0,0,3);
	setcolor(COLOUR);
	temp = first;
	if(bends == 0) line(x1,y1,x2,y2);
	else
    {
	line(x1,y1,temp->x,temp->y);
	for(i=2;i<=bends;i++)
     {
	temp_x=temp->x,temp_y=temp->y;
	temp=temp->next;
	line(temp_x,temp_y,temp->x,temp->y);
     }
	line(x2,y2,temp->x,temp->y);
    }

	putpixel(x+1,y,FOOD);
	putpixel(x-1,y,FOOD);
	putpixel(x,y,FOOD);
	putpixel(x,y+1,FOOD);
	putpixel(x,y-1,FOOD);

	engine();
	cleardevice();
   }
 }      while(exit!=0);
}

void main()
{
	int gmode,gdriver = DETECT;
	initgraph(&gdriver,&gmode,"");
	cleardevice();
	mainmenu();
}