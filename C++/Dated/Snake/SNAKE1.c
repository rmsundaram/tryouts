#include<design.h>

#define LEFT 19200
#define RIGHT 19712
#define UP 18432
#define DOWN 20480

#define ESC 283

#define BORDER LIGHTBLUE
#define COLOUR BROWN
#define FOOD YELLOW
#define BELLY WHITE

struct point
{
	int x,y,dx,dy,key,flag;
	struct point *next;
}	*first;

typedef struct point bend;

int x1=200,y1=300,x2=250,y2=300,dx1=1,dx2=1,dy1=0,dy2=0;
int x,y,bends=0,cur_key=RIGHT,points=0;

void putpoints()
{
	char str[4];
	itoa(points,str,10);
	settextstyle(0,0,0);
	setfillstyle(0,BLACK);
	bar(555,20,600,27);
	setcolor(WHITE);
	outtextxy(556,20,str);
}

void clearbyte()
{
	if(getpixel(x-1,y) == FOOD) putpixel(x-1,y,BLACK);
	if(getpixel(x,y-1) == FOOD) putpixel(x,y-1,BLACK);
	if(getpixel(x,y) == FOOD) putpixel(x,y,BLACK);
	if(getpixel(x+1,y) == FOOD) putpixel(x+1,y,BLACK);
	if(getpixel(x,y+1) == FOOD) putpixel(x,y+1,BLACK);
}

void growstomach()
{
	clearbyte();
	putpixel(x2-1,y2,BELLY);
	putpixel(x2+1,y2,BELLY);
	putpixel(x2,y2,BELLY);
	putpixel(x2,y2+1,BELLY);
	putpixel(x2,y2-1,BELLY);
	if(getpixel(x2-1,y2-1) != BLACK) putpixel(x2-1,y2-1,BELLY);
	if(getpixel(x2+1,y2+1) != BLACK) putpixel(x2+1,y2+1,BELLY);
	if(getpixel(x2+1,y2-1) != BLACK) putpixel(x2+1,y2-1,BELLY);
	if(getpixel(x2-1,y2+1) != BLACK) putpixel(x2-1,y2+1,BELLY);
}

void putfood()
{
	x=random(615);
	y=random(445);
	if(x<=30) x=x+25;
	if(y<=110) y=y+105;
	if(getpixel(x-1,y)==COLOUR || getpixel(x,y-1)==COLOUR || getpixel(x+1,y)==COLOUR || getpixel(x,y+1)==COLOUR)
	   putfood();
	putpixel(x+1,y,FOOD);
	putpixel(x-1,y,FOOD);
	putpixel(x,y,FOOD);
	putpixel(x,y+1,FOOD);
	putpixel(x,y-1,FOOD);
}

void grow()
{
	setlinestyle(0,0,3);
	setcolor(COLOUR);
	if(dx1==-1)
    {
	line(x1-1,y1,x1+10,y1);
	x1=x1+11;
    }
	else if(dx1==1)
    {
	line(x1+1,y1,x1-10,y1);
	x1=x1-11;
    }
	else if(dy1==-1)
    {
	line(x1,y1-1,x1,y1+10);
	y1=y1+11;
    }
	else
    {
	line(x1,y1+1,x1,y1-10);
	y1=y1-11;
    }
	setlinestyle(0,0,3);
	setcolor(BORDER);
	rectangle(20,100,620,450);
	points=points+10;
	putpoints();
	growstomach();
	putfood();
	sound(1200);
	delay(5);
	sound(900);
	delay(10);
	nosound();
}

void terminate(int flag)
{
	settextstyle(6,0,5);
	fflush(stdin);
	if(flag==0)
   {
	putpixel(x2+dx2,y2+dy2,COLOUR);
	if(cur_key == RIGHT || cur_key == LEFT)
     {
	putpixel(x2+dx2,y2+dy2-1,COLOUR);
	putpixel(x2+dx2,y2+dy2+1,COLOUR);
     }
	else
     {
	putpixel(x2+dx2-1,y2+dy2,COLOUR);
	putpixel(x2+dx2+1,y2+dy2,COLOUR);
     }
	while(!kbhit())
     {
	setcolor(LIGHTRED);
	outtextxy(215,50,"GAME OVER");
	delay(250);
	setcolor(BLACK);
	outtextxy(215,50,"GAME OVER");
	delay(250);
     }
   }
	else
   {
	while(!kbhit())
     {
	setcolor(LIGHTRED);
	outtextxy(193,50,"GAME PAUSED");
	delay(250);
	setcolor(BLACK);
	outtextxy(193,50,"GAME PAUSED");
	delay(250);
     }
	if(bioskey(0) == ESC) closegraph();
	else engine(flag);
   }
}

void setbend(int key)
{
	bend *add,*new;
	if(key == DOWN)
	   dx2=0,dy2=1;
	else if(key == UP)
	   dx2=0,dy2=-1;
	else if(key == RIGHT)
	   dx2=1,dy2=0;
	else if(key == LEFT)
	   dx2=-1,dy2=0;
	if(first->flag==0)
   {
	   if(key == DOWN)
	      first->dx=0,first->dy=1;
	   else if(key == UP)
	      first->dx=0,first->dy=-1;
	   else if(key == RIGHT)
	      first->dx=1,first->dy=0;
	   else if(key == LEFT)
	      first->dx=-1,first->dy=0;
	   first->x=x2,first->y=y2;
	   first->key=key;
	   first->flag=1;
	   first->next=NULL;
   }
	else
   {
	   new=(bend *)malloc(sizeof(bend));
	   new->key=key,new->flag=1;
	   new->x=x2,new->y=y2;
	   new->next=NULL;
	   if(key == DOWN)
	      new->dx=0,new->dy=1;
	   else if(key == UP)
	      new->dx=0,new->dy=-1;
	   else if(key == RIGHT)
	      new->dx=1,new->dy=0;
	   else if(key == LEFT)
	      new->dx=-1,new->dy=0;
	   add=first;
	   while(add->next!=NULL)
		 add=add->next;
	   add->next=new;
   }
	cur_key=key;
	bends++;
}

void releasebend()
{
	bend *temp;
	temp=first;
	first=first->next;
	free(temp);
}

void snake()
{
	setlinestyle(0,0,3);
	setcolor(BLACK);
	line(x1,y1,x1+dx1,y1+dy1);
	setcolor(COLOUR);
	line(x2,y2,x2+dx2,y2+dy2);
	putpixel(x1+dx1+dx1,y1+dy1+dy1,BLACK);
	if(cur_key == LEFT || cur_key == RIGHT)
    {
	   putpixel(x2+dx2,y2+dy2-1,BLACK);
	   putpixel(x1+dx1+dx1,y1+dy1+dy1+1,BLACK);
    }
	else
    {
	   putpixel(x2+dx2-1,y2+dy2,BLACK);
	   putpixel(x1+dx1+dx1+1,y1+dy1+dy1,BLACK);
    }
}

int engine(int speed)
{
	int key;
	while(!kbhit())
   {
	if(((cur_key == DOWN || cur_key == RIGHT) && (getpixel(x2+dx2+1,y2+dy2+1) == COLOUR)) || ((cur_key == UP || cur_key == LEFT) && (getpixel(x2+dx2-1,y2+dy2-1) == COLOUR)))
	   return(0);
	if(bends>0)
    {
	if(first->x == x1 && first->y == y1)
     {
	dx1=first->dx,dy1=first->dy;
	bends--;
	if(bends!=0)
	   releasebend();
	else
	   first->flag=0;
     }
    }
	if(x1>=617 && dx1>0) x1=23;
	if(x2>=617 && dx2>0) x2=23;
	if(y1>=447 && dy1>0) y1=103;
	if(y2>=447 && dy2>0) y2=103;
	if(x1<=23 && dx1<0) x1=617;
	if(x2<=23 && dx2<0) x2=617;
	if(y1<=103 && dy1<0) y1=447;
	if(y2<=103 && dy2<0) y2=447;
	snake();
	delay(speed);
	if(((x2-1==x+1 || x2-1==x  || x2-1==x-1) && (y2-1==y+1 || y2-1==y || y2-1==y-1)) || ((x2==x+1 || x2==x  || x2==x-1) && (y2==y+1 || y2==y || y2==y-1)) || ((x2+1==x+1 || x2+1==x  || x2+1==x-1) && (y2+1==y+1 || y2+1==y || y2+1==y-1)))
	    grow();
	x1=x1+dx1,x2=x2+dx2;
	y1=y1+dy1,y2=y2+dy2;
   }
	key=bioskey(0);
	if(key == ESC)
	   return(speed);
	else if(!(key == cur_key) && !(key == RIGHT && cur_key == LEFT) && !(key == LEFT && cur_key == RIGHT) && !(key == UP && cur_key == DOWN) && !(key == DOWN && cur_key == UP))
	   setbend(key);
	engine(speed);
}

void initialize()
{
	first=(bend *)malloc(sizeof(bend));
	first->flag=0;
	randomize();
	settextstyle(4,0,5);
	setcolor(LIGHTMAGENTA);
	outtextxy(200,10,"S N A K E");
	line(200,65,430,65);
	setlinestyle(0,0,3);
	setcolor(BORDER);
	rectangle(20,100,620,450);
	setcolor(LIGHTGREEN);
	settextstyle(0,0,0);
	outtextxy(500,20,"Points");
	setlinestyle(0,0,1);
	setcolor(WHITE);
	rectangle(495,16,610,30);
	line(551,16,551,30);
	putpoints();
	putfood();
	setlinestyle(0,0,3);
	setcolor(COLOUR);
	line(x1,y1,x2,y2);
}

void main()
{
	int gmode,gdriver = DETECT;
	int speed=10,performance;
	initgraph(&gdriver,&gmode,"..//");
	cleardevice();
	initialize();
	performance=engine(speed);
	terminate(performance);
}