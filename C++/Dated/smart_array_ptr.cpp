#include <iostream>
#include <vector>
#include <boost/smart_ptr.hpp>

using std::cout;
using std::endl;

class testee
{
public:
    testee()
    {
        std::cout<<"Default constructor"<<std::endl;
        test = new int;
        *test = 0;
    }
    explicit testee(int val)
    {
        std::cout<<"Single value constructor: "<<val<<std::endl;
        test = new int;
        *test = val;
    }
    void setVal(int val)
    {
        *test = val;
    }
    void printme()
    {
        std::cout<<*test<<std::endl;
    }
    ~testee()
    {
        if(test)
        {
            delete test;
            test = NULL;
            std::cout<<"Yay destructed!"<<std::endl;
        }
    }
    static int m_rectangle[];
private:
    int *test;
};

class myClass
{
    private:
        int m_nDataLength;
        boost::shared_array <int> m_pData;

    public:
        myClass(): m_nDataLength(10), m_pData(new int[m_nDataLength])
        {
        }
        
        void printMe()
        {
            printf("%d\n", m_nDataLength);
            printf("%p\n", m_pData.get());
        }
};

int testee::m_rectangle[] = {0};

int main()
{
    /*int **palette = new int* [5];
    palette[0] = new int[5];
    palette[1] = new int[10];*/

    /*boost::shared_array<boost::shared_array<int> > palette(new boost::shared_array<int> [5]);
    palette[0].reset(new int[10]);
    palette[1].reset(new int[3]);*/

    boost::shared_array< boost::shared_array<testee> > smart_dbl_ptr(new boost::shared_array<testee> [5]);
    smart_dbl_ptr[0].reset(new testee[2]);
    smart_dbl_ptr[1].reset(new testee[3]);

    smart_dbl_ptr[0][0].printme();

    smart_dbl_ptr.reset();

    boost::shared_ptr<int> a(new int);
    *a = 12;
    boost::shared_ptr<int> b(a);
//    b = a;
    cout<<*a<<endl;    
    cout<<*b<<endl;
    cout<<a.use_count()<<endl;
    cout<<b.use_count()<<endl;
    a.reset();
    cout<<a.use_count()<<endl;
    cout<<b.use_count()<<endl;

    cout<<sizeof(testee::m_rectangle)<<endl;
    
    boost::scoped_array <int> parr(new int[5]);
    memset(parr.get(), 0, sizeof(int) * 5);
    printf("%d, %d, %d, %d, %d\n", parr[0], parr[1], parr[2], parr[3], parr[4]);
    
    myClass objMyclass;
    objMyclass.printMe();
    
	return 0;
}
