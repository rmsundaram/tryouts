#include <iostream>
using std::cout;
using std::endl;

class testClass
{
	int test_x, test_y;
public:
	testClass(int x = 0, int y = 0) : 
	test_x(x),
	test_y(y)
	{
		cout << "Constructor called" << endl;
	}

	testClass(const testClass &other)
	{
		cout << "Copy constructor called" << endl;

		test_x = other.test_x;
		test_y = other.test_y;
	}

	testClass& operator =(const testClass &other)
	{
		cout << "Assignment operator called" << endl;
		if(&other != this)
		{
			test_x = other.test_x;
			test_y = other.test_y;
		}
		return *this;
	}

	void printMe()
	{
		cout << test_x << ", " << test_y << endl;
	}
};

int main()
{
	testClass x = testClass(2, 3);
	testClass y = 1;		// how come this works!
	testClass z = testClass(5, 4);
	y.printMe();
}

