#include <iostream>

class ptrMem
{
public:
	int *ptr_;
	int val;
	ptrMem(int *ptr) : 
		ptr_(ptr), val(*ptr)
	{
	}
	
	int getVal()
	{
		return val;
	}
};

int main()
{
	int a = 4;
	ptrMem pM(&a);
	int ptrMem::*mem_ptr = &ptrMem::val;
	int* ptrMem::*ptr = &ptrMem::ptr_;

	std::cout << pM.*mem_ptr << std::endl;
	std::cout << *(pM.*ptr) << std::endl;
	
	int (ptrMem::*fp)(void) = &ptrMem::getVal;
	std::cout << "Value is " << (pM.*fp)() << "!" << std::endl;

	return 0;
}

