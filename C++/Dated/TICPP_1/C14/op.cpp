#include <iostream>
using std::cout;

class X
{
public:
	X() { cout << "X::X()\n"; }
	X(const X &rhs) { cout << "X::X(const X&)\n"; }
	void operator=(const X &rhs) { cout << "X::operator=\n"; }
	void operator*(const X &rhs) { cout << "X::operator*\n"; }
};

class Y
{
public:
	Y() { cout << "Y::Y()\n"; }
	Y(const Y &rhs) { cout << "Y::Y(const Y&)\n"; }
	void operator=(const Y &rhs) { cout << "Y::operator=\n"; }
};

class A : public X
{
	Y y;
public:
	//A(const A &rhs)
	//void operator=(const A &rhs) {}
};

int main()
{
	A objA1, objA2(objA2);
	objA1 = objA2;

	X objX;
	objX = objA1; // - will work ~ since A is a type of X (Car is a vehicle)
	// objA = objX; - won't work ~ since X is not a type of A (Vehicle needn't be a Car)
	
	objA1 * objA2;

	return 0;
}
