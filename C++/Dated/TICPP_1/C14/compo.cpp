#include <iostream>
using namespace std;

class X {
  int i;
public:
  X() { i = 0; }
  void set(int ii) { i = ii; }
  int read() const { return i; }
  int permute() { return i = i * 47; }
};

class Y : public X {
  int i; // Different from X's i
public:
using X::set;
  Y() { i = 0; }
  int change() {
    i = permute(); // Different name call
    return i;
  }
  void set() {
    X::set(0); // Same-name function call
  }
};

int main() {
  cout << "sizeof(X) = " << sizeof(X) << endl;
  cout << "sizeof(Y) = " << sizeof(Y) << endl;
  Y D;
  D.change();
  // X function interface comes through:
  D.read();
  D.permute();
  // Redefined functions hide base versions:
  D.set(12);
} ///:~
