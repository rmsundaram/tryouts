#include <iostream>
#include <sstream>

using std::stringstream;
using std::cout;
using std::endl;

int main()
{
	int arr[7] = {0};
	stringstream ss("0 1 2 3 4 5 6");
	for(int i = 0; i < 7; ++i)
		ss >> arr[i];

	cout << arr[6] << endl;

	int x = 1, 2;
	cout << x << endl;

	return 0;
}

