#include <iostream>
using std::cout;
using std::endl;

class comma_op
{
	int val;
public:
	comma_op(int x = 0) : val(x) {}

	void operator,(const comma_op &rhs)
	{
		cout << this->val << ", " << rhs.val << endl;
	}

	void operator,(const float &rhs) const
	{
		cout << rhs << ", " << this->val << endl;
	}
};

void operator,(const float &lhs, const comma_op &rhs)
{
	cout << "Reached!\n";
	rhs, lhs;
}

int main()
{
	comma_op a(1), b(2);
	a, b;
	12.2f, b;
	
	b(0.2f);

	return 0;
}

