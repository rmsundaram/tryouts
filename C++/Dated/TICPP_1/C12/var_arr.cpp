#include <iostream>
using std::cout;
using std::endl;

class intern
{
};

class X
{
	intern in;

public:	
	operator intern() const
	{
		return in;
	}
};

int main()
{

	return 0;
}

