#include <iostream>
using std::cout;
using std::endl;

const int i[] = { 1, 2, 3, 4 };
//	float f[i[3]]; // g++ cries "error: array bound is not an integer constant"

const int l = 100; const int m = l + 10; int n[m+10];

int main()
{
   const int j[] = { 0, 1, 2, 3 };
//   float g[j[3]];                   // compiler is happy :)

   const char *s = "pepsi";
   cout << j << endl;
   cout << &s << endl;

   return 0;
}

