// compile with -fno-elide-constrcutors to see what REALLY happens :)
// http://stackoverflow.com/questions/2323225/c-copy-constructor-temporaries-and-copy-semantics

#include <iostream>
using std::cout;
using std::endl;

struct C 
{
	C()
	{
		cout << "Default C called!\n";
	}

	C(const C &rhs)
	{
		cout << "CC called!\n";
	}

	C& operator=(const C& rhs)
	{
		cout << "Assignment called!\n";
		return *this;
	}

	void printC()
	{
		cout << "C!" << endl;
	}
};

const C f()
{
	cout << "Entered f()!\n";
	return C();
}

int main()
{
	C a = f();
	C b = a;
	C c;

	a = b = c;

	return 0;
}


