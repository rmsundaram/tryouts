#include <iostream>

class vola
{
	int x;
public:
	void setx(int x) volatile
	{
		this->x = x;
	}

	int getx() const
	{
		return x;
	}
};

int main()
{
	vola x;
	x.setx(10);

	std::cout << x.getx() << std::endl;

	return 0;
}

