#include <iostream>
#include <new>

class A
{
public:
	void* operator new(size_t sz) throw(std::bad_alloc)
	{
		std::cout << "hehe" << std::endl;
		throw std::bad_alloc();
	}

	void* operator new(size_t sz, const std::nothrow_t&) throw()
	{
		std::cout << "haehae" << std::endl;
		return ::operator new(sz);
	}
};

int main()
{
	A *ptr = NULL;

	try
	{
		ptr = new A();
	}
	catch(std::bad_alloc &e)
	{
		std::cout << "huhu" << std::endl;
		ptr = new (std::nothrow) A();
	}

	if(ptr)
	{
		delete ptr;
		std::cout << "haha" << std::endl;
	}
}

