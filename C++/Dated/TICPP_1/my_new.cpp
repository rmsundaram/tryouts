/*
    When global new is over-rided, then for new[] too this is called.
	While per-class new is over-rided it's only called for new and not for 
	new[]; if required, this should be declared as well for that class.
 */

#include <cstdio>
#include <cstdlib>

/*void* operator new(size_t s)
{
	printf("::new size req.: %d\n", s);

	void *vp = NULL;
	vp = malloc(s);
	return vp;
}*/

class a
{
	int x;
public:
	a() {printf("Hehe\n");}
	~a() {printf("Huhu\n");}

	void* operator new(size_t s)
	{
		printf("a::new size req.: %d\n", s);

		void *vp = NULL;
		vp = malloc(s);
		return vp;
	}

	/*void* operator new[](size_t s)
	{
		printf("a::new[] size req.: %d\n", s);

		void *vp = NULL;
		vp = malloc(s);
		return vp;
	}*/
};

int main()
{
	a *A = new a;
	A = new a[2];

	return 0;
}
