#include <iostream>
#include <string>
#include <new>

using std::string;
using std::cout;
using std::endl;

int main()
{
	void *strp = new string("hi");
	delete reinterpret_cast<string*>(strp);
	
	string *sstr = new string [10]();

	for(int i = 0; i < 9; ++i)
	{
		sstr[i] = (i + 20);
		cout << sstr[i] << "\t";
	}

	cout << endl << *sstr << endl;

	delete [] sstr;
	
	char a[] = "";
	cout << sizeof(a) << endl;

	return 0;
}
