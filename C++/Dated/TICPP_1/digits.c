#include <stdio.h>

unsigned int digits_count(unsigned long number)
{
	int count = 0;

	do
	{
		number /= 10;
		++count;
	} while(number);

	return count;
}

int main()
{
	int number = 0;
	scanf("%d", &number);
	if(number < 0) number = -number;
	printf("%d, %d\n", digits_count(number), (2/10));

	return 0;
}

