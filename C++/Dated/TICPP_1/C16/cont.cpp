#include <cstdlib>
#include <iostream>
#include <vector>

void* operator new(size_t sz) throw (std::bad_alloc)
{
	void *vp = NULL;

	if(sz)
	{
		std::cout << "Called for " << sz << std::endl;
		vp = malloc(sz);
		if (!vp) throw std::bad_alloc();
	}

	return vp;
}

void operator delete(void *ptr) throw ()
{
	if(ptr)
	{
		std::cout << "Called del for " << ptr << std::endl;
		free(ptr);
	}
}

int main()
{
	std::vector<int*> a;
	int *x = NULL;
	try
	{
		std::cout << "Line 1" << std::endl;
		x = new int(3);

		// containers internally don't care about the type (be it a pointer or not);
		// it allocates the required space to hold the type and during destruction
		// it deletes the allocated space; if T is A* it calls delete A* else if
		// T is A, then delete A is called. Destructor for a pointer doesn't call
		// any destructor further, while if it's a non-pointer, then the type's
		// destructor is called.
		std::cout << "Line 2" << std::endl;
		a.push_back(x);

		// when push_back's called it allocates space for a int* which is 8;
		// this is confirmed with the below statement
		// int **y = new int*;
		// delete y;
	}
	catch(std::bad_alloc x)
	{
		std::cout << x.what() << std::endl;
	}

	std::cout << "Value stored " << *a[0] << std::endl;

	if(x) delete x;
}

