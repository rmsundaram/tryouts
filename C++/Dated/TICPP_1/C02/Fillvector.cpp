// Fillvector.cpp

#include <string>
#include <vector>
#include <iostream>
#include <fstream>

using namespace std;

int main()
{
	vector<string> v;
	ifstream in_file("Fillvector.cpp");
	string line;
	
	while(getline(in_file, line))
		v.push_back(line);

	for(int i = 0; i < v.size(); ++i)
	{
		cout << i << ": " << v[i] << endl;
		getline(cin, line);
		
		// alternative ways
		//cin.get();
		//cin.ignore();
	}
}

