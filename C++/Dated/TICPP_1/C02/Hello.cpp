#include <iostream>

using std::cout;
using std::dec;
using std::hex;
using std::oct;
using std::endl;

int callme(int, int)
{
	cout<<"Yay!"<<endl;
}

int main()
{
	cout<<"Hellow World!"<<endl;
	callme(1, 2);
	cout<<dec<<12<<endl;
	cout<<hex<<12<<endl;
	cout<<oct<<12<<endl;
	cout<<12.345<<endl;
	cout<<dec<<int('a')<<endl;
	cout<<(char) 27<<endl;
}

