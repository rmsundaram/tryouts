#include <iostream>
#include <iostream>

using std::string;
using std::cout;
using std::endl;

int main()
{
	string s4, s2;
	string s1 = "Hello World!", s3("Good");
	
	s2 = "Hail Earth!";
	s4 = s1 + " " + s2;
	s4 += '\n';
	
	cout << s4 + s3 + "!" << endl;
}

