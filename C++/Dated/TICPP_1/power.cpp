#include <iostream>
using std::cout;
using std::endl;

template <unsigned int X, unsigned int Y>
class XpowY
{
public:
	static const int result = X * (XpowY<X, Y - 1>::result);
};

template <unsigned int N>
class XpowY<N, 0>
{
public:
	static const int result = 1;
};

int main()
{
	cout << XpowY<7, 2>::result << endl;
	return 0;
}

