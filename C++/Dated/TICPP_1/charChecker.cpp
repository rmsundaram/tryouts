#include <iostream>
#include <fstream>

using std::cout;
using std::endl;
using std::ifstream;
using std::string;

int main()
{
	unsigned int count = 0;
	ifstream in("profile.txt", ifstream::in);
	while(in.good() && in.get()) ++count;
	cout << count << endl;
	
	return 0;
}

