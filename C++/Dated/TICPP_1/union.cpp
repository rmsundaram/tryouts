#include <iostream>
#include <inttypes.h>

using namespace std;

union uARGB
{
	int b;

	struct
	{
		short c;
		short d;
	} abc;
};


int main()
{
	short x = 1;
	int y = 1;
	long a = 12;
	long double b = 23;

	cout <<sizeof(x) << endl;
	cout <<sizeof(y) << endl;
	cout <<sizeof(a) << endl;
	cout <<sizeof(b) << endl;
	cout <<sizeof(char) << endl;

	int res = 0;
	res = ++x, ++y, ++a;
	cout << res <<endl;

	uARGB color;
	cout << &color.b << ", "<< sizeof(color.b)<<endl;
	cout << &color.abc.c << ", "<< sizeof(color.abc.c)<<endl;
	cout << &color.abc.d << ", "<< sizeof(color.abc.d)<<endl;

	// different form of != operator for keyboards without them
	// instead of the trigraphs
	if(x not_eq 4) cout << "Yes!" << endl;

	// invalid operation error 3.14 << 2
	cout << (3 << 2) << endl;

	return 0;
}

