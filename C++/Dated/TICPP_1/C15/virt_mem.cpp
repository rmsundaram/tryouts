#include <iostream>

struct A
{
    void add() { subadd(); }
    virtual void subadd() { std::cout << "A::subadd()\n"; }
};

struct B : public A
{
    void subadd() { std::cout << "B::subadd()\n"; }
};

class C : public B
{
public:
    // Not overriding B::add.
    void subadd() { std::cout << "C::subadd()\n"; }
};

int main()
{
	A a;
	B b;
	a.add();
	b.add();

	A *c = new C;
	c->add();
	
	return 0;
}
