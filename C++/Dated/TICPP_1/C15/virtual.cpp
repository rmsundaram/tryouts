#include <iostream>
using std::cout;

class A
{
public:
	virtual void eat() = 0;
};

class B : public A
{
public:
	void eat()
	{
		cout << "B::eat()\n";
	}
};

class C : public B
{
	void eat()
	{
		cout << "C::eat()\n";
	}
};

int main()
{
	B *b = new C;

	// for b, change the vptr point to B's vtable instead of C's
	B bb;
	int *vptr_base = reinterpret_cast<int*>(&bb);
	int *vptr_deri = reinterpret_cast<int*>(b);
	*vptr_deri = *vptr_base;

	b->eat();

	return 0;
}
