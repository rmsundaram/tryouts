#include <stdio.h>
#include <stdlib.h>

typedef struct slnode
{
	struct slnode *next;
	int data;
} node;

void recursive_reverse(node **head, node *prev)
{
	node *next = NULL;

	if(*head)
	{
		next = (*head)->next;
		(*head)->next = prev;
		prev = *head;
		recursive_reverse(&next, prev);

		// fix the head ptr
		if(next) *head = next;
	}

	return;
}

void RecursiveReverse(node** head)
{
	node *first = NULL, *rest = NULL;
	
	if (*head == NULL) return;	// empty list base case

	first = *head;			// suppose first = {1, 2, 3}
	rest = first->next;		// rest = {2, 3}

	if (rest == NULL) return;	// empty rest base case

	RecursiveReverse(&rest);	// Recursively reverse the smaller {2, 3} case
					// after: rest = {3, 2}
	first->next->next = first;	// put the first elem on the end of the list
	first->next = NULL;		// (tricky step -- make a drawing)
	*head = rest;			// fix the head pointer
}

void reverse(node **head)
{
	node *prev = NULL, *next = NULL;

	if(*head == NULL || ((*head)->next == NULL)) return;

	do
	{
		next = (*head)->next;
		(*head)->next = prev;
		prev = *head;
		*head = next;
	} while (*head);

	// fix the head ptr
	*head = prev;
}

void add(node **head, int data)
{
	node *temp = NULL, *new_node = (node*) malloc(sizeof(node));

	new_node->next = NULL;
	new_node->data = data;

	if(*head)
	{
		for(temp = *head; temp->next; temp = temp->next);
		temp->next = new_node;
	}
	else
		*head = new_node;
}

void print(node *head)
{
	node *temp = NULL;
	for(temp = head; temp; temp = temp->next)
		printf("%d\t", temp->data);
	printf("\n");
}

int main()
{
	node *head = NULL;
	unsigned int i = 0;
	
	for(i = 0; i < 10; ++i)
	{
		add(&head, i);
	}

	print(head);

	recursive_reverse(&head, NULL);

	print(head);

	RecursiveReverse(&head);

	print(head);

	reverse(&head);

	print(head);

	return 0;
}

