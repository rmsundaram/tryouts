#include <stdio.h>

/* to print fibonacci series in the reverse order, 
   make this a head recursion */
void print_fibonacci(unsigned int n, unsigned int a, unsigned int b)
{
	printf("%d ", a);

	if(n > 0) print_fibonacci(n - 1, b, a + b);
}

void fibonacci(unsigned int n)
{
	print_fibonacci(n, 0, 1);
}

void fibo_non_rec(unsigned int n)
{
	int a = 0, b = 1, c = 0;

	for(++n; n > 0; --n)
	{
		printf("%d ", a);
		c = a + b;
		a = b;
		b = c;
	}
}

/* prints a number digit by digit */
void print_digits(unsigned int n)
{
	if(n >= 10) print_digits(n/10);

	printf("%d", n%10);
}

int main()
{
	fibonacci(10);
	printf("\n");

	fibo_non_rec(10);
	printf("\n");

	print_digits(13);

	return 0;
}

