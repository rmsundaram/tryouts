#include <iostream>

// http://en.wikipedia.org/wiki/Virtual_inheritance

#ifdef INHERIT_VIRTUALLY
#    define INHERIT_TYPE virtual
#else
#    define INHERIT_TYPE
#endif

struct Animal
{
	virtual void eat() { std::cout << __PRETTY_FUNCTION__ << '\n'; }
};

struct Winged: public INHERIT_TYPE Animal
{
	virtual void eat() { std::cout << __PRETTY_FUNCTION__ << '\n'; }
	virtual void flap() { std::cout << __PRETTY_FUNCTION__ << '\n'; }
};

struct Mammal: public INHERIT_TYPE Animal
{
	virtual void feed() { std::cout << __PRETTY_FUNCTION__ << '\n'; }
};

struct Bat: public Mammal, public Winged
{
};

int main()
{
	Bat baty;

    // Without virtual inheritance by Winged and Mammal, this call would be
    // ambigious since Bat internally would've had two Animals; one per base.
#ifdef INHERIT_VIRTUALLY
	baty.eat();
    // since we've only one Animal::eat which is overridden by Winged::eat, it
    // is chosen without any ambiguity
#else
    // we've two Animal::eats and since we choose the Mammal route, we get
    // Animal::eat as Mammal doesn't provide an override
    baty.Mammal::eat();
    // see below for an alternative syntax involving cast; however, explicit
    // qualification allows for static dispatch, so is preferable.
#endif

    // Upcasting to a base type is usually implicit; no cast required. However,
    // in this situation, without virtual inhertiance, since Bat would've had
    // two Animals, binding an Animal reference to a Bat object would've
    // required a cast to disambiguate.
#ifdef INHERIT_VIRTUALLY
    // only one Animal::eat overridden by Winged which is chosen
    Animal &a = baty;
#else
    // here we choose the Winged route, so we get the Winged's Animal whose eat
    // is overridden by Winged which is chosen
    Animal &a = static_cast<Winged&>(baty);
#endif
    a.eat();
}
