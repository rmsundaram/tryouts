#include <iostream>

int main()
{
	int a, b;
	auto p_diff = &b - &a;
	std::cout << "&a: " << &a << std::endl << "&b: " << &b << std::endl;
	std::cout << "Difference: " << p_diff << std::endl;
}
