int max_sub_sequence_sum( int a[], unsigned int n )
{
	return max_sub_sum( a, 0, n-1 );
}

int max_sub_sum( int a[], int left, int right )
{
	int max_left_sum, max_right_sum;
	int max_left_border_sum, max_right_border_sum;
	int left_border_sum, right_border_sum;
	int center, i;

	if ( left == right )      /* Base Case */
	{
		if( a[left] > 0 )
			return a[left];
		else
	                return 0;
	}

	center = (left + right )/2;

	max_left_sum = max_sub_sum( a, left, center );
	max_right_sum = max_sub_sum( a, center+1, right );
	max_left_border_sum = 0; left_border_sum = 0;

	for( i=center; i>=left; i-- )
	{
		left_border_sum += a[i];
		if( left_border_sum > max_left_border_sum )
			max_left_border_sum = left_border_sum;
	}

	max_right_border_sum = 0; right_border_sum = 0;
	for( i=center+1; i<=right; i++ )
	{
		right_border_sum += a[i];
		if( right_border_sum > max_right_border_sum )
			max_right_border_sum = right_border_sum;
	}
	
	return max3( max_left_sum, max_right_sum, max_left_border_sum + max_right_border_sum );
}

