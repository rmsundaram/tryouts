#include <iostream>
using std::cout;

char* simple_itoa(int n)
{
	char str[8] = "";
	char *ptr = &str[6];
	char *ret_string = NULL;
	bool is_negative = false;
	int digit = 0, length = 0;

	if(n < 0)
	{
		is_negative = true;
		n = -n;
	}

	while(n >= 10)
	{
		digit = n % 10;
		*ptr = '0' + digit;
		--ptr;
		n /= 10;
	}

	*ptr = '0' + n;

	if(is_negative)
	{
		--ptr;
		*ptr = '-';
	}

	length = &str[7] - ptr + 1;

	ret_string = new char[length];
	memcpy(ret_string, ptr, length);

	return ret_string;
}

int main()
{
	int x = -143;
	char *x_str = simple_itoa(x);

	cout << x_str;
	delete x_str;

	return 0;
}
