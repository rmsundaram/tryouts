#include <iostream>
using namespace std;

int main()
{
	try
	{
		try
		{
			throw std::exception();
		}
		catch(std::exception &e)
		{
			cout << "Came to catch 1" << endl;
			throw std::exception();
		}
	}
	catch(std::exception &e)
	{
		cout << "Came to catch 2" << endl;
	}
	cout << "Program exited normally" << endl;
	exit(0);
}
