#include <stdio.h>
#include <malloc.h>

int** getNumbers()
{
	printf("Enters 0!\n");
	int **a = new int* [1];
	a[0] = new int;
	*a[0] = 12;
	printf("Exits 0!\n");
	return a;
}

int* getNumber()
{
	printf("Enters 1!\n");
	int **myNum = getNumbers();
	int *b = myNum[0];
	delete [] myNum;
	printf("Exits 1!\n");
	return b;
}

int main()
{
	int *num = getNumber();
	printf("%d\n", *num);
	delete num;
	
	int x = 20, y = 10;
	
	printf("%d\n", x - y / 5);

	return 0;
}
