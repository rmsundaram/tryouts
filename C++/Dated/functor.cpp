#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

struct strPrinter
{
	void operator()(string &str)
	{
		cout << "strPrinter::operator()" << endl;
		cout << str << endl;
	}
	strPrinter() {}
private:
	strPrinter(const strPrinter&);
	strPrinter& operator=(const strPrinter&);
};

void printStr(string &str)
{
	cout << "printStr" << endl;
	cout << str << endl;
}

template <typename T>
void printMe(string &str, T &x)
{
	cout << "printMe" << endl;
	x(str);
}

int main()
{
	string s = "hi";
	strPrinter strp;
	
	cout << "main" << endl;
	printMe(s, printStr);		// function pointer
	printMe(s, strp);			// function object
}
