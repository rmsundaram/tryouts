#include <iostream>
#include <cstdint>
using namespace std;

// type punning isn't portable, the purpose of unions is something else
// http://stackoverflow.com/questions/2310483/purpose-of-unions-in-c-and-c
union colour
{
	struct
	{
		uint8_t r;
		uint8_t g;
		uint8_t b;
		uint8_t a;
	};
	uint8_t col[4];

	colour() : col{0}
	{
	}
};

class Shape
{
public:
	virtual ~Shape()
	{
	}

	virtual uint32_t getArea() const = 0;

	colour getColour() const
	{
		return col;
	}
private:
	colour col;
};

struct Square : public Shape
{
	Square (uint32_t size = 1) : size(size)
	{
	}

	uint32_t getArea() const
	{
		return size * size;
	}

private:
	uint32_t size;
};

ostream& operator<< (ostream& os, const colour &c)
{
	os << hex;
	os << (int) c.r << " " << (int) c.g << " " << (int) c.b << " " << (int) c.a;
	os << dec;
	return os;
}

int main() {
	cout << "Hello World!" << endl; // prints !!!Hello World!!!

	Square a(20);
	auto x = a.getColour();
	x.r = 0x20;
	cout << "Square colour: " << x << endl;
	cout << "Square area: " << a.getArea() << endl;

	return 0;
}
