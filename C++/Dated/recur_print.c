#include <stdio.h>
#include <stdlib.h>

void print(int n)
{
	if(0 == n)
		printf("0 ");
	else
	{
		print(n - 1);
		printf("%d ", n);
	}
}

int main(int argc, char *argv[])
{
	int x = 0, i =0;

	if(argc >= 2)
	{
		x = atoi(argv[1]);

		for(i = 0; i < x; ++i)
		{
			print(i);
			printf("\n");
		}
	}

	return 0;
}

