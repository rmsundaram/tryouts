#include <cstdio>
#include <malloc.h>

#define ARRAYLENGTH(ptr) ((int)(((int*)ptr)[-1] / sizeof(*ptr)))

#define newArr(T, sz) (T*)newArrFunc(sizeof(T) * sz)

void* newArrFunc(int sz)
{
	int * ptr = (int*) new char[sizeof(int) + sz];
	if(!ptr) return ptr;

	int * iptr = ptr + 1;
	iptr[-1] = (int) sz;
	char* cptr = (char*) iptr;
	return cptr;
}

int main()
{
	int x = 0x7FFFFFFF;
	int *ptr = newArr(int, 5);
	printf("Array Length: %*d\n", 3, ARRAYLENGTH(ptr));

	printf("_msize(ptr): %d\n", _msize(ptr));
	printf("_msize(ptr): %d\n", _msize(--ptr));
	delete [] ptr;

	ptr = NULL;
	printf("Integer Limit: %d\n", x);

	int a[] = {10, 20, 30, 40, 50};
	int *p1 = &a[0], *p2 = &a[2];

	printf("%u\t%u\t%u\t%i\n", p2, *p1, (p1-p2), (p2 > p1));

	int t = 5;
	printf("%d, %d, %d\n", t--, t, t++);
	t = t++;
	printf("%d\n", t);

	return 0;
}
