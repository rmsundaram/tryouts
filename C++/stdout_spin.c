/* gcc -Wall -std=gnu99 stdout_spin.c */

#include <stdio.h>

#if __has_include("windows.h")
    #include <windows.h>
#else
    /* standard C doesn’t sport a sleep(); use POSIX’s nanosleep */
    #include <time.h>

    void Sleep(int milli)
    {
        struct timespec ts = {0, milli * 1000 * 1000};
        nanosleep(&ts, NULL);
    }
#endif

void advance_cursor()
{
  static int pos=0;
  char cursor[4]={'/','-','\\','|'};
  printf("%c\b", cursor[pos]);
  // fflush(stdin) is UB, this is not
  // http://stackoverflow.com/q/2979209
  fflush(stdout);
  pos = (pos+1) % 4;
}

int main(int argc, char **argv)
{
  int i;
  for (i=0; i<100; i++)
  {
    advance_cursor();
    Sleep(250);
  }
  printf("\n");
  return 0;
}
