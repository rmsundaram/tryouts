#include <iostream>

// Came to know about mixins [0] through Casey Muratori’s video [1].  Usually
// it’s achieved by composition; however [2] demos it by flipping vanilla CRTP.
// Rust’s traits are similar to mixins but better (compile-time type safety and
// performance) as per [3].
//
// REFERENCES
//
// [0]: https://en.wikipedia.org/wiki/Mixin
// [1]: https://www.youtube.com/watch?v=rFA1SzRCRWc&t=1267
// [2]: https://www.fluentcpp.com/2017/12/12/mixin-classes-yang-crtp/
// [3]: https://stackoverflow.com/q/49269231

struct Name
{
public:
  Name(std::string firstName, std::string lastName)
    : firstName_(std::move(firstName))
    , lastName_(std::move(lastName)) {}

  void print() const
  {
    std::cout << lastName_ << ", " << firstName_ << '\n';
  }

  std::string firstName_;
  std::string lastName_;
};

template<typename Printable>
struct RepeatPrint : Printable
{
  explicit RepeatPrint(Printable const& printable) : Printable(printable) {}
  void repeat(unsigned int n) const
  {
    while (n-- > 0)
    {
      this->print();
    }
  }
};

// In regular CRTP this would’ve been
//
//    template<typename Printable>
//    struct RepeatPrint
//    {
//       ...
//    };
//
//    class Name : public RepeatPrint<Name>
//    {
//        ...
//    };
//
//    ned.repeat(10);

int main() {
  Name spock("Spock", "Vulcan");
  RepeatPrint(spock).repeat(10);
}
