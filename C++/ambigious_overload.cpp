#include <iostream>

// http://stackoverflow.com/q/13368474/183120
// our case is slightly different; the contenders are T and const T&
void g(const float&)
{
    std::cout << "const float&\n";
}

void g(float)
{
    std::cout << "float\n";
}

int main(int argc, char **)
{
    float a = argc;
    // cast g to disambiguate
    static_cast<void(*)(float)>(g)(a);
}
