# Any Ordering

1. Asymmetry: if (a ⊕ b) then !(b ⊕ a)
2. Transitivity: if (a ⊕ b) and (b ⊕ c), then (a ⊕ c)
3. Totality (comparability): either (a ⊕ b) or (b ⊕ a) holds

# Total Ordering
Equality _is_ equivalence; rather no equivalence.

# Weak Ordering
Equivalence, not necessarily equal; it's a generalization of total ordering.

# Partial Ordering
Either reflexive (non-strict) or irreflexive (strict).  The strict part is what makes it < (irreflexive) as opposed to ≤ (reflexive); (x < x) is alway false, while (x ≤ x) is always true.

## Non-Strict Partial Ordering
4. Reflexive: (a ⊕ a) is always true

## Strict Partial Ordering
4. Irreflexive: (a ⊕ a) is always false

# Strict Weak Ordering
5. Transitive equivalence: if (a ~ b) and (b ~ c), then (a ~ c)
A partial ordering with transitivity of equivalence makes it a strict weak ordering.

## Definition from [Simple Wiki](https://simple.wikipedia.org/wiki/Strict_weak_ordering)
For any two elements that are of different types, the relation holds. On the other hand, there are some elements that cannot be ordered that way, because they are of the same type. An example might be the relation "costs less than", or "is cheaper than". Milk may cost less than bread, and bread may cost less than cake. Two items of different types may cost less than another, but they can otherwise have the same price.

## Examples
1. String comparison can define a total order among them. Introducing the constraint that the comparison is case insensitive makes it a ([since irreflexive] strict) weak ordering with strings differing only by case forming an equivalence class[1](http://math.stackexchange.com/questions/585396/what-is-meaning-of-strict-weak-ordering-in-laymans-term#comment1236527_58547).

2. Egg is cheaper than milk is cheaper than gold. However brand A milk and brand B milk are equivalent (not equal). All such milk brands form a equivalence class.

3. Two different algorithms that sort words by the number of letters in the word may not give the same ordering of the words with the same number of letters, but they will always give an ordering where the four-letter words come before the five-letter-words, and after the three-letter-words.

4. Time will give a strict weak ordering too; events that are equivalent in the timeframe happen at the same time.

# References

* [Strict Weak Ordering](http://stackoverflow.com/a/981299/183120), SO
* [Strict Weak Ordering](http://www.sgi.com/tech/stl/StrictWeakOrdering.html), STL, SGI
* [Strict Weak Ordering](http://math.stackexchange.com/a/585477/51968), Math.SE
* [Weak and Strict Weak Ordering](http://ece.uwaterloo.ca/~dwharder/aads/Abstract_data_types/Weak_ordering/), University of Waterloo
* [Total Order](http://en.wikipedia.org/wiki/Total_order)
* [Weak Ordering](http://en.wikipedia.org/wiki/Weak_ordering)
* [Poset](http://en.wikipedia.org/wiki/Partially_ordered_set)
