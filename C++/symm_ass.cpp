#include <algorithm>
#include <list>
#include <vector>
#include <iostream>
#include <functional>

// REFERENCES:
// Item 24, Effective C++, Third Edition
// Item 26, Effective STL
// http://stackoverflow.com/a/28080616
// C++ Reference Guide, Danny Kalev
// http://web.archive.org/web/20160711000208/http://www.informit.com/guides/
// content.aspx?g=cplusplus&seqNum=523
// TICPP Volume 1, §12 Non-member operators, Basic guidelines recommends:
//   Must-be member: =, [], (), ->, ->*
//   Member: Unary operators, +=, -=, /=, *=, ^=, &=, |=, %=, >, >=, <, <=
//   Non-member: all other binary operators

// Why Comparison Operators Are Declared Friends
// A generic algorithm assumes that the expression o1==o2 works fine, so long as
// there is an accessible operator == that accepts the two operands and returns
// a Boolean value. This symmetric assumption simplifies the design of many
// algorithms and allows C++ to employ various optimizations that aren't always
// possible otherwise.
struct S
{
    int val;
    S(int i) : val(i) {}

    // Symmetric Violation[1]
    // When you declare the overloaded operator a member function, the symmetric
    // assumption is violated. Seemingly, the overloaded operator takes a single
    // parameter of type const T&. The second parameter, i.e., the this pointer,
    // is implicit. The problem is that the implicit parameter and the overt
    // parameter are quite different: the implicit parameter is a pointer,
    // whereas the overt parameter is a reference. An efficient generic
    // algorithm will fail if it tries to pass two Date objects to the
    // overloaded member operator== because the member expects only a single
    // overt parameter.

    // asymmetric operator==
#ifndef SYMMETRIC_OP
    bool operator== (S const &that)
    {
        return val == that.val;
    }
#endif
};

// The symmetric assumption imposes a requirement on the designer of
// user-defined types: in order to qualify as symmetric, a binary operator has
// to be defined an extern function, never as a member function. Recall that if
// your class is never meant to be manipulated by Standard Library algorithms
// and containers, you don't have to abide by the symmetric assumption[1].
#ifdef SYMMETRIC_OP
bool operator==(S const &lhs, S const &rhs)
{
    return lhs.val == rhs.val;
}
#endif

int main()
{
    std::list<S> l = {-4, -3, -2, -1, 0, 1, 2, 3, 4};
    std::vector<std::reference_wrapper<S>> v(l.begin(), l.end());
    auto& x = l.front();
#ifndef SYMMETRIC_OP
    std::cout << "Asymmetric operator\n";
    // Use find_if and a lambda to manually check for equality by unwrapping the
    // reference_wrapper to get S const & (LHS) and do an explicit comparison
    // against S& (RHS) for the member operator== to kick-in; can't just call
    // std::find since S::operator== breaks the symmetric assumption it makes.
    // More specifically, implicit conversion applies to only parameters that
    // are listed in a function’s parameter list.  With a member function, in
    // the LHS is the this pointer whose type is fixed to be the class' type,
    // without any conversion, so even though const std::reference_wrapper<S>&
    // is implicitly convertable to S, it doesn't happen here.
    v.erase(std::find_if(v.cbegin(), v.cend(),
        [&](const std::reference_wrapper<S> &i)
        {
            return i.get() == x;
        }));
#else
    std::cout << "Symmetric operator\n";
    // free-standing, non-member operator== abides by the symmetric assumption
    v.erase(std::find(v.cbegin(), v.cend(), x));
#endif
    std::cout << v[0].get().val;
}
