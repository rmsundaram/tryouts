#include <iostream>
#include <memory>

struct IAnimal
{
    virtual ~IAnimal() { }
    virtual void eat() = 0;
};

struct IBat : public IAnimal
{
    virtual ~IBat() { }
    virtual void fly() = 0;
};

/*

struct Animal : public IAnimal
{
    void eat() override { std::cout << __PRETTY_FUNCTION__ << '\n'; }
};

struct Bat : public Animal, public IBat
{
    void fly() override { std::cout << __PRETTY_FUNCTION__ << '\n'; }
};

error: cannot declare variable 'b' to be of abstract type 'Bat'
     Bat b;
         ^
note:   because the following virtual functions are pure within 'Bat':
 struct Bat : public Animal, public IBat
        ^
note:   virtual void IAnimal::eat()
     virtual void eat() = 0;
                  ^
This doesn't work, since IBat would require IAnimal::eat to
be implemented by Bat and not by Animal which is inherited by Bat
________________________________________________________________________________
Solution 1: Templates (perhaps the cleanest)

Optionally inherit from enabled_shared_from_this at base (Animal) if it or its
descendants need a shared_ptr to the underlying object.

*/
template <typename Interface>
struct Animal : public Interface, public std::enable_shared_from_this<Interface>
{
    void eat() override { std::cout << __PRETTY_FUNCTION__ << '\n'; }
};

// This is similar to, but not, Curiously Recurring Template Pattern (CRTP).  In
// CRTP we inherit from actual derived class, no the interface it’d adhere to.
struct Bat : public Animal<IBat>
{
    void fly() override { std::cout << __PRETTY_FUNCTION__ << '\n'; }
};

/*

Solution 2: Virtual inheritance

Need shared_from_this?  Inherit enable_shared_from_this at A.  B’s methods can
std::dynamic_pointer_cast<Bat>(A::shared_from_this()).

struct IAnimal
{
    virtual ~IAnimal() { }
    virtual void eat() = 0;
};

// This involves even the interface to be inheriting virtually
// since we need eat()'s implementation to be reused (see error above)
// it's class needs to be inherited virtually
struct IBat : virtual public IAnimal
{
    virtual ~IBat() { }
    virtual void fly() = 0;
};

// here too virtual inheritance since eat()'s what we need
struct A : virtual public IAnimal
{
    void eat() override { std::cout << __PRETTY_FUNCTION__ << '\n'; }
};

// note that IBat and it's concrete class has no virtual inheritance
// by itself; it's only the IAnimal and Animal class' part of it that
// has virtual inheritance
struct B : public A, public IBat
{
    void fly() override { std::cout << __PRETTY_FUNCTION__ << '\n'; }
};

*/

int main()
{
    // upcast
    Bat bat;
    IBat* ib = &bat;
    ib->fly();
    IAnimal* ia = &bat;
    ia->eat();

    // downcast
    std::shared_ptr<IAnimal> spAnimal(new Bat);
    assert(std::dynamic_pointer_cast<Bat>(spAnimal) != nullptr);
    assert(std::dynamic_pointer_cast<Animal<IBat>>(spAnimal) != nullptr);
    assert(std::dynamic_pointer_cast<Animal<IAnimal>>(spAnimal) == nullptr);
}
