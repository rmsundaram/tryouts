// g++ -Wall -std=c++11 -g -pedantic -o breakpoint breakpoint.cpp

// Continuable assertions (and breakpoints) if < C++26 as it
// introduced <debugging> with std::breakpoint{,_if_debugging}().
//
// Refer
//   1. https://nullprogram.com/blog/2024/01/28/
//   2. https://en.cppreference.com/w/cpp/utility/breakpoint
// See Also
//   1. https://github.com/nemequ/portable-snippets/
//   2. https://github.com/scottt/debugbreak
//   3. https://stackoverflow.com/questions/173618/

// Clang has __builtin_debugtrap and MSVC has __debugbreak
#if defined(__has_builtin) && __has_builtin(__builtin_debugtrap)
#  define breakpoint() __builtin_debugtrap()
#elif defined(_MSC_VER)
#  define breakpoint() __debugbreak()
#else
#  define breakpoint()  asm ("int3; nop")  // x86 only
#endif

#ifdef NDEBUG
#define assert(c)
#else
#define assert(c)  do if (!(c)) breakpoint(); while (0)
#endif

int main() {
  for (auto i = 0u; i < 100; ++i) {
    if (i >= 10)
      breakpoint();
  }
}
