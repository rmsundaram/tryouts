#include <unordered_map>
#include <iostream>

template <typename FuncType, FuncType *fn>
struct memoized_fn {
    int operator()(int x) {
        auto const it = m.find(x);
        if (it != m.cend())
            return it->second;
        auto const ret = fn(x);
        m[x] = ret;
        return ret;
    }

private:
    std::unordered_map<int, int> m;
};

int f(int i) {
    std::cout << __PRETTY_FUNCTION__ << '\n';
    return i;
}

using fMemoized = memoized_fn<decltype(f), &f>;

int main() {
    fMemoized mf;
    std::cout << mf(2) << '\n';
    std::cout << mf(1 + 1) << '\n';
}
