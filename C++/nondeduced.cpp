#include <vector>
#include <algorithm>
#include <functional>
#include <iostream>

/*
 * The best option to receive a callable is
 *
 *     template<typename T, typename Func>
 *     std::vector<T> keep(const std::vector<T> &original,
 *                         Func useful);
 *
 * Works with lambdas, functionoids, function pointers and std::functions.
 * However, this tryout is about making
 *
 *     template<typename T>
 *     std::vector<T> keep(const std::vector<T> &original,
 *                         std::function<bool(const T &)> useful);
 *
 * work without caller casting lambda to std::function<bool(const T&)>.
 * Below works but input function prototype is no longer generic.
 *
 *     template<typename T>
 *     std::vector<T> keep(const std::vector<T> &original,
 *                         std::function<bool(const int &)> useful);
 *
 * Learned while answering http://stackoverflow.com/q/32840369.
 */

#if (__cplusplus < 202002L || (defined(_MSVC_LANG) && (_MSVC_LANG < 202002L)))

// boost provides boost::identity for id
template <typename T> struct id { typedef T type; };

template <typename T> using nondeduced = typename id<T>::type;
// Make the second parameter a non-deduced context for the template parameters
// therein: http://stackoverflow.com/a/7609835
template<typename T>
std::vector<T> keep(const std::vector<T> &original,
                    std::function<nondeduced<bool(const T &)>> useful)
{
    std::vector<T> out;
    for(T const &item : original)
        if(useful(item))
            out.push_back(item);
    return out;
}

#else

// C++20 abbreviated function template; shorthand for `U useful` where
// U is a new template typename.  std::vector<auto> won’t work though [1].
// [1]: https://stackoverflow.com/a/60358715/183120
template <typename T>
std::vector<T> keep(const std::vector<T>& original, auto useful)
{
    std::vector<T> out;
    for(auto const &item : original)
        if(useful(item))
            out.push_back(item);
    return out;
}

#endif  // pre-C++20

int main()
{
    std::vector<int> a = { 4, 6, 2, -5, 3, -8, 13, -11, 27 };
    a = keep(a, [](const int& x) { return x > 0; });
    for(int y : a)
        std::cout << y << '\t';
}
