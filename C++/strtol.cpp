// §2.3 Numeric/String Conversions, Write Great Code, Volume 1

#include <iostream>
#include <cstdlib>
#include <cstdint>
#include <array>

struct radix {
    uint8_t r;
    explicit radix(uint8_t rad) : r(rad) { }
};

std::ostream& operator<<(std::ostream &os, radix r) {
    char const *nos[] = { u8"₀", u8"₁", u8"₂", u8"₃", u8"₄", u8"₅", u8"₆", u8"₇", u8"₈", u8"₉"};
    std::array<uint8_t, 3> digits;
    unsigned i = 0u;
    // also based on §2.3, Write Great Code, Volume 1
    while (r.r) {
        auto const d = div(r.r, 10);
        digits[i++] = d.rem;
        r.r = d.quot;
    }
    for (int j = i - 1; j >= 0; --j)
        os << nos[digits[j]];
    return os;
}

int main(int argc, char *argv[]) {
    if (argc < 3) {
        std::cout << "Usage: dec radix number\n";
        return EXIT_SUCCESS;
    }
    auto const r = strtoull(argv[1], nullptr, 0);
    if ((r > 36u) || (r < 2u)) {
        std::cerr << "Error: Radix unsupported! Range: [2, 36] ∈ ℤ";
        return EXIT_FAILURE;
    }
    bool const is_neg = (argv[2][0] == '-');
    char *i = argv[2] + (is_neg ? 1 : 0);
    uint32_t n = 0u;
    while (*i) {
        char const ch = *i++;
        if (!(isdigit(ch) || isalpha(ch))) {
            std::cerr << "Error: Invalid character for digit supplied: " << ch << '\n';
            return EXIT_FAILURE;
        }
        uint8_t d = ch - (isdigit(ch) ? '0' : islower(ch) ? 'a' : 'A');
        if (d >= r) {
            std::cerr << "Error: Digit greater than radix: " << ch << '\n';
            return EXIT_FAILURE;
        }

        // based on §2.3, Write Great Code, Volume 1
        n = d + (n * r);
    }
    std::cout << argv[2] << radix(r) << " = "
              << (is_neg ? '-' : '\0')
              << n << radix(10) << '\n';
    auto const v = strtol(argv[2], nullptr, r);
    std::cout << "std::strtol: " << v << '\n';
}
