// http://stackoverflow.com/a/19442790/183120

#include <iostream>
#include <limits>

int main()
{
	std::cout << "Press ENTER(s) to exit..." << std::endl;
#ifndef BRUTE_FORCE
    // flush input in buffer
	// http://stackoverflow.com/a/257182/183120
    std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

	// std::cin.sync is an alternative but its implementation isn't consistent
	// http://stackoverflow.com/questions/10585392/the-difference-between-cin-ignore-and-cin-sync

    // blocking call to fresh input from user
	std::cin.get();
#else
	char ch;
	while(std::cin.get(ch) && ch != '\n');
#endif
}
