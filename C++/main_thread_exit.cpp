#include <iostream>
#include <thread>
#include <chrono>

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN

// list from Windows.h
#define NOGDICAPMASKS
#define NOVIRTUALKEYCODES
#define NOWINMESSAGES
#define NOWINSTYLES
#define NOSYSMETRICS
#define NOMENUS
#define NOICONS
#define NOKEYSTATES
#define NOSYSCOMMANDS
#define NORASTEROPS
#define NOSHOWWINDOW
#define OEMRESOURCE
#define NOATOM
#define NOCLIPBOARD
#define NOCOLOR
#define NOCTLMGR
#define NODRAWTEXT
#define NOGDI
#define NOKERNEL
#define NOUSER
#define NONLS
#define NOMB
#define NOMEMMGR
#define NOMETAFILE
#define NOMSG
#define NOOPENFILE
#define NOSCROLL
#define NOSERVICE
#define NOSOUND
#define NOTEXTMETRIC
#define NOWH
#define NOWINOFFSETS
#define NOCOMM
#define NOKANJI
#define NOHELP
#define NOPROFILER
#define NODEFERWINDOWPOS
#define NOMCX

#ifndef NOMINMAX
#  define NOMINMAX
#endif

#include <windows.h>
#endif

void f(const char* name) {
  using namespace std::chrono_literals;

  std::cout << name << " start\n";
  std::this_thread::sleep_for(2000ms);
  std::cout << name << " stop\n";
}

int main() {
  std::cout << "main start\n";
  std::thread t1(f, "t1");
  std::cout << "main stop\n";

  // C (and by inheritance C++) mandates return from main is equivalent to
  // calling exit() i.e. process exit which prempts running threads.  Instead
  // calling ExitThread on Win32 exits the thread but not the process.  This
  // means the process awaits all running threads before completion.
  //
  // References
  // 1. https://devblogs.microsoft.com/oldnewthing/20100827-00/?p=13023
  // 2. https://stackoverflow.com/q/6363849/183120
#ifndef _WIN32
  t1.join();
#else
  std::cout << "Leapt into Win32 world!\n";
  ExitThread(0);
#endif
}
