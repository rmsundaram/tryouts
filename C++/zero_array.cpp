#include <iostream>

using std::cout;
using std::endl;

// http://stackoverflow.com/q/1087042
// http://herbsutter.com/2009/09/02/when-is-a-zero-length-array-okay/
int main()
{
	int *x = new int[0];
	if(x)
	{
		delete x;
		cout << "Deleted!" << endl;
	}
}
