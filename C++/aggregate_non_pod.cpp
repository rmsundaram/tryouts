#include <iostream>
#include <memory>
#include <type_traits>

using namespace std;

// this is not a POD (since shared_ptr is not a POD) but an aggregate type as (refer section 8.5.1)
// no user-provided ctors
// no private or protected non-static members
// no virtual functions
// brace-or-equal-initializers for member variables allowed since C++14
// public, base class is allowed by C++17
struct MyHandle
{
    std::shared_ptr<int> handle_;
    int x;
};

int main()
{
    // https://en.cppreference.com/w/cpp/language/classes#POD_class
    // C++20 deprecates std::is_pod; defines it as (std::is_trivial && std::is_standard_layout)
    cout << is_trivial<MyHandle>::value << endl;           // shared_ptr -> non-trivial ctor
	cout << is_standard_layout<MyHandle>::value << endl;   // has standard layout though
    cout << is_aggregate<MyHandle>::value << endl;
	// since MyHandle is an aggregate type, aggregate initialization is allowed
	MyHandle x{nullptr, 1};
}
