#include <iostream>
#include <vector>

// http://stackoverflow.com/questions/20186084/evaluation-order-initialization-array-in-c

template <typename... T>
auto make_vector(T... t) -> std::vector<typename std::common_type<T...>::type>
{
	std::vector<typename std::common_type<T...>::type> v;
	v.reserve(sizeof...(T));

	using list = int[];
	// comma operator used to make a throw-away variable; when it's evaluated, the side effect is to evaluate the
	// operand before it too, which makes the push_back
	list{(v.push_back(std::move(t)), 0)...};

	// this would print n 0s
	// for(auto const &i : a)
	// {
		// std::cout << i << tab;
	// }

	return v;
}

// self-written; first tried static_cast, then found forward to be even better
template <typename... T>
auto create_vector(T... t) -> std::vector<typename std::common_type<T...>::type>
{
	return {std::forward<typename std::common_type<T...>::type>(t)...};
}

std::ostream& tab(std::ostream& os)
{
	return os << '\t';
}

int main()
{
	auto v = make_vector(2, 3.0, 'a', 7UL);
	for(auto e : v)
		std::cout << e << tab;

	std::cout << std::endl;

	auto v1 = create_vector(2, 3.0, 'a', 7UL);
	for(auto e : v1)
		std::cout << e << tab;
}
