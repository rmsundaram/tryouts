// g++ -fdump-class-hierarchy cstyle_downcast.cpp

#include <iostream>
#include <cstdio>

using std::cout;

// usually vptr preceeds all members
//    vptr   age
// +-------+-----+
// | void* | int |
// +-------+-----+
struct cat
{
    int age = 1;
    virtual ~cat() { }
};

//    vptr   age   speed
// +-------+-----+-----+
// | void* | int | int |
// +-------+-----+-----+
struct tiger : cat
{
    int speed = 2;
    void run() { cout << "Speed = " << speed << '\n'; }
};

// no vptr here since this has no virtual methods
//    stripes
//    +-----+
//    | int |
//    +-----+
struct striped
{
    int stripes = 10;
    void stripe_count() { cout << "Stripes = " << stripes << '\n'; }
};

//    vptr   age  stripes
// +-------+-----+-----+
// | void* | int | int |
// +-------+-----+-----+
struct cheeta : private cat, private striped
{
};

int main()
{
    cat c;
    // C-style down-casting leading to undefined behaviour had it been static or
    // dynamic_cast, this could've been avoided
    ((tiger*)&c)->run();
    // prints garbage; it tries to access speed in a cat and access out of
    // bounds memeory

    cheeta p;
    // p.stripe_count();   error: stripe_count() is inaccessible

    // this compiles but leads to undefined behaviour since this is not what we
    // want: it just forcefully converts the pointer's type into striped* but
    // not after offsetting the cat part to get to the striped part as
    // static_cast would have rightly done.
    reinterpret_cast<striped*>(&p)->stripe_count();

    // however, static_cast won't work; striped is inaccessible as earlier
    // static_cast<striped*>(&p)->stripe_count();

    // C-style cast can convert to a private base; the only cast which can
    // safely do this i.e. well-defined
    // http://stackoverflow.com/q/10770044
    ((striped*)&p)->stripe_count();

    std::cout << sizeof(cat) << '\n';     // usually 16 (= 8 + 4 + 4 padding)
    std::cout << sizeof(tiger) << '\n';   // usually 16 (= 8 + 4 + 4)
    std::cout << sizeof(striped) << '\n'; // usually 4  (= sizeof(int))
    std::cout << sizeof(cheeta) << '\n';  // usually 16 (= 8 + 4 + 4)
}
