#include <cstdio>
#include <thread>

static char bar[] = "======================================="
                    "======================================>";
const char bars[][10] = { "abc", "praise", "open" , "logic", "array", "C & C++" };

template <typename T>
inline constexpr size_t strLiteralLength(const T&)
{
	return std::extent<T>::value - 1;
}

template <typename T, size_t N>
inline constexpr size_t
arraySize(const T (&arrName) [N])
{
    return N;
}

int main() {
    using namespace std::chrono_literals;

    int i = strLiteralLength(bar) - 1;
    // NOTE: unsigned (size_t) here will lead to overflow as we decrement
    for (; i >= 0; i--) {
        printf("[%s]\r", &bar[i]);
        fflush(stdout);
        std::this_thread::sleep_for(25ms);
    }
    printf("\n");

    for (i = 0; i < static_cast<signed>(arraySize(bars)); i++) {
		printf("%7s\r", bars[i]);
        fflush(stdout);
        std::this_thread::sleep_for(200ms);
    }

    printf("\n");
    return 0;
}
