#include <iostream>

struct A
{
    A() { std::cout << "A()\n"; }

    virtual ~A() = default;
    A(const A&) = delete;
    A& operator=(const A&) = delete;
    A(const A&&) = delete;
    A& operator=(A&&) = delete;

    virtual int access() { return this->value; }

    int value = 0;
};

struct B : public A
{
    virtual int access() { return this->better_value; }

    int better_value = 2;
};

int main() {
    B o;
    // C++ gotcha: a cast giving rise to a new object!
    std::cout << static_cast<A>(o).value;
    // C++ value semantics leads us to this situation
}
