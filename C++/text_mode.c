#include <stdio.h>

// the point is that \n will behave differently for
// different operating systems due to text mode

int main()
{
	FILE *fp = fopen("temp.tmp", "w");
	fprintf(fp, "a\nb\nc\n");
	fclose(fp);
	fp = fopen("temp.tmp", "r");
	char line[3] = "";
	while(fscanf(fp, "%s\n", line) != EOF)
	{
		printf("%s", line);
	}
	return 0;
}
