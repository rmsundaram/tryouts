// This compiles and links fine, demonstrating client code that lazy loads DLL
// at run time, not launch time, builds with just the interface, without
// implementation. Reason: virtual functions get resolved at run time; no
// definition needed at compile-time.  Delete virtual to see linker errors.
// https://www.codeproject.com/articles/28969/howto-export-c-classes-from-a-dll
//         #CppMatureApproach

// BEGIN plugin interface
// App code defining pluign interface to be implementated by plugin authors.
// Note: no data is defined inside the class. When using different compilers
// offsetof(m_member) needn’t be the same, as it’s implementaion-defined.
// Use only C datatypes (like POD structs) in DLL interface. Opaque pointers
// with only forward declaration are okay; IPlugin is an opaque pointer[1],
// since its implementation is unknown.
//
// [1]: https://stackoverflow.com/q/7553750/183120

struct IPlugin
{
  virtual ~IPlugin() { }          // virtual is mandatory; pure virtual isn’t
  virtual void setVal(int) = 0;
  virtual int val() const = 0;
};

// Optionally, define an IApplication to be implemented by (some class in) the
// application. Pass it to plugin through create function. This interface might
// be needed for plugin to talk to application; it might not be feasible to pass
// all data plugin needs through IPlugin’s methods by application in one shot.

// One needs a C interface to the actual create and destroy function for
// 1. there’s no (established) C++ ABI standard; C has a de facto one and hence
//    calling conventions are fairly similar between compilers; use it to
//    create. C++ objects, work atop those objects thereon.
// 2. circumventing varying name mangling styles among toolchains; extern "C" is
//    a linkage specification, needed only in declaration, strictly speaking.
// 3. the CRT used by the client and the DLL code might be different and hence
//    the responsibility of freeing can’t be delegated to the opposite party;
//    irrespective of caller or callee clean-up, to be done by the same entity.
// 4. Different C runtimes → different malloc + free implementations e.g. one
//    might store chunk length before returned address, other mightn’t.
// Refer https://stackoverflow.com/a/8534952/183120 for (1) and (2).
//       https://stackoverflow.com/a/2171570/183120 for (2).
// Refer dll_alloc.cpp for (3).

// `extern "C" { #include "my_lib.h" }` in implementation (.cpp) is cleaner
extern "C" {
  IPlugin* create();
  void destroy(IPlugin*);
}
// END plugin interface


#include <memory>

IPlugin* (*create_fp)(void);
void (*destroy_fp)(IPlugin*);

struct PluginDeleter {
  void operator()(IPlugin* p) {
    destroy_fp(p);
  }
};

int main() {
  // dlopen plugin dynamic library at runtime, look up symbols for create and
  // destroy, make function pointers and call them to get a new instance of
  // IPlugin. Call create to get some CPluginXYZ object at run-time that adheres
  // to IPlugin. Write a struct DeleterFunctor which calls CPluginXYZ’s destroy
  // (obtained from dlsym). Wrap in a unique_ptr<IPlugin, DeleterFunctor>.  Each
  // plugin will be deleted properly with its own destroy implementation.  At
  // the plugin side, destroy internally calls delete IPlugin* but since
  // IPlugin::~IPlugin is virtual ~CPluginXYZ is called.

  std::unique_ptr<IPlugin, PluginDeleter> plugin(create_fp());
  p->setVal(1);
  p->val();
}
