#include <iostream>
#include <limits>
#include <cmath>

bool sign(int x) {
    return (x < 0);
}

bool isPowerOf2(unsigned x) {
    // Check for 0 as it’d be (rightly) deemed POT otherwise e.g. for a 4-bit
    // integral type -1 in two’s complement = 0b1111 & 0b0000 = 0
    return x && ((x & (x - 1)) == 0);
}

// TODO: make this branchless
unsigned myAbs(int x) {
    return (x < 0) ? -x : x;
}

// y is assumed to be a power of 2
int idivByPow2(int x, int y, int& q) {
    // xor is used even in hardwares to find the sign of the result of
    // multiplication and division
    bool const isNeg = sign(x) ^ sign(y);
    x = myAbs(x);
    y = myAbs(y);
    unsigned n = y - 1;
    q = x;
    // for division by a power of 2, the dividend should be right shifted power
    // times; however, the actual value of power isn't needed, so ulog2 can be
    // avoided. Doing y - 1 would give a number with its last n bits set, where
    // n = log2(y); shift till n becomes zero.
    while (n) {
        q >>= 1;
        n >>= 1;
    }
    q *= (isNeg ? -1 : 1);
    // use the same y - 1 to mask out the remainder bits from dividend
    auto const r = x & (y - 1);
    return isNeg ? -r : r;
}

int main(int argc, char** argv) {
    if (argc >= 3) {
        auto const y = std::strtol(argv[2], nullptr, 0);
        if ((y == 0) || !isPowerOf2(myAbs(y))) {
            std::cerr << "Divisor cannot be 0 or NPOT!\n";
            return 0;
        }
        auto const x = std::strtol(argv[1], nullptr, 0);
        std::cout << myAbs(x) << '\t' << myAbs(y) << '\n';
        auto q = 0;
        auto r = idivByPow2(x, y, q);
        std::cout << x << " = " << myAbs(y) << " * " << q << ' ';
        if (r > 0)
            std::cout << "+ ";
        std::cout << r << '\n';
    }
    else
        std::cerr << "usage: idiv x y\n";
}
