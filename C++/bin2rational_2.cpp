#include <iostream>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <iomanip>
#include <limits>
#include <sstream>
#include <stdexcept>
#include <iterator>
#include <numeric>

template<typename T>
T pow2(unsigned char n)
{
    // since unsigned it cannot be lesser than 0, so it's ok to check only for the upper limit
    auto constexpr n_shifts = std::numeric_limits<T>::digits - 1;
    if(n > n_shifts)
    {
        std::stringstream ss;
        ss << "Error: Left shift for "
           << n << " bits when only "
           << n_shifts << "are allowed.";
        throw std::overflow_error(ss.str());
    }
    T v{1u};
    return v << n;
}

int get_binary_digit(char ch)
{
    const std::string err = "Invalid binary digit " + std::string(1, ch);
    return (ch == '1') ? 1 : (ch == '0') ? 0 : throw std::invalid_argument(err);
}

template <typename T>
T int_bin2dec(const std::string &str_integral)
{
    signed power = 0;
    const T integral = std::accumulate(str_integral.crbegin(), str_integral.crend(), T{},
    [&](T accum, char ch)
    {
        const auto digit = get_binary_digit(ch);
#ifndef NDEBUG
        std::cout << power << '\t' << pow2<T>(power) << std::endl;
#endif
        return accum + (digit * pow2<T>(power++));
    });
    return integral;
}

template <typename T>
void fract_bin2dec(const std::string &str_fraction,
                   T &num,
                   T &den)
{
    num = 0u, den = 1u;
    signed power = -1;
    for_each(str_fraction.cbegin(), str_fraction.cend(), [&](char ch)
    {
        const auto numerator = get_binary_digit(ch);
        if(numerator)
        {
            const auto denominator = pow2<T>(-power);
            const auto scalar = denominator / den;
            num = (num * scalar) + 1u;
            den = denominator;
#ifndef NDEBUG
        std::cout << power << "\t 1/" << denominator << '\t' << num << " / " << den << std::endl;
#endif
        }
        --power;
    });
}

template <typename T>
void print_fraction(bool negative,
                    T integral,
                    T num,
                    T den)
{
    if (negative) std::cout << "-[";
    if (integral) std::cout << integral << " + (";
    std::cout << num
              << " / "
              << den;
    if (integral) std::cout << ")";
    if (negative) std::cout << "]";
}

template <typename fraction_type, typename T>
void print_real(bool negative,
                T integral,
                T num,
                T den)
{
    if (negative) std::cout << "-";
    std::cout << std::fixed << std::setprecision(std::numeric_limits<fraction_type>::max_digits10)
              << (integral + (num / static_cast<fraction_type>(den)))
              << std::endl;
}

// usage: bin2rational 0.000110011001100110011001101
int main(int argc, char *argv[])
{
    if(argc < 2)
    {
        std::cerr << "Insufficient data" << std::endl;
        std::cerr << "Usage: bin2rational <fractional binary number>" << std::endl;
        return -1;
    }

    auto const *binary = argv[1];
    const bool negative = ('-' == *binary);
    binary += negative ? 1 : 0;

    std::stringstream ss{binary};
    std::string int_part, fract_part;
    if(getline(ss, int_part, '.'))
    {
        if(getline(ss, fract_part, '.'))
        {
            std::string dummy;
            if(getline(ss, dummy, '.'))
                std::cerr << "Invalid format!" << std::endl;
        }
    }

    using integer_type = unsigned long long;
    const auto integral = int_bin2dec<integer_type>(int_part);
    integer_type fract_num = {}, fract_den = {};
    fract_bin2dec(fract_part, fract_num, fract_den);

    if(integral || fract_num)
    {
        print_fraction(negative, integral, fract_num, fract_den);
        std::cout << " = ";
        print_real<long double>(negative, integral, fract_num, fract_den);
    }
    else
        std::cout << "0" << std::endl;
}
