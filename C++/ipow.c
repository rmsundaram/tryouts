#include <stdio.h>
#include <stdint.h>

#if _MSC_VER
#include <inttypes.h>
#endif

// exponentiation by squaring; O(log₂ n)
int powr(int x, unsigned int n)
{
	if(n == 0) return 1;

	if((n & 1) == 0)
		return powr(x * x, n / 2);   // verified tail-call optimization
	else
		return (x * powr(x * x, (n - 1) / 2));
}

// Orson Peters (nightcracker)
// http://gist.github.com/orlp/3551590
int64_t ipow(int32_t base, uint8_t exp) {
    static const uint8_t highest_bit_set[] = {
        0, 1, 2, 2, 3, 3, 3, 3,
        4, 4, 4, 4, 4, 4, 4, 4,
        5, 5, 5, 5, 5, 5, 5, 5,
        5, 5, 5, 5, 5, 5, 5, 5,
        6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 6, 6, 6, 6,
        6, 6, 6, 6, 6, 6, 6, 255, // anything past 63 is a guaranteed overflow with base > 1
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
        255, 255, 255, 255, 255, 255, 255, 255,
    };

    uint64_t result = 1;

    switch (highest_bit_set[exp]) {
    case 255: // we use 255 as an overflow marker and return 0 on overflow/underflow
        if (base == 1) {
            return 1;
        }

        if (base == -1) {
            return 1 - 2 * (exp & 1);
        }

        return 0;
    case 6:
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    case 5:
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    case 4:
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    case 3:
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    case 2:
        if (exp & 1) result *= base;
        exp >>= 1;
        base *= base;
    case 1:
        if (exp & 1) result *= base;
    default:
        return result;
    }
}

int main()
{
    printf("%d\n", powr(-3, 3));

    const int64_t res = ipow(-3, 3);
#ifdef _MSC_VER
	printf("%"PRIi64"\n", res);
#else
/*
MinGW-w64 defines both __MINGW32__ and __MINGW64__, so
__MINGW32__ only will work for both mingw and mingw-w64.

This is very handy when writing portable software, because mingw-w64
automatically gets all the __MINGW32__ stuff which is often also good
for mingw-w64. You only need __MINGW64__ when the code needs to be
different for 64 bit compared to 32 bit.
*/
#ifdef __MINGW32__
    __mingw_printf("%lld\n", res);
#else
    printf("%lld\n", res);
#endif
#endif
	return 0;
}
