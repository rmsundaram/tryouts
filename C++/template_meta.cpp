#include <iostream>

using std::cout;
using std::endl;

template <int X, int Y>
struct XpowY
{
	static const int value = X * XpowY<X, Y - 1>::value;
};

template <int X>
struct XpowY <X, 1>
{
	static const int value = X;
};

int main()
{
	cout << endl << "5 ^ 7 = " << XpowY<5, 7>::value << endl;
	cout << endl << "7 raised 5 = " << XpowY<7, 5>::value << endl;

	return 0;
}
