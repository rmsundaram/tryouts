#include <vector>
#include <algorithm>
#include <iostream>
#include <string_view>

// [1]: https://stackoverflow.com/a/41155595/183120
// [2]: https://stackoverflow.com/a/22631234/183120

void print_ivec(const std::vector<int>& v, std::string_view prefix = "") {
  if (!prefix.empty())
    std::cout << prefix;

  for (auto i : v)
    std::cout << i << '\t';
  std::cout << '\n';
}

int main() {
  std::vector<int> v{0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
  print_ivec(v, "Original: ");

  // Elements following the removed elements overwrite the removed ones; after
  // remove(), the last n elements are garbage and can be `erase`d; see [1].
  // NOTE: if for every element removal, the elements following it are reseated,
  // this becomes a O(N²) operation; this doesn’t happen. The standard
  // guarantees linear complexity; see [2].
  auto it = std::remove_if(std::begin(v),
                           std::end(v),
                           [](int x) {
                             return ((x >= 4) && (x <= 6));
                           });
  print_ivec(v, "Removed: ");

  // no std::erase in algorithms
  v.erase(it, v.end());
  print_ivec(v, "Erased: ");
}
