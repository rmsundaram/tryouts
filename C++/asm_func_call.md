# Function call in Assembly

The reason stack is faster is that the allocation happens at the beginning.  Reclamation of memory is just a pointer increment operation.
Remember stack grows down NOT up.  `EBP` will be `0` for `main`, letting the debugger know that stack walking can stop there \[[1](http://stackoverflow.com/a/8362530)\].

## Caller

* `EBP` pointing to something, say 1016
* `ESP` pointing to top of stack, say 1012; perhaps the only local variable is at 1012
* Before calling a function, push all the parameters from right to left on to the stack; say 1008 = `ESP`
* Return address would be in `EIP`; `push EIP`
* `ESP` now pointing to return address in the stack space
* Jump to callee (`CALL`)
* *[cdecl]* clean-up parameters by adding to `ESP` to move it up

## Callee

* Push `EBP`, since we're going to use it, we want to preserve the current value; now `ESP` is pointing to `EBP`'s value on stack
* Copy `ESP` to `EBP` i.e. the bottom of the stack for this frame. `EBP` is now pointing to the address where old `EBP` value lives; this forms a linked list of stack frames forming the call stack \[[2](http://stackoverflow.com/a/579311)\]\[[3](https://blogs.msdn.microsoft.com/oldnewthing/20040116-00/?p=41023/)\].  If the optimizer does frame pointer omission (FPO), this might not happen \[[4](https://blogs.msdn.microsoft.com/larryosterman/2007/03/12/fpo/)\].
* Now `ESP` is free to move down for the allocation of local variable; `EBP + x` gives parameters, `EBP − x` gives local variables
* When returning, the function places the return value in `EAX` (or `R0` for a floating point result)
* Pop back all local variables leaving `ESP` = `EBP` or simply do `ESP` = `EBP`, both point to the old `EBP` value on stack
* Restore `EBP`'s value: pop top to `EBP`; `EBP` now has old value
* `ESP` is now pointing to return address
* *[stdcall]* clean-up parameters by adding to `ESP` to move it up
* IP will be assigned the value at `ESP`
* return back to caller (`RET`)

## Prologue / ENTER

    push EBP
    mov  EBP, ESP

## Epilogue / LEAVE

    mov ESP, EBP
    pop EBP
    
See [Calling Conventions](http://stackoverflow.com/documentation/x86/3261/calling-conventions) for detailed information on different x86 calling conventions with examples.

For x86-64 prologue and epilogue explanation [see here](https://github.com/legends2k/spirit-of-cpp/blob/master/code/basic.cpp); it follows System V AMD64 ABI calling convention, the default for all Unix-like OSs.

See Also [Call-de-stack](http://www.shakthimaan.com/downloads/glv/presentations/call-de-stack.pdf) for disassembly examples on function calls with arguments.
