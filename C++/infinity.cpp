#include <iostream>
#include <limits>

using namespace std;

int main()
{
	if (numeric_limits<float>::max() < numeric_limits<float>::infinity())
		cout << "Float works!" << endl;
	if (numeric_limits<int>::max() > numeric_limits<int>::infinity())
		cout << "Yikes, infinity is less than max!!" << endl;
	// correct way - one should always check for the type's infinity availability
	if (!numeric_limits<int>::has_infinity)
		cout << "Integral data types don't've infinity!" << endl;
    // http://stackoverflow.com/q/13861629
    // http://stackoverflow.com/q/7973737
}
