#include <type_traits>
#include <functional>

#include <iostream>

class A
{
};

class B : public A
{
};

class C
{
};

template <class T, class = void>
struct X;

// if T is not derived from A (say C) then this will fail to match
// and match the above declaration. Then compilation will fail since there's no
// implementation for X<C>
template <class T>
struct X<T, typename std::enable_if<std::is_base_of<A, T>::value>::type> : public T
{
    void yippe()
    {
        std::cout << "Yippe!" << std::endl;
    }
};

void simp(int a)
{
    std::cout << a << std::endl;
}

int main()
{
    X<B> b;
    std::function<void (void)> func = std::bind(&X<B>::yippe, std::ref(b));
    func();

    void (X<B>::*fp)(void) = &X<B>::yippe;
    (b.*fp)();

    std::function<void (void)> func1 = std::bind(&simp, 100);
    func1();

    // try inside catch
    try
    {
        std::cout << "trying..." << std::endl;
        throw std::bad_alloc();
    }
    catch(...)
    {
        try
        {
            std::cout << "trying again!!" << std::endl;
            throw std::bad_alloc();
        }
        catch(...)
        {
            std::cout << "finally caught it!!" << std::endl;
        }
    }
}
