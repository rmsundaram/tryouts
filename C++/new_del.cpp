// g++ -Wall -std=c++17 -pedantic{,-errors} new_del.cpp
// cl /EHsc /std:c++17 new_del.cpp

// Tested on GCC 13.2, Clang 17.0 on ArchLinux, MSVC 19.39 (VS 2022) on Win 10;
// they all respect the documentation’s admonition of overriding just the
// single-pointer varaint sufficing to handle all cases [1] (see multiple
// comments by self); also see [2]; just replacing 2 functions until C++17 or 4
// since should do.  MinGW 13.2 (MSYS2 Win 10) is a notable deviant; it expects
// different user-defined variants to cover all cases.  [3] talks about
// C++17-introduced align_val_t variant of new getting called when T requires
// alignment stricter/wider than __STDCPP_DEFAULT_NEW_ALIGNMENT__.
//
// See also [4] for a header with macros to print leak log with this technique.
//
// [1]: https://stackoverflow.com/q/8186018/#comment138163974_8187510
// [2]: https://www.cppstories.com/2019/08/newnew-align/
// [3]: https://stackoverflow.com/q/53145018/183120
// [4]: https://github.com/legends2k/heapstat

#include <cstdlib>  // for std::{malloc,free,size_t}
#include <cstdio>   // for printf
#include <cinttypes>// for PRI printf format specifiers
#include <new>      // for std::bad_alloc

void TraceAlloc(void* ptr, std::size_t count) {
  printf("Allocated %" PRIuMAX " bytes: %p\n", count, ptr);
}

void TraceFree(void* ptr) {
  printf("Freed %p\n", ptr);
}

void* operator new(std::size_t count) {
  printf("operator new(size_t)\n");
  // std::malloc may return nullptr if count is 0
  count = (count != 0) ? count : 1;
  if (void* ptr = std::malloc(count))
  {
    TraceAlloc(ptr, count);
    return ptr;
  }
  throw std::bad_alloc{};
}

void operator delete(void* ptr) noexcept {
  printf("operator delete(void*)\n");
  TraceFree(ptr);
  std::free(ptr);
}

// C++17-introduced aligned variants of new and delete
// MSVC doesn’t set __cplusplus properly with just /std flag; refer
// https://stackoverflow.com/a/58316194/183120
#if (__cplusplus >= 201703L || (defined(_MSVC_LANG) && (_MSVC_LANG >= 201703L)))

#define CPP_17_PLUS

// MSVC’s cstdlib doesn’t support C++17 std::aligned_alloc; use alternative.
// https://learn.microsoft.com/en-us/cpp/standard-library/cstdlib?view=msvc-170

// for _mm_{malloc,free}
#if defined(_MSC_VER) || defined(__MINGW32__)
#  include <malloc.h>
#else  // GCC, Clang
#  include <mm_malloc.h>
#endif

void* operator new(std::size_t count, std::align_val_t a) {
  printf("operator new(size_t, align_val_t)\n");
  // alloc functions may return nullptr if count is 0
  count = (count != 0) ? count : 1;
  if (void* ptr = _mm_malloc(count, static_cast<size_t>(a)))
  {
    TraceAlloc(ptr, count);
    return ptr;
  }
  throw std::bad_alloc{};
}

void operator delete(void* ptr, std::align_val_t a) noexcept {
  printf("operator delete(void*, align_val_t)\n");
  TraceFree(ptr);
  _mm_free(ptr);
}

#endif // __cplusplus >= 201703L

// Handle MinGW stdlib quirks.
#if __MINGW32__ || __MINGW64__

// NOTE: Only defined variants invoked by the driver code in main(), many are
// left undefined; test on your compiler + stdlib combo and define as needed.

// Array variants
void* operator new[](std::size_t count) {
  printf("Redirect from `operator new[](std::size_t count)`\n");
  return operator new(count);
}

void operator delete[](void* ptr) noexcept {
  printf("Redirect from `operator delete[](void* ptr)`\n");
  operator delete(ptr);
}

// No-throw, array new
void* operator new[](std::size_t count, const std::nothrow_t&) noexcept {
  printf("Redirect from "
         "`operator new[](std::size_t count, const std::nothrow_t&)`\n");
  try {
    auto p = operator new(count);
    return p;
  }
  catch(const std::bad_alloc&) {
    return nullptr;
  }
}

// Sized delete
void operator delete(void* ptr, std::size_t count) {
  printf("Redirect from `operator delete(void* ptr, std::size_t count)`\n");
  operator delete(ptr);
}

#ifdef CPP_17_PLUS
void operator delete(void* ptr, std::size_t s, std::align_val_t a) noexcept {
  printf("Redirect from "
         "`operator delete(void* ptr, std::size_t s, std::align_val_t a)`\n");
  operator delete(ptr, a);
}
#endif // CPP_17_PLUS

#endif // __MINGW32__ || __MINGW64__

// Include TU in executable/library project to replace new and delete globally.
// ------------------------- End of new_del.hpp -------------------------------


#ifdef CPP_17_PLUS
// Type to demonstrate align_val_t variant of new introduced by C++17 along
// with __STDCPP_DEFAULT_NEW_ALIGNMENT__.  If a type’s alignment requirement
// exceeds this then the align_val_t variant is called.  alignas itself was
// introduced in C++11.
struct alignas(__STDCPP_DEFAULT_NEW_ALIGNMENT__ * 2) double4 {
  double d[4];
};
#endif // CPP_17_PLUS

int main() {
  printf("# Single pointer\n");
  int* p = new int(0);
  delete p;

  printf("\n# Array pointer\n");
  int* pa = new int[10]{};
  delete [] pa;

  printf("\n# no-throw\n");
  int* pp = new(std::nothrow) int[10]{};
  delete [] pp;

#ifdef CPP_17_PLUS
  printf("\n# Over-aligned, single-pointer\n");

  // __STDCPP_DEFAULT_NEW_ALIGNMENT__ is defined UL by stdlibs of MSVC, Clang
  // but as int by GCC’s libstdc++.
  printf("__STDCPP_DEFAULT_NEW_ALIGNMENT__ is %" PRIuMAX "\n",
         __STDCPP_DEFAULT_NEW_ALIGNMENT__);
  double4* f = new double4{};
  delete f;
#endif // __cplusplus >= 201703L

}
