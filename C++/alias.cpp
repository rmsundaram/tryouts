// Type alias and alias template
// http://en.cppreference.com/w/cpp/language/type_alias

#include <vector>
#include <string>
#include <iostream>

// http://stackoverflow.com/questions/3656726/array-and-rvalue
// you need this or below as char[]{1, 2, 3, 4, 5} and char[5]{} are invalid
template<typename T>
using alias = T;

// another way to create a temporary array
using intArray = int[];

template<typename T>
using Vector = std::vector<T>;

std::string f() {
  return std::string("hello");
}

int main()
{
    Vector<float> temps{0.4f, 1.2f};
    std::cout << temps[0] << std::endl;

    // http://stackoverflow.com/a/2044918/183120
    // http://stackoverflow.com/a/2036125/183120
    // http://stackoverflow.com/a/12542503/183120
    // http://stackoverflow.com/questions/10004511/why-are-string-literals-l-value-while-all-other-literals-are-r-value
    // normally an array to array copy cannot be done this way; need to do it element by element manually, except here
    // a string literal is an lvalue of type const char[]; its contents are copied to str1, another char array
    char str1[] = "this is a char array";
    // since string literals are const char arrays, it's perfectly fine for them to be the operand of sizeof
    std::cout << sizeof "another char array" << '\t' << sizeof str1 << std::endl;
    // here const char[] decays into const char* and the pointer is copied
    const char *str2 = "char array";

    // constructing a temporary rvalue array
    int const (&temp_arr1)[5] = intArray{1, 2, 3, 4, 5};    // removing const would give an error
    int const (&temp_arr2s)[5] = alias<int[]>{1, 2, 3, 4, 5};

	// http://stackoverflow.com/questions/3656726/array-and-rvalue
	int x = alias<int[]>{0, 1, 2}[0];

    // http://stackoverflow.com/questions/24800112/arent-elements-of-a-temporary-array-rvalues-themselves
    // temporary's element not a temporary (rvalue) but lvalue, §5.2.1 Subscripting, N3337 draft
    int &e = alias<int[]>{0, 1, 2}[0];  // yikes!! e is now a dangling reference, since the temporary is gone

    // “Temporaries are destroyed as the last step in evaluating the full-expression that (lexically) contains the point
    // where they were created.”  Hence below statement is OK; storing c_str’s return value for later use isn’t.
    // https://stackoverflow.com/q/584824/183120
    // https://en.cppreference.com/w/cpp/language/lifetime
    const std::string s(f().c_str());
}
