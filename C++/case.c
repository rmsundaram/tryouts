#include <stdio.h>

int main(int argc, const char *argv[])
{
    const int FOO = 1;
    const int BAR = 2;

    int n = FOO;

    // C and C++ gotcha
	// this prints "None of the above" since FOO and BAR are mere labels.
	// when you add case in front
	//     C doesn't allow const variables; it expects integer constant (literals)
	//     C++ would allow const variables as switch-case labels.
    switch (n)
    {
        FOO:
            printf("FOO");
            break;

        BAR:
            printf("BAR");
            break;

        default:
            printf("None of the above");
            break;
    }

    return 0;
}
