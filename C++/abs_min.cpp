// Catching Integer Overflows in C
// http://www.fefe.de/intof.html
#include <iostream>
#include <limits>

int main()
{
    int i = std::numeric_limits<decltype(i)>::min();
    i = std::abs(i);    // undefined behaviour
    /*
     * Simple example would be to imagine a nibble (quartet) where the
     * representable range would be [-8, 7]
     * -1 = 0b1111 = 0xf, -8 = 0b1000 = 0x8
     * although, mathematically |-8| = 8, this isn't representable in a nibble.
     * This is undefined behaviour; most compilers assign the result
     * 8 = 0b1000 = 0x8 to the variable, which wraps around to become −8 again
     * on a 2's complement implementation. See also bit_wizardry.md.
     */
    std::cout << i << '\n';
}
