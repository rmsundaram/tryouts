// g++ -U__STRICT_ANSI__ epsilon.cpp
// undefine __STRICT_ANSI__ in GCC for M_PI, M_E, etc. to be defined
#include <iostream>
#include <limits>
#include <iomanip>
#include <cmath>
#include <cfloat>

int main()
{
    // floating point and integral type ranges
    std::cout << sizeof(float) << '\t' << sizeof(double) << '\t' << sizeof(long double) << '\n';
    std::cout << sizeof(char) << '\t' << sizeof(short) << '\t' << sizeof(int) << '\t'
              << sizeof(long) << '\t' << sizeof(long long) << '\n';
    // for integral types the standard doesn't guarantee anything other than
    // char = one byte, short alteast 2 bytes, int atleast 2 bytes, long atleast 4 bytes, long long alteast 8 bytes
    // as for floating point types C++ standard doesn't even mention about IEEE 754
    // See http://isocpp.org/wiki/faq/newbie#choosing-int-size

    std::cout << "numeric_limits<float>::digits: " << std::numeric_limits<float>::digits << std::endl;
	std::cout << "numeric_limits<double>::digits: " << DBL_MANT_DIG << std::endl;
	std::cout << "numeric_limits<uint8_t>::digits: " << std::numeric_limits<unsigned char>::digits << std::endl;
	std::cout << "numeric_limits<uint8_t>::digits10: " << std::numeric_limits<unsigned char>::digits10 << std::endl;
    /*
     numeric_limits<T>::digits is the number of digits in base-r that can be represented by the type T without change.
     For integer types, this is the number of bits not counting the sign bit (so signed values have one less). For
     floating point types, this is the number of digits in the mantissa[1]. The value of r is defined in
     numeric_limits<T>::radix, which is 2 for integral and binary float types, but may be 10 for decimal floats[2]. In
     other words std::digits is the bit width of T.

     numeric_limits<float>::digits returns 24 = 23 + 1; 23 bits of mantissa + 1 for the hidden bit. When working in
     binary, the mantissa/significand is characterized by its width in binary digits (bits), because the most
     significant bit is always 1 for a normalized number, this bit is not typically stored and is called the "hidden
     bit". Depending on the context, the hidden bit may or may not be counted towards the width of the significand[3].

     numeric_limits<T>::digits10 = numeric_limits<T>::digits * log₁₀(numeric_limits<T>::radix)
     This is the number of base-10 digits that can be represented by the type T without change, that is, any number with
     this many decimal digits can be converted to a value of type T and back to decimal form, without change due to
     rounding or overflow. E.g.: An 8-bit binary type can represent any 2-digit decimal number exactly, but 3-digit
     decimal numbers [256, 999] cannot be represented[4]. Thus digits10 for an 8-bit type is 2 = floor(log₁₀(2⁸) = 2.41).
     Here “without change” means the original value should be obtainable from the type's value without change. Say we've
     the decimal number 120 as a string, converting to a value of type unsigned char and that converted back again to
     string should give 120 _without change_. Had it been 1000, with an 8-bit unsigned char, this value would've gotten
     wrapped by modulo 2⁸ i.e. uint8_t x = 1000; std::cout << +x; should print 232 which is 1000 % 256. Thus 1000 was
     not represented without change, while any integer ∈ [0, 99] would be represented without change.

     When 24 * log10(2) = 7.22, why does numeric_limits<float>::digits10 say 6 for binary32?[5] It should be noted
     digits10 and precision are not the same things[6].

     [1]: http://en.cppreference.com/w/cpp/types/numeric_limits/digits
     [2]: http://stackoverflow.com/q/29174165
     [3]: http://en.wikipedia.org/wiki/Significand#Significands_and_the_hidden_bit
     [4]: http://en.cppreference.com/w/cpp/types/numeric_limits/digits10
     [5]: http://stackoverflow.com/q/30688422
     [6]: http://stackoverflow.com/questions/10484332/
     how-to-calculate-decimal-digits-of-precision-based-on-the-number-of-bits?lq=1#comment52277889_10484553
    */

	// to print a float such that converting the printed string back into a float will yeild back the same float, use
	// max_digits10[2][3]
	// [1]: http://stackoverflow.com/a/22458961
	// [2]: http://stackoverflow.com/a/554134
    std::cout << std::setprecision(std::numeric_limits<float>::max_digits10)
              << "pi = " << std::scientific << M_PI
              << ", e = " << M_E << std::endl;

    // Summary
    //   • max_digits10 is the number of decimal digits needed to guarantee correct float → text → float round-trip
    //   • digits10 is the number of decimal digits guaranteed to survive text → float → text round-trip
    //   • digits is the bit width of T. For intgerals, it’s the number of bits not counting the sign bit and the
    //       padding bits (if any). For floating-point types, this is the number of digits in the mantissa.
    //
    // C++17 provides <charconv> for local-independant, non-allocating, non-throwing functions to allow fastest possible
    // conversion implementations of integral and floating-point values to and from char; this would be useful in common
    // high-throughput contexts such as text-based interchange (JSON, XML, …).  Due to the above guarentees it’s around
    // 10x faster than many implementations!
    // CppCon 2019: Stephan T. Lavavej - Floating-Point <charconv>: Making Your Code 10x Faster With C++17's Final Boss
    // https://www.reddit.com/r/cpp/comments/dgj89g/cppcon_2019_stephan_t_lavavej_floatingpoint/
}
