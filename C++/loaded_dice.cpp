// For a given count of items, *randomly* generate one of the four possible
// outcomes based on the probability of each outcome given by the user. For
// e.g. if 5, 10, 5, 1 were passed, _approximately_ there should be 5 reds for
// every 10 greens, 5 blues and an orange.

#include <random>
#include <iostream>
#include <array>
#include <cstdint>
#include <algorithm>

enum class Marble : uint8_t
{
    Red = 0,
    Green,
    Blue,
    Orange,
};

std::array<double, 4> calc_ratios(int reds, int greens, int blues, int oranges)
{
    double const total = reds + greens + blues + oranges;
    double const rRatio = reds / total;
    double const gRatio = greens / total;
    double const bRatio = blues / total;
    return {rRatio, gRatio, bRatio, 1.0 - (rRatio + gRatio + bRatio)};
}

// larger the count of runs, the more well-pronounced the uniformity of
// uniform_real_distribution gets: https://stackoverflow.com/a/10178888
constexpr auto COUNT = 10000;
std::array<Marble, COUNT> generate(std::array<double, 4> ratios)
{
    double const rCumilativeRatio = ratios[0];
    double const gCumilativeRatio = ratios[0] + ratios[1];
    double const bCumilativeRatio = ratios[0] + ratios[1] + ratios[2];
    //double const oCumilativeRatio = 1.0;

    std::array<Marble, COUNT> items = { };
    // obtain to get seed to feed to the random number engine
    std::random_device rd;
    // standard mersenne_twister_engine seeded with rd()
    std::mt19937 gen(rd());
    // we want the generated samples to be uniformly distributed over [0, 1)
    std::uniform_real_distribution<> dis(0, 1);
    // use dis to transform the generated random unsigned int into a double

    for (auto i = 0; i < COUNT; ++i)
    {
        double const sample = dis(gen);
        Marble m = Marble::Orange;
        // map uniform to biased probability distribution as given by the user
        // https://stackoverflow.com/a/9956511
        // See also: alias method which also seems to be related to this
        // https://stackoverflow.com/a/5033445
        if ((sample >= 0) && (sample <= rCumilativeRatio))
            m = Marble::Red;
        else if ((sample > rCumilativeRatio) && (sample <= gCumilativeRatio))
            m = Marble::Green;
        else if ((sample > gCumilativeRatio) && (sample <= bCumilativeRatio))
            m = Marble::Blue;
        items[i] = m;
    }
    return items;
}

int main()
{
    int reds, greens, blues, oranges;
    std::cout << "Red ratio: ";
    std::cin >> reds;
    std::cout << "Green ratio: ";
    std::cin >> greens;
    std::cout << "Blue ratio: ";
    std::cin >> blues;
    std::cout << "Orange ratio: ";
    std::cin >> oranges;

    auto const ratios = calc_ratios(reds, greens, blues, oranges);
    auto items1 = generate(ratios);
    std::array<size_t, 4> n1{ };
    for (Marble m: items1)
        ++n1[static_cast<size_t>(m)];

    // https://stackoverflow.com/q/15509270#comment139441459_15509942
    std::random_device r;
    std::seed_seq seeds{r(), r(), r(), r(), r(), r(), r(), r()};
    std::mt19937 eng(seeds);
    std::discrete_distribution<uint8_t> dist{ratios.cbegin(), ratios.cend()};
    std::array<uint8_t, COUNT> items2{};
    std::generate_n(items2.begin(), COUNT, [&](){return dist(eng);});
    std::array<size_t, 4> n2{};
    for (auto const m : items2)
      ++n2[m];

    std::cout << "\n# Method 1: Custom\n"
              << "Reds    = " << n1[0] << '\n'
              << "Greens  = " << n1[1] << '\n'
              << "Blues   = " << n1[2] << '\n'
              << "Oranges = " << n1[3] << '\n';

    std::cout << "\n# Method 2: std::discrete_distribution\n"
              << "Reds    = " << n2[0] << '\n'
              << "Greens  = " << n2[1] << '\n'
              << "Blues   = " << n2[2] << '\n'
              << "Oranges = " << n2[3] << '\n';
}

/*
 * RANDOM NUMBER GENERATION
 *
 * 1. Most random number generators are pseudo-random; not truly random. Also
 *    notice that randomness isn’t the characteristic of a number but of the
 *    sequence and its generator’s.
 * 2. Truly random sequence may be possible with additional hardware [2].
 *      - e.g. `std::random_device("/dev/urandom")` may be hardware implemented
 * 3. Most PRNGs work atop the previous value to generate a new one.  Previous
 *    of first is called “seed”, often provided by the programmer e.g. srand(C),
 *    math.randomseed(Lua), random.seed(Python) [4].
 * 4. Reproducibility: using the same seed with the same RNG should (re)produce
 *    the same sequence of numbers; useful for debugging, serialization, etc.
 * 5. Check [5], [6] for reasons to avoid C’s rand.
 *      - Global mutable state isn’t multithreading-friendly
 *      - Implementation-specified; hampers reproducibility even with same seed
 *      - rand is commonly implemented with 32-bit LCG; not great statistically
 *        as the better LCGs require wider seeds while srand takes unsigned :(
 *      - Lacks a distribution engine; plagued by non-uniform distributions due
 *        to the Pegionhole Principle.
 *      - Avoid using `time(nullptr)` or modulo; severly disturbs distribution
 *      - [11] lists places to look for good seeds (entropy) e.g. /dev/urandom
 * 6. An engine is the source of a stream of random bits.  A distribution
 *    consumes these bits and turns them into numbers. A distribution may
 *    consume more or fewer bits for each number generated (e.g. if it uses
 *    “rejection sampling”) [14].
 * 7. [7] shows “default” seeding to generate non-cryptographically-secure
 *    sequences with the Mersenne Twister engine (std::mt19937) with a
 *    std::seed_seq; sequence of values as seed instead of just one.
 *      - A u32 seed isn’t too bad; mt19937::mt19937 does complex init
 *      - “Warmup” is needed to randomize the entire state i.e. a std::seed_seq
 *        of mt19937::state_size (624) elements are to be generated;
 *      - [8] shows std::mt19937 warmup both using {std,boost}::random_device
 * 8. MT has very large state and is easy to predict.  Better alternatives
 *    exist [9]: Xoshiro256** [10] and PCG [3] (both seem to be from rivals)
 *      - Use pcg64 for 32-bit, xoshiro256** for 64-bit sequences [12]
 *      - A dated comparison of alternatives [16]
 * 9. Use one engine + mutiple distributions if correlation b/w sequences are
 *    acceptable [13] [14]; use n engines if you want them uncorrelated and/or
 *    for multi-threading [15].
 *
 * REFERENCES
 *
 * 1. https://en.wikipedia.org/wiki/Pseudorandom_number_generator
 * 2. https://math.stackexchange.com/q/2056780/51968
 * 3. http://www.pcg-random.org/
 * 4. https://stackoverflow.com/a/22639752/183120
 * 5. https://stackoverflow.com/q/52869166/183120
 * 6. [Stephan T. Lavavej’s intro to <random>](https://youtu.be/LDPMpc-ENqY)
 * 7. https://stackoverflow.com/q/15509270#comment139441459_15509942
 * 8. https://codereview.stackexchange.com/q/109260#comment586186_109260
 * 9. https://rust-random.github.io/book/guide-rngs.html
 * 10. https://prng.di.unimi.it/
 * 11. https://nullprogram.com/blog/2019/04/30/
 * 12. https://nullprogram.com/blog/2017/09/21/
 * 13. https://stackoverflow.com/q/48093621/183120
 * 14. https://stackoverflow.com/a/73194479/183120
 * 15. https://stackoverflow.com/q/14923902/183120
 * 16. https://arvid.io/2018/06/30/on-cxx-random-number-generator-quality/
 */
