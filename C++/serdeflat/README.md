See [1][] for comparison against protobuf, [capnproto][] and SBE.

[1]: https://capnproto.org/news/2014-06-17-capnproto-flatbuffers-sbe.html

# See Also

* [Be aware that serialization is a big subject - preshing.com][serial-big]

# Appendix A

When a custom file format needs to be deserialized/read, following libs let’s
you define the structure in an interface and emits source (multi-language) to
parse and load a `struct`:

1. [Kaitai Struct][]
2. [Wuffs][]

[kaitai struct]: https://doc.kaitai.io/user_guide.html

[wuffs]: https://github.com/google/wuffs-mirror-release-c

When reading very large files sequentially use `posix_fadvise` / `madvice` if
on a POSIX-ly OS; on Windows use `FILE_FLAG_SEQUENTIAL_SCAN`.  It makes a
significant different.  Memory mapped files are another option: useful when
reading a specific portion of a large file.  Refer
https://stackoverflow.com/a/17925143/183120.

[capnproto]: https://capnproto.org/

[serial-big]: https://preshing.com/20171218/how-to-write-your-own-cpp-game-engine/#be-aware-that-serialization-is-a-big-subject
