#include "savedata_generated.h"
#include <iostream>
#include <fstream>
#include <memory>

// Persist to file
bool save(const uint8_t* buffer, size_t buffer_size, const char* file_name) {
  std::ofstream level1_save(file_name, std::ios::binary);
  if (!level1_save.is_open())
    return false;
  level1_save.write(reinterpret_cast<const char*>(buffer), buffer_size);
  return true;
}

// Load from file
std::tuple<std::unique_ptr<uint8_t[]>, const Level*>
load(const char* file_name) {
  if (std::ifstream level1_load(file_name, std::ios::binary | std::ios::ate);
      level1_load.is_open()) {
    // Get file size and seek back to beginning
    const size_t buffer_size = level1_load.tellg();
    level1_load.seekg(0);
    auto buffer = std::make_unique<uint8_t[]>(buffer_size);
    level1_load.read(reinterpret_cast<char*>(buffer.get()), buffer_size);
    std::cout << "level_1.lvl of size "
              << buffer_size
              << " bytes loaded.\n";
    const Level* level = GetLevel(buffer.get());
    return std::make_tuple(std::move(buffer), level);
  }
  return std::make_tuple(nullptr, nullptr);
}

void save_driver() {
  // Build level buffer
  flatbuffers::FlatBufferBuilder builder(1024); // buffer size bytes; can grow
  // Except structs, nested creation of strings/vectors/tables while creating
  // the containing table (b/w level_build and .Finish()) is prohibited.
  // Create inner objects first, otherwise a nested assert fires.
  // https://google.github.io/flatbuffers/flatbuffers_guide_tutorial.html
  auto name = builder.CreateString("Wild Woods");
  auto hero_position = vec3(1.0456f, 2.72f, -20.23f);
  LevelBuilder level_builder(builder);
  level_builder.add_layers(5);
  level_builder.add_terrain_width(1024);
  level_builder.add_terrain_height(2048);
  level_builder.add_hero_pos(&hero_position);
  level_builder.add_name(name);
  auto level_1 = level_builder.Finish();
  builder.Finish(level_1);

  // Save to file
  if (save(builder.GetBufferPointer(), builder.GetSize(), "level_1.lvl"))
    std::cout << "level_1.lvl of size "
              << builder.GetSize()
              << " bytes saved.\n";
  else
    std::cerr << "Failed to save level_1.lvl!\n";
}

void load_driver() {
  // Load from file.  Keep buffer (level_1’s backing store) alive.
  // All level_1 members are pointers into buffer.
  if (auto [buffer, level_1] = load("level_1.lvl"); level_1) {
    const vec3* pos = level_1->hero_pos();
    const flatbuffers::String* level_name = level_1->name();
    std::cout << "Level: " << level_name->c_str() << '\n'
              << "Terrain Dimension: " << level_1->terrain_width()
              << " × " << level_1->terrain_height() << '\n'
              << "Layers: " << +level_1->layers() << '\n'
              << "Hero Start Pos: ("
              << pos->x() << ", " << pos->y() << ", " << pos->z()
              << ")\n";
  } else
    std::cerr << "Failed to load level_1.lvl!\n";
}

int main(int argc, char** argv) {
  if (argc >= 2)
    save_driver();
  else
    load_driver();
}
