// Lisp power: We want to write a function that generates accumulators -- a
// function that takes a number n, and returns a function that takes another
// number i and returns n incremented by i.
// http://www.paulgraham.com/icad.html

#include <iostream>
#include <functional>

using namespace std;

template <typename T>
function<T(T)> foo(T& n)
{
    // mutable is to allow editing of n which is captured by value (read-only)
    // adding the mutable makes a copy in the lambda's anonymous struct editable
    // http://stackoverflow.com/q/16944894
    function<T(T)> bar = [n](T i) mutable
    {
        return n += i;
    };
    return bar;
}

int main()
{
	auto n = new int{5};
	cout << *n << endl;
	auto acc = foo(*n);	// stores a copy of the captured *n
	delete n;
	cout << acc(10) << endl;	// updates the stored integer
	cout << acc(5) << endl;
}
