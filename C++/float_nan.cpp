#include <iostream>
#include <algorithm>
#include <bitset>
#include <type_traits>
#include <cstdlib>

union float_char
{
    float f;
    char ch[4];

    float_char() : f{0.0f}
    {
    }
};

int main()
{
    unsigned int i{0xffffffff};
    float f;
    // this avoid the undefined behaviour that reinterpret_cast<float*> would invoke
    memcpy(&f, &i, sizeof(f));
    std::cout << std::showbase << std::hex << i << std::dec << '\n' << f << '\n';
    std::cout << std::bitset<32>{i} << '\n';   // will print the value representation of i in binary
    // while having an array of 8-bit bitsets would give the memory representation, endianess will show its rear end

    float_char a;
    // using union for type-punning is UB in C++, allowed in C11 and upwards
    std::fill_n(a.ch, std::extent<decltype(a.ch)>(), 0xff);
    std::cout << a.f << '\n';

    // https://en.cppreference.com/w/cpp/language/classes#POD_class
    // C++20 deprecates std::is_pod; defines it as (std::is_trivial && std::is_standard_layout)
    std::cout << std::is_trivial<float_char>::value << std::is_standard_layout<float_char>::value << '\n';
}
