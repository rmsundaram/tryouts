// Order of trying various C++ casts: static_cast, const_cast, dynamic_cast,
// reinterpret_cast and C-style cast
// https://stackoverflow.com/q/332030/183120
//
// Reinterpret bits of an object of type T, as if it were an object of type
// U with reinterpret_cast. int(12) -> float(12.0) requires some conversion
// to be done by the CPU (static_cast does this); while reinterpreting
// int(12)’s bits as float will give a completely different value that has
// no mathematical correlation between them; see [0].
//
// Use static_cast to convert to and from void* to T*, use reinterpret_cast
// if it’s U* instead of void*.  reinterpret_cast only guarantees getting
// original value back when doing: T* -> U* -> T*.  Value inbeween (U*) is
// unspecified.  While all stages are specified when T* -> void* -> T* with
// static_cast.
//
// See [1] for differences, [2] for reinterpret_cast’s valid use case.
//
// [0]: https://stackoverflow.com/a/43273907/183120
// [1]: https://stackoverflow.com/a/573345
// [2]: https://stackoverflow.com/a/573574/183120
//
// See Also: More Effective C++, Item 24: Understand the costs of virtual
// functions, multiple inheritance, virtual base classes, and RTTI

// Cross Cast a.k.a Side Cast: converting from Base1 to Base2
// https://stackoverflow.com/a/7789468/183120
// https://stackoverflow.com/q/35959921/183120
// https://en.cppreference.com/w/cpp/language/dynamic_cast
// https://codeburst.io/understanding-c-casts-ef1f36e54240

#include <iostream>

struct B1 {
  virtual ~B1() = default;
  int x;
};

struct B2 {
  virtual ~B2() = default;
  int y;
};

struct C : B1, B2 {
  int z;
};

int main() {
  C c;
  B1 *p1 = &c;
  B2 *p2 = static_cast<C*>(p1);
  // The same can be achieved by dynamic_cast with a runtime penalty if the
  // classes are polymorphic (has virtual methods). Strictly speaking using
  // dynamic_cast and directly converting from p1 to p2 is true cross cast,
  // while with static_cast it’s just a downcast and (implicit) upcast.
  if (dynamic_cast<B2*>(p1))
    std::cout << "Success!\n";
}
