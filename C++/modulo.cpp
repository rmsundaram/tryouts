// g++ -Wall -std=c++20 -pedantic -pedantic-errors modulo.cpp
/*

Example 1:

−6 mod 18, two variants
   ______
18 ) −6 ( 0
      0
   ------
     -6
   ------

   ______
18 ) −6 (−1
     18
   ------
     12
   ------

One can be obtained from the other.  When having negative, adding divisor to it
will give its positive counterpart; when having positive, subtracting divisor
will give its negative counterpart.  12 − 18 = −6 and −6 + 18 = 12.  When
visualizing modulo operator with a clock, rotating the hand clockwise would give
the positive result while rotating it counter-clockwise will give the negative
result.  Notice that the quotient of the two variants differ by 1.

Example 2:

−7 mod 18

   ______
 6 ) −7 (−1
      6
   ------
     −1
   ------

   ______
 6 ) −7 (−2
     12
   ------
      5
   ------

Example 3:

29 mod 10

   ______
10 ) 29 ( 2
     20
   ------
      9
   ------

   ______
10 ) 29 ( 3
     30
   ------
     −1
   ------

Example 4:

−5 mod −4

   ______
 −4) −5 ( 1
      4
   ------
     −1
   ------

   ______
 −4) −5 ( 2
      8
   ------
      3
   ------

C99 mandated that the sign of the result would be that of the dividend, while
Knuth's method would be to choose the result having the same sign as the
divisor's; this convention is used in languages like Perl, Python, Lua,
etc.  Euclid's definition of modulo is that the result is always positive.
Refer the Modulo.jpg calculation for the math behind the methods.

Formally, both these variants are "congruent modulo n" E.g. 12:00 and 00:00 are
congruent modulo 12; similarly, 13:00 and 1:00 are congruent modulo 12. However,
12:00 and 1:00 aren't.  Modular arithmetic[1] gives us a simple formula to find
congruency between numbers over some modulus of congruence, n:

        a ≡ b (mod n) if (a − b) = kn

where all variables (a, b, n and k) are integers

PROPERTIES
1. Reflexivity: a ≡ a (mod n)
2. Symmetry: a ≡ b (mod n) if and only if b ≡ a (mod n)
3. Transitivity: If a ≡ b (mod n) and b ≡ c (mod n), then a ≡ c (mod n)

REFERENCES
[1]: https://en.wikipedia.org/wiki/Modular_arithmetic

SEE ALSO
1. stb_divide.h has three methods for quotient and remainder computation
   https://github.com/nothings/stb/blob/master/stb_divide.h
2. Division and Modulus for Computer Scientists - Daan Leijen
   https://www.microsoft.com/en-us/research/wp-content/uploads/2016/02/divmodnote-letter.pdf
*/

#include <iostream>
#include <type_traits>
#include <cmath>
#include <cstdlib>

// C++20 concept auto function parameters; abbreviated function template.

// sign(result) = sign(dividend)
constexpr
auto cmod(std::integral auto a, std::integral auto b) {
    return a % b;
}

// sign(result) = sign(divisor)
// Aside: this is a fast way to get an int’s positive modulo in C.
// https://stackoverflow.com/q/14997165/183120
// Refer //Misc/bit_wizardry.md.html for a faster version for POTs.
constexpr
auto kmod(std::integral auto a, std::integral auto b) {
    // If sign(a) = sign(b) then no fiddling is needed; if one is positive
    // and the other is negative, then result NOT obtained by C's % operator
    // is needed.  When a is positive and b is not, r − b is needed, while
    // if a is negative and b is not, r + b is needed.  However, it'd be
    // nice to avoid branches.

    return ((a % b) + b) % b;

    // swapping signs when sign(a) ≠ sign(b) may also work
}

// sign(result) = +
constexpr
auto emod(std::integral auto a, std::integral auto b) {
    // If the dividend is negative, the result will be negative,
    // adding the divisor will make it positive; however, if it is positive the
    // result will be positive but the addition will make it larger than the
    // divisor, when do mod b again to always get a positive result.
    return ((a % b) + std::abs(b)) % b;
}

int main(int argc, char **argv)
{
    if (argc >= 2) {
        long const a = std::strtol(argv[1], nullptr, 0);
        long const b = std::strtol(argv[2], nullptr, 0);
        std::cout << a << " % " << b << " = " << cmod(a, b) << " [C]\n";
        std::cout << a << " % " << b << " = " << kmod(a, b) << " [Knuth]\n";
        std::cout << a << " % " << b << " = " << emod(a, b) << " [Eculid]\n";
    }
    else
        std::cout << "Insufficient arguments; two integers expected.";
}
