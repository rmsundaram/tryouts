#include <iostream>
#include <utility>

std::ostream& tab(std::ostream& os)
{
	return os << '\t';
}

void out() {}

// recursive function using variadic templates and perfect forwarding
template<typename T, typename... Args>
void out(T&& value, Args&&... args)
{
    std::cout << std::forward<T>(value);
    out(std::forward<Args>(args)...);
}

int main()
{
	// cannot have endl as a template param since it's not a function but a template
	// http://stackoverflow.com/a/10016203
    out("12345", std::endl<char, std::char_traits<char>>);
	out("12345", "man", tab, '1');
}
