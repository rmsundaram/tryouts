// categorize a Poker hand
// See Also: https://stackoverflow.com/q/10363927

#include <iostream>
#include <cstdint>
#include <array>
#include <algorithm>

enum Category
{
    HighCard      = 0,
    Pair          = 1,
    TwoPair       = 2,
    ThreeOfAKind  = 4,
    FourOfAKind   = 8,
    FullHouse     = Pair | ThreeOfAKind,
    Straight      = 16,
    Flush         = 32,
    StraightFlush = Straight | Flush,
};

enum Suit
{
    Heart,
    Diamond,
    Spade,
    Club
};

using Rank = uint8_t;

struct Card
{
    Suit s;
    Rank r;
};

std::ostream& operator<< (std::ostream &os, Category c) {
    char const *str = nullptr;
    switch(c) {
    case HighCard:
        str = "High card";
        break;
    case Pair:
        str = "Pair";
        break;
    case TwoPair:
        str = "Two pair";
        break;
    case ThreeOfAKind:
        str = "Three of a kind";
        break;
    case FourOfAKind:
        str = "Four of a kind";
        break;
    case FullHouse:
        str = "Full house";
        break;
    case Straight:
        str = "Straight";
        break;
    case Flush:
        str = "Flush";
        break;
    case StraightFlush:
        str = "Straight flush";
        break;
    };
    return os << str;
}

Category categorize(std::array<Card, 5> &hand) {
    auto same_suit = std::all_of(std::cbegin(hand) + 1, std::cend(hand), [&](Card const &c) {
            return c.s == hand[0].s;
        });
    std::sort(std::begin(hand), std::end(hand), [](Card &a, Card &b) {
            return a.r < b.r;
        });
    std::array<uint8_t, 2> matches = { };
    bool seq = true;
    Rank prev_rank = hand[0].r;
    size_t m = 0u;
    std::for_each(std::begin(hand), std::end(hand), [&](Card const &c) {
            if (c.r == prev_rank)
                ++matches[m];
            else {
                seq = seq && ((c.r - prev_rank) == 1);
                prev_rank = c.r;
                if (matches[m] > 1)
                    matches[++m] = 1;
            }
        });
    Category c = HighCard;
    if (m == 0) {
        if (matches[0] == 1) {
            if (seq)
                c = static_cast<Category>(c | Straight);
            if (same_suit)
                c = static_cast<Category>(c | Flush);
        }
    }
    else {
        bool pair = false, twoPair = false, three = false, four = false;
        std::for_each(std::cbegin(matches), std::cbegin(matches) + m + 1, [&](uint8_t i) {
                four = four || (i == 4);
                three = three || (i == 3);
                if (i == 2) (pair ? twoPair : pair) = true;
            });
        c = four ? FourOfAKind : (three ? (pair ? FullHouse : ThreeOfAKind) : (twoPair ? TwoPair : Pair));
    }
    return c;
}

// driver program
int main() {
    std::array<Card, 5> sf {
        Card{Suit::Club, 2},
        Card{Suit::Club, 3},
        Card{Suit::Club, 4},
        Card{Suit::Club, 5},
        Card{Suit::Club, 6}
    };
    std::array<Card, 5> fok {
        Card{Suit::Club, 2},
        Card{Suit::Diamond, 2},
        Card{Suit::Spade, 2},
        Card{Suit::Heart, 2},
        Card{Suit::Club, 6}
    };
    std::array<Card, 5> fh {
        Card{Suit::Club, 2},
        Card{Suit::Diamond, 2},
        Card{Suit::Heart, 4},
        Card{Suit::Club, 4},
        Card{Suit::Spade, 4}
    };
    std::array<Card, 5> f {
        Card{Suit::Club, 10},
        Card{Suit::Club, 11},
        Card{Suit::Club, 8},
        Card{Suit::Club, 7},
        Card{Suit::Club, 2}
    };
    std::array<Card, 5> s {
        Card{Suit::Club, 5},
        Card{Suit::Spade, 6},
        Card{Suit::Heart, 7},
        Card{Suit::Club, 8},
        Card{Suit::Diamond, 9}
    };
    std::array<Card, 5> tok {
        Card{Suit::Club, 2},
        Card{Suit::Heart, 2},
        Card{Suit::Diamond, 2},
        Card{Suit::Club, 8},
        Card{Suit::Spade, 9}
    };
    std::array<Card, 5> tp {
        Card{Suit::Club, 2},
        Card{Suit::Heart, 2},
        Card{Suit::Diamond, 8},
        Card{Suit::Club, 8},
        Card{Suit::Spade, 9}
    };
    std::array<Card, 5> p {
        Card{Suit::Club, 2},
        Card{Suit::Heart, 2},
        Card{Suit::Diamond, 11},
        Card{Suit::Club, 8},
        Card{Suit::Spade, 9}
    };
    std::array<Card, 5> hc {
        Card{Suit::Heart, 12},
        Card{Suit::Spade, 2},
        Card{Suit::Diamond, 4},
        Card{Suit::Club, 7},
        Card{Suit::Spade, 9}
    };

    std::cout << categorize(sf) << '\n';
    std::cout << categorize(fok) << '\n';
    std::cout << categorize(fh) << '\n';
    std::cout << categorize(f) << '\n';
    std::cout << categorize(s) << '\n';
    std::cout << categorize(tok) << '\n';
    std::cout << categorize(tp) << '\n';
    std::cout << categorize(p) << '\n';
    std::cout << categorize(hc) << '\n';
}
