#include <iostream>
#include <vector>
#include <sstream>
#include <cstdint>
#include <algorithm>
#include <iterator>

#ifdef USE_TEMP_STR
std::vector<uint8_t> convert(const std::stringstream& buff) {
  const std::string& contents = buff.str();
  return std::vector<uint8_t>(contents.cbegin(), contents.cend());
}
#elif defined(USE_ISTREAM_ITER)
std::vector<uint8_t> convert(std::iostream& buff) {
  std::vector<uint8_t> raw_data;
  raw_data.reserve(buff.tellp());
  buff >> std::noskipws;  // sticky
  uint8_t c;
  while (buff >> c)
    raw_data.push_back(c);
  return raw_data;
}
#else  // USE_ISTREAMBUF_ITER
std::vector<uint8_t> convert(std::iostream& buff) {
  std::vector<uint8_t> raw_data;
  raw_data.reserve(buff.tellp());
  // Prefer istreambuf_iterator over istream_iterator as called out in
  // istream_iterator’s documentation [1] for two reasons:
  //   1. istreambuf_iterator is more efficient
  //   2. istream_iterator by default skips whitespace unless disabled
  //      with std::noskipws
  // [1]: http://en.cppreference.com/w/cpp/iterator/istream_iterator
  std::transform(std::istreambuf_iterator<char>(buff.rdbuf()),
                 std::istreambuf_iterator<char>(),
                 std::back_inserter(raw_data),
                 [](char c) { return static_cast<uint8_t>(c); });
  return raw_data;
}
#endif

void print_result(const std::vector<uint8_t> &raw_data) {
  std::cout << "vector<uint8_t>: [ " << std::hex;
  for (uint8_t b : raw_data) {
    std::cout << "0x" << +b << ", ";
  }
  std::cout << std::dec << "]\n";
  std::cout << "Sink length: " << raw_data.size() << '\n';
}

void load_it_up(std::ostream& os) {
  os << "This is a test.\t";
  os << "Another string.\n";
}

int main()
{
  std::stringstream buff;
  load_it_up(buff);
  std::cout << "Source: " << buff.str() << '\n';
  std::cout << "Source length: " << buff.str().size() << "\n\n";

  print_result(convert(buff));
}
