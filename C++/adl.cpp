// http://stackoverflow.com/a/17826781/183120

#include <iostream>

namespace foo {
    struct Foo {};
    void frobnicate(Foo const &)
	{
		std::cout << "in foo::frobnicate" << std::endl;
	}
}

namespace bar {
	template <typename T>
    struct Bar {
		T t;
    }; 

	template <typename T>
    void frobnicate(Bar<T> const& b)
    {
		std::cout << "in bar::frobnicate" << std::endl;
        frobnicate(b.t);
    }
}

int main () {
    bar::Bar<foo::Foo> x;
    frobnicate(x);
	std::cout << "in main" << std::endl;
    frobnicate(foo::Foo());
}
