#include <iostream>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <iomanip>
#include <limits>

//#define NDEBUG        // release build disables assert and debug prints
#include <cassert>

unsigned long long pow2(unsigned char n)
{
    // since unsigned it cannot be lesser than 0, so it's ok to check only for the upper limit
    assert(n <= (std::numeric_limits<unsigned long long>::digits) - 1);

    // although the return type is unsigned long long for the shift operator the object shifted should be
    // long enough i.e. unsigned long long x = (1 << 48) will still be wrong; although x is wide enough to
    // hold the result, the object shifted (1) is just an int which is perhaps only 32-bits long, thus the
    // right expression is unsigned long long x = (1ULL << 48);
    return 1ULL << n;
}

// usage: bin2rational 0.000110011001100110011001101
int main(int argc, char *argv[])
{
    if (argc < 2)
    {
        std::cerr << "Insufficient data" << std::endl;
        std::cerr << "Usage: bin2rational <fractional binary number>" << std::endl;
        return -1;
    }

    auto const *binary = argv[1];
    bool negative = false;
    if (binary[0] == '-')
    {
        negative = true;
        ++binary;
    }

    bool parsing_integer_part = true;
    signed power = 0;
    if (binary && (*binary == '.'))
    {
        parsing_integer_part = false;
        power = -1;
        ++binary;
    }
    else
    {
        const auto binary_end = binary + strlen(binary);
        const auto binary_point = std::find(binary, binary_end, '.');
        power = binary_point - binary - 1;
    }

    unsigned long long integer_part = 0;
    unsigned long long fract_num = 0, fract_den = 1;
    while (*binary)
    {
        if (parsing_integer_part)
        {
            if (*binary == '1')
            {
                integer_part += pow2(power);
#ifndef NDEBUG
                std::cout << power << '\t' << pow2(power) << std::endl;
#endif
            }
            else if (*binary == '.')
            {
                parsing_integer_part = false;
                ++power;
            }
            else if (*binary != '0')
            {
                std::cerr << "Invalid binary number" << std::endl;
                return -1;
            }
        }
        else
        {
            if (*binary == '1')
            {
                const auto denominator = pow2(-power);
                const auto scalar = denominator / fract_den;
                fract_num = (scalar * fract_num) + 1;
                fract_den = denominator;
#ifndef NDEBUG
                std::cout << power << "\t 1/" << denominator << '\t' << fract_num << " / " << fract_den << std::endl;
#endif
            }
            else if (*binary != '0')
            {
                std::cerr << "Invalid binary number" << std::endl;
                return -1;
            }
        }
        ++binary;
        --power;
    }

    // print fraction
    if (negative) std::cout << "-[";
    if (integer_part) std::cout << integer_part << " + (";
    std::cout << fract_num
              << " / "
              << fract_den;
    if (integer_part) std::cout << ")";
    if (negative) std::cout << "]";

    // print approximation
    using fraction_type = long double;
    std::cout << " = ";
    if (negative) std::cout << "-";
    std::cout << std::setprecision(std::numeric_limits<fraction_type>::max_digits10)
              << (integer_part + (fract_num / static_cast<fraction_type>(fract_den)))
              << std::endl;
}
