#include <GL/glfw.h>
#include <stdexcept>

#include <iostream>

template <typename T>
struct return_type;

template <typename ReturnType, typename... Args>
struct return_type<ReturnType (*) (Args...)>
{
	typedef ReturnType type;
};

template <typename UninitF>
struct RAII_wrapper_type
{
    RAII_wrapper_type(UninitF f) : _f(f) { }
    RAII_wrapper_type(RAII_wrapper_type &&that) : _f(nullptr) { std::swap(that._f, _f); }
    ~RAII_wrapper_type() { if (_f) _f(); }

    // non-copyable
    RAII_wrapper_type(const RAII_wrapper_type &) = delete;
    RAII_wrapper_type& operator=(const RAII_wrapper_type &) = delete;

  private:
    UninitF _f;
};

template <typename InitF, typename UninitF, typename RType, typename... Args>
RAII_wrapper_type<UninitF> capture(InitF init_f, UninitF uninit_f, RType succ, 
                                   const char* error, Args... args)
{
  if(init_f(args...) != succ) {
    throw std::runtime_error(error);
  }
  return RAII_wrapper_type<UninitF>(uninit_f);
}

template<typename InitF, typename UninitF, typename... Args>
RAII_wrapper_type<UninitF> capture(InitF init_f, UninitF uninit_f, Args... args)
{
  static_assert(std::is_void<typename return_type<InitF>::type>::value, "ignored return value");
  init_f(args...);
  return RAII_wrapper_type<UninitF>(uninit_f);
}

void t_void_init(int) { }
void t_uninit() { }

int t_int_init() { return 1; }

void a(int, float)
{
	std::cout << "Entered!" << std::endl;
}

void b()
{
	std::cout << "Exiting!" << std::endl;
}

int main()
{
  auto t1 = capture(t_void_init, t_uninit, 7);
  // below line always throws, to demonstrate that a failure is thrown correctly
  // auto t2 = capture(t_int_init, t_uninit, 0, "some error");
  auto t3 = capture(glfwInit, glfwTerminate, GL_TRUE, "Error!");

  // say a function of type void (int, const char*) exists, then calling
  // capture will match the first variant than second; thus such functions
  // won't work with capture
  auto t4 = capture(a, b, 0, 1.0f);
  {
	auto t5(std::move(t4));
  }
  std::cout << "Exiting main..." << std::endl;
}

