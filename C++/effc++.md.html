<meta charset="utf-8">

        **Effective C++**
    *Digest of Scott Meyer's Book*

# Chapter 1: Accustoming Yourself to C++

## 1. View C++ as a federation of languages

Parts of C++

* C
* Object-oriented
* Template/Generic
* STL

Rules for effective C++ programming vary, depending on the part of C++ you are using.

## 2. Prefer `const`s and `inline`s to `#define`s

* Use `const` instead of `#define` for constant objects.
* For pointers in header, for internal linkage, mark the pointer too as `const`
  - [`const` also gives internal linkage][const-linkage] like `inline` does for functions
  - Each [translation unit][tu] gets a copy however; avoid this with `extern const` but needs code in both header and source
  - [C++17 offers `inline` variables][inline-const]; one object for all TUs, one line in header
* Use `static` for internal linkage and `extern` for external linkage.
* Use `static` as a storage class specifier to denote static local or non-local storage duration.
* Use `static` for class-specific, as opposed to object-specific, data.
* Use `inline` functions to `#define`s.

[tu]: https://stackoverflow.com/q/1106149/183120
[const-linkage]: https://stackoverflow.com/q/2190919/#comment25949564_2190981
[inline-const]: https://stackoverflow.com/a/53898717/183120

## 3. Use `const` whenever possible

* Constant data.
* Constant pointer.
* `const` iterator is same as `const` pointer.
* `const_iterator` is the same as pointer to `const`.
* Constant member function
    + Overloading by `const`ness of a member function is possible; return `const` reference or non-`const` reference.
    + If they're the same, just define `const` version and in the non-`const` version `static_cast` to `const`, call `const` version and cast away `const` from what is returned.
    + Physical/bitwise `const` that the compiler enforces (e.g. pointers repointed is prohibited but changing the pointee is allowed) versus logical `const` that the programmer wants.
    + Use `mutable` to denote state change not observable by clients to manage internal state.
* Don't return by `const` value for types with move semantics as it'll prohibit from moving from the returned temporary.
* Return by `const` value for types with no move semantics to avoid silly expressions like `(a + b) = c`.
* The benefit of `const`-correctness is validation of design and aggressive optimisation by compiler.
* C++11 introduced `constexpr` to denote compile-time evaluated constant while `const` qualifies an entity to not be able to change something through that entity

## 4. Make sure that objects are initialized before they're used

* _Always_ initialize your objects before you use them.
* For non-member objects of built-in types, you'll need to do this manually: assigning or by passing to a function.
* For members, do it in constructor with member initialization list[^init-list]; it's more efficient than assigning since it avoids default construction immediately followed by assignment.  It directly passes argument to the object's constructor.
* Follow member declaration order in the list.
* In C++11, use in-class member initialization.
* A static object is one that exists from the time it's constructed until the end of the program;  they're automatically destroyed when the program exits.
* [Static initialization order fiasco][] - the relative order of initialization of non-local static objects defined in different translation units is undefined.
* Avoid initialization order problems across translation units by replacing non-local static objects with local static objects.

[Static initialization order fiasco]: https://isocpp.org/wiki/faq/ctors#static-init-order-on-intrinsics
[member initializer lists]: https://en.cppreference.com/w/cpp/language/constructor
[^init-list]: Official name: [member initializer lists][]; not to be confused with `std::initializer_list`.

# Chapter 2: Constructors, Destructors, and Assignment Operators

## 5. Know what functions C++ silently writes and calls

* Default constructor (when no constructor is provided)
* Copy constructor (when no constructor is provided) — it performs full member-wise copy of the object's bases and non-static members, in their initialization order, using direct initialization.
* Copy assignment operator (when one is not user provided) — it performs member-wise copy assignment of the object's bases and non-static members, in their initialization order, using built-in assignment for the scalars and copy assignment operator for class types.
* Move constructor (provided when no copy or move constructor or `operator=` are provided)
* Move assignment operator (same as above)
* Destructor (when one is not user provided)

## 6. Explicitly disallow the use of compiler-generated functions you do not want

* In C++03, declare them as `private` and give no implementation.  `boost::noncopyable` is usually used as base class to be derived from to avoid copying.  Empty base class optimisation will make sure it takes up no space in the derived object.
* In C++11, declare them `delete`d; use `default` when you want one.

## 7. Declare destructors virtual in polymorphic base classes

* _Factory_ function — a function that returns a base class pointer to a newly-created derived class object.
* C++ specifies that when a derived class object is deleted through a pointer to a base class with a non-virtual destructor, results are undefined. What typically happens at runtime is that the derived part of the object is never destroyed.  The memory is reclaimed but the dervied class destructor is never called.
* Polymorphic base classes should declare virtual destructors.  If a class has any virtual functions, it should have a virtual destructor.

## 8. Prevent exceptions from leaving destructors

* When exception occurs, stack unwinding happens, during which all automatic objects are destroyed by calling their respective destructor.  If it too throws, then there would be two active exceptions to handle; program execution either terminates or yields undefined behavior.
* Instead one could log the error to a file/terminal and
    + terminate the program
    + swallow the exception, if it's not fatal
* If class clients need to be able to react to exceptions thrown during an operation, the class should provide a regular (i.e., non-destructor) function that performs the operation.

## 9. Never call virtual functions during construction or destruction

* Don't call virtual functions during construction or destruction, because such calls will never go to a more derived class than that of the currently executing constructor or destructor.
* The solution is to have a derived class static function which will pass to the base class the required data that the virtual function would've returned.
* In other words, since you can't use virtual functions to call down from base classes during construction, you can compensate by having derived classes pass necessary construction information up to base class constructors instead.

## 10. Have assignment operators return a reference to `*this`

* This is needed to allow expression chaining e.g. `a = b = c;` i.e. `(a = (b = c))`.

## 11. Handle assignment to self in `operator=`

* Two issues to handle in `operator=`: self-assignment and exception safety.
* Self-assignment leads to useless work most of the times in a resource managing class.  Also if the owned resource is first deleted and then the new resource is copied, in a self-assignment scenario, it'd have the resource gone when we get to step 2.
* Self-assignment issue is usually avoided with a `if (this != &that)`.  The latter issue is avoided by swapping the two steps: first create the new resource copy and then delete the old one and if all went well, assign the new resource to the mangaing member.
* Usually, resource managing classes call `new` in their assignment, there by an exception thrown might lead to leaks or inconsistent states in the older object i.e. exception safety will be violated.
* Making `operator=` exception-safe typically renders it self-assignment-safe, too.  As a result, it's increasingly common to deal with issues of self-assignment by ignoring them, focusing instead on achieving exception safety.
* Use *copy-swap idiom* to make copy assignment both exception and self-assignment safe.
* Make sure `operator=` is well-behaved when an object is assigned to itself.  Techniques include comparing addresses of source and target objects, careful statement ordering, and copy-and-swap.
* Make sure that any function operating on more than one object behaves correctly if two or more of the objects are the same.

## 12. Copy all parts of an object

* A resource managing class has copy constructor and copy assignment operator: the _copying functions_.
* Since C++11 onwards, it would additionally have move constructor and a move assignment operator.
* A single assignment operator that takes `that` by value may be used as both copy assignment and move assignment operator eliminating the need for separate assignment operator functions.
* When writing a copying function, be sure to (1) copy all local data members and (2) invoke the appropriate copying function in all base classes, too.
* Don't try to implement one of the copying functions in terms of the other. Instead, put common functionality in a third function that both call.

# Chapter 3: Resource Management

## 13. Use objects to manage resources

* Resources are acquired and immediately turned over to resource-managing objects (RAII).
* Resource-managing objects use their destructors to ensure that resources are released.

## 14. Think carefully about copying behavior in resource-managing classes

What should happen when an RAII object is copied?

* Prohibit copying (`std::unique_ptr` has only moving, no copying, unlike the deprecated `std::auto_ptr`).
* Reference-count the underlying resource.
* Perform a deep copy.
* Transfer ownership of the underlying resource (`std::auto_ptr`).

## 15. Provide access to raw resources in resource-managing classes

* APIs often require access to raw resources, so each RAII class should offer a way to get at the resource it manages.
* Access may be via explicit conversion or implicit conversion. In general, explicit conversion is safer, but implicit conversion is more convenient for clients.

## 16. Use the same form in corresponding uses of `new` and `delete`

* When you employ a `new` expression (i.e., dynamic creation of an object via a use of `new`), two things happen:
    + First, memory is allocated (via a function named `operator new`). 
    + Second, one or more constructors are called for that memory.
* When you employ a `delete` expression (i.e., use `delete`), two other things happen:
    + one or more destructors are called for the memory
    + the memory is then deallocated
* If you use `[]` in a `new` expression, you must use `[]` in the corresponding delete expression.

## 17. Store `new`ed objects in smart pointers in standalone statements

In this statement

```
processWidget(std::shared_ptr<Widget>(new Widget), priority());
```

three things happen before calling `processWidget`:

1. Call `priority`.
2. Execute `new Widget`.
3. Call the `std::shared_ptr` constructor.

The order of these operations are undefined.  Hence, if the order the compiler chooses is 2, 1 and 3 and if 1 throws, we'll have a memory leak.  The way to avoid problems like this is simple: use a separate statement to create the `Widget` and store it in a smart pointer, then pass the smart pointer to `processWidget`:

```
std::tr1::shared_ptr<Widget> pw(new Widget);  // store newed object
                                              // in a smart pointer in a
                                              // standalone statement

processWidget(pw, priority());                // this call won't leak
```

# Chapter 4: Designs and Declarations

## 18. Make interfaces easy to use correctly and hard to use incorrectly

* Good interfaces are easy to use correctly and hard to use incorrectly.
* Ways to facilitate correct use include consistency in interfaces and behavioral compatibility with built-in types: when in doubt, do as the `int`s do.
* Ways to prevent errors include creating new types, restricting operations on types, constraining object values, and eliminating client resource management responsibilities.
* In the general case `std::shared_ptr` takes no `U deleter` in its constructor, since it uses the default deleter: `delete`.  If needed, there's an overload which takes a separate deleter.  How does it remember the unknown type `U`?  [It achieves this][top-cpp-aha] by having a pointer to a base class of known type, which has a virtual deleter; a templatized derived class `U` inherits from and calls the deleter in its virtual function override.
* _cross-DLL problem_: an object is created using `new` in one dynamically linked library (DLL) but is `delete`d in a different DLL.
* `shared_ptr` avoids the problem, because its uses `delete` from the same DLL where it was created.  This it achieves using _type erasure_ explained in the previous point.
* The downsides of `std::shared_ptr`:
    + It is twice the size of a raw pointer
    + Uses dynamically allocated memory for bookkeeping and deleter-specific data
    + Uses virtual function call when invoking its deleter
    + Incurs thread synchronization overhead when modifying the reference count in an application it believes is multi-threaded.

[top-cpp-aha]: http://www.artima.com/cppsource/top_cpp_aha_moments.html

## 19. Treat class design as type design

Questions to ask:

* How should objects of your new type be created and destroyed?
* How should object initialization differ from object assignment?
* What does it mean for objects of your new type to be passed by value?
* What are the restrictions on legal values for your new type?
* Does your new type fit into an inheritance graph?
* What kind of type conversions are allowed for your new type?
* What operators and functions make sense for the new type?
* What standard functions should be disallowed?
* Who should have access to the members of your new type?
* What is the “undeclared interface” of your new type?
* How general is your new type?
* Is a new type really what you need?

## 20. Prefer pass-by-reference-to-`const` to pass-by-value

* Prefer pass-by-reference-to-const over pass-by-value.  It's typically more efficient and it avoids the slicing problem.
* The rule doesn't apply to built-in types and STL iterator and function object types.  For them, pass-by-value is usually appropriate.

## 21. Don't try to return a reference when you must return an object

* Never return 
    + a pointer or reference to a local stack object
    + a reference to a heap-allocated object (e.g. temporaries allocated in freestore returned by reference will always leak)
    + a pointer or reference to a local static object if there is a chance that more than one such object will be needed i.e. singleton is the only allowed case.

## 22. Declare data members private

* Declare data members `private`. It gives clients syntactically uniform access to data, affords fine-grained access control, allows invariants to be enforced, and offers class authors implementation flexibility.
* `protected` is no more encapsulated than `public`.

## 23. Prefer non-member non-friend functions to member functions

Doing so increases

* encapsulation
* packaging flexibility
* functional extensibility

## 24. Declare non-member functions when type conversions should apply to all parameters

* If you need type conversions on all parameters to a function (including the one pointed to by the this pointer), the function must be a non-member.
* It also allows the type to work with standard library algorithms which does symmetric assumption.

## 25. Consider support for a non-throwing swap

C++03 had the _Rule of Three_; if your type needs one of these, you need all of these: copy constructor, copy assignment operator and destructor.  To implement the first two using copy-swap idiom, one needs a `swap`; using `std::swap` isn't performant as it would end up making redundant copies as it simply does `T t = a; a = b; b = t;` while you, the type author, can provide a much cheaper alternative of swapping the pointers that hold the resource.  Hence the rule is really the [_Rule of Three and a Half_][three-and-half-rule].

* Provide a non-member friend `swap` when `std::swap` would be inefficient for your type.  Make sure your `swap` doesn't throw exceptions.
* An unqualified call to `swap` would call this friend due to ADL (Argument-Dependent Lookup).
* If the caller calls `std::swap` on your type it wouldn't work, in which case a explicit (full/total) specialization of `std::swap` template for your type needs to be provided.
* When calling `swap` for a template type parameter, an unqualified call to `swap` preceded by `using std::swap` is the best approach.  If there's a friend, it'd be used due to ADL, if none available, then the `std::swap` will be used.

With C++11 memory managing classes are to follow the [_Rule of Five/Zero_][five-zero-rule] due to the new additions: move constructor and move assignment operator.

[three-and-half-rule]: http://stackoverflow.com/a/3279550
[five-zero-rule]: https://en.cppreference.com/w/cpp/language/rule_of_three

# Chapter 5: Implementations

## 26. Postpone variable definitions as long as possible

It increases program clarity and improves program efficiency.

## 27. Minimize casting

* Avoid casts whenever practical, especially `dynamic_cast`s in performance-sensitive code.  If a design requires casting, try to develop a cast-free alternative.
* When casting is necessary, try to hide it inside a function. Clients can then call the function instead of putting casts in their own code.
* Prefer C++-style casts to old-style casts. They are easier to see, and they are more specific about what they do.

## 28. Avoid returning “handles” to object internals

* Avoid returning handles (references, pointers, or iterators) to object internals.
* It increases encapsulation, helps `const` member functions act `const`, and minimizes the creation of dangling handles.

## 29. Strive for exception-safe code

When an exception is thrown, exception-safe functions:

* Leak no resources
* Don't allow data structures to become corrupted

Exception-safe functions offer one of three guarantees:

* Functions offering the **basic guarantee** promise that if an exception is thrown, everything in the program remains in a valid state. No objects or data structures become corrupted, and all objects are in an internally consistent state (e.g., all class invariants are satisfied). However, the exact state of the program may not be predictable
* Functions offering the **strong guarantee** promise that if an exception is thrown, the state of the program is unchanged. Calls to such functions are atomic in the sense that if they succeed, they succeed completely, and if they fail, the program state is as if they'd never been called.
* Functions offering the **nothrow guarantee** promise never to throw exceptions, because they always do what they promise to do.  This is a dynamic exception specification deprecated by C++11, which introduces the compile-time `noexcept` specification which allows the compiler to perform better optimisation.

All operations on built-in types (e.g., ints, pointers, etc.) never throw (i.e., offer the noexcept guarantee). This is a critical building block of exception-safe code.

* The strong guarantee can often be implemented via copy-swap idiom, but the strong guarantee is not practical for all functions.
* A function can usually offer a guarantee no stronger than the weakest guarantee of the functions it calls.

## 30. Understand the ins and outs of inlining

* It is more of a cue to the linker telling that it's defined in multiple translation units and only one should be considered.
* Just a hint to the compiler; it may or may not honour it.
* Functions defined inside the class definition is inline.
* Downside is code bloat.

## 31. Minimize compilation dependencies between files

* Avoid using objects when object references and pointers will do.
* Depend on class declarations instead of class definitions whenever you can.
* Provide separate header files for declarations and definitions.
* The general idea behind minimizing compilation dependencies is to depend on declarations instead of definitions.  Two approaches based on this idea are Handle classes and Interface classes.
* Library header files should exist in full and declaration-only forms.  This applies regardless of whether templates are involved.

# Chapter 6: Inheritance and Object-Oriented Design

## 32. Make sure public inheritance models “is-a”

* Public inheritance means “is-a.”  Everything that applies to base classes must also apply to derived classes, because every derived class object is a base class object.

## 33. Avoid hiding inherited names

* Names in derived classes hide names in base classes.  Under public inheritance, this is never desirable.
* To make hidden names visible again, employ `using` declarations or forwarding functions.

## 34. Differentiate between inheritance of interface and inheritance of implementation

* Inheritance of interface is different from inheritance of implementation.  Under public inheritance, derived classes always inherit base class interfaces.
* Pure virtual functions specify inheritance of interface only.
* Simple (impure) virtual functions specify inheritance of interface plus inheritance of a default implementation.
* Non-virtual functions specify inheritance of interface plus inheritance of a mandatory implementation.

## 35. Consider alternatives to virtual functions

* The Template Method Pattern via the Non-Virtual Interface Idiom
  + Public non-virtual interface method calling private virtual function overridden by derived
  + Base class forms a template: allows {calling,checking} pre-/post-{functions,condition}
* The Strategy Pattern via function pointers
* The Strategy Pattern via `std::function`
* The “Classic” Strategy Pattern

## 36. Never redefine an inherited non-virtual function

Apart from hiding the inherited implementation (_Item 33_), it also breaks the base's contract i.e. assumptions client would make that derived is a base.

## 37. Never redefine a function's inherited default parameter value

This is because the default parameter values are statically bound, while virtual functions — the only functions you should be overriding — are dynamically bound.

## 38. Model “has-a” or “is-implemented-in-terms-of” through composition

* Composition has meanings completely different from that of public inheritance.  In C++ composition is for code reuse and not for interface compliance.
* In the application domain, composition means has-a.  In the implementation domain, it means is-implemented-in-terms-of.

## 39. Use private inheritance judiciously

* Private inheritance means is-implemented-in-terms of.  It's usually inferior to composition, but it makes sense when a derived class needs access to protected base class members or needs to redefine inherited virtual functions.
* Unlike composition, private inheritance can enable the empty base optimization.  This can be important for library developers who strive to minimize object sizes.
* Use composition whenever you can; use private inheritance whenever you've to.

## 40. Use multiple inheritance judiciously

* Multiple inheritance is more complex than single inheritance.  It can lead to new ambiguity issues and to the need for virtual inheritance.
* Diamond problem solved with virtual inheritance.  It would make only one instance of the grand parent in the derived object's memory layout.
* Virtual inheritance imposes costs in size, speed, and complexity of initialization and assignment.  It's most practical when virtual base classes have no data.
* Multiple inheritance does have legitimate uses.  One scenario involves combining public inheritance from an Interface class with private inheritance from a class that helps with implementation.

# Chapter 7: Templates and Generic Programming

## 41. Understand implicit interfaces and compile-time polymorphism

* Both classes and templates support interfaces and polymorphism.
* For classes, interfaces are explicit and centered on function signatures.  Polymorphism occurs at runtime through virtual functions.
* For template parameters, interfaces are implicit and based on valid expressions.  Polymorphism occurs during compilation through template instantiation and function overloading resolution.

## 42. Understand the two meanings of typename

* When declaring template type parameters, `class` and `typename` are interchangeable.
* Use `typename` to identify nested dependent type names, except in base class lists or as a base class identifier in a member initialization list.

## 43. Know how to access names in templatized base classes

In derived class templates, refer to names in base class templates via 

  * “`this->`” prefix
  * `using` declarations
  * explicit base class qualification

## 44. Factor parameter-independent code out of templates

* Templates generate multiple classes and multiple functions, so any template code not dependent on a template parameter causes bloat.
* Bloat due to non-type template parameters can often be eliminated by replacing template parameters with function parameters or class data members.
* Bloat due to type parameters can be reduced by sharing implementations for instantiation types with identical binary representations.

## 45. Use member function templates to accept “all compatible types”

* Use member function templates to generate functions that accept all compatible types.
* If you declare member templates for generalized copy construction or generalized assignment, you'll still need to declare the normal copy constructor and copy assignment operator, too.

## 46. Define non-member functions inside templates when type conversions are desired

When writing a class template that offers functions related to the template that support implicit type conversions on all parameters, define those functions as friends inside the class template.

## 47. Use traits classes for information about types

* Traits classes make information about types available during compilation. They're implemented using templates and template specializations.
* In conjunction with overloading, traits classes make it possible to perform compile-time `if...else` tests on types.

## 48. Be aware of template metaprogramming

* Template metaprogramming can shift work from runtime to compile-time, thus enabling earlier error detection and higher runtime performance.
* TMP can be used to generate custom code based on combinations of policy choices, and it can also be used to avoid generating code inappropriate for particular types.

# Chapter 8: Customizing new and delete

## 49. Understand the behavior of the new-handler

* When `operator new` is unable to fulfill a memory request, it calls the new-handler function repeatedly until it *can* find enough memory.
* A well-designed new-handler function must do one of the following:
    + Make more memory available.
    + Install a different new-handler.
    + Deinstall the new-handler.
    + Throw an exception.
    + Abort or exit.
* `set_new_handler` allows you to specify a function to be called when memory allocation requests cannot be satisfied.
* Nothrow `new` is of limited utility, because it applies only to memory allocation; subsequent constructor calls may still throw exceptions.

## 50. Understand when it makes sense to replace `new` and `delete`

These are three of the most common reasons:

* To detect usage errors (debugging).
* To improve efficiency.
* To collect usage statistics.
* To reduce the space overhead of default memory management.
* To cluster related objects near one another.
* To compensate for suboptimal alignment in the default allocator.

## 51. Adhere to convention when writing `new` and `delete`

* `operator new` should
    + contain an infinite loop trying to allocate memory
    + call the new-handler if it can't satisfy a memory request
    + handle requests for zero bytes.
* Class-specific versions should handle requests for larger blocks than expected.
* `operator delete` should do nothing if passed a pointer that is null.
* Class-specific versions should handle blocks that are larger than expected.

## 52. Write placement `delete` if you write placement `new`

* The term “placement new” is overloaded.
* Most of the time when people talk about placement `new`, they're talking about this specific function, the `operator new` taking a single extra argument of type `void*`.
* Less commonly, they're talking about any version of `operator new` that takes extra arguments.
* It's important to understand that the general term “placement new” means any version of `new` taking extra arguments, because the phrase “placement delete” derives directly from it.
* When a constructor that follows a call to the placement `new` function throws, the runtime system looks for a version of `operator delete` that takes _the same number and types of extra arguments_ as `operator new`, and, if it finds it, that's the one it calls. If none can be found, the runtime does nothing, thereby leaking memory!
* The rule is simple: If an `operator new` with extra parameters isn't matched by an `operator delete` with the same extra parameters, no `operator delete` will be called if a memory allocation by the `new` needs to be undone.
* Placement `delete` is called only if an exception arises from a constructor call that's coupled to a call to a placement `new`. Applying `delete` to a pointer never yields a call to a placement version of delete. *Never*.  It invokes the normal `operator delete`.
* When you declare placement versions of new and delete, be sure not to unintentionally hide the normal versions of those functions.

# Chapter 9: Miscellany

## 53. Pay attention to compiler warnings

* Take compiler warnings seriously, and strive to compile warning-free at the maximum warning level supported by your compilers.
* Don't become dependent on compiler warnings, because different compilers warn about different things.  Porting to a new compiler may eliminate warning messages you've come to rely on.

## 54. Familiarize yourself with the standard library

The primary standard C++ library functionality consists of the STL, iostreams, locales, smart pointers, generalized function pointers (`std::function`), hash-based containers, regular expressions.

## 55. Familiarize yourself with Boost

* Boost is a community and web site for the development of free, open source, peer-reviewed C++ libraries.  Boost plays an influential role in C++ standardization.
* Boost offers implementations of many components, but it also offers many other libraries, too.


<!-- Markdeep: --><link rel="stylesheet" href="https://morgan3d.github.io/markdeep/latest/apidoc.css?"><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace;}</style><style>.md h1:before, .md h2:before { content: none; }</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'medium', inlineCodeLang: 'c++' };</script>
