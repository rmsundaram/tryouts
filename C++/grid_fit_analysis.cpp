#include <d2d1.h>

#include <algorithm>
#include <type_traits>
#include <iostream>

#include <cstdio>
#include <cmath>

typedef D2D_POINT_2F Point;
typedef D2D_RECT_F Rect;

template <typename T, size_t N>
const char(&array_len(const T (&) [N]))[N];
#define ArrayLength(Name) sizeof(array_len(Name))

float adjusted_stroke_width(float scale)
{
    return ceil(scale) / scale;
}

Point transform(const Point &point, float factor)
{
    Point ret = {point.x * factor, point.y * (-factor)};
    return ret;
}

Point& operator+=(Point &lhs, float rhs_delta)
{
    lhs.x += rhs_delta, lhs.y += rhs_delta;
    return lhs;
}

std::ostream& operator<<(std::ostream &os, const Point &p)
{
    return os << p.x << ", " <<  p.y;
}

float AlignCoordinate(float coordinate)
{
    return ((coordinate < 0.0f) && (coordinate > -0.5f)) ? 0.0f : floor(coordinate);
}

Point AlignCoordinate(const Point &p)
{
    Point ret = {AlignCoordinate(p.x), AlignCoordinate(p.y)};
    return ret;
}

void grid_fit(const Point &left_top,
              const Point &right_bottom,
                    bool   fAlignToInteger,
                    float  scale,
                    float  strokeWidth,
                    Point *pDev_LT,
                    Point *pDev_RB)
{
    unsigned uStrokeWidth = 0u;
    if (!fAlignToInteger)
    {
        const auto oneDevicePixel = scale;
        uStrokeWidth = static_cast<unsigned>((strokeWidth * oneDevicePixel) + 0.5f); 
        if (uStrokeWidth == 0)
        {
            uStrokeWidth = 1;
        }
    }

    const auto dev_real_LT = transform(left_top, scale);
    const auto dev_real_RB = transform(right_bottom, scale);

    auto dev_LT = AlignCoordinate(dev_real_LT);
    auto dev_RB = AlignCoordinate(dev_real_RB);

    if (uStrokeWidth & 1)
    {
        dev_LT += 0.5f;
        dev_RB += 0.5f;
    }

    // adjust for over compensation
    const auto real_width_diff = std::abs(dev_real_LT.x - dev_real_RB.x);
    const auto real_height_diff = std::abs(dev_real_LT.y - dev_real_RB.y);

    const auto int_width_diff = std::abs(dev_LT.x - dev_RB.x);
    const auto int_height_diff = std::abs(dev_LT.y - dev_RB.y);

/*
width(2.9, 3.0) = 0.1
width(2, 3) = 1
3, 4
adding 0.9 to compensate for 0.1 is an overkill
*/

    // 29 hits for 37 rects on 150 DPI
    if ((0.0f == int_width_diff) ||                 // 15 hits for 37 rects on 150 DPI
        (int_width_diff < real_width_diff))         // 14 hits for 37 rects on 150 DPI
    {
        (dev_LT.x > dev_RB.x) ? dev_LT.x : dev_RB.x += 1.0f;
    }
    // 37 hits for 37 rects on 110 zoom
    if ((0.0f == int_height_diff) ||
        (int_height_diff < real_height_diff))
    {
        (dev_LT.y > dev_RB.y) ? dev_LT.y : dev_RB.y += 1.0f;
    }

    // brute force
    if (dev_LT.x == dev_RB.x)
    {
        dev_RB.x += 1.0f;
    }
    if (dev_LT.y == dev_RB.y)
    {
        dev_RB.y += 1.0f;
    }

    *pDev_LT = dev_LT;
    *pDev_RB = dev_RB;
}

int main(int argc, char* argv[])
{
    // 37 rectangles from content stream
    const Point pdf[] =
    {
//      left, top, right, bottom
        {342, -200}, {343, -263},
        {345, -200}, {345, -263},
        {348, -200}, {350, -263},
        {353, -200}, {353, -263},
        {355, -200}, {356, -263},
        {359, -200}, {361, -263},
        {364, -200}, {364, -263},
        {368, -200}, {368, -263},
        {372, -200}, {373, -263},
        {375, -200}, {375, -263},
        {377, -200}, {378, -263},
        {383, -200}, {383, -263},
        {386, -200}, {387, -263},
        {390, -200}, {391, -263},
        {393, -200}, {394, -263},
        {397, -200}, {398, -263},
        {400, -200}, {402, -263},
        {404, -200}, {404, -263},
        {408, -200}, {409, -263},
        {413, -200}, {413, -263},
        {415, -200}, {415, -263},
        {419, -200}, {419, -263},
        {424, -200}, {424, -263},
        {427, -200}, {428, -263},
        {430, -200}, {430, -263},
        {433, -200}, {433, -263},
        {438, -200}, {439, -263},
        {441, -200}, {443, -263},
        {445, -200}, {446, -263},
        {448, -200}, {450, -263},
        {452, -200}, {452, -263},
        {456, -200}, {458, -263},
        {460, -200}, {461, -263},
        {463, -200}, {464, -263},
        {468, -200}, {470, -263},
        {472, -200}, {472, -263},
        {474, -200}, {475, -263}
    };

    // 1.4546521f for 150 DPI printer
    // 6.2500005f for 600 DPI printer
    // 1.0f for 100% zoom, 1.1003788f for 110% zoom
    const auto scale_factor = 1.4546521f;
    const auto pdfStrokeWidth = 1.0f;
    const auto strokeWidth = adjusted_stroke_width(pdfStrokeWidth * scale_factor);
    const auto strokeHalfWidth = 0.5f * strokeWidth;
    const auto alignToInt = false;

    Point dev[ArrayLength(pdf)] = { };
    for (auto i = 0u; i < ArrayLength(pdf); i += 2u)
    {
        grid_fit(pdf[i], pdf[i + 1], alignToInt, scale_factor, strokeWidth, &dev[i], &dev[i + 1]);
        dev[i].x -= strokeHalfWidth;
        dev[i + 1].x += strokeHalfWidth;
    }

    float gaps[(ArrayLength(pdf) / 2u)] = { std::numeric_limits<float>::infinity() };
    auto count = 0u;
    for (auto i = 2u; i < ArrayLength(dev); i += 2u)
    {
        gaps[i / 2u] = std::abs(dev[i - 1].x - dev[i].x);
        count += (gaps[i / 2u] < 1.0f) ? 1u : 0u;
    }

    std::cout << "#\tPDF Coords\t\tDev Coords\t\t\tGaps" << std::endl;
    for (auto i = 0u; i < ArrayLength(dev); i += 2u)
    {
        std::cout << (i / 2u + 1) << ".\t"
                  << pdf[i] << ", " << pdf[i + 1] << '\t'
                  << dev[i] << ", " << dev[i + 1] << '\t'
                  << gaps[i / 2u] << std::endl;
    }
    const auto total_gaps = (ArrayLength(pdf) / 2) - 1;
    std::cout << "\nGaps killed    = " << count;
    std::cout << "\nGaps surviving = " << total_gaps - count << std::endl;
}
