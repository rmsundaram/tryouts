#include <iostream>
#include <utility>

struct Circle {
    float r;
};

Circle makeCircle(float x) {
    return Circle{x};
}

// binds to lvalue
void growCircle1(Circle& c) {
}

// binds to rvalue (prvalue + xvalue)
void growCircle2(Circle&& c) {
}

// binds to lvalue and rvalue (prvalue + xvalue)
void printCircle(const Circle& c) {
}
// should be used only for constructors and NOT for general methods to avoid
// needless allocation; see Herb Sutter’s CppCon 2014 presentation on defaults
// for in/out parameter passing to functions; pages 22 to 34 in
// https://github.com/CppCon/CppCon2014/blob/master/Presentations/Back%20to%20the%20Basics!%20Essentials%20of%20Modern%20C%2B%2B%20Style/Back%20to%20the%20Basics!%20Essentials%20of%20Modern%20C%2B%2B%20Style%20-%20Herb%20Sutter%20-%20CppCon%202014.pdf

// binds to everything by calling respective constructor
void showCircle(Circle c) {
}

int main()
{
    Circle c1{2.3f};

    growCircle1(c1);                // OK: bind lvalue-ref to lvalue
    //growCircle1(std::move(c1));   // Error: cannot bind non-const lvalue-ref to rvalue (specifically xvalue)
    //growCircle1(makeCircle(4.2)); // Error: cannot bind non-const lvalue-ref to rvalue (specifically prvalue)

    printCircle(c1);                // OK: bind const lvalue-ref to lvalue
    printCircle(std::move(c1));     // OK: bind const lvalue-ref to xvalue
    printCircle(makeCircle(3.5));   // OK: bind const lvalue-ref to prvalue

    //growCircle2(c1);              // Error: cannot bind rvalue-ref to lvalue
    growCircle2(std::move(c1));     // OK: bind rvalue-ref to xvalue
    growCircle2(makeCircle(0.4));   // OK: bind rvalue-ref to prvalue 

    showCircle(c1);                 // OK: bind lvalue to lvalue; copied
    showCircle(std::move(c1));      // OK: bind lvalue to xvalue; moved
    showCircle(makeCircle(8.3));    // OK: bind lvalue to prvalue; moved
}
