#include <utility>
#include <iostream>

// https://en.cppreference.com/w/cpp/feature_test#Library_features
// https://en.cppreference.com/w/cpp/preprocessor/include
// Check language standard (pre-C++11) and library support (C++17)
#if (__cplusplus >= 202002L) && __has_include(<source_location>)
#  include <source_location>                        // C++20
#endif

// https://en.cppreference.com/w/cpp/utility/source_location
// https://gcc.gnu.org/onlinedocs/gcc/Function-Names.html
// https://en.cppreference.com/w/cpp/preprocessor/replace
#ifdef __cpp_lib_source_location
#  define FN_NAME \
  std::source_location::current().function_name()   // C++20
#elif defined(__PRETTY_FUNCTION__)
#  define FN_NAME __PRETTY_FUNCTION__               // GCC-specific
#else
#  define FN_NAME __func__                          // C++11 standard
#endif

void inner(const int&) {
  std::cout << FN_NAME << '\n';
}

void inner(int&&) {
  std::cout << FN_NAME << '\n';
}

// Universal reference perfectly forwarded using std::forward forwards preserves
// value category of the argument passed. Don't let T&& fool you; x is not an
// rvalue reference. When it appears in a type-deducing context, T&& acquires a
// special meaning (universal reference). When this function is instantiated, T
// depends on whether the argument passed is an lvalue or an rvalue. If it's an
// lvalue of type U, T is deduced to U&. If it's an rvalue, T is deduced to U.
// http://eli.thegreenplace.net/2014/perfect-forwarding-and-universal-
// references-in-c/
// http://stackoverflow.com/a/3860676
// http://stackoverflow.com/a/32350146
template<class T>
void outer(T&& x) {
  inner(std::forward<T>(x));
}

int main() {
  int k = 23;
  outer(k);     // outer<T=int&> → forward<int&> → #1
  outer(k + 2); // outer<T=int>  → forward<int>  → #2
}
