#include<iostream>

/*
	SO: https://stackoverflow.com/q/4178175/183120
	No virtual functions
	No private/protected non-static data memebers (private/protected member functions and static data are allowed)
	No user-defined ctors (compiler endowed ctors may be there for the class)
	Can have a user-declared/defined copy-assignment operator and/or destructor
	An array is an aggregate even it is an array of non-aggregate class type
    Brace-or-equal-initializers for non-static data members are allowed since C++14
    Public, non-virtual base classes are allowed since C++17
*/
class AggregateType
{
public:
    int a;
    char b{5};                 // C++14 allows in-class initializers in aggregates

    int getA()
    {
        return a;
    }

    void putA(int a)
    {
        this->a = a;
    }
};

int main()
{
    AggregateType b = {1, 'a'};
    // Member-wise initialization within braces implies class is nothing more than the sum of its members. A
    // user-defined constructor means the dev needs to do some extra work to initialize the members; if virtual
    // functions exist, it means class has (on most implementations) a pointer to so-called vtable, needs to be set in
    // constructor -- in these case aggregate-initialization would be insufficient.

    std::cout << b.getA() << '\t' << std::is_aggregate<AggregateType>::value << std::endl;
}
