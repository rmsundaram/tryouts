#include <string>
#include <iostream>
#include <memory>

struct Tracer
{
  int val;
  ~Tracer() { std::cout << __func__; }
};

int main() {
  char const* s[] = { "hello", "world" };
  auto constexpr n = std::size(s);

  // uninitialized aligned storage buffer on the stack
  // https://stackoverflow.com/q/4552504
  std::aligned_storage<sizeof(std::string), n>::type buffer;
  std::string* str_begin = reinterpret_cast<std::string*>(&buffer);

  // copy a range to uninitialized memory and call placement-new
  // only calls constructor; no memory allocation
  std::string* str_end = std::uninitialized_copy_n(s, n, str_begin);

  for (auto it = str_begin; it != str_end; ++it)
    std::cout << *it << '\n';

  // placement delete on a range; calls p->~string() ∀ p via std::destroy_at
  // no memory deallocation
  std::destroy(str_begin, str_end);

  alignas(Tracer) unsigned char buf[sizeof(Tracer)];
  // placement new and delete
  Tracer* t = new (buf) Tracer{2};
  std::cout << t->val << '\n';
  std::destroy_at(t);

  // If you didn’t have Tracer* and want it from buf, you can’t do
  // 1. placement new again since it’s already constructed
  // 2. reinterpret_cast as it’s undefined behaviour due to strict aliasing rule
  // You’d have to use std::launder i.e. to prevent the compiler from tracing
  // where you got your object pointer from, this forcing it to avoid any
  // optimizations that no longer apply.
  // https://stackoverflow.com/q/39382501/183120

  // [1] has a very simple example of breaking strict aliasing rule in turn
  // breaking compiler assumptions leading to UB.  C++17 adds std::launder to
  // let the compiler know you’re accessing a different object at an address
  // than what you have a pointer to [2].
  // [1]: https://stackoverflow.com/a/51983267/183120
  // [2]: https://stackoverflow.com/a/51983162/183120
}
