#include <iostream>
#include <cstdint>
#include <cmath>

uint8_t digit_count(uint64_t input) {
  if (input == 0)  // log₁₀(0) is undefined
    return 1;
  return static_cast<uint8_t>(std::trunc(std::log10(input)) + 1.0f);
}

int main(int argc, char** argv) {
  if (argc < 2) {
    std::cerr << "Usage: armstrong NUMBER\n";
    return 0;
  }

  // TODO: check for negative, overflows/limits, etc.
  const uint64_t input = strtoul(argv[1], nullptr, 0);
  const uint8_t digits = digit_count(input);
  uint64_t sum = 0u;
  for (auto temp = input; temp > 0; temp /= 10) {
    uint8_t digit = temp % 10;
    sum += pow(digit, digits);
  }
  std::cout << "armstrong(" << input << "): "
            << std::boolalpha << (sum == input)
            << '\n';
}
