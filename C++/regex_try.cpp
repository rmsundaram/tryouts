#include <regex>
#include <string>
#include <iostream>

int main(int argc, const char *argv[])
{
    if (argc > 1)
    {
        // simple E-Mail ID regex pattern
        const std::regex re(".+\\@.+\\..+");
        const std::string strSearch(argv[1]);
        std::cout << std::boolalpha << std::regex_match(strSearch, re) << std::endl;
    }
    else
    {
        // using basic_string::compare as String.BeginsWith
        std::string str("abcdefghxyz");
        if (0 == str.compare(0, 4, "abcd")) std::cout << "Yes!" << std::endl;
    }
}
