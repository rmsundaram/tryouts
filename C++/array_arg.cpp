#include <iostream>
using namespace std;

// http://stackoverflow.com/a/17569578/183120

// safest
/*
Note: when just giving int a[rows][cols] without
int (&a) [rows][cols] i.e. if not a reference
the compiler will not be able to deduce rows because
it becomes the case below i.e. a[][cols] or (*a)[cols]
cols is known to be 10, while rows are not deduced since
the parameter passed is decayed into pointer losing its
first dimension, thereby expecting the caller to pass it
as an arg when calling the function
*/
template <size_t rows, size_t cols>
void process_2d_array_template(int (&a)[rows][cols])
{
	cout << __func__ << endl;
	for (size_t i = 0; i < rows; ++i)
	{
		cout << i << ": ";
		for (size_t j = 0; j < cols; ++j)
			cout << a[i][j] << '\t';
		cout << endl;
	}
}

/* not as safe as above, but better than the below ones
compiler flags an error if the 2nd index isn't 10; another
way of declaring the formal arg. is int a[][10] which is
the same as a single pointer to a 10 integer array due to
array's decay-to-pointer behaviour; in this notation, all
but the first dimension is to be specified */
void process_2d_array(int (*a)[10], size_t rows)
{
	cout << __func__ << endl;
	for (size_t i = 0; i < rows; ++i)
	{
		cout << i << ": ";
		for (size_t j = 0; j < 10; ++j)
			cout << a[i][j] << '\t';
		cout << endl;
	}
}

// completely depends on the caller's correctness in passing the
// right values for bounds
void process_pointer_2_pointer(int **a, size_t rows, size_t cols)
{
	cout << __func__ << endl;
	for (size_t i = 0; i < rows; ++i)
	{
		cout << i << ": ";
		for (size_t j = 0; j < cols; ++j)
			cout << a[i][j] << '\t';
		cout << endl;
	}
}

/*
this is the same as the above case; compiler ignores the [5]
and perhaps it's only a hint for the caller to mean 5 rows are
expected but strictly any int** can be passed and the compiler
won't flag for anything based on any length, since when passing
a pointer, like explained in the first case, the first
dimension is always lost as the array isn't sent as is, but it
decays to a pointer and then is a pointer copy to the
function's argument variable; so passing x[6][10] would also be
not flagged and leads to UB, since only this is programmed to
work only on row count 5 due to the magic number buried
*/
void process_array_of_pointers(int* a[5], size_t cols)
{
	cout << __func__ << endl;
	for (size_t i = 0; i < 5; ++i)
	{
		cout << i << ": ";
		for (size_t j = 0; j < cols; ++j)
			cout << a[i][j] << '\t';
		cout << endl;
	}
}

int main()
{
	int b[5][10] = {
                     {1, 2, 3, 4, 5, 6, 7, 8, 9, 10 },
                     {2, 3, 4, 5, 6, 7, 8, 9, 10, 11 },
                     {3, 4, 5, 6, 7, 8, 9, 10, 11, 12 },
                     {4, 5, 6, 7, 8, 9, 10, 11, 12, 13 },
                     {5, 6, 7, 8, 9, 10, 11, 12, 13, 14 },
                   };
	process_2d_array_template(b);
	process_2d_array(b, 5);

	int *a[5];
	for (auto i = 0; i < 5; ++i)
	{
		//a[i] = new int [10]{};
        a[i] = b[i];
	}
	process_pointer_2_pointer(&a[0], 5, 10);
	process_array_of_pointers(a, 10);
}
