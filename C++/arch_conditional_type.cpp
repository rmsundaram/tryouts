#include <iostream>
#include <type_traits>

// http://stackoverflow.com/questions/17854407/how-to-make-a-conditional-typedef-in-c

int main()
{
	std::cout << sizeof(int) << std::endl;
	std::cout << sizeof(void*) << std::endl;
	// on 64-bit Windows with MinGW64 sizeof(int) = sizeof(long) = 4
	// hence the right check would be based on pointer size
	std::conditional<sizeof(void*) >= 4, char[4], char>::type var;
    // http://stackoverflow.com/questions/5894892/why-do-i-need-to-use-parentheses-after-sizeof
    // The operand is either an expression, which is an unevaluated operand (Clause 5), or a parenthesized
    // type-id - §5.3.3, N3337
	std::cout << sizeof var << std::endl;
}
