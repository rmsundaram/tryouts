#include <type_traits>
#include <iostream>

using std::cout;
using std::endl;

/*
    Local types cannot be used in any of these solutions since local type template instantiation is
	disallowed in C++03 and allowed since C++11 (http://stackoverflow.com/a/20897201/183120)
*/
#ifdef _MSC_VER		// if C++03 then this solution works
template <typename T, size_t N>
typename std::enable_if<(std::is_same<T, char>::value || std::is_same<T, wchar_t>::value), const char>::type
(&strLiteralLen(const T (&strLiteral) [N]))[N - 1];
#define staticStringLiteralLength(strName) sizeof(strLiteralLen(strName))

#else	// these work only if the compiler supports C++11

// since GCC supports constexpr this function is a simpler approach for compile-time evaluation of array size
template <typename T, size_t N>
inline constexpr typename std::enable_if<(std::is_same<T, char>::value || std::is_same<T, wchar_t>::value), size_t>::type
staticStringLiteralLength(const T (&strName) [N])
{
    return N - 1;
}

// another more elegant C++11 alternative (supported by both GCC and MSVC)
template <typename T>
inline constexpr size_t strLiteralLength(const T&)
{
	return std::extent<T>::value - 1;
}

// most elegant C++17 alternative due to std::size
template <typename T>
inline constexpr size_t strLiteralLengthFromSize(const T &s)
{
    return std::size(s) - 1;
}
#endif

int main()
{
    const char s[][4] = {"abc", "def", "xyz" };

	// range-based for C++11!
	// can use std::remove_extent (recursively) to get to the inner-most level and
	// std::rank to know the number of dimensions
	for (const char (&si)[4] : s)
	{
		cout << si << endl;
	}

    cout << std::size(s) << endl;

    const wchar_t a[] = L"String";
    const char b[] = "String";
    cout << "Array length = " << std::size(a) << endl;
    cout << "staticStringLiteralLength = " << staticStringLiteralLength(a) << endl;
    cout << "staticStringLiteralLength = " << staticStringLiteralLength(b) << endl;
    cout << "staticStringLiteralLength = " << strLiteralLength(b) << endl;
    cout << "staticStringLiteralLength = " << strLiteralLengthFromSize(b) << endl;
    cout << "wcslen = " << wcslen(a) << endl;

    // only if this statement compiles, does it prove that staticStringLiteralLength is a compile-time const
    int abc[staticStringLiteralLength(b)] = { 0 };
    abc[0] = 1;
    abc[1] = abc[0];
	cout << "size(abc) = " << std::size(abc) << endl;
}
