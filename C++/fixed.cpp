// g++ -Wall -std=c++20 -pedantic -pedantic-errors -o fp fixed.cpp
//
// REFERENCES
//
// 1. Essential Math, 2/e, Appendix C: More on Real-World Computer Number Representation
// 2. What’sACreel
//      https://www.youtube.com/watch?v=S12qx1DwjVk
//      https://www.youtube.com/watch?v=npQF28g6s_k
// 3. OneLoneCoder: https://www.youtube.com/watch?v=ZMsrZvBmQnU
// 4. https://jet.ro/files/The_neglected_art_of_Fixed_Point_arithmetic_20060913.pdf
// 5. https://stackoverflow.com/q/79677/183120
// 6. https://stackoverflow.com/a/6725981/183120
//
// SEE ALSO
//
// 1. http://www.digitalsignallabs.com/fp.pdf
// 2. https://mikelankamp.github.io/fpm/

#include <cstdint>
#include <bit>
#include <cmath>
#include <bit>
#include <type_traits>
#include <iomanip>
#include <iostream>

// M.N → M integral bits + N fractional bits
// Scaling factor: 2⁻ⁿ for M.N fixed-point (or even integers e.g. u16 is 16.0)
// Range and Precision
//   Integral Range: [-2^(m-1), 2^(m-1) - 2⁻ⁿ]
//   Precision (distance b/w successive values i.e. ulp): 2⁻ⁿ
// Absolute error: ½ × 2⁻ⁿ
// `fixed` doesn’t have any trap representations [6]; no NaN, Inf, etc.
// Divide by zero results in the usual SIGFPE/crash.

// Refer fixed44.cpp for a simpler (no template) implementation.

template <unsigned M, unsigned N>
struct fixed
{
  constexpr static unsigned BITS = M + N;
  // Required bit width should a POT and fit in an existing type.
  static_assert((std::popcount(BITS) == 1) && (BITS <= 32),
                "Unsupported fixed-point type expected.");
  constexpr static unsigned BYTE_COUNT = std::max(BITS / 8u, 1u);
  // https://www.codeproject.com/Tips/1244491/Cplusplus-Compile-time-Conditional-Types
  using underlying_type = typename std::conditional<
    BYTE_COUNT == 1u,
    int8_t,
    typename std::conditional<
      BYTE_COUNT == 2u,
      int16_t, int32_t
      >::type
    >::type;
  using unsigned_underlying_type = typename std::conditional<
    BYTE_COUNT == 1u,
    uint8_t,
    typename std::conditional<
      BYTE_COUNT == 2u,
      uint16_t, uint32_t
      >::type
    >::type;
  using promoted_type = typename std::conditional<
    BYTE_COUNT == 1u,
    int16_t,
    typename std::conditional<
      BYTE_COUNT == 2u,
      int32_t, int64_t
      >::type
    >::type;

  underlying_type v;
  constexpr static int HALF = 1 << (N - 1);
  constexpr static int ONE = 1 << N;

  constexpr fixed() : v{} {}
  constexpr fixed(unsigned_underlying_type bits)
    : v {std::bit_cast<underlying_type>(bits)} { }

  // Add (fixed space) 0.5 and return integer part for nearest int.  Use next
  // integer type as rounding would need that.
  // e.g. fixed<4, 4>::round({7.9375}) = 8
  constexpr promoted_type round() const { return (v + HALF) >> N; }

  // Just shifting won’t work for -ve values; negate, shift and add sign.
  constexpr underlying_type trunc() const {
    return (v < 0) ? -(-v >> N) : (v >> N);
  }

  constexpr fixed operator+(fixed other) const {
    other.v += v;
    return other;
  }

  constexpr fixed operator-(fixed other) const {
    other.v = v - other.v;
    return other;
  }

  constexpr fixed operator*(fixed other) const {
    promoted_type x = v;
    promoted_type y = other.v;
    promoted_type r = x * y;
    r >>= N;
    fixed f;
    f.v = static_cast<underlying_type>(r);
    return f;
  }

  // No special handling of divide by zero; does what regular integer divide by
  // zero does on your platform + toolchain.
  constexpr fixed operator/(fixed other) const {
    promoted_type r = v;
    r <<= N;
    r /= other.v;
    fixed f;
    f.v = static_cast<underlying_type>(r);
    return f;
  }

  constexpr fixed& operator++() {
    v += ONE;
    return *this;
  }

  constexpr fixed operator++(int) {
    fixed r{*this};
    ++*this;
    return r;
  }

  constexpr fixed& operator--() {
    v -= ONE;
    return *this;
  }

  constexpr fixed operator--(int) {
    fixed r{*this};
    --*this;
    return r;
  }
};

// Undefined if |d| is beyond fixed<M, N>’s range.
template <unsigned M, unsigned N>
constexpr fixed<M, N> FromDouble(double d) {
  fixed<M, N> f;
  // Bump magnitude by 0.5 in integer space to get std::round behaviour from
  // shift which really truncates i.e rounds towards 0.
  f.v = static_cast<decltype(f)::underlying_type>(
    d * double(decltype(f)::ONE) + ((d >= 0) ? 0.5 : -0.5));
  return f;
}

// We use double to print our fixed-point type values.  In some sense
// this is cheating as fixed-point types are required in the first place for
// machines with no float/double support.  We take this shortcut on purpose as
// we don’t want to work on floating point print routines.
template <unsigned M, unsigned N>
std::ostream& operator<<(std::ostream& os, fixed<M, N> f) {
  double d = f.v;
  const auto default_precision {os.precision()};
  return os << std::fixed
            << std::setprecision(N)
            << (d / decltype(f)::ONE)
            << std::setprecision(default_precision)
            << std::defaultfloat;
}

int main() {
  fixed<4, 4> l { 0b1000'0000 };
  fixed<4, 4> u { 0b0111'1111 };
  std::cout << l << '\t' << +l.trunc() << '\t' << l.round() << '\n';
  std::cout << u << '\t' << +u.trunc() << '\t' << u.round() << '\n';

  auto x = FromDouble<4, 4>(-2.76);
  std::cout << x << '\t' << +x.trunc() << '\t' << x.round() << '\n';

  auto y = FromDouble<4, 4>(1.93);
  std::cout << y << '\t' << +y.trunc() << '\t' << y.round() << '\n';
  auto z = x * y;
  std::cout << z << '\n';

  auto f1 = FromDouble<24, 8>(34546.4023);
  std::cout << f1 << '\n';
  auto f2 = FromDouble<24, 8>(-93.7296);
  std::cout << f2 << '\n';
  std::cout << f1 * f2 << '\n';

  // TODO: Write tests.
}
