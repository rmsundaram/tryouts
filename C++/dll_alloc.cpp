#include <memory>
#include <iostream>

/*
 * Memory alloc/dealloc across module boundary (DLL, so, dylib)
 *
 * Different modules might be built with different toolchains, or the same
 * toolchian but different settings.  Therefore the C runtime used may be
 * different for different modules; so calling the right deleter for an object
 * allocated in a different module is essential.  Run-time errors -- heap
 * corruptions, crashes, leaks, … -- might occur if the deleters don’t match.
 * These may be is due to deleting from the wrong heap or the deleter expecting
 * the allocation to have some structure/data which it mightn’t confirm to/have
 * e.g. it’s conventional to store allocation size before what’s handed out; see
 * align.cpp/alloc_aligned.  Problem details in [1] and [2].  Also passing STL
 * containers across boundaries is a huge no-no as C++ has no standard ABI [3].
 * Using STL within the DLL is still OK [7].
 *
 * OPTION 1
 * When giving out objects created in a shared object, also provide a deleter
 * function; the client code can call it later directly or thru’ RAII; see [4].
 *
 * OPTION 2
 * Effective C++, 3rd Edition, §4.18 recommends solving this situation by
 * returning a `std::shared_ptr`, as it’s guarenteed to call the right deleter.
 * This is an option if the DLL can expose C++ interfaces.  Another alternative
 * is `std::unique_ptr` with a custom default deleter to avoid the (template)
 * type having the `Deleter`; see [5].  Both makes interface inflexible though.
 * These smart pointers use type erasure and call the right deleter.  Also the
 * client code shouldn’t call release on it directly.
 *
 * OPTION 3
 * Another option is to let the caller pass in memory for said object and it’s
 * up to the caller to clean it up however it wishes to; see [6].  However, this
 * means placement new for C++ objects and placement delete(!) at caller’s end.
 * It has the advantage of allowing the client to use its own allocator though.
 *
 * REFERENCES
 * [1]: Allocating and freeing memory across modulue boundaries, Raymond Chen
 *      https://devblogs.microsoft.com/oldnewthing/20060915-04/?p=29723
 * [2]: https://docs.microsoft.com/en-us/cpp/c-runtime-library/
 *      potential-errors-passing-crt-objects-across-dll-boundaries
 * [3]: https://stackoverflow.com/q/22797418/183120
 * [4]: https://codereview.stackexchange.com/a/153569/29287
 * [5]: https://www.codeproject.com/Articles/594671/
 *      How-to-ensure-proper-dynamic-library-boundary-cros
 * [6]: https://stackoverflow.com/q/13625388/183120
 * [7]: https://stackoverflow.com/a/4032503/183120
 *
 * SEE ALSO
 * [8]: A Simple Plugin Architecture For C++
 *      http://www.andynicholas.com/?p=27
 * [9]: Making a Plugin System
 *      http://www.cplusplus.com/articles/48TbqMoL
 * [10]: Implementing a plug-in system in C or C++
 *       https://stackoverflow.com/q/708527/183120
 * [11]: Exporting C++ classes from a DLL
 *       https://eli.thegreenplace.net/2011/09/16/exporting-c-classes-from-a-dll
 * [12]: Boost’s DLL module tutorial demos a plugin architecture with code
 *       https://www.boost.org/doc/libs/1_73_0/doc/html/boost_dll/tutorial.html
 * dll_iface.cpp: reason why interface’s implementation code isn’t needed by the
 * linker when building client code loading DLL at run-time, not launch time.
 */

class A
{
public:
  ~A() {
    std::cout << __PRETTY_FUNCTION__ << '\n';
  }
};

extern "C" {
  A* get_obj() {
    return new A;
  }

  void del_obj(A* obj) {
    delete obj;
  }
}  // extern "C"

int main() {
  // unique_ptr has the deleter as part of its type, unlike shared_ptr
  std::unique_ptr<A, decltype(&del_obj)> up{get_obj(), &del_obj};
  std::shared_ptr<A> sp{get_obj(), del_obj};
}
