// g++ -std=c++11 -O2 -fverbose-asm -Wa,-adhls=fact.s tail_call_fact.cpp -o fact.exe
// GCC 4.8.1 does Tail Call Optimization for this from -O2 onwards

#include <iostream>

unsigned fact(unsigned i, unsigned total = 1u)
{
    if(i == 0)
        return total;
    else
        return fact(i - 1, total * i);
}

// after the optimization the above code (conceptually) becomes
// http://stackoverflow.com/a/36985/183120
/*
    unsigned fact(unsigned i, unsigned total = 1u)
    {
        while(i)
        {
            total *= i;
            --i;
        }
        return total;
    }
*/

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        std::cout << "usage: fact <non-negative integer>\n";
        return -1;
    }

    const int base = 0;
    char *end = nullptr;
    unsigned n = std::strtoul(argv[1], &end, base);

    if (((n != 0) || (*end == '\0')) && (n != ULONG_MAX))
        std::cout << fact(n) << '\n';
    else
        std::cout << "Failed to convert value.\n";
}
