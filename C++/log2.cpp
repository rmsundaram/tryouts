#include <iostream>
#include <cmath>
#include <limits>

#ifdef __has_include
#    if __has_include(<bit>)
#        include <bit>
#        include <numbers>
#    endif
#elif _MSC_VER
#    include <intrin.h>
#endif

// Binary logarithm, log₂ or lb, is useful to find the count of binary digits
// required to hold data (from integers to data structures like quadtree), steps
// in binary search algorithm, … lb(x) is the zero-based index of the highest
// bit set in x’s binary representation; in this sense it’s the complement of
// find first set (ffs): index of lowest bit set.  [1] discusses using lb to
// find quotient of POT division.

// Returns log2 of integers rounded down towards zero; add one to result for bit
// width required to hold a number; refer [1].  Alternatively, for bit width use
// C++20’s bit_width() or (numeric_limits<T>::digits - countl_zero()).
//
// [1]: //Misc/bit_wizardry.md.html
inline unsigned long log2_intrinsic(unsigned long x)
{
    // C++20 or later, use std::bit_width()-1 to avoid compiler-specific code;
    // std::countr_zero() can be used if |x| is guaranteed to be power of 2.
    // Use C++20’s std::has_single_bit to check if an int is POT.
#if __cpp_lib_int_pow2
    return static_cast<unsigned long>(std::bit_width(x) - 1);
#else
    // log 0 is undefined for all bases, since there's no number the base can
    // be raised-to to get 0
    if (0 == x) return 0;
#ifdef __GNUC__
    // Pass unsigned to numeric_limits<T>::digits as clz also takes unsigned.
    // Digits of signed is one less than that of unsigned but perhaps confusing.
    constexpr auto max_i_index = numeric_limits<unsigned long>::digits - 1;
    return max_i_index - __builtin_clzl(x);
#elif defined(_MSC_VER)
    // First first set bit from MSB and return its index w.r.t LSB.
    unsigned long value;
    _BitScanReverse(&value, x);
    return value;
#endif  // __GNUC__
#endif  // __cpp_lib_int_pow2
}

int main(int argc, char *argv[])
{
    // can't use auto as bit manipulation functions like clz take unsigned int
    const unsigned int GL_MAX_TEXTURE_SIZE = 0x0D33;    // 3379

    // use bit manipulation/compiler intrinsics for log2’s integer approximation
    std::cout << "log2_intrinsic(GL_MAX_TEXTURE_SIZE) = "
              << log2_intrinsic(GL_MAX_TEXTURE_SIZE) << '\n';
    /*
        How did we arrive at the above log2_intrinsic function?

        log2(n) is the zero-based index of highest set bit. The above input
        0x0D33 = 0000 0000 0000 0000 0000 1101 0011 0011 has 20 leading zeros,
        position of first set bit from MSB is 11. x86 ISA’a bsr instruction
        gives the index of the leading 1; MSVC exposes this as _BitScanReverse
        and GCC exposes it slightly differently as __builtin_clz.

        There're other related functions to count trailing zeros (ctz), leading
        zeros (clz) or find first set bit (ffs); ffs = ctz + 1.  ctz can be used
        for log2 only if input is a POT.

        The difference between Bit Scan Forward and Reverse is that the former
        searches from LSB -> MSB, while the latter from MSB -> LSB, but both
        return the zero-based index (from LSB) of the first bit encountered.
    */

#ifdef __GNUC__
    std::cout << "clz(0000 0000 0000 0000 0000 1101 0011 0011) = "
              << __builtin_clz(GL_MAX_TEXTURE_SIZE) << '\n';
    constexpr auto max_ll_index =
        std::numeric_limits<unsigned long long>::digits - 1; // = 63
    std::cout << "log2 (0x0D33) using MSB index - clz = "
              << max_ll_index - __builtin_clzll(GL_MAX_TEXTURE_SIZE) << '\n';


    // There are two common variants of find first set: the POSIX definition
    // which starts indexing of bits at 1, and the variant which starts indexing
    // of bits at zero, which is equivalent to ctz; although the index of the
    // first set bit is truly ctz and ffs is just ctz + 1
    std::cout << "ctz(0000 0000 0000 0000 0000 1101 0011 0011) = "
              << __builtin_ctz(GL_MAX_TEXTURE_SIZE) << '\n';
    std::cout << "ffs(0000 0000 0000 0000 0000 1101 0011 0011) = "
              << __builtin_ffs(GL_MAX_TEXTURE_SIZE) << '\n';

// Comment as macro isn’t defined by g++
//#if __STDC_VERSION__ >= 199901L
// C99 introduced log2f (math.h); variants: log2{,l} taking {,long} double
    std::cout << "std::log2(0x0D33) = "
              << std::log2f(static_cast<float>(GL_MAX_TEXTURE_SIZE)) << '\n';
//#endif
#endif  // __GNUC__

    // Another way using ln and change of base rule.
#if __has_include(<numbers>)
    constexpr auto LOG2E = std::log2(std::numbers::e);
#else
    constexpr auto LOG2E = 1.44269504088896340736;
#endif  // __has_include(<numbers>)
    std::cout << "log₂e * logₑ (0x0D33) = "
              << logf(static_cast<float>(GL_MAX_TEXTURE_SIZE)) * LOG2E << '\n';
}
