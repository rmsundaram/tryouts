// find the next larger integer from the given number's digits
// similar to next_permutation but specialized to ints
#include <iostream>
#include <cstring>

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cout << "Usage: nexti INTEGER\n";
        return 0;
    }
    char *str = argv[1];
    int const last = strlen(str) - 1;
    auto i = last;
    // find the pivot, the number smaller than the previous, going from LSD towards MSD
    // e.g. for 132959642 we'd stop at 9 i.e. just before 5, the pivot
    while ((i > 0) && (str[i - 1] >= str[i])) --i;
    if (i == 0) {
        std::cout << "No larger integer may be formed with given digits.\n";
        return 0;
    }
    // substring to be actually fixed; everything before pivot is in tact
    // in our example, str would be pointing to 5 now
    str += i - 1;
    // this substring is verified to be in descending order, except the MSD
    // reverse it excluding the MSD
    for (auto c = 0; c < ((last - i + 1) / 2); ++c)
        std::swap(argv[1][i + c], argv[1][last - c]);
    // find the digit next larger than the pivot
    // in our example, we'd now have 132952469, find 6 and swap it with 5
    i = 1;
    while (str[i] <= *str) ++i;
    // swap that with the pivot
    std::swap(*str, str[i]);
    std::cout << argv[1];
}
