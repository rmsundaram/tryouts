// http://barendgehrels.blogspot.com/2010/10/tag-dispatching-by-type-tag-dispatching.html
// the author mentions that reversing tags is easier with tag dispatch by type than by tag
// dispatch by instance; so it is tried out here. Boost.Geometry is powered by tag dispatching
// by type and it has all the required helper functions.

#include <iostream>
#include <string>

struct apple
{
	std::string name = "apple";
};

struct banana
{
	std::string name = "banana";
};

struct apple_tag { };
struct banana_tag { };

template <typename T> struct tag { };
template <> struct tag <apple> { using value = apple_tag; };
template <> struct tag <banana> { using value = banana_tag; };

#ifndef BY_TYPE
void eat(const apple & a, apple_tag)
{
	std::cout << "bite " << a.name << std::endl;
}
void eat(const banana & b, banana_tag)
{
	std::cout << "peel " << b.name << std::endl;
}
template <typename T>
void eat(const T &fruit)
{
	eat(fruit, typename tag<T>::value());
}


void mix(const apple &a, const banana &b, apple_tag, banana_tag)
{
	std::cout << "mix " << a.name << " with " << b.name << std::endl;
}
void mix(const banana &b, const apple &a, banana_tag bt, apple_tag at)
{
	mix(a, b, at, bt);
}
template <typename T, typename U>
void mix(const T &fruit1, const U &fruit2)
{
	mix(fruit1, fruit2, typename tag<T>::value(), typename tag<U>::value());
}

#else

namespace fruit_impl
{
	template <typename T> struct eat { };
	template <> struct eat<apple_tag> {
		static void munch(const apple &a) {
			std::cout << "bite " << a.name << std::endl;
		}
	};
	template <> struct eat<banana_tag> {
		static void munch(const banana &b) {
			std::cout << "peel " << b.name << std::endl;
		}
	};

	
	template <typename T, typename U> struct mix { };
	template <> struct mix <apple_tag, banana_tag> {
		static void match(const apple &a, const banana &b) {
			std::cout << "mix " << a.name << " with " << b.name << std::endl;
		}
	};
	template <> struct mix <banana_tag, apple_tag> {
		static void match(const banana &b, const apple &a) {
			// this can reuse the previous definition
			mix<apple_tag, banana_tag>::match(a, b);
		}
	};
}

template <typename T>
void eat(const T &fruit)
{
	fruit_impl::eat<typename tag<T>::value>::munch(fruit);
}

template <typename T, typename U>
void mix(const T &fruit1, const U &fruit2)
{
	fruit_impl::mix<typename tag<T>::value, typename tag<U>::value>::match(fruit1, fruit2);
}
#endif

int main()
{
	apple a;
	banana b;
	
	eat(a);
	eat(b);
	
	mix(a, b);
	mix(b, a);
}
