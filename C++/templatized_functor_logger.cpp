#include <iostream>
#include <type_traits>

// this logger works for both member functions and non-member functions
// written for http://stackoverflow.com/q/17218712/183120

template <typename T>
struct Logger
{
	Logger(T func) : func(func)
	{
	}

	template <typename... Args>
	auto operator()(const Args&... params) -> decltype(std::declval<T>()(params...))
	{
		return func(params...);
	}

	T func;
};

void simple_logger(const char *mesg)
{
	std::cout << "Simple: " << mesg << std::endl;
}

struct ComplexLogger
{
	std::ostream& operator()(const char *mesg)
	{
		return std::cout << "Complex: " << mesg;
	}
};

int main()
{
	Logger<decltype(&simple_logger)> l1(simple_logger);
	l1("hello!");
	ComplexLogger cl; 
	Logger<ComplexLogger> l2(cl);
	l2("hello!") << "yello!";
}
