// http://cplusplus.co.il/2010/01/03/tag-dispatching/

#include <iostream>
#include <type_traits>

// dummy tags
struct hi_speed_tag {};
struct low_speed_tag {};

// traits
template <typename T, bool = std::is_integral<T>::value>
struct traits { // default
    typedef low_speed_tag speed;
};

template <typename T>
struct traits <T, true> { // hi-speed tag for integral types
	using speed = hi_speed_tag;	// C++11 style, effect is the same as above though
};

template <>
struct traits<long long> { 	// long long (although an integral type) is fully specialized and tagged slow
    using speed = low_speed_tag;
};

// work() and its dispatches
template <typename T>
void work_dispatch (const T &val, const low_speed_tag&) {
    std::cout << "slow" << std::endl;
}

template <typename T>
void work_dispatch (const T &val, const hi_speed_tag&) {
    std::cout << "fast" << std::endl;
}

template <typename T>
void work (const T &val) {
    work_dispatch(val, typename traits<T>::speed());
}

// driver program
int main () {
    float x;
	int y;    
	long long z;

	work(x); // slow
    work(y); // fast
    work(z); // slow

    return 0;
}
