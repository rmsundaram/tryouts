#include <iostream>
#include <vector>
#include <string>
#include <memory>

// Type erasure allows you to rebind existing concrete classes to a
// new interface.

// https://davekilian.com/cpp-type-erasure.html
// https://en.wikibooks.org/wiki/More_C%2B%2B_Idioms/Type_Erasure#Example_Implementation_from_Sean_Parent_talk

struct AnimalConcept {
  virtual ~AnimalConcept() { }
  virtual std::string species() const = 0;
};

template <typename T>
struct AnimalModel final : AnimalConcept {
  T m;

  template<typename... TArgs>
  AnimalModel(TArgs&&... args) : m(std::forward<TArgs>(args)...) {}

  std::string species() const override { return m.name(); }
};

struct Dog {
  Dog(const char* s) : species_name{s} {}

  std::string name() const {
    return species_name;
  }
  std::string species_name;
};

struct Cat {
  Cat(const char* s) : species_name{s} {}

  std::string name() const {
    return species_name;
  }
  std::string species_name;
};

void print_species(AnimalConcept* a) {
  std::cout << a->species() << '\n';
}

int main() {
  std::vector<std::unique_ptr<AnimalConcept>> animals;
  animals.emplace_back(std::make_unique<AnimalModel<Dog>>("Canis familiaris"));
  animals.emplace_back(std::make_unique<AnimalModel<Cat>>("Felis catus"));
  for (const auto& a : animals)
    print_species(&*a);
}
