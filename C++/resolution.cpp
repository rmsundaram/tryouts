#include <iostream>
#include <iomanip>
#include <limits>

/*
 * In general, the resolution of a datatype means how granular can one get with
 * a given type i.e. the smallest non-zero magnitude representable[1]. On a
 * fixed-point system that's unsigned and m + n bits wide, the resolution is
 * 2^−n and the range would be [0, 2^m − 2^−n]. Say for Q4.4, the resolution
 * would be 1/2^4 = 0.0625 and smallest number representable would be 0000.0000
 * and the largest number would be 1111.1111; in decimal, it'd be value sans
 * radix point / 2^n = 255 / 16 = 15.9375. On a signed, two's complement
 * fixed-point system, that's m + n + 1 bits wide, the resolution would again be
 * 2^−n and the range would be [−2^m, 2^m − 2^−n]. Say in UQ3.4,
 * 10000010.10000000₂ = −2⁷ + 2¹ + 2^−1 = −125.5₁₀. The resolution of this
 * system would be 1/2^4 = 0.0625 and the range would be [1000.0000, 0111.1111]₂
 * = [−8, 127 / 2^4 = 7.9375]₁₀. The resolution of fixed-point numbers, unlike
 * floating-point numbers, remain constant over the entire range[2]. Precision
 * of a type is the maximum number of non-zero bits representable[1] e.g. the
 * single precision floating point number specified by IEEE 754 has 24 bits of
 * precision (includding the hidden bit) for the mantissa/significand. Accuracy
 * is the magnitude of the maximum difference between a real value and its
 * representation[1] and is given by ½Resolution E.g. with a resolution of ¼,
 * the numbers between 1 and 1¼ cannot be represented exactly but with an
 * accuracy of ⅛ i.e. the stop closer to such a number would be chosen to
 * represent it; the one in the centre of that interval would've maximum error
 * and it'd be half of ¼ away from stops on either sides, hence ⅛.
 * [1]: http://dominiquethiebaut.com/dftwiki3/index.php/CSC231_An_Introduction_to_Fixed-_and_Floating-Point_Numbers
 * [2]: http://en.wikipedia.org/wiki/Q_%28number_format%29
 *
 * What's the resolution with which the interval [2, 4] be stored in 4 slots?
 * Interval span = 4 - 2 = 2. Although the number of integer stops are 3, the
 * span is 2.
 *
 * Resolution = Span / Slots
 *
 * Although it may seem that all 4 slots are available, calculating the
 * resolution with that notion gives an incorrect result: r = 2 / 4 = 0.5.
 * { 2, 2.5, 3, 3.5 } notice that there's no slot to put the last stop (4) and
 * hence this is wrong. We've to allot one state to one of the boundary stops
 * (either 2 or 4) which doesn't participate in the span. Hence the number of
 * available slots is actually 3.
 * r = 2 / 3 = 0.66666667; slots = { 2, 2.66667, 3.3333, 4 }
 *
 * Note that similar to IEEE 754 floating-point numbers although our little
 * system has a greater resolution i.e. we can store numbers finer than
 * integers, but not all integers between can be stored exactly e.g. 2, 3. This
 * is not due to the non-availability of slots, we might as well have chosen an
 * encoding like this: { 2, 3, 4, ∅ }. This is due to the epsilon/resolution we
 * chose whose multiples skip some integers.
 *
 * Storing angles with high resolution using integers
 * http://stackoverflow.com/q/7934623
 * Barring the one bit for sign, we've 31 bits available to store [0, 180]. The
 * total states we've is 2³¹, however we've 2³¹ − 1 available states. Thus the
 * resolution would be 180 / 2³¹ − 1 = 8.38190317544e-8 degree i.e. a degree as
 * small as this would be representable in this system. The reciprocal of this
 * would give the number of states required in this system to represent 1 unit
 * in the original system: 11930464.7056. This can be verified:
 * 11930464 * 8.38190317544e-8 =  0.999999940861
 * n items, m slots, slots/item = m/n, items/slot = n/m
 */

int encode(double angle) {
    // multiply one unit (smallest angle representable) in this system with the
    // actual value to get the value in this system. Except MSB all other bits
    // are set: 2³¹ − 1
    return angle * (0x7FFFFFFF / 180.0);
}

// inverse of encode
double decode(int angle) {
    return (angle * 180.0) / 0x7FFFFFFF;
}

int main() {
    double const f = 90.0;
    auto const i = encode(f);
    std::cout << "90° = " << i << " in encoded system\n";
    // set to maximum precision
    std::cout << std::setprecision(std::numeric_limits<decltype(f)>::max_digits10)
              << "Decoding yeilds " << decode(i) << "°\n";
    auto constexpr fw = 16;
    std::cout << std::left << std::setw(fw) << "ENCODED" << "DEGREES\n";
    std::cout << std::left << std::setw(fw) << "0" << decode(0) << '\n';
    std::cout << std::left << std::setw(fw) << "11930464 - 1" << decode(11930464 - 1) << '\n';
    std::cout << std::left << std::setw(fw) << "11930464" << decode(11930464) << '\n';
    std::cout << std::left << std::setw(fw) << "11930464 + 1" << decode(11930464 + 1) << '\n';
    std::cout << std::left << std::setw(fw) << "2147483647" << decode(0x7FFFFFFF) << '\n';
}
