#include <GL/glfw.h>
#include <utility>
#include <stdexcept>

template <typename T>
struct return_type;

template <typename ReturnType, typename... Args>
struct return_type<ReturnType (*) (Args...)>
{
	typedef ReturnType type;
};

template <typename UninitFuncType>
class RAIIWrapper
{
public:
  // enable_if makes sure that check or ignoreReturn is used and this is not used as-is by function pointers
  template <typename InitFuncType, typename... Args, class = typename std::enable_if<!std::is_pointer<InitFuncType>::value>::type>
  RAIIWrapper(InitFuncType fpInitFunc,
              UninitFuncType fpUninitFunc,
              Args&&... args)
    : fpUninit(std::move(fpUninitFunc))
  {
    fpInitFunc(std::forward<Args>(args)...);
  }

  bool isInitialized() const { return true; } // false is impossible in your implementation

  ~RAIIWrapper() { fpUninit(); } // won't be called if constructor throws

private:
  UninitFuncType fpUninit; // avoid overhead of std::function not needed
};

template <typename InitFuncType, typename UninitFuncType, typename... Args>
RAIIWrapper<UninitFuncType>
raiiWrapper(InitFuncType fpInitFunc,
            UninitFuncType fpUninitFunc,
            Args&&... args)
{
  return RAIIWrapper<typename std::decay<UninitFuncType>::type>
    (std::move(fpInitFunc), std::move(fpUninitFunc), std::forward<Args>(args)...);
}

template <typename InitFuncType, typename SuccessValueType>
struct ReturnChecker {
  InitFuncType func;
  SuccessValueType success;
  const char *errorString;
  ReturnChecker(InitFuncType func,
                SuccessValueType success,
                const char *errorString)
    : func(std::move(func)), success(std::move(success)), errorString(errorString) {}

  template <typename... Args>
  void operator()(Args&&... args)
  {
    if (func(args...) != success)
      throw std::runtime_error(errorString);
  }
};

template <typename InitFuncType, typename SuccessValueType,
	  typename Ret = ReturnChecker<InitFuncType, SuccessValueType> >
Ret checkReturn(InitFuncType func, SuccessValueType success, const char *errorString)
{
  return Ret(func, success, errorString);
}

template <typename InitFuncType>
struct ReturnIgnorer {
  InitFuncType func;
  ReturnIgnorer(InitFuncType func)
    : func(std::move(func))
  {
    static_assert(std::is_void<typename return_type<InitFuncType>::type>::value, "ignored return value");
  }

  template <typename... Args>
  void operator()(Args&&... args)
  {
    func(args...);
  }
};

template <typename InitFuncType >
ReturnIgnorer<InitFuncType> ignoreReturn(InitFuncType func)
{
  return ReturnIgnorer<InitFuncType>(func);
}

void t_void_init(int){}
int t_int_init(){ return 1; }
void t_uninit(){}

int main()
{
	/*
		auto glfwSys = raiiWrapper(ignoreReturn(glfwInit), glfwTerminate);
		will fail due to the static_assert in ignoreReturn
	*/
	auto glfwSys = raiiWrapper(checkReturn(glfwInit, GL_TRUE, "Failed to initialize GLFW"),
                     glfwTerminate);
	auto glfwWin = raiiWrapper(checkReturn(glfwOpenWindow, GL_TRUE, "Failed to open GLFW window"),
				   glfwCloseWindow,
				   800, 600, 0, 0, 0, 0, 0, 0, GLFW_WINDOW);
	auto abc = raiiWrapper(checkReturn(t_int_init, 1, "failed!"), t_uninit);
	auto abcvoid = raiiWrapper(ignoreReturn(t_void_init), t_uninit, 7);

	// however, this is allowed and it defeats the purpose of both checkReturn and ignoreReturn
	//auto abc1 = raiiWrapper(t_int_init, t_uninit);
}

