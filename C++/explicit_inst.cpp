// explicit instantiation of templates
// http://stackoverflow.com/q/2351148/183120

// contents of header
template <typename T>
struct myclass
{
    void func();
};
// end contents

// contents of source
#include "interface.hpp"

template <typename T>
void myclass<T>::func()
{
}

// explicitly instantiate myclass<float>
// only it gets defined in the object file
template class myclass<float>;
// this is one of the few places where typename cannot be used instead of class
// http://stackoverflow.com/a/2024173/183120
//end contents

// contents of client
#include "interface.hpp"
int main()
{
	myclass<float> f;
	f.func();

	myclass<int> i;
	i.func();						// undefined reference to myclass<int>::func()
}
// end contents
