#include <iostream>
#include <memory>

int main()
{
    std::shared_ptr<int> sp0(new int(0));
    std::shared_ptr<int> sp1(new int(1)), sp2 = sp1;
    sp1 = std::move(sp0);

    if (sp0)
        std::cout << "*sp0 = " << *sp0 << " is_unique = " << std::boolalpha << sp0.unique() << std::endl;
    if (sp1)
        std::cout << "*sp1 = " << *sp1 << " is_unique = " << std::boolalpha << sp1.unique() << std::endl;
    if (sp2)
        std::cout << "*sp2 = " << *sp2 << " is_unique = " << std::boolalpha << sp2.unique() << std::endl;

    std::cout << "Came here!" << std::endl;
}
