#include <iostream>
#include <vector>
#include <deque>
#include <algorithm>
#include <iterator>
#include <stdexcept>
#include <sstream>

/*
Coons patch border detector for §8.7.4.5.7 Type 6 Shadings, PDF 1.7 Spec. 2008-7-1, 1st Edition
Finds the borders of patches that would contribute to the mesh's border.
Flag of a patch decides where it'll occur with respect to the previous patch.
A new, independent patch is created for flag 0.

    +---------+
    |    1    |
    |  patch  |
    |  above  |
    +---------+

    +---------+  +---------+
    |    0    |  |    2    |
    |   new   |  |  patch  |
    |  patch  |  |  right  |
    +---------+  +---------+

    +---------+
    |    3    |
    |  patch  |
    |  below  |
    +---------+

sample input:

    0 3 3 2 1

would mean

    +---+
    | 0 |
    +---+---+
    | 3 | 1 |
    +---+---+
    | 3 | 2 |
    +---+---+

expected ouput:

Left    0  0  0
Bottom  3  3
Right   1  2  2
Top     0  1

*/

enum class DIRECTION : int
{
    NONE = 0,
    TOP = 1,
    RIGHT = 2,
    BOTTOM = 3
};

void output(std::deque<int> const &left,
            std::deque<int> const &bottom,
            std::deque<int> const &right,
            std::deque<int> const &top)
{
    std::ostream_iterator<int> ois(std::cout, "\t");
    std::cout << "Left\t";
    std::copy(left.cbegin(), left.cend(), ois);
    std::cout << "\nBottom\t";
    std::copy(bottom.cbegin(), bottom.cend(), ois);
    std::cout << "\nRight\t";
    std::copy(right.cbegin(), right.cend(), ois);
    std::cout << "\nTop\t";
    std::copy(top.cbegin(), top.cend(), ois);
}

void patch_right(int &x,
                 int &y,
                 std::deque<int> &top,
                 std::deque<int> &right,
                 std::deque<int> &bottom)
{
    ++x;
    right[y] = x + 1;
    top.push_back(y);
    bottom.push_back(y + 1);
}

void patch_bottom(int &x,
                  int &y,
                  std::deque<int> &left,
                  std::deque<int> &bottom,
                  std::deque<int> &right)
{
    ++y;
    bottom[x] = y + 1;
    if (static_cast<unsigned>(y) < right.size())
        right[y] = x + 1;
    else
    {
        right.push_back(x + 1);
        left.push_back(x);
    }
}

void patch_top(int &x,
               int &y,
               std::deque<int> &left,
               std::deque<int> &top,
               std::deque<int> &right,
               std::deque<int> &bottom)
{
    // beginning a new row, move everything down in top and bottom
    // insert new values for left and right at the front side
    if (0 == y)
    {
        left.push_front(x);
        right.push_front(x + 1);
        std::for_each(std::begin(top), std::end(top), [](int &i) { ++i; });
        std::for_each(std::begin(bottom), std::end(bottom), [](int &i) { ++i; });
        y = 0;
    }
    else
    {
        --y;
        right[y] = x + 1;
    }
    top[x] = y;
}


void patch(std::vector<unsigned long> const &patches)
{
    if (patches.front() != 0)
        throw std::invalid_argument("First patch should always be started independently");

    std::deque<int> left = { 0 }, bottom = { 1 }, right = { 1 }, top = { 0 };
    auto iter = patches.cbegin() + 1;
    int x = 0, y = 0;
    while (iter != patches.cend())
    {
        if (*iter > static_cast<unsigned>(DIRECTION::BOTTOM))
            throw std::invalid_argument("Unrecognized direction");
        DIRECTION const d = static_cast<DIRECTION>(*iter++);

        switch (d)
        {
            case DIRECTION::RIGHT:
                patch_right(x, y, top, right, bottom);
                break;
            case DIRECTION::BOTTOM:
                patch_bottom(x, y, left, bottom, right);
                break;
            case DIRECTION::TOP:
                patch_top(x, y, left, top, right, bottom);
                break;
            case DIRECTION::NONE:
                std::cerr << "Invalid direction in the midst?";
        }
    }
    output(left, bottom, right, top);
}

int main(int argc, char** argv)
{
    if (argc <= 1)
    {
        std::cerr << "usage: patches 0 [x ...]\n"
                     "where x is one of\n"
                     "0 - new patch\n"
                     "1 - patch above\n"
                     "2 - patch right\n"
                     "3 - patch below";
        return -1;
    }

    std::vector<unsigned long> patch_flags;
    patch_flags.reserve(argc - 1);
    std::transform(&argv[1], &argv[argc], std::back_inserter(patch_flags), [](char const *argi) -> unsigned long
    {
        char *sentinal = nullptr;
        auto const x = std::strtoul(argi, &sentinal, 0);
        // NOTE: De Morgan’s laws might be useful to simplify this expression
        // https://nerderati.com/2018/06/27/leverage-demorgans-laws-to-rewrite-boolean-expressions/
        // ¬(A ∧ B) ⇔ (¬A) ∨ (¬B)
        // ¬(A ∨ B) ⇔ (¬A) ∧ (¬B)
        //
        // For proper circuit minimization (algorithms like Quine–McCluskey) see
        // https://en.wikipedia.org/wiki/Logic_optimization
        if ((!x && (!sentinal || sentinal == argi)) || x == ULONG_MAX)
        {
            std::ostringstream ss("Error converting ", std::ios_base::ate);
            ss << argi << " to a number";
            throw std::invalid_argument(ss.str());
        }
        return x;
    });
    patch(patch_flags);
}
