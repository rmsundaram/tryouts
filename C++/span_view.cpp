#include <span>
#include <string_view>
#include <iostream>

// https://www.nextptr.com/question/qa1344313286/differences-between-stdstring_view-and-stdspan

size_t count(std::span<const std::string_view> s) {
  size_t c = 0;
  for (auto sv : s) {
    c += sv.size();
  }
  return c;
}

void f(std::span<int, 3> s) {
  s[0] = -1;
}

int main() {
  int a[] = {1, 2, 3, 4, 5};
  std::span<int> s(a+1, 3);
  std::cout << "Span size: " << s.size() << "\nSpan: ";
  // Print the span
  for (auto i : s) {
    std::cout << i << ',';
  }
  std::cout << "\nBacking store: ";
  // spans are writable, unless it’s span<const T>
  s[0] = 0;
  // Print span’s backstore (underlying array)
  for (auto i : a) {
    std::cout << i << ',';
  }
  std::cout << '\n';

  // string views are strictly views; read-only
  char const *str = "Hello, world!";
  std::string_view sv{str+7, 5};
  std::cout << "String view: " << sv << '\n';
  // sv[2] = 'x';  error: assignment of read-only location

  // Can’t we use std::span<const char>?  We can but we don’t get all the extra
  // methods that string_view exposes e.g. operator< is exposed by string_view
  // which makes it hashable i.e. set<string_view> works directly.  It also
  // exposes other string-like methods substr, find, compare, etc.
  std::cout << count(std::array<std::string_view, 2>{ std::string_view("abc"),
                                                      std::string_view("1234") })
            << '\n';

  int a[] = {0, 1, 2, 3, 4, 5, 6, 7};
  for (auto i = 0u; i < 8; ++i)
    std::cout << a[i] << ", ";
  std::cout << '\n';

  f(std::span<int, 3>{&a[3], 3});

  for (auto i = 0u; i < 8; ++i)
    std::cout << a[i] << ", ";
  std::cout << '\n';
}
