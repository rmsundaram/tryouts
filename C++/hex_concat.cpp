#include <iostream>

/* source: http://stackoverflow.com/a/17206948/183120

   The rank of a digit is its position in a value. For example, units has
   rank 0, tens - 1, hundreds - 2, etc. Checking the rank of a value means
   checking the rank of its most-significant position.
   Rank equals to the power to which you must raise the base to reach given
   position. The rank of 5 in 25 is 0, because 5 = 5 * 10^0. The rank of 2
   in 25 is 1, because 20 = 2 * 10^1. Also, the ranks are continuous - the
   rank of 3 in 0.3 is equal to -1. Notice, that I return ranks increased by
   one from my function for the purpose of this particular problem. But that
   doesn't change their definition.

   Refer Matters Computational (fxtbook.pdf) for details on ranking and unranking
*/

/* returns the number of digits in n based on the number system (base) passed
   it was (mis)named rank by the author in SO due to the aforementioned reason,
   but digits is more appropriate, since say the rank(1432) = rank(1 in 1432) =
   3, but it returns 4 = 3 + 1, which is nothing but the number of digits
*/
unsigned digits(int n, unsigned base)
{
	unsigned digits = 0;
	while (n > 0)
	{
		n /= base;
		++digits;
	};
	return digits;
}

int main()
{
	// this doesn't work for -ve numbers
	int left = 0x12, right = 0x34;
	// 4 is the number of bits required for holding a hex digit
	std::cout << std::hex << "0x" << ((left << (4 * digits(right, 16))) + right) << std::endl;
}
