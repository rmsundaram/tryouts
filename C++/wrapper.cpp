#include <windows.h>
#include <utility>

// an alternative is to use std::unique_ptr

// RAII wrapper class for Handles, DCs, etc. which needs closure calls for release of a resource
// usage:
// template args take the
//        i. resources's data type          (e.g. HANDLE)
//       ii. it's invalid check value       (e.g. INVALID_HANDLE_VALUE)
//      iii. the closure function's type    (e.g. BOOL (WINAPI *)(HANDLE))
//       iv. closure function               (e.g. CloseHandle)
//
// ctor takes the handle to wrap

template <typename Type, Type tInvalidHandle,
		  typename ClosureFuncType, ClosureFuncType fpClosure>
class CClosureWrap
{
public:
	explicit CClosureWrap(Type tResource = tInvalidHandle) : _tResource(tResource) { }

	operator Type() const { return _tResource; }

	// Reset, when called without any param, will just close the contained resource
	// when a new resource is passed, after the old one's closure, this will be wrapped
	// call this when you want to force the resources's closure, instead of waiting
	// for the compiler to call the dtor on an out of scope
	void Reset(Type tResource = tInvalidHandle)
	{
		if (_tResource != tResource)
			_Swap(CClosureWrap(tResource));
	}

	// Releases ownership of the contained resource without closing it.
	Type Release()
	{
		const Type tResource(_tResource);
		_tResource = tInvalidHandle;
		return tResource;
	}

	~CClosureWrap()
	{
		if (tInvalidHandle != _tResource)
		{
			fpClosure(_tResource);
		}
	}
private:
	void _Swap(CClosureWrap<Type, tInvalidHandle, ClosureFuncType, fpClosure> that)
	{
		std::swap(this->_tResource, that._tResource);
	}

	// non - copyable
	CClosureWrap(const CClosureWrap&);
	CClosureWrap& operator=(const CClosureWrap&);

	// raw resource
	Type _tResource;
};

// managing SDL objects using just unique_ptr
struct SDL_Deleter
{
  void operator()( SDL_Surface* ptr ) { if (ptr) SDL_FreeSurface( ptr );}
  void operator()( SDL_RWops* ptr )   { if (ptr) SDL_RWclose( ptr );}
};
using SurfacePtr = std::unique_ptr<SDL_Surface, SDL_Deleter>;
using RWopsPtr = std::unique_ptr<SDL_RWops, SDL_Deleter>;
// This makes the above class redundant; just need to write functors for objects
// of different kinds. Deleter template argument shouldn’t be a function pointer
// type but a DefaultConstructible one.  Former incurs an additional function
// pointer cost; a value that never changes and hinders inlining. Refer [3] and
// [4] for details. Prefer option 1 over 2 below.
//
//     OPTION 1: DefaultConstructible deleter (no second arg to constructor)
//     struct C_Deleter { void operator()(void* ptr) { free(ptr); } };
//     std::unique_ptr<void, C_Deleter> p{malloc(4)};
//
//     OPTION 2: Non-DefaultConstructible (second arg, deleter, to constructor)
//     std::unique_ptr<void, void(*)(void*)> p{malloc(4), &free};
//     std::unique_ptr<void, decltype(&free)> p{malloc(4), &free};
//
// [1]: http://stackoverflow.com/q/27331315
// [2]: http://stackoverflow.com/a/14842047
// [3]: https://stackoverflow.com/q/50956579#comment88911312_50956579
// [4]: https://coliru.stacked-crooked.com/a/821855cb2f533adf

// Refer [5] for Windows’ and CloseHandle(HANDLE):
// [5]: https://stackoverflow.com/a/14841564/183120

int main()
{
	// compiles find with cl but not with g++ 4.8.1
    HANDLE myFile = INVALID_HANDLE_VALUE;
    CClosureWrap<HANDLE, INVALID_HANDLE_VALUE, decltype(&CloseHandle), CloseHandle> myyWrap(myFile);

    CClosureWrap<void*, nullptr, void(*)(void*), free> myWrap(malloc(10));
}
