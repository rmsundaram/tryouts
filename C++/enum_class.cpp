#include <iostream>

// scoped and strongly typed enum
enum class Day : char;  // forward declaration

void process(Day d);

enum class Day : char
{
    Sunday = 0,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday
};

// ADVANTAGES
// conventional enums get implicitly converted to int, while enum class doesn't
// scope is restricted and doesn't pollute the surrounding scope like conventional enums
// underlying type can be specified for compatibility and enabling forward declarations

// When needs enum flags where one can OR multiple flags together and pass to another
// function which will check for a flag's persence using AND, this has to be done
// manually by defining the necessary operators for conversion:
// http://stackoverflow.com/q/18553843/183120

int main()
{
    // however, like conventional enums, enum classes too doesn't wrap around
    // i.e. forcing it to point to a value not in the enumerated set is possible; but
    // as long as the value is representable in the underlying type, it is valid and
    // doesn't lead to undefined behaviour: http://stackoverflow.com/q/18195312/183120
    // if the value is not representable in the underlying type, then the result is
    // unspecified and the evaluation of such an expression leads to undefined behaviour.
    Day d = static_cast<Day>(100);
    std::cout << (unsigned int)d << '\n';
}
