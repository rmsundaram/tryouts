#include <iostream>

// http://en.wikipedia.org/wiki/Arithmetic_shift
// "Arithmetic Shifting Considered Harmful", MIT AI Lab, Guy Steele Jr.

// http://stackoverflow.com/a/22734721

/*
 * DIFFERENCE BETWEEN LOGICAL AND ARITHMETIC SHIFTS
 *
 * Logical shifts always fills discarded bits with zeros while arithmetic shift
 * fills it with zeros only for left shift but for right shift it copies the
 * MSB thereby preserving the sign of the operand. In other words, logical
 * shift looks at the shifted operand as just a stream of bits and move them,
 * without bothering about the sign of the resulting value. Arithmetic shift
 * looks at it as a (signed) number and preserves the sign as shifts are
 * made. Thus their behaviours are the same for left shifting but for right
 * shifting only for positive numbers.
 */

int main()
{
  /*
   * Right shifting of signed, positive values have no difference to integer
   * division (/) of the number by 2ⁿ. Both are equivalent to mathematically
   * dividing (÷) the number by 2 and rounding the result towards 0. Also the
   * sign bit of positive values would be zero thereby leading logical and
   * arithmetic right shifting of positive numbers to have the same effect.
   */
  std::cout << (37 / 2) << '\n';
  std::cout << (37 >> 1) << "\n\n";

  /*
   * Signed, negative value right-shift is implementation-defined by C99 and
   * upto C++17; C++20 defines it as arithmetic.  Compilers implement it thus
   * i.e. copies MSB; however, the result ISN'T integer division by 2ⁿ.  Both
   * divide by 2ⁿ but integer-divide-by-2 rounds towards zero while right-shift
   * rounds towards -∞.  The Wikipedia states that this discrepancy has led to
   * bugs in more than one compiler.
   */

  signed char i = -37;  // = 0b1101'1011 (in two's complement)

  // (-37 / 2¹) = -37 ÷ 2 = -18.5 rounded towards 0 = -18
  std::cout << (i / 2) << '\n';
  // (-37 >> 1) = -19 (arithmetic right shifting without concerning ÷2ⁿ meaning)
  std::cout << (i >> 1) << "\n\n";
  // however, when viewed as -37 ÷ 2 = -18.5 is rounded towards -∞ = -19

  // (-37 / 2²) = -37 ÷ 4 = -9.25 rounded towards 0 = -9
  std::cout << (i / 4) << '\n';
  // (-37 >> 2) = -10 (arithmetic right shifting without concerning ÷2ⁿ meaning)
  std::cout << (i >> 2) << '\n';
  // however, when viewed as -37 ÷ 2² = -9.25 is rounded towards -∞ = -10

  /*
   * Another interesting fact is that left shifting of signed, negative values
   * is undefined by both C and upto C++17. Also for signed, non-negative values
   * left shifting is defined only if the result of multiplying the operand by
   * 2ⁿ can be accommodated by the result type (the integral promoted left
   * operand type and not the type of the variable which would hold the
   * result).
   *
   *      signed char r = 0x40;     // 0b0100'0000
   *      r <<= 2;
   *
   * r would first be promoted to a signed int; on a machine with a 32-bit
   * integer, this would become 0x00000040; left shifting twice would give
   * 0b100000000 = 0x100 which can be represented on a 32-bit. This value would
   * then be implicitly converted to a signed char; since this value is not
   * representable in the destination type, the conversion performed is
   * implementation defined by C99 §6.3.1.3/3 and C++11 §4.7/3.
   * http://stackoverflow.com/a/24180487/183120
   *
   * C++20
   *
   * Shift behaviour is unified irrespective of shifted value's sign!
   * Consequently, right shift is clearly defined as arithmetic :)
   * i.e. left-most bits are copied thereby maintaining sign.  Relevant excerpt:
   *
   *     The value of a << b is the unique value congruent to a * 2^b modulo 2ⁿ
   *     where n is the number of bits in the return type (that is, bitwise
   *     left shift is performed and the bits that get shifted out of the
   *     destination type are discarded).
   *
   *     The value of a >> b is a/(2^b), rounded towards negative infinity (in
   *     other words, right shift on signed `a` is arithmetic right shift).
   *
   * Both shifts are clearly defined with nothing implementation-defined; only
   * undefined is when the b is negative or ≥ a's bit width.
   *
   * https://en.cppreference.com/w/cpp/language/operator_arithmetic#Built-in_bitwise_shift_operators
   */
}
