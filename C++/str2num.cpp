#include <iostream>
#include <sstream>
#include <string>
#include <cmath>

// https://isocpp.org/wiki/faq/misc-technical-issues#convert-string-to-num

// http://stackoverflow.com/q/5796983
bool is_int(float f) {
    return std::floor(f) == f;
}

char const* method1(std::string const &input) {
    char *e = nullptr;
    char const *str = input.c_str();
    // strtof skips the leading whitespaces
    float const f = strtof(str, &e);
    // skip trailing whitespaces, but if (e == str) no conversion happened, so
    // if a string with leading whitespaces has failed conversion, without the
    // if (str != e) check this will skip leading whitespaces
    if (str != e)
        while (std::isspace(*e)) ++e;
    // no conversion was performed or was stopped halfway through due to
    // disallowed characters encountered: Not A Number
    if ((e == str) || (*e != '\0'))
        return "NAN";
    else if ((f == HUGE_VALF) || !std::isfinite(f)) {
        return "too large";
        if (errno == ERANGE) errno = 0;
    }
    else
        return (is_int(f) ? "integer" : "non-integer");
}

// Creation of a temporary stream object is an overkill compared to the C
// library function alternative[1]; also multi-token input may need manual
// analysis anyways[2] for which strto* functions are a start. Also it takes
// lesser code size and much better runtime performance[3].
// [1]: http://stackoverflow.com/a/2845275
// [2]: http://stackoverflow.com/q/784563/#comment8604723_784578
// [3]: http://stackoverflow.com/a/6154614
// [4]: http://stackoverflow.com/a/447307
// [5]: http://stackoverflow.com/a/332886
char const* method2(std::string const &input) {
    std::istringstream ss(input);
    float f = 0.f;
    ss >> f;
    // checking (ss.fail() || !ss.eof()) will do if the input is guaranteed to
    // have no trailing spaces, leading spaces are tackled by istringstream
    // since std::skipws is enabled by default. Here we check if no error flags
    // are set and that the eof is reached after extracting all trailing spaces.
    // The order is important: check the flags first, which would be the result
    // of the first read, then read ws and check the eof flag.
    if (ss && (ss >> std::ws).eof())
        return (is_int(f) ? "integer" : "non-integer");
    else
        return "NAN";
}

void run(std::string const &input) {
    std::cout << input  << " --> " << method1(input) << '\n';
    std::cout << input  << " --> " << method2(input) << '\n';
}

int main(int argc, char **argv) {
    if (argc > 1)
        run(argv[1]);
    // test cases
    run("  133  ");
    run("  1337");
    run("133.9e-1    ");
    run({});
}
