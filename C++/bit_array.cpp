// write a bit array class without using std function

#include <limits>
#include <stdexcept>

class BitArray
{
public:
    BitArray(int size) : m_bits(nullptr), m_count(size) {
        if (size < 0)
            throw std::invalid_argument("Size cannot be negative!");
        // If std were allowed, std::div may seem like better option to get
        // remainder and quotient, but with g++ even at -O0, multiplication,
        // division and modulo by a constexpr power of 2 is optimised with bit
        // wizardry; refer idiv_pow2.cpp workout for a custom implementation of
        // idiv when the divisor is unknown at compile-time.
        // http://stackoverflow.com/q/2580680
        auto n = (size / std::numeric_limits<unsigned>::digits);
        auto const q = sizeof(unsigned) - 1u;
        // one more bin if there're remainder bits to be accomodated
        n += (size & q) ? 1u : 0u;
        if (n)
            m_bits = new unsigned[n] {};
    }
    ~BitArray() {
        if (m_bits)
            delete[] m_bits;
    }
    bool GetBit(int index) const {
        if (index >= m_count)
            throw std::range_error("Index out of bounds!");
        // TODO: make this a function and use it from both Get and SetBit
        auto const i = index / std::numeric_limits<unsigned>::digits;
        // shift 1 by index % UNSIGNED_BIT_COUNT
        unsigned const mask = 1u << (index - i * std::numeric_limits<unsigned>::digits);
        return m_bits[i] & mask;
    }
    void SetBit(int index, bool value) {
        if (index >= m_count)
            throw std::range_error("Index out of bounds!");
        auto const i = index / std::numeric_limits<unsigned >::digits;
        unsigned const mask = 1u << (index - i * std::numeric_limits<unsigned>::digits);
        // TODO: make it branchless
        if (value)
            m_bits[i] |= mask;
        else
            m_bits[i] &= ~mask;
    }

private:
    unsigned *m_bits;
    int m_count;
};

// http://stackoverflow.com/q/137038/#comment23509844_137479
// http://stackoverflow.com/a/840363
// https://sourceware.org/binutils/docs/as/a.html#a
// disassembly to Intel syntax with inter-weaved source listing
// g++ -std=c++14 -g -c -masm=intel -fverbose-asm -Wa,-adhln=prgm.s prgm.cpp

#include <cstdlib>
#include <iostream>

int main(int argc, char** argv) {
    if (argc > 1) {
        const auto x = std::strtol(argv[1], nullptr, 0);
        // all three operations are optimised with bit wizardry by g++ -O0
        const auto m = x * 8;
        const auto q = x / 16;
        const auto r = x % 32;
        std::cout << x << " * 8 = " << m << '\n'
                  << x << " / 16 = " << q << '\n'
                  << x << " % 32 = " << r;
    }
}
