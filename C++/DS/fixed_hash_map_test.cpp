#define WARN_SUBOPTIMAL_LOAD 0
#include "fixed_hash_map.hpp"

#include <cassert>
#include <utility>

#ifdef _MSC_VER
// Disable warning on using -1u as shorthand for 0xFFFF’FFFF.
__pragma(warning(disable:4146))
#endif

struct DecimalHasher
{
  uint64_t operator()(uint32_t key) const {
    return key % 10;
  }
};

using SimpleMap = fixed_hash_map<uint32_t, uint32_t, 8, DecimalHasher>;
using status = SimpleMap::insertion_status;

bool is_ins_eq(std::pair<status, size_t> actual,
               status expected_status,
               size_t expected_idx) {
  return ((actual.first == expected_status) && (actual.second == expected_idx));
}

bool are_keys_eq(
  const std::array<SimpleMap::key_type, SimpleMap::size>& buckets,
  const std::array<SimpleMap::key_type, SimpleMap::size>& expected) {
  for (auto i = 0u; i < buckets.size(); ++i)
    if (buckets[i] != expected[i])
      return false;
  return true;
}

int main(int argc, char** argv) {
  SimpleMap m;
  constexpr auto kInsertedWithCollision =
          static_cast<status>(status::kSucceeded | status::kCollided);

  // x ∅                | ∅  ∅
  assert(is_ins_eq(m.insert(1, 1), status::kSucceeded, 1));
  assert(m.erase(1));
  std::array<SimpleMap::key_type, SimpleMap::size> expected;
  expected.fill(-1u);
  assert(are_keys_eq(m.m_keys, expected));

  // x 2  ∅             | ∅  2  ∅
  m.clear();
  assert(is_ins_eq(m.insert(1, 1), status::kSucceeded, 1));
  assert(is_ins_eq(m.insert(2, 1), status::kSucceeded, 2));
  assert(m.erase(1));
  expected[2] = 2;
  assert(are_keys_eq(m.m_keys, expected));

  // 1 1' 2  ∅          | 1  x  2  ∅
  // 1 ∅  2  ∅
  m.clear();
  assert(is_ins_eq(m.insert(1, 1), status::kSucceeded, 1));
  assert(is_ins_eq(m.insert(11, 2), kInsertedWithCollision, 2));
  assert(is_ins_eq(m.insert(3, 1), status::kSucceeded, 3));
  assert(m.erase(2));
  expected = {-1u, 1, -1u, 3, -1u, -1u, -1u, -1u};
  assert(are_keys_eq(m.m_keys, expected));

  // x 1' 2 ∅           | 1  ∅  2  ∅
  m.clear();
  assert(is_ins_eq(m.insert(1, 1), status::kSucceeded, 1));
  assert(is_ins_eq(m.insert(11, 2), kInsertedWithCollision, 2));
  assert(is_ins_eq(m.insert(3, 1), status::kSucceeded, 3));
  assert(m.erase(1));
  expected = {-1u, 11, -1u, 3, -1u, -1u, -1u, -1u};
  assert(are_keys_eq(m.m_keys, expected));

  // x 1' 1' ∅          | 1  1' ∅  ∅
  m.clear();
  assert(is_ins_eq(m.insert(1, 1), status::kSucceeded, 1));
  assert(is_ins_eq(m.insert(11, 2), kInsertedWithCollision, 2));
  assert(is_ins_eq(m.insert(111, 3), kInsertedWithCollision, 3));
  assert(m.erase(1));
  expected = {-1u, 11, 111, -1u, -1u, -1u, -1u, -1u};
  assert(are_keys_eq(m.m_keys, expected));

  // 1  1' 2' ∅         | 1  x  2  ∅  ∅
  // 1  2  ∅  ∅
  m.clear();
  assert(is_ins_eq(m.insert(3, 1),status::kSucceeded, 3));
  assert(is_ins_eq(m.insert(33, 2), kInsertedWithCollision, 4));
  assert(is_ins_eq(m.insert(4, 1), kInsertedWithCollision, 5));
  assert(m.erase(4));
  expected = {-1u, -1u, -1u, 3, 4, -1u, -1u, -1u};
  assert(are_keys_eq(m.m_keys, expected));

  // x 1' 2' 2' ∅       | 1  2  2' ∅
  m.clear();
  assert(is_ins_eq(m.insert(1, 1), status::kSucceeded, 1));
  assert(is_ins_eq(m.insert(11, 2), kInsertedWithCollision, 2));
  assert(is_ins_eq(m.insert(2, 1), kInsertedWithCollision, 3));
  assert(is_ins_eq(m.insert(22, 2), kInsertedWithCollision, 4));
  assert(m.erase(1));
  expected = {-1u, 11, 2, 22, -1u, -1u, -1u, -1u};
  assert(are_keys_eq(m.m_keys, expected));

  // x 1' 1' 2' ∅       | 1  1' 2' ∅  ∅
  m.clear();
  assert(is_ins_eq(m.insert(1, 1), status::kSucceeded, 1));
  assert(is_ins_eq(m.insert(11, 2), kInsertedWithCollision, 2));
  assert(is_ins_eq(m.insert(111, 3), kInsertedWithCollision, 3));
  assert(is_ins_eq(m.insert(2, 1), kInsertedWithCollision, 4));
  assert(m.erase(1));
  expected = {-1u, 11, 111, 2, -1u, -1u, -1u, -1u};
  assert(are_keys_eq(m.m_keys, expected));

  // x  2'  1' ∅        | 2  1' ∅  ∅
  m.clear();
  assert(is_ins_eq(m.insert(3, 1), status::kSucceeded, 3));
  assert(is_ins_eq(m.insert(33, 2), kInsertedWithCollision, 4));
  assert(is_ins_eq(m.insert(4, 1), kInsertedWithCollision, 5));
  assert(is_ins_eq(m.insert(333, 3), kInsertedWithCollision, 6));
  assert(m.erase(4));
  expected = {-1u, -1u, -1u, 3, 4, 333, -1u, -1u };
  assert(are_keys_eq(m.m_keys, expected));

  // x 1' 2  1' ∅       | 1  1' 2  ∅  ∅
  m.clear();
  assert(is_ins_eq(m.insert(3, 1), status::kSucceeded, 3));
  assert(is_ins_eq(m.insert(33, 2), kInsertedWithCollision, 4));
  assert(is_ins_eq(m.insert(5, 1), status::kSucceeded, 5));
  assert(is_ins_eq(m.insert(333, 3), kInsertedWithCollision, 6));
  assert(m.erase(3));
  expected = {-1u, -1u, -1u, 33, 333, 5, -1u, -1u };
  assert(are_keys_eq(m.m_keys, expected));

  // x 2  3  1' ∅       | 1  2  3  ∅  ∅
  m.clear();
  assert(is_ins_eq(m.insert(3, 1), status::kSucceeded, 3));
  assert(is_ins_eq(m.insert(4, 1), status::kSucceeded, 4));
  assert(is_ins_eq(m.insert(5, 1), status::kSucceeded, 5));
  assert(is_ins_eq(m.insert(33, 2), kInsertedWithCollision, 6));
  assert(m.erase(3));
  expected = {-1u, -1u, -1u, 33, 4, 5, -1u, -1u};
  assert(are_keys_eq(m.m_keys, expected));

  // x 1' 1' 3' 3' ∅    | 1  1' 3  3' ∅ ∅
  m.clear();
  assert(is_ins_eq(m.insert(1, 1), status::kSucceeded, 1));
  assert(is_ins_eq(m.insert(11, 2), kInsertedWithCollision, 2));
  assert(is_ins_eq(m.insert(111, 3), kInsertedWithCollision, 3));
  assert(is_ins_eq(m.insert(3, 1), kInsertedWithCollision, 4));
  assert(is_ins_eq(m.insert(33, 2), kInsertedWithCollision, 5));
  assert(m.erase(1));
  expected = {-1u, 11, 111, 3, 33, -1u, -1u, -1u};
  assert(are_keys_eq(m.m_keys, expected));

  // x 1' 1' 3' 4' ∅    | 1  1' 3  4  ∅ ∅
  m.clear();
  assert(is_ins_eq(m.insert(1, 1), status::kSucceeded, 1));
  assert(is_ins_eq(m.insert(11, 2), kInsertedWithCollision, 2));
  assert(is_ins_eq(m.insert(111, 3), kInsertedWithCollision, 3));
  assert(is_ins_eq(m.insert(3, 1), kInsertedWithCollision, 4));
  assert(is_ins_eq(m.insert(4, 1), kInsertedWithCollision, 5));
  assert(m.erase(1));
  expected = {-1u, 11, 111, 3, 4, -1u, -1u, -1u};
  assert(are_keys_eq(m.m_keys, expected));

  // x 1' 2  1' 2' ∅    | 1  1' 2  2' ∅  ∅
  m.clear();
  assert(is_ins_eq(m.insert(1, 1), status::kSucceeded, 1));
  assert(is_ins_eq(m.insert(11, 2), kInsertedWithCollision, 2));
  assert(is_ins_eq(m.insert(3, 1), status::kSucceeded, 3));
  assert(is_ins_eq(m.insert(111, 3), kInsertedWithCollision, 4));
  assert(is_ins_eq(m.insert(33, 2), kInsertedWithCollision, 5));
  assert(m.erase(1));
  expected = {-1u, 11, 111, 3, 33, -1u, -1u, -1u};
  assert(are_keys_eq(m.m_keys, expected));

  // x 1' 2  2' 1' ∅    | 1  1' 2  2' ∅ ∅
  m.clear();
  assert(is_ins_eq(m.insert(1, 1), status::kSucceeded, 1));
  assert(is_ins_eq(m.insert(11, 2), kInsertedWithCollision, 2));
  assert(is_ins_eq(m.insert(3, 1), status::kSucceeded, 3));
  assert(is_ins_eq(m.insert(33, 2), kInsertedWithCollision, 4));
  assert(is_ins_eq(m.insert(111, 3), kInsertedWithCollision, 5));
  assert(m.erase(1));
  expected = {-1u, 11, 111, 3, 33, -1u, -1u, -1u};
  assert(are_keys_eq(m.m_keys, expected));

  // circular form of
  // x 1' 2  2' 1' ∅    | 1  1' 2  2' ∅ ∅
  m.clear();
  assert(is_ins_eq(m.insert(7, 1), status::kSucceeded, 7));
  assert(is_ins_eq(m.insert(77, 2), kInsertedWithCollision, 0));
  assert(is_ins_eq(m.insert(9, 1), status::kSucceeded, 1));
  assert(is_ins_eq(m.insert(99, 2), kInsertedWithCollision, 2));
  assert(is_ins_eq(m.insert(777, 3), kInsertedWithCollision, 3));
  assert(m.erase(7));
  expected = {777, 9, 99, -1u, -1u, -1u, -1u, 77};
  assert(are_keys_eq(m.m_keys, expected));

  // 1 1' 2  2' 1' ∅    | 1 x  2  2' 1' ∅
  // 1 1' 2  2' ∅  ∅    | 1 x  2  2' ∅  ∅
  // 1 ∅  2  2' ∅  ∅
  m.clear();
  assert(is_ins_eq(m.insert(7, 1), status::kSucceeded, 7));
  assert(is_ins_eq(m.insert(77, 2), kInsertedWithCollision, 0));
  assert(is_ins_eq(m.insert(9, 1), status::kSucceeded, 1));
  assert(is_ins_eq(m.insert(99, 2), kInsertedWithCollision, 2));
  assert(is_ins_eq(m.insert(777, 3), kInsertedWithCollision, 3));
  assert(m.erase(0));
  assert(m.erase(7));
  expected = {-1u, 9, 99, -1u, -1u, -1u, -1u, 777};
  assert(are_keys_eq(m.m_keys, expected));
}
