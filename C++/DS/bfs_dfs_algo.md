# BFS Algorithm

1. Enqueue the root node.
2. Dequeue and visit node.
    * Mark node visited.
    * If the element sought is found in this node, quit the search and return a result.
    * Otherwise enqueue any adjacent nodes that are umarked.
3. Is queue empty?  Quit search and return "not found" as all nodes are visited.
4. Go to (2) if not empty.

# DFS

Using a stack instead of a queue would turn BFS into DFS.


```
            1
           /|\
          2 7 8
         /|   |\
        3 6   9 12
       /|     |\
      4 5    10 11
```

DFS: 1 2 3 4 5 6 7 8 9 10 11 12
BFS: 1 2 7 8 3 6 9 12 4 5 10 11
