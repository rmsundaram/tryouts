#include <iostream>
#include <stack>
#include <cstdlib>
#include <cassert>

void print(std::stack<int> s) {
    while (!s.empty()) {
        const auto i = s.top();
        s.pop();
        std::cout << i << '\n';
    }
}

bool isPlacable(int i, std::stack<int> s) {
    return s.empty() || (s.top() > i);
}

void moveDisk(std::stack<int> &s, std::stack<int> &t) {
    assert(!s.empty() && isPlacable(s.top(), t));
    t.push(s.top());
    s.pop();
}

void moveDisks(int n, std::stack<int> &a, std::stack<int> &b, std::stack<int> &c) {
    if (n <= 0) return;
    moveDisks(n - 1, a, c, b);
    moveDisk(a, c);
    moveDisks(n - 1, b, a, c);
}

int main(int argc, char **argv) {
    unsigned const n = (argc > 1) ? strtoul(argv[1], nullptr, 0) : 0u;
    std::stack<int> a, b, c;
    for (auto i = n; i > 0; --i)
        a.push(i);
    moveDisks(n, a, b, c);
    std::cout << "A\n";
    print(a);
    std::cout << "B\n";
    print(b);
    std::cout << "C\n";
    print(c);
}
