#pragma once

#if __cplusplus < 201703L
#  error "C++17 or later is required"
#endif

#include <cstddef>
#include <cstdint>
#include <array>
#include <optional>
#include <limits>

#ifdef __cpp_concepts
#  include <concepts>

template <typename Fn, typename Key>
concept DefaultConstructibleCallable = std::default_initializable<Fn> &&
requires(Fn fn, Key k)
{
  { fn(k) } -> std::convertible_to<uint64_t>;
};
#else
#  include <type_traits>
#endif  // __cpp_concepts

// Enable suboptimal load factor warning for non-release builds if unset.
#if !defined(WARN_SUBOPTIMAL_LOAD) && !defined(NDEBUG)
#  define WARN_SUBOPTIMAL_LOAD 1
#endif  // WARN_SUBOPTIMAL_LOAD

#if WARN_SUBOPTIMAL_LOAD
#  include <iostream>
#endif  // WARN_SUBOPTIMAL_LOAD

// Basic open-addressed, linear probed hash table.  Flat as items are not on
// separate lists but inline; fixed as bucket size is set at compile-time on
// array backing storage.  std::unordered_map does separate chaining [3] which
// isn’t always needed.  Salient points:
//
//   * Stores keys and values separately for better cache coherence
//   * Uses % POT as recommended by C++ Now talk [2]; a prime bucket size is
//     needed for a poor hash function with poor entropy in lower bits [1].
//   * No load factor calculations as it's fixed size.
//   * Not using Robin Hood hashing as we expect small bucket sizes where linear
//     search is better.  400000+ items is when Robin’s heroics show up [2].
//
// See [4] for some options on hash tables.
//
// [1]: https://www.reedbeta.com/blog/data-oriented-hash-table/
// [2]: https://medium.com/applied/gist-better-than-unordered-map-1ad07b0a81b7
//      Original talk: https://youtu.be/M2fKMP47slQ
// [3]: https://stackoverflow.com/q/31112852/183120
// [4]: https://legends2k.github.io/note/c++_primer/#fn:2
template <
  typename Key,
  typename Value,
  size_t BUCKETS,
#ifdef __cpp_concepts
  DefaultConstructibleCallable<Key> Hasher
#else
  typename Hasher,
  typename = std::enable_if_t<std::is_default_constructible_v<Hasher>>
#endif  // __cpp_concepts
  >
struct fixed_hash_map {
  fixed_hash_map()
#if WARN_SUBOPTIMAL_LOAD
    : m_fills{0}
#endif  // WARN_SUBOPTIMAL_LOAD
  {
    clear();
  }


  enum insertion_status : uint8_t
  {
    kFailed = 0,
    kSucceeded = 1,
    kCollided = 1 << 1,
  };


  // Clears key-value stores.
  void clear() {
    m_keys.fill(KEY_INVALID);
    m_values.fill(VALUE_INVALID);
#if WARN_SUBOPTIMAL_LOAD
    m_fills = 0;
#endif  // WARN_SUBOPTIMAL_LOAD
  }


  // Returns insertion status along with index of insertion.  Returns index if
  // |key| is already present, without any value updation.
  std::pair<insertion_status, size_t>
    insert(Key key, Value value) {
    uint64_t h = hash(key) % BUCKETS;
    const auto final_h = (h != 0) ? (h - 1) : (BUCKETS - 1);
    bool collided = false;
    // Handle collision by linear probing this open addressing hash table.
    while (m_keys[h] != KEY_INVALID) {
      if (m_keys[h] == key)
        return { insertion_status((collided ? kCollided : 0) | kFailed),  h };
      // only first iteration is collision; rest is just linear probing
      if (!collided)
        collided = true;
      // Searched all items; no vacancy in backing store.  Bail out!
      if (h == final_h)
        return { insertion_status(kFailed | kCollided), -1};
      h = (h + 1) % BUCKETS;
    }

    m_keys[h] = key;
    m_values[h] = value;

#if WARN_SUBOPTIMAL_LOAD
    const float load_factor = ++m_fills / static_cast<float>(BUCKETS);
    // “Using linear probing, hash table utilization should not be much higher
    // than 50% for good performance.” — Real-Time Collision Detection, §7.1.3
    if (load_factor > 0.5f)
      std::cout << "Warning: load factor > 0.5; consider increasing BUCKETS\n";
#endif  // WARN_SUBOPTIMAL_LOAD
    return { insertion_status((collided ? kCollided : 0) | kSucceeded),  h };
  }


  // Returns index of |key| inside table.  Lookup by m_values[search(key)].
  std::optional<size_t> search(Key key) const {
    uint64_t h = hash(key) % BUCKETS;
    const auto final_h = (h != 0) ? (h - 1) : (BUCKETS - 1);
    while (m_keys[h] != key) {
      if ((m_keys[h] == KEY_INVALID) || (final_h == h))
        return std::nullopt;
      h = (h + 1) % BUCKETS;
    }
    return h;
  }


  // Searches and removes element if present.
  // https://attractivechaos.wordpress.com/2019/12/28/deletion-from-hash-tables-without-tombstones/
  bool remove(Key key) {
    const auto idx = search(key);
    if (!idx.has_value())
      return false;
    return clear(*idx);
  }


  // Removes element at bucket index |h|.
  bool erase(size_t h) {
    if ((h >= BUCKETS) || m_keys[h] == KEY_INVALID)
      return false;
    // We may need to bring forward all of [x + 1, ∅); stop when an empty
    // bucket is hit.  Possible cases and solutions:
    // BEFORE             | AFTER
    // x ∅                | ∅  ∅
    // x 2  ∅             | ∅  2  ∅
    // x 1’ 2 ∅           | 1  ∅  2  ∅
    // x 1’ 1’ ∅          | 1  1’ ∅  ∅
    // x 1’ 1’ 2’ ∅       | 1  1’ 2’ ∅  ∅
    // x 1’ 1’ 3’ 3’ ∅    | 1  1’ 3  3’ ∅ ∅
    // x 1’ 1’ 3’ 4’ ∅    | 1  1’ 3  4  ∅ ∅
    // x 1’ 2  1’ ∅       | 1  1’ 2  ∅  ∅
    // x 1’ 2  1’ 2’ ∅    | 1  1’ 2  2’ ∅  ∅
    // x 1’ 2  2’ 1’ ∅    | 1  1’ 2  2’ ∅ ∅
    // x 2  3  1’ ∅       | 1  2  3  ∅  ∅

    m_keys[h] = KEY_INVALID;
    m_values[h] = VALUE_INVALID;
    // Given h = natural hash position, v = vacancy, i = current bucket, swap
    // if v is cyclically in [h, i) i.e. following configurations hold:
    // ..h v.i..  (OR)  i..h v  (OR)  v.i..h
    auto v = h;
    auto i = (h + 1) % BUCKETS;
    while (m_keys[i] != KEY_INVALID) {
      h = hash(m_keys[i]) % BUCKETS;
      if (((i > v) && (v >= h)) ||
        ((i < h) && (v >= h)) ||
        ((i < h) && (v < i))) {
        std::swap(m_keys[v], m_keys[i]);
        std::swap(m_values[v], m_values[i]);
        v = i;
      }
      i = (i + 1) % BUCKETS;
    }
    return true;
  }


  static_assert(BUCKETS && ((BUCKETS& (BUCKETS - 1)) == 0) &&
    (BUCKETS < std::numeric_limits<uint64_t>::max()),
    "Bucket size should be a non-zero power of two value.");


  std::array<Key, BUCKETS> m_keys;
  std::array<Value, BUCKETS> m_values;
#if WARN_SUBOPTIMAL_LOAD
  size_t  m_fills;
#endif

  using value_type = Value;
  using key_type = Key;
  static constexpr size_t size = BUCKETS;
  static constexpr Key KEY_INVALID = std::numeric_limits<Key>::max();
  static constexpr Value VALUE_INVALID = std::numeric_limits<Value>::max();

private:

  uint64_t hash(Key id) const {
    return Hasher{}(id);
  }
};
