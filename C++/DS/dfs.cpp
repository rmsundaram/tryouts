enum VertexState { White, Gray, Black };

void runDFS() {
    VertexState *state = new VertexState[vertexCount];
    for (int i = 0; i < vertexCount; i++)
        state[i] = White;
    DFS(state, 0);
    delete [] state;
}

void DFS(VertexState* state, int u) {
    state[u] = Gray;
    for (int v = 0; v < vertexCount; v++)
        if (isEdge(u, v) && state[v] == White)
            DFS(v, state);
    state[u] = Black;
}
