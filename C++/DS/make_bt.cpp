#include <iostream>
#include <vector>
#include <cassert>

// geeksforgeeks.org/construct-a-binary-tree-from-parent-array-representation

struct node {
  int val = -INT_MAX;
  node *left = nullptr, *right = nullptr;
};

std::vector<node> make_tree(int const *parent, size_t n, node **root) {
  std::vector<node> t(n);
  for (auto i = 0u; i < n; ++i) {
    t[i].val = i;
    if (parent[i] == -1)
      *root = &t[i];
    else
      (t[parent[i]].left ? t[parent[i]].right : t[parent[i]].left) = &t[i];
  }
  assert(*root);
  return t;
}

void print_inorder(node *n) {
  if (n) {
    print_inorder(n->left);
    std::cout << n->val << ' ';
    print_inorder(n->right);
  }
}

int main() {
  int const parents[] = { 1, 5, 5, 2, 2, -1, 3 };
  node *root;
  auto t = std::move(make_tree(parents,
                               std::extent<decltype(parents)>::value,
                               &root));
  print_inorder(root);
}
