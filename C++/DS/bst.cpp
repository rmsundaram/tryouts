#include <iostream>
#include <vector>
#include <stack>

struct Node {
  int data = -1;
  Node* left = nullptr;
  Node* right = nullptr;
};

Node* insert(Node* n, int data) {
  if (!n)
    return new Node{data};

  if (data < n->data)
    n->left = insert(n->left, data);
  else if (data > n->data)
    n->right = insert(n->right, data);
  return n;
}

Node* search(Node* n, int data) {
  if (!n || n->data == data)
    return n;
  if (n->data > data)
    return search(n->left, data);
  return search(n->right, data);
}

Node* search_iter(Node* n, int data) {
  while (n) {
    if (n->data == data)
      return n;
    else
      n = (n->data > data) ? n->left : n->right;
  }
  return nullptr;
}

// Recursive in-order traversal.
size_t count(Node* n) {
  if (!n)
    return 0;
  return count(n->left) + 1 + count(n->right);
}

int height(Node* n) {
  if (!n)
    return 0;
  return 1 + std::max(height(n->left), height(n->right));
}

bool isBalanced(Node* n) {
  if (!n)
    return true;
  const int hl = height(n->left);
  const int hr = height(n->right);
  return (std::abs(hl - hr) <= 1) &&
    isBalanced(n->left) && isBalanced(n->right);
}

Node* min(Node* n) {
  while (n && n->left)
    n = n->left;
  return n;
}

Node* remove(Node* n, int data) {
  if (!n)
    return nullptr;
  if (n->data > data)
    n->left = remove(n->left, data);
  else if (n->data < data)
    n->right = remove(n->right, data);
  // Found the to-be-deleted node.
  else {
    // Node with no or right child only.
    if (n->left == nullptr) {
      Node* temp = n->right;
      delete n;
      return temp;
    }
    // Node with left child only.
    else if (n->right == nullptr) {
      Node* temp = n->left;
      delete n;
      return temp;
    }
    Node* temp = min(n->right);
    n->data = temp->data;
    // Delete inorder successor and ensure its parent’s reference adjustment.
    n->right = remove(n->right, temp->data);
  }
  return n;
}

// Returns a new tree.  Iterative in-order traversal.
// https://www.geeksforgeeks.org/iterative-depth-first-traversal/
// https://www.geeksforgeeks.org/tree-traversals-inorder-preorder-and-postorder/
std::vector<int> toArray(Node* n) {
  std::vector<int> v;
  std::stack<Node*> s;

  while (!s.empty() || !n) {
    while (n) {
      s.push(n);
      n = n->left;
    }
    n = s.top();
    s.pop();
    v.push_back(n->data);
    n = n->right;
  }
  return v;
}

// Expects |data| to point to sorted array with range ∈ [left, right) e.g.
//    index     0 1 2 3
//     data     3 5 6 x
// where left = 0, right = 3.
Node* fromArray(const int* data, int left, int right) {
  if (!data || (left >= right))
    return nullptr;
  const int mid = left + ((right - left) / 2);
  Node* const n = new Node{data[mid]};
  n->left = fromArray(data, left, mid);
  n->right = fromArray(data, mid + 1, right);
  return n;
}

// https://www.geeksforgeeks.org/convert-normal-bst-balanced-bst/?ref=lbp
Node* balance(Node* n) {
  const std::vector<int> v = toArray(n);
  return fromArray(v.data(), 0, v.size());
}

// Least common ancestor
Node* lca(Node* n, int a, int b) {
  if (!n)
    return nullptr;
  if (n->data > a && n->data > b)
    return lca(n->left, a, b);
  else if (n->data < a && n->data < b)
    return lca(n->right, a, b);
  // No further common ancestor to descend.
  return n;
}

int main() {

}
