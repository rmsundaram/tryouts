#include <iostream>
#include <cmath>

struct node
{
    int val = 0;
    node *left = nullptr, *right = nullptr;
};

bool isBalanced(node *n, int *ht) {
    if (!n) {
        *ht = 0;
        return true;
    }
    int lh = 0, rh = 0;
    bool lb = isBalanced(n->left, &lh),
         rb = isBalanced(n->right, &rh);
    *ht = std::max(lh, rh) + 1;
    if (std::abs(lh - rh) > 1)
        return false;
    else
        return lb && rb;
}

node* newNode(int data) {
    return new node{data, nullptr, nullptr};
}

int main() {
    /* Constructed binary tree is
               1
              / \
             /   \
            2     3
           / \   /
          4   5 6
         /
        7
    */
    node *root = newNode(1);
    root->left = newNode(2);
    root->right = newNode(3);
    root->left->left = newNode(4);
    root->left->right = newNode(5);
    root->right->left = newNode(6);
    root->left->left->left = newNode(7);

    int height = 0;
    std::cout << "Balanced: "
              << std::boolalpha << isBalanced(root, &height)
              << '\n';
}
