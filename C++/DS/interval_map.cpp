#include <cassert>
#include <map>
#include <limits>
#include <iterator>
#include <random>

/*
 * Each key-value-pair (k,v) in the m_map member means that the value v is
 * associated to the interval from k (including) to the next key (excluding) in
 * m_map.
 *
 * Example: the std::map (0,'A'), (3,'B'), (5,'A') represents the mapping
 *    0 -> 'A'
 *    1 -> 'A'
 *    2 -> 'A'
 *    3 -> 'B'
 *    4 -> 'B'
 *    5 -> 'A'
 *    6 -> 'A'
 *    7 -> 'A'
 *    ... all the way to numeric_limits<key>::max()
 *
 * The representation in m_map must be canonical, that is, consecutive map
 * entries must not have the same value: ..., (0,'A'), (3,'A'), ... is not
 * allowed. Initially, the whole range of K is associated with a given initial
 * value, passed to the constructor.
 *
 * Key type K
 *   besides being copyable and assignable, is less-than comparable via
 *   operator< is bounded below, with the lowest value being
 *   std::numeric_limits<K>::lowest() does not implement any other operations,
 *   in particular no equality comparison or arithmetic operators
 *
 * Value type V
 *   besides being copyable and assignable, is equality-comparable via
 *   operator== does not implement any other operations
 */

template<class K, class V>
class interval_map {

    std::map<K,V> m_map;

    friend void IntervalMapTest();

public:

    // constructor associates whole range of K with val by
    // inserting (K_min, val) into the map
    interval_map( V const& val ) {
        m_map.insert(m_map.begin(),
                     std::make_pair(std::numeric_limits<K>::lowest(), val));
    }

    // Assign value val to interval [keyBegin, keyEnd).
    // Overwrite previous values in this interval.
    // Do not change values outside this interval.
    // Conforming to the C++ Standard Library conventions, the interval
    // includes keyBegin, but excludes keyEnd.
    // If !( keyBegin < keyEnd ), this designates an empty interval,
    // and assign must do nothing.
    void assign( K const& keyBegin, K const& keyEnd, const V& val ) {
        // degenerate range, no-op
        if (!(keyBegin < keyEnd)) return;

        // get the same key or, if absent, the one after it
        auto lb = m_map.lower_bound(keyBegin);
        auto lb_prev = std::prev(lb);
        // We now have either keyBegin and the one before it, or in the case
        // of keyBegin's absence, the one after and before it.  In the former
        // case, lb_prev may be pointing to nothing since lb itself is cbegin
        // while in the latter case, lb can be cend and lb_prev being the one
        // before it.
        bool const keyExists =
            (lb != m_map.cend()) && (!(keyBegin < lb->first));
        // backup old value about to be over-written by val
        auto oldVal = (keyExists ? lb : lb_prev)->second;
        // the last part of the condition is for the case where keyBegin is lb
        // (found) but its value ≠ val and the key before it has val as value
        bool const oldValueIsVal = (oldVal == val);
        bool const noValueIsVal = (!oldValueIsVal) &&
            ((std::cbegin(m_map) == lb) || !(lb_prev->second == val));

        // the previous key isn't bearing val as its key, update or create
        if (noValueIsVal) {
            // keyBegin exists in the map, just update it
            if (keyExists)
                lb->second = val;
            // keyBegin doesn't exist, create a new (k, v) pair
            else
                lb = m_map.insert(lb, { keyBegin, val });
        }
        // In the case where lb is keyBegin and its value = val then lb should
        // be moved forward, in all other cases lb already would be pointing to
        // the head of tehe erasure list
        if (noValueIsVal || (keyExists && oldValueIsVal))
            lb = std::next(lb);
        // lb now points to the start of erasure list;
        // if lb_prev already had val as its value, then lb would be pointing
        // to keyBegin or later, either should be deleted.

        // if the iterator's key is less than keyEnd erase, if they're equal do
        // not delete since val's mapping ends just before keyEnd, so keyEnd and
        // whatever it's pointing to should be left alone.  However, an
        // exception is to erase if the values match.
        while ((lb != m_map.cend()) &&
               ((lb->first < keyEnd) || (lb->second == val))) {
            oldVal = lb->second;
            lb = m_map.erase(lb);
        }

        if (!(val == oldVal)) {
            // lb would now be keyEnd or later (includes cend());
            // update if not equal to an interval start
            if ((lb == m_map.cend()) || (keyEnd < lb->first))
                m_map.insert(lb, {keyEnd, oldVal});
        }
    }

    // look-up of the value associated with key
    V const& operator[]( K const& key ) const {
        return ( --m_map.upper_bound(key) )->second;
    }
};

// test code borrowed from
// http://github.com/burantino/IntervalMap

#include <vector>
#include <ctime>
#include <utility>

void check(const std::map<int, char> &m_map,
           const std::vector<std::pair<int, char>> &data) {
	auto it = data.cbegin();
	for (auto& pair : m_map) {
		assert(pair.first == it->first);
		assert(pair.second == it->second);
		++it;
	}
}

// Many solutions we receive are incorrect. Consider using a randomized test
// to discover the cases that your implementation does not handle correctly.
// We recommend to implement a function IntervalMapTest() here that tests the
// functionality of the interval_map, for example using a map of unsigned int
// intervals to char.

// Write a randomized unit test, assigning a million random intervals with
// begin/end between 0 and 10, with random values between 0 and 10, and check
// for the invariants after each step. Assign the same interval to an array and
// compare this trivial implementation to the results of the assign
// function. Also check how many O(log n) operations you need on each step (you
// can usually see that from the code already, but we actually wrap the map to
// check). Are your insertion hints correct? Make sure you use only the allowed
// operations on K and V. Disable all optimizations and use an STL
// implementation with checking iterators.

void IntervalMapTest() {
	interval_map<int, char> map('X');
	auto min = std::numeric_limits<int>::min();
	auto max = std::numeric_limits<int>::max();

	//test1 simple case
	map.assign(0, 50, 'A');
	map.assign(-1, 0, 'A');
	map.assign(45, 50, 'B');

	check(map.m_map, std::vector<std::pair<int, char>> {
            { min, 'X' },
            { -1, 'A' },
            { 45, 'B' },
            { 50, 'X' }
        });

	//test1 simple case
	map.assign(min, max, 'X');
	map.assign(0, 0, 'A');
	map.assign(20, 30, 'B');
	map.assign(40, 50, 'C');

	check(map.m_map, std::vector<std::pair<int, char>> {
            { min, 'X' },
            { 20, 'B' },
            { 30, 'X' },
            { 40, 'C' },
            { 50, 'X' }
        });

	//test1 simple case
	map.assign(0, 10, 'A');
	map.assign(20, 30, 'B');
	map.assign(40, 50, 'C');

	check(map.m_map, std::vector<std::pair<int, char>> {
			{ min, 'X' },
			{ 0, 'A' },
			{ 10, 'X' },
			{ 20, 'B' },
			{ 30, 'X' },
			{ 40, 'C' },
			{ 50, 'X' },
	});

	//test2
	map.assign(5, 25, 'O');     // overlap
	check(map.m_map, std::vector<std::pair<int, char>> {
			{ min, 'X' },
			{ 0, 'A' },
			{ 5, 'O' },
			{ 25, 'B' },
			{ 30, 'X' },
			{ 40, 'C' },
			{ 50, 'X' },
	});

	map.assign(5, 35, 'O');     // overlap
	check(map.m_map, std::vector<std::pair<int, char>> {
			{ min, 'X' },
			{ 0, 'A' },
			{ 5, 'O' },
			{ 35, 'X' },
			{ 40, 'C' },
			{ 50, 'X' },
	});


	map.assign(2, 45, 'O');     // overlap
	check(map.m_map, std::vector<std::pair<int, char>> {
            { min, 'X' },
			{ 0, 'A' },
			{ 2, 'O' },
			{ 45, 'C' },
			{ 50, 'X' },
	});

	map.assign(2, 45, 'O');
	check(map.m_map, std::vector<std::pair<int, char>> {
            { min, 'X' },
			{ 0, 'A' },
			{ 2, 'O' },
			{ 45, 'C' },
			{ 50, 'X' },
	});

	map.assign(min, max, 'X');
	map.assign(0, 10, 'A');
	map.assign(20, 30, 'A');
	map.assign(10, 20, 'A');

	check(map.m_map, std::vector<std::pair<int, char>> {
            { min, 'X' },
			{ 0, 'A' },
			{ 30, 'X' },
	});

	map.assign(min, max, 'X');
	map.assign(0, 10, 'A');
	map.assign(5, 30, 'A');

	check(map.m_map, std::vector<std::pair<int, char>> {
            { min, 'X' },
			{ 0, 'A' },
			{ 30, 'X' },
	});

	map.assign(min, max, 'X');
	map.assign(0, 10, 'A');
	map.assign(20, 30, 'A');
	map.assign(8, 20, 'A');

	check(map.m_map, std::vector<std::pair<int, char>> {
            { min, 'X' },
			{ 0, 'A' },
			{ 30, 'X' },
	});

	map.assign(min, max, 'X');
	map.assign(0, 10, 'A');
	map.assign(20, 30, 'A');
	map.assign(12, 20, 'A');

	check(map.m_map, std::vector<std::pair<int, char>> {
            { min, 'X' },
			{ 0, 'A' },
			{ 10, 'X' },
			{ 12, 'A' },
			{ 30, 'X' },
	});

	map.assign(min, max, 'X');
	map.assign(0, 10, 'A');
	map.assign(20, 30, 'B');
	map.assign(5, 15, 'A');
	map.assign(15, 28, 'B');

	check(map.m_map, std::vector<std::pair<int, char>> {
            { min, 'X' },
			{ 0, 'A' },
			{ 15, 'B' },
			{ 30, 'X' },
	});

	//random test
    std::random_device rd;
    std::seed_seq seeds{rd(), rd(), rd(), rd(), rd(), rd(), rd(), rd()};
    std::mt19937 mt{seeds};
	constexpr int maxi = 100000;
    std::uniform_int_distribution<> dist_key{-maxi-1, maxi-1};
    std::uniform_int_distribution<char> dist_val{'A', 'B'};
	map.assign(min, max, 'X');
	for (auto i = -maxi; i < maxi; i++)
		map.assign(dist_key(mt), dist_key(mt), dist_val(mt));
}

// run unit tests
int main() {
	IntervalMapTest();
}
