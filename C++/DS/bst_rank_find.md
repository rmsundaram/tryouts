# Question

Given below tree, find a node based on rank.  Node ranks aren’t given.

![BST with nodes ranked](bst_ranked.jpg "BST with nodes ranked")

```
                75(7)
               /     \
          60(5)       100(10)
        /     \       /     \
     50(2)   62(6)  87(8)   110(11)
   /    \              \      \
20(1)  55(3)          94(9)   120(12)
          \
         58(4)
```

People solve it in O(n) but the challenge is to solve in O(log n).  Brute-force solution: in-order traversal and

1. store in a sorted vector; give results
2. give result without storing

# Solution

## Observations

* _rank_, here, is actually the order of the node in an DFS [in-order traversal][].
* Number of nodes in left sub-tree gives us a node’s rank.
  - However, there’re also cases with no left sub-tree.
  - In such case, its parent’s rank would give its; do we really need this?
  - We can make-do without knowing the actual rank.
* Modification of `record` structure to include child count is needed.

## Pseudocode

Assume availability of pre-computed child count of a node.

``` text
record
{
  left,     // left subtree
  right,    // right subtree
  nChild    // child count (left + right, excluding itself)
};

find(node, r)
{
  if ((r <= 0) || (r > (node->nChild + 1)))
    return nil;

  // grand father |node| rank = grand children count + 2 (child and self)
  // if left subtree is absent, assume 1 and offset for it later
  let rank = node->left ? (node->left->nChild + 2) : 1;

  if (r == rank)
    return node

  if (r > rank)
    return find(node->right, r - rank);

  return find(node->left, r)
}
```

[in-order traversal]: https://en.wikipedia.org/wiki/Tree_traversal#In-order_(LNR)
