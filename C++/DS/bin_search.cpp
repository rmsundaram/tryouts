#include <iostream>

using std::cout;
using std::endl;

int binary_search(int *arr, int low, int high, int x)
{
	if(low > high)
		return -1;

	int mid = low + ((high - low) / 2);

	if(x == arr[mid])
		return mid;
	else if(x < arr[mid])
		return binary_search(arr, low, mid - 1, x);
	else
		return binary_search(arr, mid + 1, high, x);
}

int main()
{
	int a[] = {2, 5, 9, 12, 45, 73, 92, 103, 150, 193, 204, 248};

	cout << binary_search(a, 0, (sizeof(a) / sizeof(*a)) - 1, 9) << endl;
}
