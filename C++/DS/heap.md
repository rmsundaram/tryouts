# Adding to a heap
Push to the end of the array and keep verifying if the heap's property that an element's priority is lesser (or greater) than its parent's.  If this is violated, bubble it up.  Keep doing this until this is satisfied.

# Removing from a heap
Pop the smallest (or largest) element, the root and replace it with the last element.  Now verify if the heap property is satisfied; if not, keep sinking it down until the parent is smaller (or larger) than its children.

# Fixing a heap
When a heap is corrupted i.e. an element's priority silently changed, the heap no longer returns the smallest (or largest) element correctly in order.  To fix this situation, we've to _re-heapify_ . This is just walking down the tree from root, left to right, verifying if an element is smaller (or larger) than its parent.  When this property is violated, we swap the element with its parent; we bubble it up until the property is satisfied.  When all nodes are visited, the heap will be fixed.

# Heap sort
To heap sort an unsorted array, we just build a heap out of it by adding elements one by one.  Once the heap is built, we keep popping the root (placing it in a sorted array), replacing it with the last element to _sink-down_ (this part will automatically be done when the root if popped if we built a heap data structure and are not operating atop a flat array).

# `boost::priority_queue`
1. For every element pushed, it'll store a pointer to a handle that's returned.
2. As an element moves, the handle is updated to have the element's slot index
3. Once can change the priority using the handle and reseat the element
