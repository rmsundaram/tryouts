// Program hashing integer 2D vectors into a fixed size array.
// https://softwareengineering.stackexchange.com/q/49550/4154

// https://burtleburtle.net/bob/hash/hashfaq.html

#include <iostream>
#include <cstdint>
#include <string>
#include <sstream>
#include <tuple>

constexpr size_t MAX_BUCKETS = 64;

// https://gist.github.com/ruby0x1/81308642d0325fd386237cfa3b44785c
uint64_t hash_64_fnv1a(const void* data, const uint64_t len) {
  const char* key = (char*)data;
  const uint64_t prime = 0x100000001b3;
  uint64_t hash = 0xcbf29ce484222325;

  for (auto i = 0ull; i < len; ++i) {
    uint8_t value = key[i];
    hash ^= value;
    hash *= prime;
  }

  return hash;
}


// Under LGPL licence; Author: Paul Hsieh
// http://azillionmonkeys.com/qed/hash.html
#include <cstdint>
#undef get16bits
#if (defined(__GNUC__) && defined(__i386__)) || defined(__WATCOMC__) \
  || defined(_MSC_VER) || defined (__BORLANDC__) || defined (__TURBOC__)
#define get16bits(d) (*((const uint16_t *) (d)))
#endif

#if !defined (get16bits)
#define get16bits(d) ((((uint32_t)(((const uint8_t *)(d))[1])) << 8)\
                       +(uint32_t)(((const uint8_t *)(d))[0]) )
#endif

uint32_t SuperFastHash (const char * data, int len) {
uint32_t hash = len, tmp;
int rem;

    if (len <= 0 || data == NULL) return 0;

    rem = len & 3;
    len >>= 2;

    /* Main loop */
    for (;len > 0; len--) {
        hash  += get16bits (data);
        tmp    = (get16bits (data+2) << 11) ^ hash;
        hash   = (hash << 16) ^ tmp;
        data  += 2*sizeof (uint16_t);
        hash  += hash >> 11;
    }

    /* Handle end cases */
    switch (rem) {
        case 3: hash += get16bits (data);
                hash ^= hash << 16;
                hash ^= ((signed char)data[sizeof (uint16_t)]) << 18;
                hash += hash >> 11;
                break;
        case 2: hash += get16bits (data);
                hash ^= hash << 11;
                hash += hash >> 17;
                break;
        case 1: hash += (signed char)*data;
                hash ^= hash << 10;
                hash += hash >> 1;
    }

    /* Force "avalanching" of final 127 bits */
    hash ^= hash << 3;
    hash += hash >> 5;
    hash ^= hash << 4;
    hash += hash >> 17;
    hash ^= hash << 25;
    hash += hash >> 6;

    return hash;
}


// http://web.archive.org/web/20071223173210/http://www.concentric.net/~Ttwang/tech/inthash.htm
uint32_t hash32shiftmult(uint32_t key)
{
  uint32_t c2 = 0x27d4eb2d; // a prime or an odd constant
  key = (key ^ 61) ^ (key >> 16);
  key = key + (key << 3);
  key = key ^ (key >> 4);
  key = key * c2;
  key = key ^ (key >> 15);
  return key;
}


// http://www.beosil.com/download/CollisionDetectionHashing_VMV03.pdf
// https://stackoverflow.com/a/5929567/183120
uint32_t hash_2d(uint32_t x, uint32_t y) {
  constexpr uint32_t P1 = 0x466F45D;
  constexpr uint32_t P2 = 0x4F9FFB7;
  return (x * P1) ^ (y * P2);
}


// https://burtleburtle.net/bob/hash/doobs.html
// https://gist.github.com/badboy/6267743
#define rot(x,k) (((x)<<(k)) | ((x)>>(32-(k))))
uint32_t mix(uint32_t a, uint32_t b, uint32_t c) {
  a -= c;  a ^= rot(c, 4);  c += b;
  b -= a;  b ^= rot(a, 6);  a += c;
  c -= b;  c ^= rot(b, 8);  b += a;
  a -= c;  a ^= rot(c,16);  c += b;
  b -= a;  b ^= rot(a,19);  a += c;
  c -= b;  c ^= rot(b, 4);  b += a;
  return c;
}


// Integer hashing function found using skeeto’s prospector [1][2].
// [1]: https://nullprogram.com/blog/2018/07/31/
// [2]: https://github.com/skeeto/hash-prospector/issues/19
// https://nullprogram.com/blog/2020/10/19/#hash-table-memoization
uint32_t hash32(uint32_t x) {
  // 16 21f0aaad 15 d35a2d97 15
  x ^= x >> 16;
  x *= 0x21f0aaad;
  x ^= x >> 15;
  x *= 0xd35a2d97;
  x ^= x >> 15;
  return x;
}


// Pairing two integers into one in a unique and deterministic way.
// https://stackoverflow.com/a/682617/183120
// https://en.wikipedia.org/wiki/Pairing_function#Cantor_pairing_function
// See Also:
// https://graphics.stanford.edu/~seander/bithacks.html#InterleaveTableObvious
// The above interleaved bit hack is a specific case (2D -> 1D) of Morton order:
// multidimensional data to one dimension map such that coordinates close to
// each other in N-dimensional space have close morton numbers.
// https://en.wikipedia.org/wiki/Z-order_curve
// https://github.com/Forceflow/libmorton
uint64_t cantor(uint32_t x, uint32_t y) {
  const uint64_t i = x, j = y;
  return ((i + j) * (i + j + 1) / 2) + j;
}


// Szudzik’s pairing function; more elegant pairing that Cantor’s.
// https://stackoverflow.com/a/13871379
uint64_t szudzik(uint32_t x, uint32_t y) {
  uint64_t i = x, j = y;
  return (i > j) ? ((i * i) + i + j) : ((j * j) + i);
}


// https://stackoverflow.com/questions/2634690/good-hash-function-for-a-2d-index
// Why is it best to use a prime number as a mod in a hashing function?
// https://cs.stackexchange.com/questions/11029
auto hash(uint32_t x, uint32_t y) {
  const uint32_t PRIME = 53;
  const auto xh = hash32shiftmult(x);
  const auto yh = hash32shiftmult(y);
  const auto key = (PRIME + xh) * PRIME + yh;
  // const uint64_t meld = ((uint64_t) x) << 32 | y;
  return std::make_tuple(
    hash_64_fnv1a(reinterpret_cast<const void*>(&key), sizeof(key)),
    SuperFastHash(reinterpret_cast<const char*>(&key), sizeof(key)));
}


int main() {
  std::string input;
  while (std::getline(std::cin, input)) {
    if (input.empty()) continue;
    std::stringstream ss(input);
    uint32_t x = 0, y = 0;
    ss >> x >> y;
    auto [fnv, sfh] = hash(x, y);
    std::cout << "  fnv(" << x << ", " << y << ") = " << fnv
              << " % " << MAX_BUCKETS << " = " << fnv % MAX_BUCKETS << '\n'
              << "  sfh(" << x << ", " << y << ") = " << sfh
              << " % " << MAX_BUCKETS << " = " << sfh % MAX_BUCKETS << '\n';
  }
}
