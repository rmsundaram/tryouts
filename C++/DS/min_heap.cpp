#include <array>
#include <cassert>
#include <iostream>

/*
 * Find K largest elements from an arbitrarily large set of elements.  A min
 * heap can be employed to find them with a space complexity of just K.  The
 * stock std::priority_queue cannot be used here since it isn't bounded; it may
 * be used as a max heap (the intuitive option), inserting all N elments and
 * extracting K would give the same result in the right order but at a space
 * complexity of N.
 *
 * In min heap lighter elements bubble up while heavier elements sink down i.e.
 * value(parent) < value(children).  We (ab)use this property to get K largest
 * elements; refer min_heap::insert.
 *
 * SEE ALSO
 *
 * 1. //C++/DS/heap.md
 * 2. Binary Heap @ //CG/WebGL/NavmeshPather/bin-heap.mjs.
 * 3. Fibonacci Heap -- theoretically faster -- for priority queue operations,
 *    consisting of a collection of heap-ordered trees, has better amortized
 *    running time than many other priority queue implementations including
 *    binary heap; practically may not be as efficient though.
 */

template <size_t N>
class min_heap
{
    std::array<int, N>  store = { };
    size_t              count = 0u;

    // returns the index of the left child
    size_t child(size_t idx) {
        auto const i = idx * 2u + 1u;
        // // validate if this child exists
        // assert(i < count);
        return i;
    }

    size_t parent(size_t idx) {
        // root never has a parent
        assert(idx != 0u);
        return (idx - 1u) / 2u;
    }

    // Terminology Reference (both interface and implementation)
    // http://en.wikipedia.org/wiki/Binary_heap
    void bubble_up(size_t i) {
        // root has no parent; no more bubbling up
        if (i != 0) {
            auto const p = parent(i);
            if (store[i] < store[p]) {
                std::swap(store[i], store[p]);
                bubble_up(p);
            }
        }
    }

    void bubble_down(size_t p) {
        auto const li = child(p);
        // we're at a leaf; no more bubbling down
        if (li >= count)
            return;
        auto const ri = li + 1;
        // if right is valid, so will left be
        size_t const i = ((ri < count) && (store[ri] < store[li])) ? ri : li;
        if (store[p] > store[i]) {
            std::swap(store[p], store[i]);
            bubble_down(i);
        }
    }

public:

    bool insert(int x) {
        if (count < N) {
            store[count] = x;
            bubble_up(count++);
        }
        else if (x > store[0]) {
            // In an unbounded heap, insertion is always at leaf level (store's
            // end) and the root is always extracted.  When we're out of bounds,
            // the trick is to insert at root, thereby killing min with a larger
            // value and maintaining the heap as the running set of K largest
            // elements encountered thus far.
            store[0] = x;
            bubble_down(0u);
        }
        else
            return false;
        // Out of bounds but x ≤ root; discard as we need the largest elements
        // and adding this would disturb that.

        return true;
    }

    int extract() {
        if (count == 0)
            throw std::out_of_range("No more items");
        int const out = store[0];
        store[0] = store[--count];
        bubble_down(0u);
        return out;
    }

    size_t size() const {
        return count;
    }

    // friend printer
    template <size_t n>
    friend std::ostream& operator<<(std::ostream& os, min_heap<n> const &mh);
};

// The array representation of a heap is in its level-order; top to bottom,
// left to right.  See Also:
//   1. Algorithms, 4/e by Robert Sedgewick
//      §2.4 Priority Queues -> Binary heap representation
//   2. https://www.geeksforgeeks.org/level-order-tree-traversal/
template <size_t N>
std::ostream& operator<<(std::ostream& os, min_heap<N> const &mh) {
    for (auto i = 0u; i < mh.count; ++i)
        std::cout << mh.store[i] << '\t';
    return os << '\n';
}

int main() {
    constexpr size_t k = 6;
    min_heap<k> mh;

    // insert elements
    for (int i : {12, 9, 4, -3, 1, 2, 82, -54, 87, 29, 74 }) {
        mh.insert(i);
        std::cout << mh;
    }
    std::cout << '\n';

    // extract K largest in reverse order
    for (auto i = 0u; i < k; ++i)
        std::cout << mh.extract() << '\n';
    std::cout << '\n';

    std::cout << "Count = " << mh.size();
}
