#include <iostream>
#include <string>
#include <vector>
#include <stack>
#include <algorithm>
#include <utility>

template <typename T>
struct Node {
  T data;
  std::vector<size_t> dependants;
};

template <typename T>
using Graph = std::vector<Node<T>>;

// Iterative (stack-based) variant of recursive solution at
// https://www.geeksforgeeks.org/topological-sorting/
template <typename T>
std::vector<size_t> tsort(const Graph<T>& g) {
  std::vector<bool> visited(g.size(), false);
  std::vector<size_t> order;
  order.reserve(g.size());

  for (auto i = 0u; i < g.size(); ++i) {
    if (visited[i])
      continue;
    std::stack<size_t> stack;
    stack.push(i);
    while (!stack.empty()) {
      auto k = stack.top();
      if (visited[k]) {
        order.push_back(k);
        stack.pop();
        continue;
      }
      visited[k] = true;
      for (auto d : g[k].dependants) {
        // TODO: detect cycle and error out
        if (!visited[d])
          stack.push(d);
      }
    }
  }
  return order;
}

int main() {
  // Test 1
  {
    // Physics depends on Maths
    // Maths depends on Language
    // Art depends on Language and Craft
    // Lab depends on Physics and Craft
    Graph<std::string> g = {Node<std::string>{"Physics"},
                            Node<std::string>{"Math"},
                            Node<std::string>{"Language"},
                            Node<std::string>{"Art"},
                            Node<std::string>{"Craft"},
                            Node<std::string>{"Lab"}};
    g[1].dependants = {0};
    g[2].dependants = {1, 3};
    g[4].dependants = {3, 5};
    g[0].dependants = {5};

    auto order = tsort(g);
    for (auto it = order.crbegin(); it != order.crend(); ++it)
      std::cout << g[*it].data << '\n';
  }
  // Test 2
  {
    Graph<int> g = {Node<int>{0},
                    Node<int>{1},
                    Node<int>{2},
                    Node<int>{3},
                    Node<int>{4},
                    Node<int>{5}};
    g[5].dependants = {0, 2};
    g[4].dependants = {0, 1};
    g[2].dependants = {3};
    g[3].dependants = {1};

    auto order = tsort(g);
    std::cout << '\n';
    for (auto it = order.crbegin(); it != order.crend(); ++it)
      std::cout << g[*it].data << '\n';
  }
}
