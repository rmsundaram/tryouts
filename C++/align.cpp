// g++ -Wall -Wextra -std=c++17 -pedantic -pedantic-errors align.cpp
// cl /EHsc /std:c++17 align.cpp
#include <iostream>
#include <iomanip>
#include <type_traits>
#include <algorithm>
#include <memory>
#include <limits>

// Intel’s _mm_malloc and _mm_free are supported by GCC, Clang and MSVC. These are in mm_malloc.h for GCC and Clang,
// while for MSVC it’s malloc.h. Same header has _aligned_malloc and _aligned_free too for MSVC and MinGW. Another
// option is stdlib.h’s posix_memalign/free; works everywhere except Windows, unavailable on both MSVC and MinGW.
#if defined(_MSC_VER) || defined(__MINGW32__)
#  include <malloc.h>
#  ifdef _MSC_VER
#    include <cstdalign>    // no stdalign.h as of VS2019
#  endif
#else
#  include <mm_malloc.h>
#  include <stdalign.h>   // cstdalign deprecated in C++17, removed in C++20
#endif

// Dynamic Aligned Memory Allocation
// C11/C++17’s aligned_alloc honours the call only if requested size is a integral multiple of alignment
// i.e. stricter/wider/extended alignments larger than the natural alignment is dishonoured.
// aligned_alloc(/*align*/ 16, /*size*/ 4) returns 0, while aligned_alloc(16, 48) returns a valid address.
// Also it’s unsupported by MSVC and MinGW.

// Alignment of Automatic/Stack-living Variables
// Use attributes for reliability. C++11 leaves ‘alignas’ as implementation-defined for extended alignment requests.
#ifdef __GNUC__  // GCC and Clang toolchains define this; use __GNUG__ for (__cplusplus__ && __GNUC__)
#  define ALIGN(x) __attribute__ ((aligned(x)))
#elif defined(_MSC_VER)
#  define ALIGN(x) __declspec(align(x))
#else
#  error "Unknown compiler; can't set alignment attribute!"
#endif

#if defined(__cplusplus) && defined(__alignof_is_defined)
#  define ALIGNOF(x) alignof(x)
#elif defined(_Alignof)
#  define ALIGNOF(x) _Alignof(x)
#else
#  ifdef _MSC_VER
#    define ALIGNOF(X) __alignof(X)
#  else
#    define ALIGNOF(X) __alignof__(X)
#  endif
#endif

/*
 * Noel Llopis, Game Developer Magazine, Dec 08 and Jan 09
 * http://www.gamasutra.com/view/feature/3942/data_alignment_part_1.php
 * http://www.gamasutra.com/view/feature/132367/data_alignment_part_2_objects_on_.php
 *
 * Aligning dynamically allocated memory
 * http://stackoverflow.com/q/227897
 * http://github.com/NickStrupat/AlignedMalloc
 *
 * Data alignment in general (explains 16-byte alignment in detail):
 * http://www.songho.ca/misc/alignment/dataalign.html
 *
 * Manually aligning memory can be done by allocating extra space of one less than the required alignment and then
 * aligning it with pointer arithmetic using the mask ~(align - 1) so that it zeros out the required part of the
 * address. E.g. to align to 16-byte boundary, a mask of ~0x0F = 0b1111 1111 1111 1111 1111 1111 1111 0000 = 0xFFFFFFF0
 * will be used as a mask to align it to 16, because 16-byte aligned address must be divisible by 16, the least
 * significant digit in hex number should be 0 all the time. In the below function, however, we avoid the pointer
 * arithmatic using C++11's std::align. When doing it manually, using binary & with a char* and a size_t will result in
 * an error - uintptr_t should be used instead[1], although it's C99 and isn't mandatory, most compilers support it.
 * [1]: http://stackoverflow.com/q/1845482
 * [2]: http://jongampark.wordpress.com/2008/06/12/implementation-of-aligned-memory-alloc/
 *
 * EXAMPLE:
 * assume sizeof(void*) = sizeof(float) = 4
 * alloc_aligned<float>(1u, 16u)
 *
 *  01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 * |  |  |  |  |  |  |  |  |  |  |  |00|00|00|06|  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |
 * +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
 *                 p            q                a                                     End
 * malloc 4 + 4 + 15 bytes
 * pass (char*)p + 4 to std::align i.e. past the space we've set aside for a void*
 * even if the immediately following byte is the aligned one we're looking for
 * store p at (char**)a - 4
 * free (char**)a - 4
 */

template<typename T>
T* alloc_aligned(size_t count, size_t align) {
    size_t const bytes = sizeof(T) * count;
    size_t len = bytes + (align - 1u);
    // additional reserve to store pointer to be passed to free and for alignment
    // p is at start
    void *p = malloc(sizeof(void*) + len);
    if (!p) return nullptr;
    // q is offset from start by the size of a void pointer to store the free address
    void *q = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(p) + sizeof(void*));
    void *a = reinterpret_cast<T*>(std::align(align,
                                              bytes,
                                              q,
                                              len));
    if (a) {
        reinterpret_cast<void**>(a)[-1] = p;
    }
    else {
        free(p);
    }
    return reinterpret_cast<T*>(a);
}

template <typename T>
void free_aligned(T *p) {
    free(reinterpret_cast<void**>(p)[-1]);
}

/*
 * Instead of diving, remembering the fact that the least significant digit of a hex number would be zero for it to be
 * completely divisible by 16 helps — 0b…0000 = 0x…0; likewise, for a number completely divisible by 8, the bit pattern
 * would be 0b…000 ⇒ 0x…y: y = {0x0, 0x1}; similarly to be completely divisible by 4, n = 0b…00 ⇒ 0x…y: y = { 0x0, 0x4,
 * 0x8, 0xC }. The essence is that all the bits less than the number's should be cleared leading to no remainders when
 * divided by it; otherwise the ones set will lead to a non-zero remainder upon dividing. The bit preceeding those zeros
 * may be 0 or 1 E.g. 0b100000 is divisible not only by 16 but also by 32. 0x20 is 32, 16, 8, 4, 2, 1-byte aligned. 0x40
 * is 64-and-lesser-powers-of-two byte aligned.
 */
template <typename T>
void ptr_print(T *p, float align = 16.0f) {
    auto a = reinterpret_cast<uintptr_t>(p);
    std::cout << std::hex << "0x" << a << std::dec << '\t'
              << std::fixed << std::setprecision(2) << (a / align) << '\n';
}

template <typename T>
bool is_aligned(T *p, size_t align = 16u) {
    return (reinterpret_cast<uintptr_t>(p) & (align - 1)) == 0;
}

/*
 * http://stackoverflow.com/a/8752720
 * Alignment requirements are recursive: the alignment of any struct is simply the largest alignment of any of its
 * members, and this is understood recursively. Every member may have padding after it so that the following member can
 * align on its natual length boundary. The first member of a struct can have padding following it but never before
 * it. Arrays have no padding in between their elements, but the elements (struct objects) themselves may have padding
 * after their last member so that the following array element will be aligned properly.
 *
 * Natural alignment means alignment to a boundary equal to its own size. A char can be allocated at any address, short
 * of 2 bytes requires an even address to be naturally aligned and so on.
 * http://stackoverflow.com/q/6474316
 *
 * Order members by decreasing alignment to minimize padding. Keep most used members in the first 128 bytes for perf.
 * Use -Wpadded to warn upon padding to a structure [3][4].
 *
 * If x > y, and both x and y are powers of two, a variable that is x-byte aligned is also y-byte aligned. With powers
 * of two alignments ascending/descending (by size) declaration delivers an optimal struct size.
 * http://virtrev.blogspot.in/2010/09/memory-alignment-theory-and-c-examples.html
 *
 * For X86, these are the guidelines from Intel[5]:
 * In general, for the best performance, align data as follows:
 *   – align 8-bit data at any address
 *   – align 16-bit data to be contained within an aligned four-byte word
 *   – align 32-bit data so that its base address is a multiple of four
 *   – align 64-bit data so that its base address is a multiple of eight
 *   – align 80-bit data so that its base address is a multiple of sixteen
 *   – align 128-bit data so that its base address is a multiple of sixteen
 *
 * [3]: The Lost Art of Structure Packing: http://www.catb.org/esr/structure-packing/
 * [4]: Order Your Members: https://jonasdevlieghere.com/order-your-members/
 * [5]: https://software.intel.com/en-us/articles/data-alignment-when-migrating-to-64-bit-intel-architecture
 */

/*
 * the struct should also bear the alignment attribute if it has more than one member since the size of the struct
 * mightn't be a multiple of 16 and setting the attribute to the struct too makes sure this is the case; if the size
 * isn't a muliple of 16, then objects of this type residing in an array will be unaligned. However both GCC and VC++
 * aligns the objects of Vec3 in an array with only the member bearing the attribute i.e. the size is made a multiple of
 * 16 by inserting padding bits. This makes sense since the alignment of a struct is the alignment of its widest member.
 */
struct ALIGN(16) Vec3
// GCC supports taking this as a template non-type argument which may be passed to the macro while VC++ (VS2013) doesn't
{
    // no ctor to maintain it as a POD as per C++14; dummy non-const method to test if it is still a POD
    void zero_out() { std::fill(e, e + 3, 0.0f); }

    // e would be aligned without the attribute too if unit follows e as it'd be the first member of an aligned struct.
    // Additionally, the struct size would be optimal if e were the first member. Now both unit and e would've to be
    // 16-byte aligned and thus 15 bytes would be padded between unit and e
    bool unit;
    ALIGN(16) float e[3];
};
static_assert((sizeof(Vec3) % 16u == 0) && (ALIGNOF(Vec3) == 16), "Error: Vec3 not aligned on a 16-byte boundary");

// here the struct needn't bear the attribute since its size is guarenteed to be 16 bytes
// and its only member is 16-byte aligned, so members of Matrix4x4 in an array and their
// respective e's would all be 16-byte aligned
struct Matrix4x4
{
    ALIGN(16) float e[4];
};
static_assert((sizeof(Matrix4x4) == 16) && (ALIGNOF(Matrix4x4) == 16),
              "Error: Matrix4x4 not aligned on a 16-byte boundary");

// Use ‘typedef’ to define an aligned type. ‘using’ compiles but ignores the requirement ;)
typedef ALIGN(8) float v3_typedef[3];
using v3_using = ALIGN(8) float[3];

struct Bulky
{
    int health;
    bool is_alive;
    Matrix4x4 xform;
    short unit_id;

    // Additionally the array, nothrow, array nothrow overloads for both new and delete are to be provided too.
    // Placement overloads (both default and custom) may be helpful when having object pools or custom allocators.
    void* operator new (std::size_t s) {
        std::cout << "ctor\n";
#if defined(_MSC_VER) || defined(__MINGW32__)
        return _aligned_malloc(s, ALIGNOF(Bulky));
#else
        static_assert(sizeof(Bulky) > ALIGNOF(Bulky), "aligned_alloc works only when Size = n * Alignment, n ∈ Z");
        return aligned_alloc(ALIGNOF(Bulky), s);
#endif
    }

    void operator delete(void *p) {
        std::cout << "dtor\n";
#if defined(_MSC_VER) || defined(__MINGW32__)
        _aligned_free(p);
#else
        free(p);
#endif
    }
};

struct field
{
    int w;
    explicit field(int width) : w (width) { }
};

std::ostream& operator<<(std::ostream &os, field f) {
    return os << std::left << std::setw(f.w);
}

int main() {
    auto const fw = 15u;
    std::cout << field(fw) << "TYPE" << "SIZE\tALIGNMENT" << '\n'
              << field(fw) << "void*" << sizeof(void*) << '\t' << ALIGNOF(void*) << '\n'
              << field(fw) << "double" << sizeof(double) << '\t' << ALIGNOF(double) << '\n'
              << field(fw) << "long double" << sizeof(long double) << '\t' << ALIGNOF(long double) << '\n'
              << field(fw) << "max_align_t" << sizeof(max_align_t) << '\t' << ALIGNOF(max_align_t) << '\n'
              << field(fw) << "Vec3" << sizeof(Vec3) << '\t' << ALIGNOF(Vec3) << '\n'
              << field(fw) << "Matrix4x4" << sizeof(Matrix4x4) << '\t' << ALIGNOF(Matrix4x4) << '\n'
              << field(fw) << "Bulky" << sizeof(Bulky) << '\t' << ALIGNOF(Bulky) << '\n'
              << field(fw) << "v3_typedef" << sizeof(v3_typedef) << '\t' << ALIGNOF(v3_typedef) << '\n'
              << field(fw) << "v3_using" << sizeof(v3_using) << '\t' << ALIGNOF(v3_using) << '\n';

    // C++17 defines __STDCPP_DEFAULT_NEW_ALIGNMENT__; it’s 16 bytes on GCC, MSVC and Clang. A wider
    // default alignment requirement on dyanmic memory allocation can be done with -faligned-new.
    // https://stackoverflow.com/q/49373287/183120
    std::cout << "__STDCPP_DEFAULT_NEW_ALIGNMENT__ = " << __STDCPP_DEFAULT_NEW_ALIGNMENT__ << '\n';
#ifndef _MSC_VER
    std::cout << "__BIGGEST_ALIGNMENT__ = " << __BIGGEST_ALIGNMENT__ << '\n';
#endif

    std::cout << "Vec3 a pod? " << std::boolalpha << std::is_pod<Vec3>::value << '\n'
              << std::setprecision(std::numeric_limits<float>::max_digits10);

    Vec3 p[3];
    std::cout << "p[i]\tp[i].e\n";
    for (auto i = 0u; i < 3; ++i) {
        std::cout << is_aligned(&p[i]) << '\t' << is_aligned(p[i].e) << '\n';
    }

    Bulky b[3];
    std::cout << "b[i]\tb[i].xform\n";
    for (auto i = 0u; i < 3; ++i) {
        std::cout << is_aligned(&b[i]) << '\t' << is_aligned(&b[i].xform) << '\n';
    }

    /*
     * Automatic objects of classes bearing the aligned attribute will be rightly aligned; however dynamically
     * allocating space for it using new will be aligned only if the alignment required ≤ alignof(max_align_t) since the
     * pointer returned by new “shall be suitably aligned so that it can be converted to a pointer of any complete
     * object type with a fundamental alignment requirement”. std::max_align_t is a complete object type with the
     * strictest fundamental alignment[6]. Thus when an alignment stricter (wider) than that of std::max_align_t is
     * required, C++11's alignment tools may not work since their behaviour is implementation-defined. If the compiler
     * guarentees that it'll respect C++11's std::aligned_storage then it seems to be a very simple and effective
     * solution for both automatic and dyanmically allocated objects[7]. Overloading class new/delete too is a viable
     * option. C++17 requires dynamic allocation of over-aligned data to respect its alignment[8]; also introduces new
     * and delete overloads that take a std::align_val_t argument.
     * [6]: http://stackoverflow.com/q/6973995
     * [7]: http://thebytekitchen.com/2012/12/23/harder-to-c-aligned-memory-allocation-3/
     * [8]: http://open-std.org/JTC1/SC22/WG21/docs/papers/2016/p0035r4.html
     */
    Bulky *bp = new Bulky;
    std::cout << is_aligned(bp) << '\n';
    delete bp;

    // Supported by GCC, Clang, MinGW, MSVC
    auto *q = _mm_malloc(sizeof(float), 16u);
    ptr_print(q);
    _mm_free(q);

#if defined(_MSC_VER) || defined(__MINGW32__)
    auto *r = _aligned_malloc(sizeof(float), 16u);  // GCC and Clang doesn’t support this
    ptr_print(r);
    _aligned_free(r);
#endif

    // custom implementation
    auto *s = alloc_aligned<float>(1, 16u);
    ptr_print(s);
    free_aligned(s);

    // Making std::vector allocate aligned memory using custom allocators
    // http://stackoverflow.com/q/8456236
    // http://stackoverflow.com/q/12942548
}
