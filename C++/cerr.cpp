#include <iostream>
#include <stdexcept>

/*
 * Three files are always open[0]
 *
 *     FD | Stream |   Sink
 *    ----|--------|----------
 *     0  | STDIN  | keyboard
 *     1  | STDOUT |  screen
 *     2  | STDERR |  screen
 *
 * Although STDERR and STDOUT are directed to screen (terminal), they're very much different streams, which are copied
 * to the screen by default. They can be separately redirected to different files.
 *
 * Redirect STDERR to the file that STDOUT is _currently_ going to:
 *     $ ./cerr >some_file 2>&1
 * Redirect both to different files:
 *     C:\> cerr >out.log 2>err.log
 *
 * In the latter we first redirect STDOUT to the file first and STDERR to STDOUT; see [here][1] to know why the order
 * matters. Redirect a stream to NUL using the FD (file descriptor) nul on Windows[2] and /dev/null on Linux. All the
 * above applied to both Windows and Linux uniformly. Also cin, cout and cerr are the C++ counterparts of the above.
 *
 * PIPE (Linux-only)
 * Piping works by redirecting cmd1's STDOUT to cmd2's STDIN. See [here][3] to redirect both STDOUT and STDERR of cmd1
 * to cmd2's STDIN; see [here][4] for just redirecting cmd1's STDERR to cmd2's STDIN. Refer to err_redir.sh tryout for
 * detailed Bash test cases.
 *
 * [0]: http://www.tldp.org/LDP/abs/html/io-redirection.html
 * [1]: http://stackoverflow.com/a/25259518/183120
 * [2]: https://support.microsoft.com/en-in/kb/110930
 * [3]: http://unix.stackexchange.com/q/24337/30580
 * [4]: http://stackoverflow.com/q/2342826/183120
 *
 */

int main()
{
    std::cout << "Hello, world!\n";
    std::cerr << "Error, world!\n";

    // VC++ compiler show nothing for the exception when linked to libcmt.lib
    throw std::runtime_error("Exception, world!");
    // cl /EHsc /c /MTd cerr.cpp
    // link /out:"cerr.exe" /debug /nodefaultlib:libcmt.lib libcmtd.lib cerr.obj
    // should show a message box without the what() message or the line number
}
