// g++ -Wall -std=c++17 -pedantic{,-errors} no_dtor.cpp

#include <iostream>
#include <type_traits>

// Non-trivially destructible
struct A
{
  int x = 1;  // non-trivial as ctor isn't default due to `x = 1`
  ~A() { }    // = default or not declaring will make A trivially destructible
};

// Indestructible
struct B
{
    int x;

    // This allows placement-new only; no default/aggregate construction.
    ~B() = delete;
    // If all members are trivially-destructible and this class has default dtor
    // it’s implicitly trivially-destructible.  Otherwise dtor can be explicitly
    // deleted to indicate that no smarts are involved in destruction.

    // B() = delete;
    // Uncomment to render this class impossible to construct (C++20).
    // https://stackoverflow.com/a/52080879/183120
};

// Trivial Destructible
struct C
{
  int x;
};

// Trivially Destructible
struct D
{
  ~D() = default;
  C c;
  int x;
};

// If object destruction is just memory deallocation, it’s suitable for arena or
// slab allocation -- objects created at different times are destroyed in
// one-shot by clearing the whole arena/slab.
// https://news.ycombinator.com/item?id=33408804
template <typename T>
constexpr bool can_arena_allocate() {
  return std::is_trivially_destructible_v<T> || !std::is_destructible_v<T>;
}

template <typename T>
void print() {
  std::cout << "  Destructible? " << std::is_destructible_v<T> << '\n'
            << "  Trivially? " << std::is_trivially_destructible_v<T> << '\n'
            << "  Standard Layout? " << std::is_standard_layout_v<T> << '\n'
            << "  Trivial? " << std::is_trivial_v<T> << '\n'
            << "  Can arena allocate? " << can_arena_allocate<T>() << '\n';
  if constexpr (can_arena_allocate<T>()) {
    char mem[sizeof(T)];
    T* p = new (&mem) T;
    p->x = 1;
    std::cout << "    p->x = " << p->x << "\n";
    // no need to delete; clearing the backing memory is enough
  }
}

int main()
{
  std::cout << std::boolalpha << "A\n";
  print<A>();
  std::cout << "\nB\n";
  print<B>();
  std::cout << "\nC\n";
  print<C>();
  std::cout << "\nD\n";
  print<D>();
}
