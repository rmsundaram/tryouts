#include <iostream>
#include <initializer_list>

using std::cout;
using std::endl;

class B
{
public:
    B() = delete;
	
	B(std::initializer_list<int> il)
	{
		b = *il.begin();
	}

	template <typename T>
	B(std::initializer_list<T> il) = delete;
    
    B(const B&)
    {
        cout << "copy ctor" << endl;
    }
	
	B& operator=(const B&) = default;

private:
	int b;
};

struct Point {
		int x,y;
		constexpr Point(int xx, int yy) : x(xx), y(yy) { }
		constexpr int getX() { return x; }
	};

int main()
{
	B b{1};
	constexpr Point cOrigo(1, 1);
	constexpr int a[cOrigo.getX()] = {1};
	constexpr int h = a[0];
	int x = 1;
	/*constexpr*/ Point origo(x,x+1);
	origo.getX();
}
