#include <iostream>
#include <set>
#include <tuple>
#include <algorithm>

struct Edge
{
    unsigned from;
    unsigned to;
};

// Comparator using the stock lexicographical_compare; creating a tuple, not an array, on-the-fly to tackle cases
// where the edges differ only by cost, which has a different type e.g. crossing a river by swimming vs rafting.
bool operator<(Edge const &l, Edge const &r) {
    // a tie is a tuple of lvalue references; since we know l and r would outlive the references we
    // use it to avoid copying.  For primitives copying is however better; this is just demonstration.
    // Another good use of std::tie is when receiving from a function returning multiple values; see std::ignore.
    return std::tie(l.from, l.to) < std::tie(r.from, r.to);
}

bool operator<(Edge const &e, unsigned fromNode) {
    return e.from < fromNode;
}

bool operator<(unsigned fromNode, Edge const &e) {
    return fromNode < e.from;
}

std::ostream& operator<<(std::ostream &os, Edge const &e) {
    return os << e.from << " --> " << e.to;
}

int main() {
    std::set<Edge> s{ {0, 4}, {2, 6}, {1, 2}, {1, 3}, {2, 4}, {3, 4}, {4, 1}, {1, 2} };
    for (auto const &e : s) {   //                                            ^^^^^^
        std::cout << e << '\n'; //                                          duplicate
    }                           // doesn’t get inserted
    std::cout << "Listing elements starting from 1:\n";
    auto itPair = std::equal_range(s.cbegin(), s.cend(), 1);
    while (itPair.first != itPair.second)
        std::cout << *itPair.first++ << '\n';
    std::cout << '\n';

    // in a multiset, if we take only ‘from’ as key, dupes show up
    // otherwise, the performance guarantees seem similar for both data structures
    using EdgeWithoutComparator = unsigned;
    std::multiset<EdgeWithoutComparator> ms { 0, 2, 1, 1, 2, 3, 4, 1 };
    for (auto const &e : ms) {
        std::cout << e << '\n';
    }
    std::cout << "Listing elements starting from 1:\n";
    auto ar = std::equal_range(ms.cbegin(), ms.cend(), 1);
    while (ar.first != ar.second)
        std::cout << *ar.first++ << '\n';
}
