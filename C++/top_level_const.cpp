// http://stackoverflow.com/a/37717304/183120
// http://stackoverflow.com/questions/2253738/c-typedef-interpretation-of-const-pointers
// http://stackoverflow.com/questions/3438125/non-const-pointer-argument-to-a-const-double-pointer-parameter
// http://stackoverflow.com/questions/5248571/is-there-const-in-c/5249001#5249001
// http://stackoverflow.com/a/24434872/183120
// §4.4.4, N3337

#include <iostream>

/*
 * Typedef-name don't define new types (only aliases to existing ones), but they are "atomic" in a sense that
 * any qualifiers (like const) apply at the very top level, i.e. they apply to the entire type hidden behind
 * the typedef-name. Once you defined a typedef-name, you can't "inject" a qualifier into it so that it would
 * modify any deeper levels of the type.
 */

typedef char* PCHAR;

PCHAR const cpc = nullptr;
const PCHAR pcc = nullptr;      // same as above i.e. char* const

typedef PCHAR const CPCHAR1;    // const pointer to character
typedef const PCHAR CPCHAR2;    // const pointer to character, not pointer to const character (PCCHAR)

/*
 * C++ Templates: The Complete Guide, §About This Book recommends putting const after the entity that is constant to
 * answer the question "what is const?" it's always what is just before the const qualifier. int const * const p;
 * pointer is a constant and integer is also a constant; although int const a; has the alternative const int a; there's
 * no such alternative for int *const p; where const can be put in front *, thus having the const after the entity gives
 * uniformity.  Secondly, syntactical substitution that is common when dealing with templates i.e. for typedefs having
 * const before is misleading for e.g. const PCHAR may seem to be const char* for someone who's imagining textual
 * replacement while in truth it's only char *const irrespective of where const is put w.r.t PCHAR.
 */

/*
 * When having pointers of multiple levels, the top-most level is the level of the variable i.e. int ***p;
 * the third (from left) star is the (top-most) level of p (it's a pointer to a pointer to a pointer to an int), the
 * preceding levels are lower, deeper levels. This is the reason typedef-ing such a type and applying const to that
 * type leads to const applied to the top most level as explained above.
 *
 * When defining functions having pointer parameters we additionally qualify them as const to guarantee no edits to the
 * object (e.g. strcmp) thus allowing both const and non-const pointer arguments to be passed; since non-const pointers
 * get implicitly converted to a const pointer this is allowed BUT the implicit conversion to const is only for the
 * top − 1 level, this can be done to any level provided the levels above it, except for top-most, are all const too
 * e.g. const char *ch works because barring the top-most level there're no levels and thus adding const to the next
 * level works. T* is implicitly converted to T const *; if T is also a pointer this rule can be applied recursive only
 * in C++ where one can add const-qualification at any depth of indirection, as long as const-qualification is added all
 * the way to the top level.
 *
 *       int **X = nullptr;
 *       int *const *Y = X;          // OK X converted to int *const *
 *       int const *const *Z = X;    // OK X converted to int *const * which is then converted to int const *const *
                                     // since target (assignee's) type has const all the way to the top
 *       int const **W = X;    // Error int *const * OR int const *const * cannot be converted to int const ** since
 *                             // higher levels are not const
 *
 * However, passing an argument of char** to a parameter of const char** doesn't work due the above reason that the
 * level before char (bottom − 1) OR level after the top-most (top + 1) is not a const. The reason for disallowing such
 * a conversion is it prevents a loop-hole in the type system e.g.
 *
 *        const int i = 1;
 *        int *p;
 *        const int **pp = &p;   // this is rightly disallowed based on the above rule by the language standard
 *        *pp = &i;
 *        *p = 10;               // oops!!!
 *
 * In all this, the top-level cv-qualifiers are irrelevant and have no role to play.
 */

void process(char const *
             /*const*/           // optional here since this is the top-most level */
             p)
{
    std::cout << +*p << std::endl;
}

// the deepest const would be allowed only if the previous higher levels were const too
// alternatively void process(char * const * pp) is allowed i.e. the const can proceed only level by level
// from right to left (top to bottom), left-most (bottom-most) requiring all the right (upper) ones to be const
void process(char const *const *pp)
{
    std::cout << +**pp << std::endl;
}

// here removing const from bottom-most or even both bottom-most and bottom − 1 levels would work, but removing
// bottom-most and top+1 wouldn't work since for the bottom − 1 level const, the top + 1 const should've been there
void process(char const *const *const *ppp)
//             ^        ^      ^      ^
//         bottom  bottom−1  top+1   top
// bottom-2 can also be written as top+1 imaging top = 0 and bottom = n, where n = number of pointers
{
    std::cout << +***ppp << std::endl;
}

int main()
{
    char c = '\2';
    char *p = &c;
    char **pp = &p;
    process(&c);
    process(&p);
    process(&pp);
}

// Reading types in C and C++
//   Use Right-Left Rule to read complicated types [1].
//   ‘char const * const * argv’ means argv is a pointer to a constant pointer to a const character [2].
//
// References
// 1. Right-Left Rule to read C++ declarations: https://stackoverflow.com/a/31789426/183120
// 2. Meaning of two consts in C++ type: http://stackoverflow.com/q/18972943/183120
