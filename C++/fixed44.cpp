// g++ -Wall -std=c++20 -pedantic -pedantic-errors -o fp fixed44.cpp
//
// REFERENCES
//
// 1. Essential Math, 2/e, Appendix C: More on Real-World Computer Number Representation
// 2. What’sACreel
//      https://www.youtube.com/watch?v=S12qx1DwjVk
//      https://www.youtube.com/watch?v=npQF28g6s_k
// 3. OneLoneCoder: https://www.youtube.com/watch?v=ZMsrZvBmQnU
// 4. https://jet.ro/files/The_neglected_art_of_Fixed_Point_arithmetic_20060913.pdf
// 5. https://stackoverflow.com/q/79677/183120
// 6. https://stackoverflow.com/a/6725981/183120
//
// SEE ALSO
//
// 1. http://www.digitalsignallabs.com/fp.pdf
// 2. https://mikelankamp.github.io/fpm/

#include <cstdint>
#include <bit>
#include <cmath>
#include <iostream>

// M.N → M integral bits + N fractional bits
// Scaling factor: 2⁻ⁿ for M.N fixed-point (or even integers e.g. u16 is 16.0)
// Range and Precision
//   Integral Range: [-2^(m-1), 2^(m-1) - 2⁻ⁿ]
//   Precision (distance b/w successive values i.e. ulp): 2⁻ⁿ
// Absolute error: ½ × 2⁻ⁿ
// `fixed44` doesn’t have any trap representations [6]; no NaN, Inf, etc.
// Divide by zero results in the usual SIGFPE/crash.
// Refer fixed.cpp for a templatized fixed-point type taking M and N bits.

// Signed 4.4 fixed point type
// Scaling factor: 1/16 for fixed44
// Range and Precision (ulp): [-8, 7.9375]; 0.0625
// Absolute error: 0.03125
//
// Elements of fixed44
//
// -8.0     -7.9375  -7.8750  -7.8125  -7.7500  -7.6875  -7.6250  -7.5625
// -7.5000  -7.4375  -7.3750  -7.3125  -7.2500  -7.1875  -7.1250  -7.0625
// -7.0     -6.9375  -6.8750  -6.8125  -6.7500  -6.6875  -6.6250  -6.5625
// -6.5000  -6.4375  -6.3750  -6.3125  -6.2500  -6.1875  -6.1250  -6.0625
// -6.0     -5.9375  -5.8750  -5.8125  -5.7500  -5.6875  -5.6250  -5.5625
// -5.5000  -5.4375  -5.3750  -5.3125  -5.2500  -5.1875  -5.1250  -5.0625
// -5.0     -4.9375  -4.8750  -4.8125  -4.7500  -4.6875  -4.6250  -4.5625
// -4.5000  -4.4375  -4.3750  -4.3125  -4.2500  -4.1875  -4.1250  -4.0625
// -4.0     -3.9375  -3.8750  -3.8125  -3.7500  -3.6875  -3.6250  -3.5625
// -3.5000  -3.4375  -3.3750  -3.3125  -3.2500  -3.1875  -3.1250  -3.0625
// -3.0     -2.9375  -2.8750  -2.8125  -2.7500  -2.6875  -2.6250  -2.5625
// -2.5000  -2.4375  -2.3750  -2.3125  -2.2500  -2.1875  -2.1250  -2.0625
// -2.0     -1.9375  -1.8750  -1.8125  -1.7500  -1.6875  -1.6250  -1.5625
// -1.5000  -1.4375  -1.3750  -1.3125  -1.2500  -1.1875  -1.1250  -1.0625
// -1.0     -0.9375  -0.8750  -0.8125  -0.7500  -0.6875  -0.6250  -0.5625
// -0.5000  -0.4375  -0.3750  -0.3125  -0.2500  -0.1875  -0.1250  -0.0625
//  0.0      0.0625   0.1250   0.1875   0.2500   0.3125   0.3750   0.4375
//  0.5000   0.5625   0.6250   0.6875   0.7500   0.8125   0.8750   0.9375
//  1.0      1.0625   1.1250   1.1875   1.2500   1.3125   1.3750   1.4375
//  1.5000   1.5625   1.6250   1.6875   1.7500   1.8125   1.8750   1.9375
//  2.0      2.0625   2.1250   2.1875   2.2500   2.3125   2.3750   2.4375
//  2.5000   2.5625   2.6250   2.6875   2.7500   2.8125   2.8750   2.9375
//  3.0      3.0625   3.1250   3.1875   3.2500   3.3125   3.3750   3.4375
//  3.5000   3.5625   3.6250   3.6875   3.7500   3.8125   3.8750   3.9375
//  4.0      4.0625   4.1250   4.1875   4.2500   4.3125   4.3750   4.4375
//  4.5000   4.5625   4.6250   4.6875   4.7500   4.8125   4.8750   4.9375
//  5.0      5.0625   5.1250   5.1875   5.2500   5.3125   5.3750   5.4375
//  5.5000   5.5625   5.6250   5.6875   5.7500   5.8125   5.8750   5.9375
//  6.0      6.0625   6.1250   6.1875   6.2500   6.3125   6.3750   6.4375
//  6.5000   6.5625   6.6250   6.6875   6.7500   6.8125   6.8750   6.9375
//  7.0      7.0625   7.1250   7.1875   7.2500   7.3125   7.3750   7.4375
//  7.5000   7.5625   7.6250   7.6875   7.7500   7.8125   7.8750   7.9375

struct fixed44
{
  int8_t v;

  constexpr fixed44() : v{} {}
  constexpr fixed44(uint8_t bits) : v {std::bit_cast<int8_t>(bits)} { }

  // Add (fixed44 space) 0.5 and return integer part for nearest int.  Use next
  // integer type as rounding would need that e.g. round({7.9375}) = 8
  constexpr int16_t round() const {
    constexpr auto HALF = 1 << 3;
    return (v + HALF) >> 4;
  }

  // Just shifting won’t work for -ve values; negate, shift and add sign.
  constexpr int8_t trunc() const { return (v < 0) ? -(-v >> 4) : (v >> 4); }

  constexpr fixed44 operator+(fixed44 other) const {
    other.v += v;
    return other;
  }

  constexpr fixed44 operator-(fixed44 other) const {
    other.v = v - other.v;
    return other;
  }

  constexpr fixed44 operator*(fixed44 other) const {
    int16_t x = v;
    int16_t y = other.v;
    int16_t r = x * y;
    r >>= 4;
    fixed44 f;
    f.v = static_cast<int8_t>(r);
    return f;
  }

  // No special handling of divide by zero; does what regular integer divide by
  // zero does on your platform + toolchain.
  constexpr fixed44 operator/(fixed44 other) const {
    int16_t r = v;
    r <<= 4;
    r /= other.v;
    fixed44 f;
    f.v = static_cast<int8_t>(r);
    return f;
  }

  constexpr fixed44& operator++() {
    v += 1 << 4;
    return *this;
  }

  constexpr fixed44 operator++(int) {
    fixed44 r{*this};
    ++*this;
    return r;
  }

  constexpr fixed44& operator--() {
    v -= 1 << 4;
    return *this;
  }

  constexpr fixed44 operator--(int) {
    fixed44 r{*this};
    --*this;
    return r;
  }
};

// undefined if |d| is beyond fixed44’s range
constexpr fixed44 FromDouble(double d) {
  fixed44 f;
  // Bump magnitude by 0.5 in integer space to get std::round behaviour from
  // shift which really truncates i.e rounds towards 0.
  f.v = static_cast<int8_t>(d * double(1 << 4) + ((d >= 0) ? 0.5 : -0.5));
  return f;
}

std::ostream& operator<<(std::ostream& os, fixed44 f) {
  uint8_t v = (f.v < 0) ? -f.v : f.v;
  if (f.v < 0) os << '-';

  const int16_t part_i = v >> 4;
  constexpr auto MASK = (1 << 4) - 1;  // 0b1111
  uint16_t part_f = v & MASK;

  constexpr uint16_t least = (5 * 1000) >> 3;  // 0.5 * 10⁴ / 2³
  uint16_t fract = 0;
  for (auto i = 0u; i < 4; ++i) {
    fract += (least << i) * (0x1 & part_f);
    part_f >>= 1;
  }
  // Append 0 since 0000.0001 → 0.0625; not .625
  if (fract == 625)
    return os << part_i << ".0" << fract;
  else
    return os << part_i << '.' << fract;
}

int main() {
  fixed44 l { 0b1000'0000 };
  fixed44 u { 0b0111'1111 };
  std::cout << l << '\t' << +l.trunc() << '\t' << l.round() << '\n';
  std::cout << u << '\t' << +u.trunc() << '\t' << u.round() << '\n';

  auto x = FromDouble(-2.76);
  std::cout << x << '\t' << +x.trunc() << '\t' << x.round() << '\n';

  auto y = FromDouble(1.93);
  std::cout << y << '\t' << +y.trunc() << '\t' << y.round() << '\n';
  auto z = x * y;
  std::cout << z << '\n';

  // TODO: Write tests.
}
