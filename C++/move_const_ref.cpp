// g++ -Wall -std=c++17 -pedantic move_const_ref.cpp

#include <iostream>
#include <vector>
#include <utility>

void f(const std::vector<int> &v) {
  // C++ gotcha: std::move(v) returns ‘const T&&’ which gets implicitly
  //             converted to ‘const T&’ thus copying v to my_v! 🤦
  // https://stackoverflow.com/q/38917130/183120
  // “Copyable is a subset of the concept Movable.  Everything that is
  //  Copyable automatically is also Movable, even if the thing that
  //  C++ refers to as a move is actually implemented just as a copy.”
  std::vector<int> my_v = std::move(v);

  // prints 3 elements: OK
  for (auto i : my_v) {
    std::cout << i << '\n';
  }

  // shouldn’t print anything if moved, but prints!
  for (auto i : v) {
    std::cout << i << '\n';
  }
}

int main()
{
  std::vector<int> v = {1, 2, 3};
  f(std::move(v)); // works since ‘T&&’ can be bound to ‘const T&’
  // v is empty here!
}
