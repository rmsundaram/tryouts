#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <algorithm>
#include <cctype>

#include <functional>

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::string;

// convert a given file into a header file with its contents as a byte array of hex values
int main(int argc, char const *argv[])
{
    if (argc > 2)
    {
        ifstream fIn(argv[1], std::ios_base::binary);
        unsigned const columns = (argc >= 4 && argv[3]) ? std::max(strtol(argv[3], nullptr, 0), 1L) : 4u;

        if (fIn.is_open())
        {
            string outFileName(argv[1]);
            outFileName.append(".h");
            ofstream fOut(outFileName.c_str());
            if (fOut.is_open())
            {
                // TODO: need to remove everything before '/' if one exists
                // http://stackoverflow.com/q/7131858/183120
                std::transform(outFileName.cbegin(), outFileName.cend(), outFileName.begin(),
                               static_cast<int(*)(int)>(std::toupper));
                std::replace(outFileName.begin(), outFileName.end(), '.', '_');

                // headers
                fOut << "#ifndef _" << outFileName << "_" << endl;
                fOut << "#define _" << outFileName << "_" << endl << endl;
                fOut << "char const " << argv[2] << "[] = {" << endl;
                fOut << std::hex;
                fOut << std::setfill('0');

                size_t uCounter = 0;
                unsigned char ch;
                // don't skip whitespace
                fIn >> std::noskipws;
                // The idiomatic way of reading from ifstream; references:
                // [0]: https://stackoverflow.com/a/5605161/183120
                // [1]: https://stackoverflow.com/q/7868936/183120
                while (fIn >> ch)
                {
                    fOut << "0x" << std::setw(2) << static_cast<unsigned>(ch) << ',';
                    fOut << (((++uCounter % columns) == 0) ? '\n' : ' ');
                }

                // footers
                if ((uCounter % columns) != 0)
                {
                    fOut.seekp(-2, std::ios_base::cur);
                    fOut << '\n';
                }
                else
                {
                    // TODO: remove both '\n' and ',' before it - but do it in a platform-independent way
                }
                fOut << "};\n\n" << "#endif /* _" << outFileName << "_ */" << endl;
            }
            else
            {
                cout << "Error: Unable to write to " << argv[1] << endl;
            }
        }
        else
        {
            cout << "Error: Unable to read from " << argv[1] << endl;
        }
    }
    else
    {
        cout << "usage: bin2c <binary_file> <string_literal_name> [columns]" << endl;
    }
}
