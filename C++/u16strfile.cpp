﻿// g++ -Wall -std=c++20 -pedantic{,-errors} wstrfile.cpp

#include <iostream>
#include <fstream>

int main()
{
  // Avoid using wchar_t (and wstring) since its width is implementation-defined
  // and is usually 16 bits on Windows and 32 bits otherwise.  Even if you only
  // use BMP, using char16_t and u16string is better since they’re well-defined.
  // https://stackoverflow.com/q/19068748/183120
  // https://stackoverflow.com/q/4588302/183120
  std::u16string str = u"அன்பு\n";

  using uofstream = std::basic_ofstream<char16_t>;
  {
    uofstream ofs("myfile.bin",
                  std::ios_base::out |
                  std::ios_base::trunc |
                  std::ios_base::binary);
    ofs << str;
  }
  std::cout << str.length() << " codeunits\n";
  std::cout << str.length() * sizeof(char16_t) << " bytes \n";
}
