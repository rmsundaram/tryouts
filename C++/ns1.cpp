#include <iostream>

namespace Animal
{
	int a = 1;
	namespace Cat
	{
		int a = 2;
		namespace Animal
		{
			int a = 3;
			void print()
			{
				std::cout << __PRETTY_FUNCTION__ << std::endl;
				std::cout << ::Animal::a << std::endl;
				std::cout << ::Animal::Cat::a << std::endl;
				std::cout << a << std::endl;
			}
		}
		void print()
		{
			std::cout << __PRETTY_FUNCTION__ << std::endl;
			std::cout << ::Animal::a << std::endl;
			std::cout << a << std::endl;
			std::cout << Animal::a << std::endl;
		}
	}
	void print()
	{
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		std::cout << a << std::endl;
		std::cout << Cat::a << std::endl;
		std::cout << Cat::Animal::a << std::endl;
	}
}

int main()
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	// however, all these are global variables only
	// but hidden from global namespace in subnamespaces
	std::cout << Animal::a << std::endl;
	std::cout << Animal::Cat::a << std::endl;
	std::cout << Animal::Cat::Animal::a << std::endl;

	Animal::print();
	Animal::Cat::print();
	Animal::Cat::Animal::print();
}

