#include <cstdio>

#include <semaphore.h>
#include <sys/stat.h>
#include <unistd.h>

// Problem with named semaphore approach to single-instance applications in
// POSIX OSes is clean-up when app crashes i.e. if sem_unlink isn’t run, no
// instance of the app can be run in future until restart.  Unlike Windows,
// POSIX OSes, implement semaphore in user-space.  Shared memory doesn’t suffer
// from this problem.
// https://stackoverflow.com/q/5339200#comment49114610_19749583
// https://stackoverflow.com/a/71828312/183120

int main() {
    const char name[] = "/hello";
    auto s = sem_open(name, O_CREAT | O_EXCL, S_IRUSR, 1);
    if (s != SEM_FAILED) {
      fprintf(stderr, "Sleeping...\n");
      sleep(5);
      sem_close(s);        // optional
      sem_unlink(name);    // needed
      fprintf(stderr, "Closed semaphore\n");
    } else {
      fprintf(stderr, "Failed to get semaphore\n");
    }
}
