#include <iostream>
#include <cstring>
#include <vector>
#include <stdexcept>
#include <cassert>
#include <iterator>

// bash one-liner
// bash -c 'expr $(echo -n $0 | tr -s "01" | wc -c) - 1 + ${0:0:1}' 000111101
// perl one-line
// perl -pe
//  'shift; chomp; tr/01/01/s; $_ = length() - 1 + substr($_, 0, 1)' <<< 1100

// This function expects a C-string made of '0' or '1' characters.  It returns
// the number of distinct ranges of 0s and 1s in the input, excluding the
// leading range of 0s, if present.
size_t flip_count(char const* pattern)
{
    size_t flips = 0u;
    char current = 0;           // start with 0 as current to skip leading 0s
    for (; pattern && *pattern; ++pattern) {
#ifndef USE_BRANCH
        bool const changed = current ^ (*pattern - '0');
        flips += changed;       // increment if this is a new range start
        current ^= changed;     // flip current, if changed
#else
        if (current != *pattern) {
            current = *pattern;
            ++flips;
        }
#endif  // USE_BRANCH
    }
    return flips;
}
// This function also happens to be Google Code Jam 2016's Qualification Round,
// Problem B's solution.  However, this was not arrived at it in that route.
// http://code.google.com/codejam/contest/6254486/dashboard#s=p1

bool flip(std::vector<bool> &list, size_t from, size_t *next)
{
    *next = from;
    bool found = false;
    if (from >= list.size())
        throw std::runtime_error("Error: access out of bounds");
    for (; from < list.size(); ++from) {
        list[from] = list[from] ^ 1;
        found = found || list[from];
        if (!found) ++*next;
    }
    return found;
}

std::ostream& operator<< (std::ostream& os, std::vector<bool> const &v)
{
    std::ostream_iterator<bool> it(os);
    std::copy(std::cbegin(v), std::cend(v), it);
    return os;
}

int main(int argc, char **argv)
{
    if (argc > 1) {
        size_t const n = std::strlen(argv[1]);
        std::vector<bool> pattern(n, false);
        size_t flips = 0u;
        size_t next = 0u;
        bool found = false;
        for (auto i = 0u; i < n; ++i) {
            bool const bit = argv[1][i] - '0';
            pattern[i] = bit;
            found = found || bit;
            if (!found) ++next;
        }
        std::cout << "0: " << pattern << '\n';
        while (found) {
            found = flip(pattern, next, &next);
            ++flips;
            std::cout << flips << ": " << pattern << '\n';
        }
        std::cout << flips << " flip(s)\n";
        assert(flip_count(argv[1]) == flips);
        flip_count(nullptr);
    }
    else
        std::cout << "usage: flip <bit-pattern>\n";
}
