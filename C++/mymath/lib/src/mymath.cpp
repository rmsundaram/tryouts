#include "mymath.h"

#include <cmath>

void sanitize(float* f) {
  if (!std::isfinite(*f))
    *f = 0.0f;
}

MYMATH_API auto add(int x, int y) -> int {
  return x + y;
}

MYMATH_API auto sub(float x, float y) -> float {
  sanitize(&x);
  sanitize(&y);
  return x - y;
}
