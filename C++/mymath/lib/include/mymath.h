#include "mymath_export.h"

extern "C" {

MYMATH_API int add(int x, int y);

MYMATH_API float sub(float x, float y);

}  // extern "C"
