#include "mymath.h"

#include <string>
#include <iostream>

int main(int argc, char** argv) {
  if (argc < 4) {
    std::cout << "Usage: mycalc +/- NUM1 NUM2";
    return 0;
  }
  if (argv[1][0] == '+') {
    const int x = static_cast<int>(std::strtol(argv[2], nullptr, 0));
    const int y = static_cast<int>(std::strtol(argv[3], nullptr, 0));
    std::cout << add(x, y) << '\n';
  }
  else if (argv[1][0] == '-') {
    const float x = std::strtof(argv[2], nullptr);
    const float y = std::strtof(argv[3], nullptr);
    std::cout << sub(x, y) << '\n';
  }
  else
    std::cout << "Operation unknown: " << argv[1][0] << '\n';
}
