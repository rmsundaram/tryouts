#include <iostream>
#include <limits>
#include <cstdint>
#include <cassert>

// http://www.ibm.com/developerworks/library/pa-dalign/
// also see read_words.lua
// this is a very simple case of the change-making knapsack problem

template <typename T>
bool get_bit(T n, uint8_t b) {
    auto const mask = T(1U) << b;
    return n & mask;
}

int main(int argc, char *argv[]) {
    if (argc < 2)
        return -1;

    auto const n = strtoull(argv[1], nullptr, 0);
    unsigned long constexpr max = 4u; // assume 32-bit processor that can read a DWORD in one gulp
    // http://www.exploringbinary.com/ten-ways-to-check-if-an-integer-is-a-power-of-two-in-c/
    static_assert(max && ((max & (~max + 1)) == max), "Error: processor word not a power of two!");

    // E.g. n = 67 bytes, max_read = 4 bytes
    // 64 32 16 8 4 2 1
    // 1  0  0  0 0 1 1
    // On a 32-bit machine there'll be some k 32-bit/4-byte reads followed by reads of lesser denomination each eactly
    // following the bit pattern from the max_read's MSB to LSB. In this case, 16 × 32-bit reads, one 16 and 8-bit reads
    // the obvious way to find k is n / max_read

    // bit-twiddling way
    unsigned long b = 0u;
    do
    {
        if (get_bit(n, b))
            std::cout << "1 × " << (1UL << b) << "-byte read\n";
        ++b;
    }
    while ((1UL << b) < max);
    auto const m = n >> b;
    if (m)
        std::cout << m << " × " << (1UL << b) << "-byte " << ((m > 1) ? "reads\n" : "read\n");
}
