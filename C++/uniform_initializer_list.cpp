#include <initializer_list>
#include <iostream>

using namespace std;

struct A
{
	int x;

	A(int x) : x(x)
	{
	}

	A(const std::initializer_list<int> &) : x(0)
	{
	}

	A(const A&) = delete;
	A(const A&&) = delete;
};

struct B
{
	int x;

	// ctor 1
	B() : x(0)
	{
	}

	// ctor 2
	B(int i, float f, char c) : x(1)
	{
	}

	// ctor 3
	B(const std::initializer_list<int> &) : x(2)
	{
	}
};

int main()
{
	A a1(1);
	A a2{1};		// Call ctor 2 for both b{1, 2} and b{1, 2, 3} since {} matches initializer-list which gets higher
                    // priority than other overloads; in the absense of ctor 2, ctor 1 would get called for latter
	cout << a1.x << "\t" << a2.x << endl;
	B b1 = {1, 2, 3};	// b.x = 2
	B b2 = { };			// b.x = 0
	B b3 = {1, .5f, 'a'}; // b.x would’ve been 1 if not for ctor 3; now ctor 3 called with warnings for narrowing float
	B b4(1, .5f, 'x');
	B b5 = (1, .5f, 'x');
    // This is the old comma operator playing, where 1 and 0.5f are discarded and only 'x' is assigned to B, since B
    // doesn't have a ctor taking a char, this fails; had the parenthesis been braces, then it'd pick ctor 3 had the
    // type been an aggregate type -- an array or a class (Clause 9) with no user-provided constructors (12.1), no
    // private or protected non-static data members (Clause 11), and  no virtual functions (10.3);
    // brace-or-equal-initializers for non-static data members are allowed since C++14, public, non-virtual base classes
    // are allowed since C++17 -- there wouldn't've been any ctors, hence the elements in the braces would initialize
    // the members in order of declaration in the struct/class - refer aggregate_class.cpp.
	cout << b3.x << endl;
}
