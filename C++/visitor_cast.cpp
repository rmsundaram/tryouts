// Shared at https://coliru.stacked-crooked.com/a/b02e9aeba65b0d02

#include <iostream>
#include <memory>
#include <vector>

// RTTI may be disabled at times for size or speed; it skips adding code for
// runtime type facilities including dynamic_cast.  Exceptions may also need
// some RTTI info but controlled by a separate switch.  Try disabling exceptions
// (-fno-exceptions or /GX-) too.  Actual disk footprint gain depends on the
// toolchain and OS.  Actual speed gain of VisitCast?  Measure!

// See Also
// 1. https://www.foonathan.net/2017/12/visitors
// 2. Hands-On Design Patterns with C++ by Fedor G. Pikus
//    Chapter 18: Visitor and Multiple Dispatch

// GCC: g++ -fno-rtti cast_visitor.cpp
// MSVC: cl /EHsc /GR- cast_visitor.cpp

#if !defined(__GXX_RTTI) && !defined(_CPPRTTI)
#  define NO_RTTI
#endif

#ifdef NO_RTTI
struct Tool;
struct Spanner;
struct Hammer;

struct Visitor
{
  virtual ~Visitor() = default;

  virtual void visit(Spanner&) { }
  virtual void visit(Hammer&) { }
};

#  define ACCEPT_VISITOR_IF_NO_RTTI_DECL virtual void accept(Visitor&) = 0;
#  define ACCEPT_VISITOR_IF_NO_RTTI      void accept(Visitor& v) override { \
                                           v.visit(*this);                  \
                                         }
#else
#  define ACCEPT_VISITOR_IF_NO_RTTI_DECL
#  define ACCEPT_VISITOR_IF_NO_RTTI
#endif // NO_RTTI

struct Tool
{
  virtual ~Tool() = default;

  ACCEPT_VISITOR_IF_NO_RTTI_DECL
};

struct Spanner : Tool
{
  ACCEPT_VISITOR_IF_NO_RTTI
};

struct Hammer : Tool {
  ACCEPT_VISITOR_IF_NO_RTTI

  const char* strike() { return "Hammer"; }
};

#ifdef NO_RTTI
template <typename T>
struct VisitCast : Visitor
{
  static_assert(std::is_pointer_v<T>, "VisitCast needs a pointer type");
  VisitCast(Tool* t) { t->accept(*this); }
  void visit(std::remove_pointer_t<T>& t) override { m_tool = &t; }
  operator T() { return m_tool; }
  T m_tool = nullptr;
};
#endif // NO_RTTI

void use_hammer(Tool* t) {
#ifdef NO_RTTI
  if (Hammer* h = VisitCast<Hammer*>(t))
#else
  if (Hammer* h = dynamic_cast<Hammer*>(t))
#endif // NO_RTTI
    std::cout << h->strike() << '\n';
  else
    std::cout << "Not a hammer\n";
}

int main() {
  // std::variant (C++17) may be a better alternative to std::unique_ptr; use
  // std::visit in that case.
  // http://www.vishalchovatiya.com/double-dispatch-visitor-design-pattern-in-modern-cpp/
  std::vector<std::unique_ptr<Tool>> tools;
  tools.push_back(std::make_unique<Spanner>());
  tools.push_back(std::make_unique<Hammer>());
  tools.push_back(std::make_unique<Spanner>());

  for (auto& t: tools)
    use_hammer(t.get());
}
