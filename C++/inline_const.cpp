// C++17 inline const helps with having just one allocation / memory address per
// constant in one header file; all TUs will refer to this one symbol [1][4].
// Prefer this over ‘extern const’ in general, except when coding a DLL which
// another DLL links to, in which case ‘inline const’ means both DLLs have to be
// rebuilt; not so with ‘extern const’ [2].
//
// constexpr functions are implicitly inline; constexpr variables not [3].
//
// REFER
// [1]: https://stackoverflow.com/a/53898717/183120
// [2]: https://stackoverflow.com/q/2190919/53898717#comment104112414_53898717
// [3]: https://www.learncpp.com/cpp-tutorial/sharing-global-constants-across-multiple-files-using-inline-variables/
// [4]: https://stackoverflow.com/a/38043566/183120

// --------------------------- notmain.hpp --------------------------------

#pragma once

// `inline const` works too but prefer `constexpr` over `const` where possible.
// Avoid using `char*` as `sizeof` bungles up result e.g. `sizeof(TITLE)` would
// be 8, not 3 as expected.
inline constexpr char TITLE[] = "Mr";

const char* get_title();

// --------------------------- notmain.cpp ------------------------------

#include "notmain.hpp"

const char* get_title() {
  return TITLE;
}

// ------------------------------ main.cpp ------------------------------

#include <cassert>
#include <cstdio>
#include "notmain.hpp"

int main() {
  assert(TITLE == get_title());
  printf("%p\n", (const void*)TITLE);
  printf("%p\n", (const void*)get_title());
  printf("%lu\n", sizeof(TITLE));
}
