#include <iostream>

struct A
{
    void print()
    {
        std::cout << "Print\n";
    }
    void printer()
    {
        std::cout << "Printer\n";
    }
};

template <typename Fn, Fn f>
void trial()
{
    A a;
    (a.*f)();
}

int main()
{
    trial<decltype(&A::print), &A::print>();
    trial<decltype(&A::printer), &A::printer>();
}
