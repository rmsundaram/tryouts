#include <cmath>
#include <cstdlib>
#include <iostream>

int main(int argc, char **argv)
{
    if (argc < 2)
    {
        std::cerr << "Please enter a real number." << std::endl;
    }
    else
    {
        const float real = strtof(argv[1], nullptr);
        std::cout << "Real Number = " << real << std::endl;

        std::cout << "--------------------------------" << std::endl;

        // using modf - a trick mentioned in Lua PiL book
        std::cout << "Fractional Part = " << fmod(real, 1.0f) << std::endl;
        std::cout << "Integer Part = " << real - fmod(real, 1.0f) << std::endl;
        std::cout << "Precision of 3 digits = " << real - fmod(real, 0.001f) << std::endl;

        std::cout << "--------------------------------" << std::endl;

        // using C99/C++11's trunc
        std::cout << "Integer Part = " << trunc(real) << std::endl;
        std::cout << "Fractional Part = " << real - trunc(real) << std::endl;
        // this works for negative numbers too; trunc(-4.235443f) = -4f
    }
}
