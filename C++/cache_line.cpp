#include <iostream>
#include <stddef.h>
#include <new>

#if __cpp_lib_hardware_interference_size

size_t cache_line_size() {
    // C++17 constant; minimum distance between data to avoid false sharing.
    // https://stackoverflow.com/q/39680206/183120
    return std::hardware_destructive_interference_size;
}

#else
// Author: Nick Strupat
// Date: October 29, 2010
// Returns the cache line size (in bytes) of the processor, or 0 on failure
#  if defined(__APPLE__)

#include <sys/sysctl.h>
size_t cache_line_size() {
    size_t line_size = 0;
    size_t sizeof_line_size = sizeof(line_size);
    sysctlbyname("hw.cachelinesize", &line_size, &sizeof_line_size, 0, 0);
    return line_size;
}

#  elif defined(_WIN32)

#include <windows.h>
#include <memory>
size_t cache_line_size() {
    DWORD buffer_size = 0;
    GetLogicalProcessorInformation(nullptr, &buffer_size);
    auto buffer = std::make_unique<SYSTEM_LOGICAL_PROCESSOR_INFORMATION[]>(buffer_size);
    GetLogicalProcessorInformation(buffer.get(), &buffer_size);

    size_t line_size = 0;
    const size_t n = buffer_size / sizeof(SYSTEM_LOGICAL_PROCESSOR_INFORMATION);
    for (auto i = 0u; i != n; ++i) {
        if (buffer[i].Relationship == RelationCache && buffer[i].Cache.Level == 1) {
            line_size = buffer[i].Cache.LineSize;
            break;
        }
    }

    return line_size;
}

#  elif defined(__linux__)

#include <stdio.h>
size_t cache_line_size() {
    FILE * p = fopen(
      "/sys/devices/system/cpu/cpu0/cache/index0/coherency_line_size", "r");
    unsigned int i = 0;
    if (p) {
        fscanf(p, "%d", &i);
        fclose(p);
    }
    return i;
}

#  else
#    error Unrecognized platform
#  endif  // platform
#endif  // __cpp_lib_hardware_interference_size

int main() {
    std::cout << "Cache line size = " << cache_line_size() << " bytes\n";
}
