// http://barendgehrels.blogspot.com/2010/10/tag-dispatching-by-type-tag-dispatching.html
// tag dispatching by type is the one used in Boost.Geometry
// which is better than usual tag dispatching by instance (see tag_dispatch_instance.cpp)

#include <string>
#include <iostream>

struct apple
{
    double radius;

    std::string name;
    apple(std::string const& n) : name(n) {}
};

struct banana
{
    double length;

    std::string name;
    banana(std::string const& n) : name(n) {}
};

struct apple_tag {};
struct banana_tag {};

template <typename T>
struct tag {};

template <>
struct tag<apple> {
	typedef apple_tag type;
};

template <>
struct tag<banana> {
	typedef banana_tag type;
};

namespace dispatch
{
    template <typename Tag> struct eat {};

    template <> struct eat<apple_tag>
    {
        static void apply(apple const& a)
        {
          std::cout << "bite" << std::endl;
        }
    };

    template <> struct eat<banana_tag>
    {
        static void apply(banana const& b)
        {
          std::cout << "peel" << std::endl;
        }
    };
	
	template <typename T, typename U> struct eater{};
	template <> struct eater<apple_tag, banana_tag>
	{
		static void apply(const apple &appy, const banana &banny)
		{
			std::cout << appy.name << " eat " << banny.name << std::endl;
		}
	};
}

// this is the exposed interface function
// its internal implementation functions of the
// same name are wrapped inside the dispatch namespace
template <typename T>
void eat(T const& fruit)
{
    dispatch::eat<typename tag<T>::type>::apply(fruit);
}

template <typename T, typename U>
void eater(T const &fruit1, U const &fruit2)
{
	dispatch::eater<typename tag<T>::type, typename tag<U>::type>::apply(fruit1, fruit2);
}

int main()
{
    apple a("my apple");
    banana b("my banana");
    eat(a);
    eat(b);
	
	eater(a, b);

    return 0;
}
