#include <iostream>

// global scope is also a namespace; variables declared here will have global namesapce scope
// this will have external linkage, unless declared static

int confusing_var = 1;

/*
to have internal linkage, we can declare them as static or put them under an unnamed namespace
unnamed namespaces are considered superior (idiomatic C++) to static since static only applies
to variables and functions while unnamed namespace applies to (user) type declarations (union, class, struct) too;
 - http://stackoverflow.com/questions/4422507/superiority-of-unnamed-namespace-over-static
one can define and specialize members of an inline namespace as if they were also members of the enclosing namespace.

Anonymous namespaces can't specialize templates outside of the namespace block.
This is why inline namespace was introduced, although static works too. - http://stackoverflow.com/a/8460364/183120
*/

namespace
{
	// as per C++11 this will always have internal linkage - http://stackoverflow.com/a/19501842/183120
	// in C++03 it used to have external linkage; to make it internal, static was to be used
	int confusing_var = 0;
}

int main()
{
	// this always sees global confusing_var than the unnamed namespace's confusing_var
	// if the former is removed, it'll see the latter
	std::cout << ::confusing_var << std::endl;
}
