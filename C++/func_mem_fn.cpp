#include <functional>
#include <iostream>
using namespace std;

struct abc
{
	auto add(int x, int y) -> decltype(x + y)
	{
		return (x + y);
	}
};

int main()
{
    // avoid the arcane member function pointer syntax
    // int (abc::*)(int, int);    // declaration
    // obj.*mem_fn_ptr(1, 2);     // dereference using obj or its ref
    // ptr->*mem_fn_ptr(3, 4);    // dereference using obj's pointer
    // even if you've to use it, use a typedef[1]
    // Although non-member function names decay into a function pointer without
    // the & prefix to maintain backword compatibility with C (a vestige from C
    // days), member function names don't.
    // [1]: http://isocpp.org/wiki/faq/pointers-to-members#dotstar-vs-arrowstar
	function<int (abc*, int, int)> fn = mem_fn(&abc::add);
	abc a;
	cout << fn(&a, 1, 2);
}
