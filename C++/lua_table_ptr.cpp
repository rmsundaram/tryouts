#include <iostream>
#include <cmath>

// PIL book, 3rd Ed, exercise 2.6

struct table
{
        struct table *a = nullptr;
};

int main()
{
        auto num = 5.0f, den = 2.0f;
        std::cout << "5.0f % 2.0f = " << fmod(num, den) << std::endl;

        table *a = new table;   // original owning pointer
        a->a = a;
        std::cout << "Primary pointer: " << a << std::endl;
        std::cout << "Secondary pointer: "
                  << a->a->a->a->a->a->a->a->a->a << std::endl;
        // child pointer referred-to multiple times
        // in this sequence, except the first, all else are the same
        a->a->a->a->a = nullptr;
        std::cout << "Primary pointer: " << a << std::endl;
        std::cout << "Secondary pointer: " << a->a << std::endl;
        delete a;
}
