#include <vector>
#include <list>

// template template parameters
// http://stackoverflow.com/a/213811/183120

// standard containers take two parameters: value_type and allocator_type
// hence omitting A will not work
template <template<typename, typename> class U, typename T, typename A>
void f(T x, U<T, A> &c) {
}

// http://www.informit.com/articles/article.aspx?p=376878
// This is more or less the approach employed by the standard container
// adapters stack, queue, and priority_queue.
template <typename T, typename U = std::vector<T>>
void g(T x, U &c) {
}

int main() {
    std::vector<int> v;
    f(1, v);
    std::list<int> l;
    g(0, l);
}
