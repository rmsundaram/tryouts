#include <array>
#include <algorithm>
#include <string>
#include <string_view>
#include <vector>
#include <iostream>

bool is_host_known(std::string_view token) {
  const std::array<const std::string_view, 4> sites = {
    "sourcehut.org",
    "news.ycombinator.com",
    "sf.net",
    "wikipedia.org",
  };
  std::cout << "\nMatching " << token << "...";
  auto const hit = std::find(sites.cbegin(), sites.cend(), token);
  if (hit != sites.cend()) {
    std::cout << "  found!";
    return true;
  }
  return false;
}

// Given "abc.xyz.srt"; it returns ["srt", "xyz.srt", "abc.xyz.srt"]
std::vector<std::string_view> tokenize(std::string_view original,
                                       char delim = '.') {
  std::vector<std::string_view> subs;
  for (auto it = original.crbegin(); it != original.crend(); it++) {
    if (*it == delim)
      // No --it as reverse_iterator::base returns next element from cbegin.
      subs.push_back(std::string_view(it.base(), original.cend()));
  }
  if (!original.empty() && (original.back() != delim))
    subs.push_back(std::string_view(original));
  return subs;
}

int main() {
  const std::string_view host = "ta.wikipedia.org";

  auto const subs = tokenize(host);
  if (!std::any_of(subs.cbegin(), subs.cend(), [](const auto s) {
    return is_host_known(s);
  }))
    std::cout << "\nNo matches\n";
}
