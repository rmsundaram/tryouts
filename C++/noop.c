// http://stackoverflow.com/questions/2198950/why-is-void-0-a-no-operation-in-c-and-c

#define noop ((void)0)

int main()
{
	int x = noop;
	return 0;
}
