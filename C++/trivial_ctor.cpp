// http://stackoverflow.com/questions/31672207/what-are-defaulted-destructors-used-for#comment51292414_31672317

#include <iostream>
#include <type_traits>

// trivial — implicitly-provided
struct A { };

// non-trivial — user-provided constructor
struct B { B() { } };

// trivial — user-declared, implicitly-provided defaulted constructor
struct C { C() = default; };

// http://stackoverflow.com/a/17221752/183120
// For constructor the triviality is based on user-provided and not user-declared. However, the implicit move members
// are inhibited if the destructor is user-declared. i.e. both ~A() { … } and ~A() = default prohibits to compiler from
// providing an implicit move member function.

int main() {
    std::cout << std::boolalpha
              << std::is_trivial<A>() << '\n'
              << std::is_trivial<B>() << '\n'
              << std::is_trivial<C>();
}

