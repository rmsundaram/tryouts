// §13.3.1.2/8, N3337
// http://stackoverflow.com/a/22835168/183120

#include <iostream>

struct val
{
    int get_val()
    {
        return i;
    }
    int i = 7;
};

struct L4
{
    // non-class (pointer) type
    val* operator->()
    {
        return &v;
    }

    val v;
};

struct L3
{
    // class type
    L4& operator->()
    {
        return l;
    }
    L4 l;
};

struct L2
{
    L3& operator->()
    {
        return l;
    }
    L3 l;
};

struct L1
{
    L2& operator->()
    {
        return l;
    }
    L2 l;
};

int main()
{
    L1 l;
    std::cout << l->get_val() << '\n';
}
