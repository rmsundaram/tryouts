#include <iostream>
#include <iomanip>
#include <limits>
#include <bitset>
#include <cstdlib>
#include <cassert>
#include <type_traits>

static_assert(std::numeric_limits<float>::is_iec559,
              "float is not IEEE 754's binary32!");

// inspired by http://stackoverflow.com/a/8378022
template <typename T>
std::enable_if_t<std::is_integral<T>::value, T>
ftoi(float f) {
    uint32_t i;
    memcpy(&i, &f, sizeof(i));
    uint8_t const bias_e = (0x7F800000 & i) >> 23;
    if (bias_e == 0) return 0;                       // subnormals
    bool const is_neg = 0x80000000 & i;              // sign
    typedef std::numeric_limits<T> lim;
    // min/max for Inf/NaN
    if (bias_e == 0xff) return is_neg ? lim::min() : lim::max();
    signed e = bias_e - 127;
    if (e < 0) return 0;        // values within (0, 1), excluding denormals
    // use T's unsigned variant to perform (well-defined) shifting
    typedef typename std::make_unsigned<T>::type UT;
    UT m = 0x00800000 | (0x007FFFFF & i);            // complete significand
    // extracted significand has no radix point i.e. it sits after all 24
    // explicit bits; adjust exp accordingly
    e -= 23;

#ifndef NDEBUG
    std::bitset<lim::digits> mant(m);
    std::cout << "M: " << mant << '\n';
    std::cout << "E: " << e << '\n';
#endif

    // shift m to make e = 0 leaving us with the integer part of f
    // digits(UT) = digits(T) + 1, but don't use the extra bit as T can't
    // represent the result without change
    if (e >= 0) {
        // if the left shift is going to wipe out the mantissa we know T can't
        // represent trunc(f), return the maximum instead as a fallback.
        // if e == digits then it means the sign bit will be set in the
        // resulting T hence avoid that too by checking for e >= digits
        if (e >= lim::digits)
            m = lim::max();
        else
            m <<= e;
    }
    else {
        // if the right shift is going to wipe out the mantissa we know f < 1
        if (-e >= std::numeric_limits<float>::digits)
            return 0;
        else
            m >>= -e;
    }
    // validate that the sign bit is clear
    assert(!((UT(1) << (std::numeric_limits<UT>::digits - 1)) & m));
    T n;
    memcpy(&n, &m, sizeof(m));
    return is_neg ? -n : n;
}

int main(int argc, char *argv[]) {
    if (argc < 2) {
        std::cout << "Usage: f2i FLOAT\n";
        return 0;
    }
    auto const f = std::strtof(argv[1], nullptr); // e.g. 312234.34523
    std::cout << std::fixed
              << std::setprecision(std::numeric_limits<float>::max_digits10)
              << f << '\n';
    std::cout << ftoi<int64_t>(f) << '\n';
    // on a binary32 implementation of float, to represent FLT_MAX, an integer
    // type of width 128 bits is required[1]
    // floor(ld(340282346638528859811704183484516925440)) + 1 = 128
    // [1]: http://www.exploringbinary.com/number-of-bits-in-a-decimal-integer
}
