// g++ -Wall -std=c++20 -pedantic -pedantic-errors move_sem.cpp
// https://coliru.stacked-crooked.com/a/f8fd8ab09fc79e27

#include <iostream>
#include <unordered_map>
#include <tuple>
#include <algorithm>

struct Heavy
{
  int* data = nullptr;
  size_t bytes = 0;

  ~Heavy() {
    reset();
  }

  // default
  Heavy(const int* newData = nullptr, size_t newSize = 0)
    : data{new int[newSize]}
    , bytes{newSize} {
    std::copy_n(newData, newSize, data);
  }

  // copy; inheriting constructor
  Heavy(const Heavy& other) : Heavy(other.data, other.bytes) {
  }

  // move
  Heavy(Heavy&& other) : data{other.data}, bytes{other.bytes} {
    other.release();
  }

  // copy-assignment
  Heavy& operator=(const Heavy& rhs) {
    reset();
    bytes = rhs.bytes;
    data = new int[bytes];
    std::copy_n(rhs.data, bytes, data);
    return *this;
  }

  // move-assignment
  Heavy& operator=(Heavy&& rhs) {
    reset();
    std::tie(data, bytes) = rhs.release();
    return *this;
  }

  operator bool() {
    return data && bytes;
  }

  // free held resource; make it anew
  void reset() {
    if (data) delete [] data;
    bytes = 0;
  }

  // relinquish held resource; make it anew
  std::pair<int*, size_t>
  release() {
    int* ptr = data;
    const size_t oldBytes = bytes;
    data = nullptr;
    bytes = 0;
    return {ptr, oldBytes};
  }
};

int main()
{
  int arr[] = {1, 4, 7, 9, 12};

  // create
  Heavy h(arr, 5);
  std::cout << "Heavy items: " << h.bytes << "\tItem 3: " << h.data[3] << '\n';
  std::cout << "Org Heavy: " << h.bytes << "\t DataPtr: " << h.data << "\n\n";

  std::unordered_map<int, Heavy> m;
  // move
  m[2] = std::move(h);
  std::cout << "New Heavy items: " << m[2].bytes << "\tItem 3: " << m[2].data[3] << '\n';
  std::cout << "Org Heavy: " << h.bytes << "\t DataPtr: " << h.data << '\n';

  // copy
  h = m[2];
  std::cout << "\nOrg Heavy: " << h.bytes << "\t DataPtr: " << h.data << '\n';
}
