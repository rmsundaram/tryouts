#include <iostream>
#include <chrono>

int main()
{
  {
    using namespace std::chrono_literals;
    std::cout << std::chrono::seconds(1h).count() << '\n';
  }
  // without using literals
  std::cout << std::chrono::seconds(std::chrono::hours(2)).count() << '\n';
}
