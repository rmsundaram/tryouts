/*
 * Coons and tensor-product patch mesh data from PDF 1.7 Spec. 2008-7-1, 1st Edition, §8.7.4.5.7
 * Type 6 & 7 Shadings are to be converted into D2D patch mesh data which requires that each patch
 * has 4 flags associated to its sides to denote if it is part of the mesh's border.
 *
 * CREATION OF THE MESH
 * Flag of a patch decides where it'll occur with respect to the previous patch.
 * A new, independent patch is created for flag 0. The other flags denote other directions.
 *
 *     +---------+
 *     |    1    |
 *     |  patch  |
 *     |  above  |
 *     +---------+
 *
 *     +---------+  +---------+
 *     |    0    |  |    2    |
 *     |   new   |  |  patch  |
 *     |  patch  |  |  right  |
 *     +---------+  +---------+
 *
 *     +---------+
 *     |    3    |
 *     |  patch  |
 *     |  below  |
 *     +---------+
 *
 * sample input:
 *
 *     0 3 3 2 1
 *
 * would mean
 *
 *     +---+
 *     | 0 |
 *     +---+---+
 *     | 3 | 1 |
 *     +---+---+
 *     | 3 | 2 |
 *     +---+---+
 *
 * ALGORITHM
 * • Patch addition with flag
 *     ○ Set the 2 flags orthogonal to direction of the one added
 *     ○ top - clear prev. patch's top and set this patch's top
 *     ○ bottom - clear prev. patch's bottom and set this patch's bottom
 *     ○ right - clear prev. patch's right and set this patch's right
 * • A problem will be encountered only for the top/bottom addition if
 *     ○ a prior patch in the same row but previous column is present and hence its right border is to be cleared
 *     ○ note that an older patch in the same row but in any other column has no bearing on this patch's addition
 *     ○ have a deque to store the previous column's patch IDs with the appropriate level as index
 *     ○ 2 deques are required one for the previous and one for current, which will become next one's previous
 *     ○ on a new top or bottom addition check the deque to see if in the same row, previous column an
 *        adjacent patch is present; if yes, clear that patch's right and this patch's left
 *     ○ if the look-up index is larger than the max index in the deque, then we're sure that there're no patches
 *        on the left and thus can proceed with the addition safely
 *
 * input: 0 2 3 3 2 2 1 2 2 3 3 2 1
 *
 *   +---+---+
 *   |   |   |
 *   +---+---+   +---+---+---+
 *       |   |   |   |   |   |
 *       +---+---+---+---+---+---+
 *       |   |   |   |   |   |   |
 *       +---+---+---+   +---+---+
 *                       |   |   |
 *                       +---+---+
 *
 * input: 0 1 1 2 3      output
 *
 *    +---+---+         +---+---+
 *    |   |   |         |       |
 *    +---+---+         +       +
 *    |   |   |         |       |
 *    +---+---+         +   +---+
 *    |   |             |   |
 *    +---+             +---+
 */

#include <deque>
#include <vector>
#include <bitset>
#include <iostream>
#include <sstream>
#include <algorithm>
#include <stdexcept>
#include <cstdlib>
#include <limits>

enum class DIRECTION : char
{
    NONE = 0,
    TOP,
    RIGHT,
    BOTTOM
};

enum class BIT : size_t
{
    LEFT = 0,
    BOTTOM,
    RIGHT,
    TOP
};

void print_borders(unsigned patch_id, std::bitset<4> const &borders)
{
    std::cout << "Patch " << patch_id << "\n+";
    std::cout << (borders[static_cast<size_t>(BIT::TOP)] ? "---" : "   ") << "+\n";
    std::cout << (borders[static_cast<size_t>(BIT::LEFT)] ? '|' : ' ') << "   ";
    std::cout << (borders[static_cast<size_t>(BIT::RIGHT)] ? '|' : ' ') << "\n+";
    std::cout << (borders[static_cast<size_t>(BIT::BOTTOM)] ? "---" : "   ") << "+\n\n";
}

void clear_if_abutting(unsigned cur_level,
                       std::deque<int> const &prev_right,
                       std::vector<std::bitset<4>> &patches)
{
    if (cur_level < prev_right.size())
    {
        patches[prev_right[cur_level]].reset(static_cast<size_t>(BIT::RIGHT));
        patches.back().reset(static_cast<size_t>(BIT::LEFT));
    }
}

void new_patch(unsigned border_config,
               BIT clear_prev,
               std::vector<std::bitset<4>> &patches)
{
    patches.back().reset(static_cast<size_t>(clear_prev));
    patches.emplace_back(border_config);
}

void patch_right(std::vector<std::bitset<4>> &patches,
                 std::deque<int> &curr_right,
                 std::deque<int> &prev_right,
                 unsigned patch_id)
{
    new_patch(14u, BIT::RIGHT, patches);                    // 1110  +---+
    std::swap(curr_right, prev_right);                      //           |
    curr_right.assign(1, patch_id);                         //       +---+
}

void patch_top(std::vector<std::bitset<4>> &patches,
               std::deque<int> &curr_right,
               std::deque<int> &prev_right,
               unsigned patch_id,
               unsigned level)
{
    new_patch(13u, BIT::TOP, patches);                      // 1101  +---+
    clear_if_abutting(level, prev_right, patches);          //       |   |
    curr_right.push_front(patch_id);
}

void patch_bottom(std::vector<std::bitset<4>> &patches,
                  std::deque<int> &curr_right,
                  std::deque<int> &prev_right,
                  unsigned patch_id,
                  unsigned level)
{
    new_patch(7u, BIT::BOTTOM, patches);                    // 0111  |   |
    clear_if_abutting(level, prev_right, patches);       //       +---+
    curr_right.push_front(patch_id);
}

std::vector<std::bitset<4>> make_mesh(std::vector<DIRECTION> const &flags)
{
    std::vector<std::bitset<4>> patches{{15u}};                     // 1111  +---+
    patches.reserve(flags.size());                                  // trbl  |   |
                                                                    //       +---+
    std::deque<int> curr_right{0}, prev_right;   // patch IDs indexed by level number
    unsigned level = 0u;                         // reset at each column, marking its first patch's row as level 0
    for (auto i = 1u; i < flags.size(); ++i)
    {
        if (((flags[i] == DIRECTION::BOTTOM) && (flags[i - 1] == DIRECTION::TOP)) ||
            ((flags[i] == DIRECTION::TOP) && (flags[i - 1] == DIRECTION::BOTTOM)))
            throw std::invalid_argument("Top/bottom following a bottom/top!\nFlag configuration invalid.");
        switch (flags[i])
        {
        case DIRECTION::RIGHT:
            level = 0;
            patch_right(patches, curr_right, prev_right, i);
            break;
        case DIRECTION::TOP:
            ++level;
            patch_top(patches, curr_right, prev_right, i, level);
            break;
        case DIRECTION::BOTTOM:
            ++level;
            patch_bottom(patches, curr_right, prev_right, i, level);
            break;
        case DIRECTION::NONE:
            throw std::invalid_argument("Independent patch in the midst is not supported!");
        }
    }
    return patches;
}

DIRECTION str_to_dir(char const *str)
{
    int dir = -1;
    if (str) dir = *str - '0';
    if ((dir < 0) || (dir > static_cast<decltype(dir)>(DIRECTION::BOTTOM)))
    {
        std::ostringstream ss("Conversion error ", std::ios_base::ate);
        ss << str << " cannot be converted to a direction";
        throw std::invalid_argument(ss.str());
    }
    return static_cast<DIRECTION>(dir);
}

int main(int argc, char** argv)
{
    if (argc <= 1)
    {
        std::cerr << "usage: patches 0 [x ...]\n"
                     "where x is one of\n"
                     "0 - new patch\n"
                     "1 - patch above\n"
                     "2 - patch right\n"
                     "3 - patch below";
        return EXIT_FAILURE;
    }

    std::vector<DIRECTION> patch_flags;
    patch_flags.reserve(argc - 1);
    std::transform(argv + 1, argv + argc, std::back_inserter(patch_flags), str_to_dir);
    if (DIRECTION::NONE != patch_flags.front())
        throw std::invalid_argument("First patch should be independent");
    auto const patches = make_mesh(patch_flags);
    for (auto i = 0u; i < patches.size(); ++i)
    {
        print_borders(i, patches[i]);
    }
}
