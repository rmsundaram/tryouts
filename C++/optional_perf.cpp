// Performance overhead of std::optional
// https://stackoverflow.com/a/23524137/183120
// http://coliru.stacked-crooked.com/a/a5bc890a4ecbc305
// https://github.com/akrzemi1/markable marks one state of T as a sentinal value
// and gets rid of the addional storage needed for std::optional’s bool

#include <optional>
#include <chrono>
#include <vector>
#include <random>
#include <iostream>
#include <algorithm>

// Since HRC is almost always aliased to {steady,system}_clock it’s better to
// use the former directly; avoid latter as it isn’t monotonic i.e. it can go
// back and jump around too!
// https://en.cppreference.com/w/cpp/chrono/high_resolution_clock
// https://stackoverflow.com/q/1487695/183120
// https://stackoverflow.com/q/38252022/183120
// https://gamedev.stackexchange.com/a/131094/14025
typedef std::conditional<
  std::chrono::high_resolution_clock::is_steady,
  std::chrono::high_resolution_clock,
  std::chrono::steady_clock>::type Clock;
static_assert(Clock::is_steady,
               "Clock is not monotonically-increasing (steady).");

int main()
{
  // 1 million elements show a difference of 47 ms on an quad core Intel Intel
  // Core i7-7700HQ; use std::optional if isn’t in a tight loop or hot path.

  // make T = int, make *v to v below and benchmark again
  using T = std::optional<int>;

  constexpr auto size = 1000*1000;

  auto v = std::vector<T>(size);

  std::random_device rd;
  std::uniform_int_distribution<> dist;
  std::mt19937 gen;
  auto const rand = [&dist, &gen]{ return dist(gen); };

  for(auto& e : v)
  {
    e = rand();
  }

  auto const start = Clock::now();
  std::sort(begin(v), end(v));
  auto const stop = Clock::now();
  auto const d = stop - start;

  std::cout << "Result: "
            << *v[std::uniform_int_distribution<>(0, size)(gen)] << '\n';
  std::cout << "Duration: "
            << std::chrono::duration_cast<std::chrono::milliseconds>(d).count()
            << " ms\n";
}
