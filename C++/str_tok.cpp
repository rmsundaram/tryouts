// g++ -Wall -std=c++20 -pedantic{,-errors} str_tok.cpp
// C++20 introduced string_view::string_view taking iterators

#include <string_view>
#include <algorithm>
#include <cstdint>
#include <iostream>

// Inspired by Chromium’s base::StringTokenizer.
// https://source.chromium.org/chromium/chromium/src/+/main:base/strings/string_tokenizer.h
// See [1] for a policy-based (tag dispatch) implementation.
// [1]: https://github.com/dacap/tok

struct StringTokenizer {

  enum class Options : uint8_t {
    StripDelimiter,
    KeepDelimiter,
    //StripWhitespace,  -- needs ‘string next()’, not ‘string_view next()’
  };

  StringTokenizer(std::string_view str, std::string_view delims)
    : delims_{delims}
    , s_{str}
    , it_{s_.cbegin()}
    , opts_{Options::StripDelimiter}
  {
    skip_delim_prefix();
  }

  std::string_view next() {
    auto i = it_;
    std::string_view result;
    size_t successive_delims = 0;
    while (i != s_.cend()) {
      if (is_delimiter(*i++)) {
        if (result.empty())
          result = std::string_view{it_, i - 1};
        successive_delims++;
      } else if (!result.empty()) {  // met a non-delimiter after a delimiter
        --i;
        break;
      }
    }
    // handle the case where we never met a delimiter
    if (result.empty())
      result = std::string_view{it_, s_.cend()};

    if (opts_ == Options::KeepDelimiter)
      result = std::string_view{result.cbegin(),
                                result.size() + successive_delims};

    it_ = i;

    return result;
  }

  // This used to be Safe Bool idiom in pre-C++11:
  // https://www.artima.com/articles/the-safe-bool-idiom
  explicit operator bool() const {
    return it_ != s_.cend();
  }

  // Reset with new input string to operate on
  void assign(std::string_view input) {
    s_ = input;
    it_ = s_.cbegin();
    skip_delim_prefix();
  }

  void set(Options options) { opts_ = options; }

private:

  bool is_delimiter(char c) {
    return std::find(delims_.cbegin(), delims_.cend(), c) != delims_.cend();
  }

  void skip_delim_prefix() {
    while (is_delimiter(*it_))
      ++it_;
  }

  std::string_view delims_;
  std::string_view s_;
  std::string_view::const_iterator it_;
  Options opts_;
};

int main() {
  constexpr char my_str1[] = "This is a carrot. That is a banana.";

  StringTokenizer st1{my_str1, "."};
  while (st1)
    std::cout << st1.next() << "$\n";

  // check KeepDelimiter
  st1.assign(my_str1);
  st1.set(StringTokenizer::Options::KeepDelimiter);
  while (st1)
    std::cout << st1.next() << "$\n";

  // input ending without a delimiter; input with consecutive delimiters
  const std::string my_str2 = "Pulses, Grains, Vegetables, Spices, Fruits";
  StringTokenizer st2{my_str2, ", "};
  while (st2)
    std::cout << st2.next() << "$\n";

  // delimiter empty
  auto st3 = StringTokenizer("This is some string.", {});
  while(st3)
    std::cout << st3.next() << "$\n";

  // input starts and ends with delimiters
  auto st4 = StringTokenizer("  How are you?  ", " ");
  while(st4)
    std::cout << st4.next() << "$\n";
}
