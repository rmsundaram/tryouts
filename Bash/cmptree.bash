#!/bin/bash
#
# cmptree: compare directory trees recursively and report the differences.
# Original Author: Ives Aerts
# from http://github.com/notro/fbtft-spindle/blob/master/cmptree
# checked with http://shellcheck.net/

function gettype () {
    if [ -L "$1" ]; then
        echo "softlink"
    elif [ -f "$1" ]; then
        echo "file"
    elif [ -d "$1" ]; then
        echo "directory"
    elif [ -c "$1" ] || [ -b "$1" ]; then
        echo "special"
    elif [ -p "$1" ]; then
        echo "pipe"
    else
        echo "unknown"
    fi
}

function exists () {
    if [ -e "$1" ] || [ -L "$1" ]; then
        return 0;
    else
        # echo "missing: $1"
        return 1;
    fi
}

function comparefile () {
    [ "$1" = "/var/swap" ] && return
    "cmp" -s "$1" "$2"
    if [ $? -gt 0 ]; then
        echo "changed: $1 differs from $2"
        #  else
        #    echo "identical: $1 same as $2"
    fi
    return
}

function comparedirectory () {
    local result=0
    IFS=$'\n'
    # avoid aliases to ls by surrounding with ""
    # http://www.cyberciti.biz/faq/ignore-shell-aliases-functions-when-running-command/
    # TEST: find may be a better alternative to ls?
    local arr=($( ("ls" -A "$1" && "ls" -A "$2") | sort | uniq ))
    for i in "${arr[@]}"; do
        compare "$1/$i" "$2/$i" || result=1
        ((++cnt))
        echo -ne "$cnt file(s) compared!"'\r'
    done
    unset IFS
    return $result
}

function comparesoftlink () {
    # dest1=$( ls -l "$1" | awk '{ print "$11" }' )
    local dest1
    dest1="$( find "$1" -maxdepth 1 -type l -printf '%l' )"
    local dest2
    dest2="$( find "$2" -maxdepth 1 -type l -printf '%l' )"

    if [ "$dest1" = "$dest2" ]; then
        return 0
    else
        echo "different link targets $1 -> $dest1, $2 -> $dest2"
        return 1
    fi
}

# compare a file, directory, or softlink
function compare () {
    #  (exists "$1" && exists "$2") || return 1;

    if ! ( exists "$1" && exists "$2" ); then
        exists "$1" && echo "missing: $2"
        exists "$2" && echo "missing: $1"
        return 1;
    fi


    local type1
    type1=$(gettype "$1")
    local type2
    type2=$(gettype "$2")

    if [ "$type1" = "$type2" ]; then
        case "$type1" in
            file)
                comparefile "$1" "$2"
                ;;
            directory)
                comparedirectory "$1" "$2"
                ;;
            softlink)
                comparesoftlink "$1" "$2"
                ;;
            special)
                true
                ;;
            pipe)
                true
                ;;
            *)
                echo "$1 of unknown type"
                false
                ;;
        esac
    else
        echo "type mismatch: $type1 ($1) and $type2 ($2)."
        false
    fi

    return
}

if [ 2 -ne $# ]; then
    "cat" << EOU
Usage: $0 DIR1 DIR2
Compare directory trees:
  files are binary compared (cmp)
  directories are checked for identical content
  soft links are checked for identical targets
EOU
    exit 10
fi

cnt=0
compare "$1" "$2"
printf "\n"
exit $?
