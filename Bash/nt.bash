#!/usr/bin/bash

set -e

function perform {
  local cmd
  cmd=$([ -n "${1}" ] && [ "${1}" == "-u" ] && echo unmount || echo mount)
  for d in /dev/disk/by-label/* ; do
    # un/mount all drives except /home and Windows' like
    # System, WINRE_DRV, SYSTEM_DRV, etc.
    if [[ -h "${d}" ]] && ! [[ "${d}" =~ .*Swap|Home|Windows|System|_DRV$ ]]; then
      local unmounted
      unmounted=$(findmnt -n "${d}" &> /dev/null && echo 0 || echo 1)
      if [[ ( "${cmd}" == "mount" && ${unmounted} -eq 0 ) || \
            ( "${cmd}" == "unmount" && ${unmounted} -eq 1 ) ]]; then
        echo "$(basename "${d}") already ${cmd}ed."
      else
        udisksctl "${cmd}" -b "${d}"
      fi
    fi
  done
}

if ! command -v udisksctl &> /dev/null; then
  echo "error: udisksctl command not found" >&2
  # command not found
  # https://shapeshed.com/unix-exit-codes/
  exit 127
fi

perform "$@"
