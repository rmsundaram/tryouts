#!/usr/bin/env sh

function fn() {
    echo 'STDOUT: message!'
>&2 echo 'STDERR: message!'     # http://stackoverflow.com/q/2990414/183120
}

printf "Pipe STDOUT only:\nfn | sed -e 's;me;pa;'\n"
fn | sed -e 's;me;pa;'
# http://stackoverflow.com/a/2342841/183120
printf "\nPipe STDERR only:\nfn 2>&1 >/dev/null | sed -e 's;me;pa;'\n"
fn 2>&1 >/dev/null | sed -e 's;me;pa;'
# http://unix.stackexchange.com/q/24337/30580
printf "\nPipe both:\nfn 2>&1 | sed -e 's;me;pa;'\n"
fn 2>&1 | sed -e 's;me;pa;'
printf "\nPipe both (shorthand for above):\nfn |& sed -e 's;me;pa;'\n"
fn |& sed -e 's;me;pa;'

# http://stackoverflow.com/questions/2342826/how-to-pipe-stderr-and-not-stdout#comment51581050_2342841
printf "\nPipe nothing:\nfn >/dev/null 2>&1 | sed -e 's;me;pa;'\n"
fn >/dev/null 2>&1 | sed -e 's;me;pa;'
printf "\nPipe nothing:\nfn >/dev/null |& sed -e 's;me;pa;'\n"
fn >/dev/null |& sed -e 's;me;pa;'
