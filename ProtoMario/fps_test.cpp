#include <raylib.h>
#include <iostream>

// https://gameprogrammingpatterns.com/game-loop.html
// https://gamedev.stackexchange.com/q/1589/14025
// https://gafferongames.com/post/fix_your_timestep/
// https://dewitters.com/dewitters-gameloop/

constexpr auto TARGET_FPS = 30;
// allow 10 fps wiggle room, but skip updates if framerate drops below this
constexpr auto MAX_SEC_PER_FRAME = 1.0f / (TARGET_FPS - 10);

float ball_x = 10.0f;
constexpr float MOVE_X_PER_SEC = 100.0f;

void update(float ticks) {
  auto const delta = MOVE_X_PER_SEC * ticks;
  // this works since for 30 fps ticks ≈ 0.0333, 60 fps ticks ≈ 0.0167
  // number of update calls per second * milliseconds elapsed per frame = 1s
  // MOVE_X_PER_SEC * ticks * fps = 100
  // MOVE_X_PER_SEC * 0.0333 * 30 = 100
  // MOVE_X_PER_SEC * 0.0167 * 60 = 100
  ball_x += delta;
}

void render() {
  BeginDrawing();
  ClearBackground(GOLD);

  DrawCircleV(Vector2{ball_x, 100.0f}, 10.0f, BLACK);
  DrawLine(400, 0, 400, 600, RED);

  DrawFPS(5, 5);
  EndDrawing();
}

int main()
{
  InitWindow(800, 600, "FPS-agnostic updates");
  {
    SetTargetFPS(TARGET_FPS);
    bool quit = false;
    while (!WindowShouldClose() && !quit) {
      auto const ticks = GetFrameTime(); // frame time in seconds
      if (ticks <= MAX_SEC_PER_FRAME)    // pause updates if screen was inactive
        update(ticks);

      render();

      if (ball_x >= 400.0f) {
        std::cout << TARGET_FPS << " fps reached target at " << GetTime() << "s\n";
        quit = true;
      }

    }
  }
  CloseWindow();
}
