#include "updater.hpp"
#include "tileset.hpp"
#include "tileid.hpp"

#ifdef DEBUG_DRAW
#    include "renderer.hpp"
#endif  // DEBUG_DRAW

#include <raymath.h>

#include <tuple>
#include <cmath>
#include <cassert>

namespace {

// returns true if there's a collision, along with the penetration vector
std::pair<bool, Vector2>
checkCollision(Map const& m,
               BBox2D const& bounds,
               size_t col, size_t row,
               Orientation o)
{
    auto [neighbour_exists, new_col, new_row] = m.tileAtSide(col, row, o);
    if (neighbour_exists) {
        auto [_, neighbour_id] = m.tile(new_col, new_row);
        if (m.tileProperty(neighbour_id) == TileProperty::Solid) {
            auto const neighbour_bounds = tileBounds(new_col,
                                                     new_row,
                                                     Map::tile_width,
                                                     Map::tile_height);
            if (bounds * neighbour_bounds) {

#ifdef DEBUG_DRAW
                m.m_dbg_boxes.emplace_back(neighbour_bounds, Color(RED));
#endif  // DEBUG_DRAW

                // When hero’s moving forward, he’d also be moving downward at
                // the same time due to gravity. If you choose to resolve that
                // collision by reversing the last move (forward and down), it’d
                // move the hero both upward and backward — but that's not what
                // we want! We want the forward movement to continue.

                // If there's a collision, simply undoing it in both dimensions
                // isn't what we want. Collision resolution will be done by
                // applying minimum force (shortest vector) along one
                // direction. This is given by penetrationVector function.
                Vector2 const p = penetrationVector(neighbour_bounds, bounds);

                // Ideally this condition should be an assert, but due to
                // floating-point rounding issues, what's qualified by operator*
                // as intersecting is sometimes non-intersecting by Minkowski
                // implementation of penetrationVector; so check again. e.g.
                // BBox2D bounds{{758.270081, 487.000031}, {18.5, 25}};
                // BBox2D neighbour_bounds{{800, 544}, {32, 32}};
                if (vec2IsValid(p))
                    return std::make_pair(true, p);
            }
#ifdef DEBUG_DRAW
            else {
                m.m_dbg_boxes.emplace_back(neighbour_bounds, Color(BLUE));
            }
#endif  // DEBUG_DRAW
        }
    }
    return std::make_pair(false, vec2Inf());
}

Vector2 testTile(Map const& m,
                  Orientation o,
                  Vector2 const& displacement)
{
    BBox2D const new_bounds = move(m.m_hero.m_bounds, displacement);
    auto [valid_pos, col, row] = m.tileIndex(m.m_hero.m_bounds.m_centre);
    assert(valid_pos == true);

    auto const [does_collide,
                penetration] = checkCollision(m,
                                              new_bounds,
                                              col, row,
                                              o);
    return does_collide ? Vector2Add(displacement, penetration) : displacement;
}

}  // namespace

namespace Updater {

// Reference
// https://www.raywenderlich.com/
//         2891-how-to-make-a-platform-game-like-super-mario-brothers-part-1
Vector2 displaceHero(Map const &m, Vector2 displacement)
{
    // Order-dependant code!
    // Primarily colliding tiles; resolving collision for these would mostly
    // resolve collision for the corner tiles.
    //
    // Say displacement.y > 0, which is usually the case since gravity is always
    // ON, three tiles should be checked: Bottom, RightBottom and LeftBottom;
    // likewise for each of the four primary directions. The corner tile
    // conditions should be tested with if and not else-if; an else-if, for
    // instance, when (displacement.x > 0), would skip testing RightTop.

    // test bottom
    if (displacement.y > 0)
        displacement = testTile(m, Orientation::Bottom, displacement);
    // test top
    else if (displacement.y < 0)
        displacement = testTile(m, Orientation::Top, displacement);
    // test right
    if (displacement.x > 0)
        displacement = testTile(m, Orientation::Right, displacement);
    // test left
    else if (displacement.x < 0)
        displacement = testTile(m, Orientation::Left, displacement);

    // test right-bottom
    if ((displacement.x > 0) || (displacement.y > 0))
        displacement = testTile(m, Orientation::RightBottom, displacement);
    // test right-top
    if ((displacement.x > 0) || (displacement.y < 0))
        displacement = testTile(m, Orientation::RightTop, displacement);
    // test left-bottom
    if ((displacement.x < 0) || (displacement.y > 0))
        displacement = testTile(m, Orientation::LeftBottom, displacement);
    // test left-top
    if ((displacement.x < 0) || (displacement.y < 0))
        displacement = testTile(m, Orientation::LeftTop, displacement);

    // no collision, return original displacement unaltered
    return displacement;
}

void update(Map &m, float dt)
{
    auto [valid_pos, col, row] = m.tileIndex(m.m_hero.m_bounds.m_centre);

    // game over
    if (!valid_pos) {
        m.m_state = GameState::OVER;
        return;
    }
    else {
        auto const p = m.m_hero.position();
        // HACK: brute-force check on whether hero is atop the coin tile!
        auto const target_col = (static_cast<float>(m.cols) - 0.5f) *
                                Map::tile_width;
        auto const target_row = 9;
        if ((p.x > target_col) && (row == target_row))
        {
            m.m_state = GameState::WON;
            return;
        }
    }

    auto &vel = m.m_hero.m_velocity;

    // factor in gravity and user's push into existing velocity
    vel = Vector2Add(vel, Vector2Scale(GRAVITY, dt));

    // Cases
    //     1. JUMP pressed and on ground
    //     2. JUMP continued to press but not on ground
    //     3. JUMP not pressed and velocity above cut-off
    //
    // Only (1) and (3) need code. (1) is simple. Once there's a new Y velocity
    // set, it’ll act on the hero, gradually moves it up in subsequent frames;
    // gravity will eventually erode it in a few frames. However, if the user
    // released the key (3), since JUMP_CUTOFF is smaller than JUMP, velocity is
    // clamped without letting the earlier set higher velocity to work on the
    // body. If the user continues to press JUMP (2), though not on ground,
    // it’ll avoid (3) from happening, leading to a full jump.
    if (IsKeyDown(KEY_SPACE) && (m.m_hero.m_is_on_ground))
        vel = Vector2Add(vel, Vector2Scale(JUMP, dt));
    else if (!IsKeyDown(KEY_SPACE)) {
        // clamp to minimum jump if the user isn't continuing to press JUMP
        const auto jump_cutoff_step = JUMP_CUTOFF * dt;
        vel.y = std::max(vel.y, jump_cutoff_step);
    }

#if NO_FRICTION == 0
    vel = vec2Scale(vel, FRICTION);
    if (IsKeyDown(KEY_RIGHT) || IsKeyDown(KEY_LEFT)) {
        auto const dir = IsKeyDown(KEY_RIGHT) ? 1.0f : -1.0f;
        auto const walk_step = Vector2Scale(WALK, dir * dt);
        vel = Vector2Add(vel, walk_step);
        // update hero's state machine
        m.m_hero.m_is_facing_left = (dir == -1.0f);
        m.m_hero.m_is_walking = true;
    }
    else
        m.m_hero.m_is_walking = false;
    // Usually there're min and max velocity constants defined in both
    // dimensions; velocity is clamped to limit the effect of accumulating
    // forces: gravity (accumulates automatically) and walk (as long as the user
    // is commanding the hero to walk). However, when friction in a given
    // dimension is < 1, it acts as a limiting factor and converges the
    // resulting value in that dimension to a constant value. Try setting
    // FRICTION.x >= 1 and print vel to observe changes to vel over time.
#endif // NO_FRICTION

    // displacement vector this frame
    auto dv = Vector2Scale(vel, dt);
#if NO_FRICTION == 1
    if (IsKeyDown(KEY_RIGHT) || IsKeyDown(KEY_LEFT)) {
        auto const dir = IsKeyDown(KEY_RIGHT) ? 1 : -1;
        // We're directly adding to dv since we don't want any memory of the
        // forward/backward push stored in hero.velocity. Since we directly
        // add to dv, we need to scale forward dt²
        dv = Vector2Add(dv, Vector2Scale(WALK, dir * dt * dt));
    }
#endif  // NO_FRICTION
    auto const dv_prime = displaceHero(m, dv);

    // If displaceHero changes dv.y, it means hero has hit solid ground or a
    // high ceiling. Set velocity in Y to zero. In case of hitting ground
    // dv_prime.y <= dv.y, and in case of ceiling, dv_prime.y >= dv.y will be
    // true. Without zeroing vel.y, we'd continue to accumulate vel.y along with
    // gravity and eventually velocity will be larger a tile hight; it'd move
    // the hero beyond the ground tile beneath! Since we're only doing discrete
    // collision detection, not continuos, there'd be no way to prevent this.
    // This is the reason why step value shouldn't be larger than tile size.
    if (std::abs(dv.y - dv_prime.y) > eps) {
        vel.y = 0.0f;
        m.m_hero.m_is_on_ground = (dv_prime.y <= dv.y);
    }
    else
        m.m_hero.m_is_on_ground = false;

    auto const pos = m.m_hero.position();
    m.m_hero.setPosition(Vector2Add(pos, dv_prime));

#ifdef DEBUG_DRAW
    m.m_dbg_boxes.emplace_back(m.m_hero.m_bounds, Color(GREEN));
#endif  // DEBUG_DRAW

    // notify hero of internal state changes
    m.m_hero.update(dt);
    m.m_hero_sprite.update(dt);
}

}  // namespace Updater
