#ifndef RENDERER_HPP
#define RENDERER_HPP

#include "common.hpp"

#include <vector>

struct Map;
struct TileID;
struct Tileset;

class Renderer
{
public:
    ~Renderer();

    void loadTexture(const Image& img);
    void render(Map const &m);

private:
    void renderTile(TileID tileID,
                     Tileset const &tileset,
                     Vector2 dst_pt);
    void renderHero(Map const &m);

    std::vector<Texture2D> m_textures;
};

#endif // RENDERER_HPP
