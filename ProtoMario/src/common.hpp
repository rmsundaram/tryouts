#ifndef COMMON_HPP
#define COMMON_HPP

#ifndef NDEBUG
#    define DEBUG_DRAW 1
#endif // !NDEBUG

#define NO_FRICTION 0

#include <ostream>
#include <cstdint>
#include <raylib.h>

inline
std::ostream& operator<<(std::ostream& os, Vector2 const& v)
{
    return os << v.x << ", " << v.y;
}

enum class Orientation : uint8_t {
    Bottom,
    Right,
    Top,
    Left,
    RightBottom,
    LeftBottom,
    RightTop,
    LeftTop
};

#endif // COMMON_HPP
