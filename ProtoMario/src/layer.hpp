#ifndef LAYER_HPP
#define LAYER_HPP

#include "common.hpp"
#include "tileid.hpp"

#include <vector>
#include <cassert>

struct Layer
{
    Layer(int id, size_t cols, size_t rows);
    template<typename T>
    void loadData(T IDs);
    int getId() const;
    TileID getTile(unsigned col, unsigned row) const;
    void setOffsets(int x, int y);
    size_t cols() const;
    size_t rows() const;

private:
    int m_id;
    size_t m_cols, m_rows;
    int m_x_offset, m_y_offset;
    std::vector<TileID> m_tileIDs;
};

template<typename T>
void Layer::loadData(T IDs) {
    assert(IDs.size() == m_rows * m_cols);
    m_tileIDs = IDs;
}

#endif // LAYER_HPP
