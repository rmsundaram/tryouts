#ifndef TILESET_HPP
#define TILESET_HPP

#include "common.hpp"

#include <vector>
#include <cstdint>

enum class TileProperty : uint8_t
{
    Generic,
    Solid = 1
};

struct Tileset
{
    // constructor to satisfy std::vector::emplace_back as
    // aggregate initialization doesn't work
    // https://stackoverflow.com/q/8782895/183120
    Tileset(unsigned id,
            unsigned tile_count,
            unsigned columns,
            Image    src_img);

    // non-copyable
    Tileset(const Tileset&) = delete;
    Tileset& operator=(const Tileset&) = delete;

    // movable
    Tileset(Tileset&&);
    Tileset& operator=(Tileset&&);

    ~Tileset();

    void setProperty(unsigned tile_id, TileProperty properties);

    // Input:  tile ID w.r.t its tileset
    // Return: tile offset in pixels
    Vector2 offset(unsigned tile_id) const;

    uint8_t                   m_id;
    uint16_t                  m_count;
    uint16_t                  m_cols;
    std::vector<TileProperty> m_tile_props;
    Image                     m_image;
};

#endif // TILESET_HPP
