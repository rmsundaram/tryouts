// created to debug tiled.h from Randy Gaul’s cute headers

#include <iostream>
#include <cstdint>
#include <string>

uint64_t cute_tiled_FNV1a(const void* buf, int len)
{
	uint64_t h = (uint64_t)14695981039346656037U;
	const char* str = (const char*)buf;

	while (len--)
	{
		char c = *str++;
		h = h ^ (uint64_t)c;
		h = h * (uint64_t)1099511628211;
	}

	return h;
}

template <typename T>
inline constexpr size_t strLiteralLength(const T&)
{
	return std::extent<T>::value - 1;
}

int main() {
    std::string str[] = {"height", "infinite", "layers", "nextlayerid", "nextobjectid", "orientation", "renderorder", "tiledversion", "tileheight", "tilesets", "tilewidth", "type", "version", "width", "id", "name", "opacity", "visible", "x", "y", "columns", "firstgid", "image", "imageheight", "imagewidth", "margin", "spacing", "tilecount", "tileheight", "tilewidth"};
    for (std::string &s : str) {
        std::cout << s << '\t' << cute_tiled_FNV1a(s.c_str(), s.size() + 1) << '\n';
    }
}
