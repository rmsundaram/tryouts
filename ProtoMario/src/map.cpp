#include "map.hpp"
#include "tileset.hpp"
#include "layer.hpp"

#include <raymath.h>
#include <json/json.h>

#include <string>
#include <map>
#include <iterator>
#include <limits>
#include <fstream>
#include <stdexcept>
#include <cassert>

float Map::tile_width = 0;
float Map::tile_height = 0;

// Tiled JSON (format v1.2) loading code
// https://doc.mapeditor.org/en/stable/reference/tmx-map-format
namespace {

auto loadTilesets(Json::Value& tileset_json)
{
    auto n = tileset_json.size();

    if (n == 0)
        throw std::invalid_argument("No tilesets found.");

    Map::tile_width = tileset_json[0]["tilewidth"].asFloat();
    Map::tile_height = tileset_json[0]["tileheight"].asFloat();

    std::vector<Tileset> tilesets;
    std::vector<unsigned> gids;
    tilesets.reserve(n);
    gids.reserve(n);
    for (auto i = 0u; i < n; ++i) {
        // different tile sizes - unsupported
        if ((Map::tile_width != tileset_json[i]["tilewidth"].asFloat()) ||
            (Map::tile_height != tileset_json[i]["tileheight"].asFloat()))
            throw std::invalid_argument("Tilesets have different tile sizes.");

        auto img_path = std::string(RESOURCE_DIR);
        img_path.append(tileset_json[i]["image"].asString());
        tilesets.emplace_back(i,
                              tileset_json[i]["tilecount"].asUInt(),
                              tileset_json[i]["columns"].asUInt(),
                              LoadImage(img_path.c_str()));
        auto const tiles_json = tileset_json[i]["tiles"];
        auto const n_tile_props = tiles_json.size();
        for (auto j = 0u; j < n_tile_props; ++j) {
            auto const id = tiles_json[j]["id"].asUInt();
            auto const props = tiles_json[j]["properties"];
            for (auto const &p : props) {
                if (p["name"].asString() == "Props") {
                    tilesets.back().setProperty(
                        id,
                        static_cast<TileProperty>(p["value"].asUInt()));
                }
            }
        }
        gids.push_back(tileset_json[i]["firstgid"].asUInt());
    }

    // https://doc.mapeditor.org/en/stable/reference/tmx-map-format/#tileset
    // says "The first tileset always has a firstgid value of 1".
    assert((gids.size() > 0) && (gids[0] == 1));
    // insert an additional, sential maximum value to avoid
    // special casing while searching
    gids.push_back(std::numeric_limits<decltype(gids)::value_type>::max());

    return std::make_tuple(std::move(tilesets), gids);
}

// Input: global tile ID and the first GIDs of all tileset
// Output: tileset global_tile_id belongs to
// Example:  1, { 1, 13, 43 }
// Example:  3, { 1, 13, 43 }
// Example: 50, { 1, 13, 43 }
unsigned findTileset(unsigned global_tile_id,
                     std::vector<unsigned> const &tileset_first_gid)
{
    auto i = 0u;
    for (; i < tileset_first_gid.size(); ++i) {
        if (global_tile_id <= tileset_first_gid[i]) {
            i -= (global_tile_id != tileset_first_gid[i]);
            break;
        }
    }
    return i;
}

void populateLayers(Map &m,
                    std::vector<unsigned> const &gids,
                    Json::Value const &root)
{
    const auto layers = root["layers"];
    size_t num_layers = layers.size();
    m.m_layers.reserve(num_layers);
    for (auto i = 0u; i < num_layers; ++i) {
        if (layers[i]["type"].asString() == "tilelayer") {
            auto layer_cols = layers[i]["width"].asUInt();
            auto layer_rows = layers[i]["height"].asUInt();
            m.m_layers.push_back(Layer{static_cast<int>(i),
                                       layer_cols,
                                       layer_rows});
            std::vector<TileID> tile_ids;
            tile_ids.reserve(layer_cols * layer_rows);
            auto const json_tile_ids = layers[i]["data"];
            assert(json_tile_ids.size() == layer_cols * layer_rows);
            for (auto j = 0u; j < json_tile_ids.size(); ++j) {
                auto global_tile_id = json_tile_ids[j].asUInt();
                auto tileset_id = 0u, id_in_tileset = 0u;
                // Set tileset ID for non-empty tiles belonging to a tileset
                if (global_tile_id != 0) {
                    tileset_id = findTileset(global_tile_id, gids);
                    id_in_tileset = global_tile_id - gids[tileset_id];
                }
                tile_ids.push_back({tileset_id, id_in_tileset});
            }
            m.m_layers[i].loadData(tile_ids);
        }
    }
}

void loadObjects(Map &m, Json::Value const &root)
{
    const auto layers = root["layers"];
    size_t num_layers = layers.size();
    int i;
    for (i = static_cast<int>(num_layers) - 1; i >= 0; --i)
        if (layers[i]["type"].asString() == "objectgroup")
            break;
    assert(i != -1);  // objectgroup layer not found
    for (auto const &o : layers[i]["objects"]) {
        // NOTE: code knows about data
        if (o["name"].asString() == "HeroStart") {
            m.m_hero.setPosition({o["x"].asFloat(), o["y"].asFloat()});
            m.m_hero.m_state = Hero::State::Falling_Right;
            break;
        }
    }
}

auto loadHeroBounds(Json::Value const &hero_objectgroup)
{
    // NOTE: code knows about data
    auto const bbox_json = hero_objectgroup["objects"][0];
    Vector2 left_top{bbox_json["x"].asFloat(),
                     bbox_json["y"].asFloat()};
    Vector2 dims{bbox_json["width"].asFloat() * 0.5f,
                 bbox_json["height"].asFloat() * 0.5f};
    auto const centre = Vector2Add(left_top, dims);

    // hero's position will be anchored w.r.t hero's bounding box space;
    // amount to scale the basis vectors to translate the origin comes from
    // the JSON
    Vector2 const anchor_scales{
        bbox_json["properties"][0]["value"].asFloat(),
        bbox_json["properties"][1]["value"].asFloat()
    };
    return std::make_tuple(BBox2D{centre, dims}, anchor_scales);
}

auto findHeroTile(Json::Value const &root_json)
{
    unsigned tileset_id = 0;
    unsigned tile_id = 0;
    // locate hero tile's JSON
    for (auto const &p : root_json["properties"]) {
    // NOTE: code knows about data
        if (p["name"].asString() == "HeroTilesetID")
            tileset_id = p["value"].asUInt();
        else if (p["name"].asString() == "HeroTileID")
            tile_id = p["value"].asUInt();
    }
    auto const tilset_json = root_json["tilesets"][tileset_id];
    Json::Value hero_tile_json;
    for (auto const &t : tilset_json["tiles"]) {
        if (t["id"].asUInt() == tile_id) {
            hero_tile_json = t;
            break;
        }
    }
    return std::make_tuple(hero_tile_json, tileset_id);
}

auto parseHeroStates(Json::Value const &hero_tile_json,
                     unsigned tileset_id)
{
    // check hero tile's properties for associated sprites
    auto const n_states = hero_tile_json["properties"].size();
    std::vector<TileID> state_tile(n_states,
                                   TileID(tileset_id, 0));
    std::vector<bool> state_anim(n_states, false /* no animation */);
    for (auto const &p : hero_tile_json["properties"]) {
        auto const state = p["name"].asString();
        auto const state_id_str = state.substr(state.rfind(',') + 1);
        auto const state_id = std::stoul(state_id_str);
        assert(state_id < state_tile.size());
        unsigned tile_id = 0u;
        auto tile_str = p["value"].asString();
        // check if animated
        if ((state_anim[state_id] = (tile_str.back() == 'a')))
            tile_str.pop_back();
        tile_id = static_cast<unsigned>(std::stoul(tile_str));
        state_tile[state_id].setTileId(tile_id);
    }
    return std::make_tuple(state_tile, state_anim);
}

void loadHeroSprites(Json::Value root_json,
                     Json::Value hero_tile_json,
                     unsigned tileset_id,
                     Map &m)
{
    // load sprite data
    auto const [states_tiles, state_anim] = parseHeroStates(hero_tile_json,
                                                              tileset_id);
    std::map<unsigned, std::vector<TileID>> tile_id_to_tiles;
    std::map<uint8_t, float> tile_id_to_duration;
    auto const tiles_json = root_json["tilesets"][tileset_id]["tiles"];
    for (auto const &t : tiles_json) {
        if (!t["animation"].empty()) {
            std::vector<TileID> frames;
            frames.reserve(t["animation"].size());
            for (auto const &f : t["animation"]) {
                auto const tile_id = static_cast<uint8_t>(f["tileid"].asUInt());
                frames.push_back(TileID(tileset_id, tile_id));
                tile_id_to_duration[tile_id] = f["duration"].asFloat();
            }
            tile_id_to_tiles[t["id"].asUInt()] = std::move(frames);
        }
    }
    auto const n_states = static_cast<size_t>(Hero::State::Total);
    // + 1 for the last sential useful to prepare length
    m.m_hero_sprite.m_state_start_idx.reserve(n_states + 1);
    m.m_hero_sprite.m_state_start_idx.push_back(0u);
    for (auto i = 0u; i < n_states; ++i) {
        auto const start_tile_id = states_tiles[i];
        uint8_t inserted_frames = 1u;
        auto tile_frames = tile_id_to_tiles.find(start_tile_id.id());
        // state with animation
        if (state_anim[i] && (tile_frames != tile_id_to_tiles.end())) {
            std::copy(tile_frames->second.cbegin(),
                      tile_frames->second.cend(),
                      std::back_inserter(m.m_hero_sprite.m_tiles));
            inserted_frames = static_cast<uint8_t>(tile_frames->second.size());
        }
        else
            m.m_hero_sprite.m_tiles.push_back(start_tile_id);

        auto const end =
                static_cast<uint8_t>(m.m_hero_sprite.m_state_start_idx.back() +
                                     inserted_frames);
        m.m_hero_sprite.m_state_start_idx.push_back(end);
    }
    m.m_hero_sprite.m_tile_duration = std::move(tile_id_to_duration);
}

void loadHero(Map &m, Json::Value const &root_json)
{
    auto const [hero_tile_json, tileset_id] = findHeroTile(root_json);

    // load hero's bounding box; set tile offset w.r.t it
    auto const [hero_bounds, anchor_scales] =
            loadHeroBounds(hero_tile_json["objectgroup"]);
    m.m_hero.m_bounds = hero_bounds;
    // hero should not be larger than a tile for collision to work
    assert((Map::tile_width >= hero_bounds.m_half_size.x * 2) &&
           (Map::tile_height >= hero_bounds.m_half_size.y * 2));
    m.m_hero.m_anchor_scales_in_bounds = anchor_scales;
    m.m_hero_sprite.m_tile_offset = Vector2Negate(m.m_hero.position());

    loadHeroSprites(root_json, hero_tile_json, tileset_id, m);
    m.m_hero_sprite.m_hero = &m.m_hero;
}

}  // unnamed namespace

Map Map::loadMap(std::string path)
{
    Json::Value root;
    std::ifstream map_file(path, std::ifstream::binary);
    map_file >> root;

    if (root["orientation"].asString() != "orthogonal")
        throw std::invalid_argument("Orthogonal map expected.");

    // loading code is order-dependant e.g. Map::tile_width is expected to be
    // populated by the time load_hero is populated
    auto cols = root["width"].asUInt();
    auto rows = root["height"].asUInt();
    // structured bindings
    // https://ddavis.fyi/blog/2018-01-09-cpp17-early-favorites/
    auto [tilesets, gids] = loadTilesets(root["tilesets"]);

    Map m{cols, rows,
          {} /*hero*/,
          {} /*hero sprite*/,
          {} /*layers*/,
          std::move(tilesets),
          GameState::RUNNING};
    populateLayers(m, gids, root);
    loadHero(m, root);
    loadObjects(m, root);
    assert((m.m_hero.position().x != 0.0f) || (m.m_hero.position().y != 0.0f));
    return m;
}


// width and height in pixels
float Map::width() const
{
    return static_cast<float>(cols) * tile_width;
}

float Map::height() const
{
    return static_cast<float>(rows) * tile_height;
}

std::tuple<bool, size_t, size_t> Map::tileIndex(Vector2 pos) const
{
    int const col = static_cast<int>(pos.x / tile_width);
    int const row = static_cast<int>(pos.y / tile_height);

    // early return for out of bounds cases
    if ((col < 0) || (row < 0) || (col >= static_cast<int>(cols)) ||
                                  (row >= static_cast<int>(rows))) {
        return std::make_tuple(false, 0, 0);
    }
    return std::make_tuple(true, col, row);
}

std::pair<bool, TileID> Map::tile(size_t col, size_t row) const
{
    // early return for out of bounds cases
    if ((col >= cols) || (row >= rows))
        return std::make_pair(false, TileID{0, 0});

    TileID tile_id(0, 0);
    for (auto const& l : m_layers) {
        auto t= l.getTile(static_cast<unsigned>(col),
                           static_cast<unsigned>(row));
        if (!t.isEmpty())
            tile_id = t;
    }
    return std::make_pair(true, tile_id);
}

std::tuple<bool, size_t, size_t> Map::tileAtSide(size_t col,
                                                 size_t row,
                                                 Orientation o) const
{
    if ((o == Orientation::RightTop) || (o == Orientation::RightBottom) ||
        (o == Orientation::Right)) {
        ++col;
    }
    else if ((o == Orientation::LeftTop) || (o == Orientation::LeftBottom) ||
             (o == Orientation::Left)) {
        --col;
    }
    if ((o == Orientation::LeftBottom) || (o == Orientation::RightBottom) ||
        (o == Orientation::Bottom)) {
        ++row;
    }
    else if ((o == Orientation::LeftTop) || (o == Orientation::RightTop) ||
             (o == Orientation::Top)) {
        --row;
    }
    return std::make_tuple((row < rows) || (col < cols),
                           col,
                           row);
}

TileProperty Map::tileProperty(TileID tile_id) const
{
    assert(tile_id.tileset() < m_tilesets.size());
    return m_tilesets[tile_id.tileset()].m_tile_props[tile_id.id()];
}
