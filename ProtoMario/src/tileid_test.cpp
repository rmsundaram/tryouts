#include <iostream>
#include <cstdint>
#include <cassert>
#include <tuple>
#include <limits>

static constexpr unsigned TILESET_BITS = 4;
static constexpr unsigned TILE_ID_BITS = 12;
static constexpr unsigned MAX_TILESET_ID = (1 << TILESET_BITS) - 1;
static constexpr unsigned MAX_TILE_ID = (1 << TILE_ID_BITS) - 1;

uint16_t setTilesetTileId(unsigned tileset_id, unsigned tile_id)
{
    assert(tileset_id <= MAX_TILESET_ID);
    static_assert (sizeof(unsigned) >= 4,
                   "Invalid assumption on unsigned's size");
    auto constexpr BIT_MASK = std::numeric_limits<unsigned>::max();
    uint16_t tileset_tile_id = 0;
    // clear MSB 4 bits
    tileset_tile_id &= static_cast<uint16_t>(~(BIT_MASK << TILE_ID_BITS));
    // copy those bits from tileset_id
    tileset_tile_id |= static_cast<uint16_t>(tileset_id << TILE_ID_BITS);

    assert(tile_id <= MAX_TILE_ID);
    tileset_tile_id &= static_cast<uint16_t>(BIT_MASK << TILE_ID_BITS);
    tileset_tile_id |= static_cast<uint16_t>(tile_id);
    return tileset_tile_id;
}

auto getTilesetTileId(uint16_t tileset_tile_id)
{
    unsigned tileset_id = tileset_tile_id;
    tileset_id >>= TILE_ID_BITS;
    return std::make_tuple(tileset_id, (tileset_tile_id &
                                      ~(std::numeric_limits<unsigned>::max() << TILE_ID_BITS)));
}

int main(int argc, char** argv) {
    if (argc < 3)
        std::cerr << "Usage: " << argv[0] << " TILESET_ID TILE_ID" << '\n';
    else {
        auto tileset_id = std::strtoul(argv[1], nullptr, 0);
        auto tile_id = std::strtoul(argv[2], nullptr, 0);
        auto combined_id = setTilesetTileId(tileset_id, tile_id);
        auto [x, y] = getTilesetTileId(combined_id);
        std::cout << x << '\t' << y << '\n';
    }
}
