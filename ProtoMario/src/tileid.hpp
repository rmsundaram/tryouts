#ifndef TILEID_H
#define TILEID_H

#include "common.hpp"

#include <cstdint>

// tileset and tile ID pair denote a TileID unambigiously
// NOTE: tile ID is w.r.t its tile set
struct TileID
{
    TileID(unsigned tileset_id, unsigned id);

    unsigned tileset() const;
    unsigned id() const;
    bool isEmpty() const;

    void setTileset(unsigned tileset_id);
    void setTileId(unsigned tile_id);

    static constexpr unsigned TILESET_BITS = 4;
    static constexpr unsigned TILE_ID_BITS = 12;
    static constexpr unsigned MAX_TILESET_ID = (1 << TILESET_BITS) - 1;
    static constexpr unsigned MAX_TILE_ID = (1 << TILE_ID_BITS) - 1;
    // packed as 4 + 12
    // max tile set ID: 15
    // max tile ID: 4095
    uint16_t m_tileset_tile_id;
};

#endif // TILEID_H
