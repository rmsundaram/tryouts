#ifndef HERO_HPP
#define HERO_HPP

#include "common.hpp"
#include "bbox.hpp"

#include <raylib.h>

#include <vector>
#include <map>

struct TileID;

struct Hero
{
    enum class State : uint8_t {
        Idle_Right,
        Idle_Left,
        Walking_Right,
        Walking_Left,
        Jumping_Right,
        Jumping_Left,
        Falling_Right,
        Falling_Left,
        Total
    };

    Vector2    position() const;
    void       setPosition(Vector2 new_pos);
    void       update(float ticks);

    Vector2    m_velocity;
    BBox2D     m_bounds;
    // coordinates in bounding box space to get anchor point
    Vector2    m_anchor_scales_in_bounds;

    State      m_state;
    bool       m_is_on_ground;
    bool       m_is_facing_left;
    bool       m_is_walking;
};

struct HeroSprite
{
    // offset to add to Hero::pos to get tile left-top to draw
    Vector2              m_tile_offset;
    std::vector<TileID>  m_tiles;
    std::vector<uint8_t> m_state_start_idx;

    // a tile's duration of persistance when playing an animation
    std::map<uint8_t,
             float>      m_tile_duration;
    Hero*                m_hero;
    // cache hero's state to detect state changes
    Hero::State          m_state;
    uint8_t              m_frame_offset;
    float                m_pending_duration;

    void    update(float ticks);
    uint8_t getCurrentFrameId() const;
};

#endif // HERO_HPP
