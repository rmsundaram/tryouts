#include "hero.hpp"
#include "tileid.hpp"

#include <raymath.h>

#include <cassert>

Vector2 Hero::position() const
{
    // P = C + SV, where S is scale vector and V is half_size (basis) vector
    auto const pos_in_bbox = vec2Scale(m_bounds.m_half_size,
                                       m_anchor_scales_in_bounds);
    return Vector2Add(m_bounds.m_centre, pos_in_bbox);
}

void Hero::setPosition(Vector2 new_pos)
{
    assert(vec2IsValid(new_pos));
    // C = P - SV
    auto const SV = vec2Scale(m_anchor_scales_in_bounds,
                                 m_bounds.m_half_size);
    m_bounds.m_centre = Vector2Subtract(new_pos, SV);
}

// update m_state based on boolean values
// TODO: remove redundant flags; fold into m_state
void Hero::update(float /*ticks*/)
{
    // jump and fall have the same sprite due to lack of assets ;)
    if (!m_is_on_ground) {
        m_state = m_is_facing_left ? State::Jumping_Left : State::Jumping_Right;
    }
    else {
        if (m_is_walking)
            m_state = m_is_facing_left ? State::Walking_Left
                                    : State::Walking_Right;
        else
            m_state = m_is_facing_left ? State::Idle_Left : State::Idle_Right;
    }
}

void HeroSprite::update(float ticks)
{
    auto const state = m_hero->m_state;
    auto const state_idx = static_cast<uint8_t>(state);
    auto const start_frame = m_state_start_idx[state_idx];
    auto const end_frame = m_state_start_idx[state_idx + 1u];
    auto const total_frames = end_frame - start_frame;
    bool refill_duration = false;
    // hero's state change entails animation states reset
    if (m_state != state) {
        m_state = state;
        m_frame_offset = 0u;
        m_pending_duration = 0.0f;
        // if animated state, set refill flag to deduce new frame's duration
        refill_duration = (total_frames > 1);
    }
    else if (total_frames > 1) {
        m_pending_duration -= ticks;
        if (m_pending_duration <= 0.0f) {
            m_frame_offset = static_cast<uint8_t>((m_frame_offset + 1) %
                                                  total_frames);
            refill_duration = true;
        }
    }
    if (refill_duration) {
        // in case of animation, load up frame's persistance duration
        unsigned const frame_id = start_frame + m_frame_offset;
        auto const tile_id = static_cast<uint8_t>(m_tiles[frame_id].id());
        auto search = m_tile_duration.find(tile_id);
        if (search != m_tile_duration.end())
            m_pending_duration = search->second;
    }
}

uint8_t HeroSprite::getCurrentFrameId() const
{
    auto const state = m_hero->m_state;
    auto const state_idx = static_cast<uint8_t>(state);
    auto const start_frame = m_state_start_idx[state_idx];
    return static_cast<uint8_t>(start_frame + m_frame_offset);
}
