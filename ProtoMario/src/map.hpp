#ifndef MAP_HPP
#define MAP_HPP

#include "common.hpp"
#include "hero.hpp"

#include <string>
#include <vector>

struct Tileset;
struct Layer;
enum class TileProperty : uint8_t;

const char * const RESOURCE_DIR = "data/";

enum class GameState {
    RUNNING,
    OVER,
    WON
};

struct Map {
    static Map loadMap(std::string file_path);

    // width and height in pixels
    float width() const;
    float height() const;

    // Helpers

    // return col and row index given a pixel position
    std::tuple<bool, size_t, size_t> tileIndex(Vector2 pos) const;
    // return the top-most tile for a given index, if in bounds
    std::pair<bool, TileID> tile(size_t col, size_t row) const;
    // returns neighbouring tile for a given tile, if one exists
    std::tuple<bool, size_t, size_t> tileAtSide(size_t col, size_t row,
                                                Orientation o) const;
    TileProperty tileProperty(TileID tile_id) const;

    static float         tile_width;
    static float         tile_height;

    size_t               cols, rows;
    Hero                 m_hero;
    HeroSprite           m_hero_sprite;
    std::vector<Layer>   m_layers;
    std::vector<Tileset> m_tilesets;
    GameState            m_state;

#ifdef DEBUG_DRAW
    mutable
    std::vector<std::pair<BBox2D, Color>> m_dbg_boxes = {};
#endif
};

#endif // MAP_HPP
