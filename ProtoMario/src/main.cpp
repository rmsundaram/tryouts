#include "layer.hpp"
#include "map.hpp"
#include "tileset.hpp"
#include "renderer.hpp"
#include "updater.hpp"

#include <chrono>
using std::chrono::duration;

// Since HRC is almost always aliased to {steady,system}_clock it’s better to
// use the former directly; avoid latter as it isn’t monotonic i.e. it can go
// back and jump around too!
// https://en.cppreference.com/w/cpp/chrono/high_resolution_clock
// https://stackoverflow.com/q/1487695/183120
// https://stackoverflow.com/q/38252022/183120
// https://gamedev.stackexchange.com/a/131094/14025
typedef std::conditional<
  std::chrono::high_resolution_clock::is_steady,
  std::chrono::high_resolution_clock,
  std::chrono::steady_clock>::type Clock;
static_assert(Clock::is_steady,
               "Clock is not monotonically-increasing (steady).");

constexpr auto TARGET_FPS = 60;
// allow 10 fps wiggle room, but cut off update on frame drops beyond
constexpr auto MAX_MS_PER_FRAME = 1000.0f / (TARGET_FPS - 10);

int main()
{
    Map m = Map::loadMap(std::string(RESOURCE_DIR) + "map2.json");

    SetConfigFlags(FLAG_WINDOW_UNDECORATED | FLAG_MSAA_4X_HINT);

    // all calls involving a OpenGL context must follow this
    InitWindow(static_cast<int>(m.width()),
               static_cast<int>(m.height()),
               "Hola Mario!");
    {
        Renderer r;
        // load textures only after a OpenGL context is acquired
        for (auto const &t : m.m_tilesets)
            r.loadTexture(t.m_image);

        SetTargetFPS(TARGET_FPS);
        auto earlier = Clock::now();
        auto now = earlier;
        while (!WindowShouldClose()) {
            now = Clock::now();
            float const ticks = duration<float,
                                         std::milli>(now - earlier).count();
            if (ticks <= MAX_MS_PER_FRAME)
                Updater::update(m, ticks);
            r.render(m);
            earlier = now;
        }
    }
    CloseWindow();
}
