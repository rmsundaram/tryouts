#include "renderer.hpp"
#include "map.hpp"
#include "tileset.hpp"
#include "layer.hpp"

#include <raymath.h>

#include <cmath>
#include <limits>

void Renderer::loadTexture(const Image &img)
{
    m_textures.emplace_back(LoadTextureFromImage(img));
}

void Renderer::renderHero(Map const &m)
{
    auto pos = m.m_hero.position();

#ifdef DEBUG_DRAW
    DrawCircle(static_cast<int>(pos.x),
               static_cast<int>(pos.y),
               3,
               Color{255, 0, 0, 255});
#endif  // DEBUG_DRAW

    auto const frame_id = m.m_hero_sprite.getCurrentFrameId();
    auto const tile = m.m_hero_sprite.m_tiles[frame_id];
    auto const &tileset = m.m_tilesets[tile.tileset()];
    pos.x += m.m_hero_sprite.m_tile_offset.x;
    pos.y += m.m_hero_sprite.m_tile_offset.y;
    renderTile(tile, tileset, pos);
}

void Renderer::renderTile(TileID tileID,
                           Tileset const &tileset,
                           Vector2 dst_pt)
{
    auto const tileset_id = tileID.tileset();
    float const tile_width = Map::tile_width;
    float const tile_height = Map::tile_width;
    auto const src_offset = tileset.offset(tileID.id());
    Rectangle const src_rect { src_offset.x,
                               src_offset.y,
                               tile_width,
                               tile_height };
    DrawTextureRec(m_textures[tileset_id],
                   src_rect,
                   dst_pt,
                   WHITE);
}

namespace {
void render_game_over()
{
    DrawText("Ouch!  Game over mate...", 150, 200, 40, MAROON);
}

void render_game_won()
{
    DrawText("Well done!  You've made it.", 150, 200, 40, DARKGREEN);
}
} // unnamed namespace

void Renderer::render(Map const &m)
{
    BeginDrawing();
    ClearBackground(GOLD);

    for (auto i = 0u; i < m.m_layers.size(); i++) {
        auto const &l = m.m_layers[i];
        for (auto j = 0u; j < l.rows(); ++j) {
            for (auto k = 0u; k < l.cols(); ++k) {
                auto tileID = l.getTile(k, j);
                if (!tileID.isEmpty()) {
                    auto const &tileset = m.m_tilesets[tileID.tileset()];
                    renderTile(tileID,
                                tileset,
                                Vector2 {
                                  Map::tile_width * static_cast<float>(k),
                                  Map::tile_height * static_cast<float>(j)
                                });
                }
            }
        }
    }
    if (m.m_state == GameState::RUNNING)
        renderHero(m);
    else if (m.m_state == GameState::OVER)
        render_game_over();
    else {
        renderHero(m);
        render_game_won();
    }

#ifdef DEBUG_DRAW
    for (auto const &box_with_colour : m.m_dbg_boxes) {
        auto const &box = box_with_colour.first;
        auto const left_top = leftTop(box);
        auto const dims = Vector2Scale(box.m_half_size, 2.0f);
        DrawRectangleLines(static_cast<int>(left_top.x),
                           static_cast<int>(left_top.y),
                           static_cast<int>(dims.x),
                           static_cast<int>(dims.y),
                           box_with_colour.second);
    }
    m.m_dbg_boxes.clear();

    DrawFPS(static_cast<int>(m.width()) - 100, 10);
#endif  // DEBUG_DRAW

    EndDrawing();
}

Renderer::~Renderer()
{
    for (auto it = m_textures.rbegin(); it != m_textures.rend(); ++it) {
        UnloadTexture(*it);
        m_textures.pop_back();
    }
}
