#ifndef UPDATER_HPP
#define UPDATER_HPP

#include "common.hpp"
#include "map.hpp"

namespace Updater {

// constants per millisecond
Vector2 constexpr GRAVITY{0.0f, 0.0005f};
Vector2 constexpr JUMP{0.0f, -0.023f};
float constexpr JUMP_CUTOFF = -0.013f;
static_assert (JUMP.y < JUMP_CUTOFF,
               "Cut-off jump must be smaller than jump velocity");

Vector2 constexpr FRICTION{0.65f, 1.0f};
Vector2 constexpr WALK{.0038f, 0.0f};

void update(Map &m, float dt);

}

#endif // UPDATER_HPP
