#include "tileid.hpp"

#include <limits>
#include <cassert>

TileID::TileID(unsigned tileset_id, unsigned id)
    : m_tileset_tile_id{0u}
{
    setTileset(tileset_id);
    setTileId(id);
}

void TileID::setTileset(unsigned tileset_id)
{
    assert(tileset_id <= MAX_TILESET_ID);
    static_assert (sizeof(unsigned) >= 4,
                   "Invalid assumption on unsigned's size");
    auto constexpr BIT_MASK = std::numeric_limits<unsigned>::max();
    // clear MSB 4 bits
    m_tileset_tile_id &= static_cast<uint16_t>(~(BIT_MASK << TILE_ID_BITS));
    // copy those bits from tileset_id
    m_tileset_tile_id |= static_cast<uint16_t>(tileset_id << TILE_ID_BITS);
}

void TileID::setTileId(unsigned int tile_id)
{
    assert(tile_id <= MAX_TILE_ID);
    auto constexpr BIT_MASK = std::numeric_limits<unsigned>::max();
    m_tileset_tile_id &= static_cast<uint16_t>(BIT_MASK << TILE_ID_BITS);
    m_tileset_tile_id |= static_cast<uint16_t>(tile_id);
}

unsigned TileID::tileset() const
{
    unsigned tileset_id = m_tileset_tile_id;
    tileset_id >>= TILE_ID_BITS;
    return tileset_id;
}

unsigned TileID::id() const
{
    return (m_tileset_tile_id &
            ~(std::numeric_limits<unsigned>::max() << TILE_ID_BITS));
}

bool TileID::isEmpty() const
{
    return m_tileset_tile_id == 0;
}
