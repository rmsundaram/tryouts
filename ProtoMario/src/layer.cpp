#include "layer.hpp"
#include <cassert>

Layer::Layer(int id, size_t cols, size_t rows)
    : m_id{id}
    , m_cols{cols}
    , m_rows{rows}
    , m_x_offset {0}
    , m_y_offset {0}
{
}

void Layer::setOffsets(int x, int y)
{
    m_x_offset = x;
    m_y_offset = y;
}

size_t Layer::cols() const
{
    return m_cols;
}

size_t Layer::rows() const
{
    return m_rows;
}

int Layer::getId() const
{
    return m_id;
}

TileID Layer::getTile(unsigned col, unsigned row) const
{
    assert((col < m_cols) && (row < m_rows));

    auto const idx = row * m_cols + col;
    return m_tileIDs[idx];
}
