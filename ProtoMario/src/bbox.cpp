#include "bbox.hpp"

#include <raymath.h>

#include <cmath>
#include <algorithm>

Vector2 vec2Diff(Vector2 a, Vector2 b)
{
    auto const s = Vector2Subtract(a, b);
    return {std::abs(s.x), std::abs(s.y)};
}

Vector2 vec2Scale(Vector2 v, Vector2 s)
{
    return {v.x * s.x, v.y * s.y};
}

bool operator*(BBox2D const &a, BBox2D const &b)
{
    auto const d = vec2Diff(a.m_centre, b.m_centre);
    auto const l = Vector2Add(a.m_half_size, b.m_half_size);
    return (d.x < l.x) && (d.y < l.y);
}

Vector2 leftTop(BBox2D const &box)
{
    return Vector2Subtract(box.m_centre, box.m_half_size);
}
Vector2 rightBottom(BBox2D const &box)
{
    return Vector2Add(box.m_centre, box.m_half_size);
}

BBox2D move(BBox2D const& box, Vector2 displacement)
{
    return {Vector2Add(box.m_centre, displacement), box.m_half_size};
}

BBox2D tileBounds(size_t col, size_t row, float width, float height)
{
    Vector2 const half_size{width * 0.5f, height * 0.5f};
    return BBox2D{ Vector2Add(Vector2{static_cast<float>(col) * width,
                                      static_cast<float>(row) * height},
                              half_size),
                   half_size };
}

Vector2 vec2Inf()
{
    return {std::numeric_limits<float>::infinity(),
            std::numeric_limits<float>::infinity()};
}

// refer pen_depth.js
BBox2D intersection(BBox2D const &a, BBox2D const &b)
{
    auto const lt_A = leftTop(a), rbA = rightBottom(a);
    auto const lt_B = leftTop(b), rbB = rightBottom(b);
    auto const left = std::max(lt_A.x, lt_B.x);
    auto const right = std::min(rbA.x, rbB.x);
    if (left > right)
        return BBox2D{vec2Inf(), vec2Inf()};
    auto const top = std::max(lt_A.y, lt_B.y);
    auto const bottom = std::min(rbA.y, rbB.y);
    if (top > bottom)
        return BBox2D{vec2Inf(), vec2Inf()};
    auto const half_size = Vector2{ (right - left) * 0.5f,
                                    (bottom - top) * 0.5f };
    auto const centre = Vector2{Vector2Add(Vector2{left, top}, half_size)};
    return BBox2D{centre, half_size};
}

// refer rect_minkowski.lua
Vector2 penetrationVector(BBox2D const &box_a, BBox2D const &box_b)
{
    auto const lt_A = leftTop(box_a);
    auto const rb_A = rightBottom(box_a);
    auto const lt_B = leftTop(box_b);
    auto const rb_B = rightBottom(box_b);
    auto const left = lt_A.x - rb_B.x;
    auto const right = rb_A.x - lt_B.x;
    auto const top = lt_A.y - rb_B.y;
    auto const bottom = rb_A.y - lt_B.y;

    if ((left < 0) && (right > 0) && (top < 0) && (bottom > 0))
    {
        auto const norm = std::min({-left, -top, right, bottom});
        if ((norm + left) == 0.0f)
            return Vector2{left, 0.0f};
        else if ((norm + top) == 0.0f)
            return Vector2{0.0f, top};
        else if ((norm - right) == 0.0f)
            return Vector2{right, 0.0f};
        return Vector2{0.0f, bottom};
    }
    // only touching or disjoint
    return vec2Inf();
}

bool vec2IsValid(Vector2 v)
{
    return std::isfinite(v.x) && std::isfinite(v.y);
}
