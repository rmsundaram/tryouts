#include "tileset.hpp"
#include "map.hpp"

#include <cassert>
#include <cstdlib>

Tileset::Tileset(unsigned id,
                 unsigned tile_count,
                 unsigned columns,
                 Image src_img)
    : m_id(static_cast<uint8_t>(id))
    , m_count(static_cast<uint16_t>(tile_count))
    , m_cols(static_cast<uint16_t>(columns))
    , m_tile_props(tile_count, TileProperty::Generic)
    , m_image(src_img)
{
}

Tileset::Tileset(Tileset&& other)
    : m_id {other.m_id}
    , m_count {other.m_count}
    , m_cols {other.m_cols}
    , m_tile_props {std::move(other.m_tile_props)}
    , m_image {other.m_image}
{
    other.m_image.data = nullptr;
}

Tileset& Tileset::operator=(Tileset&& that)
{
    if (this != &that)
    {
        m_id = that.m_id;
        m_count = that.m_count;
        m_cols = that.m_cols;
        m_tile_props = std::move(that.m_tile_props);
        m_image = that.m_image;

        that.m_image.data = nullptr;
    }
    return *this;
}

Tileset::~Tileset()
{
    if (m_image.data) {
        UnloadImage(m_image);
        m_image = {};
    }
}

void Tileset::setProperty(unsigned int tile_id, TileProperty properties)
{
    assert(tile_id < m_tile_props.size());
    m_tile_props[tile_id] = properties;
}

Vector2 Tileset::offset(unsigned tile_id) const
{
    assert(tile_id < m_count);
    auto const xy = std::div(static_cast<long>(tile_id),
                             static_cast<long>(m_cols));
    return {static_cast<float>(xy.rem) * Map::tile_width,
            static_cast<float>(xy.quot) * Map::tile_height};
}
