#ifndef BBOX_HPP
#define BBOX_HPP

#include "common.hpp"

constexpr auto eps = 0.00001f;

struct BBox2D
{
    Vector2 m_centre;
    Vector2 m_half_size;
};

Vector2 leftTop(BBox2D const &box);
Vector2 rightBottom(BBox2D const &box);
BBox2D move(BBox2D const& box, Vector2 displacement);

// returns true if intersecting, false if disjoint or touching
bool operator*(BBox2D const &a, BBox2D const &b);

// make BBox2D from position
BBox2D tileBounds(size_t col, size_t row, float width, float height);

// assumes that supplied BBox2D intersect, check wtih a * b first
// returns the intersection of two BBox2Ds
BBox2D intersection(BBox2D const &a, BBox2D const &b);

// returns penetration vector to be applied on B to resolve collision
Vector2 penetrationVector(BBox2D const &box_a, BBox2D const &box_b);

// component-wise (unsigned) difference in magnitude
Vector2 vec2Diff(Vector2 a, Vector2 b);

// component-wise v * s
Vector2 vec2Scale(Vector2 v, Vector2 s);

// returns a degenerate Vector2 with components set to infinity
Vector2 vec2Inf();

bool vec2IsValid(Vector2 v);

#endif // BBOX_HPP
