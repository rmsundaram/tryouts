<meta charset="utf-8">

    **Character Controller for a Platformer**
            **Sundaram Ramaswamy**

# Basics: The Physics

> "A force is a vector that causes an object with mass to accelerate."

$$
F = m \times a
$$

A force -- usually a push/pull -- acting on a body, for a given amount of time, accelerates it i.e. its velocity[^fn1] increases/decreases for that duration due to the force.  When the force stops, acceleration wears but the body continually changes position -- with a constant velocity -- _until_ an external force acts on it; for the body to rest, it should be decelerating such that velocity becomes $\vec{0}$.

> "An object at rest stays at rest and an object in motion stays in motion at a constant velocity unless acted upon by an unbalanced force."

$$
v = \frac{s}{t} \qquad s = v \times t \\[4ex]
a = \frac{v - u}{t} \qquad v = u + at
$$

where $s$ is displacement, $u$ and $v$ are old and new velocities; $a$ is acceleration, the change in velocity over time.  For instantaneous -- rather than average -- values of these quantities, differentiate w.r.t time

$$
v = \frac{ds}{dt} \qquad  a = \frac{dv}{dt} = \frac{d^2s}{dt^2}
$$

Rate of change of position over time is velocity i.e. position changes with time, by some velocity during that time.  Velocity changes with time, by some acceleration during that time.  New position will be old position, plus the displacement delta ($ds = v \times dt$)

1. `pos' = pos + vel ∙ dt`
2. `vel' = vel + acc ∙ dt`

The first time derivative of position is velocity; its second derivative is acceleration.

!!! Warning
    Force is the _cause_ of acceleration; not to be confused with its rate of change over time -- [jerk][] -- position’s third derviative.  Force’s S.I. unit is $kg \frac{m}{s^2}$ while jerk’s is $\frac{m}{s^3}$.

!!! Note
    Though [higher-order derivatives of position exist][jounce], we stop with the second -- acceleration.

> "For every action there is an equal and opposite reaction i.e. forces always occur in pairs.  The two forces are of equal strength, but in opposite directions."

Let’s say you push against a wall.  The wall doesn’t actively decide to push back on you.  There is no “origin” force.  Your push simply includes both forces, referred to as an “action/reaction pair.”  However, the forces act on different objects.  When you push the wall with a force F, the hand is pushed by −F.  This observation is used when modelling friction: friction points in the opposite direction of velocity.

## Code

Examining [Nature of Code][]’s [`Mover`][mover-code] class (from Chapter 2 `Forces`) would concretize the above ideas; though named `Mover`, it’s more like `Body`.

~~~~~~~~~~~~~~ cpp linenumbers
struct Mover
{
  Vector position {30, 30};
  Vector velocity {};
  Vector acceleration {};

  float mass = 1.0f;                   // mass equals 1 for simplicity

  void applyForce(Vector force) {      // Newton’s second law, F = ma.
    Vector f = force / mass;           // Receive a force, divide by mass,
    acceleration += f;                 // and accumulate in acceleration.
  }
 
  void update() {
    velocity += acceleration;          // Motion 101 from Chapter 1
    position += velocity;

    acceleration *= 0;                 // Clear acceleration each frame!
                                       // Acceleration ceases unless force(s)
                                       // keep acting over time (across frames)
  }
};

void Game::update() {
  Vector wind {0.01, 0};               // Unlike gravity (applied every iteration)
                                       // this is applied on user input only.

  // Apply both forces to each
  for (auto i = 0u; i < movers.size(); i++) {
    Vector gravity {0, 0.1 * mover[i].mass};  // scale by mass since gravity is
                                              // not really a force, but
                                              // acceleration; yet we use
                                              // applyForce to factor it in.

    movers[i].applyForce(wind);
    movers[i].applyForce(gravity);

    movers[i].update();
    movers[i].checkCollisionWithWorld();
  }
}
~~~~~~~~~~~~~~
[Listing [mover]: Basic physics update cycle]

**Line 18**: If acceleration isn’t reset every iteration, a ceased force would still be in effect; it’d still be increasing or decreasing velocity e.g. even after a wind force is no longer applied, its respective acceleration would be changing velocity every iteration.  Even worse, it would add onto itself from the previous frame[^fn2], since we are accumulating forces!  _Acceleration, in this simulation, has no memory; it is simply calculated based on the environmental forces present at a moment in time._  As we are not worried about the rate of change of acceleration (as noted above, we don’t consider higher-order derivatives of position), we don’t maintain jerk to keep changing acceleration across update cycles -- unlike for velocity, for which we maintain acceleration.

!!! Note
    $g$ = 9.81 m/s² represents _acceleration_ due to gravity -- a _force_ exerted by earth that changes a body’s velocity over time.  $g$ denotes the rate of change, acceleration, not the force -- gravity -- itself.

**Line 30**: We model gravity as a force; to make sure that the acceleration due to gravity on two bodies with different mass is the same, we scale it by each body’s mass.  Consider earth’s gravity on a light and heavy ball[^fn3] -- though the force acting on each would be different (higher on the heavier one to bring it to the same acceleration) -- it’d be such that their accelerations are the same: 9.8 m/s². In $F = m \times a$, for $a$ to be constant for all bodies, $F$ would be larger for bodies with larger $m$.


# Game Update

## Gravity

> "To build a physics simulation, you could write a complex set of branching logic that takes the Koala’s state into account and decides which forces to apply based on that state. But, this would quickly become very complicated — and it isn’t really how physics works. In the real world, gravity is always pulling things towards the earth. So you’ll add the constant force of gravity to the Koala in every frame."

We’ve two possible approaches to implement gravity:

1. Apply every frame
    - Check collision to check if player is falling or not
    - Jump needs to be greater than some escape velocity
    - **Merit**: No need of seperate check if on ground
    - **Merit**: Collision check decides if on ground, if yes allow jump
2. Apply only in certain state(s) e.g. _falling_
    - Possible simple jump code
    - Manual on-ground check
    - **Merit**: Neglibibly optimised
    - **Demerit**: Cumbersome state management

Approach 1 feels more natural, closer to reality, less hacky.

## Behaviour

1. Gravity will always be ON.
2. Forces aren’t treated binary; every force will have momentum that continues to move the object across frames.
3. A jump overcomes gravity for a few frames.
4. A force pushing an object is countered by friction, and it gradually stops the object.

> "Hero will have a velocity variable acted upon in each frame by a number of forces, including gravity, forward/jumping forces supplied by the user, and friction, which will slow and stop him."

## Algorithm

1. Update `velocity` based on input
2. Apply gravity step to `velocity`
3. `pos' = pos + (dt * velocity)`
4. Check collision with world w.r.t `pos'`
5. Integrate and arrive at `new_pos`
6. Update state based on `new_pos` and previous state
7. Update `velocity` based on state

!!! Note
    Not gravity, but _gravity step_ -- gravity scaled by `dt` -- is added to velocity.  We want gravity to be proportional to number of ticks passed; math agrees too $a = \frac{d^2s}{dt^2}$ .

# References

1. [Nature of Code][], Chapter 1. Vectors, Chapter 2. Forces
2. [How to Make a Platform Game Like Super Mario Brothers](https://www.raywenderlich.com/2891-how-to-make-a-platform-game-like-super-mario-brothers-part-1)
3. [How to make a 2d platform game](https://wildbunny.co.uk/blog/2011/12/14/how-to-make-a-2d-platform-game-part-2-collision-detection/)
4. [Good 2D Platformer Physics](https://gamedev.stackexchange.com/questions/2799/good-2d-platformer-physics)
5. [What is the term used for the third derivative of position?](http://www.math.ucr.edu/home/baez/physics/General/jerk.html)
6. [Essential Mathematics for Aspiring Game Developers](https://youtu.be/DPfxjQ6sqrc?t=2435)

# See Also

1. [Sonics Physics Guide][]: highly referenced
2. [Physics in platformer games][]: different jump kinds and gravity
3. [Platformer Physics][platformer101]: fundamental equations and deducing constant values
4. [The guide to implementing 2D platformers][]: different platformers with implementation
5. [Basic 2D Platformer Physics][]: four parts including ledge grabbing
6. [A Platform Game][] in Eloquent Javascript, 3rd Edition
7. Defold’s [Platformer Tutorial][]
8. [Simple 2D Game Physics][] (Gravity, Jumping, Movement & Block Collision)
9. [Advanced Platfomer Movement][] including forgiving jump
10. [Tiny Platformer][]
11. [Designing a 2D Jump][]

[Nature of Code]: https://natureofcode.com/book/
[mover-code]: https://natureofcode.com/book/chapter-2-forces/#chapter02_example1
[jerk]: https://en.wikipedia.org/wiki/Jerk_(physics)
[jounce]: https://en.wikipedia.org/wiki/Jounce
[Galileo's Leaning Tower of Pisa experiment]: https://en.wikipedia.org/wiki/Galileo%27s_Leaning_Tower_of_Pisa_experiment
[Sonics Physics Guide]: http://info.sonicretro.org/Sonic_Physics_Guide
[platformer101]: https://error454.com/2013/10/23/platformer-physics-101-and-the-3-fundamental-equations-of-platformers/
[Physics in platformer games]: https://2dengine.com/?p=platformers
[Tiny Platformer]: https://codeincomplete.com/posts/tiny-platformer/
[The guide to implementing 2D platformers]: http://higherorderfun.com/blog/2012/05/20/the-guide-to-implementing-2d-platformers/
[Basic 2D Platformer Physics]: https://gamedevelopment.tutsplus.com/tutorials/basic-2d-platformer-physics-part-1--cms-25799
[A Platform Game]: https://eloquentjavascript.net/16_game.html
[Designing a 2D Jump]: https://www.gamasutra.com/blogs/MohanRajagopalan/20140813/223251/Designing_a_2D_Jump.php
[Platformer Tutorial]: https://www.defold.com/tutorials/platformer/
[Simple 2D Game Physics]: https://www.codeproject.com/Tips/881397/%2FTips%2F881397%2FCsharp-Simple-D-Game-Physics-Gravity-Jumping-Movem
[Advanced Platfomer Movement]: https://www.instructables.com/id/Advanced-Platformer-Movement/

[^fn1]: Speed is just the norm of velocity; no direction, always positive.

[^fn2]: When a new force is applied, in current iteration, acceleration due to old, now-dead forces would still be added to acceleration cased by new one.

[^fn3]: [Galileo's Leaning Tower of Pisa experiment][]; also explained in [Nature of Code][], §2.6 Gravity on Earth and Modeling a Force


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none', inlineCodeLang: 'c++' };</script>
