#!/usr/bin/perl

use v5.20;
use strict;
use warnings;

my %h;
my $page_id = 0;
my $base = [1, 2, 3];           # $base is a ref. to anonymous array
my $actual = [1, 2, 32, 3];
my $n_base = @$base;            # assign length to a scalar
my $n_actual = @$actual;

# h is a direct hash: notice that parenthesis is used here
# h = (
#         0 => {                                    # $h{'0'} is a reference to some anonymous hash created with {}
#                  n_base => <some val>
#                  n_actual => <some scalar>
#                  base => <some anon array>
#                  actual => <some anon array>
#              }
#     )
$h{$page_id}{n_base} = $n_base;
$h{$page_id}{n_actual} = $n_actual;
$h{$page_id}{base} = $base;
$h{$page_id}{actual} = $actual;

my $hr = \%h;
say "Value of base ($n_base): @$base";
# similar to multi-dimensional array reference syntax
say "Value of actual ($hr->{$page_id}->{n_actual}): @{$hr->{$page_id}->{actual}}";

