#!/usr/bin/env perl

use v5.14;
use strict;
use warnings;

# prints all the files ending .md in current dir
sub list_md {
    my @files;
    opendir(DIR, ".") or die $!;
    @files = grep {/\.md$/i} readdir(DIR);
    foreach my $file (@files) {
        say $file;
    }
}

# takes in a path and a name e.g. /home and log
# tries to create dir /home/log_0, if that exists
# tires for /home/log_1 and so on
sub create_next_dir {
    my $dir = shift;
    my $name = shift;
    # create a new dir keeping old ones alive
    my $i = 0;
    # http://stackoverflow.com/q/7137873
    until (mkdir "${dir}/${name}_${i}") {
        ++$i;
    }
}

# useful to create progressive names like for files
# without overwriting existing ones
sub get_next_name {
    my $name = shift;
    my $id = 0;
    while (-e "${name}_${id}") {
        ++$id;
    }
    return "${name}_${id}";
}

# takes in a list of dirs and iterates over them printing filenames without the '.log' extension
while (@ARGV) {
    my $dir = shift @ARGV;

    # all files in $dir will be fed to the lambda
    # including '.' and '..' which needs to be avoided
    use File::Find qw(finddepth);
    my @files;
    finddepth(sub {
        # exit if not a log file, or the current or prev dir
        return if($_ eq '.' || $_ eq '..' || ($_ =~ /\.log$/));
        push @files, $File::Find::name;
              }, $dir);

    foreach my $file (@files) {
        next if (-d $file);
        say $file;
    }
}
