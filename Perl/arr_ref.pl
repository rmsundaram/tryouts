#!/usr/bin/env perl

use v5.20;
use strict;
use warnings;

my @arr = (1, 2, 5);            # direct array
say '@arr ('. scalar(@arr) .') = '."@arr";
my $ref = \@arr;                # reference to a direct array
my $l = @$ref;                  # scalar context; other way scalar(@$ref)
say "Its size is $l\nElement 0: " , $$ref[0];

my @arr_cpy = @$ref;
print("Copied array length: ". ($#arr_cpy + 1) . "\nElement 0: $arr_cpy[0]"); # $# can't be done with a reference

my $ref_arr = ['a', 2, 9.8];    # reference to an anonymous array
my $m = @$ref_arr;
say "\nLength of anonymous array: $m\nElement 0: $ref_arr->[0]"; # same as $$ref_arr[0]
print "Its contents: @{$ref_arr}";
