#!/usr/bin/env perl

use v5.14;
use strict;
use warnings;

# convert mp3 inside a wave to raw mp3
sub wav_mp3 {
    my @files;
    my $dir = shift;
    opendir(DIR, $dir) or die $!;
    @files = grep {/\.mp3$/i} readdir(DIR);
    foreach my $file (@files) {
        my $outfile = $file;
        $outfile =~ s/_/ /;
        $outfile = "[Converted]_${outfile}";
        my $cmd = qq(vlc -I dummy --sout='#transcode{acodec=mp3,channels=2,samplerate=44100,ab=128,vcodec=dummy}:standard{access=file,mux=raw,dst="$dir\\$outfile"}' '$dir\\$file' vlc://quit);
        say $cmd;
        system($cmd);
    }
}

# takes in a list of dirs and iterates over them transcoding wav mp3 to raw mp3
while (@ARGV) {
    my $dir = shift @ARGV;
    wav_mp3($dir);
}
