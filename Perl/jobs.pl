#!/usr/bin/env perl

use v5.14;
use strict;
use warnings;

use Win32::Job;
use File::Copy;

my $exec_path = shift;          # executable's path
my $files_dir = shift;          # files to act on
my $ext = shift;                # extension to work on
my $p_cnt = shift;              # parallel process count

my @files;
opendir(DIR, $files_dir) or die $!;
@files = grep {/\.${ext}$/i} readdir(DIR);

foreach my $file (@files) {
    say $file;
}

my $outfile = "Timeouts";
my $id = 0;
while (-e "${outfile}_${id}.txt") {
    ++$id;
}
my $timeout_log;
open $timeout_log, ">", "${outfile}_$id.txt" or die $!;

my $cnt;
for ($cnt = 0; $cnt < @files; $cnt += $p_cnt) {
    # create a job for this batch
    my $job = Win32::Job->new or die $!;
    # add subprocesses to job
    my %jobs_files;
    # number of files to process in this lot
    my $pending = (($cnt + $p_cnt) <= @files) ? $p_cnt : (@files - $cnt);
    for (my $i = 0; $i < $pending; ++$i) {
        my $args = qq("${exec_path}" "${files_dir}/${files[$cnt + $i]}" -arg1);
        say $args;
        my $pid = $job->spawn($exec_path, $args) or die $!;
        $jobs_files{$pid} = $files[$cnt + $i];
    }
    # 5 minute time out for all subprocesses to complete
    my $ok = $job->run(5 * 60);
    my $s = $job->status();
    # log timed-out jobs' filenames
    while ((my $job, my $stat) = each $s) {
        say $timeout_log $jobs_files{$job} if ($stat->{"exitcode"} == 293);
    }
}

# delete temp files
unlink glob "$files_dir/*.log";
