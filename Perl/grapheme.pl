#!/usr/bin/env perl

# This pragma is important.  Perl sources default to bytes pragma thereby
# forcesing byte semantics on |length|.  This pragma defines the meaning of
# logical characters explained in |length|’s documentation.
# https://stackoverflow.com/a/1326654/183120
use utf8;
use 5.32.0;
use charnames qw(:full);
use Unicode::GCString;

binmode STDOUT, ':encoding(UTF-8)';

# https://www.effectiveperlprogramming.com/2011/06/treat-unicode-strings-as-grapheme-clusters/
my $s1  = "நியாயம்";    # 4 grapheme clusters; 7 code points
my $s2 = Unicode::GCString->new("நியாயம்");

say "Plain Perl length -> ", length $s1;    # code points
say "Unicode::GCString  -> ", $s2->length;  # grapheme clusters
