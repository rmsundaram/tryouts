#!/usr/bin/env perl

use v5.20;
use strict;
use warnings;

use XML::LibXML;
use POSIX;

my $parser = XML::LibXML->new();
my $doc = $parser->parse_file($ARGV[0]);
my @colours;

foreach my $key ($doc->findnodes('/colors/key')) {
    push @colours, $key->to_literal;
}

my @std_colours = ('Black', 'Red', 'BoldGreen', 'BoldYellow', 'BoldBlue', 'BoldMagenta', 'BoldCyan', 'BoldWhite', 'Green', 'Yellow', 'Blue', 'Magenta', 'Cyan', 'White', 'BoldBlack', 'BoldRed');

foreach my $dict ($doc->findnodes('/colors/dict')) {
    #    say shift @colours;
    my @channels;
    foreach ($dict->findnodes('real')) {
        my $real = $_->to_literal;
        unshift @channels, floor((255 * $real) + 0.5);
    }
    my $name = shift @colours;
    if (my $n = shift @std_colours) {
        $name = $n;
    }
    $name =~ s/ //g;
    print $name, ' = ', join(',', @channels), "\n";
}
