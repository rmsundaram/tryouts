-- tested on Löve 11.4 (Mysterious Mysteries)

local screen_width = love.graphics.getWidth()
local screen_height = love.graphics.getHeight()
-- Move origin to bottom-left corner and flip axes
local love_to_math_xform = love.math.newTransform()
love_to_math_xform:translate(0, screen_height)
love_to_math_xform:scale(1, -1)
local cell_size = 100
local lines = {}

function love.draw()
  love.graphics.clear(1.0, 0.937, 0.835)

  -- draw grid
  love.graphics.setColor(0.3921, 0.5843, 0.9294)
  for i, line in ipairs(lines) do
    love.graphics.line(line)
  end

  -- draw numbers
  local text_height = love.graphics.getFont():getHeight()
  love.graphics.setColor(1, 1, 1)
  love.graphics.print({{0.6, 0.6, 0.6}, '0'}, 5, screen_height - text_height - 5)
  for i = 1, math.floor(screen_width / cell_size) do
    love.graphics.print({{0.6, 0.6, 0.6}, tostring(i * 10)},
                        5 + i * cell_size,
                        screen_height - text_height - 5)
  end

  love.graphics.push()
  love.graphics.applyTransform(love_to_math_xform)
  love_to_math_xform:inverse()
  for i = 1, math.floor(screen_height / cell_size) do
    love.graphics.print({{0.6, 0.6, 0.6}, tostring(i * 10)}, 5, i * cell_size, 0, 1, -1)
  end
  love.graphics.pop()

  love.graphics.push()
  local math_to_small_xform = love_to_math_xform:clone()
  math_to_small_xform:scale(10, 10)
  love.graphics.applyTransform(math_to_small_xform)
  draw(love)
  love.graphics.pop()
  love.graphics.setLineWidth(1)
  love.graphics.line(10, 10, 20, 30)
end

function draw(love)
  love.graphics.setColor(1, 0, 0)
  love.graphics.circle('fill', 10, 10, 0.5)
  love.graphics.circle('fill', 20, 30, 0.5)
  love.graphics.setLineWidth(.1)
  love.graphics.line(10, 10, 20, 30)
end

function love.update()
  if love.keyboard.isDown("escape") then
    love.event.quit()
  end
end

function love.load()
  for i = 1, math.floor(screen_width / cell_size) do
    l = { i * cell_size, 0, i * cell_size, screen_height }
    table.insert(lines, l)
  end

  for i = 1, math.floor(screen_height / cell_size) do
    l = { 0, i * cell_size, screen_width, i * cell_size }
    table.insert(lines, l)
  end
end
