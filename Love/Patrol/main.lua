Points = {}
Mode = 'Path'
Patrol = {}

Speed = 50 -- pixel/second

function love.update(dt)
  if Mode ~= 'Lerp' then
    return
  end
  idx = (Patrol.line * 2) + 1
  target_x, target_y = Points[idx], Points[idx+1]
  v_x = target_x - Patrol.loc[1]
  v_y = target_y - Patrol.loc[2]
  d = math.sqrt((v_x * v_x) + (v_y * v_y))
  if d > 0.75 then
    v_x = v_x / d
    v_y = v_y / d
    Patrol.loc[1] = Patrol.loc[1] + (v_x * dt * Speed)
    Patrol.loc[2] = Patrol.loc[2] + (v_y * dt * Speed)
  else
    Patrol.line = (Patrol.line + 1) % (#Points/2)
  end
end

function love.draw()
  -- Draw path
  if Mode == 'Path' then
    love.graphics.setColor(0.8, 0.8, 0.8)
  else
    love.graphics.setColor(0, 0.9, 0)
  end
  for i = 1, (#Points/2) do
    i = i * 2
    love.graphics.circle('fill', Points[i-1], Points[i], 10)
  end
  if #Points >= 6 then
    love.graphics.polygon('line', Points)
  elseif #Points == 4 then
    love.graphics.line(Points)
  end
  -- Draw patrol
  if Patrol.loc then
    love.graphics.setColor(0.8, 0.1, 0.1)
    love.graphics.circle('fill', Patrol.loc[1], Patrol.loc[2], 10)
  end
end

function love.mousepressed(x, y, button, istouch, presses)
  if Mode ~= 'Path' then
    return
  end
  if button == 1 then
    print(string.format('Add (%i, %i)', x, y))
    table.insert(Points, x)
    table.insert(Points, y)
  elseif #Points >= 6 then
    print(string.format('Close path', x, y))
    Mode = 'Lerp'
    Patrol.loc = {Points[1], Points[2]}
    Patrol.line = 1
  end
end

function love.keypressed(key, scancode, isrepeat)
  if scancode == 'escape' then
    love.event.quit(0)
  end
end
