function love.load()
  love.physics.setMeter(64)
  World = love.physics.newWorld(0, 9.81 * 64, true)

  -- create ground
  local w, h = love.window.getMode()
  Objects = { ground = {}, boxes = {}, balls = {} }
  Objects.ground.shape = love.physics.newRectangleShape(w-200, 60)
  Objects.ground.body = love.physics.newBody(World,
                                             w/2,
                                             h - (60/2),
                                             'static')
  Objects.ground.fixture = love.physics.newFixture(Objects.ground.body,
                                                   Objects.ground.shape)

  local box = {}
  box.shape = love.physics.newRectangleShape(20, 20)
  box.body = love.physics.newBody(World, 140, 80, 'dynamic')
  box.fixture = love.physics.newFixture(box.body, box.shape)
  table.insert(Objects.boxes, box)
end

function love.update(dt)
  World:update(dt)
end

function love.draw()
  love.graphics.setColor(0.486274509804, 0.701960784314, 0.258823529412)
  local points = {Objects.ground.shape:getPoints()}
  love.graphics.polygon('fill', Objects.ground.body:getWorldPoints(unpack(points)))

  love.graphics.setColor(1.0, 0.627450980392, 0.0)
  -- Sheepolution tutorials
  for _, b in ipairs(Objects.boxes) do
    -- if not b.body:isAwake() then
    --   love.graphics.setColor(0.301960784314, 0.81568627451, 0.882352941176)
    -- else
    --   love.graphics.setColor(1.0, 0.627450980392, 0.0)
    -- end
    love.graphics.polygon('fill', b.body:getWorldPoints(b.shape:getPoints()))
  end

  love.graphics.setColor(0.8, 0.627450980392, 0.8)
  for _, c in ipairs(Objects.balls) do
    love.graphics.circle('fill', c.body:getX(), c.body:getY(), c.shape:getRadius())
  end
end

function love.mousepressed(x, y, button, _, _)
  if button == 1 then
    local box = {}
    box.shape = love.physics.newRectangleShape(40, 40)
    box.body = love.physics.newBody(World, x, y, 'dynamic')
    box.fixture = love.physics.newFixture(box.body, box.shape)
    table.insert(Objects.boxes, box)
  else
    local ball = {}
    ball.shape = love.physics.newCircleShape(25)
    ball.body = love.physics.newBody(World, x, y, 'dynamic')
    ball.fixture = love.physics.newFixture(ball.body, ball.shape)
    table.insert(Objects.balls, ball)
  end
end

function love.keypressed(_, scancode, _)
  if scancode == 'escape' then
    love.event.quit(0)
  end
end
