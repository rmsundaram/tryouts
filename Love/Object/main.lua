Rect = {
  pos = { x = 0, y = 0},
  orientation = 0.7853981634, -- 45° in radians (clockwise)
  width = 100,
  height = 100,
}
Seconds_Per_Frame = 0

-- Called every frame; if last frame compute didn’t overshoot
-- it’d be called again in 16.667 ms i.e. 60 times in a second.
function love.update(dt)
  Rect.pos.x = Rect.pos.x + (dt * 50)
  Rect.pos.y = Rect.pos.y + (dt * 50)
  -- Uncomment to animate rect by spining
  Rect.orientation = Rect.orientation + (dt * 0.6)
  Seconds_Per_Frame = dt
end

function love.draw()
  local fps = 1 / Seconds_Per_Frame
  love.graphics.setColor{255, 0, 0, 255}
  love.graphics.print("FPS: " .. tostring(fps), 400, 10)
  love.graphics.setColor{255, 255, 255, 255}

  love.graphics.push()
  love.graphics.translate(Rect.pos.x, Rect.pos.y)
  love.graphics.rotate(Rect.orientation)
  love.graphics.rectangle('fill',
                          -Rect.width / 2,
                          -Rect.height / 2,
                          Rect.width,
                          Rect.height)
  love.graphics.pop()
end

function love.keypressed(_, scancode, _)
  if scancode == 'escape' then
    love.event.quit(0)
  end
end
