<meta charset="utf-8">
    **WIP: Lock Free Programming**

<!-- Merge with lock_free.md.html -->
<!-- TODO: Code trie building with goroutines -->

<!--

Watch
    Herb Sutter’s ‘Juggling Razor Blades’
      https://www.youtube.com/watch?v=c1gO9aB9nbs
    Hans Boehm’s ‘Using weakly ordered C++ atomics correctly’
      https://www.youtube.com/watch?v=M15UKpNlpeM
    Jeff Preshing’s ‘How Ubisoft Develops Games for Multicore - Before and After C++11’
      https://www.youtube.com/watch?v=X1T3IQ4N-3g

Read
    ./concurrency-primer.pdf
    GCC Atomic Sync
    C++ Concurrency in Action
    Parallel Programming: Concepts and Practice, §4 C++11 Multithreading has an in-depth thread pool discussion with implementation
    Game Engine Architecture, 3rd Edition

Refer
    https://bartoszmilewski.com/2008/12/01/c-atomics-and-memory-ordering/
    https://bartoszmilewski.com/2008/08/04/multicores-and-publication-safety/
    https://bartoszmilewski.com/2008/11/11/who-ordered-sequential-consistency/
    https://stackoverflow.com/questions/1787450/how-do-i-understand-read-memory-barriers-and-volatile
    https://preshing.com/20120612/an-introduction-to-lock-free-programming/
    https://paoloseverini.wordpress.com/2014/04/07/concurrency-in-c11/
    https://www.internalpointers.com/post/lock-free-multithreading-atomic-operations
-->

# Lock-Free Programming with Atomics

This is a digest of Herb Sutter’s [Lock-Free Programming with Atomics][sutter-lock-free] (high-level: how to use atomics) and not [atomic<> Weapons][sutter-atomic-weapons] (low-level: what they are and why).

## Why?

- Need better concurrency and scalability
  + Eliminate/reduce blocking/waiting in algorithms and data structures
  + Locks have their own issues (simplicity vs scalability trade-off)
    - Be fine-grained, but more finer-grained = harder to use locks
    - Take the right lock
    - Not forget to take the lock
    - Take in the correct order
- Be conservation: use it only when you need it (no premature optimisation)
- Only when it’s justified by measuring
- Lock-free isn’t a panacea

## Assumptions

- **Already measured** performance/scalability and proven you’ve a high-contention data structure
- **Measure again** after every change, confirm it’s an improvement over the previous one and confirm if it’s worth the complexity

## Traffic Analogy

- A single road is single threaded
- Multiple roads ≅ multiple threads, we need synchronisation
- Locks are traffic lights arbitrating over an intersection (a piece of shared road/state)
- Using lock-free we want to build clover leaves
  - Replace shared road with a bridge (elevate to different level)
  - Communication: try to build ramps so that traffic going one way can merge with traffic going the other way
  - When merging using ramps there’s a slow-down but not too much speed loss (no waits/blocks)

# Fundamentals

1. Key concept: Think in transactions (ACID)
  + Atomicity
    - Transaction commit is atomic; other code must not see partially-updated state
    - Think of your program taking the system from one balanced state to another
    - Every method is conceptually a transaction on the object; invariants hold only at the beginning and end of the function.  In between if we stop object would be in an inconsistent state.
    - [Lock-free] Publish each change using **one** atomic write (read-modify-write)
  + Consistency: a transaction takes data from one consistent state to another
  + Isolation: two transactions can never simultaneously operate on the same data
  + Durability: a committed transaction is never overwritten by another without seeing results of first transaction
    - [Lock-free] Make sure concurrent updates don’t interfere with each other

!!! Note: Single atomic write
    Do complex stuff but publish to outside world with one atomic write e.g. build a complex graph and finally swap `atomic<node*>` with root node.  Almost all uses for atomics are for primitives like `bool`s, `int`s and pointers.

2. Key tool: the ordered atomic variable (`std::atomic<T>` in C++)
  + Semantics
    - Each individual read and write is indivisible (without locking)
    - Reads/writes are guaranteed to never be reordered
    - Okay to reason about in source code order (as long as `std::memory_order` parameter is left to default)
    - Compare-and-swap (CAS) conceptually is _if atomic’s value is as expected then atomically change it to desired value or give back its value_
    - `compare_exchange` pseudocode below
    - `exchange` to blindly write returning current value
    
``` c++
bool atomic<T>::compare_exchange_strong(T& expected, T desired) {
  if (this->value == expected) {
    this->value = desired;
    return true;
  }
  else {
    expected = this->value;
    return false;
  }
}
```
!!! Tip: Good way to think about `i.compare_exchange()`
    With the `bool` result, you’re asking “am I the one who gets to change this atomic from _expected_ to _desired_”.

!!! Warning: `atomic<BigPOD>`
    When using atomic with a large POD a lock-based implementation gets used, a spin-lock is inserted under the covers; it’s never incorrect but is probably not performant.  Remember, spin locks are a lot slower if you run more threads than you’ve cores.

- Interleaving: atomics are inter-thread shared, mutable states; between successive calls on this thread its state can change
- Granularity: _two atomic operations together aren’t atomic_ even if they’re right next to each other i.e. each operation itself is atomic but there’s a window between two atomic operations where invariants can change

!!! Note: load vs store performance
    Generally atomic stores (writes) have significant overhead while loads (reads) have little to no overhead.

## Three levels of “Lock-Freedom”

- Wait-free (**no one ever waits**): every operation will complete in a bounded number of steps no matter what else is going on
  - Guaranteed system-wide throughput + starvation-freedom
- Lock-free (**someone makes progress**): thread A may wait because thread B is making progress
  - Guaranteed system-wide throughput but no starvation-freedom
- Obstruction-free (**progress if no interference**): a single thread execution in isolation (all obstructing threads suspended) for a bounded number of steps will complete its operation
  - No thread can be blocked by delays or failures of other threads
  - Doesn’t guarantee progress while two or more threads run concurrently
  - All lock-free 

> "wait-free (all threads progress) > lock-free (at least one does) > locking"

Informally _lock-free_ ≅ “not using mutexes” = {wait-free, lock-free, obstruction-free}

## Double Checked Locking

A technique used to reduce overhead of acquiring a lock by testing the locking criterion before acquiring the lock; [Wikipedia][dbl-chkd-locking] has code samples.

**Example**: a singleton returned `Widget* Widget::Instance()` that lazily creates the `Widget`; we don’t want two threads to make two instances.  Having a lock for the entire function means locking and unlocking every time just to read the pointer to the constructed object (numerous times throughout the app’s lifespaen), when you only want to guard/lock the construction (just once).

``` c++
atomic<Widget*> Widget::pInstance{nullptr};

Widget* Widget::Instance() {
  if (pInstance == nullptr) {        // 1. check
    lock_guard<mutex> lock{mutW};    // 2. acquire lock
    if (pInstance == nullptr)        // 3. check again
      pInstance = new Widget{};      // 4. create and assign
  }                                  // 5. release lock
  return pInstance;                  // 6. return pointer
}
```

**Solution**: Atomically check instance pointer (check 1) and return it if non-null.  If null take the lock (mutex), check instance pointer again (check 2) and create if null else return it.  The second check is to avoid the situation where two threads reached the lock acquiring code, the first one to acquire will create, while the second one will skip creation due to check 2.  This code thread-safe only if `pInstance` is atomic otherwise it has a race condition.

!!! Note: Compilers are atomic-aware
    Compilers know about speciality of atomic variables as something used for inter-thread communication.  Reading from `pInstance` is guarded by an acquire-barrier (code cannot be moved up across it), writing to `pInstance` is guarded by a release-barrier (code cannot be moved down across it).  More about acquire and release fences covered in [atomic weapons talk][sutter-atomic-weapons].  Any thread seeing a non-null value of `pInstance` sees everything the constructor did and the handoff is fine.


!!! Tip: Function local (magic) `static`s
    Standard requires `static Widget instance;` be thread-safe initialized.  They get initialized the first time control passes through them on any thread.  Internally compiler might do double checked locking or something equivalent.

# Idea

Modern hardware supports atomic operations; when used properly they help avoid locks in multi-threaded programming.

An atomic operation typically performs _read-modify-write_ sequence on a memory address --- indivisibly i.e. a thread cannot interrupt an atomic operation underway on another thread.  So an atomic increment would load a value, increment it, and store the result in such a way that no other thread can modify the value in the middle.  It also means the increment would be observable by all other threads right away.

!!! NOTE: Use case
    If you have two threads performing an atomic increment on the same value a thousand times, the result will be two thousand times greater than the initial value, irrespective of how these threads run. If they used a non-atomic increment, it would be possible for both to read the value, increment their copy (in a register) and then to store the result, so the observable result is a single increment, rather than two.

Even if not used directly, they’re used to implement high-level synchronization primitives like mutexes.

# Memory Barriers

No memory operation written before the barrier is allowed to complete after the barrier or vice-versa i.e. all memory writes are guaranteed to complete before reaching the barrier.  Likewise, all memory writes written after a barrier will start (and complete) only after the barrier.

``` c++
a = 1;
b = 2;
a = 3;
```

In the above code, the compiler[^cpus-reorder-too] is free to reorder the assembly instructions anyway it sees fit e.g.

``` c++
a = 3;
b = 2;
```

Unless the variables are qualified `volatile`[^volatile-purpose] --- in which case the compiler would ensure that every write to and read from a volatile memory address (a) remains (b) in the order it’s written in code[^volatile-seq-point] --- the compiler is free to remove and reorder operations.  This constraint applies only to objects qualified thus.

With a full barrier, you have the guarantee that nothing is reordered outside its reign; but you actually have a stronger guarantee than you need, which is not ideal for optimization.

# Memory Ordering

Memory orderings are something like transaction isolation levels in a database. They tell the system how much you care about such philosophical notions as causes preceding effects and time not having loops, as opposed to performance. Memory orderings are crucial to program correctness, and they are tricky to understand and reason about. Happily, the performance penalty for choosing sequential consistency (`memory_order_seq_cst`), the strictest memory ordering, is often quite low.  So when in doubt, use it.

# References

1. [Understanding C11 and C++11 Atomics](http://www.informit.com/articles/article.aspx?p=1832575)
2. _Programming Rust_, §19 Concurrency

[sutter-lock-free]: https://www.youtube.com/watch?v=c1gO9aB9nbs
[sutter-atomic-weapons]: https://herbsutter.com/2013/02/11/atomic-weapons-the-c-memory-model-and-modern-hardware/
[dbl-chkd-locking]: https://en.wikipedia.org/wiki/Double-checked_locking

# Footnotes

[^cpus-reorder-too]: Some CPUs will as well, unless explicit barrier instructions are inserted

[^volatile-purpose]: Pointers are marked `volatile` to prevent optimizer from optimizing away a read/write by making assumptions that it didn’t change value at a location; the hardware might have changed it e.g. `volatile char* on_switch = 0xBBB; on_off = true;` this write isn’t to the memory but to the hardware (memory range mapped to hardware); see [this](https://youtu.be/UNJrgsQXvCA?t=1145).

[^volatile-seq-point]: Every access made through a volatile-qualified type is treated as a visible side-effect for the purposes of optimization (that is, within a single thread of execution, volatile accesses cannot be optimized out or reordered with another visible side effect that is sequenced-before or sequenced-after the volatile access. This makes volatile objects suitable for communication with a signal handler, but not with another thread of execution i.e. it has nothing to do with multi-threading. -- [C++ Reference](https://en.cppreference.com/w/cpp/language/cv)


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none'};</script>

