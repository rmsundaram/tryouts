<meta charset="utf-8">
    **Atomics**

Hardware provides atomic instructions for [linearizability](https://en.wikipedia.org/wiki/Linearizability).  Below are some classes of atomic instructions (all operate indivisibly):

* Load / Store
* Swap
* [Test-and-set][]: write one to a memory location and return old value
* [Compare-and-swap][]: compares value at memory location with some value and swap if true
* [Fetch-and-add][]: increment contents at memory location by some value
* Load-link / store-conditional

Languages and libraries provide wrappers over these hardware-supported features like atomic counters.  These primitives help implement lock-free or lock-less data structures.

[Test-and-set]: https://en.wikipedia.org/wiki/Test-and-set
[Compare-and-swap]: https://en.wikipedia.org/wiki/Compare-and-swap
[Fetch-and-add]: https://en.wikipedia.org/wiki/Fetch-and-add

Linux RCU (read-copy-update) is one of the inspirations in forging C++ atomic’s release-acquire.

+ _Happens-Before_ is a like a mathematical relation the C++ standard specifies, not like the colloquial "A happens before B"; see [this][happens-before]
  - If operation A **happens-before** B, then the memory effects of A’s completion on one thread is effectively visible to another thread before commencing B
  - In single-threaded programs, in program order, line 1 happens-before 2 and so on
  - In multi-threaded programs, operation A happens-before B only if A has write-release and B has read-acquire memory model on the same atomic object
+ Memory model (`std::memory_order`) choice has two aspects
  - **Ordering constraints**: instructions are (dis?)allowed to be reorder across certain instruction
  - **Synchronisation**: just disabling reordering -- something that matters to only a single thread (the executing one) -- isn’t enough for thread B to see the effects of thread A; synchronisation at the hardware level is needed for the effects of A’s operation to be visible to B
+ _Synchronizes-With_ another relationship C++ defines; see [this][synchronizes-with]
  - At runtime, if thread B’s _read-acquire_ operation reads the value written by thread A’s _write-release_ operation on the **same atomic object**, then **all effects** to shared memory done **till write-release** by thread A will be visible to thread B **beyond read-acquire**

> "An atomic operation A that performs a release operation on an atomic object M **synchronizes with** an atomic operation B that performs an acquire operation on M and takes its value from any side effect in the release sequence headed by A."
>    -- C++ Standard, [atomics.order]

[Memory model synchronization modes -- GCC][gcc-atomics]

Normally compiler and hardware reorders instructions as part of optimisation.  This happens in both single and multi-threaded environments.  However, there’re restrictions on moving loads/stores around that sequential programmers are inherently familiar with e.g.

``` c++
x = 0;
y = 1;
if (y == 1)
  assert(x == 0);  // should always be true
```

Reading in source code order, a programmer expects `x == 0` if `y == 1` since the assignment to `x` _happens-before_ the assignment to `y` (at least) in the source code.  Though movement can happen this fundamental assumption shouldn’t be violated; this is called _sequential consistency_.  This is automatically given by C++ in single-threaded programs (see _as-if_ rule).  Using the memory model mode `std::memory_order_seq_cst`, the default mode for `std::atomic`, gives this guarantee but for multi-threaded environments too.

+ Memory barriers / fence instructions ([membar][])
  - These are costly as all hardware threads should see all the changes made (needing synchronisation) thusfar as a `fence` instruction is executed (not all hardware threads (logical CPUs) sit on the same core or socket) [1][cpu-parts]. See
    * _Jeff Preshing_’s [Acquire and Release Fences][acq-rel-semantics]
    * _Rainer Grimm_ [shows these fences with images][sync-ordering-series]
- Every opaque function call is a full fence; the compiler is disallowed to reorder instructions across something it doesn’t know -- unknown side effects of the function
- Two properties of atomics
  1. Atomicity
  2. Memory barrier / synchronisation point
- Memory model modes of atomic operations specify the strength of the data-sharing bond between participating threads; stronger = more synchronised and slower
  - Sequential consistency or sequentially consistent is the strongest
  - Relaxed is the weakest (only atomicity, no barriers)

Sutter talks about `std::atomic<BigPOD>` falling back to a locked implementation; this can be checked using `std::atomic<T>{}.is_{,always_}lock_free()`.  Such atomic objects have a bunch of leaf-level locks for this to work; not worth using!  `is_lock_free` at run time informs if some object is or isn’t lock-free; it’s at run time because of (padding and) alignment mostly.  If not properly aligned, it’d not be locked on some platforms.

Atomic operations do wait on each other for writes, reads are wait-free [Fedor CppCon 2017 talk][].   Two separate threads having their own atomic object which aren’t false shared -- different cache lines -- do the best.  Avoid false sharing by aligning per-thread data to separate cache lines.

Compile with `g++ -O3 -fsanitize=thread -g main.cpp` to know about data races a program might have to be detected at runtime as suggested by Rainer in his CppCon 2019 talk: Atomics, Locks, and Tasks.

> "think of two threads A and B as events that happen on two different planets that are orbiting with different speeds around a star. Times on these planets are relative, and synchronization between them takes place only when a signal that is issued from one planet reaches the other. The transmission of the signal takes time by itself, and when the signal reaches its destination, its source has moved on. So the mutual knowledge of the two planets is always partial."
>    -- _Modern C_, §19 Atomic access and memory consistency

# Relationships

The [Cardinal rule of memory ordering][mem-order-rule] says operations A and B are performed by the same thread, and A’s statement comes before B’s statement in program order, then A happens-before B.

> "Thou shalt not modify the behavior of a single-threaded program."

As per Jeff Preshing’s [Acquire and Release semantics][acq-rel-semantics]

+ **Release Semantics**
  - Applies to **shared-memory write** only
  - Through `store` or `read-modify-write` (_write-release_ operation)
  - Prevent any read or write preceding write-release in source code be reordered _below_
+ **Acquire Semantics**
  - Applies to **shared-memory read** only
  - Through `load` or `read-modify-write` (_read-acquire_ operation)
  - Prevent any read or write following read-acquire in source code be reordered _above_

## Synchronizes-With

!!! Tip: Payload + Guard
    In every _Synchronizes-With_ relationship, there’re two entities: _payload_ and _guard_.  The payload is the actual data thread A is initializing and thread B wants to read; while guard prevents thread B from accessing payload before initialization and lets thread A signal availability post-initialization.

_Synchronizes-With_ Relation between threads describes ways in which effects of source code-level operations -- even non-atomic ones -- are guaranteed to become visible to other threads.  This also implies a _happens-before_ relationship between the threads.

## Happens-before

``` c++
// Thread 1      // Thread 2
y = 1            if (x.load() == 2)
x.store (2);       assert (y == 1)
```

Although `x` and `y` are unrelated variables, the memory model (defaulting to `seq_cst`) specifies that the assert cannot fail. The store to `y` _happens-before_ the store to `x` in thread 1. If the load of `x` in thread 2 gets the results of the store that happened in thread 1, it must all see **all** operations that _happened before_ the store in thread 1, even unrelated ones. That means the optimizer is not free to reorder the two stores in thread 1 since thread 2 must see the store to `y` as well.

## Figure: Release-Acquire Semantics

****************************************************************************************
* int payload = 0;
* std::atomic<bool> READY {false};
*
*            Thread A                                 Thread B
*
* All ops to shared-mem till fence…
* \                            /
*  \  x = f1(); y = f2();     /  no reorder
*   \ payload = x + y;       /     beyond      if (READY.load(acquire))
*    '----------------------'  ◀┄┄ barrier ┄┄▶ .---------------------.
*   READY.store(true, release);               /  assert(payload);     \
*                                            /   fn(payload);          \
*                                           /                           \
*                                         … is guaranteed to be visible here.
*
****************************************************************************************

# Sequential Consistency Requirements/Guarantees

While Release-Acquire semantics gives a causal partial ordering between events sequential consistency makes this a stronger guarantee: total ordering.

>  "All atomic operations with sequential consistency occur in one global modification order, regardless of the atomic object they are applied to."
>      -- _Modern C_, §19.3 Sequential consistency

Summary of _Hans Boehm_’s [Using weakly ordered C++ atomics correctly][weak-atomics-boehm] (CppCon 2016).

Mutexes give atomicity at a higher (coarser) level hence may not be as performant; there’re issues of deadlocks, not taking the right lock, etc.

- I: indivisible (required by all models including _relaxed_ to prevent torn-reads/torn-writes)
- S: all memory operations -- loads and stores -- preceding an atomic store are visible to other threads (_store-release_)
- L: loads following an atomic load shouldn’t take effect until it’s complete (_load-acquire_)
  - In other words loads should commence only after completion of preceding atomic load
- SL: atomic stores are not reordered with subsequent atomic loads

## Message Passing Example

One thread initializes data and signals other threads that initialization is complete and data is safe to access.  Without S or L guarantees this won’t work i.e. thread 2’s assert will fail.  Here `atomic` is used as much for synchronisation as it’s for indivisibility.

``` c++
int x;
std::atomic<bool> x_init{false};

// Thread 1                    // Thread 2
x = 17;                        if (x_init.load())
x_init.store(true);              assert(x == 17);
```

- Thread 1, without the S guarantee, might signal thread 2 before `x` is initialized.
- Thread 2, without the L guarantee, might have accessed `x` (though inside a condition, modern processors speculate) only to find it not `17`.

## Dekker’s Example

Needs all 4 guarantees, importantly SL, without which it breaks -- both traffic gets green!

``` c++
std::atomic<bool> x{false}, y{false};

// Thread 1                             // Thread 2
x.store(true);                          y.store(true);
if (!y.load())                          if (!x.load())
  make_EW_green();                        make_NS_green();
```

# Weakly Ordered Atomics

+ `memory_order_acquire`, `memory_order_release`, `memory_order_acquire_release`
  - Sacrifice SL guarantee
  - Reads/Loads generally need `acquire`
  - Writes generally need `release`
  - RMW generally need `acquire_release`
+ `memory_order_relaxed`
  - Sacrifice everything except (no ordering) I
  - Accesses to the same location shouldn’t be reordered still

## Valid uses of `memory_order_relaxed`

* Primary usage is single-word data structures e.g. atomic counter updated by different threads and **we don’t care about result during the concurrent processing**; the result is read only after properly synchronising/joining between the threads.  Since `relaxed` ensure ordering of operations on that one, same memory location, this is a valid use-case.
* Safe to use if an operation doesn’t program-correctness e.g. computing a guess for `compare_exchange`

``` c++
// if ‘load’ is incorrect ‘while’ will do one extra iteration; that's okay
old = x.load(memory_order_relaxed);
while (x.compare_exchange_weak(old, foo(old))) {}
```

* Non-racing accesses to `atomic<T>` i.e. while a higher-level lock is already in action e.g. double-checked locking
* For reference counting, `memory_order_relaxed` is enough for `inc` while for `dec` we need `memory_order_acquire_release` to make sure all changes done to the variable prior to it are visible including the relaxed ones; `memory_order_acquire_release` is almost `memory_order_seq_cst` but without the SL guarantee, so this works.

Taken from [Synchronization and Ordering Constraints series][sync-ordering-series]

- Read operation: `memory_order_acquire`, `memory_order_consume`[^fn1]
- Write operation: `memory_order_release`
- Read-Modify-Write operation: `memory_order_acquire_release`, `memory_order_seq_cst`
- Order-agnostic operation: `memory_order_relaxed`

[membar]: https://en.wikipedia.org/wiki/membar
[cpu-parts]: https://www.howtogeek.com/194756/cpu-basics-multiple-cpus-cores-and-hyper-threading-explained/
[weak-atomics-boehm]: https://www.youtube.com/watch?v=M15UKpNlpeM
[gcc-atomics]: https://gcc.gnu.org/wiki/Atomic/GCCMM/AtomicSync
[acq-rel-semantics]: https://preshing.com/20130922/acquire-and-release-fences/
[sync-ordering-series]: https://www.modernescpp.com/index.php/synchronization-and-ordering-constraints
[mem-order-rule]: https://preshing.com/20120625/memory-ordering-at-compile-time/
[happens-before]: https://preshing.com/20130702/the-happens-before-relation/
[synchronizes-with]: https://preshing.com/20130823/the-synchronizes-with-relation/
[Fedor CppCon 2017 talk]: https://www.youtube.com/watch?v=ZQFzMfHIxng

[^fn1]: As Hans Boehm notes in his CppCon 2016 talk, this is deprecated by the standard until it’s stated otherwise.  [The Purpose of `memory_order_consume` in C++11](https://preshing.com/20140709/the-purpose-of-memory_order_consume-in-cpp11/) details about this though.


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none' };</script>
