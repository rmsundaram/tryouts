<meta charset="utf-8">

    **Lock-free Programming**

# Introduction
The _lock_ in lock-free does not refer directly to mutexes, but rather to the possibility of “locking up” the entire application in some way, whether it’s deadlock, livelock – or even due to hypothetical thread scheduling decisions.

> "In an infinite execution, infinitely often some method call finishes."
>     — _The Art of Multiprocessor Programming_

In other words, as long as the program is able to keep calling those lock-free operations, the number of completed calls keeps increasing, no matter what. It is algorithmically impossible for the system to lock-up during those operations.

# Atomic Read-Modify-Write Operations
Atomic operations are ones which manipulate memory in a way that appears indivisible: no thread can observe the operation half-complete. On modern processors, lots of operations are already atomic e.g. aligned read/write of simple types. 

**Read-modify-write** (RMW) operations -- a class of atomic operations (such as test-and-set, fetch-and-add, and compare-and-swap) -- that both read a memory location and write a new value into it simultaneously, either with a completely new value or some function of the previous value) go a step further, allowing you to perform more complex transactions atomically. They’re especially useful when a lock-free algorithm must support multiple writers, because when multiple threads attempt an RMW on the same address, they’ll effectively line up in a row and execute those operations one-at-a-time. Examples of RMW operations include `_InterlockedIncrement` on Win32, `OSAtomicAdd32` on iOS, and `std::atomic<int>::fetch_add` in C++11.

# Compare-And-Swap Loops
Perhaps the most often-discussed RMW operation is compare-and-swap (CAS). Often, programmers perform compare-and-swap in a loop to repeatedly attempt a transaction. This pattern typically involves copying a shared variable to a local variable, performing some speculative work, and attempting to publish the changes using CAS to check; if the update made holds true, by comparing if the copy is the same as the shared variable still, the exchange (old shared for new local) happens.

# Sequential Consistency
Sequential consistency means that all threads agree on the order in which memory operations occurred, and that order is consistent with the order of operations in the program source code. Some programming languages offer sequentially consistency even for optimized code running in a multiprocessor environment. In C++11, you can declare all shared variables as C++11 atomic types with default memory ordering constraints. To achieve this, the compiler outputs additional instructions behind the scenes – typically memory fences and/or RMW operations. Those additional instructions may make the implementation less efficient compared to one where the programmer has dealt with memory ordering directly.

# Memory Ordering
Any time you do lock-free programming for multicore (or any symmetric multiprocessor), and your environment does not guarantee sequential consistency, you must consider how to prevent memory reordering. On today’s architectures, the tools to enforce correct memory ordering generally fall into three categories, which prevent both compiler reordering and processor reordering:

* A lightweight sync or fence instruction
* A full memory fence instruction
* Memory operations which provide acquire or release semantics

Acquire semantics prevent memory reordering of operations which follow it in program order, and release semantics prevent memory reordering of operations preceding it. These semantics are particularly suitable in cases when there’s a producer/consumer relationship.

# Different Processors Have Different Memory Models
Different CPU families have different habits when it comes to memory reordering. The rules are documented by each CPU vendor and followed strictly by the hardware. There’s a temptation to abstract away such platform-specific details, especially with C++11 offering us a standard way to write portable lock-free code. But currently, I think most lock-free programmers have at least some appreciation of platform differences. If there’s one key difference to remember, it’s that at the x86/64 instruction level, every load from memory comes with acquire semantics, and every store to memory provides release semantics – at least for non-SSE instructions and non-write-combined memory.

# Read-Copy Update
A synchronization mechanism that was added to the Linux kernel in October of 2002; RCU achieves scalability improvements by allowing reads to occur concurrently with updates. In contrast with conventional locking primitives that ensure mutual exclusion among concurrent threads regardless of whether they be readers or updaters, or with reader-writer locks that allow concurrent reads but not in the presence of updates, RCU supports concurrency between a single updater and multiple readers. RCU ensures that reads are coherent by maintaining multiple versions of objects and ensuring that they are not freed up until all pre-existing read-side critical sections complete. The trick is to ensure that each reader either reads the old version of the data, or the new one, but not some weird combination of old and new. RCU decouples the *removal* and *reclamation* phases of the update. Remove is removing the node from the tree but reclaim is freeing it, which happens after grace period. The name signifies the usual update procedure of a node: read, make a copy, update the copy and atomically relink parent to the copy. After all readers are done and once the grace period is over, free the old copy.

# References
1. http://preshing.com/20120612/an-introduction-to-lock-free-programming/
2. Is Parallel Programming Hard, And, If So, What Can You Do About It? — Paul E. McKenney
3. Modern Operating Systems, 4th Edition — Andrew S. Tanenbaum, Herbert Bos


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none' };</script>
