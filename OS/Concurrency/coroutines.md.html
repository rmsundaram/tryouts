<meta charset="utf-8">

    **Coroutines**

# Introduction

_Coroutines_ (Lua’s stackful or C++’s stackless) and their different packaging _async-await_ (Python, Rust, JS, etc.) are really single-process, single-thread at the hardware thread level; there’s no parallelism.  Multiple threads (not in the hardware execution engine sense) of different tasks progress at the same time by cooperating.  It’s a clever utilization of wait time.  When one task (in the English sense) is going to wait on something, it relinquishes control to other task(s) which may utilize this wait time productively.

> "A system is said to be **concurrent** if it can support two or more actions **in progress** at the same time. A system is said to be **parallel** if it can support two or more actions executing simultaneously. The key concept and difference between these definitions is the phrase “in progress.”"
>     -- The Art of Concurrency, Chapter 1: _Parallelism and Concurrency: What’s the Difference?_

This improves the overall system throughput.

[UNIX pipelines][pipes] are a good example of a coroutine-based system; there’s a source, transformer and a sink in the sense of a AV filters/graphs.

!!! Tip: CPU-bound vs IO-bound
    When your problem is CPU-bound throw more CPU at it i.e. add threads/cores.  If your problem is IO-bound then coroutine/async-await is a better choice to utilize those dead waits productively.  Of course, you can mix both intelligently too.

!!! Tip: Non-blocking I/O in C
    One can do non-blocking I/O with simple system calls like [`select`][syscall-select] or [`poll`][syscall-poll][^fn-iouring]; coroutine/async-await is just a different packaging of the same.  Rust async [executor][rs-executor] or Python’s [Event Loop][py-event-loop] internally would be doing the same.

[pipes]: https://en.wikipedia.org/wiki/Pipeline_(Unix)
[syscall-select]: https://www.man7.org/linux/man-pages/man2/select.2.html
[syscall-poll]: https://www.man7.org/linux/man-pages/man2/poll.2.html
[py-event-loop]: https://docs.python.org/3/library/asyncio-eventloop.html
[rs-executor]: https://tokio.rs/tokio/tutorial/async#executors
[tux-io-models]: https://notes.shichao.io/unp/ch6/#io-models
[io_uring]: https://en.wikipedia.org/wiki/Io_uring

[^fn-iouring]: Linux’s [io_uring][] is the latest kid in the [I/O Models][tux-io-models] block.

# Stackful vs Stackless Coroutines

C++, Python and JS has stackless coroutines, while Lua and Wren has stackful coroutines.

In stackful/symmetric coroutines both cooperating stacks are peers with their own stacks; control circles between the stacks upon one relinquishing it with `coroutine.yield`.  Yielding can be done even from a nested function i.e. in `main()` -> `task_A()` -> `f()` <-> `task_B()` -> `g()`, `g` yields to `f`.  OTOH stackless/asymmetric coroutines allow yielding only to the caller i.e. `g` can’t yeild to `f` but only to `task_B`.  In stackless systems `task_B` is subordinate to the main (and only) callstack in the program as `task_B` is really a `struct` having a function and a state variable to jump into when resuming a coro [[2](#references)].  Both stackless and stackful coroutines has a stack _while running_ but in stackless the stack isn’t serialized but only a simple `struct` for resumption.  Refer [Stackless vs. Stackful Coroutines][stackful-vs-stackless] for a good explanation with examples from JS and Lua.

C++ chose stackless as it maintains that the “co” part implies subordination to the caller; otherwise it’d actually be a fibre [[2](#references)].  Some are disappointed in C++’s coroutines as there’s always a heap allocation involved on use.

[stackful-vs-stackless]: https://blog.varunramesh.net/posts/stackless-vs-stackful-coroutines/

# AsyncIO in JavaScript and Python

## JavaScript

* Async functions always return a promise
  - Even if you `return 0` it’ll be wrapped in (already-resolved) promise
* `await` expressions are allowed only inside async functions or from an ES6 module (`.mjs`)
* Async functions can called with
  - `await` from async functions e.g. `await getImage();`
  - `then()` from regular functions in e.g. `getImage().then(useIt, errorOut);`
    + or `catch()` or `finally()` which internally call `then()`

When an async function is called, its all code runs as usual until an `await` expression is hit.  When hit, the expression is executed (which would return immediately) but said function is paused at that point and control returns to the caller.  Caller runs its own code to completion.  The message queue goes about its business of popping messages and calling respective handlers.  When the awaited value is resolved/rejected a microtask that resumes from the paused point (code after the expression that required the value) is posted to the queue.  This happens even if the expression returns an already-resolved promise.  This microtask eventually gets picked up and control resumes from the paused point.

Refer to these excellent articles by MDN:

1. [Control flow effects of `await`_][js-async-control-flow]
2. [The event loop][js-event-loop]
3. [In depth: Microtasks and the JavaScript runtime environment][js-microtasks]

[js-async-control-flow]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/await#control_flow_effects_of_await
[js-event-loop]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Event_loop
[js-microtasks]: https://developer.mozilla.org/en-US/docs/Web/API/HTML_DOM_API/Microtask_guide/In_depth

## Python

Python’s AsyncIO is similar to JS’ but the loop is exposed to the programmer.  High-level APIs like `asyncio.run`, `asyncio.gather` are generally recommended by the documentation stating `asyncio.loop` is for low-level manipulation mostly for library authors and prosumers.

A regular function launching a loop of async functions has to wait it out.  If there’re tasks that need to continue while awaiting the loop, posted them too for cooperation (to avoid waiting).

Refer

1. [Very good AsyncIO example][py-async-eg]
2. [Difference between JS and Python AsyncIO][async-js-vs-py]

[py-async-eg]: https://stackoverflow.com/a/53420574/183120
[async-js-vs-py]: https://stackoverflow.com/a/68139719/183120

# Aside: Parallelism

When you want many tasks to progress _simultaneously_ then you’ve to resort to multi-threading or multi-processing (running multiple processes like Chromium or Python `multiprocessing` package does).  It’d involve playing with razor blades like data races, thread synchronization, effective data/work partitioning, etc. but it might be worth the effort depending on the problem.   This would be the true utilization of all hardware threads (multiprocessor/multi-core programming).  One can go further by employing the GPU (heterogeneous computing) too to solve compute-heavy workloads.

C++ has many options [^fn-overload] in its arsenal but many are not portable (vendor-specific) or short-lived e.g. [AMP][]; ordered by portability and stability:

1. [C++17 parallel algorithms][cpp17-par-algo]: best bet (standardized); use this if you can
2. [taskflow][]: cross-platform, cross-arch open-source library
3. manual: create and schedule own thread pool
4. OpenMP (Open MultiProcessing): [standardized, cross-arch, multi-toolchain][openmp-toolchain]
  - Uses _fork-join_ model: primary thread forks sub-threads diving work and joining them all
5. SYCL
6. Clik
7. OpenCL
8. Intel Threading Building Blocks: works on Intel and AMD but not on ARM processors
9. Microsoft [ConcRT][] (Parallel Patterns Library): Windows/MSVC-specific

Of these, (1), (4), (5), (6) and (7) seem suitable mostly for embarrassingly/data parallel problems.  If you’ve [uneven tasks to be parallelized][tbb-uneven-work] with techniques like work stealing, reach for the other options.

Python parallelism is achieved with `multiprocessing` package where multiple processes (with their own PID) is created to utilize multi-core machines.

[^fn-overload]: [Tim Mattson disucss _choice overload_][par-less-more] programmers face when trying to parallelize their solution.

[taskflow]: https://github.com/cpp-taskflow/cpp-taskflow
[cpp17-par-algo]: https://devblogs.microsoft.com/cppblog/using-c17-parallel-algorithms-for-better-performance/
[concrt]: https://learn.microsoft.com/en-us/cpp/parallel/concrt/concurrency-runtime?view=msvc-170
[amp]: https://learn.microsoft.com/en-us/cpp/parallel/amp/cpp-amp-cpp-accelerated-massive-parallelism?view=msvc-170
[openmp-toolchain]: https://www.openmp.org/resources/openmp-compilers-tools/
[tbb-uneven-work]: https://www.youtube.com/watch?v=Zu5JcxZt_f8


# Aside: Green Threads

Golang has [green threads][] (using the [M:N threading model][m-n model]) but not coroutines/async-await.  Every _goroutine_ is a user-space thread, a notion of the Go runtime, which creates and manages these threads by assigning hardware threads to progress them.  User threads are generally faster as no system calls are involved and lesser time is spent context switching.

The Go runtime takes the onus of creating and scheduling them effectively to utilize the machine’s compute facilities effectively.  It gives them a design to effectively use multiprocessor/multi-core machines without the programmer needing to know multi-threaded programming.  Thread synchronisation is also avoided for the most part, thanks to channels.  Of course, under the hood channels are similar to producer-consumer queues and the runtime takes care of the synchronisation part.

Go’s model comes at a cost: the runtime uses one multi-threading model while there’re many (boss-worker, pipeline, background task, etc. [[3](#references)]); different models suit different problems.  Go’s model might not always be the best fit for the problem solved by its programmer.

Java and Erlang also uses green threads.  JVM performs the M:N mapping at runtime unlike Go.

Green threads have become unpopular e.g. [Rust had green threads at one point and was dropped][rust-rm-gc] as the runtime (doing M:N mapping) gets complicated to maintain and not doesn’t scale well to all kinds of problems; Rust wants its runtime to be slim.  Refer C10K problem:

> There is a choice when implementing a threading library: you can either put all the threading support in the kernel (this is called the 1:1 threading model), or you can move a fair bit of it into userspace (this is called the M:N threading model). At one point, M:N was thought to be higher performance, but it's so complex that it's hard to get right, and most people are moving away from it.
>    -- The C10K Problem, § 1:1 threading vs. M:N threading

[green threads]: https://en.wikipedia.org/wiki/Thread_(computing)#User_threads
[m-n model]: https://en.wikipedia.org/wiki/Thread_(computing)#Threading_models
[rust-rm-gc]: https://www.reddit.com/r/rust/comments/18ede5k/comment/kcn5g6m


# References

1. Art of Concurrency
2. [C++ Coroutines Do Not Spark Joy][c++-coro]
3. pthread Tutorial, §4 Thread Models by _Peter Chaplin_
4. [AsyncIO in Python][real-py-asio]
5. [Rust Async Book][rust-async]

[c++-coro]: https://probablydance.com/2021/10/31/c-coroutines-do-not-spark-joy/
[real-py-asio]: https://realpython.com/async-io-python/
[rust-async]: https://rust-lang.github.io/async-book
[par-less-more]: http://timmattson.com/parallel-programming-environments-less-is-more/


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js" charset="utf-8"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js" charset="utf-8"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none' };</script>
