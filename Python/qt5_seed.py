#!/bin/env python3
"""Hello world Qt seed script."""

import sys

from PySide2.QtCore import QDateTime
from PySide2.QtWidgets import QApplication, QMainWindow, QPushButton, \
    QDateTimeEdit, QVBoxLayout, QWidget


class MainWindow(QMainWindow):
    """Subclass QMainWindow to customize your application's main window."""

    def __init__(self):
        super().__init__()
        self.setWindowTitle("Hello Qt!")
        self.timeWidget = QDateTimeEdit()
        self.timeWidget.setDateTime(QDateTime.currentDateTime())
        button = QPushButton("Reset")
        button.clicked.connect(self.setCurrentTime)
        vbox = QVBoxLayout()
        vbox.addWidget(self.timeWidget)
        vbox.addWidget(button)
        centralWidget = QWidget()
        centralWidget.setLayout(vbox)
        self.setCentralWidget(centralWidget)

    def setCurrentTime(self):
        self.timeWidget.setDateTime(QDateTime.currentDateTime())

app = QApplication(sys.argv)
window = MainWindow()
window.show()
app.exec_()
