<meta charset="utf-8">

    **Python 3**

# Integers

Python integer objects are immutable and considered signed numbers.  Numeric
literals do not include a sign (unary minus operator does it).

!!! Warning: `__floordiv__` on negative integers
    Floor division `//` rounds towards −∞ unlike `int()` (or `math.trunc()`)
    rounding towards 0 e.g. `-5//2` = -3, `int(-5/2)` = -2

Reminder operator `%` does Knuth modulo: sign(rem) = sign(divisor) and
magnitude is floor division result.

!!! Tip: `divmod()`
    Use the `divmod()` built-in on numbers if you want both quotient and
    reminder in one shot; internally calls `__divmod__` on the object.

## Bit operations

Python doesn’t specify the internal representation of integer objects.  _For
bitwise operations, assume unbounded preceding 0s for positive values and 1s
for negatives._

> "The result of bitwise operations is calculated as though carried out in two’s
>  complement with an infinite number of sign bits.""
>     -- [Python Manual][manual-bitwise]

!!! Warning: Bitwise complement flips sign
    `~4` = `-5`, `~-5` = `4` might surprising as bitwise operations are usually
    done on unsigned integers in C.  Bound result with `& 0xff`, `& 0xffff`,
    etc. to required bit width.

```
 4 =  0..0_0100)₂
~4 =  1..1_1011)₂ = -5 in two's complement (high bit is set)
~4 & 0xff = 1..1_1011 & 0..0_1111_1111 = 0..0_1111_1011 = 251 (high bit unset)
```

This is similar to C’s bitwise `~` on signed integers with two’s complement
implementation.  The result doesn’t change based on signed integer’s width but
changes if unsigned. `~4` as `u8` is `0b1111_1011`; when this bit pattern is
interpreted as signed it’s -5 and 251 as unsigned.  As `u16` it equates to
65531 if unsigned; signed stays the same.

[manual-bitwise]: https://docs.python.org/3.11/library/stdtypes.html#bitwise-operations-on-integer-types

# Iterables and Iterators

> "Iterators are also iterable, but iterables are not iterators."
>     -- _Fluent Python_, 2/e

_Iterables_ (ABC) implement `__iter__` method[^getitem], called by
`iter(iterable)`, returning an _iterator_ (ABC) having the `__next__` method.
Iterators raise `StopIteration` on exhaustion e.g. sequences are iterables;
generator functions, generator expressions are iterators (but also iterables;
see below).

It’s an anti-pattern to implement `__next__` on an iterable as it’d break
multiple iterators from working correctly (refer _Fluent Python_, 2/e, Chapter
17 Iterators, Generators, and Classic Coroutines § Don’t Make the Iterable an
Iterator for Itself).  OTOH, iterators are required to implement `__iter__` to
`return self` as they’re iterable.  When an iterator is fed to `for`, unlike
the usual scenario of iterable fed to `for`, it works hence.

!!! Note: `for` needs iterable
    `for` calls `__iter__` and on the obtained iterator `__next__`; it fails
    with `TypeError` if its argument isn’t an iterable.

``` python
l = [1, 2, 3]
i = iter(l)  # calls l.__iter__
for n in i:  # calls l.__iter__ followed by __some.__next__ (l is __some)
    print(n)

for k in (i for i in range(5)):  # genexp is an iterator but also iterable
    print(k)
```

!!! Tip: Callable iterator
    `iter(f, sentinel)` creates an iterator object for any function `f` which
    raises `StopIteration` when `f` returns the sentinel marker.

# Generator {functions, objects, expressions}

A function containing `yield` is a _generator_ or more explicitly _generator
function_.  It returns a _generator iterator_/_generator object_ wrapping the
function’s body.  Generator functions are just functions; neither an iterable
nor an iterator.  Generator iterators are iterators (and iterables).  _Generator
expressions_  a.k.a genexps evaluate to generator iterators; they’re syntactic
sugar to defining and calling generator functions.

Generator functions and genexps are factories of generator iterators.
Generator functions do not `return` or `raise StopIteration`.  Python generates
an iterator object when the generator function is called; this object
implements `__next__` which calls `StopIteration` when generator returns.

Classes implement `__iter__` as a generator function or return a genexp
i.e. a generator iterator is obtained calling it.

A generator can `yield from` a sub-generator i.e. delegate work to another
generator.  Until sub-generator depletes delegating generator pauses.

[^getitem]: `__getitem__` is another method iterables implement for backward
compatibility.  It raises `IndexError` on exhaustion.

<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js" charset="utf-8"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js" charset="utf-8"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none', inlineCodeLang: 'python' };</script>
