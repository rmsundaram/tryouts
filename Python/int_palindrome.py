#!/usr/bin/env python3

import sys

def is_palindrome_int(num):
    tmp = num
    mun = 0
    while tmp > 0:
        digit = tmp % 10
        mun = (mun * 10) + digit
        tmp //= 10
    return num == mun

def is_palindrome_float(f):
    return f[::-1] == f         # string reverse

def is_palindrome(num):
    if '.' in num:
        return is_palindrome_float(num)
    else:
        return is_palindrome_int(int(num))

if __name__ == '__main__':
    if len(sys.argv) > 1:
        sys.argv[1]
        print(f"{sys.argv[1]} is "
              f"{'a' if is_palindrome(sys.argv[1]) else 'not a'}"
              " palindrome")
    else:
        print(f"Usage: {sys.argv[0]} NUMBER")
