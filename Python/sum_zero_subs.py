#!/usr/bin/env python3

# https://www.geeksforgeeks.org/print-all-subarrays-with-0-sum/

l = [6, 3, -1, -3, 4, -2, 2, 4, 6, -12, -7]

sum = 0
sum_to_idx = {}
subs = []

for i, v in enumerate(l):
    sum += v
    if sum == 0:
        subs.append((0, i))
    elif sum in sum_to_idx:
        # sum reached earlier too: S … S i.e. sum(in between values) should = 0
        subs.extend([(prev + 1, i) for prev in sum_to_idx[sum]])
        sum_to_idx[sum].append(i)
    else:
        sum_to_idx[sum] = [i]

print(subs)
