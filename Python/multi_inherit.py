#!/usr/bin/env python3

class Base:
    def __init__(self):
        print('Base')

class D1(Base):
      def __init__(self):
          print('D1 init')
          super().__init__()
          print('D1 done')

class D2(Base):
      def __init__(self):
          print('D2 init')
          super().__init__()
          print('D2 done')

class Derived(D1, D2):
      def __init__(self):
          print('Init start')
          super().__init__()
          print('Init end')

Derived()
