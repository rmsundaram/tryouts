from __future__ import division
import sys, flickrapi
import urllib
import Image

app_key = 'a37f82a73b0cdbd480fd03b9af4dc3b4'
secret_key = '63a34750f6691245'
my_user_id = '36674132@N02'

flickr = flickrapi.FlickrAPI(api_key = app_key, secret = secret_key)

photoset = flickr.photos_search(api_key = app_key, user_id = my_user_id)	#, tags=['self'], tag_mode='all'

print "\nSearch returned %d photo(s)!\n" % (len(photoset.getchildren()[0].getchildren()))

for photo in photoset.getchildren()[0].getchildren():
	photo_id = photo.get('id')
	grab_url = "http://farm%s.static.flickr.com/%s/%s_%s.jpg" % (photo.get('farm'), photo.get('server'), photo_id, photo.get('secret'))

	print "ID: " + photo_id
	print "TITLE: " + photo.get('title')
	print "GRAB_URL: " + grab_url

	sys.stdout.write("Downloading...  ")
	urllib.urlretrieve(grab_url, photo_id + ".jpg")
	print "OK"
'''
	img = Image.open(photo_id + ".jpg")
	print "Original Dimension: %d x %d" % img.size
	aspect_ratio = img.size[0] / img.size[1]

	if img.size[0] > img.size[1]:
		new_x = min(img.size[0], 320)
		new_y = int(new_x / aspect_ratio)
	else:
		new_y = min(img.size[1], 320)
		new_x = int(new_y * aspect_ratio)

	new_size = (new_x, new_y)

	print "Resizing to %d x %d... " % new_size, 
	new_img = img.resize(new_size, Image.BICUBIC)
	print "OK\nSaving as %s... " % (photo_id + "_mobi.jpg"), 
	new_img.save(photo_id + "_mobi", "JPEG")
	print "OK\n"
'''
