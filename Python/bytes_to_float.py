#!/usr/bin/env python3

import struct


# https://stackoverflow.com/a/434328/183120
def chunker(seq, size):
    """Return |seq| in chuncks of |size| values."""
    return (seq[pos:pos + size] for pos in range(0, len(seq), size))


def main():
    b = bytearray(b'\x00\x00\x00\x00\x00\x00\x80?\x00'
                  b'\x00\x80?\x00\x00\x80?\x00\x00\x00'
                  b'\x00\x00\x00\x00\x00\x00\x00\x80?'
                  b'\x00\x00\x00\x00')
    print(f'Byte array length: {len(b)}')
    # Decode bytes in native endian byte order and convert to float
    #https://stackoverflow.com/q/56396524/183120
    for flt_bytes in chunker(b, 4):
        print(f'{flt_bytes.hex()} => {struct.unpack("f", flt_bytes)}')


if __name__ == '__main__':
    main()
