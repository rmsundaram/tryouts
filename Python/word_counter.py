"""This is a python module to read a file and display its stats"""
# interface functions
def count_words():
    """Requests the user for a file and displays its word stats"""
    file_name = input("Enter file to read: ")
    file = open(file_name, 'r')
    contents = file.read().split()
    file.close()
    occurances = { }
    for word in contents:
        occurances[word] = occurances.get(word, 0) + 1;
    print("{} has {} words, out of which {} are unique".format(file_name, len(contents),  len(occurances)))
    print(occurances)

if __name__ == '__main__':
    count_words()
