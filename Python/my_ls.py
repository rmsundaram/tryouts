#!/usr/bin/env python3

"""Print files in a directory with human-readable sizes, ctime and mtime."""

import sys
import os
import time


def timestamp(t):
  return time.strftime("%Y-%m-%d %H-%M", time.localtime(t))


def human_size(n):
  for u in ["", "Ki", "Mi", "Gi", "Ti"]:
    if abs(n) < 1024.0:
      return (f"{n:3.1f}", f"{u}B")
    n /= 1024.0
  return (f"{n:3.1f}", "PiB")


def list_files(path):
  for f in os.scandir(path):
    if f.is_file():
      st = f.stat()
      s, unit = human_size(st.st_size)
      # mtime is content last-modified time; ctime is OS-reported time:
      # metadata changed time on Unix-likes and creation time on Windows
      print(f"{timestamp(st.st_ctime):<20} {timestamp(st.st_mtime):<20} \
      {s:>5} {unit:<3} {f.name}")


if __name__ == '__main__':
  # list PWD if no input
  list_files(sys.argv[1] if len(sys.argv) >= 2 else '.')
