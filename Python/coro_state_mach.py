#!/usr/bin/env python3

"""Program to demonstrate coroutines elegantly representing state machines.

Python 3 version of program in Eli Bendersky's
Co-routines as an alternative to state machines.
https://eli.thegreenplace.net/2009/08/29/co-routines-as-an-alternative-to-state-machines
"""


def coroutine(func):
    """Coroutine wrapper."""
    def start(*args, **kwargs):
        cr = func(*args, **kwargs)
        cr.__next__()
        return cr
    return start


@coroutine
def unwrap_protocol(header=0x61,
                    footer=0x62,
                    dle=0xAB,
                    after_dle_func=lambda x: x,
                    target=None):
    """Simplified framing (protocol unwrapping) source co-routine."""
    # Outer loop looking for a frame header
    while True:
        byte = (yield)
        frame = bytearray()

        if byte == header:
            # Capture the full frame
            while True:
                byte = (yield)
                if byte == footer:
                    target.send(frame)
                    break
                elif byte == dle:
                    byte = (yield)
                    frame.append(after_dle_func(byte))
                else:
                    frame.append(byte)


@coroutine
def frame_receiver():
    """Sink co-routine for receiving full frames."""
    while True:
        frame = (yield)
        print(f'Got frame: {bytes(frame)}')


DATA = b'\x70\x24\x61\x99\xAF\xD1\x62\x56\x62\x61\xAB\xAB\x14\x62\x07'

unwrapper = unwrap_protocol(target=frame_receiver())

for byte in DATA:
    unwrapper.send(byte)
