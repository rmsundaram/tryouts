#!/usr/bin/env python3

import copy

# REFERENCES
# https://realpython.com/python-mutable-vs-immutable-types/#variables-and-objects
# https://stackoverflow.com/a/25670170/183120
# https://docs.python.org/3/reference/datamodel.html#objects-values-and-types
# Fluent Python 2/e, Chapter 6, Effective Python 2/e, The Quick Python Book 2/e

# ------------------------------------------------------------------------------
# Variables are labels in Python.  Every object has a read-only id which is its
# memory address (CPython), type (decided by __class__) and value (its data).

# Type dictates if an object is mutable (changing value) or immutable (stable
# and hence hasable); most built-ins are immutable e.g. bool, int, float, bytes,
# complex, tuple, str, frozenset; mutable built-ins e.g. list, dict, bytearray,
# set.  Immutable methods return a copy while mutatating methods work in-place
# and return None e.g. str.join vs list.sort.

# When a variable bound to an int object is assigned a new value, it’s really
# rebound to another int object; old object’s value is unchanged irrespective of
# type’s mutability.  Old object lives until GC cleans up.
x = 1
old_id = id(x)
x = 2
# Different id as variable/label ’x’ is now stuck on a different object: int(2)
assert old_id != id(x)
# This is irrespective of object mutability.
l = [1, 2, 3]
old_id = id(l)
l = [3, 2, 1]
assert old_id != id(l)

# All these variables are bound to the same object `0`.
a = b = c = 1
assert a is b is c  # equivalent: a is b and b is c
# Comparison operators can be chained!
# https://stackoverflow.com/q/25103085/183120

# This concept of variable assignment applies to function argument assignment,
# list element assignment, etc.
a = [0, 'python', ['x', 'y']]
b = a[:]  # also list(a) and copy.copy(a)
# Two different lists but their members alias same int, str and list objects.
assert a is not b
assert a[0] is b[0]
assert a[2] is b[2]
# Assigning a new int to b[0] means a new object is created and assigned.  a[0]
# continues to refer to the old int object.
b[0] = 1
assert a[0] is not b[0]
# Lists are mutable and so allow in-place editing; such edits (not assignments)
# would reflect on other references too; a[2][2] is b[2][2]
a[2].append('z')
assert a[2] is b[2]
# Use copy.deepcopy to clone every member object.
b = copy.deepcopy(a)
assert a[2] is not b[2]

# Gotcha: usually editing a variable, for immutables means drop (old) and
# rebound (new) while for mutables its in-place; this may lead to confusion.
# Function parameters aliase arguments; for an int (immutable) when a new value
# is assigned, a new object is created leaving the original unaffected while
# for a list (mutable) changes are reflected.

# To reflect changes to immutable in a function return a tuple; check FAQ:
# https://docs.python.org/3/faq/programming.html#how-do-i-write-a-function-with-output-parameters-call-by-reference

# Fluent Python, 2/e, § 2. Building Lists of Lists
board = [['_'] * 3 for i in range(3)]
# Each row is a different list as constructed by listcomp with ref to same strs
assert board[0] is not board[1]
assert board[0][0] is board[1][0] is board[2][0]
# Assignment creates a new string object and they’re no longer the same
board[0][0] = 'X'
assert board[0][0] != board[1][0]

# Same list is referenced thrice; unexpected as lists are to be disjoint.
# Lists are mutable, programmer won’t generally assign a new list like str, so
# setting a slot directly means all there references getting the same value.
dupe_board = [['_'] * 3] * 3
assert dupe_board[0] is dupe_board[1]
dupe_board[0][0] = 'X'
assert dupe_board[0][0] is dupe_board[1][0] is dupe_board[2][0]

# ------------------------------------------------------------------------------

x  = x + y  # Never mutates x; x is rebound to a new object.
x += y      # Mutates x in-place if it has __iadd__ else behaves like x = x + y
# Both {im,}mutable support augmented assignment; internally immutable ones
# call __add__ returning a new object while mutable ones call __iadd__ return
# None as they mutate in-place.
old_id = id(b)
b += b[:]
assert old_id == id(b)  # mutates in-place like list.append, list.extend, …
b = [1, 2]
b *= 3  # makes lengthens b to 6 slots, assigns same objects in 4 new slots
assert b[0] is b[2] is b[4]

# ------------------------------------------------------------------------------

# Tuples are immutable; can’t add/remove/change member references.
t = ('amma', [0, 1, 0])
# t[1] = [0, 0, 0] throws TypeError as tuples are immutable
# t[0] = 'appa'  # throws TypeError too
# t[0][1] = 'p'  # throws TypeError as str is immutable too

# However, editing a tuple’s mutable member, works!  Technically, tuple isn’t
# modified i.e. its member reference is in tact; the member object internally
# changes.  Tuple immutability is limited to its held references only, not to
# held objects.  It’s discouraged to store mutable objects in tuple as it
# breaks immutability assumption.
t[1][1] = 0

# Both dict and set only take hashable objects as keys.  Set supports union |,
# intersection &, difference - (in A but not B) and symmetric difference ^
# (only in A and only in B).  The non-operator versions of these operations
# support any iterable while operators expect operands to be sets.  These
# operators also have augmented variants: a |= b (mutates in-place).  Dict
# supports merging two dictionary with the union operator in both variants:
# d | other (returns a new dict) and d |= other (in-place).

# ------------------------------------------------------------------------------

# del statements unbinds references, doesn’t delete objects; GC may delete.
# {__delattr__, __delitem__} is invoked to to entertain del a.b and a[b]
# respectively; objects/dictionaries and collections usually implement these.

# ------------------------------------------------------------------------------

# FOOTNOTE
# In the above examples, do not confuse with CPython’s interned integers:
a = 12
b = 12
assert a is b
# Though incorrect, it succeeded as [-5, 256] are interned in a global cache.
# https://github.com/satwikkansal/wtfpython#-how-not-to-use-is-operator
# https://realpython.com/python-bitwise-operators/#interned-integers
