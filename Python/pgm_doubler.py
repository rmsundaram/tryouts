#!/usr/bin/env python3

def nearest_neighbour_2x_scale(width, height, data):
    assert (width * height) == len(data)
    out_width = width * 2
    out_height = height * 2
    out_size = out_width * out_height
    out_data = [None] * out_size  # pre-allocate memory
    for col in range(0, out_width):
        for row in range(0, out_height):
            in_col = col // 2
            in_row = row // 2
            out_data[row * out_width + col] = data[in_row * width + in_col]
    return out_width, out_height, out_data

def write_pgm(path, width, height, max_intensity, data):
    with open(path, mode='w') as o:
        o.write('P2\n')
        o.write(str(width) + ' ' + str(height) + '\n')
        o.write(str(max_intensity) + '\n')
        for row in range(0, height):
            offset = row * width
            o.write(' '.join(map(str, data[offset:(offset + width)])))
            o.write('\n')

if __name__ == "__main__":
    # assume PGM input already parsed
    in_width = 24
    in_height = 8
    in_max_intensity = 15
    # data: https://en.wikipedia.org/wiki/File:Feep_netbpm_p2_pgm_example.png
    in_data = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 3, 3, 3, 3, 0, 0, 7, 7, 7, 7, 0, 0, 11, 11, 11, 11, 0,
               0, 15, 15, 15, 15, 0, 0, 3, 0, 0, 0, 0, 0, 7, 0, 0, 0, 0, 0, 11,
               0, 0, 0, 0, 0, 15, 0, 0, 15, 0, 0, 3, 3, 3, 0, 0, 0, 7, 7, 7, 0,
               0, 0, 11, 11, 11, 0, 0, 0, 15, 15, 15, 15, 0, 0, 3, 0, 0, 0, 0,
               0, 7, 0, 0, 0, 0, 0, 11, 0, 0, 0, 0, 0, 15, 0, 0, 0, 0, 0, 3, 0,
               0, 0, 0, 0, 7, 7, 7, 7, 0, 0, 11, 11, 11, 11, 0, 0, 15, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
               0, 0, 0, 0, 0]
    out_width, out_height, out_data = nearest_neighbour_2x_scale(in_width,
                                                                 in_height,
                                                                 in_data)
    write_pgm('out.pgm', out_width, out_height, in_max_intensity, out_data)
