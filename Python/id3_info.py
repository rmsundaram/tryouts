#!/usr/bin/env python3

import sys
from mutagen.easyid3 import EasyID3 as ID3

def main():
    if len(sys.argv) <= 1:
        print("usage: pyd3 AUDIO_FILE")
        sys.exit(0)

    audioFile = ID3(sys.argv[1])
    # enable to print debug info
    # print(type(audioFile), *ID3.valid_keys.keys(), sep='\n\t')
    # enable print available fields
    # print(ID3.pprint(audioFile))

    printTag(audioFile, "title")
    printTag(audioFile, "artist")
    printTag(audioFile, "album")
    printTag(audioFile, "tracknumber")
    printTag(audioFile, "discnumber", "Disc")
    printTag(audioFile, "albumartist", "Album Artist")
    printTag(audioFile, "date", "Year")
    printTag(audioFile, "genre")
    printTag(audioFile, "lyricist")
    printTag(audioFile, "author", "Original Lyricist")
    printTag(audioFile, "encodedby", "Encoder")
    printTag(audioFile, "language")
    printTag(audioFile, "length")
    printTag(audioFile, "composer")
    printTag(audioFile, "copyright")

def printTag(track, tag, pretty_name=None):
    pretty_name = pretty_name or tag.capitalize()
    print("{}:".format(pretty_name), str(track[tag]).encode('utf-8') if tag in track else "<unset>")

if __name__ == "__main__":
    main()
