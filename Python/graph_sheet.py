#!/usr/bin/env python3

import pyglet
from pyglet import shapes

width = 1024
height = 800
graph_size = 100

def main():
    window = pyglet.window.Window(width, height)
    # https://stackoverflow.com/q/42470333/183120
    pyglet.gl.glClearColor(1.0, 0.937, 0.835, 1.0)
    batch = pyglet.graphics.Batch()
    graph_lines = []
    graph_numbers = [pyglet.text.Label(f'0',
                                       # font_name='Times New Roman',
                                       font_size=8,
                                       color=(100, 100, 100, 255),
                                       x=5, y=5,
                                       anchor_x='left')]

    for i in range(1, 1 + (width // graph_size)):
        offset = i * graph_size
        graph_lines.append(shapes.Line(offset,
                                       0,
                                       offset,
                                       height,
                                       width=1,
                                       batch=batch,
                                       color=(100, 149, 237)))
        graph_numbers.append(pyglet.text.Label(f'{i * graph_size}',
                                               # font_name='Times New Roman',
                                               font_size=8,
                                               color=(100, 100, 100, 255),
                                               x=offset+5, y=5,
                                               anchor_x='left'))

    for i in range(1, height // graph_size):
        offset = i * graph_size
        graph_lines.append(shapes.Line(0,
                                       offset,
                                       width,
                                       offset,
                                       width=1,
                                       batch=batch,
                                       color=(100, 149, 237)))
        graph_numbers.append(pyglet.text.Label(f'{i * graph_size}',
                                               # font_name='Times New Roman',
                                               font_size=8,
                                               color=(100, 100, 100, 255),
                                               x=5, y=offset+5,
                                               anchor_x='left'))

    # pyglet.gl.glScalef(10, 10, 1)
    dot = shapes.Circle(100, 100, 10, color=(255, 0, 0), batch=batch)

    @window.event
    def on_draw():
        window.clear()
        batch.draw()
        for n in graph_numbers:
            n.draw()

    pyglet.app.run()

if __name__ == '__main__':
    main()
