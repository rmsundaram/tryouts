#!/usr/bin/env python3

"""Stop needless processing once SIGPIPE is hit."""

# References
# [1]: https://docs.python.org/3/library/signal.html#note-on-sigpipe
# [2]: https://stackoverflow.com/q/26692284/183120

import sys


def main():
  i = 0
  try:
    while True:
      print('0', flush=True)
      i += 1
  # Non-Windows platforms throw BrokenPipeError; IOError on Windows.
  except (BrokenPipeError, IOError) as e:
    # Stop Python from writing to STDOUT further, leading to another exception.
    sys.stdout = None
    # [1] suggests copying STDOUT to /dev/null to prevent above but it involves
    # opening a file and ideally Python shouldn’t write to STDOUT.
    #     import os
    #
    #     devnull = os.open(os.devnull, os.O_WRONLY)
    #     os.dup2(devnull, sys.stdout.fileno())
    print(f'{e}; processed iterations: {i}', file=sys.stderr)
    sys.exit(1)  # Python exits with error code 1 on EPIPE


if __name__ == '__main__':
  main()
