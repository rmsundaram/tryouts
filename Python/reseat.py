#!/usr/bin/env python3

"""Print position of slots after reseats.

A utility to find the final position of some records I wanted to move
in a DB table after inserting a few rows in a haphazard fashion.
"""

from collections import namedtuple
from typing import Iterable


class Slot(namedtuple('Slot', ['org_pos', 'pos'])):
    """Simple slot class."""
    # https://stackoverflow.com/a/7914212/183120
    __slots__ = ()

    def __str__(self):
        return f'{self.org_pos} --> {self.pos}'


def indexof(slots: Iterable[Slot], x: Slot):
    """Find the index of a slot in slots."""
    for idx, s in enumerate(slots):
        if x.pos == s.pos:
            return idx
    return None


def ins(slots: Iterable[Slot], x: Slot):
    """Insert a new slot in slots."""
    idx = indexof(slots, x)
    slots.insert(idx, x)
    slots[idx+1:] = [Slot(s.org_pos, s.pos+1) for s in slots[idx+1:]]
    return slots


def main():
    """Run test driver."""
    slots = [Slot(i, i) for i in range(1, 11)]
    slots = ins(slots, Slot(11, 4))
    slots = ins(slots, Slot(12, 2))

    for s in slots:
        print(s)


if __name__ == '__main__':
    main()
