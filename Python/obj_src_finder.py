#! /usr/bin/env python3

# Find source files from Mach-O object files using BSD’s dwarfdump
# Resorting to this as there’s no library by Apple for parsing them
# https://lowlevelbits.org/parsing-mach-o-files/

import sys
import os
import subprocess
import re

langRE = re.compile(r'language\( (.*) \)')
srcRE = re.compile(r'name\( "(.*)" \)')

# set to filter source file dupes
sources = set()

def look_for_objs(path):
    for dirName, subDirList, fileList in os.walk(path):
        objFile = [fi for fi in fileList if fi.endswith('.o')]
        for fname in objFile:
            # set encoding for cmdRes.stdout to be a string, not byte sequence
            cmdRes = subprocess.run(['dwarfdump', '-e', '-r 0', fname],
                                    cwd=dirName,
                                    capture_output=True,
                                    encoding='utf-8')
            langMatch = langRE.search(cmdRes.stdout)
            # filter Objective-C and Objective-C++ files
            if langMatch and langMatch.expand('\g<1>').startswith('Obj'):
                srcMatch = srcRE.search(cmdRes.stdout)
                # we add source path to a set to avoid dupes
                # so make there’s only match
                if srcMatch and len(srcMatch.groups()) == 1:
                    srcName = srcMatch.expand("\g<1>")[6:]
                    if srcName not in sources:
                        sources.add(srcName)
                        print(srcName, fname, sep=' --> ')
                else:
                    print("{0} couldn't locate source(s)".format(fname),
                          file=sys.stderr)
            elif langMatch == None:
                print("{0} has no language".format(fname), file=sys.stderr)

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("usage: " + sys.argv[0] + " DIR_PATH")
    elif os.path.exists(sys.argv[1]) != True:
        print(sys.argv[1] + " unreachable")
    else:
        look_for_objs(sys.argv[1])
