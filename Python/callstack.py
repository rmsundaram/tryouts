#!/usr/bin/env python3

import traceback

def b():
  traceback.print_stack()

def a():
  b()

def main():
  a()
  print('Hello, Python.')

if __name__ == '__main__':
  main()
