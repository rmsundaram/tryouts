#!/usr/bin/env python3

"""Tool to pretty print C preprocessor's output.

Tested aginst GCC and Clang.  Both follow
https://gcc.gnu.org/onlinedocs/gcc-12.1.0/cpp/Preprocessor-Output.html

Sample line:
# 863 "/usr/include/memory" 2 3
"""

import fileinput
import sys


def split(s):
  delim1 = s.find(" ", 3)
  toks = [s[2:delim1]]
  delim2 = s.find('"', delim1 + 2)
  toks.append(s[delim1 + 2:delim2])
  toks.reverse()  # place file name before line number
  toks.extend(s[delim2 + 1:].split())
  return toks


def main():
  level = 0
  prev = None
  cur_inc = None
  try:
    for line in fileinput.input():
      if not line.startswith('# '):
        continue
      toks = split(line)
      # ignore noise and in-built files
      if len(toks) < 2 or toks[0].startswith('<'):
        continue
      # For len(toks) == 2, just store tokens in |prev|
      if len(toks) > 2:
        if '1' in toks[2:]:
          print('  ' * level, end='')
          print(f'{prev[0]}:{prev[1]}')
          level += 1
          cur_inc = (toks[0], toks[1])
        elif '2' in toks[2:] and level > 0:
          if cur_inc:
            print('  ' * level, end='')
            print(f'{cur_inc[0]}:{cur_inc[1]}')
            cur_inc = None
          level -= 1
      prev = (toks[0], toks[1])
  except (BrokenPipeError, IOError) as e:
    sys.stdout = None
    print(f'Pipe closed; stopped processing: {e}', file=sys.stderr)


if __name__ == '__main__':
  main()
