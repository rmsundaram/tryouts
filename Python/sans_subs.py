#!/usr/bin/env python3

import sys
import os
from shutil import which
import re
from pathlib import Path
import subprocess


class CheckToolOutput:

  def __init__(self, tool_with_args: [str], regex: str):
    self.tool = tool_with_args
    self.regex = re.compile(regex)

  def check(self, input_):
    result = subprocess.run(self.tool + [input_],
                            capture_output=True,
                            encoding='utf-8')
    return re.search(self.regex, result.stderr) is not None

def scan(d: os.PathLike):
  vid = re.compile('\.(avi|mkv|mp4)$', re.IGNORECASE)
  sub = re.compile('\.(srt|ass|sub)$', re.IGNORECASE)
  embedded_sub = CheckToolOutput(['ffprobe', '-hide_banner'],
                                 'Stream.*: Subtitle')
  for root, dirs, files in os.walk(d):
    videos = [f for f in files if re.search(vid, f)]
    subtitles = [f for f in files if re.search(sub, f)]
    missing = [v for v in videos
               if not any(map(lambda s: s.startswith(v[:-4]), subtitles))
               and not embedded_sub.check(Path(root).joinpath(Path(v)))]
    for m in missing:
      print(m)

def main():
  if not which('ffprobe'):
    print('ffprobe not in PATH; check installation.', file=sys.stderr)
  elif len(sys.argv) <= 1:
    print(f'usage: {sys.argv[0]} DIRECTORY [DIRECTORY ...]')
  else:
    for d in sys.argv[1:]:
      scan(d)

main()
