#!/usr/bin/env python3

import preppy
from z3c.rml import rml2pdf

# Read template
m = preppy.getModule('template.prep')

# Build context
ns = {'to': 'amma', 'say': 'Love you amma!  Thaai illamal naan illai :)'}

# Pass context and render template
rml = m.getOutput(ns, quoteFunc=preppy.stdQuote, lquoteFunc=None)

# Build PDF from RML
b = rml2pdf.parseString(rml)

# Write in-memory PDF to disk
with open('template.pdf', 'wb') as f:
  f.write(b.getbuffer())
