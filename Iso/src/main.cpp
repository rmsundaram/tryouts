#include "config.h"

#include <raylib.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtx/string_cast.hpp>

#include <iostream>
#include <string>
#include <type_traits>
#include <cstdio>
#include <cassert>
#include <filesystem>

namespace fs = std::filesystem;

constexpr auto TILE_WIDTH = 64;
constexpr auto TILE_HEIGHT = 32;

constexpr auto TILES_X = 4;
constexpr auto TILES_Y = 3;

constexpr glm::vec2 TILE[] = {{0,33}, {0,384}};
constexpr glm::vec2 TILE_SIZE[] = {{64, 32}, {64, 64}};

// Reference
// http://clintbellanger.net/articles/isometric_math/
//
// Trick is to treat X and Y independantly.  Observe that moving 1 X unit in
// (orthogonal) map means moving ½ tile width in screen X and ½ height in
// screen Y.  Likewise moving 1 Y unit in map means moving -½ tile width in
// screen X and ½ height in screen Y.  Combine both equations:
//
// Sx = ( Mx * Tw, Mx * Th)    // assume Tw = ½ tile width
// Sy = (-My * Tw, My * Th)    // assume Th = ½ tile height
//
// Screen = ((Mx - My) * Tw, (Mx + My) * Th)
//
// Assuming tile size of 64 x 32, moving to (1, 0) on map = (32, 16) on screen.
// Moving to (2, 1) on map = (32, 48) on screen.
//
// Transform map coordinates to view coordinates
glm::vec2 ortho_to_iso(glm::vec2 ortho, glm::vec2 tile_half) {
  return {(ortho.x - ortho.y) * tile_half.x,       // (1)
          (ortho.x + ortho.y) * tile_half.y};      // (2)
}

// Transforming from screen to map coordinates done with algebra:
//
// Sx = (x - y) Tw
// Sy = (x + y) Th
//
// (Sx / Tw) + y = x                   x - (Sx / Tw) = y
// (Sy / Th) - y = x                   (Sy / Th) - x = y
//
// (Sx / Tw) + y = (Sy / Th) - y       x - (Sx / Tw) = (Sy / Th) - x
// (Sy / Th) - (Sx / Tw) = 2y          2x = (Sy / Th) + (Sx / Tw)
//
//     (Sy / Th) - (Sx / Tw)               (Sy / Th) + (Sx / Tw)
// y = ---------------------           x = ---------------------
//               2                                   2
glm::vec2 iso_to_ortho(glm::vec2 iso, glm::vec2 tile_half) {
  auto const Mx = ((iso.y / tile_half.y) + (iso.x / tile_half.x)) / 2;
  auto const My = ((iso.y / tile_half.y) - (iso.x / tile_half.x)) / 2;
  return {Mx, My};
}

Vector2 to_rl_vec2(glm::vec2 v) {
  return {v.x, v.y};
}

glm::vec2 from_rl_vec2(Vector2 v) {
  return {v.x, v.y};
}

struct Screen {
  Screen(float tile_width, float tile_height, float tiles_x, float tiles_y)
    : bounds_tile {tile_width, tile_height}
    , bounds_half_tile {tile_width * 0.5f, tile_height * 0.5f}
    , tiles {tiles_x, tiles_y}
    , origin_screen {0, 0}
  {
    // left-bottom’s left and right-top’s right give the X bounds
    auto const left_bottom = ortho_to_iso(glm::vec2{0, tiles.y},
                                          bounds_half_tile);
    auto const right_top = ortho_to_iso(glm::vec2{tiles.x, 0}, bounds_half_tile);
    assert(left_bottom.x < 0);
    bounds_screen.x = right_top.x - left_bottom.x;
    auto const right_bottom = ortho_to_iso(glm::vec2{tiles.x, tiles.y},
                                           bounds_half_tile);
    bounds_screen.y = right_bottom.y;
    origin_screen.x = -left_bottom.x;

    // Populate convenience transforms.
    // Long form of M_tile_screen:
    // const auto shear = glm::mat2x2( bounds_half_tile.x, bounds_half_tile.y,
    //                                -bounds_half_tile.x, bounds_half_tile.y);
    // const auto translate = glm::mat3x2(1.0, 0.0,
    //                                    0.0, 1.0,
    //                                    origin_screen.x, 0.0);
    // const auto res = translate * glm::vec3(shear * glm::vec2(tile.x, tile.y),
    //                                        1.0);
    // Matrix form of
    // ortho_to_iso(tile, bounds_half_tile).translate(origin_screen.x, 0);
    M_tile_screen = glm::mat3x2( bounds_half_tile.x, bounds_half_tile.y,
                                -bounds_half_tile.x, bounds_half_tile.y,
                                 origin_screen.x, 0.0);
    M_screen_tile = glm::affineInverse(glm::mat3(M_tile_screen));
  }

  glm::vec2 GetWorldOrigin() const {
    return origin_screen;
  }

  glm::vec2 GetBounds() const {
    return bounds_screen;
  }

  // map tile ID to tile’s origin in screen space
  glm::vec2 GetScreenCoords(glm::vec2 tile) const {
    return M_tile_screen * glm::vec3(tile.x, tile.y, 1.0);
  }

  glm::vec2 GetScreenLeftTop(glm::vec2 tile) const {
    // convert tile’s origin to left-top since in orthogonal screen space
    // tile’s origin isn’t left top; there’s an X offset
    return GetScreenCoords(tile) + glm::vec2(-bounds_half_tile.x, 0);
  }

  // map screen space to world space
  glm::vec2 GetWorldCoords(glm::vec2 screen) const {
    return M_screen_tile * glm::vec3(screen.x, screen.y, 1.0);
  }

  // map screen space point to tile ID
  glm::ivec2 GetTile(glm::vec2 screen) const {
    return glm::floor(GetWorldCoords(screen));
  }

  glm::vec2 GetOffsetWithinTile(glm::vec2 tile) const {
    return tile - glm::floor(tile);
  }

  // Data

  glm::vec2 const bounds_tile;
  glm::vec2 const bounds_half_tile;
  glm::vec2 const tiles;
  glm::vec2 bounds_screen;
  glm::vec2 origin_screen;

  // 2 row × 3 col
  glm::mat3x2 M_tile_screen;
  glm::mat3x2 M_screen_tile;
};

int main()
{
  Screen s{TILE_WIDTH, TILE_HEIGHT, TILES_X, TILES_Y};
  auto const bounds = s.GetBounds();
  Vector2 const cell[4] = { to_rl_vec2(s.GetScreenCoords({0, 0})),
                            to_rl_vec2(s.GetScreenCoords({0, TILES_Y})),
                            to_rl_vec2(s.GetScreenCoords({TILES_X, TILES_Y})),
                            to_rl_vec2(s.GetScreenCoords({TILES_X, 0})) };

  InitWindow(bounds.x, bounds.y, "Isometric Projection");

  // convert to generic string; direct c_str returns wchar_t* on Win32
  auto const tex_path =
    fs::path(RES_DIR).append("res/iso-64x64-outside.png").generic_string();
  auto tex = LoadTexture(tex_path.c_str());

  SetTargetFPS(60);

  while (!WindowShouldClose())
  {
    BeginDrawing();
    ClearBackground(BLACK);

    // deduce mouse-over tile
    auto const screen_pos = GetMousePosition();
    auto const pos = s.GetTile(from_rl_vec2(screen_pos));

    // draw tiles
    for (auto row = 0u; row < s.tiles.y; ++row) {
      for (auto col = 0u; col < s.tiles.x; ++col) {
        auto const t_id = 0;
        Rectangle const src {TILE[t_id][0],         // x
                             TILE[t_id][1],         // y
                             TILE_SIZE[t_id][0],    // width
                             TILE_SIZE[t_id][1]};   // height
        glm::vec2 const dst = s.GetScreenLeftTop({static_cast<float>(col),
                                                  static_cast<float>(row)});
        auto const tint = (pos != glm::ivec2(col, row)) ? WHITE : ORANGE;
        DrawTextureRec(tex, src, to_rl_vec2(dst), tint);
      }
    }

    // draw world boundary
    DrawLineV(cell[0], cell[1], GOLD);
    DrawLineV(cell[1], cell[2], GOLD);
    DrawLineV(cell[2], cell[3], GOLD);
    DrawLineV(cell[3], cell[0], GOLD);

    // draw mouse-over tile ID
    char msg[300] = {};
    snprintf(msg, std::extent<decltype(msg)>::value,
             "%d, %d",
             pos.x, pos.y);
    DrawText(msg, 1 /*X*/, 1 /*Y*/, 9 /*font size*/, RAYWHITE);

    EndDrawing();
  }

  UnloadTexture(tex);

  CloseWindow();
}
