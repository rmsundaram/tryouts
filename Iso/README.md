# Isometric Rendering

A skeleton project to understand transforms involved in making an isometric game.

# Scope

* Render n x m isometric tiles
* Map mouse clicks to tile X, Y
* Render taller/wider tiles on a higher level e.g. tree, house, etc.

# Basic Math

Isometric tiles usually have 2:1 ratio i.e. twice wide as they’re tall e.g. 64 x 32 pixels.  There may be an affine map from orthogonal to isometric and vice-versa.  Formulate and code this up.

# Rendering

* [x] Map isometric to orthographic and vice-versa
* [x] Determine world bounds and origin orthographically
* Additional width and height for taller/wider tiles in boundary

# Build and Debug

Setting an absolute `--prefix` path lets us install to a sub-directory.  Then we conveniently debug without worrying about the executable (at `//build/debug`) picking up the right `data` directory.

``` bash
meson --prefix="$PWD/build/debug" build && cd build
meson compile
meson install  # needed for absolute path lookups to work
gdb iso        # don't use stripped binary within build/debug/bin
```

# Assets Path

Assets directory path is obtained from the macro `RES_DIR`.  This macro is set, in `config.h`, at configure-time based on meson arguments `--prefix` and `--datadir`.  Effectively, the generated _binary has a hard-coded path_ for resource/data/assets directory.

## Linux

Most Linux distributions specify a standard _share_ directory for assets e.g. [Arch Linux’s `/usr/share`][arch-share-dir]; packaging for distros work by setting this directory at configure-time.  Irrespective of the current working directory the executable is invoked from, it’ll always search this hard-coded path for assets.

## Windows

On Windows, the application package’s final location isn’t known at build-time.  Also there isn’t a common directory hierarchy like `/bin`, `/usr/lib`, etc.  Each software package has all of its files under one directory generally.  Hence application packages are expected to be relocatable.  This means no hard-cording of paths.  Code should be written such that the executable’s directory and directories relative to it can be searched.  The searched path is dependant on the executable’s directory and independent of the current working directory it was run from.

This project isn’t relocatable yet.

[arch-share-dir]: https://man.archlinux.org/man/file-hierarchy.7.en#VENDOR-SUPPLIED_OPERATING_SYSTEM_RESOURCES

# Credits

* [Isometric Tiles Math - Clint Bellanger](http://clintbellanger.net/articles/isometric_math/)
* [Coding Quickie: Isometric Tiles - OneLoneCoder](https://www.youtube.com/watch?v=ukkbNKTgf5U)
* [Isometric 64×64 Outside Tileset - OpenGameArt.org](https://opengameart.org/node/3012)
