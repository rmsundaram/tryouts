// Unified logging system (OSLog or os_log) superseding ASL and Syslog APIs;
// available macOS 10.12+, iOS 10+, ….  Use `os_log` with both Obj-C or Swift;
// Since macOS 11, iOS 14, Swift has convenience wrapper `struct Logger`.

// swiftc logging.swift && ./logging
import os.log

// ---------- WRITE ----------
// Create custom log object; can also be initialized without subsystem/category
var l = Logger(subsystem: "org.FOSS.Coreutils", category: "Basic")
let pi = 3.14159265359
l.debug("Computing value of Pi...")  // doesn’t show up unless run under Xcode
l.info("Simple information display: \(pi)")
l.log("Logging complete")

// Pre-macOS 11/pre-iOS 14 use os_log
// // let log = OSLog(subsystem: "medium_article", category: "basic")
// // os_log("Error Message", log: log, type: .info)
// Obj-C: use os_log_with_type() from OSLog/OSLog.h

// ---------- READ -----------
// Check output with `log`; Console.app never showed these on macOS 11 or 12
//   log show --predicate 'process == "logging"' --info --debug --last 5m
// Interesting Predicates
//   sender == "logging"
//   category CONTAINS "Basic"
//   eventMessage MATCHES "value of Pi.*"
//   eventMessage BEGINSWITH "Computing value"
//   subsystem == "org.FOSS.Coreutils"
//   (sender == "logging") && (category contains "Basic")

// References
//   1. man log
//   2. https://developer.apple.com/documentation/os/logging/generating_log_messages_from_your_code
//   3. https://stevenpcurtis.medium.com/logging-in-swift-d9b59146ff00
//   4. https://apple.stackexchange.com/questions/256769/how-to-use-logger-command-on-sierra
//   5. https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/Predicates/Articles/pSyntax.html
//   6. https://www.raywenderlich.com/605079-migrating-to-unified-logging-console-and-instruments
//   7. https://steipete.com/posts/logging-in-swift/
//   8. https://www.sentinelone.com/blog/getting-started-swift-logging/
