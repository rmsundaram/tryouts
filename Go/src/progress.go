package main

import (
	"fmt"
	"strings"
	"time"
)

// TODO: this assumes that the terminal width will be at least 103 chars.
// Instead adapt this to interpolate based on the available width.
func main() {
	var toClean int
	for i := 0; i < 101; i++ {
		time.Sleep(30 * time.Millisecond)
		eqs := strings.Repeat("=", i) + strings.Repeat(" ", 100-i)
		toClean, _ = fmt.Printf("\r[%s]", eqs)
		//fmt.Printf("\r%2v%%", i)
	}
	fmt.Printf("\r%s\rDone.", strings.Repeat(" ", toClean))
}
