package simphy

import "github.com/go-gl/mathgl/mgl32"

const (
	ShapeAABB = 0 + iota
	ShapeCircle
)

type ShapeType interface {
	GetType() int
}

type AABB struct {
	Min, Max mgl32.Vec2
}

type Circle struct {
	Radius float32
}

func (*AABB) GetType() int {
	return ShapeAABB
}

func (*Circle) GetType() int {
	return ShapeCircle
}

func AABBvsAABB(a, b AABB) bool {
	return !((a.Min[0] > b.Max[0]) || (b.Min[0] > a.Max[0]) ||
		(a.Min[1] > b.Max[1]) || (b.Min[1] > a.Max[1]))
}

func Distance2(p1, p2 mgl32.Vec2) float32 {
	d := p2.Sub(p1)
	return d.Dot(d)
}
