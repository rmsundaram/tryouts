package simphy

import (
	"math"

	"github.com/go-gl/mathgl/mgl32"
)

type ContactManifold struct {
	Point  mgl32.Vec2
	Normal mgl32.Vec2
}

func CirclevsCircle(a, b Body) *ContactManifold {
	cA := *Circle(a.Shape)
	cB := *Circle(b.Shape)
	r := cA.Radius + cB.Radius
	n := b.Pos.Sub(a.Pos)
	d2 := n.Dot(n)
	if d2 < (r * r) {
		d := math.Sqrt(d2)

	}
	return nil
}
