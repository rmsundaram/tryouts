# Simple 2D Physics

I want to learn the basics of a complex Physics engine.  This is my attempt to learn it by building a toy physics engine.  I'm doing this in [Go](https://www.golang.org/) to kill two birds in one stone; yes, I want to dabble in Go too in taking up this experiment.

Since this isn't a game, it doesn’t have extensive graphics.  [GLFW](http://www.glfw.org/) and OpenGL (through [go-gl](https://github.com/go-gl/)) to create a window and draw primitives like boxes and spheres.

See [notes](notes.md.html) for concepts and references.
