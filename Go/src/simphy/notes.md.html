<meta charset="utf-8" lang="en">

        **Linear Motion of Rigid Bodies**

Rigid bodies are bodies that don't deform i.e remain in tact despite collisions which makes the math simple.  Hence game physics engines have thus far been only rigid body based.  Linear motion refers to the motion of a body in space without considering rotation.  Angular motion refers to rotation of a body about any axis.  They're independent and can (and mostly do) occur simultaneously.

# Basics

_Mass_ is the measure of a body's resistance to motion or a change in its motion.  Thus, the greater a body's mass, the harder it'll be to set it in motion or stop it.  _Centre of mass_ is a point in a body through which a force can act on it without resulting in a rotation.  Analogous to mass being a measure of a body’s resistance to linear motion, _mass moment of inertia_ (also known as _rotational inertia_) is a measure of a body’s resistance to rotational motion.  Moment of inertia is a quantitative measure of the radial distribution of the mass of a body about a given axis of rotation.

1. Velocity is speed with direction
2. Rate of change of distance is velocity; it's the time derivative of distance
3. Rate of change of velocity is acceleration; it's the time derivative of velocity and the second time derivative of distance
4. Rate of change of acceleration is jerk
5. Momentum = Mass x Velocity
6. Newton's second law

$$ F = mA $$
$$ F = m \times \frac{dv}{dt} $$
$$ F \times dt = m \times dv $$

This is _impulse_; force over a period of time which results in an instantaneous change in velocity. It's the integral of force with respect to time.

> "Applying a force changes the acceleration of an object. If we instantly change the force, the acceleration instantly changes too. We can think of acting on an object to change its velocity in a similar way. Rather than a force, this is called an _impulse_, an instantaneous change in velocity."
>
>     -- § 7.1.4 Impulses, Game Physics Engine Development, Ian Millington

$$ f = m \ \ddot{p} $$
$$ g = m \ \dot{p} $$

where $f$ is force, $g$ is impulse, $p$ is position and $m$ is mass.  Since momentum is $m \times v$, impulse is defined as the change in momentum.

# Collision Detection

* Find if two shapes are colliding
* If yes generate manifolds

# Contact Manifolds

* Point(s) of contact
* Separation/collision normal; direction and magnitude

# Collision Resolution

* Modifying two intersecting objects so as to not allow them to remain so
* Impulse resolution is a collision resolution strategy
    * Idea is to use an impulse (instantaneous change in velocity) to separate colliding objects
    * We need position, velocity, mass (larger objects move out slowly; ∞ mass object don't move at all)
    * To apply an impulse, we know the direction of impulse (from the contact manifold data), we need to solve for the magnitude only

## Math

The _closing velocity_ is the total speed at which two bodies are moving together.  We calculate the closing velocity by finding the component of their velocity in the direction from one another:

$$ \vec{V_c} = - (\vec{V_A} - \vec{V_B}) $$

However, instead of working with the closing velocity, we work with the _separating velocity_ and drop the minus.

$$ \vec{V_s} = \vec{V_A} - \vec{V_B} \tag{1} \label{1} $$

Now to know this along the direction of the separation normal, dot it with $\hat{n}$ on both sides.

$$ \vec{V_s} \cdot \hat{n} = (\vec{V_A} - \vec{V_B}) \cdot \hat{n} $$

The _Law of Restitution_ states that the _coefficient of restitution_ controls the speed at which the bodies will separate after a collision.   It is the elasticity/bounciness of a collision.  It normally ranges from 0 to 1 where 1 would be a perfectly elastic collision.  Technically, it's the ratio of the final to initial relative velocity between two objects after they collide.  Practically, it is passed in by the user, per body.  So we've the separating velocity after collision as

$$ \vec{V'} = -e \ \vec{V} $$

!!! NOTE: Negative coefficient
    We introduce the negative sign because the restitution law states that the resulting vector $V'$ after the bounce goes in the direction opposite to $V$.  Since we've already deduced the direction of our impulse, we counter it by introducing a minus sign.

[Wikipedia](https://en.wikipedia.org/wiki/Coefficient_of_restitution) has a list of materials and their restitution coefficients.  When calculating impulse between two bodies, we choose the one with the least coefficient:

``` c++
e = min(bodyA.restitution, bodyB.restitution)
```

Factoring this into the previous equation, we get the separating velocity after collision as

$$ \vec{V'_s} \cdot \hat{n} = -e \ (\vec{V_A} - \vec{V_B}) \cdot \hat{n} \tag{2} \label{2} $$

An impulse is the instantaneous change in momentum; for an impulse of magnitude $j$ along some direction $\hat{d}$, we've the change in momentum as

$$ \vec{V'} = \vec{V} + j \ \hat{d} $$

where $\vec{V}$ is the initial velocity before the impulse.  However, momentum is $mass \times velocity$.  So to get only the velocity due to the impulse, we divide by mass

$$ \vec{V'} = \vec{V} + \frac{j \ \hat{d}}{mass} $$

Now considering two colliding bodies A and B; their post-collision momentum along the separating normal $\hat{n}$ would be

$$ \vec{V'_{A}} = \vec{V_A} + \frac{j \ \hat{n}}{mass_A} $$

$$ \vec{V'_{B}} = \vec{V_B} - \frac{j \ \hat{n}}{mass_B} $$

!!! NOTE: Negative impulse
    One of the bodies will be pushed in the direction opposite to the other.  We choose B.

From $\eqref{1}$ we know

$$ \vec{V'_{s}} = \vec{V'_A} - \vec{V'_B} $$

Substituting this into $\eqref{2}$ we get

$$ (\vec{V'_A} - \vec{V'_B}) \cdot \hat{n} = -e \ (\vec{V_A} - \vec{V_B}) \cdot \hat{n} $$

$$ (\vec{V_A} + \frac{j \hat{n}}{m_A} - \vec{V_B} + \frac{j \hat{n}}{m_B}) \cdot \hat{n} + e (\vec{V_A} - \vec{V_B}) \cdot \hat{n} = 0 $$

$$ (1 + e) [(\vec{V_A} - \vec{V_B}) \cdot \hat{n}] + j (\frac{1}{m_A} + \frac{1}{m_B}) \hat{n} \cdot \hat{n} = 0 $$

$$ \fbox{ $j = - \frac{(1 + e)[(\vec{V_A} - \vec{V_B}) \cdot \hat{n}]}{\frac{1}{m_A} + \frac{1}{m_B}}$ } $$

We've all the components on the RHS to get the impulse magnitude on the LHS.

# References

1. Randy Gaul's [Custom 2D Physics Engine series](https://gamedevelopment.tutsplus.com/series/how-to-create-a-custom-physics-engine--gamedev-12715)
2. _Physics for Game Developers_, 2<sup>nd</sup> Edition by Bourg, Bywalec -- Chapter 5, §Linear and Anuglar Impulse
3. _3D Math Primer_, 2<sup>nd</sup> Edition by Dunn, Parberry -- §12.4.2 General Collision Response
4. _Game Physics Engine Development_, 2<sup>nd</sup> Edition by Ian Millington -- Chapter 7
5. Physics Class Room; [Momentum and Impulse connection](http://www.physicsclassroom.com/class/momentum/Lesson-1/Momentum-and-Impulse-Connection)
6. [Designing a Physics Engine in 5 mins](https://blog.winter.dev/2020/designing-a-physics-engine/)

# See Also

1. [Box2D Lite](https://github.com/erincatto/box2d-lite)
2. [Tiny Physics Engine](https://codeberg.org/drummyfish/tinyphysicsengine)
3. [Lightweight 3D Physics Engine](https://github.com/RandyGaul/qu3e)


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none'};</script>
