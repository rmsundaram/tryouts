package simphy

import "github.com/go-gl/mathgl/mgl32"

type Body struct {
	Pos         mgl32.Vec2
	Shape       ShapeType
	InvMass     float32
	Restitution float32
}
