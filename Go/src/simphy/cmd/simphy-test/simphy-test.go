package main

import (
	"fmt"
	"runtime"

	"github.com/go-gl/gl/v2.1/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/go-gl/mathgl/mgl32"

	phy "github.com/legends2k/simphy"
)

func init() {
	// This is needed to arrange that main() runs on main thread.
	// See documentation for functions that are only allowed to be
	// called from the main thread.
	runtime.LockOSThread()
}

const windowWidth = 640.0
const windowHeight = 480.0
const aspectRatio = windowWidth / windowHeight

func createWindow() *glfw.Window {
	err := glfw.Init()
	if err != nil {
		panic(err)
	}

	glfw.WindowHint(glfw.ContextVersionMajor, 2)
	glfw.WindowHint(glfw.ContextVersionMinor, 1)
	glfw.WindowHint(glfw.Resizable, gl.FALSE)
	window, err := glfw.CreateWindow(windowWidth, windowHeight,
		"Simple Physics", nil, nil)
	if err != nil {
		panic(err)
	}

	window.MakeContextCurrent()

	if err := gl.Init(); err != nil {
		panic(err)
	}
	return window
}

func main() {
	window := createWindow()
	defer glfw.Terminate()
	defer window.Destroy()

	gl.ClearColor(0, 0, 0, 1)

	gl.MatrixMode(gl.PROJECTION)
	gl.LoadIdentity()
	gl.Ortho(-1, 1, -1, 1, -1, 1)

	gl.MatrixMode(gl.MODELVIEW)
	gl.LoadIdentity()
	gl.Translatef(-1, -1, 0)
	gl.Scalef(2/windowWidth, 2/windowHeight, 1)

	for !window.ShouldClose() {

		gl.Clear(gl.COLOR_BUFFER_BIT)

		gl.Begin(gl.QUADS)
		gl.Color3f(0.0, 0.0, 1.0)
		gl.Vertex2f(0, 0)
		gl.Color3f(0.0, 1.0, 0.0)
		gl.Vertex2f(windowWidth/2, 0)
		gl.Color3f(1.0, 0.0, 0.0)
		gl.Vertex2f(windowWidth/2, windowHeight/2)
		gl.Color3f(1.0, 1.0, 0.0)
		gl.Vertex2f(0, windowHeight/2)
		gl.End()

		window.SwapBuffers()
		glfw.PollEvents()
	}

	c := phy.Circle{Radius: 12.0, Position: mgl32.Vec2{0, 10}}
	b := phy.Body{&c, 0.5, 0.7}
	fmt.Println(b.Shape.GetType())
}
