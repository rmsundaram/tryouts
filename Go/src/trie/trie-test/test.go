package main

import (
	"fmt"
	"os"
	"strings"
	"trie"
)

func main() {
	if len(os.Args) <= 1 {
		fmt.Println("Usage: test-trie A TEST PHRASE")
	} else {
		s := strings.Join(os.Args[1:], " ")
		t := trie.Make()
		if trie.Insert(t, s) {
			fmt.Print("Insertion successful.\nSearch term: ")
			var needle string
			n, err := fmt.Scanf("%s\n", &needle)
			if err != nil || (n != 1) {
				fmt.Println("Error receiving search term")
			}
			result := trie.Find(t, needle)
			fmt.Printf("%v occurs %v times in input\n", needle, result)
		}
	}
}
