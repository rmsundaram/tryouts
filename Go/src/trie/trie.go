// Package trie provides a simple, sequential trie implementation
package trie

import "fmt"

// Inspired by Olivier Giroux’s High-Radix Concurrernt C++
// https://www.youtube.com/watch?v=75LcDvlEIYw
// https://github.com/ogiroux/freestanding
// Olivier makes a concurrent trie using GPU, while here we try to make this
// concurrent using goroutines, with atomics of course.
// TODO: make this sequential implentation concurrent

// trie a.k.a radix tree
// Salient feature: keys are not stored in the tree, but are the locations in
// the tree; node doesn’t have any data, but just a high-fan out links array
// Note: It’s not necessary that only leaf nodes have a word; consider the words
// "manage" and "management", former will end in a non-leaf node, so isLeaf is
// perhaps not a great option for the value part of this associative structure
type node struct {
	links [26]*node // radix 26; each node has 26 links
	count int       // an arbitrary value; we store count
}

// Make returns an empty trie; the root node
func Make() *node {
	return new(node)
}

// only for English, case-insensitive
func runeToIdx(r rune) int {
	d := int(r - 'a')
	if (d >= 0) && (d <= 25) {
		return d
	}
	D := int(r - 'A')
	if (D >= 0) && (D <= 25) {
		return D
	}
	return -1
}

// Insert inserts words in key into r, a trie node, usually the root
// key is expected to contain English phrases
func Insert(r *node, key string) bool {
	if len(key) == 0 {
		return false
	}
	if r == nil {
		panic(fmt.Errorf("Trie to insert '%v' is nil", key))
	}

	n := r
	idx := -1
	for _, c := range key {
		idx = runeToIdx(c)
		if idx != -1 {
			if n.links[idx] == nil {
				n.links[idx] = new(node)
			}
			n = n.links[idx]
		} else {
			// maintain root’s emptiness: if a string starts with a delimiting
			// character, avoid bumping root’s count
			if n != r {
				n.count++
				n = r
			}
			continue
		}
	}
	// bump count if last word doesn’t have a terminating character
	if idx != -1 {
		n.count++
	}
	return true
}

// Find returns the count of a word’s occurance in node, a trie
func Find(n *node, word string) int {
	if n == nil {
		panic(fmt.Errorf("Trie node to find '%v' is nil", word))
	}
	for _, c := range word {
		idx := runeToIdx(c)
		if (idx == -1) || (n.links[idx] == nil) {
			// not a valid search entry, since it contains non-English character
			return 0
		}
		n = n.links[idx]
	}
	return n.count
}
