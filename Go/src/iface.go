package main

import "fmt"

type Point struct {
	x, y int
}

type Pt interface {
	move(s, t int)
	scale(p, q int)
}

func (p *Point) move(s, t int) {
	p.x += s
	p.y += t
}

// asterisk needed for pass by reference
func (p *Point) scale(s, t int) {
	p.x *= s
	p.y *= t
}

func main() {
	p := Point{1, 2}
	fmt.Println(p.x, p.y)
	var q Pt = &p
	q.move(12, -3)
	q.scale(1, 4)
	fmt.Println(p.x, p.y)
}
