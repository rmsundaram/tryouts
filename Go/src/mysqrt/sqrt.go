package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
)

// Newton-Raphson method of finding sqrt
// https://tour.golang.org/flowcontrol/8
func Sqrt(x float64) (float64, int) {
	// commonly used first guesses 1, x, x/2
	guess := x / 2.0
	// TODO: get rid of err; change loop condition to
	// quit when guess’ value no longer changes
	err := math.Inf(1)
	i := 0
	for err > 0.00000001 {
		guess = ((guess * guess) + x) / (2 * guess)
		err = (guess * guess) - x
		i++
		fmt.Printf("Error in iteration %d: %v\n", i, err)
	}
	return guess, i
}

func main() {
	if len(os.Args) > 1 {
		num, err := strconv.ParseFloat(os.Args[1], 64)
		if (err != nil) || (num <= 0) {
			fmt.Println("Please enter a valid positive number")
			return
		}
		s, n := Sqrt(num)
		fmt.Printf("√%v = %v found in %v iterations\n", num, s, n)
	} else {
		fmt.Println("Usage: mysqrt NUMBER")
	}
}
