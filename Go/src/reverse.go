// Package main: refer sort.IntSlice to understand code here
package main

import (
	"fmt"
)

// RevRange is an interface to any reversible range
type RevRange interface {
	Len() int
	Swap(i, j int)
}

// reverse reverses a reversible range
func reverse(r RevRange) {
	// uint wouldn’t work for empty slices
	i, n := 0, r.Len()-1
	for ; i < n; i, n = i+1, n-1 {
		r.Swap(i, n)
	}
}

// to make standard slices adhere to RevRange interface we create alias

// alias for []int
type intSlice []int

// Implement RevRange methods for intSlice
func (r intSlice) Len() int {
	return len(r)
}
func (r intSlice) Swap(i, j int) {
	r[i], r[j] = r[j], r[i]
}

type myString []rune

func (s myString) Len() int {
	return len(s)
}
func (s myString) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

// Needed To print myString by fmt.Println
// without this, it’d just print numeric rune values
func (s myString) String() string {
	return string(s)
}

func main() {
	s := []int{12, 32, 45, 9}
	fmt.Println(s)
	reverse(intSlice(s))
	fmt.Println(s)
	ms := myString("hello")
	reverse(ms)
	fmt.Println(ms)
}
