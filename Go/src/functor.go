package main

import (
	"fmt"
	"os"
	"strconv"
)

func make_adder(x int64) func(int64) int64 {
	return func(n int64) int64 {
		return x + n
	}
}

func main() {
	n, _ := strconv.ParseInt(os.Args[1], 10, 64)
	v := make_adder(n)
	fmt.Println(v(2))
}
