package main

import (
	"fmt"
	"regexp"
	"unicode/utf8"
)

// https://mymemorysucks.wordpress.com/2017/05/03/a-short-guide-to-mastering-strings-in-golang/
func main() {
	s := "தமிழ்"
	// this gives the byte count
	fmt.Printf("len('%s') = %d\n", s, len(s))
	// different runes have different length; hence utf8.RuneLen()
	fmt.Printf("len('Tam') = %v\n", len("Tam"))
	// this gives the rune count
	fmt.Printf("utf8.RuneCountInString('%s') = %d\n", s, utf8.RuneCountInString(s))
	// slightly inferior way of doing utf8.RuneCountInString
	ra := []rune(s)
	fmt.Printf("len([]rune('%s')) = %d\n", s, len(ra))

	// https://play.golang.org/p/RMMO7luJvh
	//Correct implementation for counting grapheme clusters
	// https://stackoverflow.com/a/27331885/183120
	r := regexp.MustCompile(`\PM\pM*|.`).FindAllString(s, -1)
	fmt.Printf("%v -> %#v\n", r, len(r))
}
