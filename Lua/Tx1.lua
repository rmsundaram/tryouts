function dot(a, b)
  local c = 0
  for i = 1, math.min(#a, #b) do
    c = c + (a[i] * b[i])
  end
  return c
end

function mat_col(m, i)
  local col = {}
  for k = 1, #m do
    col[k] = m[k][i]
  end
  return col
end

function mat_mult(a, b)
  local c = {}
  for i = 1, #a do
    local row = {}
    for j = 1, #b[1] do
      row[j] = dot(a[i], mat_col(b, j))
    end
    c[i] = row
  end
  return c
end

function mat_print(a)
  for i = 1, #a do
    print(table.unpack(a[i]))
  end  
end

 local M = { {839.04004,         0, 0},
             {        0, 627.52002, 0},
             {101.76001, 721.91998, 1} }

local old_rect_lt = {{-0.00090580434,    -1.0006373, 1}}
local old_rect_rb = {{     1.0002382, 0.00012755394, 1}}
local tx_old_rect_lt = mat_mult(old_rect_lt, M)
local tx_old_rect_rb = mat_mult(old_rect_rb, M)
mat_print(tx_old_rect_lt)
mat_print(tx_old_rect_rb)

local new_rect_lt = {{0.00000000745, -0.99999994, 1}}
local new_rect_rb = {{   0.99999994,           0, 1}}
local tx_new_rect_lt = mat_mult(new_rect_lt, M)
local tx_new_rect_rb = mat_mult(new_rect_rb, M)
mat_print(tx_new_rect_lt)
mat_print(tx_new_rect_rb)

local M1 = {{0, 1.3333331, 0},
            {1.3333051, 0, 0},
            {1332, 1.1133423, 1}}
local rr_lt = {{0.5, 0.5}};
local rr_rb = {{126.165, 40.963501}};
local res_lt = mat_mult(rr_lt, M1)
local res_rb = mat_mult(rr_rb, M1)
print("---------")
mat_print(res_lt)
mat_print(res_rb)
