local user_vals = {
342, 343,
345, 345,
348, 350,
353, 353,
355, 356,
359, 361,
364, 364,
368, 368,
372, 373,
375, 375,
377, 378,
383, 383,
386, 387,
390, 391,
393, 394,
397, 398,
400, 402,
404, 404,
408, 409,
413, 413,
415, 415,
419, 419,
424, 424,
427, 428,
430, 430,
433, 433,
438, 439,
441, 443,
445, 446,
448, 450,
452, 452,
456, 458,
460, 461,
463, 464,
468, 470,
472, 472,
474, 475
}

local scale = 1.1
local switch = 2
local dev_real = {}
local dev_fit = {}
local edges = {}
local gaps = {}
for i = 1, #user_vals, 2 do
  dev_real[#dev_real + 1] = scale * user_vals[i]
  dev_real[#dev_real + 1] = scale * user_vals[i + 1]
  dev_fit[#dev_fit + 1] = math.floor(dev_real[#dev_real - 1])
  dev_fit[#dev_fit + 1] = math.floor(dev_real[#dev_real])
  local real_width = math.abs(dev_real[#dev_real] - dev_real[#dev_real - 1])
  local fit_width = math.abs(dev_fit[#dev_fit] - dev_fit[#dev_fit - 1])
  if (fit_width == 0) or (fit_width < real_width) then
    dev_fit[#dev_fit] = dev_fit[#dev_fit] + 1
  end  

  io.write(dev_real[#dev_real - 1] .. '\t' ..
           dev_real[#dev_real] .. '\t' ..
           dev_fit[#dev_fit - 1] .. '\t' ..
           dev_fit[#dev_fit] .. '\t')

  edges[#edges + 1] = dev_fit[#dev_fit - 2 + switch]
  -- code to alternate between 1 and 2
  switch = ((switch + 1) % 2) ~= 0
  switch = switch and 1
  switch = switch or 2

  if dev_fit[#dev_fit - 2] then
    gaps[#gaps + 1] = dev_fit[#dev_fit - 1] - 1 - dev_fit[#dev_fit - 2]
    io.write(gaps[#gaps])
  end
  io.write('\n')
end

local zeros = 0
for i = 1, #gaps do
  if gaps[i] == 0 then
    zeros = zeros + 1
  end  
end
print(zeros)
