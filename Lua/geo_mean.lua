-- http://stackoverflow.com/a/35716729/183120
-- Geometric mean: pow(a1 × a2 × a3 × … × an, 1 / n)
-- Since the product's result may be very large that the datatype can overflow,
-- an option would be to use log and exponentiation:
-- r = (a1 × a2 × a3 × … × an)^(1/n)
-- log r = log(a1 × a2 × a3 × … × an) / n
-- log r = (log a1 + log a2 + log a3 + … + log an) / n      (log product rule)
-- r = e^x, where x = (log a1 + log a2 + log a3 + … + log an) / n
function geo_mean(...)
   local args = { ... }
   local total = 0;
   for i, v in ipairs(args) do
      total = total + math.log(v)
   end
   return math.exp(total / #args)
end

-- returns nan for no arguments since 0th root is undefined
-- http://math.stackexchange.com/a/962813/51968
print(geo_mean(table.unpack(arg)))
