-- http://stackoverflow.com/a/34942587

local mt = {}
local base = string.byte('x') - 1
function mt.__index(t, k)
   if #k == 1 then
      return rawget(t, k:byte(1) - base)
   end
end

function make_pos(p)
   assert(#p == 2, 'Error: input not a position array')
   setmetatable(p, mt)
   return p
end

local pos = make_pos{8, 1}
print(pos.y)
print(pos.x)
