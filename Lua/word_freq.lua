-- Exercise 10.3 PiL, 3/e
--[[
Generic for:
The function is called with the parameters passed-in and it should return
atleast 3 things; a function f, an invariant state s and a value a0. Lua will
call f(s, a0); whatever is returned is assigned to the loop control variable(s).
If the first control is nil, the loop is terminated else f(s, a1), where
a1 = loop control variable, is called and the loop goes on. An iterator which
keeps its state only with the help of s and a is called a stateless iterator.

Alternatively, a function called may return only a function, with no invariant
state of value, thus only f() is called and whatever it returns a nil, the loop
is terminated. This is a stateful iterator since it remembers state within
itself.

The third kind is coroutine based. Creating a coroutine and wrapping it inside
function, which when called, will resume the embedded coroutine and return its
result to its caller.
--]]

function getwords(f)
   for line in f:lines() do
      for word in line:gmatch('%w+') do
         coroutine.yield(word)
      end
   end
end

-- iterator maker
function words(f)
   --[[
       PiL, 3/e §9.3 implements this with a capture that
       curries getwords; the iterator making function has
       two anonymous functions.

       local co = coroutine.create(function () getwords(f)  end)
       return function()
                  local status, ret = coroutine.resume(co)
                  return ret
              end

       Below're two alternatives:
   --]]

   -- no capture or currying
   --[[
       local co = coroutine.create(getwords)
       -- fn can either capture f or have it as a param, since
       -- we're avoiding captures, this is avoided as well
       local function fn(f)
                 local status, ret = coroutine.resume(co, f)
                 return ret
             end
       return fn, f
       -- if fn captured f, returning it here isn't required
   --]]

   -- no capture or currying, terse
   return coroutine.wrap(getwords), f
end

local function count_freq(f)
   local freq = { }
   for word in words(f) do
      if #word >= 4 then   -- Exercise 10.3 to trim articles
         freq[word] = (freq[word] or 0) + 1
      end
   end
   return freq
end

local function sort_words(freq)
   local words = { }
   for word in pairs(freq) do
      table.insert(words, word)
   end
   table.sort(words, function (w1, w2)
                        return (freq[w1] > freq[w2]) or (freq[w1] == freq[w2] and w1 < w2)
                     end)
   return words
end

local f = io.open(arg[1], 'r')
if not f then
   print('error: ' .. arg[1] .. ' not found!')
   return
end
local freq = count_freq(f)
local words = sort_words(freq)
local n = tonumber(arg[2]) or 1
for i, word in ipairs(words) do
   if i > n then break end
   print(freq[word], word)
end

