local function fromto(n, m)
  -- no need of return nil as it is automatically done
  local function iter(target, i)
    local step = 1
    if target < i then step = -1 end
    i = i + step
    if (step == 1 and i <= target) or
       (step == -1 and i >= target) then
         return i
    end
  end

  local step = 1
  if m < n then step = -1 end
  return iter, m, n - step
end

for i in fromto(5, -10) do
  print(i)
end

print '---------'

local function fromtostep(n, m, s)
  
  local function iter(t, i)
    if s ~= 0 then
      i = t.step + i
      if (t.step > 0 and t.target >= i) or (t.step < 0 and t.target <= i) then
        return i
      end
    end
  end

  if (s == 0) or
     (n == m) or
     (n > m and s > 0) or
     (n < m and s < 0) then
    s = 0
  else
    n = n - s
  end

  return iter, {target = m, step = s}, n
end

for i in fromtostep(5, -10, -3) do
  print(i)
end