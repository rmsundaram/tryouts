--local a = {2171.5,-3897.5,2191.5,-3897.5,2191.5,-4298.5,2171.5,-4298.5}
--local scale = 0.16
--for i = 1, #a do
--  print(a[i] * scale)
--end

function dot(a, b)
  local c = 0
  for i = 1, math.min(#a, #b) do
    c = c + (a[i] * b[i])
  end
  return c
end

function vec_op(a, b, f)
  local c = {}
  for i = 1, math.min(#a, #b) do
    c[i] = f(a[i], b[i])
  end
  return c
end

function mat_col(m, i)
  local col = {}
  for k = 1, #m do
    col[k] = m[k][i]
  end
  return col
end

function mat_mult(a, b)
  local c = {}
  for i = 1, #a do
    local row = {}
    for j = 1, #b[1] do
      row[j] = dot(a[i], mat_col(b, j))
    end
    c[i] = row
  end
  return c
end

function mat_print(a)
  for i = 1, #a do
    print(table.unpack(a[i]))
  end  
end

--[[mat_print(mat_mult({{0.16, 0, 0},
                    {0, 0.16, 0},
                    {2.56348e-005, 887.044, 0}},
                   {{6.25,0,0},
                    {0,-6.25,0},
                    {3.05176e-005,-5544,0}}))
--]]

local M = {{6.25, 0, 0},
           {0, -6.25, 0},
           {3.05176e-005, -5544, 0}}
--local points = "M 344.96,-262.88 L 345.12,-262.88 345.12,-199.84 344.96,-199.84 Z"
--local points = "M 341.92,-262.88 L 343.04,-262.88 343.04,-199.84 341.92,-199.84 Z"
local points = "M 348,-262.88 L 350.08,-262.88 350.08,-199.84 348,-199.84 Z"
local outstring = ""
for w in string.gmatch(points, "[%a-%d.,]+") do
  if string.find(w, ',') then
    local sep = string.find(w, ',')
    local x = tonumber(string.sub(w, 1, sep - 1))
    local y = tonumber(string.sub(w, sep + 1))
    local new_point = mat_mult({{x, y, 1}}, M)
    local new_x = new_point[1][1]
    new_x = new_x - new_x % 0.01
    local new_y = new_point[1][2]
    new_y = new_y - new_y % 0.01
    outstring = outstring .. tostring(new_x) .. ',' .. tostring(new_y)
  else
    outstring = outstring .. w
  end
  outstring = outstring .. ' '
end
print(outstring)

function subber(x, y) return x - y end
print(table.unpack(vec_op({2156,-3901,2157,-3901,2157,-4295,2156,-4295}, {2152.5,-3897.5,2160.5,-3897.5,2160.5,-4298.5,2152.5,-4298.5}, subber)))
