-- http://www.cs.umd.edu/class/sum2003/cmsc311/Notes/BitOp/xor.html

-- finding the unique
-- http://stackoverflow.com/a/35271/183120
local b = { 3, 2, 5, 2, 1, 5, 3 }
d = 0
for i = 1, #b do
   d = bit32.bxor(d, b[i])
end
print(d)

-- finding the duplicate
-- http://stackoverflow.com/a/7118161/183120
-- http://stackoverflow.com/a/2606160/183120
local a = { 1043, 1042, 1044, 1042 }
local d = 0
local offset = math.min(table.unpack(a))
for i = 1, #a - 1 do
   d = bit32.bxor(bit32.bxor(a[i], offset), d)
   offset = offset + 1
end
d = bit32.bxor(d, a[#a])
print(d)
--[[
Another solution is using addition:
   1043 + 1042 + 1044 + 1042 - (1042 + 1043 + 1044) = 1042
However, using XOR instead avoids problems of overflow.
]]

--[[
Both of these solutions work due to XOR's properties
• commutativity: a ⊕ b = b ⊕ a
• associativity: a ⊕ (b ⊕ c) = (a ⊕ b) ⊕ c
• 0 is identity: a ⊕ 0 = a
• inverse element: every element is its own inverse: a ⊕ a = 0
• inverse function: it's its own inverse: a ⊕ b = c, c ⊕ b = a
  this works since (a ⊕ b) ⊕ b = a ⊕ (b ⊕ b) = a ⊕ 0 = a
XOR forms triads: a ⊕ b = c, c ⊕ b = a, c ⊕ a = b, so having any
two you can get the other
http://stackoverflow.com/a/14279946/183120

In the above example
(((((3⊕2)⊕5)⊕2)⊕1)⊕5)⊕3 = ((((3⊕(2⊕5))⊕2)⊕1)⊕5)⊕3 (associative)
                         = ((((3⊕(5⊕2))⊕2)⊕1)⊕5)⊕3 (commutative)
                         = (((((3⊕5)⊕2)⊕2)⊕1)⊕5)⊕3 (associative)
                         = ((((3⊕5)⊕(2⊕2))⊕1)⊕5)⊕3 (associative)
                         = (((3⊕5)⊕1)⊕5)⊕3.
Likewise, the others get cancelled out too, leaving 1 alone.
XOR can also be implemented using binary operators ~, & and |:
a ⊕ b = (a & ~b) | (b & ~a)
]]

--[[
Another different class of problem is to find the missing number
in an unsorted sequence. If only one is missing, then this XOR
solution can be used.
http://stackoverflow.com/q/3492302/183120
]]
