function print_rest(a)
    for i = 3, #a do
      io.write(', ', a[i])
    end
    io.write "\n"
end

function swap(a, i, j)
  if i ~= j then
    a[i], a[j] = a[j], a[i]
  else
    print("Useless swap")
  end
end

--[[ in contrast to the PiL version, this "unrolls" the recursion
and also start from the back and keeps moving forward
]]
function permute(a, n)
  io.write("permute(", (n or "nil"), ")\n")
  n = n or #a
  if n == 1 then    -- required for a single element table input
    print(a[1])
  elseif n == 2 then  -- note: this isn't #a but n
    io.write(a[1] .. ', ' .. a[2])
    print_rest(a)
    io.write(a[2] .. ', ' .. a[1])
    print_rest(a)
  elseif n > 2 then -- required for empty table input
    local last_ele = n
    repeat
      swap(a, last_ele, n)
      permute(a, n - 1)
      swap(a, last_ele, n)
      last_ele = last_ele - 1
    until last_ele == 0
  end
end

-- solution in PiL book (which starts from 1)
function perm(a, n)
  io.write("perm(", (n or "nil"), ")\n")
  n = n or #a
  if n <= 1 then
    for i = 1, #a do
      io.write(a[i], " ")
    end
    io.write("\n")
  else
    for i = 1, n do
      swap(a, i, n)
      perm(a, n - 1)
      swap(a, i, n)
    end
  end
end

permute {}
print "******************"
perm {1}
print "******************"
perm {1, 2}          -- 2 useless swaps, 2 inner calls
print "******************"
permute {1, 2}       -- 0 useless swaps, 0 inner calls
print "******************"
perm {1, 7, 5}       -- 8 useless swaps, 9 inner calls
print "******************"
permute {1, 7, 5}    -- 2 useless swaps, 3 inner calls
print "******************"
perm {1, 7, 5, 0}    -- 34 useless swaps, 40 inner calls
print "******************"
permute {1, 7, 5, 0} -- 10 useless swaps, 16 inner calls
