local str = [[159.8X41.6
37.29X26.26
26.73X21.64
154X32
37.3X29.8
37.4X32.6
37.29X22.85
26.73X18.23
165.10X31.75
28.56X
18.98X
39.21X
14.03X
29.3X
39X19
47X19
47X21
32X21
31X21
36X13
36X36]]

local width_min = math.huge
local width_max = 0
local height_min = math.huge
local height_max = 0
for line in str:gmatch('[%w\\.]+') do
  local idx = line:find('X')
  local width = tonumber(line:sub(1, idx - 1))
  local height = tonumber(line:sub(idx + 1))
  width = width or width_min
  height = height or height_min
  
  if width < width_min then
    width_min = width
  elseif width > width_max then
    width_max = width
  end
  
  if height < height_min then
    height_min = height
  elseif height > height_max then
    height_max = height
  end
end

print("Minimum = " .. tostring(width_min) .. " x " .. tostring(height_min) .. " mm")
print("Maximum = " .. tostring(width_max) .. " x " .. tostring(height_max) .. " mm")

local milli_to_inch = 0.0393700787
io.write(string.format('Minimum = %f" x %f"\n',
                       width_min * milli_to_inch,
                       height_min * milli_to_inch))
io.write(string.format('Maxiumum = %f" x %f"\n',
                       width_max * milli_to_inch,
                       height_max * milli_to_inch))