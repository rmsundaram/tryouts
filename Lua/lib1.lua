-- load this either with -llib1 or dofile("lib1.lua")
--[[
Eitherways it'll pass the complete file through
the interpreter, thereby exeucting the print function as well;
if the first line is shebang it'll be ignored
]]--

--[[ trick to comment out a codeblock; adding '-' in this line's front will enable the codeblock
a = 1
--]]

--[[
passing a line with -e executes it as a chunk; passing -i makes the interpreter
enter the interactive mode after executing the script. If -e wasn't passed, before
running any argument, the filename pointed by LUA_INIT_5_2 (or LUA_INIT, if former
is unavailable) if starting with '@' is executed, or the string itself is executed
]]--

function norm(x, y)
  return (x^2 + y^2)^0.5
end

function twice(x)
  return 2 * x
end

print("lib1 loaded")
