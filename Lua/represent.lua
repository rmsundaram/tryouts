-- Exercise 2.3, Programming in Lua, 3rd Edition
-- http://github.com/xfbs/PiL3

--[[
  multiply and divide by powers of 2 to see if we get a common fraction
  (also known as simple or vulgar fraction), where the number is a ratio
  a/b: a, b ∈ ℤ and b ≠ 0
]]
function represent(number, tries)
  print("Trying to represent " .. number)
  for i = 1, tries do
    local denom = 2 ^ i
    print((number * denom) .. '/' .. denom)
  end
end

represent(12.7, 10)
represent(5.5, 5)
