function strcat(...)
  return table.concat({...})
end
print(strcat("hello", "world", ":-)"))

function printer(...)
  print(...)
end
printer(1, "hello", nil, 2, "world", nil)
--> 1 hello nil 2 world nil

function unpack_print(...)
  local args = { ... }
  print(table.unpack(args))
end
unpack_print(1, "hello", nil, 2, "world", nil)
--> 1 hello nil 2 world
-- doesn't print the last nil since unpack considers the table as a sequence thus #args evaluates to 5 not 6
-- less efficient here since a table is constructed unnecessarily; unpack is useful to unpack tables where … can't be used

function minus_one(first, ...)
 return ...
end
print(minus_one(1, 2, 3, 4, 5))
