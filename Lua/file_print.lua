-- negative indices give the arguments preceeding the script name
print("Filename " .. arg[0])

local i = 1
local a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
local v = 10
while a[i] do
  if a[i] == v then return i end  -- nothing executes beyond this in this module
  i = i + 1
end
print(i)

print('hello')
