local xml = require 'xml'
local lub = require 'lub'

local lib_prefix = 'C:/Work/Libs/'
local lib_incs = {glut = {'freeglut/include'},
                  glew = {'glew-1.11.0/include'},
                  glm  = {'glm'},
                  soil = {'soil/src'}}
local lib_links = {glut = {'freeglut/lib',    'freeglut_static'},
                   glew = {'glew-1.11.0/lib', 'glew32s'},
                   soil = {'soil/lib',        'SOIL'}}
local debug_macro = '_DEBUG'
local glut_static_macro, glew_static_macro = 'FREEGLUT_STATIC', 'GLEW_STATIC'
local template = [[
local action = _ACTION or ""

solution "%sSln"
        location ("build/" .. action)
        configurations { "debug" }
        objdir ("./obj")

        project "%s"
                kind "ConsoleApp"
                language "C++"
                files { %s }
                        links { %s }
                        targetname "%s"
                        vpaths { ["inc"] = { "**.h", "**.hxx", "**.hpp" }, ["src"] = { "**.c", "**.cxx", "**.cpp" } }
                        defines { %s }

                configuration "windows"
                        includedirs { %s }
                        libdirs { %s }
                        links { "opengl32", "gdi32", "winmm", "user32" }

                configuration "debug"
                        defines { "%s" }
                        flags { "Symbols" }
                        targetdir ( "." )
]]

function get_paths(xml_node)
   local inc_path, lib_path = 'IncludePath', 'LibraryPath'
   local found_node = lub.search(xml_node,
                                 function (node)
                                    if (node.xml == 'PropertyGroup') and
                                       (node.Condition == "'$(Configuration)|$(Platform)'=='Debug|Win32'") then
                                       for i = 1, #node do
                                          if node[i].xml == inc_path or node[i].xml == lib_path then
                                             return true
                                          end
                                       end
                                    end
                                 end
   )
   local paths = { }
   for i, v in ipairs(found_node) do
      if v.xml == lib_path or v.xml == inc_path then
         paths[v.xml] = v[1]
      end
   end
   return paths
end

function transform(paths, replacements)
   local result, aux = { }, { }
   for path in string.gmatch(paths, "[^;]+") do
      for key, value in pairs(replacements) do
         if string.find(path, key) then
            result[#result + 1] = lib_prefix .. value[1]
            if value[2] then aux[#aux + 1] = value[2]  end
         end
      end
   end
   return result, aux
end

function get_defines(vcproj)
   local parent = lub.search(vcproj,
                             function (node)
                                if (node.xml == 'ItemDefinitionGroup') and
                                   (node.Condition == "'$(Configuration)|$(Platform)'=='Debug|Win32'") then
                                   if node[1].xml == 'ClCompile' then return true end
                                end
                             end
   )
   local def_line
   for i, v in ipairs(parent[1]) do
      if v.xml == 'PreprocessorDefinitions' then
         def_line = v[1]
         break
      end
   end
   local defines = { }
   for def in string.gmatch(def_line, '[^;]+') do
      if (string.find(def, '[%$%%]') == nil) and (def ~= debug_macro) then
         defines[#defines + 1] = def
      end
   end
   return defines
end

function gather_files(vcproj, src_type)
   local tag_name = 'Cl' .. src_type
   local src_node = lub.search(vcproj,
                               function (node)
                                  if node.xml == 'ItemGroup' and node[1].xml == tag_name then
                                     return true
                                  end
                               end
   )
   if src_node then
      local files = { }
      for i, tag in ipairs(src_node) do
         if tag.xml == tag_name then
            files[#files + 1] = tag.Include
         end
      end
      return files
   end
end

-- input parsing
local vcproj = xml.loadpath(arg[1])
local paths = get_paths(vcproj)
local includedirs = transform(paths.IncludePath, lib_incs)
local libdirs, links = transform(paths.LibraryPath, lib_links)
local defines = get_defines(vcproj)
for i, v in ipairs(links) do
   if string.find(v, 'glew') then
      defines[#defines + 1] = glew_static_macro
   elseif string.find(v, 'glut') then
      defines[#defines + 1] = glut_static_macro
   end
end

local src_files = gather_files(vcproj, 'Compile')
local hdr_files = gather_files(vcproj, 'Include')

local discard_base, file = lub.dir(arg[1])
local discard_i, discard_j, proj_name = string.find(file, "(.+)%.vcxproj")

-- output generation
local str_inc = '"' .. table.concat(includedirs, '", "') .. '"'
local str_defines = '"' .. table.concat(defines, '", "') .. '"'
local str_libs = '"' .. table.concat(libdirs, '", "') .. '"'
local str_links = '"' .. table.concat(links, '", "') .. '"'
local str_file = '"' .. table.concat(src_files, '", "')
if hdr_files then
   str_file = str_file .. '", "' .. table.concat(hdr_files, '", "')
end
str_file = str_file .. '"'

-- local str_links = '"' .. links[1] .. '"'
-- local str_libs = '"' .. lib_prefix .. libdirs[1] .. '"'
-- for i = 2, #links do
--    str_links = str_links .. ', "' .. links[i] .. '"'
--    str_libs = str_libs .. ', "' .. lib_prefix  .. libdirs[i] .. '"'
-- end
-- local str_inc = '"' .. lib_prefix .. includedirs[1] .. '"'
-- for i = 2, #includedirs do
--    str_inc = str_inc .. ', "' .. lib_prefix .. includedirs[i] .. '"'
-- end
-- local str_file = '"' .. src_files[1] .. '"'
-- for i = 2, #src_files do
--    str_file = str_file .. ', "' .. src_files[i] .. '"'
-- end
-- for i = 1, #hdr_files do
--    str_file = str_file .. ', "' .. hdr_files[i] .. '"'
-- end

local premake_str = string.format(template,
                                  proj_name,
                                  proj_name,
                                  str_file,
                                  str_links,
                                  proj_name,
                                  str_defines,
                                  str_inc,
                                  str_libs,
                                  debug_macro)
io.output(proj_name .. '.lua')
io.write(premake_str)
io.close()
