-- read a file line by line (including the '\n') and pass on to the transformer
function source(transform)
  local log = io.open(arg[1], "r")
  repeat
    local line = log:read("*L")
    coroutine.resume(transform, line)
  until line == nil  
end

-- prepend the line number and pass on to the consumer
function transform(sink)
  return coroutine.create(function (line)
      local i = 1
      while line do
          line = tostring(i) .. ': ' .. line
          coroutine.resume(sink, line)
          line = coroutine.yield()
          i = i + 1
      end
    end)
end

-- write a given line to a file
function sink()
  return coroutine.create(function (line)
      local out_log = io.open(arg[2], "w")
      while line do
        out_log:write(line)
        line = coroutine.yield()
      end
    end)
end

source(transform(sink()))
