-- https://en.wikipedia.org/wiki/SRGB#Specification_of_the_transformation
function srgb_to_linear(srgb)
  local s = srgb / 255
  if s <= 0.04045 then
    return s / 12.92
  else
    return ((s + 0.055) / 1.055)^2.4
  end
end

-- used to generate a LUT to be used in C
print("u8", "u16", "f64")
for i = 0, 255 do
  print(i, math.floor((65535 * srgb_to_linear(i)) + 0.5), srgb_to_linear(i))
end
