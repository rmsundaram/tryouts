#!/usr/bin/env lua

-- Written as part of AOC 2018, Day 19 solution
-- Very unoptimised form of finding factors of n
a = 0
b = 1
n = tonumber(arg[1])
print("b", "c")
while b <= n do
  c = 1
  while c <= n do
    if (b * c) == n then
        print(b, c)
        a = a + b
    end
    c = c + 1
  end
  b = b + 1
end

print()
-- common approach
for i = 1, n do
  if n % i == 0 then
    io.write(string.format("%d ", i))
  end
end
print()
