local Account = { balance = 0 }
--[[ Since Account would be the prototype for all objects
of this class, set itself as the index metamethod here
instead of in Account:new for every object]]
Account.__index = Account

function Account:deposit(x)
  self.balance = self.balance + x
  return self.balance
end

function Account:withdraw(x)
  if x > self.balance then
    error "Insufficient Balance"
  end
  self.balance = self.balance - x
  return self.balance
end

function Account:new(o)
  local o = o or { }
  setmetatable(o, self)
  return o
end

local a1 = Account:new{ balance = 30 }
print(a1.balance)
print(a1:deposit(5))
print(a1:withdraw(33))

-- use pcall to avoid termination due to error
print(pcall(a1.withdraw, a1, 4))

-- derive from base
-- fields specific to derived appear here with defaults
local SpecialAccount = Account:new { limit = 1000 }
SpecialAccount.__index = SpecialAccount

function SpecialAccount:withdraw(x)
  if x > (self.balance + self.limit) then
    error "Insufficient Balance"
  end
  self.balance = self.balance - x
  return self.balance
end

-- omitting limit field would lead to a default limit of 1000
local a2 = SpecialAccount:new{ balance = 20, limit = 500 }
print(a2.balance)
print(a2.limit)
print(a2:deposit(30))
print(a2:withdraw(300))
print(a2:withdraw(250))
print(pcall(a2.withdraw, a2, 1))
