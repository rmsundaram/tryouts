function producer(consumer)
  local log = io.open(arg[1], "r")

  repeat
    local line = log:read("*L")
    coroutine.resume(consumer, line)
  until line == nil
  
end

function consumer()
  return coroutine.create(function (line)
    local out_log = io.open(arg[2], "w")
    while line do
      out_log:write(line)
      line = coroutine.yield()
    end
  end)
end

producer(consumer())  -- producer-driven design
