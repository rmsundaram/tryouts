-- converts string to its hexadecimal equivalent
-- so that it can be used to convert ascii/utf-8 to binary
function to_hex(num)
   if ((num % 1 ~= 0) or (num < 0)) then
      error("Non-negative integer expected!")
   end
   return string.format('%08x', num)
end

-- reverses a string nibble by nibble
-- useful in converting endianness
function reverse_nibble(hexstring)
   local len = #hexstring
   if (len % 2 ~= 0) then error "Not a valid hex string" end
   local res = {}
   for i = 2, len, 2 do
      res[#res + 1] = hexstring:sub(-i, 1 - i)
   end
   return table.concat(res)
end

--[[
prints bytes constituting an unsigned int (since bit32 operates on uint)
so that they can be written to a binary file; but io.write, even though it also
takes a number, always converts in into a string (which represents the number in
ASCII) and writes it to a file. Without modules writing numbers as-is to a file
isn't that straight-forward in Lua. Options are string.char (may not be portable)
http://stackoverflow.com/a/17466416/183120 or to use the lpack or struct module
http://www.inf.puc-rio.br/~roberto/struct/.
]]
function print_bytes(num)
   for i = 31, 0, -8 do
      local byte = bit32.extract(num, i - 7, 8)
      print(byte, to_hex(byte))
   end
end

local num = tonumber(arg[1])
print_bytes(num)
local hex_num = to_hex(num)
print('Big endian: ' .. hex_num)
print('Little endian: ' .. reverse_nibble(hex_num))
