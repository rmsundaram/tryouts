-- require 'fn', if fn is so far not loaded, would do pull the entire contents
-- inside an anonymous function
(function()

-- begin contents of fn.lua
local function localFn()
  print 'Local function'
end

function globalFn()
  print 'Global function'
end
--end contents

end)()
--[[ The anonymous function is executed and its return value is cached in
package.loaded; if nil, require assumes that it returned true. Whatever was
declared local would be local to this anonymous function and thus inaccessible
outside, while the global ones are accessible. Usually a (local/global) table
is returned by the chunk in the file, which becomes the return of the anonymous
function. If global, returning is optional (as it can be accessed with the name
the file gave it), while if it's local, it needs to be returned for the
requirer to use it. If returned, the table may be assigned to some convenient
local name for later access at the requiere site. Refer Orange PiL, page 153 ]]

function printFn(f)
  if f then f() else print 'nil' end
end

printFn(globalFn)
printFn(localFn)
