#!/usr/bin/env lua

--[[
  Program to demonstrate coroutines elegantly representing state machines.

  Lua version of program in Eli Bendersky's
  Co-routines as an alternative to state machines.
  https://eli.thegreenplace.net/2009/08/29/co-routines-as-an-alternative-to-state-machines
]]

-- Takes a function and a coroutine as arguments
function unwrap_protocol(after_dle_fn, target)
  header = 0x61
  footer = 0x62
  dle = 0xAB
  while true do
    byte = coroutine.yield()
    bytes = {}
    if byte == header then
      while true do
        byte = coroutine.yield()
        if byte == footer then
          coroutine.resume(target, bytes)
          break
        elseif byte == dle then
          byte = coroutine.yield()
          table.insert(bytes, after_dle_fn(byte))
        else
          table.insert(bytes, byte)
        end
      end
    end
  end
end

function frame_recevier()
  while true do
    frame = coroutine.yield()
    io.write("Got frame: ")
    for _, v in ipairs(frame) do
      io.write(string.format("0x%x ", v))
    end
    io.write('\n')
  end
end

function identity(x)
  return x
end

rcv = coroutine.create(frame_recevier)
-- coroutine.resume always returns status (boolean) followed by anything passed
-- back by calle’s yield.
status, _ = coroutine.resume(rcv)
assert(status)

unwrapper = coroutine.create(unwrap_protocol)
-- first resume starts the function, passing args to its body
status, _ = coroutine.resume(unwrapper, identity, rcv)
assert(status)

--[[
  The convenience function coroutine.wrap(), instead of coroutine.create, would
  return a function instead of a coroutine (i.e. type isn’t ‘thread’); this
  function when called would resume the coroutine; also this function’s return
  values don’t contain the leading status; instead it raises an error if status
  isn’t true.  Refer §9.3 Coroutines as Iterators, PiL, 3/e
]]

bytes = { 0x70, 0x24, 0x61, 0x99, 0xAF, 0xD1, 0x62, 0x56, 0x62, 0x61, 0xAB,
          0xAB, 0x14, 0x62, 0x07 }

for _, v in ipairs(bytes) do
  -- Subsequent resumes start from last yielded point; args to resume come out
  -- of the callee’s yield call.  Note that this means different number/set of
  -- arguments can be recieved at different yield points of calle.
  status = coroutine.resume(unwrapper, v)
  assert(status)
end
