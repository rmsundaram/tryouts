#! /usr/bin/env lua

-- Sieve of Eratosthenes
-- Variations
--   Sieve of Sundaram
--   Sieve of Atkins

-- cross out multiples of x within l
local function strike_multiples(l, i)
  if i > #l then
    return
  end

  local n = l[i].num
  i = i + n
  while (i <= #l) do
    l[i].struck = true
    i = i + n
  end
end

local function print_list(l)
  local printed
  for _, v in ipairs(l) do
    if not v.struck then
      io.write(v.num .. ', ')
      printed = true
    end
  end
  if printed then print() end
end

local function main()
  if #arg < 1 then
    print("Prime number generation using Sieve of Eratosthenes")
    print("Usage: sieve LIMIT")
    return
  end

  local lim = tonumber(arg[1])
  if lim <= 1 then
    print("Limit should be above 2 to generate primes")
    return -1
  end

  local n = { }
  for i = 2, lim do
    table.insert(n, {num = i, struck = false})
  end

  for i = 1, #n do
    -- no striking multiples if already stricked-off
    if not n[i].struck then
      -- io.write(n[i].num .. ': ')
      strike_multiples(n, i)
      -- print_list(n)
    end
  end
  print_list(n)
end

main()
