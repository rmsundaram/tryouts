function fact(n)
  if n == 0 then
    return 1
  else
    return n * fact(n - 1)
  end
end

print("Enter number: ")
local n = io.read("*n")  -- avoid using _CAPS, _Caps or _CApS as they're reserved
if n < 0 then
  print("Invalid argument")
else
  print("Factorial = " .. fact(n))
end
