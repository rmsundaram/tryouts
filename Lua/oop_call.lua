--[[
  a way to avoid this local function is to define a function inside
  make_circle and assign it to a table field; local functions are
  visible only within the module
]]
local function circumference(circle)
  return 2 * math.pi * circle.radius
end

function make_circle(radius)
  -- although { radius = radius } is valid and works, this is explicit and more readable
  return {["radius"] = radius, ["circumference"] = circumference}
end

local c = make_circle(7)
print(c:circumference())  -- Lua's syntactic sugar to call methods
