#! /usr/bin/env lua

function smoothstep(t)
  return t * t * (3 - 2 * t)
end

local distance = 130
print("lerp\tsmoothstep\tdistance(lerp)\tdistance(smoothstep)")
for i = 0, 10 do
  local delta = i / 10
  print(delta .. "\t\t" .. smoothstep(delta) .. "\t\t" .. (distance * delta) ..
        "\t\t" .. (distance * smoothstep(delta)))
end
