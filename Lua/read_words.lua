local n = arg[1]
local widths = { 4, 2, 1 }
local r = { }

for i = 1, #widths do
   local new_n = n % widths[i]
   table.insert(r, (n - new_n) / widths[i])
   n = new_n
end

for i = #r, 1, -1 do
   if r[i] ~= 0 then
      local s = string.format('%d-byte reads = %d', widths[i], r[i])
      print(s)
   end
end
