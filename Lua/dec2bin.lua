function fract(num)
  return num % 1
end

-- 384.951257
io.write("Enter decimal number: ")
local num = io.read("*n")
local i = num - fract(num)

-- handle integer part
local ibin = ""
repeat
  local rem = i % 2
  ibin = string.format('%i', rem) .. ibin
  i = i / 2
  i = i - fract(i)
until i == 0
io.write(ibin .. '\n')

-- process the fractional part
local significand_bits = 24     -- significand bits in binary32
-- if integer part eats away all significand bits, fallback to 6 bits
local frac_digits = math.max(significand_bits - #ibin, 6)
local f = fract(num)
local fbin = ""
for i = 1, frac_digits do
 local new_f = f * 2
 --[[  enable this to truncate digits after the 6th place
 new_f = new_f - new_f % 0.000001
 --]]
 fbin = fbin .. string.format('%i', (new_f - fract(new_f)))
 print(i .. ": " .. new_f)
 f = fract(new_f)
end
io.write(ibin .. '.' .. fbin .. ' (' .. tostring(#ibin + #fbin) .. ' bits)\n')
