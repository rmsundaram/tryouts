function perm(a, n)
  n = n or #a
  if n == 1 then    -- required for a single element table input
    coroutine.yield(a)
  elseif n == 2 then  -- note: this isn't #a but n
    coroutine.yield {a[1], a[2], table.unpack(a, 3)}
    coroutine.yield {a[2], a[1], table.unpack(a, 3)}
  elseif n > 2 then -- required for empty table input
    local last_ele = n
    repeat
      a[last_ele], a[n] = a[n], a[last_ele]
      perm(a, n - 1)
      a[last_ele], a[n] = a[n], a[last_ele]
      last_ele = last_ele - 1
    until last_ele == 0
  end
end

function permute(a)
--[[ one can directly pass perm to coroutine.create() as an argument;
however, it means to define such a long function inline, locally.
Thus to pass an already defined function, we define an anonymous
wrapper function which in turn calls the original function
]]

--  local co = coroutine.create(function() perm(a)¹ end)
--  return function()
--    local code, res = coroutine.resume(co)²
--    return res
--  end

-- instead of a closure¹, one may pass a to the anonymous
-- function which in turn passes it to perm; doing so, would require
-- coroutine.resume² to take 2 args - function and its arg (the list)

-- this is so common in Lua that a standard library
-- function is provided for this
  return coroutine.wrap(function() perm(a) end)
end

for a in permute {'x', 'y', 'z'} do
  io.write(table.unpack(a))
  io.write("\n")
end
