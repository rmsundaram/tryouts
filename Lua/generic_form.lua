function numbers(a)
  local function iter(s, i)
    i = i + 1
    if i <= #a then
      local val = a[i]
      return i, val
    else
      return nil
    end
  end
  return iter, nil, 0
end

local a = {10, 20, 30, 40, 50}
for i, n in numbers(a) do
  print(n)
end
