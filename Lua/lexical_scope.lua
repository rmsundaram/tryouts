function f1()
  local v1 = 'Hello'
  return function()
            local v2 = 'world'
            return function()
                      print(v1 .. ', ' .. v2 .. '!')
                   end
         end
end

f2 = f1()
f3 = f2()
f3()
