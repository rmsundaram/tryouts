// https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=049ae1bd0fe2e2dd4cacccf57c0571c4

struct Student {
  id: u32,
}

impl Student {
  // constructor
  fn new(id: u32) -> Self {
    Student { id: id }
  }

  fn report(&self) {
    println!("Report {}", self.id);
  }
}

impl Default for Student {
  // default constructor
  fn default() -> Self {
    Student { id: 0 }
  }
}

impl Clone for Student {
  // copier
  fn clone(&self) -> Self {
    Student { id: self.id }
  }
}

impl Drop for Student {
  // destructor
  fn drop(&mut self) {
    println!("Dropped {}", self.id);
  }
}

fn main() {
  let s0 = Student::default();
  let s1 = Student::new(1);
  let s2 = s0.clone();
  s2.report();
  let s3 = s1;
  s3.report();
}
