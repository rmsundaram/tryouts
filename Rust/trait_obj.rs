// https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=e2cfcb15965bf8c754520a2b011dde1b

#[derive(Debug, Copy, Clone)]
struct Point(f32, f32);

struct Circle {
  centre: Point,
  radius: f32,
}

impl Circle {
  fn new(centre: Point, radius: f32) -> Self {
    debug_assert!(radius > 0.0);
    Circle {
      centre: centre,
      radius: radius,
    }
  }

  fn bloat(&mut self, delta: f32) -> f32 {
    self.radius += delta;
    self.radius
  }
}

trait Drawable {
  fn draw(&self);
}

impl Drawable for Circle {
  fn draw(&self) {
    println!("Circle at {:?} sized {}", self.centre, self.radius);
  }
}

// https://doc.rust-lang.org/stable/book/ch10-02-traits.html#traits-as-parameters
// static dispatch; short hand form: fn draw_stuff(obj: &impl Drawable)
fn draw_stuff<T: Drawable>(obj: &T) {
  obj.draw();
}

// dynamic dispatch using trait object
fn draw_unknown_stuff(obj: &dyn Drawable) {
  obj.draw();
}

fn main() {
  let mut c1 = Circle::new(Point(3.0, -1.2), 3.0);
  draw_stuff(&c1);
  c1.bloat(3.0);
  c1.draw();
  c1.bloat(3.0);
  draw_unknown_stuff(&c1);
}
