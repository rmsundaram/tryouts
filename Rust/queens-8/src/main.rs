use matrix_display::*;
use std::fmt;

#[derive(Copy, Clone, Default)]
struct Chessboard {
  board: u64,
}

impl Chessboard {
  fn get_queen_at_row(&self, idx: i8) -> Option<i8> {
    debug_assert!((0..8).contains(&idx));
    let board = self.board >> (8 * idx);
    let mask = 0xFF_u64;
    let pos = board & mask;
    match pos.trailing_zeros() as i8 {
      x @ 0..=7 => Some(x),
      _ => None,
    }
  }

  // works on 0-indexing
  fn place_queen(&mut self, row: i8, col: i8) {
    debug_assert!(row <= 7 && col <= 7);
    self.board |= 1_u64 << ((8 * row) + col);
  }

  fn remove_queen(&mut self, row: i8, col: i8) {
    self.board ^= 1_u64 << ((8 * row) + col);
  }

  fn is_queen_threatened(&self, row: i8) -> bool {
    debug_assert!((0..8).contains(&row));
    let col = self
      .get_queen_at_row(row)
      .unwrap_or_else(|| panic!("No queen in row {}", row + 1));
    (0..row).any(|prev_row| {
      let prev_col = self
        .get_queen_at_row(prev_row)
        .expect("Previous row unfilled!");
      // no need to check row equality as we never try on the same row
      prev_col == col || ((row - prev_row).abs() == (col - prev_col).abs())
    })
  }
}

impl fmt::Display for Chessboard {
  fn fmt(&self, _f: &mut fmt::Formatter<'_>) -> fmt::Result {
    let format = Format::new(7, 3);
    let board = (0..64)
      .map(|i| {
        let ch = match self.board & (1_u64 << i) {
          0 => ' ',
          _ => '♛',
        };
        let ansi_fg = 33;
        let ansi_bg = match i % 2 + (i / 8) % 2 == 1 {
          true => 7,
          false => 0,
        };
        cell::Cell::new(ch, ansi_fg, ansi_bg)
      })
      .collect();
    let mut data = matrix::Matrix::new(8, board);
    let display = MatrixDisplay::new(&format, &mut data);
    display.print(&mut std::io::stdout(), &style::BordersStyle::None);
    Ok(())
  }
}

fn place_queen(board: &mut Chessboard, row: i8) -> bool {
  if let Some(col) = board.get_queen_at_row(row) {
    board.remove_queen(row, col);
    board.place_queen(row, col + 1);
  } else {
    board.place_queen(row, 0);
  }
  !board.is_queen_threatened(row)
}

fn place_queens(board: &mut Chessboard, from_row: i8) -> bool {
  for _ in 0..8 {
    if place_queen(board, from_row) {
      // for every row, if place_queens(row + 1) succeeds we’re done
      // for the last (7th) row, if we’ve successfully placed we’re done
      if from_row == 7 || place_queens(board, from_row + 1) {
        return true;
      }
    }
  }
  // tried placing in all 8 columns in vain; report failure
  // clear the last trial, to start afresh when revisiting by backtracking
  board.remove_queen(from_row, 7);
  false
}

fn main() {
  let mut b = Chessboard::default();
  b.place_queen(0 /*row*/, 0 /*col*/); // other columns work too
  place_queens(&mut b, 1 /*from_row*/);
  println!("{}", b);
}
