// https://play.rust-lang.org/?version=stable&mode=debug&edition=2018&gist=779bfd82f20caf0e588901f3a5051a6d

fn main() {
  let yes = "y̆es";
  // print each UTF-8 char's index
  yes.char_indices().inspect(|i| println!("{} --> {}", i.0, i.1)).map(|i| i.0).sum::<usize>();
  println!("");

  // https://stackoverflow.com/q/58611262/183120 compares approach 1 and 2
  // approach 1
  let mut rev1 = String::with_capacity(yes.len());
  for c in yes.chars().rev() {
      rev1.push(c);
  }
  println!("{}", rev1);

  // approach 2
  let rev2 = yes.chars().rev().collect::<String>();
  println!("{}", rev2);
}
