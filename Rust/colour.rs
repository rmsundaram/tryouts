// Exercise on std::fmt at
// https://doc.rust-lang.org/stable/rust-by-example/hello/print/fmt.html
use std::fmt::{self, Display, Formatter};

#[derive(Debug)]
struct Colour {
    red: u8,
    green: u8,
    blue: u8,
}

impl Display for Colour {
    fn fmt(&self, f: &mut Formatter) -> fmt::Result {
        write!(
            f,
            "RGB ({}, {}, {}) 0x{0:02X}{1:02X}{2:02X}",
            self.red, self.green, self.blue
        )
    }
}

fn main() {
    for colour in [
        Colour {
            red: 128,
            green: 255,
            blue: 90,
        },
        Colour {
            red: 0,
            green: 3,
            blue: 254,
        },
        Colour {
            red: 0,
            green: 0,
            blue: 0,
        },
    ]
    .iter()
    {
        println!("{}", *colour);
    }
}
