/// Returns the most frequent element in a vector if there’s one.
fn most_freq<T: Ord + Copy>(v: &mut [T]) -> Option<(T, u16)> {
  if !v.is_empty() {
    v.sort_unstable();
    let mut max_element = *v.first().unwrap();
    let mut max_count = 1u16;
    let mut current_count = 1u16;
    let mut is_tie = false;
    for prev_cur in v.windows(2) {
      if prev_cur[1] == prev_cur[0] {
        current_count += 1;
      } else {
        if max_count < current_count {
          max_count = current_count;
          max_element = prev_cur[0];
          is_tie = false;
        } else if max_count == current_count {
          is_tie = true;
        }
        current_count = 1;
      }
    }
    if max_count < current_count {
      max_count = current_count;
      max_element = *v.last().unwrap();
      is_tie = false;
    } else if max_count == current_count {
      is_tie = true;
    }
    if !is_tie {
      return Some((max_element, max_count));
    }
  }
  None
}

fn test(v: &mut [u8]) {
  if let Some(m) = most_freq(v) {
    println!("Most frequent element: {}, occured {} times", m.0, m.1);
  }
}

fn main() {
  let mut v1 = vec![1, 4, 5, 8, 9]; // no most freqent (tie)
  // with most frequent and a tie
  let mut v2 = vec![1, 4, 5, 8, 14, 14, 17, 17, 18, 29, 15, 17];
  let mut v3 = vec![1, 4, 5, 8, 14, 14, 17, 17, 18, 29, 15];
  // last on most frequent
  let mut v4 = vec![8, 14, 14, 17, 17, 18, 29, 15, 101, 101, 101];
  test(&mut v1);
  test(&mut v2);
  test(&mut v3);
  test(&mut v4);
}

