# rudoku: Rust Sudoku Solver

Simple, brute-force, iterative sudoku solver written in Rust.  Reads input from `STDIN`.  If just one puzzle was fed it prints the solved board (in colour :)); otherwise it prints the total and average time taken to solve the input set.

## Algorithm

1. Read input
    + Fill given cells with values
2. While there’re vacant cells
    + Pick cell with least possibilities
    + If possibilities == 0, error out as _unsolvable board_
    + Remove digit from cells’s possibilities and fill it
    + If constraints are violated (digit occurs more than once among peers)
        - Reset this cell’s possibilities
        - Backtrack by setting previously filled cell as unfilled
3. Print board

[Eight queens puzzle][]’s [solution][queens-8] is a simpler candidate to understand backtracking algorithms.

[Eight queens puzzle]: https://en.wikipedia.org/wiki/Eight_queens_puzzle
[queens-8]: ../queens-8/src/main.rs

## Optimisations

1. **Calculating a cell’s possibilities**: instead of going over a cell’s every peer of every unit, we precalculate and maintain possibilities of every row, column and block on the board and reuse when needed.
2. **Picking least filled cell**: usually in brute-force solving, every vacant cell is accessed in order (like in Eight Queens solver), so that backtracking is also merely backtracking in reverse order.  However, this optimisation is a low-hanging fruit that could be implemented without too much code churn.

## Performance

Since this is a brute-force solver the performance isn’t great.  It solves 17-clue puzzles in ~15.24 ms.  For a fast solver with constraint propagation and exclusive pruning, see [rudoku2](../rudoku2/README.md).

# Original (Crude) Pseudocode

```
Find first unfilled cell index and store in first_unfilled_cell
For i = 1 to 9
  For j = 1 to 9
    if m[i*9 + j] is originally unfilled
      for k = m[i*9 + j] + 1 to 9
        break if set(m[i*9 + j], k)
      m[i*9 + j] is unchanged? hit a road block
      if i*9 + j == first_unfilled_cell
        error("invalid input")
      else
        backtrack: decrement i and j to reach next originally-unfilled-element
```

`set()` checks for repetition of a digit peers (row, column and sub-matrix).  If no repetition, sets and returns `true`, otherwise returns `false`.
