use colored::*;
use std::fmt::Display;
use std::fmt::Error;
use std::io::{self, BufRead};
use std::str::FromStr;
use std::time::Instant;

// a 9 × 9 sudoku solver

#[derive(Copy, Clone, Debug)]
struct Cell {
  data: u16,
}

impl Cell {
  const CANDIDATE_MASK: u16 = 0b1111_1111_1000_0000;
  const GIVEN_MASK: u16 = 0b0000_0000_0001_0000;
  const VALUE_MASK: u16 = 0b0000_0000_0000_1111;

  fn new(value: u16) -> Self {
    Cell { data: value }
  }

  fn new_given(value: u16) -> Self {
    Cell {
      data: value | Cell::GIVEN_MASK,
    }
  }

  fn is_given(&self) -> bool {
    (self.data & Cell::GIVEN_MASK) != 0
  }

  fn is_vacant(&self) -> bool {
    self.data == 0
  }

  fn value(&self) -> u8 {
    (self.data & Cell::VALUE_MASK) as u8
  }

  fn candidates(&self) -> u16 {
    self.data & Cell::CANDIDATE_MASK
  }

  // clears value retaining candidates
  fn clear_value(&mut self) {
    self.data &= !Cell::VALUE_MASK;
  }

  fn pop_candidate(&mut self) -> bool {
    if self.is_given() || self.is_vacant() {
      false
    } else {
      let mut candidates = self.data & Cell::CANDIDATE_MASK;
      if candidates != 0 {
        let bsf = candidates.trailing_zeros(); // bit scan forward
        candidates ^= 1_u16 << bsf; // clear poped candidate bit
        self.data = candidates | (bsf - 6) as u16;
        // -7 to skip trailing zeros, but -6, as value starts at 1, not 0
        true
      } else {
        false
      }
    }
  }
}

impl Display for Cell {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    write!(
      f,
      "{}",
      match self.is_given() {
        true => self.value().to_string().red(),
        false => {
          match self.is_vacant() {
            false => self.value().to_string().green(),
            true => "x".normal(),
          }
        }
      }
    )
  }
}

struct Grid {
  data: [Cell; 81],
  row_candidates: [u16; 9],
  col_candidates: [u16; 9],
  mat_candidates: [u16; 9],
  fill_idx: u8,
  fill_order: [u8; 64], // sudoku needs at least 17 clues
}

impl Grid {
  fn get_row(&self, row: usize) -> &[Cell] {
    debug_assert!(row < 9);
    let idx = 9 * row;
    &self.data[idx..(idx + 9)]
  }

  fn get_col(&self, col: usize) -> [Cell; 9] {
    debug_assert!(col < 9);
    let mut out = [Cell::new(0); 9];
    for i in 0..9 {
      out[i] = self.data[idx(i, col)];
    }
    out
  }

  fn get_sub(&self, idx: usize) -> [Cell; 9] {
    debug_assert!(idx < 9);
    const START_IDX: [usize; 9] = [0, 3, 6, 27, 30, 33, 54, 57, 60];
    let mut sub = [Cell::new(0); 9];
    let mut idx = START_IDX[idx];
    for i in 0..3 {
      sub[i * 3 + 0] = self.data[idx + 0];
      sub[i * 3 + 1] = self.data[idx + 1];
      sub[i * 3 + 2] = self.data[idx + 2];
      idx += 9;
    }
    sub
  }

  fn get_candidates(&self, idx: usize) -> u16 {
    match self.data[idx].candidates() {
      0 => {
        let (row, col) = row_col(idx);
        let sub_matrix_idx = (row / 3) * 3 + (col / 3);
        self.row_candidates[row]
          & self.col_candidates[col]
          & self.mat_candidates[sub_matrix_idx]
      }
      _ => self.data[idx].candidates(),
    }
  }

  fn set_candidates(&mut self, idx: usize) {
    let candidates = self.get_candidates(idx);
    self.data[idx] = Cell::new(candidates);
  }

  fn toggle_candidate_digit(&mut self, digit: u8, row: usize, col: usize) {
    debug_assert!(digit > 0 && digit < 10);
    let mask = 1_u16 << (6 + digit);
    self.row_candidates[row] ^= mask;
    self.col_candidates[col] ^= mask;
    let sub_matrix_idx = (row / 3) * 3 + (col / 3);
    self.mat_candidates[sub_matrix_idx] ^= mask;
  }

  fn fill(&mut self, idx: usize) -> bool {
    let mut cell = self.data[idx];
    if cell.pop_candidate() {
      let (row, col) = row_col(idx);
      self.toggle_candidate_digit(cell.value(), row, col);
      self.data[idx] = cell;
      self.fill_order[self.fill_idx as usize] = idx as u8;
      self.fill_idx += 1;
      return true;
    }
    false
  }

  fn erase(&mut self, idx: usize) {
    self.data[idx] = Cell::new(0);
  }

  fn next_unfilled_idx(&mut self) -> Option<usize> {
    let (mut min_opts, mut min_idx) = (9, 81);
    match self
      .data
      .iter()
      .enumerate()
      .filter(|&(_idx, &v)| v.value() == 0)
      .find(|&(idx, _v)| {
        let candidates = self.get_candidates(idx).count_ones();
        if min_opts > candidates {
          min_opts = candidates;
          min_idx = idx;
        }
        candidates <= 1
      }) {
      Some(c) => Some(c.0),
      None => match min_idx {
        81 => None,
        _ => Some(min_idx),
      },
    }
  }

  fn backtrack(&mut self) -> bool {
    let mut searching = true;
    while (self.fill_idx >= 1) && searching {
      self.fill_idx -= 1;
      let prev = self.fill_order[self.fill_idx as usize] as usize;
      debug_assert_ne!(self.data[prev as usize].value(), 0);
      let (p_row, p_col) = row_col(prev);
      // next candidate to try for prev cell will undisturbed be within itself,
      // free its value for use by other peers
      self.toggle_candidate_digit(self.data[prev].value(), p_row, p_col);
      self.data[prev].clear_value();
      // continue searching if previous cell has no more candidates
      searching = self.data[prev].candidates() == 0;
    }
    !searching
  }

  fn is_subset_solved(subset: &[Cell]) -> bool {
    subset
      .iter()
      .fold(0_u16, |bits, c| bits ^ 1_u16 << (6 + c.value()))
      == Cell::CANDIDATE_MASK
  }

  fn is_solved(&self) -> bool {
    (0..9).all(|i| Grid::is_subset_solved(self.get_row(i)))
      && (0..9).all(|i| Grid::is_subset_solved(&self.get_col(i)))
      && (0..9).all(|i| Grid::is_subset_solved(&self.get_sub(i)))
  }
}

impl Display for Grid {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    for (i, row) in self.data.chunks(3).enumerate() {
      write!(f, "{} {} {}", row[0], row[1], row[2])?;

      // draw table using delimiters
      let del = match i + 1 {
        3 | 6 | 9 | 12 | 15 | 18 | 21 | 24 | 27 => "\n".normal(),
        _ => " ┃ ".normal(),
      };
      write!(f, "{}", del)?;
      if (i + 1) == 9 || (i + 1) == 18 {
        writeln!(f, "{}", "━━━━━━╋━━━━━━━╋━━━━━━".normal())?;
      }
    }
    Ok(())
  }
}

impl FromStr for Grid {
  type Err = Error;
  fn from_str(input: &str) -> Result<Grid, Self::Err> {
    if input.len() != 81 {
      panic!("Input length invalid; 81 characters expected.");
    }
    let mut g = Grid {
      data: [Cell::new(0); 81],
      row_candidates: [Cell::CANDIDATE_MASK; 9],
      col_candidates: [Cell::CANDIDATE_MASK; 9],
      mat_candidates: [Cell::CANDIDATE_MASK; 9],
      fill_idx: 0,
      fill_order: [0_u8; 64],
    };
    for (i, ch) in input.char_indices().filter(|&(_i, c)| c.is_ascii_digit()) {
      g.data[i] = Cell::new_given(ch as u16 - b'0' as u16);
    }

    let mut vacancies = [0_u16; 27];
    for (i, sub) in g.data.chunks(3).enumerate() {
      vacancies[i] = (!sub
        .iter()
        .fold(0_u16, |bits, c| bits | 1_u16 << (6 + c.value())))
        & Cell::CANDIDATE_MASK;
    }
    for (i, sub) in vacancies.chunks(3).enumerate() {
      g.row_candidates[i] = sub.iter().fold(0xFFFF_u16, |bits, c| bits & c);
    }
    for i in 0..9 {
      g.col_candidates[i] = (!g.data[i..]
        .iter()
        .step_by(9)
        .fold(0_u16, |bits, v| bits | 1_u16 << (6 + v.value())))
        & Cell::CANDIDATE_MASK;
    }
    const START_IDX: [usize; 9] = [0, 1, 2, 9, 10, 11, 18, 19, 20];
    for i in 0..9 {
      g.mat_candidates[i] = vacancies[START_IDX[i]..]
        .iter()
        .step_by(3)
        .take(3)
        .fold(0xFF80_u16, |bits, c| bits & c);
    }

    Ok(g)
  }
}

fn row_col(idx: usize) -> (usize, usize) {
  (idx / 9, idx % 9)
}

fn idx(row: usize, col: usize) -> usize {
  row * 9 + col
}

fn solve(s: &str) -> Result<Grid, Error> {
  let mut g: Grid = s.parse()?;

  while let Some(idx) = g.next_unfilled_idx() {
    // try filling if cell originally vacant
    let cell = g.data[idx];
    if cell.candidates() == 0 {
      g.set_candidates(idx);
    }
    if !g.fill(idx) {
      g.erase(idx);
      if !g.backtrack() {
        // Exit Condition - Failure: we’ve backtracked all the way till the
        // first unfilled cell and exhausted all its possibilities, nothing
        // works. Indicates invalid puzzle input.
        return Err(Error);
      }
    }
  }

  Ok(g)
}

fn main() -> Result<(), Error> {
  let puzzles: Vec<String> =
    io::stdin().lock().lines().map(|l| l.unwrap()).collect();
  let mut solns = Vec::<Grid>::with_capacity(puzzles.len());

  let before = Instant::now();
  for p in &puzzles {
    solns.push(solve(&p)?);
  }
  if !puzzles.is_empty() {
    // https://stackoverflow.com/a/57341631/183120
    let duration = before.elapsed();

    // print board if just one puzzle was input
    if puzzles.len() == 1 {
      println!("{}", solns[0]);
    }

    println!(
      "Solved {} puzzle(s) in {:.3?}; average {:.3?}/puzzle",
      puzzles.len(),
      duration,
      duration / puzzles.len() as u32
    );
  }

  for (i, g) in solns.iter().enumerate() {
    if !g.is_solved() {
      println!("Incorrect solution for puzzle {}: {}\n{}", i, puzzles[i], g);
      break;
    }
  }

  Ok(())
}
