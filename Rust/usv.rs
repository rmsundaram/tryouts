fn encode_u16(cp: u32) -> (u16, Option<u16>) {
  // x | x < 0x10000 i.e. x ≤ 0xFFFF belong to BMP.
  if cp < 0xFFFF {
    (cp as u16, None)
  } else {
    let cp_ = cp - 0x10000;
    assert!(cp_.leading_zeros() >= 12);
    // We’ve 20-bit number; sift out the higher and lower 10 bits.
    let hs = (0xD800 | (cp_ >> 10)) as u16;
    let ls = (0xDC00 | (0b1111111111 & cp_)) as u16;
    (ls, Some(hs))
  }
}

fn decode_u16(surrogate_pair: (u16, Option<u16>)) -> u32 {
  match surrogate_pair {
    (ls, None) => ls as u32,
    (ls, Some(hs)) => {
      let h = (hs as u32 & 0b1111111111) << 10;
      let l = ls as u32 & 0b1111111111;
      (h | l) + 0x10000
    }
  }
}

fn main() {
  // Unicode (32-bit) code points are laid out in planes (2¹⁶ = 65536 values);
  // 17 planes so far.  Plane 0, Basic Multilingual Plane [1], in [0, U+FFFF]
  // has code points for almost all modern languages.  Early Unicode version
  // defined this as UCS-2 character set, with the UTF-16 extension to address
  // code points beyond BMP with 2 code units†.  Two code units (2 * 16 bits)
  // in pairs, called surrogate pairs, represent code points beyond BMP.  To
  // accomodate UTF-16, 2048 code points in BMP, [U+D800, U+DBFF] and [U+DC00,
  // U+DFFF], are marked as high and low surrogates; these individually have
  // no meaning and don’t represent text.  The 32-bit Go type, rune includes
  // surrogates while Rust’s char doesn’t [3]; it include only Unicode scalar
  // values [4].
  //
  // REFERENCES
  // [1]: https://en.wikipedia.org/wiki/Plane_(Unicode)#Basic_Multilingual_Plane
  // [2]: https://codepoints.net/basic_multilingual_plane
  // [3]: https://christianfscott.com/rust-chars-vs-go-runes/
  // [4]: https://unicode.org/glossary/#unicode_scalar_value
  // [5]: https://en.wikipedia.org/wiki/UTF-16#Code_points_from_U+010000_to_U+10FFFF
  // [6]: https://unicode.org/glossary/#character
  //
  // †: An encoding’s basic unit: a byte for UTF-8, 2 bytes for UTF-16, …

  // A code point within BMP (TAMIL LETTER A).
  let a = core::char::from_u32(0x0B85);
  println!("{:?}", a);

  // A code point outside BMP.
  let bell: u32 = 0x1F514;
  println!("{:?}", core::char::from_u32(bell));

  let pair = encode_u16(bell);
  println!("UTF-16: (0x{:X}, 0x{:X})", pair.1.unwrap(), pair.0);

  let bell_ = decode_u16(pair);
  assert_eq!(bell, bell_);

  println!(
    "{:?}, {:?}",
    core::char::from_u32(pair.0.into()),
    core::char::from_u32(pair.1.unwrap().into())
  );

  // Python’s str.encode('utf-16') prints UTF-16LE with BOM (0xFEFF) in
  // little-endian as 0xff, 0xfe preceeding the actual bytes:
  //
  //   >>> list(map(hex, '\N{TAMIL LETTER A}'.encode('utf-16')))
  //   ['0xff', '0xfe', '0x85', '0xb']
  //   >>> list(map(hex, '\N{BELL}'.encode('utf-16')))
  //   ['0xff', '0xfe', '0x3d', '0xd8', '0x14', '0xdd']
  //
  // Converting LE bytes into u16 we get: 0xD83D, 0xDD14.  Notice that a BMP
  // code point has one u16 while a non-BMP has two.  Encode to UTF-8, notice
  // that the BMP code point spans 3 bytes, while non-BMP takes up 4 bytes.
  //
  // We use hex() as bytes.__str__ doesn’t show all values in hex; ASCIIs are
  // shown as-is e.g. `=` in output of `print('\N{BELL}'.encode('utf-16'))`:
  // `b'\xff\xfe=\xd8\x14\xdd'`.  Use ord(), and its inverse chr(), to convert
  // between Unicode Character [6] and its integer code point value.
  //
  // https://stackoverflow.com/q/14690159/is-ascii-code-7-bit-or-8-bit
}
