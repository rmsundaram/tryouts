// 'a: 'd means 'a outlives 'd
// https://doc.rust-lang.org/reference/trait-bounds.html#lifetime-bounds
fn choose<'a: 'd,'b: 'd,'c: 'd, 'd>(a: &'a i32, b: &'b i32, c: &'c i32, i: i32) -> &'d i32 {
    match i % 3 {
        0 => a,
        1 => b,
        2 => c,
        _ => unreachable!()
    }
}

fn main() {
    let a = 0;
    let b = 1;
    let c = 2;
    println!("{}", choose(&a, &b, &c, 4));
}
