use std::fmt;

struct Point {
  x: f32,
  y: f32,
  z: f32,
}

impl Point {
  // associated function: first param isn’t self
  // common convention to name the constructor ‘new’
  fn new(x: f32, y: f32, z: f32) -> Point {
    Point { x, y, z }
  }

  // In Rust, references are created by `&` and dereferenced by `*`.
  // Take self by (immutable) reference a.k.a shared borrow (&) as opposed to
  // mutable reference a.k.a mutable borrow (&mut).
  // http://intorust.com/tutorial/ownership/
  fn displace(&self, delta: Point) -> Self {
    Point {
      x: self.x + delta.x,
      y: self.y + delta.y,
      z: self.z + delta.z,
    }
  }

  fn translate(&mut self, delta: Point) -> &mut Self {
    self.x += delta.x;
    self.y += delta.y;
    self.z += delta.z;
    self
  }
}

impl fmt::Display for Point {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "({:.3}, {:.3}, {:.3})", self.x, self.y, self.z)
  }
}

fn main() {
  println!("hello");
  let f: f32 = 1.0;
  println!("{} is an example print", f);
  let mut p = Point::new(0.0, 0.0, 1.0);
  p.y = 0.3454345;
  println!("{}", p);

  // make a new Point displacing p
  let q = p.displace(Point::new(1.0, 2.0, 3.0));
  print!("{}\n", q);
  // disallowed since q is immutable
  // q.translate(Point::new(-2.0, 0.0, 1.0));

  // change p in place
  p.translate(Point::new(-2.0, 0.0, 1.0));
  println!("{}", p);

  let _s: &'static str = "hello";
}
