use std::fmt::{self, Display, Formatter};

struct Person {
  name: String,
  age: u8,
}

impl Person {
  fn new(name: &str, age: u8) -> Self {
    Person {
      name: name.to_string(),
      age: age,
    }
  }

  fn bump_age(&mut self, delta: u8) -> &mut Self {
    self.age += delta;
    self
  }
}

impl Clone for Person {
  fn clone(&self) -> Self {
    Person {
      name: self.name.clone(),
      age: self.age,
    }
  }
}

impl Display for Person {
  // immutable borrow
  fn fmt(&self, f: &mut Formatter) -> fmt::Result {
    write!(f, "{:p}: Name {}, Age {}", &self, self.name, self.age)
  }
}

impl fmt::Pointer for Person {
  fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
    // use `as` to convert to a `*const T`, which implements Pointer, which we can use

    let ptr = self as *const Self;
    fmt::Pointer::fmt(&ptr, f)
  }
}

fn main() {
  let p = Person::new("Krishna", 8);
  println!("{:p}", p);

  let mut a = p.clone();
  a.bump_age(4).bump_age(2);
  println!("{:p}", a);

  let mut q = p; // moved
  q.bump_age(10);
  println!("{:p}", q); // can’t print p since it’s moved now
}
