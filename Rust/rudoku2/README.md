A sudoku solver with constraint propagation and exclusives pruning.

# Algorithm
Follows _Peter Norvig_’s sudoku solver [\[1\]](#references) at a high-level, but iteration is used instead of recursion.  Additionally this prunes exclusives -- singles, twins and triplets -- after filling a cell; explained in [\[2\]](#references).

An as-is translation of Norvig’s Python solution to Rust is [shared as a gist][norvig-rust].  This performs poorly but is useful for pedagogical reasons.

# Performance
Solves [sudoku17.txt][] containing 49151 puzzles (with only 17 filled cells) in 26.065s; ~530.314µs/puzzle.

# TODO
1. Maintain a map of digits → contained cells in a given row, col and block
   to avoid visiting every peer of a cell when pruning, similar to [\[3\]](#references).

# References

1. [Solving Every Sudoku Puzzle](http://norvig.com/sudoku.html), _Peter Norvig_
2. [Fast Sudoku Solver in Haskell](https://abhinavsarkar.net/posts/fast-sudoku-solver-in-haskell-2/), _Abhinav Sarkar_
3. [A Fast Sudoku Solver](https://www.sebastiansylvan.com/post/sudoku/), _Sebastian Sylvan_

# See Also

* [Rapidly Solving Sudoku, N-Queens, Pentomino Placement, and More, With Knuth’s Algorithm X and Dancing Links][algo-x]
* [Zendoku Dancing Links, Algorithm X][zendoku]


[sudoku17.txt]:https://abhinavsarkar.net/files/sudoku17.txt.bz2
[norvig-rust]: https://gist.github.com/legends2k/f4530712625f3d1f94a7b66d2101531c
[algo-x]: https://blog.demofox.org/2022/10/30/rapidly-solving-sudoku-n-queens-pentomino-placement-and-more-with-knuths-algorithm-x-and-dancing-links/
[zendoku]: https://blog.demofox.org/2022/10/30/rapidly-solving-sudoku-n-queens-pentomino-placement-and-more-with-knuths-algorithm-x-and-dancing-links/
