use std::fmt;
use std::fmt::Error;
use std::io::{self, BufRead};
use std::time::Instant;

#[derive(Copy, Clone)]
struct Cell {
  candidates: u16,
}

impl Default for Cell {
  fn default() -> Self {
    Cell {
      candidates: Cell::CANDIDATE_MASK,
    }
  }
}

impl Cell {
  const CANDIDATE_MASK: u16 = 0b0001_1111_1111;

  fn new_empty() -> Self {
    Cell { candidates: 0 }
  }

  fn is_filled(&self) -> bool {
    self.candidates.count_ones() == 1
  }

  fn possibilities(&self) -> u8 {
    self.candidates.count_ones() as u8
  }

  fn value(&self) -> u8 {
    match self.is_filled() {
      true => self.candidates.trailing_zeros() as u8 + 1,
      false => 0,
    }
  }

  // returns true if it wasn’t already cleared
  fn clear(&mut self, digit: u8) -> bool {
    let mask = 1_u16 << (digit - 1);
    let was_set = self.candidates & mask;
    self.candidates &= !mask;
    was_set != 0
  }
}

impl Iterator for Cell {
  type Item = u8;
  /// USAGE
  /// ```
  /// for bit in cell {
  /// }
  /// ```
  fn next(&mut self) -> Option<u8> {
    let val = self.candidates.trailing_zeros();
    match val {
      x if x > 8 => None,
      y => {
        self.candidates ^= 1_u16 << y;
        Some(y as u8 + 1)
      }
    }
  }
}

impl fmt::Debug for Cell {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    match self.value() {
      0 => {
        write!(f, "[")?;
        for i in 1..=9 {
          write!(
            f,
            "{}",
            match self.candidates & (1_u16 << (i - 1)) {
              0 => ' ',
              _ => std::char::from_digit(i as u32, 10).unwrap(),
            },
          )?;
        }
        write!(f, "]")
      }
      value => write!(f, "     {}     ", value),
    }
  }
}

struct Grid {
  cells: [Cell; 81],
  next_idx: i8,
  // board and the next index to pick-up
  states: Vec<([Cell; 81], i8)>,
}

impl Default for Grid {
  fn default() -> Self {
    Grid {
      cells: [Cell::default(); 81],
      next_idx: -1,
      states: Vec::<([Cell; 81], i8)>::with_capacity(10),
    }
  }
}

fn row_col(idx: usize) -> (usize, usize) {
  (idx / 9, idx % 9)
}

fn idx(row: usize, col: usize) -> usize {
  row * 9 + col
}

fn sub(row: usize, col: usize, i: usize) -> usize {
  const START_IDX: [usize; 9] = [0, 3, 6, 27, 30, 33, 54, 57, 60];
  let block_idx = (row / 3) * 3 + (col / 3);
  let sub_start_idx = START_IDX[block_idx];
  sub_start_idx + ((i / 3) * 9) + (i % 3)
}

impl Grid {
  fn fill(&mut self, pos: usize, digit: u8) -> bool {
    let mask = 1_u16 << (digit - 1);
    debug_assert_ne!(self.cells[pos].candidates & mask, 0);
    let eliminated = self.cells[pos].candidates ^ mask;
    if self.prune(pos, eliminated) {
      debug_assert_eq!(self.cells[pos].candidates, mask);
      let (row, col) = row_col(pos);
      (0..9)
        .filter(|&i| idx(row, i) != pos)
        .for_each(|i| self.prune_exclusives_from_peers(row, i));
      (0..9)
        .filter(|&i| idx(i, col) != pos)
        .for_each(|i| self.prune_exclusives_from_peers(i, col));
      (0..9).filter(|&i| sub(row, col, i) != pos).for_each(|i| {
        let (r, c) = row_col(sub(row, col, i));
        self.prune_exclusives_from_peers(r, c);
      });
      return true;
    }
    false
  }

  // returns false if pruning led to inconsistencies
  fn prune(&mut self, pos: usize, candidates: u16) -> bool {
    let eliminated = candidates & self.cells[pos].candidates;
    if eliminated == 0 {
      return true; // |candidates| already eliminated
    }
    self.cells[pos].candidates ^= eliminated;
    if self.cells[pos].candidates == 0 {
      return false; // contradiction; backtrack
    }
    let (row, col) = row_col(pos);
    if self.cells[pos].is_filled() {
      if !(0..9)
        .filter(|&i| idx(row, i) != pos)
        .all(|i| self.prune(idx(row, i), self.cells[pos].candidates))
        || !(0..9)
          .filter(|&i| idx(i, col) != pos)
          .all(|i| self.prune(idx(i, col), self.cells[pos].candidates))
        || !(0..9)
          .filter(|&i| sub(row, col, i) != pos)
          .all(|i| self.prune(sub(row, col, i), self.cells[pos].candidates))
      {
        return false;
      }
    } else {
      // check for next_idx
      self.update_next_idx(self.cells[pos].possibilities(), pos as i8);
    }
    true
  }

  // accepts closure that maps a peer index to grid index
  fn prune_exclusives<F: Fn(usize) -> usize>(&mut self, peers: F) {
    // check for singles, twins and triplets in peers
    let mut digit_occurances = [0_u16; 4];
    let mut digit_cells = [-1_i8; 81]; // 9 digits, each can hold 9 cells
    for i in 0..9 {
      let peer = peers(i);
      if !self.cells[peer].is_filled() {
        let mut candidates = self.cells[peer].candidates;
        candidates ^= candidates & digit_occurances[0];
        let mut j = 3;
        while candidates != 0 && j != 0 {
          let common = digit_occurances[j] & candidates;
          candidates ^= common;
          digit_occurances[j] ^= common;
          digit_occurances[(j + 1) % 4] |= common;
          j -= 1;
        }
        digit_occurances[1] |= candidates;
        for digit in self.cells[peer] {
          let mut k = 9 * (digit - 1) as usize;
          while digit_cells[k] != -1 {
            k += 1;
          }
          digit_cells[k] = peer as i8;
        }
      }
    }
    // prune exclusives found
    if digit_occurances[1].count_ones() == 1 {
      let digit = digit_occurances[1].trailing_zeros() as usize;
      debug_assert_ne!(digit_cells[digit * 9], -1);
      let peer = digit_cells[digit * 9] as usize;
      self.fill(peer, digit as u8 + 1);
    }
    if digit_occurances[2].count_ones() == 2 {
      let new_candidates = digit_occurances[2];
      let digit1 = digit_occurances[2].trailing_zeros() as usize;
      debug_assert_ne!(digit_cells[digit1 * 9], -1);
      debug_assert_ne!(digit_cells[digit1 * 9 + 1], -1);
      let peer1 = digit_cells[digit1 * 9] as usize;
      let peer2 = digit_cells[digit1 * 9 + 1] as usize;
      digit_occurances[2] ^= 1_u16 << digit1;
      debug_assert_eq!(digit_occurances[2].count_ones(), 1);
      let digit2 = digit_occurances[2].trailing_zeros() as usize;
      debug_assert_ne!(digit_cells[digit2 * 9], -1);
      debug_assert_ne!(digit_cells[digit2 * 9 + 1], -1);
      let peer3 = digit_cells[digit2 * 9] as usize;
      let peer4 = digit_cells[digit2 * 9 + 1] as usize;
      if peer1 == peer3 && peer2 == peer4 {
        self.prune(peer1, !new_candidates);
        self.prune(peer2, !new_candidates);
      }
    }
    if digit_occurances[3].count_ones() == 3 {
      let new_candidates = digit_occurances[3];
      let digit1 = digit_occurances[3].trailing_zeros() as usize;
      debug_assert_ne!(digit_cells[digit1 * 9], -1);
      debug_assert_ne!(digit_cells[digit1 * 9 + 1], -1);
      debug_assert_ne!(digit_cells[digit1 * 9 + 2], -1);
      let peer1 = digit_cells[digit1 * 9] as usize;
      let peer2 = digit_cells[digit1 * 9 + 1] as usize;
      let peer3 = digit_cells[digit1 * 9 + 2] as usize;
      digit_occurances[3] ^= 1_u16 << digit1;
      debug_assert_eq!(digit_occurances[3].count_ones(), 2);
      let digit2 = digit_occurances[3].trailing_zeros() as usize;
      debug_assert_ne!(digit_cells[digit2 * 9], -1);
      debug_assert_ne!(digit_cells[digit2 * 9 + 1], -1);
      debug_assert_ne!(digit_cells[digit2 * 9 + 2], -1);
      let peer4 = digit_cells[digit2 * 9] as usize;
      let peer5 = digit_cells[digit2 * 9 + 1] as usize;
      let peer6 = digit_cells[digit2 * 9 + 2] as usize;
      digit_occurances[3] ^= 1_u16 << digit2;
      debug_assert_eq!(digit_occurances[3].count_ones(), 1);
      let digit3 = digit_occurances[3].trailing_zeros() as usize;
      debug_assert_ne!(digit_cells[digit3 * 9], -1);
      debug_assert_ne!(digit_cells[digit3 * 9 + 1], -1);
      debug_assert_ne!(digit_cells[digit3 * 9 + 2], -1);
      let peer7 = digit_cells[digit3 * 9] as usize;
      let peer8 = digit_cells[digit3 * 9 + 1] as usize;
      let peer9 = digit_cells[digit3 * 9 + 2] as usize;
      if (peer1 == peer4)     // 1 == 4 == 7
          && (peer4 == peer7)
          && (peer2 == peer5)   // 2 == 5 == 8
          && (peer5 == peer8)
          && (peer3 == peer6)   // 3 == 6 == 9
          && (peer6 == peer9)
      {
        self.prune(peer1, !new_candidates);
        self.prune(peer2, !new_candidates);
        self.prune(peer3, !new_candidates);
      }
    }
  }

  fn prune_exclusives_from_peers(&mut self, row: usize, col: usize) {
    // check for singles, twins and triplets in row, col and block peers
    self.prune_exclusives(|i| idx(row, i));
    self.prune_exclusives(|i| idx(i, col));
    self.prune_exclusives(|i| sub(row, col, i));
  }

  fn update_next_idx(&mut self, candidates: u8, idx: i8) -> bool {
    debug_assert_eq!(self.cells[idx as usize].is_filled(), false);
    if ((self.next_idx == -1)
      || (self.cells[self.next_idx as usize].is_filled()))
      && candidates == 2
    {
      self.next_idx = idx;
      return true;
    }
    false
  }

  fn next_unfilled(&mut self) -> Option<usize> {
    if self.next_idx != -1 {
      let next = self.next_idx as usize;
      self.next_idx = -1;
      return Some(next);
    }
    let (mut min_ops, mut min_idx) = (9, 81);
    match self
      .cells
      .iter()
      .enumerate()
      .filter(|&(_idx, &v)| !v.is_filled())
      .find(|&(idx, v)| {
        let options = v.possibilities();
        if min_ops > options {
          min_ops = options;
          min_idx = idx;
        }
        options == 2
      }) {
      Some(c) => Some(c.0),
      None => match min_idx {
        81 => None,
        _ => Some(min_idx as usize),
      },
    }
  }

  fn push_state(&mut self, index: i8, digit: u8) {
    self.states.push((self.cells, index));
    // back-up current board but clear the current digit in trial
    let state_pair = self.states.last_mut().unwrap();
    state_pair.0[state_pair.1 as usize].clear(digit);
  }

  fn pop_state(&mut self) -> bool {
    match self.states.pop() {
      Some(state_pair) => {
        self.cells = state_pair.0;
        self.next_idx = state_pair.1;
        true
      }
      _ => false,
    }
  }

  fn is_subset_solved(subset: &[Cell]) -> bool {
    subset
      .iter()
      .fold(0_u16, |bits, c| bits ^ 1_u16 << c.value().saturating_sub(1))
      == Cell::CANDIDATE_MASK
  }

  fn get_row(&self, row: usize) -> &[Cell] {
    debug_assert!(row < 9);
    let idx = 9 * row;
    &self.cells[idx..(idx + 9)]
  }

  fn get_col(&self, col: usize) -> [Cell; 9] {
    debug_assert!(col < 9);
    let mut out = [Cell::new_empty(); 9];
    for i in 0..9 {
      out[i] = self.cells[idx(i, col)];
    }
    out
  }

  fn get_sub(&self, idx: usize) -> [Cell; 9] {
    debug_assert!(idx < 9);
    let mut sub = [Cell::new_empty(); 9];
    const START_IDX: [usize; 9] = [0, 3, 6, 27, 30, 33, 54, 57, 60];
    let mut idx = START_IDX[idx];
    for i in 0..3 {
      sub[i * 3 + 0] = self.cells[idx + 0];
      sub[i * 3 + 1] = self.cells[idx + 1];
      sub[i * 3 + 2] = self.cells[idx + 2];
      idx += 9;
    }
    sub
  }

  fn is_solved(&self) -> bool {
    (0..9).all(|i| Grid::is_subset_solved(self.get_row(i)))
      && (0..9).all(|i| Grid::is_subset_solved(&self.get_col(i)))
      && (0..9).all(|i| Grid::is_subset_solved(&self.get_sub(i)))
  }

  // useful when debugging e.g. `p self->print(self)`
  #[allow(dead_code)]
  fn print(&self) {
    println!("{:?}", *self);
  }
}

impl std::fmt::Debug for Grid {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    for r in 0..9 {
      let row_start = r * 9;
      for c in 0..9 {
        write!(f, "{:?}", self.cells[row_start + c])?;
      }
      writeln!(f)?;
    }
    Ok(())
  }
}

fn solve(puzzle: &str) -> Result<Grid, Error> {
  let mut g = Grid::default();
  for (i, ch) in puzzle.char_indices() {
    if ch.is_ascii_digit() {
      let digit = ch as u8 - b'0';
      g.fill(i, digit);
    }
  }

  while let Some(unfilled) = g.next_unfilled() {
    let mut current_cell = g.cells[unfilled];
    if let Some(digit) = current_cell.next() {
      // save state only if there’re further possibilities with this cell
      if current_cell.possibilities() != 0 {
        g.push_state(unfilled as i8, digit);
      } else {
        // set a dummy bit to prevent pruning from returning early
        g.cells[unfilled].candidates |= 1;
      }
      if !g.fill(unfilled, digit) {
        if !g.pop_state() {
          break;
        }
      }
    } else {
      if !g.pop_state() {
        break;
      }
    }
  }

  if !g.is_solved() {
    println!("Invalid board\n{:?}", g);
    return Err(Error);
  }
  Ok(g)
}

fn main() -> Result<(), Error> {
  let puzzles: Vec<String> =
    io::stdin().lock().lines().map(|l| l.unwrap()).collect();
  let mut solns = Vec::<Grid>::with_capacity(puzzles.len());

  let before = Instant::now();
  for puzzle in &puzzles {
    solns.push(solve(&puzzle)?);
  }
  if !puzzles.is_empty() {
    // https://stackoverflow.com/a/57341631/183120
    let duration = before.elapsed();
    println!(
      "Solved {} puzzle(s) in {:.3?}; average {:.3?}/puzzle",
      puzzles.len(),
      duration,
      duration / puzzles.len() as u32
    );
  }

  Ok(())
}
