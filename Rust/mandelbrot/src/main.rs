// Reference
// Programming Rust, §2 A Tour of Rust, Concurrency
// Programming Rust, §19 Concurrency, Fork-Join Parallelism

use num_complex::Complex;

use image::ColorType;

use std::process;

use palette::{Hsv, Srgb};

use clap::crate_version;
use clap::{App, Arg};

use minifb::{Key, Window, WindowOptions};

// 1D mimicry of Mandelbrot Set, c ∈ M if z ⥇ ∞
// Some experimentation shows that if c is greater than 0.25, or less than –2.0,
// then z eventually becomes infinitely large; otherwise, it stays somewhere in
// the neighborhood of zero. [-2, 0.25] makes the set.
#[allow(dead_code)]
fn square_add_1d_loop(c: f64) {
  let mut z = 0.;
  loop {
    z = z * z + c;
  }
}

const ESCAPE_RADIUS: f64 = 3.0;
const ESCAPE_RADIUS_SQR: f64 = ESCAPE_RADIUS * ESCAPE_RADIUS;

/// Mandelbrot set contains complex numbers c such that f(z) = z² + c doesn’t
/// fly out to infinity. Expanding the above game of finding the set, with
/// complex numbers, produces truly bizarre and beautiful patterns; we want to
/// plot this.
///
///  black → c ∈ M
/// colour → c ∉ M (colour based on how fast f(z) diverges for given c)
///
/// To short-circuit infinite loop, we use two observations:
///   1. If z ever leaves origin-centred circle of radius 2 then it’d fly out to
///      infinity eventually
///   2. Iterating for some limited number gives a decent enough approximation
///
/// z² is nothing but the magnitude / l₂-norm of the complex number, z.
/// c takes values from the complex plane that our canvas bounds.
///
/// Smooth Colouring
///
/// [1] and [2] gives a continuous function mapping the discrete iteration count
/// to a continuous colour using ln(). [3] is a good implementation of [1] using
/// HSV colourspace. [4] gives an alternative using Bernstein polynomials in RGB
/// colourspace; this however needs around 500 iterations for band-free results.
///
/// [1]: http://linas.org/art-gallery/escape/smooth.html
/// [2]: https://en.wikipedia.org/wiki/Mandelbrot_set#Continuous_.28smooth.29_coloring
/// [3]: https://github.com/cslarsen/mandelbrot-js
/// [4]: https://solarianprogrammer.com/2013/02/28/mandelbrot-set-cpp-11/
// Function name denotes number of iterations it took to deduce membership.
fn escape_count(c: Complex<f64>, max_iter: u32) -> Option<f64> {
  let mut z = Complex { re: 0.0, im: 0.0 };
  let inv_ln2 = 1.0 / 2.0f64.ln();
  for i in 0..max_iter {
    z = z * z + c;
    if z.norm_sqr() > ESCAPE_RADIUS_SQR {
      // based on Wikipedia’s pseudocode [2]
      let ln_zn = z.norm_sqr().ln() * 0.5;
      let nu = (ln_zn * inv_ln2).ln() * inv_ln2;
      return Some(i as f64 + 1.0 - nu);
      // return i packaged in Option::Some
    }
  }
  // c ∈ M
  None // Block ending with expression not followed by
       // semicolon is its return value; any block can
       // act as an expression.
}

// Input: pixel, image dimensions, window into Mandelbrot set
// Output: point in Mandelbrot set
fn pixel_to_point(
  pixel: (usize, usize),
  img_dims: (usize, usize),
  left_top: Complex<f64>,
  right_bot: Complex<f64>,
) -> Complex<f64> {
  // Mandelbrot sampling rect dimensions
  let (width, height) =
    (right_bot.re - left_top.re, left_top.im - right_bot.im);
  let sx = width / img_dims.0 as f64;
  // flip Y as Mandelbrot follow math coordinate system
  let sy = -height / img_dims.1 as f64;
  let tx = left_top.re;
  let ty = left_top.im;
  Complex {
    re: pixel.0 as f64 * sx + tx,
    im: pixel.1 as f64 * sy + ty,
  }
}

#[test]
fn test_pixel_to_point() {
  assert_eq!(
    pixel_to_point(
      (25, 75),
      (100, 100),
      Complex { re: -1.0, im: 1.0 },
      Complex { re: 1.0, im: -1.0 }
    ),
    Complex { re: -0.5, im: -0.5 }
  );
}

const COLOR_TYPE: ColorType = ColorType::RGB(8);
const NUM_PIXEL_COMPONENTS: usize = 3;

// https://math.stackexchange.com/q/16970
fn guess_iters(left_top: Complex<f64>, right_bot: Complex<f64>) -> u32 {
  let delta = left_top - right_bot;
  let x_delta = delta.re.abs();
  let y_delta = delta.im.abs();
  let min = x_delta.min(y_delta);
  (223.0f64 / ((0.001f64 + (2.0 * min)).sqrt())).floor() as u32
}

fn put_colour(pixel: &mut [u8], intensity: f64, max_iters: u32) {
  assert!(intensity >= 0.0);
  // make sure interior isn’t filled with red (continues to remain black)
  if intensity > 0.0 {
    let colour_hsv = Hsv::new(360.0 * intensity / (max_iters as f64), 1.0, 1.0);
    let colour_srgb = Srgb::from(colour_hsv);
    let colour_byte: Srgb<u8> = colour_srgb.into_format();
    pixel.copy_from_slice(colour_byte.as_ref());
  }
}

fn compute(
  pixels: &mut [u8],
  img_dims: (usize, usize),
  left_top: Complex<f64>,
  right_bot: Complex<f64>,
) {
  assert!(pixels.len() == (img_dims.0 * img_dims.1 * NUM_PIXEL_COMPONENTS));
  let max_iters: u32 = guess_iters(left_top, right_bot);
  // https://stackoverflow.com/q/39204908/183120
  if cfg!(debug_assertions) {
    println!("{} iterations/pixel", max_iters);
  }
  for y in 0..img_dims.1 {
    for x in 0..img_dims.0 {
      let pt = pixel_to_point((x, y), img_dims, left_top, right_bot);
      let intensity = escape_count(pt, max_iters).unwrap_or(0f64);
      let offset =
        (y * (img_dims.0 * NUM_PIXEL_COMPONENTS)) + (x * NUM_PIXEL_COMPONENTS);
      put_colour(
        &mut pixels[offset..(offset + NUM_PIXEL_COMPONENTS)],
        intensity,
        max_iters,
      );
    }
  }
}

fn display(pixels: &[u8], bounds: (usize, usize)) {
  let mut window = Window::new(
    "Mandelbrot Set",
    bounds.0,
    bounds.1,
    WindowOptions {
      title: true,
      resize: false,
      borderless: true,
      scale: minifb::Scale::X1,
      scale_mode: minifb::ScaleMode::Center,
    },
  )
  .unwrap_or_else(|e| {
    // show human readable
    // https://blog.burntsushi.net/csv/#switch-to-recoverable-errors
    println!("error opening window\n{}", e);
    process::exit(1);
  });

  let buffer: Vec<_> = (0..(bounds.0 * bounds.1))
    .map(|u| -> u32 {
      let i = u * 3;
      ((pixels[i] as u32) << 16)
        | ((pixels[i + 1] as u32) << 8)
        | (pixels[i + 2] as u32)
    })
    .collect();

  while window.is_open() && !window.is_key_down(Key::Escape) {
    window
      .update_with_buffer(&buffer, bounds.0, bounds.1)
      .unwrap_or_else(|e| {
        println!("error drawing to window\n{}", e);
        process::exit(1);
      });
  }
}

// TODO: Parallelize
// https://rust-lang-nursery.github.io/rust-cookbook/concurrency/threads.html#draw-fractal-dispatching-work-to-a-thread-pool
fn main() {
  let opts = App::new("mandelbrot")
    .author("Sundaram Ramaswamy <legends2k@yahoo.com>")
    .version(crate_version!())
    .about("Mandelbrot set plotter")
    .arg(Arg::from_usage(
      "-o --output=[FILE] 'Output to image file; \
       supported extensions {.png, .jp(e)g, .bmp, .ppm}'",
    ))
    .get_matches();

  // validate file extension, if supplied
  let out = opts.value_of("output");
  let (onscreen, outfile) = match out {
    None => (true, None),
    Some(path) => {
      let valid_exts = ["png", "jpg", "jpeg", "bmp", "ppm"];
      match path.rfind('.') {
        None => (false, Some(format!("{}.png", path))),
        Some(idx) => (
          !valid_exts.iter().any(|ext| *ext == &path[idx + 1..]),
          Some(path.to_string()),
        ),
      }
    }
  };
  if onscreen && outfile.is_some() {
    println!("unknown image extension\nfalling back to on-screen render...");
  }

  // we’ve a valid sink, compute now!
  if onscreen || outfile.is_some() {
    let bounds: (usize, usize) = (1200, 1200);
    let right_bot: Complex<f64> = Complex { re: 1.1, im: -1.7 };
    let left_top: Complex<f64> = Complex { re: -2.3, im: 1.7 };
    let mut pixels = vec![0; bounds.0 * bounds.1 * NUM_PIXEL_COMPONENTS];
    compute(&mut pixels, bounds, left_top, right_bot);
    match onscreen {
      true => display(&pixels, bounds),
      false => {
        let file = outfile.unwrap();
        match image::save_buffer(
          &file,
          &pixels,
          bounds.0 as u32,
          bounds.1 as u32,
          COLOR_TYPE,
        ) {
          Ok(_) => {}
          Err(e) => println!("{}\nfailed writing {}", e, file),
        }
      }
    }
  }
}
