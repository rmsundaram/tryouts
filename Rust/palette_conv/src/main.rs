extern crate palette;

use palette::{Hsv, Srgb};

fn main() {
    // in HSV, primary and primary colours occur at 60° intervals
    let yellow_hsv = Hsv::new(60.0, 1.0, 1.0);
    let yellow_srgb: Srgb<f32> = Srgb::from(yellow_hsv);
    // convert to u8 from f32
    let yellow_byte: Srgb<u8> = yellow_srgb.into_format();
    println!("{:?} → {:?}", yellow_srgb, yellow_byte);
    let yellow_linear = yellow_srgb.into_linear();
    println!("{:?} → {:?}", yellow_srgb, yellow_linear);
    println!("------------------------------");

    // define another Srgb colour with f64 i.e. rgb::Rgb::<encoding::Srgb, f64>
    let c1_f64: Srgb<f64> = Srgb::new(0.5, 0.2, 1.0);
    let c1_u8: Srgb<u8> = c1_f64.into_format();
    println!("{:?} → {:?}", c1_f64, c1_u8);
    let c1_linear = c1_f64.into_linear();
    println!("{:?} → {:?}", c1_f64, c1_linear);
    // convert linear → Srgb<f64> → Srgb<f32>
    let c1_f32: Srgb<f32> = Srgb::from_linear(c1_linear).into_format();
    println!("{:?} → {:?}", c1_linear, c1_f32);
    println!("------------------------------");

    // define another sRGB colour with bytes
    let c_byte = Srgb::new(38u8, 42, 190);
    // convert to u8 slice
    let c_slice: &[u8] = c_byte.as_ref();
    println!("{:?} → {:?}", c_byte, c_slice);
    // let c_lin = c_byte.into_linear();
    // u8 is insufficient to represent a linear colour; compiler disallows this
    // as u8 doesn’t satisfy the trait bounds palette::float
}
