<meta charset="utf-8">

# Why explicitly inherit from `NSObject`?

Cocoa is both a library specification and implementation given by Apple (like Microsoft’s COM).  Another popular implementation of the specification is by GNU: [GNUStep](http://www.gnustep.org/information/aboutGNUstep.html).  GNUStep has it’s own [`NSObject`](http://www.gnustep.org/resources/documentation/Developer/Base/Reference/index.html) implementation which can also be used as the root class forming a different object hierarchy.  This means that the compiler cannot implicitly hard-code this inheritance.  More on Objective-C’s standard library [here](https://stackoverflow.com/a/2835348/183120).

Even if we move to Swift, `NSObject` isn’t going to leave us, since [Swift objects also inherit from `NSObject`](https://stackoverflow.com/q/25705637/183120) 😛

# Are `init` and `dealloc` implicitly supplied?

Yes.

# Simple TouchBar

1. Open a new XCode project
2. Select macOS, Cocoa app
  + Check `Use Storyboards`
  + Uncheck `Use Core Data`, `Include Unit Tests`, `Include UI Tests` and `Create Document-Based Application`
3. Once project is loaded, right-click project folder, select `New` and choose `Cocoa Class`
4. Name it `WindowController`, subclass from `NSWindowController`
5. Uncheck `xib` creation
6. Once created, go to storyboard, select `Window Controller`
7. Open `Identity Inspector`, rename _Class_ to `WindowController`
8. Open `WindowController.m`; override `makeTouchBar` and `touchBar: makeItemForIdentifier:`
9. Touchbar should now show up as the window opens up
10. In case we want to show touch bar from a view controller, to get calls to its `makeTouchBar`, do

``` objc
- (void) viewDidAppear {
    [super viewDidAppear];
    [self.view.window unbind:@"touchBar"];
    [self.view.window bind:@"touchBar" toObject:self withKeyPath:@"touchBar" options:nil];
}
```

11. In Apple WWDC’s _Touch Bar Fundamentals_ talk (at 18:24), it’s said the `NSResponder` of the `NSView` of the window should be made first by overriding `acceptsFirstResponder`; since macOS 10.10, `NSViewController` of the `NSView` would be `NSView`’s next responder and hence the above code snippet mightn’t be needed.  See video and [this](https://stackoverflow.com/q/20061052/183120) for details.

# Interface Builder Quirks

1. To create a `NSImageView` wrapped in a `NSScrollView`, create the former first under `NSView`.  When it’s selected, in the bottom-right corner, look for the _Embed In_ button.  Now click and select _Embed In View -> Scroll View_.
2. Adding constraints: though constraints look like controls, they aren’t.  Add constraints to a control by selecting it and in the bottom-right corner look for _Add New Constraints_ button.
3. To refer to a control in code, Ctrl-drag-and-drop it into a header file having the respective view controller class.  It creates a property which can be used in code.


# Views

Source: [Cocoa Programming for Mac OS X, 3rd Edition](http://www.informit.com/articles/article.aspx?p=1211756)

All visible objects in an application are either windows or views.  A window is a collection of views, each responsible for a rectangle in the window; for its rendering and eventing there.  `NSButton`, `NSTextField`, etc. are subclasses of `NSView`; a window isn’t though.

## View Hierarchy

Views form a hierarchy.  Window has a _content view_ which can be used to draw directly on; like Win32 window’s HDC.  It can also host other subviews like buttons, etc. which in turn can have subviews.  Each view has access to its super and subviews via `superview` and `subviews`.

Views are initialized with `initWithFrame` message.

Each view can override the `drawRect` message (function) that comes with the dirty rectangle as argument.  This can be used to draw the view.  Each view has its own graphics context which is _focus locked_ before passing `drawRect` to it.  Whenever you issue drawing commands, they will be executed in the current, locked graphics context.

## Drawing

Use `NSBezierPath` to draw lines, curves, rectangles, etc; `NSImage` to create composite images on the view.

``` objc
[[NSColor orangeColor] set];
[NSBezierPath fillRect: dirtyRect];
```

This works since each device context has its own colour, coordinate system, font, clip rect, etc.  One can manually trigger redraw of view with `[myView setNeedsDisplay: YES]` or a specific dirty rect with `[myView setNeedsDisplayInRect: dirtyRect]`.

`NSCell` is a mechanism to draw text or images in a view object without the overhead of a full `NSView` subclass.  This is useful when many `NSView`s have to be hosted inside a parent `NSView` e.g. the calculator app’s content view hosting many `NSButton`s.  Instead a `NSMatrix` (view) can be used to host many `NSCell`s.  In the end, NSButton became simply a view that had an NSButtonCell.  The button cell does everything, and NSButton simply claims a space in the window.

`NSCell.controlView` gives the owning `NSControl` and the reverse is usually simple as `NSControl` is where `NSCell` lives; see [SO post][cell-2-slider].

[cell-2-slider]: https://stackoverflow.com/a/44844265/183120

## Delegates (Event Handling) and Data Sources

> "A data source is like a delegate except that, instead of being delegated control of the user interface, it is delegated control of data. "
>   -- Cocoa Documentation

Data sources are used to populate sub-views with data.  Event handling is done with delegates in Cocoa.  [Official documentation][delegates-data-source] has the detailed explanation; [this post][data-source-vs-delegates] also gives a good explanation of both concepts.

``` objc
// -------------------- library code --------------------

// Declare protocol for delegates to follow.
@protocol DataHandlerDelegate
// return NO if you don't need further fetching
-(BOOL) fetchComplete : (NSArray)data;
@end

// Delegate can be any class, so hold them with an id but with the protocol type
@interface DataFetcher : NSObject {
  id<DataHandlerDelegate> delegate_;
}
-(id)initWithDelegate:(id<HistorySwiperDelegate>)delegate;
// another style is to expose it as a property and the client code sets it
@property(nonatomic, assign) id<DataHandlerDelegate> delegate;
@end

@implementation DataFetcher
@synthesize delegate = delegate_;
-(id)initWithDelegate:(id<HistorySwiperDelegate>)delegate {
  delegate_ = delegate;
}
- (void)lastByteArrived {
    // check only optional methods of formal protocol or methods of informal protocol
    if ( [delegate respondsToSelector:@selector(fetchComplete)] ) {
        if ( [delegate fetchComplete:data] ) {
            // fetch more data
        } else {
            // cease fetching
        }
    }
}
@end

// -------------------- client code --------------------
@interface MediaPlayer : NSObject <DataHandlerDelegate>
-(BOOL)fetchComplete : (NSArray)dataa;
@end

@implementation MediaPlayer
-(id)init {
  //...
  fetcher.delegate = self;
}
// handle event
-(BOOL)fetchComplete : (NSArray)data {
}

@end
```


[delegates-data-source]: https://developer.apple.com/library/archive/documentation/General/Conceptual/CocoaEncyclopedia/DelegatesandDataSources/DelegatesandDataSources.html
[data-source-vs-delegates]: https://stackoverflow.com/q/2232147/183120

# Animating Custom Property

Any normal property can be animated through the animator proxy.  `defaultAnimationForKey` will be looked up if the property is unknown to Cocoa; define the animate type for the property here.

``` objc
@interface MyClass
@property(nonatomic) float progress;
@end

@implementation MyClass
@synthesize progress = progress_;

// declare default animations for the animatable ‘progress’ property
+ (id)defaultAnimationForKey:(NSString*)key {
  return [key isEqualToString:@"progress"] ? [CABasicAnimation animation]
                                           : [super defaultAnimationForKey:key];
}
@end
```

Use it from the client code thus: `myObj.animator.progress = 1.0f`.  This would animate `progress` from current value to `1.0`; `myObj.progress = 1.0f` will set it without animation.


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none'};</script>
