% Objective-C++, Cocoa and Touch Bar 101
% Sundaram Ramaswamy

<!-- $ pandoc -t beamer -V colortheme:wolverine -V colorlinks:true -V aspectratio:1610 -V linestretch:1.5 -o slides.pdf cocoa_touchbar_slides.md -->

# Objective-C

* Evolution of C to have Smalltalk-like objects with messaging
* _NeXT Computer_ , founded by _Steve Jobs_, licensed and popularized it
  - Added a GCC extension to compile Objective-C
  - Used it for their OS: NeXTSTEP -- the precursor to macOS
  - Root class (`NSObject`) has the prefix `NS` standing for NeXTSTEP
* C++ and Objective-C are two _very_ different supersets of C
  - C++ - fairly static, focus on run-time performance, meta-programming with templates
  - Objective-C - high dynamism, rich reflection and introspection capabilities

# Fundamental Differences

* All objects inherit from `NSObject`
  - Like Java, unlike C++
  - All object pointers are of type `id`
  - Containers are heterogeneous for this reason
    - `@[ 7, YES, @"string" ]` -- array of integer, boolean and string
* There’s message passing; no method calls!
  - `[obj say: @"hello"];`  -- call bound at run-time
  - `obj.say("hello");` -- call bound at compile-time
  - Objective-C will happily let you message `nil`
  - C++ will crash calling a member function on `nullptr`
* No multiple inheritance
* Excellent reflection capabilities
  - Create a class at runtime (!?); as classes themselves are objects (!!)
  - Attach methods
  - Make an object and use it
* No `namespace`, references, templates or the benefits of strict compile-time checks
* Faster code compilation

# Basic Syntax: Class Declaration

``` objective-c++
// foo.h
@interface Foo : NSObject {
  // private data members go here
  double objVal_;
  Simple *ptr_;
}

// properties
@property(readonly) NSString* fooColour;

// instance methods
-(BOOL)isValid:(NSString*)Name
              :(float)Progress;

// class method
+(float)count;
@end
```

# Basic Syntax: Class Definition

``` objective-c++
// Foo.m
#import "foo.h"

@implementation Foo
// constructor
- (instancetype)initWithValue:(int)val {
  if ((self = [super init])) {
    objVal_ = val;
    ptr_ = [[Simple alloc] init:3.14];
  }
  return self;
}

// destructor
-(void)dealloc {
  [ptr_ release];
  [super dealloc];
}
```

# Basic Syntax: Class Definition (contd.)

``` objective-c++
// generate getter/setter, bind to ivar objColour_
@synthesize fooColour = objColour_;

-(int)increment:(int)Value {
  // do something
  [ptr_ retain];
}

-(void)decrement {
  [ptr_ release];
}
@end

// clang++ -framework Foundation hello.m
```

# Memory Management

* Primary Options
  - Manual / RAII: balancing calls to `alloc`/`retain` and `release`
  - Automatic Reference Counting (ARC)
    + `[[[SkBitmap alloc] init] autorelease]`
    + Relieves you from calling `release` manually for this _one_ `alloc`
  - Garbage collection; now deprecated
* **NARC** convention
  - `new`, `alloc`, `retain`, `copy` - method name prefix one of these?
  - Manually call `release` or arrange a smart pointer to do it
  - Don’t call `release` for functions without this

# Objective-C++

* A strange mix of Objective-C and C++
* Enables having C++ code alongside Objective-C: best of both worlds
* Object hierarchies, however, are fairly different and exist in parallel
* Source files suffixed with `.mm` as opposed to `.m`

# Cocoa

* Objective-C is the language proper -- includes only syntax and semantics
* Apple provides this macOS platform’s SDK
  - Primary tool chain is Apple Clang
  - Primary IDE is Xcode
* Considered the standard library of Objective-C
* Contains multiple frameworks/libraries including
  - Foundation Kit  -- base
  - AppKit / UIKit  -- UI
  - Core data -- serialization

# Model View Controller (MVC)

## Basic principle behind Cocoa UI

1. **Model**: app-specific data
2. **View**: user interface (I/O)
3. **Controller**: mediates between view and model
    + Provides the main operating logic for the application

# Interfaces in macOS and iOS

* Apps load interface content from `.nib` files
  + Designed in Xcode’s Interface Builder
  + Contains “freeze-drying” (Apple’s terminology) serialized objects
  + `xib` is just XML NIB (NeXT Interface Builder)
* Controls, by default, are instances of a standard Cocoa class
  + When the control’s behaviour needs to be tweaked, subclass its class
  + Use custom class as instantiate control
* Storyboards
  + Collection of view controllers all linked together via _Segues_
  + Segues are the link between two view controllers - allows you to transition between them with no code
  + `Main.storyboard` is automatically created for you

# Views

+ `NSWindow` hosts the entire views hierarchy
+ View: class responsible for a rectangle in a window
  + Rendering and eventing of its rectangle
  + All views (`NSButton`, `NSTextField`, …) are derivations of `NSView`
+ Every window starts with a plain content view - the root view
  + It hosts other subviews which in turn can host other views
  + Every view has access to `superview` and `subviews`
+ View initialized with `initWithFrame` or `awakeFromNib`
+ Override `drawRect(dirtyRect)` method and draw
+ Manual invalidation: `myView.setNeedsDisplay = YES`

# View Hierarchy

![Credits: Cocoa Programming for Mac OS X](./view_hierarchy.jpg "Credits: Cocoa Programming for Mac OS X, 3rd Edition"){ width=60% height=60% }

# Eventing

+ Responders handle event callbacks like `keyDown`, `mouseUp`, etc.
+ Every `NSView` inherits from `NSResponder`
+ Responders form a linked list
+ When the first responder ignores it’s passed on to the next
+ The window’s view would be the last responder
+ A view can set `acceptsFirstResponder` explicitly to make it the HEAD
+ `NSEvent` journey: Application → Key Window → First Responder (View / View Controller) → … → Application
  + Bubbles down and up the chain

# Touchbar

* `NSTouchBar` is the provided by a responder’s `makeTouchBar` method
  + `bar.defaultItemIdentifiers = @[]`; set default item IDs
* `NSTouchBarItem`s are populated in its `makeItemForIdentifier`
* Each responder in the chain might be overriding these methods
  + This is where you see multiple `NSTouchBar`s on the device
* **Conflict Resolution**
  + Responder with `NSTouchBar` closest to keyboard focus wins
  + A touch bar signals composability with others via `otherItemsProxy` ID
* `[NSApp setAutomaticCustomizeTouchBarMenuItemEnabled: YES]` -- to allow user customization of items and ordering
  + `customizationAllowedItemIdentifiers = @[]` takes customizable items

# Touchbar Composition

![Mail App](./touchbar_composition.jpg)

# Touchbar Items

* Set UI control in `view`
* Set your controller in `viewController`
* Set `customizationLabel`: description showing up in customization panel
* _On the bar_ `principalItemIdentifier` to physically centre _on the device_
  + `bar.principalItemIdentifier = someItemId;`
* Disallow user from removing specified item during customization
  + `bat.customizationRequiredItemIdentifiers = @[someId1, …]`

# References

1. [From C++ to Objective-C](http://www.pierre.chachatelier.fr/programmation/fichiers/cpp-objc-en.pdf) by _Pierre Chatelier_
2. [Advanced Memory Management Programming Guide](https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/MemoryMgmt/Articles/MemoryMgmt.html#//apple_ref/doc/uid/10000011-SW1), Apple Developer Documentation
3. [Cocoa Programming for Mac OS X, 3rd Edition](http://www.informit.com/articles/article.aspx?p=1211756)
4. [Delegates and Data Sources](https://developer.apple.com/library/archive/documentation/General/Conceptual/CocoaEncyclopedia/DelegatesandDataSources/DelegatesandDataSources.html), Apple Developer Documentation

# That’s all folks!

> Thank you!
