<meta charset="utf-8">

    **macOS Keychain Services**
     *from Security Framework*

# Overview

Encrypted database to store small secrets like

* Passwords
* Credit Card Information
* Cryptographic Keys
* Certificates and Identities
* Short Notes

Storing data insecure is incorrect while repeated user prompts for password is bad experience.  This problem is solved by keychain services.  It stores secrets encrypted and prompts users only when absolutely needed.

!!! Info: CryptoKit ≠ Keychain Services (compute ≠ store)
    [CryptoKit][] is a _Swift-only_ API useful for full-blown cryptography i.e. computing digests, public-key cryptography, creating/evaluating digital signatures, generating symmetric keys, authenticating messages, etc.

[CryptoKit]: https://developer.apple.com/documentation/cryptokit

# Workflow

Consider an app needing user credentials (secret) to login to a service (e.g. Reddit):

* App looks for secret in a keychain (`SecItemCopyMatching`)
* If found, authenticate with credentials
  - Succeed: logged in
  - Fail
    + Prompt user for new password
    + Authenticate
    + Update secret in keychain (`SecItemUpdate`)
* If not found
  - Prompt user for password
  - Authenticate
  - Add secret to keychain (`SecItemAdd`)

The main workflow (first three points above) has no user prompt except for the first time or when user has changed credentials using a different client.  For periodic re-authentication the user is usually not prompted; that’s taken care of silently.

Lastly, if the user decides to disconnect from the portal entirely, the secret can be removed from the keychain (`SecItemDelete`).

!!! Warning: Poor documentation = Easy derailment
    [Official documentation][keychain-services] and most online resources talk about using this API w.r.t iCloud-based keychain using Swift.  Many important ideas are documented under attributes instead of programming guides.  Older [archived documentation][keychain-services-old] might help.

[keychain-services]: https://developer.apple.com/documentation/security/keychain_services
[keychain-services-old]: https://web.archive.org/web/20170724173620mp_/https://developer.apple.com/library/content/documentation/Security/Conceptual/keychainServConcepts/01introduction/introduction.html#//apple_ref/doc/uid/TP30000897-CH203-TP1

# Keychain

On iOS there’s only one (iCloud) keychain holding may keychain items.  It’s un/locked when the device is un/locked.  Different apps access their[^fn1] keychain items, but all the items are in the same keychain.

macOS supports multiple keychains.  However, there’s a default keychain (`login`) that most apps use, unless it’s an app that wants to create and manage keys like the in-built _Keychain Access_ app.  Keychains and associated items are expected to be managed by the user via Keychain Access.

[^fn1]: Authored by the app or the items whose access group the app belongs to.

# Keychain Items

A keychain contains arbitrary number of keychain items.  Item = data + attributes.  Relevant attributes for a keychain item depend on its class/type:

* Generic Password
* Internet Password
* Certificate
* (Cryptographic) Key
* Identity

The `SecItem*` family of functions expect a dictionary, in which class (`kSecClass`) and data (`kSecValueData`) are required keys.  Optionally it can contain

* Attributes
  - `kSecAttrSynchronizable`: Enable sync with iCloud
  - `kSecUseDataProtectionKeychain`: (macOS-only; 10.15+) for multiple apps access through access groups
  - `kSecAttrAccessGroup`: item’s access group
  - `kSecAttrAccessible`: conditions of [keychain items accessibility][accessibility-article] e.g. when locked, after first unlock, always, etc.
  - `kSecAttrAccess`: ACL-based sharing (access group is preferred over this)
* Return types
  - `kSecReturnData`: retrieve value
  - `kSecReturnAttributes`: retrieve attributes
  - `kSecMatchLimit`: results limit

Search results may be limited in number and kind.  Depending on the return attributes queried for `SecItemCopyMatching` returns `CFData` (just the data), `CFDictionaryRef` (one result with attributes) or `CFArrayRef` (many results).

!!! Tip: iOS vs macOS attributes
    Some attributes are exclusive to macOS, some to iOS and some are shared.

Trying to add a duplicate (existing) item returns `errSecDuplicateItem`.  A duplicate means the same class with same set of composite primary keys.  [What’s considered the primary key][primary-key] depends on the item class.


[accessibility-article]: https://developer.apple.com/documentation/security/keychain_services/keychain_items/restricting_keychain_item_accessibility
[primary-key]: https://developer.apple.com/documentation/security/errsecduplicateitem

# Sharing Secrets

## Access Groups

!!! Note: macOS Non-synchronized Items
    This mechanism of sharing keys, the only option on iOS, is allowed on macOS (since 10.15) only if the keychain item has [`kSecUseDataProtectionKeychain`][ksecusedataprotectionkeychain] or `kSecAttrSynchronizable` set.  When you want your keys to be _not_ sync’d with iCloud but still want access groups, set `kSecUseDataProtectionKeychain`.

Sharing secrets is enabled through _access groups_.  There should be no user intervention needed to access items shared.  See [Sharing Access to Keychain Items Among a Collection of Apps][access-group-sharing] for detailed write-up.  Though some terms used there are unclear and this guide clears them up.

Every app can belong to access groups of three kinds: App ID (exclusive to the app), Keychain Access Groups (inter-app sharing of keychain items) and App Groups (inter-app sharing of more than keychain items).

To make an app belong to a keychain access group, enable the _Key Sharing_ capability under [entitlement][].  Add the required keychain access group name; this is arbitrary.  **Make sure that all apps needing sharing has this keychain access group listed; all the apps should bear the same team ID**.

!!! Warning: Keychain Access Group
    When specifying keychain access group _in code_, make sure to prepend it with `$(TEAM_ID).`.  Team ID a.k.a AppIdentifierPrefix is issued by Apple for every developer account.  [Know your Team ID][teamid].

A keychain item can belong to exactly one access group.  The item’s creating and accessing apps should also belong to the same access group.  Consequently, when you `SecItemAdd`, creating an item in a group that’s non-accessible to an app is disallowed.  Missing to give a access group defaults the item to app’s first keychain access group; if that’s missing, then app ID becomes the items access group (leading to an exclusive secret).

In Xcode 11.3, in the project properties, under _Signing & Capabilities_ tab, click `+ Capability` and select `Keychain Sharing`.  Once done, under the new section _Keychain Sharing_, add the access group name to _Keychain Groups_ list.  Without this step, you’d get `-34018: Error: A required entitlement isn't present.`

[Check a (signed) app’s entitlements][app-entitlements] with `codesign -d --entitlements :- ~/Test.app`.

[ksecusedataprotectionkeychain]: https://developer.apple.com/documentation/security/ksecusedataprotectionkeychain
[access-group-sharing]: https://developer.apple.com/documentation/security/keychain_services/keychain_items/sharing_access_to_keychain_items_among_a_collection_of_apps
[entitlement]: https://developer.apple.com/documentation/bundleresources/entitlements
[teamid]: https://stackoverflow.com/q/18727894/183120
[app-entitlements]: https://apple.stackexchange.com/q/52675

## Access Control Lists

This is the older mechanism to share keychain items; however [Apple recommends access groups over ACL][ksecusedataprotectionkeychain] for portability.

Each protected keychain item has an associated access control list (ACL).  This list has a bunch of dictionaries (entries).  Every dictionary has an array of operations mapped to an array of trusted apps.  When an app requests access to an item for a particular operation.  The system checks the ACL for an entry containing said operation:

* Operation absent: denied
* Operation present
  - Check requesting app’s presence in trusted apps list
    + Allowed: silently grant access
    + Disallowed: prompt user for _Deny_, _Allow_, _Always Allow_

If always allowed, add app to trusted app list for that entry.


<!-- Markdeep: --><style class="fallback">body{visibility:hidden;white-space:pre;font-family:monospace}</style><script src="markdeep.min.js"></script><script src="https://morgan3d.github.io/markdeep/latest/markdeep.min.js"></script><script>window.alreadyProcessedMarkdeep||(document.body.style.visibility="visible")</script><script>window.markdeepOptions = { tocStyle: 'none' };</script>

