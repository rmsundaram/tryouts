//
//  MyView.h
//  AppKitIODemo
//
//  Created by Sundaram Ramaswamy on 05/12/18.
//  Copyright © 2018 Personal. All rights reserved.
//

#import <Cocoa/Cocoa.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyView : NSView

@end

NS_ASSUME_NONNULL_END
