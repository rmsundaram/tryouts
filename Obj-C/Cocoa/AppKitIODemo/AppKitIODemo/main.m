//
//  main.m
//  AppKitIODemo
//
//  Created by Sundaram Ramaswamy on 05/12/18.
//  Copyright © 2018 Personal. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
