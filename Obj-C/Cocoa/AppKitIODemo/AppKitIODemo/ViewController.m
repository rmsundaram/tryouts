//
//  ViewController.m
//  AppKitIODemo
//
//  Created by Sundaram Ramaswamy on 05/12/18.
//  Copyright © 2018 Personal. All rights reserved.
//

#import "ViewController.h"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Do any additional setup after loading the view.
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

- (IBAction)repaintWindow:(id)sender {
    NSView* myView = [self view];
    myView.needsDisplay = YES;
}

@end
