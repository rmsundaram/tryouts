//
//  MyView.m
//  AppKitIODemo
//
//  Created by Sundaram Ramaswamy on 05/12/18.
//  Copyright © 2018 Personal. All rights reserved.
//

#import "MyView.h"

@implementation MyView

- (void)drawRect:(NSRect)dirtyRect {
    [super drawRect:dirtyRect];
    
    srandom((unsigned)time(NULL));
    int r = random() % 255;
    int g = random() % 255;
    int b = random() % 255;
    [[NSColor colorWithRed:r/255.0f green:g/255.0f blue:b/255.0f alpha:1.0] set];
    
    [NSBezierPath fillRect:dirtyRect];
}

@end
