//
//  AppDelegate.m
//  AppKitIODemo
//
//  Created by Sundaram Ramaswamy on 05/12/18.
//  Copyright © 2018 Personal. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}


- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}


@end
