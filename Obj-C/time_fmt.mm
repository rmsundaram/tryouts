// clang++ -std=c++14 -framework Foundation time_fmt.mm

#import <Foundation/Foundation.h>
#import <OSLog/OSLog.h>

int main() {
  auto fmt = [[NSDateComponentsFormatter alloc] init];
  NSLog(@"%@\n", [fmt stringFromTimeInterval:21267]);
  [fmt release];

  os_log_t log_obj = os_log_create(/*subsystem=*/ "com.MyCompany.Time.Fmt",
                                   /*category=*/ "Utility");
  os_log_with_type(log_obj, OS_LOG_TYPE_INFO, "Abracadabra!!!");
  os_log_error(log_obj, "I'm an error message!");
  os_log_debug(log_obj, "A debug message");  // shows up only in Console.app
  // ‘log show --debug’ command doesn’t show OS_LOG_TYPE_DEBUG messages.  Though
  // log object reports that DEBUG is disabled, Console.app shows them.
  NSLog(@"Is debug enabled? %s",
        (os_log_type_enabled(log_obj, OS_LOG_TYPE_DEBUG) == YES) ?
        "Yes" : "No");

  // Filter command (without ‘--info’ only default, error and fault are shown)
  // log show --predicate 'subsystem == "com.MyCompany.Time.Fmt"' --last 5m --info

  // Not strictly necessary, but good to do!
  os_release(log_obj);
}
