// clang++ -std=c++17 -framework Foundation objc_dtor.mm

#import <Foundation/Foundation.h>

#include <iostream>
#include <memory>

// Anti-RAII: dealloc isn’t guaranteed to be called on time and in order, so
// it’s discouraged to release resources in dealloc!  Recourse is to use a
// custom destructor, say deinit, and explicitly call it when needed and not
// depend on dealloc for anything.  The memory reclamation will happen on its
// own sweet time.
// https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/MemoryMgmt/Articles/mmPractical.html#//apple_ref/doc/uid/TP40004447-SW13
//
// When binding a control’s delegate, data source, or similar property to an
// object, it’s wise to clear these properties when the object is deinitialized
// without which object’s dealloc may not be called since the control might be
// retaining and holding the object from destruction. Likewise [NSObject bind:]
// should be countered with [NSObject unbind:] calls.
//
// https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/MemoryMgmt/Articles/mmPractical.html#//apple_ref/doc/uid/TP40004447-1000810
// https://developer.apple.com/library/archive/releasenotes/ObjectiveC/RN-TransitioningToARC/Introduction/Introduction.html#//apple_ref/doc/uid/TP40011226

// Debugging object-not-dealloc’d issues seems hard since one shouldn’t look at
// an object’s retain count for any purpose. Also Apple’s recommendation is to
// NOT depend on dealloc to manage scarce resources (anti-RAII) and have a
// custom deinit function which should be called voluntarily.
// https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/MemoryMgmt/Articles/mmPractical.html#//apple_ref/doc/uid/TP40004447-SW13
// https://stackoverflow.com/a/656927/183120

// Debugging use-after-free is relatively easier: zombies! When retain count
// drops to 0 an object isn’t deleted but kept around to catch _use-after-free_
// issues. Apple provides `NSZombieEnabled` feature for this. Chromium project
// replaces `NSObject`’s `-dealloc` with `ZombieDealloc`
// (`components/crash/core/common/objc_zombie.mm`). In
// `+[BrowserCrApplication initialize] ` they `ZombieEnable(true /*
// zombieAllObjects */, 10000)`.  When an object is destroyed, instead of
// dealloc, ZombieDealloc is called; technically is considered
// destroyed. Internally `ZombieDealloc` keeps the object around by converting
// its `isa` pointer some runtime generated class (`CrZombie`); objects of
// zombie class simply log all messages they receive to notifying some code
// committing the use-after-free crime!
//
//  Method m = class_getInstanceMethod([NSObject class], @selector(dealloc));
//  method_setImplementation(m, reinterpret_cast<IMP>(ZombieDealloc));
//
// https://ktatsiu.wordpress.com/2012/04/24/
//         learn-objective-c-putting-zombies-into-your-ios-bag-of-tricks/
// https://stackoverflow.com/a/3220703/183120

struct A {
  ~A() { std::cout << __func__ << '\n'; }
  int val = 0;
};

@interface Simple: NSObject {
  A a;  // construction ↓
  A b;  // destruction ↑
}
- (id)initWithValue:(int)val;
- (int)getValue;
@end
@implementation Simple
- (id)initWithValue:(int)val {
  if ((self = [super init])) {
    a.val = val;     // Order here doesn’t matter like C++
    b.val = val + 1;
  }
  return self;
}

- (int)getValue {
  return a.val;
}

// This isn’t guaranteed to be called on time, so don’t depend on it to release
// managed resources!  Use a custom dtor for that.
-(void)dealloc {
  std::cout << __func__ << '\n';
  [super dealloc];
}
@end


int main() {
  Simple* o = [[Simple alloc] initWithValue:4];
  std::cout << [o getValue] << '\n';
  [o release];
  CFArrayRef r = CFArrayCreate(nullptr, nullptr, 0, nullptr);
  using CFType = std::remove_pointer_t<CFTypeRef>;
  std::unique_ptr<CFType, decltype(&CFRelease)> up(r, CFRelease);
  // if (r) CFRelease(r);
}
