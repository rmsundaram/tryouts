// https://clang.llvm.org/docs/AutomaticReferenceCounting.html
// clang++ -framework Foundation arc.mm
// clang++ -framework Foundation -fobjc-arc arc.mm

// Good overview of manual and automatic reference counting (ARC)
// https://www.tutorialspoint.com/objective_c/objective_c_memory_management.htm
// http://blog.mugunthkumar.com/articles/migrating-your-code-to-objective-c-arc
// https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/
//         MemoryMgmt/Articles/mmAutoreleasePools.html

// When returning an object from a function, not part of the new, alloc, retain,
// copy, create family, ARC will auto-insert autorelease, else nothing.  For
// non-returned objects it’ll auto-insert release by the end of the block. Hence
// naming a function following these conventions is vital. This not only applies
// to what’s returned by platform functions but also to the function which
// returns to the platform e.g. from makeTouchBar we return an autoreleased
// NSTouchBar since it’s not named NARC, so the platform isn’t going to release
// it, while had it been newTouchBar, then we don’t’ve to autorelease.
//
// https://stackoverflow.com/q/17314231/183120
// https://www.tomdalling.com/blog/cocoa/
//         an-in-depth-look-at-manual-memory-management-in-objective-c

#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

#include <memory>

#if !__has_feature(objc_arc)
#define MANUAL 1
#endif

@interface SampleClass:NSObject

{
  // add ivars here, not properties (which belong outside this scope)
}

// @property(nonatomic, assign) BOOL isAlive;

- (void)sampleMethod;
@end

@implementation SampleClass

// associate property to a ivar
// @synthesize isAlive = isAlive_;

- (void)sampleMethod {
  NSLog(@"Hello, World! \n");
}

- (void)dealloc {
  NSLog(@"Object deallocated\n");

  // don’t call [super dealloc] with ARC
  // https://stackoverflow.com/a/7292157/183120
#ifdef MANUAL
  [super dealloc];
#endif
}
@end

#ifdef MANUAL
struct NSDeleter
{
  void operator()(NSObject *o) {
    if (o)
      [o release];
  }
};
#endif

int main() {

  // without pool
  {
    // NSObject convention: if method name contains "new", "alloc",
    // "retain", "copy" or "create" (Create NARC), manually release
    // its return value. If not -- and this applies to all sorts of
    // shorthand constructors like NSString’s stringWithFormat -- then
    // don't release it; it’d be auto-released.
    // https://stackoverflow.com/q/3067120/183120
    // https://developer.apple.com/library/archive/documentation/Cocoa
    //        /Conceptual/MemoryMgmt/Articles/mmRules.html
    SampleClass *obj1 = [[SampleClass alloc]init];
    [obj1 sampleMethod];

    // these calls are disallowed when ARC is enabled
#ifdef MANUAL
    NSLog(@"%lu\n", [obj1 retainCount]);
    [obj1 retain];
    NSLog(@"%lu\n", [obj1 retainCount]);
    [obj1 release];
    NSLog(@"%lu\n", [obj1 retainCount]);

    [obj1 release];
#endif
  }

  NSLog(@"--- autorelease ---");

  // autoreleasepool has nothing to do with ARC; both are parallel concepts
  @autoreleasepool {
    SampleClass *obj2 = [[SampleClass alloc]init];

#ifdef MANUAL
    // decrements receiver’s retain count by end of current autorelease pool
    // block; blocks may be nested. top-most autorelease pool block: main()
    [obj2 autorelease];
    // another popular style
    // SampleClass *obj2 = [[[SampleClass alloc]init] autorelease];
#endif

    [obj2 sampleMethod];
  }

#ifdef MANUAL
  NSLog(@"--- unique_ptr ---");
  std::unique_ptr<SampleClass, NSDeleter> up([[SampleClass alloc]init]);
  [up.get() sampleMethod];
  // When owning an object that’s also handed over to the platform,
  // say makeTouchBar, autorelease, retain and return to platform. The
  // retain is tallyed against the unqiue_ptr’s release and
  // autorelease will release object when platform is done with it.
  // https://stackoverflow.com/q/5192308/183120
#endif
}
