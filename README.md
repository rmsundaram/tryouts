# Tryouts

A repository for my personal workouts; a code lab.  [Code](https://bitbucket.org/rmsundaram/tryouts/src/dev) here is cross-platform and are generously embellished with comments and references to books and websites so that later on they can serve as tutorials to self and the community.

Pre-built Windows binaries of some projects are available in the [Downloads section](https://bitbucket.org/rmsundaram/tryouts/downloads); OpenGL workouts require a GPU supporting OpenGL 3.3 or later; make sure you've the latest video drivers installed; do not run them remotely.  Use the [GLversion](https://bitbucket.org/rmsundaram/tryouts/src/dev/CG/GLversion) workout to check the version your machine currently supports.  `CG` code is tested on Linux and Windows with nVidia and Intel video drivers.

## Summary

* [`CG`](https://bitbucket.org/rmsundaram/tryouts/src/dev/CG) — Mid-sized projects to understand and implement various computer graphics concepts from the ground up.  These projects are written from scratch to learn the very basics of CG and hence use no high-level libraries, just the bare minimum: OpenGL (3.3+ shader-based, for rendering), [GLM](http://glm.g-truc.net/)  (for vector, quaternion and matrix classes), [GLI](http://gli.g-truc.net/) (for texture loading) and [GLFW](http://www.glfw.org/) (for window creation).  Everything else, right from primitives to collision detection to spatial data structures to frustum culling, was implemented manually as individual, stand-alone projects.  Highlights:
    + [Free-look (6 DoF) camera](https://bitbucket.org/rmsundaram/tryouts/src/dev/CG/FreeRoamCam) using matrices and [quaternions](https://bitbucket.org/rmsundaram/tryouts/src/dev/CG/Free-look camera/) (independently) and not `gluLookAt`.
    + [3D Picking using ray casting](https://bitbucket.org/rmsundaram/tryouts/src/dev/CG/RayCastPicking).
    + [Terrain Picking by grid traversal](https://bitbucket.org/rmsundaram/tryouts/src/dev/CG/Terrain/Picking).
    + [Multi-textured terrain with lighting](https://bitbucket.org/rmsundaram/tryouts/src/dev/CG/Terrain/Texturing).
    + [Quadtree-based frustum culling of the above terrain](https://bitbucket.org/rmsundaram/tryouts/src/dev/CG/Terrain/QuadTree).
    + Math calculations on a blackboard before coding something are strewn across directories as JPG/PNG/SVGs e.g. [`CG/Calculations`](https://bitbucket.org/rmsundaram/tryouts/src/dev/CG/Calculations), [AI Vision](https://bitbucket.org/rmsundaram/tryouts/src/dev/CG/AI Vision), etc.
    + Scene data read from an external scene description file with nothing hardcoded in code; can be changed without recompiling the source.
* [Partize](https://bitbucket.org/rmsundaram/tryouts/src/dev/CG/Partize) — Spatial Partitioning Analyser of two uniform grid scheme implementations: hash table and bitsets
* [Noise](https://bitbucket.org/rmsundaram/tryouts/src/dev/CG/Noise) — notes and demo on Perlin noise, curves, etc.
* [`Puyo`](https://bitbucket.org/rmsundaram/tryouts/src/dev/Puyo) — [ncurses](http://en.wikipedia.org/wiki/Ncurses)-based tile-matching game written in modern C++.
* Language workouts with explanatory comments, references to books and [StackOverflow](http://stackoverflow.com/users/183120/legends2k) posts
    + [`C++`](https://bitbucket.org/rmsundaram/tryouts/src/dev/C++)
    + [`Lua`](https://bitbucket.org/rmsundaram/tryouts/src/dev/Lua)
* [`Misc`](https://bitbucket.org/rmsundaram/tryouts/src/dev/Misc) — Miscellaneous code, markdown and configuration (dot) files.

Each directory in `CG` is an independent, self-contained project. The [MSYS2](http://sourceforge.net/projects/msys2/) environment is used as the toolchain for development and building; some additionally use [`CMake`](http://cmake.org/) or [`premake`](http://premake.github.io/); read [`setup`](https://bitbucket.org/rmsundaram/tryouts/src/dev/Misc/setup.md?fileviewer=file-view-default) for details. Language workouts are all self-contained (each file is a complete unit) and need nothing more than a compiler.
