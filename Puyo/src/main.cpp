/* main.cpp
 * Puyo Puyo
 * Copyright (C) 2015 Sundaram Ramaswamy, rmsundaram@gmail.com
 *
 * Puyo Puyo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Puyo Puyo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Puyo Puyo; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <iostream>
#include <array>
#include <utility>
#include <vector>
#include <memory>
#include <cstdlib>
#include <ctime>
#include <functional>
#include <chrono>
#include <thread>
#include <map>
#include <algorithm>
#include <cstdint>
#include <random>

#include <curses.h>

#include "board.hpp"
#include "graphics.hpp"

int main()
{
    constexpr uint8_t rows = 12u;
    constexpr uint8_t cols = 6u;

    Board<rows, cols> board;
    std::unique_ptr<Graphics> spGraphics(Graphics::Init(board.GetNumRows(), board.GetNumColumns()));

    bool play = true;
    Keys input = Invalid;
    while (play)
    {
        board.Update(input);

        spGraphics->Paint(board);
        spGraphics->Swap();

        // C++11's sleep is better than non-portable options: Sleep, usleep
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(50ms);

        input = spGraphics->GetInput();
        play = !(Keys::Quit & input);
    }
}
