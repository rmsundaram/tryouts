/* graphics.hpp
 * Puyo Puyo
 * Copyright (C) 2015 Sundaram Ramaswamy, rmsundaram@gmail.com
 *
 * Puyo Puyo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Puyo Puyo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Puyo Puyo; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef __GRAPHICS_HPP__
#define __GRAPHICS_HPP__

enum Colors
{
    Black,
    Red,
    Green,
    Blue,
    Cyan,
    Magenta,
    Yellow,
    White,
    BlackOnWhite
};

const chtype coin_chars[] = {
                               // using ACS_CKBOARD on bash terminals is ugly and buggy and
                               // some characters like ACS_DIAMOND aren't represented in some
                               // font; so sticking with ASCII characters is optimal
                               '_' | COLOR_PAIR(Colors::White),
							   '&' | COLOR_PAIR(Colors::Red) | A_BOLD,
							   '$' | COLOR_PAIR(Colors::Green) | A_BOLD,
							   '%' | COLOR_PAIR(Colors::Blue) | A_BOLD,
							   '@' | COLOR_PAIR(Colors::Magenta) | A_BOLD,
                               '0' | COLOR_PAIR(Colors::BlackOnWhite)
                            };
static_assert(std::extent<decltype(coin_chars)>::value == static_cast<size_t>(Puyo::Coin_Max),
              "Coin chars aren't matching the number of coin types");

class Graphics
{
public:
    static Graphics* Init(size_t rows, size_t columns);

    template <size_t ROWS, size_t COLUMNS>
    void Paint(const Board<ROWS, COLUMNS>& board)
    {
        PaintBoard(board);
        PaintDetails(board.GetNextCoins(), board.GetLevel(), board.GetScore());
        if (States::Game_Over == board.GetState())
        {
            PaintGameOver();
        }
    }

    void Swap();
    Keys GetInput();

    ~Graphics();

private:
    Graphics(WINDOW *window, size_t rows, size_t cols);

    void PaintGameOver();
    void PaintDetails(const std::pair<Puyo, Puyo> &next_coins,
                      unsigned level,
                      unsigned score);

    template <size_t ROWS, size_t COLUMNS>
    void PaintBoard(const Board<ROWS, COLUMNS>& board)
    {
        clear();

        for (uint8_t i = 0; i < ROWS; ++i)
        {
            for (uint8_t j = 0; j < COLUMNS; ++j)
            {
                move(board_yoffset + (i * row_offset), board_xoffset + (j * col_offset));
                waddch(pWindow, coin_chars[static_cast<size_t>(board.GetCell(i, j))]);
            }
        }
    }

    const size_t max_offset = 3;
    unsigned col_offset = 0, row_offset = 0;
    unsigned board_xoffset = 0, board_yoffset = 0;
    WINDOW *pWindow;
};

#endif  // __GRAPHICS_HPP__
