/* graphics.cpp
 * Puyo Puyo
 * Copyright (C) 2015 Sundaram Ramaswamy, rmsundaram@gmail.com
 *
 * Puyo Puyo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Puyo Puyo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Puyo Puyo; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#include <vector>
#include <memory>
#include <stdexcept>
#include <curses.h>
#include <cstdint>
#include <ctime>
#include <cstdlib>
#include <map>
#include <thread>
#include <chrono>
#include <random>

#include "board.hpp"
#include "graphics.hpp"

Graphics* Graphics::Init(size_t rows, size_t cols)
{
    return new Graphics(initscr(), rows, cols);
}

Graphics::Graphics(WINDOW *window, size_t rows, size_t cols) :
    pWindow(window)
{
    // precalculate the offset, do as minimal as possible in draw
    size_t maxX = 0, maxY = 0;
    getmaxyx(pWindow, maxY, maxX);
    if((maxY < rows) || (maxX < cols))
    {
        throw std::length_error("Try resizing your window to a bigger size, to run Puyo Puyo");
    }

    col_offset = static_cast<unsigned>(std::min((maxX/cols), max_offset));
    row_offset = static_cast<unsigned>(std::min((maxY/rows), max_offset));
    board_xoffset = static_cast<unsigned>((maxX - (col_offset * (cols - 1))) / 2);
    board_yoffset = static_cast<unsigned>((maxY - (row_offset * (rows - 1))) / 2);

    // setup ncurses
    noecho();
    cbreak();
    nodelay(pWindow, TRUE);
    nonl();
    keypad(pWindow, TRUE);
    curs_set(0);

    if (has_colors())
    {
        start_color();

        init_pair(Colors::Red, COLOR_RED, COLOR_BLACK);
        init_pair(Colors::Green, COLOR_GREEN, COLOR_BLACK);
        init_pair(Colors::Blue, COLOR_BLUE, COLOR_BLACK);
        init_pair(Colors::Cyan, COLOR_CYAN, COLOR_BLACK);
        init_pair(Colors::Magenta, COLOR_MAGENTA, COLOR_BLACK);
        init_pair(Colors::Yellow, COLOR_YELLOW, COLOR_BLACK);
        init_pair(Colors::White, COLOR_WHITE, COLOR_BLACK);
        init_pair(Colors::BlackOnWhite, COLOR_BLACK, COLOR_WHITE);
    }
}

Graphics::~Graphics()
{
    standend();
    refresh();
    curs_set(1);
    endwin();
}

Keys Graphics::GetInput()
{
    int key = wgetch(pWindow);
    switch(key)
    {
        case 'q':
        case 'Q':
            return Keys::Quit;
        case 'r':
        case 'R':
            return Keys::Restart;
        case KEY_UP:
            return Keys::Up;
        case KEY_DOWN:
            return Keys::Down;
        case KEY_LEFT:
            return Keys::Left;
        case KEY_RIGHT:
            return Keys::Right;
    };
    return Keys::Invalid;
}

void Graphics::Swap()
{
    refresh();
}

// helper for finding string length @ compile time
template <typename T,
          size_t N,
		  typename X = typename std::enable_if<((std::is_same<T, char>::value) ||
                                                (std::is_same<T, wchar_t>::value))>::type>
constexpr inline unsigned StrLiteralLen(const T (&/*str_name*/) [N], X */*dummy*/ = nullptr)
{
    return N - 1;
}

void Graphics::PaintGameOver()
{
    const char str_game_over[] = "Game Over!!!";
    int max_row = 0, max_col = 0;
    getmaxyx(pWindow, max_row, max_col);
    wattron(pWindow, A_REVERSE);
    mvprintw(max_row / 2, (max_col - StrLiteralLen(str_game_over)) / 2, "%s", str_game_over);
    wattroff(pWindow, A_REVERSE);
}

void Graphics::PaintDetails(const std::pair<Puyo, Puyo> &next_coins,
                            unsigned level,
                            unsigned score)
{
    const char str_title[] = "Puyo Puyo";
    const char str_title_underline[] = "---- ----";
    const char str_next[] = "Next";
    const char str_level[] = "Level ";
    constexpr auto max_level_length = 2;
    const char str_score[] = "Score ";
    constexpr auto max_score_length = 5;
    const char str_keys[] =      "Quit   Restart";
    const char str_hotkeys[] =  "-      -";

    int max_row, max_col;
    getmaxyx(pWindow, max_row, max_col);

    // title
    wattron(pWindow, A_BOLD | COLOR_PAIR(White));
    mvprintw(max_row / 5, (board_xoffset - StrLiteralLen(str_title)) / 2, "%s", str_title);
    mvprintw(1 + (max_row / 5),
             (board_xoffset - StrLiteralLen(str_title_underline)) / 2,
             "%s", str_title_underline);
    wattroff(pWindow, A_BOLD | COLOR_PAIR(White));

    // next coins
    wattron(pWindow, COLOR_PAIR(White));
    mvprintw((max_row * 2) / 5, (board_xoffset - StrLiteralLen(str_next)) / 2, "%s", str_next);
    move(2 + ((max_row * 2) / 5), (board_xoffset - 2) / 2);
    waddch(pWindow, coin_chars[static_cast<size_t>(next_coins.first)]);
    waddch(pWindow, coin_chars[static_cast<size_t>(next_coins.second)]);
    wattroff(pWindow, COLOR_PAIR(White));

    // level
    wattron(pWindow, COLOR_PAIR(Cyan));
    mvprintw((max_row * 3) / 5,
             (board_xoffset - StrLiteralLen(str_level) - max_level_length) / 2,
             "%s%u", str_level, level);
    wattroff(pWindow, COLOR_PAIR(Cyan));

    // score
    wattron(pWindow, COLOR_PAIR(Cyan));
    mvprintw(2 + ((max_row * 3) / 5),
             (board_xoffset - StrLiteralLen(str_score) - max_score_length) / 2,
             "%s%05u", str_score, score);
    wattroff(pWindow, COLOR_PAIR(Cyan));

    // shortcuts
    wattron(pWindow, A_BOLD | COLOR_PAIR(Yellow));
    mvprintw((max_row * 4) / 5, (board_xoffset - StrLiteralLen(str_keys)) / 2, "%s", str_keys);
    mvprintw(1 + ((max_row * 4) / 5), (board_xoffset - StrLiteralLen(str_keys)) / 2, "%s", str_hotkeys);
    wattroff(pWindow, A_BOLD | COLOR_PAIR(Yellow));
}
