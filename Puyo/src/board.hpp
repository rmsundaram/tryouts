/* board.hpp
 * Puyo Puyo
 * Copyright (C) 2015 Sundaram Ramaswamy, rmsundaram@gmail.com
 *
 * Puyo Puyo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Puyo Puyo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Puyo Puyo; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

#ifndef __BOARD_HPP__
#define __BOARD_HPP__

enum class Puyo : uint8_t
{
    Coin_None,
    Coin_Red,
    Coin_Green,
    Coin_Blue,
    Coin_Magenta,
    Coin_Dissolve,
    Coin_Max
};

enum class States : uint8_t
{
    Restart_Board,
    Place_Coins,
    Move_Coins,
    Check_Combos,
    Clear_Combos,
    Adjust_Coins,
    Game_Over
};

enum Keys
{
    Invalid  = 0,
    Quit     = 1 << 0,
    Up       = 1 << 1,
    Down     = 1 << 2,
    Left     = 1 << 3,
    Right    = 1 << 4,
    Restart  = 1 << 5
};

enum class CoinOrientation : uint8_t
{
    Left_Right,
    Bottom_Top,
    Right_Left,
    Top_Bottom
};

CoinOrientation& operator++(CoinOrientation &c);

struct CoinPosition
{
    CoinPosition(int row_arg = default_row, int col_arg = default_col)
        : row(row_arg)
        , col(col_arg)
    {
    }

    void operator++() { ++row; }
    bool operator==(const CoinPosition &that) const { return ((this->row == that.row) && (this->col == that.col)); }
    bool operator!=(const CoinPosition &that) const { return !(*this == that); }

    static constexpr int default_row = 0;
    static constexpr int default_col = 2;
    static constexpr int default_row_2 = default_row;
    static constexpr int default_col_2 = default_col + 1;

    int row, col;
};

constexpr int DEFAULT_THRESHOLD = 20;
constexpr size_t MIN_MATCH_COUNT = 4;

template <size_t ROWS, size_t COLUMNS>
class Board
{
    static_assert((ROWS >= MIN_MATCH_COUNT) && (COLUMNS >= MIN_MATCH_COUNT),
                  "Board size less than minimum match count of coins!");
public:
    Board() : orientation{CoinOrientation::Left_Right}
            , threshold{DEFAULT_THRESHOLD}
            , ticker{0}
            , current_state{States::Place_Coins}
            , cells{{std::array<Puyo, COLUMNS>()}}
            , rand_engine{}
            , rand_dist{1, 4}
            , score{0}
            , level{1}
    {
        // https://stackoverflow.com/q/15509270#comment139441459_15509942
        std::random_device r;
        std::seed_seq s{r(), r(), r(), r(), r(), r(), r(), r()};
        rand_engine.seed(s);
        next_coins = GenerateCoins();
    }

    constexpr int GetNumRows() const { return static_cast<signed>(ROWS); }
    constexpr int GetNumColumns() const { return static_cast<signed>(COLUMNS); }
    Puyo GetCell(size_t row, size_t col) const { return cells[row][col]; }
    std::pair<Puyo, Puyo> GetNextCoins() const { return next_coins; }
    unsigned GetScore() const { return score; }
    unsigned GetLevel() const { return level; }
    States GetState() const { return current_state; }

    // TODO: update this to state pattern?
    void Update(Keys input);

private:

    void Restart();
    void HandleInput(Keys input);
    std::pair<Puyo, Puyo> GenerateCoins();
    void PutCell(size_t row, size_t col, Puyo id) { cells[row][col] = id; }
    void PutCell(const CoinPosition &pos, Puyo id) { PutCell(pos.row, pos.col, id); }

    bool GeneratePlaceCoins();
    bool HandleInputMoveCoins(Keys input);

    void Settle(const CoinPosition &pos);
    void MoveLeft();
    void MoveRight();
    void MoveUp();
    bool MoveDown();
    Puyo GetLeft(int row, int col) const { return ((col - 1) < 0) ? Puyo::Coin_None : GetCell(row, col - 1); }
    Puyo GetBelow(int row, int col) const;

    bool CheckCombinations();
    bool MarkValidMatches();
    void ClearMatches();
    void UpdateScore(unsigned count);

    void AdjustCoins();
    bool CanMoveRight(const CoinPosition &pos) const { return IsPositionFree(pos.row, pos.col + 1); }
    bool CanMoveLeft(const CoinPosition &pos) const { return IsPositionFree(pos.row, pos.col - 1); }
    bool CanMoveDown(const CoinPosition &pos) const { return IsPositionFree(pos.row + 1, pos.col); }

    bool IsPositionFree(int row, int col) const;
    void MoveCoin(const CoinPosition &source, const CoinPosition &dest);
    void MoveCoins(int row_delta, int col_delta);
    bool IsOrientationHorizontal(CoinOrientation orientation) const;
    bool IsOrientationVertical(CoinOrientation orientation) const { return (! IsOrientationHorizontal(orientation)); }

    std::map<size_t, std::vector<CoinPosition>>  connections;
    CoinOrientation                              orientation;
    time_t                                       threshold, ticker;
    std::pair<CoinPosition, CoinPosition>        current_coins_pos;
    std::pair<Puyo, Puyo>                        current_coins, next_coins;
    States                                       current_state;
    std::array<std::array<Puyo, COLUMNS>, ROWS>  cells;
    std::mt19937                                 rand_engine;
    std::uniform_int_distribution<>              rand_dist;
    unsigned                                     score;
    unsigned                                     level;

    static constexpr auto points_per_block        = 10u;
    static constexpr auto bonus_above_min_match   = 5u;
    static constexpr auto level_shift_score_delta = 200u;
};

#include "board.inl"

#endif  // __BOARD_HPP__
