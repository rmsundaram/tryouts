/* board.inl
 * Puyo Puyo
 * Copyright (C) 2015 Sundaram Ramaswamy, rmsundaram@gmail.com
 *
 * Puyo Puyo is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * Puyo Puyo is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Puyo Puyo; if not, write to the Free Software Foundation, Inc.,
 * 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
 */

template <size_t ROWS, size_t COLUMNS>
void Board<ROWS, COLUMNS>::Update(Keys input)
{
    using namespace std::chrono_literals;
    HandleInput(input);
    switch(current_state)
    {
    case States::Restart_Board:
        Restart();
        break;
    case States::Place_Coins:
        current_state = GeneratePlaceCoins() ? States::Move_Coins : States::Game_Over;
        break;
    case States::Move_Coins:
        current_state = HandleInputMoveCoins(input) ? current_state : States::Check_Combos;
        break;
    case States::Check_Combos:
        current_state = CheckCombinations() ? States::Clear_Combos : States::Place_Coins;
        break;
    case States::Clear_Combos:
        std::this_thread::sleep_for(1s);
        ClearMatches();
        current_state = States::Adjust_Coins;
        break;
    case States::Adjust_Coins:
        std::this_thread::sleep_for(1s);
        AdjustCoins();
        current_state = States::Check_Combos;
        break;
    case States::Game_Over:
        break;
    }
}

template <size_t ROWS, size_t COLUMNS>
void Board<ROWS, COLUMNS>::Restart()
{
    current_state = States::Place_Coins;
    for (auto &row : cells)
    {
        row = std::array<Puyo, COLUMNS>();
    }
    threshold = DEFAULT_THRESHOLD;
    score = 0;
    level = 1;
}

template <size_t ROWS, size_t COLUMNS>
void Board<ROWS, COLUMNS>::HandleInput(Keys input)
{
    if (Keys::Restart & input)
    {
        current_state = States::Restart_Board;
    }
}

template <size_t ROWS, size_t COLUMNS>
std::pair<Puyo, Puyo> Board<ROWS, COLUMNS>::GenerateCoins()
{
    const Puyo coin1 = static_cast<Puyo>(rand_dist(rand_engine));
    const Puyo coin2 = static_cast<Puyo>(rand_dist(rand_engine));
    return std::make_pair(coin1, coin2);
}

template <size_t ROWS, size_t COLUMNS>
bool Board<ROWS, COLUMNS>::GeneratePlaceCoins()
{
    bool coinsPlaced = true;
    // end game condition
    if ((GetCell(CoinPosition::default_row,
                 CoinPosition::default_col) != Puyo::Coin_None) ||
         GetCell(CoinPosition::default_row_2,
                 CoinPosition::default_col_2) != Puyo::Coin_None)
    {
        coinsPlaced = false;
    }
    else
    {
        current_coins = next_coins;
        do
        {
            next_coins = GenerateCoins();
        }
        while(next_coins == current_coins);

        current_coins_pos.first = current_coins_pos.second = CoinPosition();
        current_coins_pos.second.col++;
        PutCell(current_coins_pos.first, current_coins.first);
        PutCell(current_coins_pos.second, current_coins.second);

        orientation = CoinOrientation::Left_Right;
        ticker = 0;
    }
    return coinsPlaced;
}

template <size_t ROWS, size_t COLUMNS>
bool Board<ROWS, COLUMNS>::HandleInputMoveCoins(Keys input)
{
    bool further_move_possible = true;

    if (Keys::Left & input)     MoveLeft();
    if (Keys::Right & input)    MoveRight();
    if (Keys::Up & input)       MoveUp();
    if (Keys::Down & input)     MoveDown();

    if (++ticker >= threshold)
    {
        if (!MoveDown())
        {
            further_move_possible = false;

            // fix unsettled coin
            Settle(current_coins_pos.first);
            Settle(current_coins_pos.second);
        }
        ticker = 0;
    }

    return further_move_possible;
}

template <size_t ROWS, size_t COLUMNS>
void Board<ROWS, COLUMNS>::Settle(const CoinPosition &pos)
{
    CoinPosition new_pos = pos;
    while (CanMoveDown(new_pos))
    {
        ++new_pos;
    }

    if (new_pos != pos)
    {
        MoveCoin(pos, new_pos);
    }
}

template <size_t ROWS, size_t COLUMNS>
void Board<ROWS, COLUMNS>::MoveLeft()
{
    const auto &leftMostCoin =
        (current_coins_pos.first.col < current_coins_pos.second.col) ?
        current_coins_pos.first : current_coins_pos.second;
    if (CanMoveLeft(leftMostCoin))
    {
        MoveCoins(0, -1);
    }
}

template <size_t ROWS, size_t COLUMNS>
void Board<ROWS, COLUMNS>::MoveRight()
{
    const auto &rightMostCoin =
        (current_coins_pos.first.col > current_coins_pos.second.col) ?
        current_coins_pos.first : current_coins_pos.second;
    if (CanMoveRight(rightMostCoin))
    {
        MoveCoins(0, 1);
    }
}

template <size_t ROWS, size_t COLUMNS>
void Board<ROWS, COLUMNS>::MoveUp()
{
    int new_row = -1, new_col = -1;
    switch(orientation)
    {
    case CoinOrientation::Left_Right:
        if (IsPositionFree(current_coins_pos.second.row - 1, current_coins_pos.second.col - 1))
        {
            new_row = current_coins_pos.second.row - 1;
            new_col = current_coins_pos.second.col - 1;
        }
        break;
    case CoinOrientation::Bottom_Top:
        if (IsPositionFree(current_coins_pos.second.row + 1, current_coins_pos.second.col - 1))
        {
            new_row = current_coins_pos.second.row + 1;
            new_col = current_coins_pos.second.col - 1;
        }
        break;
    case CoinOrientation::Right_Left:
        if (IsPositionFree(current_coins_pos.second.row + 1, current_coins_pos.second.col + 1))
        {
            new_row = current_coins_pos.second.row + 1;
            new_col = current_coins_pos.second.col + 1;
        }
        break;
    case CoinOrientation::Top_Bottom:
        if (IsPositionFree(current_coins_pos.second.row - 1, current_coins_pos.second.col + 1))
        {
            new_row = current_coins_pos.second.row - 1;
            new_col = current_coins_pos.second.col + 1;
        }
        break;
    }

    if(new_row != -1)
    {
        auto new_pos = CoinPosition(new_row, new_col);
        MoveCoin(current_coins_pos.second, new_pos);
        current_coins_pos.second = new_pos;
        ++orientation;
    }
}

template <size_t ROWS, size_t COLUMNS>
bool Board<ROWS, COLUMNS>::MoveDown()
{
    bool moved_down = false;

    if (IsOrientationHorizontal(orientation))
    {
        if (CanMoveDown(current_coins_pos.first) &&
            CanMoveDown(current_coins_pos.second))
        {
            MoveCoins(1, 0);
            moved_down = true;
        }
    }
    else
    {
        const auto &bottomMostCoin =
            (current_coins_pos.first.row > current_coins_pos.second.row) ?
            current_coins_pos.first : current_coins_pos.second;
        if(CanMoveDown(bottomMostCoin))
        {
            MoveCoins(1, 0);
            moved_down = true;
        }
    }
    return moved_down;
}

template <size_t ROWS, size_t COLUMNS>
Puyo Board<ROWS, COLUMNS>::GetBelow(int row, int col) const
{
    return ((row + 1) >= GetNumRows()) ? Puyo::Coin_None : GetCell(row + 1, col);
}

template <size_t ROWS, size_t COLUMNS>
bool Board<ROWS, COLUMNS>::CheckCombinations()
{
    size_t connection_counter = 0;
    std::array<std::array<std::shared_ptr<size_t>, COLUMNS>, ROWS> connection_ids;
    bool no_more_valid_rows = false;

    connections.clear();
    for (int i = GetNumRows() - 1; ((i >= 0) && (!no_more_valid_rows)); --i)
    {
        no_more_valid_rows = true;
        for (int j = 0; j < GetNumColumns(); ++j)
        {
            auto const puyo_current = GetCell(i, j);
            if (Puyo::Coin_None != puyo_current)
            {
                no_more_valid_rows = false;

                auto const puyo_left = GetLeft(i, j);
                auto const puyo_below = GetBelow(i, j);

                // merge lists case
                if ((puyo_current == puyo_below) && (puyo_below == puyo_left))
                {
                    if (*connection_ids[i][j - 1] != *connection_ids[i + 1][j])
                    {
                        // merge left's list with below's and set below's ID as left's
                        auto left_connections_iter = connections.find(*connection_ids[i][j - 1]);
                        auto &below_connections = connections[*connection_ids[i + 1][j]];
                        below_connections.insert(below_connections.end(),
                                                 left_connections_iter->second.cbegin(),
                                                 left_connections_iter->second.cend());

                        // since a shared_ptr is used, this updates
                        // all Puyos with old connection ID to new ID
                        *connection_ids[i][j - 1] = *connection_ids[i + 1][j];

                        // remove left's entry from map
                        connections.erase(left_connections_iter);
                    }

                    // handle current puyo
                    connection_ids[i][j] = connection_ids[i + 1][j];
                }
                else if (puyo_current == puyo_below)
                {
                    connection_ids[i][j] = connection_ids[i + 1][j];
                }
                else if (puyo_current == puyo_left)
                {
                    connection_ids[i][j] = connection_ids[i][j - 1];
                }
                else
                {
                    connection_ids[i][j] = std::make_shared<size_t>(++connection_counter);
                }
                auto &conn_list = connections[*connection_ids[i][j]];
                conn_list.emplace_back(CoinPosition(i, j));
            }
        }
    }

    return MarkValidMatches();
}

template <size_t ROWS, size_t COLUMNS>
bool Board<ROWS, COLUMNS>::MarkValidMatches()
{
    bool matches_found = false;
    auto conn_iter = connections.begin();
    while (conn_iter != connections.end())
    {
        if (conn_iter->second.size() >= MIN_MATCH_COUNT)
        {
            for (auto cell_iter = conn_iter->second.cbegin();
                 cell_iter != conn_iter->second.cend(); ++cell_iter)
            {
                PutCell(cell_iter->row, cell_iter->col, Puyo::Coin_Dissolve);
            }
            matches_found = true;
            ++conn_iter;
        }
        else
        {
            connections.erase(conn_iter++);
        }
    }
    return matches_found;
}

template <size_t ROWS, size_t COLUMNS>
void Board<ROWS, COLUMNS>::ClearMatches()
{
    auto count = 0;
    for (auto conn_iter = connections.begin();
         conn_iter != connections.end(); ++conn_iter)
    {
        for (auto cell_iter = conn_iter->second.cbegin();
             cell_iter != conn_iter->second.cend(); ++cell_iter)
        {
            PutCell(cell_iter->row, cell_iter->col, Puyo::Coin_None);
            ++count;
        }
    }

    UpdateScore(count);
}

template <size_t ROWS, size_t COLUMNS>
void Board<ROWS, COLUMNS>::UpdateScore(unsigned count)
{
    // update score
    score += count * points_per_block;
    score += (count - MIN_MATCH_COUNT) * bonus_above_min_match;
    if (score > (level * level_shift_score_delta))
    {
        ++level;
        threshold -= 2;
    }
}

template <size_t ROWS, size_t COLUMNS>
void Board<ROWS, COLUMNS>::AdjustCoins()
{
    for (int j = 0; j < GetNumColumns(); ++j)
    {
        auto empty_length = 0;
        for (int i = GetNumRows() - 1; i >= 0; --i)
        {
            // pull down, if empty
            if (Puyo::Coin_None == GetCell(i, j))
            {
                ++empty_length;
            }
            else if (empty_length)
            {
                PutCell(i + empty_length, j, GetCell(i, j));
                PutCell(i, j, Puyo::Coin_None);
            }
        }
    }
}

template <size_t ROWS, size_t COLUMNS>
bool Board<ROWS, COLUMNS>::IsPositionFree(int row, int col) const
{
    return (((row >= 0) && (row < GetNumRows())) &&
            ((col >= 0) && (col < GetNumColumns())) &&
            (Puyo::Coin_None == GetCell(row, col)));
}

template <size_t ROWS, size_t COLUMNS>
void Board<ROWS, COLUMNS>::MoveCoin(const CoinPosition &source, const CoinPosition &dest)
{
    PutCell(dest, GetCell(source.row, source.col));
    PutCell(source, Puyo::Coin_None);
}

// this can't be impl. using MoveCoin since one coin might
// over-write the other
template <size_t ROWS, size_t COLUMNS>
void Board<ROWS, COLUMNS>::MoveCoins(int row_delta, int col_delta)
{
    PutCell(current_coins_pos.first, Puyo::Coin_None);
    PutCell(current_coins_pos.second, Puyo::Coin_None);

    current_coins_pos.first.col += col_delta;
    current_coins_pos.second.col += col_delta;
    current_coins_pos.first.row += row_delta;
    current_coins_pos.second.row += row_delta;

    PutCell(current_coins_pos.first, current_coins.first);
    PutCell(current_coins_pos.second, current_coins.second);
}

template <size_t ROWS, size_t COLUMNS>
bool Board<ROWS, COLUMNS>::IsOrientationHorizontal(CoinOrientation orientation) const
{
    return ((CoinOrientation::Left_Right == orientation) || (CoinOrientation::Right_Left == orientation));
}

inline
CoinOrientation& operator++(CoinOrientation &c)
{
    c = static_cast<CoinOrientation>((static_cast<uint8_t>(c) + 1u) % 4u);
    return c;
}
