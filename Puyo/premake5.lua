local action = _ACTION or ""

workspace "Puyo"
        location ("build/" .. action)
        configurations { "Debug", "Release" }

        project "Puyo"
                kind "ConsoleApp"
                language "C++"
                files { "src/*.cpp" }
                        buildoptions { "-Wall", "-Wextra", "-pedantic" }
                        cppdialect "C++14"
                        targetname "puyo"
                        vpaths { ["inc"] = { "**.h", "**.hxx", "**.hpp" },
                                 ["src"] = { "**.c", "**.cxx", "**.cpp" } }

                configuration "windows"
                        includedirs { "C:/Work/Libs/PDCurses" }
                        libdirs { "C:/Work/Libs/PDCurses/win32" }
                        links { "pdcurses" }

                configuration "linux"
                        links { "ncurses" }

                configuration "macosx"
                        links { "curses" }

                configuration "Debug"
                        defines { "_DEBUG" }
                        symbols "On"
                        objdir ("build/" .. action .. "/Debug/obj")
                        targetdir ( "build/" .. action .. "/Debug/bin" )

                configuration "Release"
                        defines { "NDEBUG" }
                        optimize "On"
                        objdir ("build/" .. action .. "/Release/obj")
                        targetdir ( "build/" .. action .. "/Release/bin" )

                -- statically link core C++ libraries to the binary
                configuration { "Release", "windows", "gmake" }
                        linkoptions { "-static -static-libgcc -static-libstdc++" }
