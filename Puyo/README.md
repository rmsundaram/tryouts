# Overview

[Puyo Puyo][1] is a tile-matching puzzle game on the lines of Tetris. From the start, the project was written with portability in mind. The engine code (`Board` class), handling core logic and input, is completely independent of rendering code (`Graphics` class), so that it may be plugged to any interface (GUI/TUI) later on. A minimal, terminal interface was written to make a cross-platform game out of the engine using [`ncurses`][2]; this choice is again to maintain portability and minimalism: to be playable on a simple terminal/console window on any platform — tested on Linux and Windows.

This game is written in modern C++; new features of C++14 are used when possible. The board size may be changed at compile-time by simply changing two constants in `main.cpp`.

Pre-built binary for the Windows platform is available in the [Downloads section](http://bitbucket.org/rmsundaram/tryouts/downloads).

![Puyo.png](https://bitbucket.org/repo/no48Gg/images/4252997671-Puyo.png)

# Controls

Action        | Key
--------------|-----
Rotate        | ↑
Move left     | ←
Move right    | →
Drag down     | ↓

# Matching Logic
After every coin pair placement, the code which checks for coins to be cleared uses a neat trick using `shared_ptr` worth mentioning. Instead of following a non-linear path, based on occupied locations, this algorithm parses the whole board exactly once linearly. Visiting every cell, if it's not empty, an `int` ID is assigned to it own by a `shared_ptr`. For the following cell, if the previous cell or the one below it is of the same colour, then its `shared_ptr`, would be assigned the same ID, not by creating a new `int` but by sharing the other one's. Every ID has a vector of locations which tell where all this ID is present. Once the visit is complete, the vectors with size ≥ 4 would be marked for clearance. A small problem that would arise in this algorithm is, when visiting a new row's first cell, although a new ID is assigned to it, it may end up as part of an older connection chain E.g.

```
RR-G
PRRB
```

Here, `P` will have an ID of `0`, the following two `R`s would have an ID of `1`, `B` would be `2`. For the second row's first `R`, although the coins before (none) and below it are not matching, it should eventually become a part of the ID `1` chain. However, according to the aforementioned logic, a new ID of `3` would be assigned. Now for the following `R`, when both before and below coin types match (`R`), the below one is chosen as it'd the older of the two. Although this cell was handled properly, we still've the onus of updating all members of ID `3` to ID `1`. This is done by simply assigning a value of `1` to the previous cell's ID and every cell who had `3` would now be updated to `1` since they're all pointing to the same `int` via `shared_ptr`.

# Build

```
cmake -B build
cmake --build build
./build/Puyo
```

# Dependencies

* [CMake][7] (or [premake5][5])
* ncurses
  - Or other compatible ones like [pdcurses][]
* C++ toolchain
  - Tested on Linux, Windows and macOS with GCC, MSVC and Clang toolchains

# Known Issues

- Doesn’t run on MSYS2; ncurses errors out: `Error opening terminal: xterm-256color.`.
  Binary built with MSYS2/MINGW64 works on `cmd.exe` though.

# License

Please refer `COPYING` for the ([GPL version 2][6]) lisence text.

[1]: http://en.wikipedia.org/wiki/Puyo_Puyo
[2]: http://en.wikipedia.org/wiki/Ncurses
[4]: http://sourceforge.net/projects/mingw-w64
[5]: http://premake.github.io/
[6]: http://www.gnu.org/licenses/gpl-2.0.html
[7]: http://cmake.org/
[pdcurses]: https://pdcurses.org/
